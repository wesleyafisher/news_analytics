__HTTP_STATUS_CODE
200
__TITLE
Iraq Troops Enter ISIS-Seized Mosul for 1st Time in 2 Years
__AUTHORS
__TIMESTAMP
Nov 1 2016, 9:38 am ET
__ARTICLE
 BAZWAYA, Iraq  Iraq's special forces entered the outskirts of Mosul on Tuesday and were advancing toward its more urban center despite fierce resistance by ISIS fighters who hold the city, an Iraqi general said. 
 Troops have entered Gogjali, a neighborhood inside Mosul's city limits, and were only 800 yards from the more central Karama district, according to Maj. Gen. Sami al-Aridi of the Iraqi special forces. 
 "The special forces have stormed in," he said. "Daesh is fighting back and have set up concrete blast walls to block off the Karama neighborhood and our troops' advance," he said, using the Arabic acronym ISIS. Bombs have been laid along the road into the city, he added. 
 It was the first time Iraqi troops have set foot in Mosul in over two years, after they were driven out by a much smaller group of ISIS extremists in 2014. Mosul is the final ISIS bastion in Iraq, the city from which it declared a "caliphate" stretching into Syria, and its loss would be a major defeat for the jihadis. 
 Yet entering Mosul could be the start of a grueling and slow operation for the troops, as they will be forced to engage in difficult, house-to-house fighting in urban areas. The operation is expected to take weeks, if not months. The special forces troops remain some 5 miles from the center of the city, Iraq's second-largest. 
 The morning's action opened up with artillery, tank and machine gun fire on ISIS positions on the edge of the Gogjali neighborhood, with the extremists responding with guided anti-tank missiles and small arms to block the advance. Airstrikes by the U.S.-led coalition supporting the operation added to the fire hitting the district. 
 The U.S. military estimates ISIS has 3,000-5,000 fighters in Mosul and another 1,500-2,500 in its outer defensive belt. The total includes about 1,000 foreign fighters. They stand against an anti-ISIS force that including army units, militarized police, special forces and Kurdish fighters totals more than 40,000 men. 
 Prime Minister Haider al-Abadi said on Monday that Iraqi forces were trying to close off all escape routes for the several thousand ISIS inside Mosul. 
 "God willing, we will chop off the snake's head," he told state television. "They have no escape, they either die or surrender." 
 The United Nations has said the Mosul offensive could trigger a humanitarian crisis and a possible refugee exodus, with up to 1 million people fleeing in a worst-case scenario. 
 The International Organisation for Migration said that so far nearly 18,000 people had been displaced since the start of the campaign on Oct. 17, excluding those forced back into Mosul by the retreating jihadis. 
