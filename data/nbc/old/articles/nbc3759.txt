__HTTP_STATUS_CODE
200
__TITLE
Muellers Team Traveled to Interview Ex-Spy Involved in Dossier
__AUTHORS
Ken Dilanian
__TIMESTAMP
Oct 5 2017, 8:36 pm ET
__ARTICLE
 WASHINGTON  Members of Special Counsel Robert Mueller's team traveled to interview the former British intelligence officer who authored a dossier alleging collusion between the Trump campaign and Russia, a source close to the ex-spy tells NBC News. 
 Few other details were forthcoming about the interview, but Mueller's interest in Christopher Steele puts a new focus on the 35-page dossier he compiled, which includes salacious sexual allegations that then-president-elect Donald Trump denied. CNN first reported Thursday that the interview with Mueller's team and Steele took place. 
 Steele, who works in the private intelligence business in London, was hired by an American firm, Fusion GPS, to compile opposition research about Trump during the election campaign. The dossier laid out the results of his information-gathering. Fusion GPS was co-founded by a former Wall Street Journal reporter, Glenn Simpson. 
 Related: Senate Intel Heads Say Trump-Russia Collusion Is Still Open Question 
 Steele, who was once a spy in Russia and has spent a career developing an expertise on the country, relied on a network of sources and information collectors, some of whom were paid, according to the source. He did not travel to Russia himself, the source said. 
 At one point after the FBI began investigating in July 2016, NBC News has reported, the FBI considered paying Steele to continue his information-gathering, but that never came to fruition. 
 Former U. S. intelligence officials who worked with him have vouched for Steele's professionalism and his Russia expertise. Also, Steele had provided the FBI significant information for its investigation of corruption in international soccer. But some former intelligence officials have cast doubt on some of the dossier's claims. 
 The dossier grew out of an effort by an unknown Republican person or group to gather opposition research on Trump during the primaries, and an unknown Democratic person or group took over paying for the effort during the general election, the source said. 
 Included in the dossier is an unproven allegation that Russian intelligence agencies have video recordings of Trump and prostitutes at Moscow's Ritz Carlton hotel. 
 Then-FBI Director James Comey told Trump about the dossier after his first intelligence briefing shortly before he took office. 
 At his first news conference in January, Trump denounced the document, which was first published by Buzzfeed. 
 "A thing like that should have never been written, it should never have been had and certainly should never have been released," he said, later saying the memo was written by "sick people [who] put that crap together." 
 Related: Senate Intelligence Heads Showcase Bipartisanship in Russia Probe 
 The sex allegation is only a small part of the document, and perhaps only marginally relevant to the larger question that is the subject of Mueller's investigation: Whether the Trump campaign colluded with the Russian intelligence effort to interfere in the presidential election with hacking, leaking and fake news. 
 The dossier asserts that the Trump campaign engaged, as the document puts it, in a "well-developed conspiracy of cooperation," with Russian intelligence agencies as they sought to interfere in the 2016 presidential election. 
 Sen. Richard Burr, the chairman of the Senate intelligence committee, said this week that "we have been incredibly enlightened in our ability to rebuild backwards the Steele dossier up to a certain date." Two committee sources told NBC News the intelligence committee has verified part of the dossier, but they won't say which parts. 
 U.S. officials have said that the FBI also has verified aspects of the dossier. It's unclear what those are. 
 But some claims in the dossier seem to line up with known facts. 
 For example, the dossier says Trump sought to secure business deals in Russia, saying, "Regarding TRUMP's claimed minimal investment profile in Russia, a separate source with direct knowledge said this had not been for want of trying. TRUMP's previous efforts had included exploring the real estate sector in St. Petersburg as well as Moscow" 
 In August, Trump lawyer Michael Cohen acknowledged that during the Republican primaries in 2016, he was seeking on Trump's behalf to build a Trump Tower in Moscow. 
 James Clapper, the former director of national intelligence, said in June that Trump asked him to refute the claims in the dossier. 
 "I could not and would not," Clapper said he replied. 
 CORRECTION (Oct. 6, 6 p.m.: An earlier version of this article erred in describing the date when major news media first reported that hackers linked to Russia had attacked the Democratic National Committee. It was June 14, not July 28. The Steele dossier did not mention the DNC hacks until after they were reported in the news media. 
