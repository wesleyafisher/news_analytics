__HTTP_STATUS_CODE
200
__TITLE
Poor Sleep Raises Alzheimers Risk
__AUTHORS
Maggie Fox
__TIMESTAMP
Aug 24 2017, 10:06 am ET
__ARTICLE
 Theres more evidence that losing sleep can raise the risk of Alzheimers disease. 
 Its the latest in a series of studies that show even a little sleep loss can add up to a greater risk of Alzheimers, the leading cause of dementia. 
 The study, published in Wednesdays issue of the journal Neurology, showed that the dreaming phase of sleep, called REM or rapid eye movement sleep, affects the risk. 
 And the amount of sleep loss may not be enough for the person affected to even notice. 
 Sleep disturbances are common in dementia but little is known about the various stages of sleep and whether they play a role in dementia risk, said Matthew Pase of the Boston University School of Medicine, who helped lead the study. 
 Related: Here's How Sleep Loss Might Affect Alzheimer's 
 Other studies have pointed to sleep in general, or another phase of sleep called called slow-wave sleep. Theres evidence that sleep apnea  a type of interrupted breathing during sleep  also raises the risk of developing dementia. 
 More than 5 million Americans have Alzheimer's and the number is expected to grow as the population ages. There's no cure, and treatments do not work well. 
 Drugs such as Aricept, known also as donezepil, and Namenda can reduce symptoms for a time, but they do not slow the worsening of the disease. 
 There's no guaranteed way to prevent dementia, but exercise, healthy diet, specific types of brain training and controlling blood pressure can all help. 
 Experts are looking at sleep patterns to see if they can not only help predict the risk of dementia, but to see if restoring healthful sleep can treat or even prevent it. 
 Related: How Can You Prevent Alzheimer's? 
 The Boston team looked at 321 people over 60 who volunteered for a sleep study back in the 90s. They were followed for about a dozen years, during which time 32 developed dementia. Of those, 24 were diagnosed with Alzheimers disease. 
 Those who had just a little bit less REM sleep during the sleep test were more likely to later be in the dementia group, the team reported in the journal Neurology. 
 It wasnt a big difference. The people who later developed dementia spent an average of 17 percent of sleep time in REM sleep, compared to 20 percent of the volunteers who did not develop dementia. 
 Each percentage reduction in REM sleep percentage was associated a nonstatistically significant 6 percent increase in all-cause dementia risk, the team wrote. 
 Its not clear if the disordered sleep is a cause or an early effect of the dementia process. 
 Our findings point to REM sleep as a predictor of dementia, said Pase. The next step will be to determine why lower REM sleep predicts a greater risk of dementia. 
 There are several possibilities. Stress is one. 
 It is possible that reduced REM sleep may be a marker of increased responsiveness to stress, with heightened anxiety also implicated as a risk factor for dementia, the team wrote. 
 Related: Experimental Drugs Offer Hope, No Slam Dunk, for Alzheimer's 
 REM sleep may also buffer against synaptic loss and cognitive decline by assisting in the production of new connections and networks. 
 In general, having dementia also messes with sleep patterns. 
 Many people with Alzheimers experience changes in their sleep patterns, the Alzheimers Association says in a statement on its website. 
 Many older adults without dementia also notice changes in their sleep, but these disturbances occur more frequently and tend to be more severe in Alzheimers. There is evidence that sleep changes are more common in later stages of the disease, but some studies have also found them in early stages. 
 Last July, a team found that sleep disruption raised levels of a brain-clogging protein called amyloid. The team, from Washington University in St. Louis, said they believe that interrupting sleep may allow too much of the compound, amyloid and tau, to build up and that sleep might help the body clear them away. 
