__HTTP_STATUS_CODE
200
__TITLE
 A Single Mutation May Have Turned Zika Into a Killer
__AUTHORS
Maggie Fox
__TIMESTAMP
Sep 28 2017, 5:05 pm ET
__ARTICLE
 A single genetic mutation may have turned Zika from a boring and harmless virus into the brain-destroying guided missile that causes microcephaly and other severe birth defects, Chinese researchers said Thursday. 
 Their findings, if they hold up, could help explain why Zika seemed to suddenly appear and tear across South and Central America, causing an epidemic of miscarriages and birth defects. 
 Or it might not. 
 But the intriguing study demonstrates that a single spelling error made as genetic material replicates itself could account for a profound change. 
 Zika was once a little-known virus, of interest to almost no one, because it didnt cause any special symptoms in people. It was first discovered in the Ziika forest of Uganda in 1947. 
 But then in 2015, the mosquito-borne virus was linked to an unusual increase in cases of microcephaly, a devastating birth defect in which the brain does not develop properly resulting in a smaller than normal head. 
 Related: Zika Raises Birth Defect Rate 20 Times 
 Its effects on babies are now indisputable as the mosquito-borne virus has spread across the Americas, causing not just microcephaly but other birth defects and also miscarriages. 
 What hasnt been explained is how and why it did that. Were the birth defects just never noticed before, or did the virus somehow change? 
 Ling Yuan of the Chinese Academy of Sciences, Cheng-Feng Qin of the Beijing Institute of Microbiology and Epidemiology and colleagues set up an experiment to find out if a genetic mutation may have been responsible. They compared three currently circulating strains of Zika to a sample taken from 2010 in Cambodia, when an outbreak appeared relatively harmless. 
 They tested mouse cells, living mice and immature human brain cells in the lab. 
 Related: Gene Study Shows Zika Was Spreading Quietly for Years 
 All three current strains killed all newborn mice, while the 2010 strain only killed 16 percent of them, they reported in the journal Science. 
 They did genetic comparisons and found many differences  not an unusual finding, as viruses mutate constantly. One particular mutation, called S139N, caught their attention. 
 They genetically engineered Zika viruses to carry this mutation, and found that they killed human and mouse brain cells far more efficiently than strains not carrying the mutation. 
 These results show that contemporary strains of Zika virus are more neurovirulent (damaging to nerve cells) in mice than their ancestral strain, they wrote. 
 Our findings offer an explanation for the unexpected causal link of Zika virus to microcephaly, and will help understand how Zika virus evolved from an innocuous mosquito-borne virus into a congenital pathogen with global impact. 
 Its not necessarily proof that a mutation caused the virus to either become more infectious, more dangerous or both. More testing, including on people, will have to be done to show that. 
 But its a place to start looking. 
 There were many discussions back when things were at their height about why we were just now seeing this microcephaly, said Dr. Anthony Fauci, director of the National Institute of Allergy and Infectious Diseases. Maybe only when there were hundreds of thousands and thousands of thousands cases in Brazil did you begin seeing cases of microcephaly. 
 Finding such a specific mutation may suggest the virus did change as it infected more people, Fauci said. 
 I think it just explains what was somewhat of a puzzle, he said. 
 Michael Worobey, an expert on the evolution of viruses at the University of Arizona, isnt so sure the study explains what happened in real life. 
 My feeling is its more likely to be a red herring, said Worobey, who was not involved in the research. 
 There is a leap between experimental mice and what we see in the field. 
 Worobey said the Chinese researchers mistakenly call the Cambodian strain they tested an ancestor of the currently circulating strains, but he said it is not. Its possible the 2010 strain itself is an unusually mild mutant, he said. 
 I wouldnt bet on this being the crucial factor, said Worobey. I would love to lose that bet. 
 It will be important to test other strains from before 2010 to see what mutations they carry, he said, and to test more currently circulating strains. 
 But the study offers a place to look, he said. 
 It is useful to know that there is a single mutation that is a contender for being part of the story, Worobey said. 
 Its a trail of breadcrumbs like Hansel and Gretel that allow you to work back even if you dont have access to what happened in the past. If you have got enough samples and they differ enough, its like a trail of bread crumbs that can give you crucial information about when where and how these things moved and circulated. 
 The information is useful for tracing the epidemic of Zika, but it wont change the search for a vaccine or for drugs to treat it, said Fauci. 
 It certainly is not going to change anything I am going to be doing with a vaccine or a therapeutic, Fauci said. 
 "This has nothing to do with the immunogenicity of the virus. 
 Related: Zika-Affected Babies Can Cry 24 Hours a Day 
 NIAID is working to develop several Zika vaccines, as well as treatments that might protect a fetus if a pregnant woman becomes infected. 
 The Centers for Disease Control and Prevention reported in April that 10 percent of women with Zika-affected pregnancies in the 50 U.S. states ended up having babies with birth defects. 
 The virus does not cause symptoms in most people it infects and most people who do have symptoms suffer little more than a rash, and perhaps headache, fever and general achiness. 
 But its attraction to the developing brain and other nerve cells make it a disaster for developing fetuses. 
