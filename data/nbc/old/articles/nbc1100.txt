__HTTP_STATUS_CODE
200
__TITLE
FDA Panel Advises Approval of Gene Therapy for Blinding Disease
__AUTHORS
Maggie Fox
__TIMESTAMP
Oct 13 2017, 11:18 am ET
__ARTICLE
 Misty Lovelace of Alexandria, Kentucky, had never clearly seen her mothers face. Shed never seen stars. 
 But all that changed when the 18-year-old got an experimental gene therapy treatment that not only stopped her from going blind, but gave her back much of the vision she'd lost to an incurable genetic disease. 
 I never knew they were real dots that twinkled, Lovelace told a panel of federal drug advisers Thursday. 
 However, I will say that rainbows are overrated by far. 
 Lovelace and others came to the Food and Drug Administration headquarters on Thursday to ask that it be approved so more kids can see what theyve seen. 
 After hearing their testimony, and hours of detail about the treatment and its effects, they voted yes unanimously. 
 Spark Therapeutics Luxturna, known generically as voretigene, would be the first gene therapy treatment sold in the U.S. to correct an inherited defect, if the FDA follows the panels advice. 
 Related: Gene Therapy Reverses Blinding Eye Disease 
 The FDA will decide by January. 
 Eye doctors, parents and patients were unanimous in their support of approving the treatment, which would be aimed at the one in a million children with a specific genetic defect that causes Leber congenital amaurosis, or LCA for short. 
 The treatment uses a genetically engineered virus to correct the defect, for patients who have inherited two faulty copes of a gene called RPE65, which codes for an enzyme that nourishes cells in the retina. 
 In people with faulty versions, the cells eventually die. 
 Almost all will proceed to blindness. Other than voretigene, there are no avail treatments that can slow or stop the insidious loss of vision in these patients, Dr. Albert Maguire of Childrens Hospital of Philadelphia, where the treatment was developed, told the FDA panel hearing. 
 By the time they are in their 40s, its lights out, Dr. Jean Bennett of Childrens Hospital and the University of Pennsylvania, who helped develop the treatment, told NBC News. 
 Its a fairly obvious approach to try to correct that faulty gene, but gene therapy has turned out to be far more difficult than experts thought it would be. The corrected genes often dont stay where they are supposed to, and getting them to the right place is difficult. Patients often have severe side-effects such as cancer, and can even die. 
 Related: FDA Advisers Say yes to Experimental Cancer Gene Therapy 
 And the first gene therapy approved in Europe was pulled from the market when almost no one wanted it. 
 But the eye is a good place to try it, as its a self-contained organ and doctors and patients alike can literally see the results. Other teams are trying gene therapy in the eye, as well as approaches such as infusing stem cells to regenerate dying tissue. 
 With Luxturna, the approach is fairly straightforward, experts told the FDA panel. An eye surgeon makes a small incision in the eye and the treatment is infused onto the retina. 
 The hope is that it will take a single treatment to stop the degeneration. But several of the 31 patients tested in the trials of the therapy said they saw unexpected and dramatic improvement in their vision very quickly. 
 What I saw in the clinic was remarkable, Maguire said. 
 Most patients became sure of themselves, pushed aside their guides and navigated their environments independently and with confidence, Maguire said. In all honesty, if either myself or my child had this condition, I would not hesitate for a moment to get this treatment. 
 Christian Guardino said he struggled to get along in school, squinting at people who greeted him in the hallway. I couldnt see if somebody was smiling at me or frowning, Guardino, now 17, told the hearing. That never went over very well. 
 Related: Scientists Harness Stem cells for Grow-Your-Own Treatment 
 But the treatment transformed his life. 
 Gene therapy has made my world literally so much brighter. I have been able to see things I never saw before, like stars, snow falling, fireworks and most important, the moon, the Patchogue, Long Island, high school student said. 
 I am now able to go to the movies  and now my social life is so much better. I can now see peoples facial expressions. I can see all you people right now. 
 Its not unheard of for FDA advisory committees to get emotional testimony from patients, often flown in at the expense of the company seeking approval. 
 Last year, advocates packed out an FDA advisory committee meeting about a drug designed to treat a small percentage of patients with Duchenne muscular dystrophy. 
 Despite considerable controversy over whether the drug actually helped kids who got it, the FDA approved the drug. And the year before, the FDA approved a pill to treat female sexual dysfunction against the advice of its advisers unswayed by emotional pleas from patients. 
 But Thursdays hearing was unusual in that even ophthalmologists who had nothing to do with the trial pleaded. 
 It won't help everyone with vision loss -- just the few thousand who have the particular defect targeted by the drug. 
 Children should be treated as early as possible, said Dr. Bart Leroy, an expert in genetic ophthalmology at Childrens in Philadelphia. 
 Ideally, all patients should be treated before they reach complete retinal degeneration, Leroy told the committee. That could eventually mean at birth or even before birth, while in the mothers womb. 
 Once a patient has lost too many cells in the retina, even the gene therapy treatment likely will not help, doctors at the hearing agreed. 
 Voretigene is essential to keep our patients from going blind, Leroy said. I believe we have a chance here today to make history. 
