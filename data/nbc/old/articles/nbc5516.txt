__HTTP_STATUS_CODE
200
__TITLE
Tech Gift Guide: What to Get the Person Whos Hardest to Shop For
__AUTHORS
Andrea Smith
__TIMESTAMP
Dec 19 2016, 3:19 pm ET
__ARTICLE
 Theres one on every list, that hard-to-shop-for person who seems to have just about everything. But dont stress about it! Whether that special someone on your list is a gadget lover or a world traveler, or that cousin you havent seen in ages, weve rounded up a selection of the latest gift ideas that are both thoughtful and useful. 
 For Style-Conscious Techies 
 Knomos backpacks (about $349) are stylish and comfortable, and come with enough room and compartments to hold all the tech gadgets a stylish fashionista carries, including up to a 14 laptop. 
 The Knomo Beaux is made of soft, full-grain leather with adjustable leather shoulder straps. Of course, all that fashion also needs function, so buy your favorite techie some accessories from the Knomo ecosystem to go in that gorgeous bag. Throw in a high-capacity portable battery to power up her smartphone or a World Travel Adapter with an added USB port so she can charge two devices at once, no matter what country. The flat Apple Lightning to USB cable fits perfectly alongside an iPhone to safely charge it while tucked away in one of the bags pockets. 
 For the Forgetful Friend 
 Do you have that one friend whos always misplacing his keys or her wallet? Get a Tile Mate (about $25), a Bluetooth tracker that goes on a keychain, slips in a wallet or a backpack; heck, you can even stick it to the TV remote. Using the accompanying app, your forgetful friend can locate the tile by making it ring or see its last known location on a map. And if the phone is what goes missing, pressing any Tile will make it ring, even if its in silent mode. Get a 4-pack: You'll receive four times the thanks. 
 For the Runner 
 Give the runner in your life Under Armour Sport Wireless Heart Rate headphones (about $200). A heart rate monitor built into the earbuds sends data to the UA Record app, so users can track their workouts; monitor speed, pace, and distance; and maximize the all-important heart-rate zone. 
 The headphones are made in partnership with JBL, so they deliver great-sounding music. 
 For the Fitness Buff 
 A wrist-worn activity tracker is a great gift idea for anyone who runs, works out, or just enjoys a daily walk. Most of todays trackers keep tabs on your daily steps, calories burned and how well you sleep, as well as send smart notifications like texts and calls from your connected smartphone to your wrist. They also all remind you to move when youve been inactive. But each of the top devices offers something just a little different as well. 
 Fitbits Charge 2 (about $130) has all-day activity tracking for keeping tabs on steps and calories, heart rate and sleep. In addition to smartphone notifications right on your wrist, it offers guided breathing sessions for those times when you just need to give yourself a little timeout. 
 Garmin VivoSmart HR+ (about $150)is a smart activity tracker with 24/7 wrist-based heart rate monitoring plus a built-in GPS that maps your walk or run to calculate your speed and distance. It connects to the Garmin App so users can join fitness challenges and get guided coaching. 
 The TomTom Touch (about $130) tracks your steps and calories burned much like the others, but its the first activity tracker to measure body composition  the ratio of body fat to muscle mass  so users can get a better idea of their overall fitness level and whether all that working out is doing any good. It also has an optical heart rate sensor for all-day heart rate tracking. 
 For the Neat Freak 
 Nobody likes to touch the kitchen soap when their hands are covered with raw chicken or other yucky things. For those moments, sensors come to the rescue. Simplehumans Foam Cartridge Sensor Pump (about $50) dispenses hand soap without you ever having to touch a thing. Just wave your hand under the stainless-steel dispenser, which is rechargeable and can be used in the kitchen or bathroom. 
 For the College Student 
 If safety is a concern when kids are away at school or come home from work late at night, get them peace of mind with a Wearsafe tag (monthly fees apply). Its a small wearable safety device that clips onto a pocket or attaches to a key ring and connects via Bluetooth to a smartphone. 
 Users can discreetly press the button if they feel unsafe. The app sends an alert to a pre-determined group of friends or family who get the GPS location and live audio from the scene. 
 For the Super Nerd 
 For that guy who carries a tablet, a laptop, a phone, headphones and other tech devices everywhere he goes, the SCOTTeVEST OTG (Off The Grid) jacket (about $250) will ensure he gets through the day without losing any of them. The puffer-style jacket features 29 pockets for stowing all that tech, an RFID-blocking pocket, and it can even hold a full size laptop. This is a perfect gift for someone who travels, making it super easy to get through security  as long as they can remember what pocket the boarding pass is in. 
 For the Memory Maker 
 We all know someone with thousands of photos stored in shoe boxes waiting to be digitized and organized. That person doesnt need a dumpster, they need Epsons FastFoto FF-640 High-Speed Photo Scanning System (about $650). Its a high-speed scanner that automatically scans both sides of an image, so you can preserve those handwritten notes from grandma describing whos in the picture and where it was taken. The scanner boasts speeds of one photo per second, and the software even helps preserve those precious memories by restoring color to faded photos. 
 For the Teen 
 Because 2016 was like, so awesome, and teens take tons of digital pictures, give a gift of great memories to the generation that never prints. Chatbooks Highlight Books (from $8) pull the 30 most-liked Instagram photos from a feed and turn them into a 6 in x 6 in photo album delivered to your door. 
 For the DIY Guy 
 Sometimes the smartest gadget in your home is the one with a working battery. The Ryobi ONE+ 18-Volt Lithium-Ion Compact Drill/Driver Kit is an essential DIY/homeowner tool. It comes with the Ryobi One+ 18-volt battery that packs more punch, recharges faster and weighs 30 percent less than older 18-volt batteries. It also features one-hand chucking, a built-in light, and a magnetic tray to hold screws while you work. That same battery works with flashlights like the Area Light or Workbench Light, as well as more than 70 tools that all use the 18V ONE+ battery system. Isnt it nice when all the gadgets get along? 
 For the Road Warrior 
 For the road warrior on your list, give the gift of a warmed-up car. When its cold outside, the Viper SmartStart system lets you start the car from the warmth and comfort of your kitchen or find it in a crowded parking lot. It's an after-market device that gets installed in your vehicle giving you one-touch smartphone access to commands like remote start, lock and unlock. 
