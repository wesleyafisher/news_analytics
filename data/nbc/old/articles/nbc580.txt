__HTTP_STATUS_CODE
200
__TITLE
ISIS Assault on Iraqs Kirkuk Ends After 24-Hour Battle
__AUTHORS
The Associated Press
__TIMESTAMP
Oct 22 2016, 12:43 pm ET
__ARTICLE
 KIRKUK, Iraq  A massive ISIS assault on targets in and around the northern Iraqi city of Kirkuk came to an end Saturday after a day and night of heavy clashes, as Iraqi forces launched a new advance southeast of the ISIS-held city of Mosul. 
 At least 80 people were killed and another 170 were wounded in the operation, said Brig. Gen. Khattab Omer of the Kirkuk police. 
 Related: ISIS Fighters Claim Attack on Kirkuk, Iraq 
 All of the attackers were killed or blew themselves up, added Kirkuk police Brig. Gen. Khattab Omer. The area around the provincial headquarters, where the fighting was heaviest, was quiet Saturday morning. 
 More than 50 militants took part in the assault, which appeared to be aimed at diverting attention from Mosul, around 100 miles away, where Iraqi forces are waging a major offensive. 
 The militants killed 13 workers, including four Iranians, at a power plant north of Kirkuk, and a local TV reporter was killed by a sniper in the city. It was not clear if there were other casualties among civilians or the Kurdish security forces who control Kirkuk. 
 The Iraqi army's 9th Division meanwhile launched a new push to retake the town of Hamdaniyah, around 12 miles to the southeast of Mosul. 
 Related: ISIS Attack in Kirkuk Is Reality Check Amid Mosul Offensive 
 The Joint Military Operation Command said troops were advancing on the town, also known as Bakhdida and Qaraqosh. 
 Two army officers told The Associated Press that forces were advancing on the town from the north and south, with the support of U.S.-led coalition airstrikes. They spoke on condition of anonymity because they were not authorized to brief reporters. 
 The operation is part of an offensive launched Monday aimed at liberating Mosul, Iraq's second largest city, which fell to ISIS in 2014. It is the largest operation undertaken by Iraqi forces since the 2003 U.S.-led invasion and is expected to take weeks, if not months. 
 Hamdaniyah is believed to be largely uninhabited. ISIS has heavily mined the approaches to Mosul, and Iraqi forces have had to contend with roadside bombs, snipers and suicide truck bombs as they have moved closer to the city. 
 Iraqi forces retook the town of Bartella, around nine miles east of Mosul, earlier this week, but are still facing pockets of resistance in the area. 
