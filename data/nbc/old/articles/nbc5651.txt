__HTTP_STATUS_CODE
200
__TITLE
Raising the Minimum Wage
__AUTHORS
Stephanie Ruhle
__TIMESTAMP
Jul 18 2017, 9:44 am ET
__ARTICLE
 Paying America's lowest-paid workers would definitely help them make ends meet. But, some warn that raising the minimum wage might be a job killer. 
 
