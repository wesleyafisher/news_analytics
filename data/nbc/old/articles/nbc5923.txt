__HTTP_STATUS_CODE
200
__TITLE
Iraqi Forces Fighting ISIS Near Mosul Uncover Large Network of Tunnels
__AUTHORS
NBC News
__TIMESTAMP
Oct 28 2016, 7:37 am ET
__ARTICLE
 Iraqi counterterrorism forces have found the most extensive network of ISIS tunnels so far in the campaign to retake the extremists' stronghold of Mosul, officials said. 
 The tunnels discovered Thursday stretched for almost two miles and served as stealthy links between villages to the east of the city, Iraqi military spokesman Yahya Rasool told NBC News. 
 The warren-like network featured electricity, air-conditioning and "all the necessary equipment that ISIS needs to stay inside," Rasool said. 
 The counterterrorism forces  who are advancing on Mosul from the east  also found "one of the largest factories" used by ISIS to make suicide car bombs, according to the spokesman. 
 "They are sure that once they advance they will find more tunnels that lead to the center of Mosul," Iraqi counterterrorism spokesman Sabah Al-Nuaman said of his forces' advance. 
 Related: ISIS Moves Civilians to Mosul as Human Shields: Reports 
 Meanwhile, some of the thousands of Iraqis liberated from ISIS' control around the city have been reunited with their families at camps. 
 "It's been almost three years ... that I haven't seen her," Mohammed Omar Mustafa, a Kurdish man from Mosul, told The Associated Press after being reunited with his sister. 
 He said he was in Iran when the extremists swept the region in the summer of 2014. 
 "When I came back my village had been taken and there was no way to go back," he told the AP. His sister's family "couldn't make it. But I escaped at the last minute." 
 Another Iraqi, Abdul-Salam Zeinal Hamid, described life under ISIS in his village east of Mosul. 
 "They tortured us in a way that had never happened before," he told the AP. "And in this two and a half years there was nothing. No work, no money. Whatever we had, we ate it. We just tried to stay alive." 
 
