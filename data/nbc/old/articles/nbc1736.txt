__HTTP_STATUS_CODE
200
__TITLE
Hurricane Irma Leaves Path of Destruction in Caribbean
__AUTHORS
Phil McCausland
Associated Press
__TIMESTAMP
Sep 8 2017, 10:17 pm ET
__ARTICLE
 Rescues continued in parts of the Caribbean on Friday, after powerful Hurricane Irma swept through the region and left devastation in its wake on the way to Florida. 
 The hurricane tore off roofs, damaged buildings and brought fearsome storm surges, killing at least 23 people. The storm lashed the Turks & Caicos and brushed the northern coast of Cuba on Friday. And another storm, Hurricane Jose, was approaching the Leeward Islands. 
 "If you know Barbuda before and what you saw, it's completely destroyed," Michael Joseph, president of the Red Cross in Antigua and Barbuda who was in Barbuda Thursday, said on MSNBC Friday. 
 An estimated 90 percent of the buildings on Barbuda, population around 1,400, were destroyed when Irma struck. Aided by fishing boats and other private vessels, the government was evacuating residents to its sister island of Antigua ahead of Hurricane Jose. 
 Irma slammed into the easternmost islands of the Caribbean on Wednesday, before moving on and lashing Haiti and the Dominican Republic, and bringing hurricane-force winds to the Turks & Caicos Islands. The storm also cut power to more than 1 million people in Puerto Rico, which, unlike other islands, was spared a direct hit. 
 "All these islands are in dire need of help," U.S. Virgin Islands resident Tenesha Keyes said. "We need food. We need water. We need clothes." 
 Friday evening, the U.S. Coast Guard continued to make it clear that the ports in St. Thomas and St. John, U.S. Virgin Islands remained unsafe after Irma made landfall there, and added that those who hoped to provide aid to the battered islands by sea should steer clear. 
 "We understand the desire to assist people impacted in St. Thomas and St. John, U.S. Virgin Islands," Capt. Eric King, U.S. Coast Guard captain of Port San Juan, in a statement. "At this time port conditions in these areas remain unsafe for vessel traffic, and people who choose to disregard port condition warnings could make a bad situation worse." 
 The U.S. Coast guard noted that it had cleared many of the ports in Puerto Rico and that there were no active maritime search and rescue cases associated with Hurricane Irma in the immediate area. 
 French, British and Dutch militaries brought much-needed aid to a string of Caribbean islands where thousands were left homeless and without food and water, the Associated Press reported. 
 At least four people were killed in the U.S. Virgin Islands, the government said. Three people were dead in Puerto Rico and deaths were also reported in Barbuda, St. Martin and French territories, and St. Maarten. Four people died in the British Virgin Islands and 1 person died in Anguilla, the Caribbean Disaster Emergency Management Agency said. 
 The hospital in St. Thomas was destroyed, and the patients were evacuated by the U.S. Coast Guard to St. Croix and Puerto Rico. 
 "Take it seriously, because this is the real deal," Maj. Jeremy DeHart, a U.S. Air Force Reserve weather officer who flew through the eye of Irma at 10,000 feet, told the Associated Press. 
 William Marlin, prime minister of the Dutch side of St. Martin, said recovery was expected to take months even before Jose threatened to make things worse. 
 "We've lost many, many homes. Schools have been destroyed," he said. "We foresee a loss of the tourist season because of the damage that was done to hotel properties, the negative publicity that one would have that it's better to go somewhere else because it's destroyed. So that will have a serious impact on our economy." 
 Hurricane Jose was a powerful Category 4, almost a Category 5, storm that had 155-mph winds as of 11 p.m. ET Friday. The storm was around 265 miles southeast of the northern Leeward Islands, according to the National Hurricane Center, and was forecast to be near the northern Leeward Islands on Saturday. 
 Hurricane warnings were in place for Barbuda and Anguilla, St. Maarten, and the French territories of St. Martin and St. Barts. Forecasters warned that dangerous storm surges and large and destructive waves were possible. 
