__HTTP_STATUS_CODE
200
__TITLE
New Hepatitis C Infections Triple, Driven By Drug Use, CDC Says
__AUTHORS
Maggie Fox
__TIMESTAMP
May 12 2017, 2:29 pm ET
__ARTICLE
 New cases of hepatitis C have nearly tripled in the past five years, driven mostly by people sharing needles to inject drugs, federal health officials said Thursday. 
 Since the liver-destroying infection doesnt usually cause symptoms until its too late, it is important to make sure people get tested  and to fight the practices that get people infected in the first place, the Centers for Disease Control and Prevention said in a new report. 
 That includes needle exchange programs frowned on by many conservative lawmakers. 
 Reported cases of acute HCV infection increased more than 2.9-fold from 2010 through 2015, rising annually throughout this period, the report reads. 
 The rate of new infections is rising fastest among younger adults, the CDC said. 
 Several early investigations of newly acquired hepatitis C infections reveal that most occur among young, white persons who live in non-urban areas (particularly in states within the Appalachian, Midwestern, and New England regions of the country), the report reads. 
 Related: Infections May be Masking Opioid Deaths 
 This is primarily a result of increasing injection drug use associated with Americas growing opioid epidemic, the CDC added. 
 Rates have nearly doubled among pregnant women, the CDC said. Pregnant women can pass the virus to their babies. 
 Hepatitis C is one of the leading infectious disease killers. Nearly 20,000 Americans died from hepatitis C in 2015, the CDC said. About 3.5 million people, mostly over 55, are infected. 
 Related: CDC Says Needle Exchanges Move Too Slowly 
 Drugs can cure hepatitis C, but they are expensive. Gilead's Sovaldi costs about $84,000 for a weeks-long regimen. The second-generation version, called Harvoni, costs more than $94,000 for similar treatment. 
 While new medicines can now cure hepatitis C virus infections in as little as two to three months, many people in need of treatment are still not able to get it, the CDC added. 
 By testing, curing, and preventing hepatitis C, we can protect generations of Americans from needless suffering and death, said the CDCs Dr. Jonathan Mermin. 
 Related: Fla. Governor Declares Opioid Emergency 
 And the CDC advocates using needle exchange programs to prevent infection. Sharing needles can infect people not only with hepatitis, but with the human immunodeficiency virus (HIV) that causes AIDS. 
 Comprehensive syringe service programs are one of many tools that communities can use to prevent hepatitis and other injection-related infectious diseases, the CDC said. 
 "These programs also help link people to treatments to stop drug use, testing for infectious diseases that can be spread to others, and medical care." 
 One recent CDC study found that only three states  Massachusetts, New Mexico, and Washington  have laws to help people get into comprehensive needle exchange programs and provide hepatitis treatments and preventive services for people who inject drugs. 
 The CDC found 18 states still made needle exchanges illegal and had laws making it a crime to have or distribute syringes. 
