__HTTP_STATUS_CODE
200
__TITLE
Deep Springs, One of the Last All-Male Colleges, Goes Co-Ed
__AUTHORS
Susan Donaldson James
__TIMESTAMP
Aug 30 2017, 12:50 pm ET
__ARTICLE
 Deep Springs College, which has remained all-male for a century, will accept women after a six-year legal battle. 
 The two-year college receives 180 to 250 applications a year for about a dozen spots in the 30-student body. Each receives a full scholarship valued at $50,000. The three pillars of its curriculum are academics, labor and self-governance. 
 Isolated on a cattle ranch and alfalfa farm in the high desert of California, just north of Death Valley, Deep Springs was founded in 1917 by Lucien Lucius Nunn, an electricity magnate. He had a clear idea of how young men should be educated: to serve humanity. 
 Students are expected to spend 20 hours a week tending the farm, raising chickens and milking cows. They also cook, clean and maintain community vehicles and have a strong voice in the overall running of the college, which operates year round. 
 Only three non-religious, private, four-year mens colleges remain in the country  Hampden-Sydney College in Virginia, Morehouse College in Atlanta and Wabash College in Indiana. 
 In 1994, the board of directors opposed a move to be co-educational because it conflicted with the mission language in Nunns trust to educate promising young men. A majority favored the move in 2011, but two held out. 
 Now the California Supreme Court has refused to hear their appeal of a lower court ruling, opening the way for female applicants in July 2018. 
 NBC News spoke with President David Neidorf, who previously served as director of the integrated studies program at Middlebury College and spent two decades working off-season at Outward Bound and other wilderness programs. 
 Q: How will co-education change the school? 
 A: Ive rarely met a college teacher who doesnt think co-ed classes are better academically. To the extent that we are training people for practical service to humanity through a wide range of leadership positions, there needs to be co-education. This is how we lead our lives and do work. Friendship crosses gender lines. 
 Q: Will Deep Springs be bigger? 
 A: This is an isolated school. We have only 30 students and we do not intend to change. That number is the maximum we think we ever want to be. We want students to feel they are part of community. 
 Q: Why is Deep Springs so special? 
 A: Students want a life of some practical significance to a community and thats not easy to do in a typical American college. Its pretty rigorous liberal arts. But we also teach auto repair, agriculture and nonprofit management. We are asking them to do labor for character-building and we think this kind of practical problem-solving carries over to all walks of life. And they get instant feedback on the quality of their work. 
 Q: How are students admitted? 
 A: They are selected for the folks they want to be  change agents in society. The college was formed 100 years ago by a guy who wanted to oppose what he saw as a flood of students going into money-making. He wanted to train them for humanity at large. We have very idealistic students, and because they have so much power in the institution, their idealism is practical. 
 Q: Where do students go after two years? 
 A: They transfer to complete their BA. Four or five of them transfer to the Ivies, Stanford and University of Chicago. Its a pretty worn path. Had they not come here, they would have gone to those schools. 
 Q: So Deep Springs is academically competitive? 
 A: Very competitive. But its not really competition because this is so different from the other schools. We are so much more demanding than the Ivies with a commitment to community work and self-governance. The students who come here are attracted to this. 
 Q: What do the students do for fun? 
 A: Its a very intense place. We dont have a party scene. A lot of students get involved in cooking. There are people of all ages, lots of musicians. Students can download movies. 
 Q: You mean you are wired up? 
 A: Not really. The Internet is too slow and its not much fun streaming anything. They can do basic research or go over a radio link, read email and the newspaper. But nobody lives a life online. There is no cell phone service. Eight students make up our party of governance and they control and limit access to the Internet. There is one room, not in the dorm, but in the building where we have offices and classrooms that has Internet, but nowhere else. 
 Q: Do kids ever drop out? 
 A: Sure. Actually we average one or two a year. The student body evaluates whether they can be readmitted. And students can ask students to leave if they feel someone is disruptive in any way. Sure, there have been discipline problems, but only if we really, absolutely couldnt deal with it. 
