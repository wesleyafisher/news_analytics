__HTTP_STATUS_CODE
200
__TITLE
Needle-Free Flu Vaccine Patch Works as Well as a Shot
__AUTHORS
Maggie Fox
__TIMESTAMP
Jun 29 2017, 1:09 pm ET
__ARTICLE
 A press-on patch that delivers flu vaccine painlessly worked as well as an old-fashioned flu shot with no serious side effects, researchers reported Tuesday. 
 People who tried out the patch said it was not difficult or painful to use, and tests of their blood suggested the vaccine it delivers created about the same immune response as a regular flu shot, the team reported in the Lancet medical journal. 
 The hope is the vaccine will be cheaper, easier to give and more acceptable than a regular flu vaccine. 
 It was really simple. Its kind of like a band-aid almost, said Daisy Bourassa, a college instructor who tested the new vaccine for the study. 
 Its not like a shot at all. If I had to describe it is maybe like pressing down on the hard side of Velcro. It is like a bunch of little teeny tiny stick things that you can feel but its not painful. 
 Related: Caterpillar Vaccine More Effective Than Regular Flu Vaccine 
 The team at Georgia Tech, and a spin-off company called Micron Biomedical, have been working on the patch vaccine for years. This was the first test using real flu vaccine, and the results show it caused immune responses very similar to those elicited by vaccine administered by syringe. 
 There were no treatment-related serious adverse events, Dr. Nadine Rouphael of the Emory University School of Medicine and colleagues wrote in their report. 
 It was a phase 1 trial, meant mostly to show safety in just 100 volunteers. Thats not enough to show whether the vaccine actually prevented any cases of influenza. That will take a larger trial to demonstrate. 
 The results were great, Rouphael told NBC News. We were pleased to see that the immune response was excellent. Rouphaels team were the experts in vaccinating, and were recruited by the Georgia Tech team to actually test the experimental patch. 
 Related: Pediatricians Give Thumbs-Down to FluMist 
 The tiny needle-like points on the patch are made out of the vaccine itself. When pressed into the skin, the needles dissolve, delivering the dried vaccine into the outer layer of the skin. This layer is loaded with immune system cells that are the first line of defense against invaders such as bacteria and viruses. 
 These cells take up the vaccine and use it to prime themselves against a flu infection. 
Cool small patch of microneedles for flu vaccine could be mailed to your home & may transform how we vaccinate #NIH https://t.co/2nODijQQMh
 The trial showed that people could use the patch without any help and liked it. It also showed they had an immune response to the patch, and did not have any serious side-effects from using it. 
 If it continues to work well in tests, Rouphael said it might be possible to just let people buy the vaccine patches and take them home to use. 
 Theyll be much easier to ship around the country and the world than current vaccines, which must be carefully refrigerated. 
 Bourassa is a fan of the idea. 
 I think it would be fantastic if this was something you could get and administer it yourself at home, she said. 
 The reason why many years I dont get a shot is that I dont have time to wait in a line or whatever. It would be really awesome if I could order it and it would be delivered like Amazon Prime. 
 Biomolecular engineering professor Mark Prausnitz of Georgia Tech, who leads the team developing the patch vaccines, said the vaccine stayed stable for as long as a year at temperatures up to 100 degrees F. Conditions like that would completely spoil a regular flu vaccine. 
 It is also really neat how you can keep it at room temperature, Rouphael said. It really simplifies the way we do vaccines. This could be a game-changer. 
 Prausnitz and colleagues are trying other vaccines in their device, including measles, rubella and polio vaccines. He says he has even tried experimental Ebola and Zika viruses in the delivery patch. 
 We havent yet met a vaccine we havent been able to incorporate into a microneedle device, Prausnitz said. Another possibility for the microneedle delivery system are drugs that must be injected, as well as insulin, he said. 
 Rouphael said she had no trouble recruiting volunteers for the trial. They were divided into groups and some got the patch and some got injected vaccines. 
 Related: Flu Shots Are Still a Hard Sell 
 They got so disappointed if they got the regular influenza vaccine. You could see it on their faces, she said. 
 They were into being part of the new vaccine gadget. 
 More testing will be needed but Dr. Katja Hschler and Dr. Maria Zambon from Public Health England, who were not involved in the trial, said the prospect of a cheaper, easy-to-ship flu vaccine was exciting. 
 Microneedle patches have the potential to become ideal candidates for vaccination programs, not only in poorly resourced settings, but also for individuals who currently prefer not to get vaccinated, potentially even being an attractive vaccine for the pediatric population,  they wrote in a commentary in the Lancet. 
 Flu is a major killer. The Centers for Disease Control and Prevention estimates that between 140,000 and 700,000 people get sick enough from flu every year to need hospital care, and infection kills anywhere from 12,000 to 56,000 people a year, depending on how bad the flu season is. 
 Flu killed 101 children this past season, the CDC said. 
 Only about 40 percent of Americans get vaccinated for influenza in an average year, even though the CDC says just about everyone over the age of 6 months needs to get a flu vaccine every year. 
