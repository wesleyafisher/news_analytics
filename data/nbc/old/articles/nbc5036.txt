__HTTP_STATUS_CODE
200
__TITLE
Duterte Declares Martial Law Over Chaos in Marawi, Philippines
__AUTHORS
__TIMESTAMP
May 24 2017, 1:06 pm ET
__ARTICLE
 ILIGAN CITY, Philippines  ISIS-linked militants swept through a southern Philippine city, beheading a police chief, burning buildings, seizing a Catholic priest and his worshipers and raising the black flag of the group, authorities said Wednesday. President Rodrigo Duterte, who had declared martial law across the southern third of the nation, warned he may expand it nationwide. 
 At least 21 people have died in the fighting, officials said. 
 As details of the attack in Marawi city emerged, fears mounted that the largest Roman Catholic nation in Asia could be falling into a growing list of countries grappling with the spread of influence from ISIS in Syria and Iraq. 
 The violence erupted Tuesday after the army raided the hideout of Isnilon Hapilon, a commander of the Abu Sayyaf militant group who has pledged allegiance to ISIS. He is on Washington's list of most-wanted terrorists with a $5 million reward for information leading to his capture. 
 Related: What You Need to Know About the Controversial Philippine President 
 The militants called for reinforcements and around 100 gunmen entered Marawi, a mostly Muslim city of 200,000 people on the southern island of Mindanao, Defense Secretary Delfin Lorenzana said. 
 "We are in a state of emergency," Duterte said Wednesday after he cut short a trip to Moscow and flew back to Manila. "I have a serious problem in Mindanao and the ISIS footprints are everywhere." 
 He declared martial rule for 60 days in the entire Mindanao region  home to 22 million people  and vowed to be "harsh." 
 "If I think that you should die, you will die," he said. "If you fight us, you will die. If there is open defiance, you will die. And if it means many people dying, so be it." 
 But he said he would not allow abuses and that law-abiding citizens had nothing to fear. 
 Related: Trump Praised Dutertes Drug Crackdown 
 Duterte said a local police chief was stopped at a militant checkpoint and beheaded, and added that he may declare martial law nationwide if he believes the group has taken a foothold. 
 Marawi Bishop Edwin de la Pena said the militants forced their way into the Marawi Cathedral and seized a Catholic priest, 10 worshipers and three church workers. 
 The priest, Father Chito, and the others had no role in the conflict, said Archbishop Socrates Villegas, president of the Catholic Bishops Conference of the Philippines. 
 "He was not a combatant. He was not bearing arms. He was a threat to none," Villegas said of Chito. "His capture and that of his companions violates every norm of civilized conflict." 
 Villegas said the gunmen are demanding the government recall its forces. 
 Military spokesman Col. Edgard Arevalo said 13 militants had been killed, and that five soldiers had died and 31 others were wounded. Other officials said a security guard and two policemen were also killed, including the beheaded police chief. 
 Arevalo said troops had cleared militants from a hospital, the city hall and Mindanao State University. About 120 civilians were rescued from the hospital, the military said. 
 Thousands of people have fled the city, said Mary Jo Henry, an emergency response official. She quoted another official as saying Marawi was like "a ghost town." 
 Broadcaster ABS-CBN showed people crammed inside and on top of public vehicles leaving the area, and some walking on foot with their belongings as they passed through a security checkpoint manned by soldiers. 
 Martial law allows Duterte to use the armed forces to carry out arrests, searches and detentions more rapidly. He has repeatedly threatened to place the south, the scene of decades-long Muslim separatist uprisings, under martial law. But human rights groups have expressed fears that martial law powers could further embolden Duterte, whom they have accused of allowing extrajudicial killings of thousands of people in his crackdown on illegal drugs. 
