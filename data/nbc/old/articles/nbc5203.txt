__HTTP_STATUS_CODE
200
__TITLE
Rocker Tom Pettys Career in Photos
__AUTHORS
NBC News
__TIMESTAMP
Oct 3 2017, 10:15 am ET
__ARTICLE
Tom Petty performs at Le Grand Rex on June 27, 2012 in Paris.
After being rushed Sunday to a hospital from his Malibu home in Los Angeles, unconscious and in cardiac arrest, Petty, 66, was declared dead on Oct. 2.
A Rock 'n' Roll Hall of Famer, Petty, with his band the Heartbreakers, married '60s-era folk rock with the Southern accents of his native Florida.
Tom Petty poses at the Hammersmith Odeon on May 15, 1977 in London.
Heartbreakers guitarist Mike Campbell and Tom Petty perform in 1977 in London.
Tom Petty performs in 1980.
Tom Petty signs autographs after his band Tom Petty and the Heartbreakers were honored with a star on the Hollywood Walk of Fame in 1999.
From left, Jackson Browne, Tom Petty, Billy Gibbons of ZZ Top and Keith Richards of the Rolling Stones perform at the annual Rock and Roll Hall of Fame induction ceremony in 2004.

Tom Petty and The Heartbreakers perform at the Bonnaroo Music and Arts Festival on June 16, 2006 in Manchester, Tenn.
From left, musicians Tom Petty, Jeff Lynne, Paul McCartney, Olivia Harrison and Dhani Harrison pose for photos at a ceremony for Beatles legend George Harrison who posthumously received a star on the Hollywood Walk of Fame on April 14, 2009 in Los Angeles.
Honoree Tom Petty performs at the MusiCares Person of the Year tribute at the Los Angeles Convention Center on Friday, Feb. 10, 2017.
Mike Campbell and Tom Petty perform during KAABOO 2017 at the Del Mar Racetrack and Fairgrounds on Sept. 17, 2017 in San Diego.
