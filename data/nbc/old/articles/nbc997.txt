__HTTP_STATUS_CODE
200
__TITLE
Southwest Airlines Apologizes After Video Shows Woman Being Dragged off Plane
__AUTHORS
Kalhan Rosenblatt
__TIMESTAMP
Sep 27 2017, 2:36 pm ET
__ARTICLE
 Southwest Airlines has apologized to a passenger who was recorded being pulled off a Los Angeles-bound flight after she told the crew she had a life-threatening pet allergy. 
 Taken on Tuesday evening, the video shows the unidentified woman repeatedly asking Transportation Authority officers, What are you guys doing? as they wrap their arms around her and begin to pull her down the aisle. 
 Related: Airplane Mode: An Odd and Unsettling New Era in Air Travel 
 The airline said that a pet and a service animal were on the flight, but the woman did not have the proper medical certificate to stay on board. 
 Our policy states that a customer (without a medical certificate) may be denied boarding if they report a life-threatening allergic reaction and cannot travel safely with an animal on board,"  a Southwest Airlines spokesman said in a statement. "Our flight crew made repeated attempts to explain the situation to the customer, however, she refused to deplane and law enforcement became involved. 
 Bill Dumas, who filmed the video, said the woman claimed she had a "deadly allergy" to dogs and asked that the two animals on board be removed. Dumas said the woman asked for a type of injection to alleviate her symptoms after the crew told her they could not remove the animals. 
 Dumas said he heard a flight attendant say they needed a certificate to administer an injection, which the woman didn't have. A pilot offered to let the woman exit the plane so an injection could be administered, but she refused, he said. 
 Once law enforcement boarded the flight, Dumas's video began. 
 In a short first video, two law enforcement officers can be seen tugging at the woman as they try to remove her from her seat at the rear of the plane. 
 As the second video begins, officers are seen moving the woman down the aisle. She can then be heard pleading with them to let her stay. 
 My dad has surgery tomorrow. Im sorry, my dad has a surgery. What are you doing? she says frantically, as she resists the officers. 
 Cmon, lady. Lets go, an officer responds. 
 Related: Watch the Video of United Passenger Being Dragged Off Flight 
 The cabin is filled with chatter as some passengers suggest the woman make the complaint off of the flight, while others tell her to walk as instructed to ensure her own safety. 
 The scuffle pauses briefly as the woman accuses the officers of ripping her pants. They tell her to fix her pants and leave the plane. 
 When it seems like the woman is hesitating, an officer wraps his arms around the womans chest and begins to forcefully drag her down the aisle. She begins shrieking: Dont touch me! Im walking! 
 A sense of anxiety can be heard in the voices of those around her. One woman can be heard saying, Show them that youre walking. 
 An officer yells, Then walk! 
 I cant walk. Hes got my leg, the woman replies. 
 Tempers seem to boil over as an officer shoves the woman toward the door. She turns and says: Im a professor. What are you doing? 
 As police continue to wrestle the woman down the aisle, a mans voice is heard saying: Geez, lady. Get off the plane. Make the complaint later. 
 One passenger told NBC News that the woman was also yelling that she was pregnant as police pulled her off the flight. 
 "Then I heard her say her father had surgery and then she said she was a professor and she needed to be in L.A.," the passenger, Julia Rockett, 20, said. "She was kind of yelling random things, and she knew people were recording." 
 Rockett said a flight attendant asked passengers not to record the confrontation, but "obviously, no one listened.". 
 The Maryland Transportation Authority Police said the request to remove the woman came from the plane's captain and they responded accordingly. 
 The woman was arrested and charged with disorderly conduct, failure to obey a reasonable and lawful order, disturbing the peace, obstructing and hindering a police officer, and resisting arrest. She was transported to the Anne County Court Commissioner before she was released on her own recognizance, police said. 
 Related: Airlines Make Billions in Bag and Change Fees, Setting a Record 
 NBC News was not immediately able to contact the woman for comment. 
 Rockett said the flight was delayed 45 minutes, and after the woman was removed the crew acted like nothing happened. She said the crew did apologize for the delay. 
 "We are disheartened by the way this situation unfolded and the customer's removal by local law enforcement officers," Southwest Airlines said in the statement. "We publicly offer our apologies to this customer for her experience, and we will be contacting her directly to address her concerns." 
 The Southwest Airlines incident is reminiscent of a similar controversy aboard United Airlines, in which a now-viral video showed Dr. David Dao being bloodied as he was dragged off a plane. 
