__HTTP_STATUS_CODE
200
__TITLE
Steve Bannon Declares Season of War Against GOP Establishment
__AUTHORS
Garrett Haake
Daniel Arkin
__TIMESTAMP
Oct 14 2017, 4:51 pm ET
__ARTICLE
 WASHINGTON  Steve Bannon, President Donald Trump's former chief strategist, has a blunt message for the Republican establishment: Get ready for battle. 
 In a fiery speech Saturday at the Values Voter Summit, an annual conference for social conservatives, Bannon lambasted Senate Majority Leader Mitch McConnell and declared a "season of war" on party elders. 
 "Nobody can run and hide on this one. These folks are coming for you," Bannon told the crowd, referring to populist insurgents who could challenge GOP incumbents in the 2018 midterm elections. 
 The stark comments represent the latest escalation in the intra-party spat between Trump and GOP veterans on Capitol Hill. 
 Bannon taunted McConnell, comparing him to Julius Caesar "before the Ides of March"  the date Caesar was assassinated. 
 "The donors are not happy. They've all left you," Bannon said to the cameras, as if speaking directly to the Kentucky lawmaker. "We've cut your oxygen off." 
 Related: Trumps Estrangement From the GOP  One Republican at a Time 
 Bannon also took aim at Sen. Bob Corker, R-Tenn., calling him a "real piece of work." Bannon falsely claimed that Corker, who recently feuded with Trump, was the first sitting senator to mock the commander in chief while U.S. troops are fighting abroad. 
 Bannon called on Republican senators to condemn Corker, vote against McConnell, and pledge to get rid of the filibuster. If not, he suggested, GOP incumbents will face primary challenges. 
 "There's a time and season for everything," Bannon said, "and right now it's a season of war against the GOP establishment." 
 Bannon left the White House in August and quickly returned to his executive chairman post at Breitbart News, the far-right media company. He has pledged to fight for Trump's agenda from outside the White House gates, pushing nationalist policies and cultivating grassroots candidates. 
 "It's not my war. This is our war, and you all didn't start it," Bannon told the crowd. "The establishment started it. 
 Garrett Haake reported from Washington. Daniel Arkin reported from New York. 
