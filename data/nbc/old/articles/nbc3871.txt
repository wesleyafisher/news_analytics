__HTTP_STATUS_CODE
200
__TITLE
She Was Told Internment Didnt Happen. Now, Her Familys Story Is in School Books.
__AUTHORS
Frances Kai-Hwa Wang
__TIMESTAMP
Oct 13 2017, 8:35 am ET
__ARTICLE
 When artist Katie Yamasaki was in seventh grade, a teacher asked her if she wanted to tell the class what had happened on Dec. 7, the anniversary of the attack on Pearl Harbor. 
 Not taking the bait, Yamasaki told the class about how that was the day her great-grandfather was arrested by the FBI on suspicion of being a spy because he owned a produce market. She also mentioned how her family was held in incarceration camps during World War II. 
 But her teacher told the class that the incarceration of Japanese Americans never happened. And after that day, Yamasaki said she never learned anything about the Japanese-American experience in school again. 
 To have that shut down and then go through the rest of my education without it ever being taught just felt like, oh this is so wrong, Yamasaki said. And I know that this is not exclusive to Japanese-American history." 
 Starting this semester, students in the U.S. will be able to learn about the World War II incarceration of Japanese Americans from Yamasakis 2013 childrens picture book, Fish for Jimmy, which was selected to be included in the newest version of McGraw-Hill Educations anthology textbook for fourth grade students. 
 Inspired by a true family story of incarceration at Granada War Relocation Center in Colorado, Fish for Jimmy follows two brothers who are incarcerated during World War II with their mother. 
 RELATED: When Japanese Americans Were Caged: 75 Years After Executive Order 9066 
 Not understanding why they had to leave their home, missing their father who was arrested by the FBI, and unused to the cafeteria-style camp food, younger brother Jimmy stops eating. Older brother Taro sneaks out of the camp at night to catch fish in a nearby stream, and Jimmy is slowly nursed back to health. 
 Fish for Jimmy to me was a way to humanize the internment for children, to show that kids were there too, Yamasaki said. Kids coped in many ways, and kids helped each other, and sometimes we have to help our brothers and our sisters. 
 Nancy Konkus, the director of early reading at McGraw-Hill Education, said that the story is an age-appropriate way to teach a difficult and often overlooked part of U.S. history. 
 The bond between Jimmy and his older brother, Taro, and the sacrifices made by many Japanese American families represent courage and devotion to one's family and culture, she said. Because of its strong ties to our theme, Our History, Our Heritage, we made this story the capstone selection within the unit. 
 Raised in Michigan during the 1980s, Yamasaki remembers the recession during the early 80s and anti-Asian racism connected to layoffs at nearby car companies. 
 A muralist, childrens book author and illustrator, and teaching artist, she comes from a family of artists and is the granddaughter of Japanese-American architect Minoru Yamasaki, who designed the twin towers of the first World Trade Center. 
 Yamasaki said that while she was shielded somewhat by having a famous grandfather and a white mother who was active in her school, she was continually shocked by the casual racism she encountered, even from friends she had known all her life. 
 She noted that Fish for Jimmy is more relevant than ever considering todays political climate. 
 I keep thinking this will relate to one group, but then it ends up relating to a completely different group and that surprises me, Yamasaki said. And thats nice. 
 She added that at a recent exhibition of the art from Fish for Jimmy in New York City, a Syrian person who had lived in refugee camps commented on the final image from the book, which shows older brother Taro and his father looking out from the fence of the incarceration camp. The former refugee told Yamasaki, I know that look, she said. 
 I dont personally know that look because I have never been on the inside of one of those fences," Yamasaki said. "But I felt grateful that it resonated because thats all that you can hope for is that you are doing it right enough that people will connect. 
 In addition to her appearance in classrooms, Yamasaki is working on more projects that such on multiculturalism. She has finished her fourth childrens picture book, When the Cousins Came, scheduled for release in spring 2018 by Holiday House. The story follows two sets of cousins  one pair from the city and one from the country  based on her experiences growing up in a multiracial family with 31 first cousins. 
 She is also currently working on a childrens picture book about her grandfather to be published by Lee and Low Books. She said her grandfathers designs were a response to the racial discrimination and racial profiling he faced throughout his life and career. 
Author/Illus Katie Yamasaki explains the process of creating her new book 'When the Cousins Came' @ our lib preview! pic.twitter.com/Di8WQjMAcG
 Yamasaki hopes that her murals, books, and Fish for Jimmy will help young people realize the power they have to help those who are bullied for being different and to challenge larger political issues like the travel ban or the border wall. 
 I think a lot of American early education teaches that were always the good guy, and were not always the good guy, Yamasaki said. So I think its good for kids to grow up with a consciousness that our country has made mistakes  huge mistakes  and its OK to challenge that. 
 Follow NBC Asian America on Facebook, Twitter, Instagram and Tumblr. 
