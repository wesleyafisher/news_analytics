__HTTP_STATUS_CODE
200
__TITLE
Moral Authority: How Justice Thurgood Marshall Transformed Society
__AUTHORS
Mashaun D. Simon
__TIMESTAMP
Jul 7 2017, 1:47 pm ET
__ARTICLE
 This week marked the 109th birthday of Thurgood Marshall, the first African-American to be appointed to the highest court in America. 
 Later this year Marshall, the motion picture focused on the early days of Marshalls legal career, will be released nationwide. Starring Chadwick Boseman as Marshall, the movie centers around the case against a black chauffeur who is accused of sexually assaulting his white female employer. The movie provides a rare glimpse into Marshall beyond his historic victory of Brown v. Board of Education and his appointment to the Supreme Court. 
 For those who knew him or have studied him, the film sheds a light on a Marshall they were familiar with. 
 Nicole Austin-Hillery, director and counsel at The Brennan Center for Justice in Washington, D.C. told NBC News that Marshalls legal career prior to his appointment was itself a movement. 
 As Marshall was taking on these cases of Black men falsely accused of crimes in the South, he was trying to do more than prove their innocence, she said. He was trying to change the law so that precedence would be put into play. It was a long term game plan focused on changing the legal system of the future. 
 Charles Hamilton Houston, Dean of the Law School at Howard University and Marshalls mentor, passed Marshalls legal strategy to him. One cannot understand Marshall without understanding Houston, said Austin-Hillery. 
 He taught Marshall to strategically think about law. He pressed the importance of choosing cases very carefully, she said. And how you build a lineage of case law on a series of cases, not just one case at a time. Thats what people dont know and are missing about Marshall. 
 They were strategies, Austin-Hillery points out, that are still alive today. 
 When you look at various cases from voting rights to education to the death penalty to gender discrimination  The strategy is to change the legal framework for how these cases are adjudicated; long term legal precedence. 
 John A. Powell agrees. Marshall, he said, had a radical vision of integration and of the law. 
 He had this vision for transforming society, which I think will be around for a long time, he said. There wouldnt be gay rights, there wouldnt be the Loving case without Marshall. 
 Powell, who is Director of the Haas Institute for a Fair and Inclusive Society at the University of California-Berkley, had the pleasure of meeting Marshall once. He describes Marshall as the bone of the civil rights aspiration of an inclusive society  an aspiration that Powell believes we are still struggling with today. 
 Marshall was future-focused. He thought the best way of achieving an inclusive society was through integration and presented himself as being part of the system or within the system while at the same time transforming the system. 
 In the spring of 1978, Marshall who had been on the Supreme Court for just 11 years gave the commencement address to the graduating class of the University of Virginia. It was a major moment in his career, records historians, because he wasnt a fan of giving such addresses. 
 Yet, his advice to the graduates exemplifies his commitment to justice and fairness. He told the class, Where you see wrong or inequality or injustice, speak out, because this is your country. This is your democracy. Make it. Protect it. Pass it on. 
 It is that commitment, that voice that is missing in the legal system and on the court today, as far as Austin-Hillery is concerned. 
 By no longer having Marshall, we are missing that moral authority; that voice of moral authority. And let me say this, we are missing the diverse voice of moral authority. Yes, we have that voice in Ruth Bader Ginsburg, she said. But there is no one who can speak in the same manner that Marshall did  on behalf of the disenfranchised; on behalf of the vulnerable and the misunderstood. That is what he gave us when he was on the bench. 
 RELATED: Thurgood Marshall on civil rights after Brown v Board 
 The experiences of his clients in his early days of his career, even his own experiences, influenced his time on the court. And it is that sensitivity  not to take anything away from Justices Elena Kagan or Sonia Sotomayor because they are marvelous in Austin-Hillerys own words  that is missing from the court. 
 But, the presence of Marshall, his early career, and his time on the court have left us with lessons for us to use as reference points  especially in the wake of the current political and legal climate. 
 Marshalls lineage teaches us that we cannot be distracted, dissuaded or disparaged by what is happening in the moment, Austin-Hillery said. Imagine if Marshall was distracted, dissuaded or disparaged by things happening in the moment. None of the cases he fought, none of the cases he took to the Supreme Court, none of those cases would have occurred. Again, he wasnt just working on case law for the moment but that he was formulating a strategy that would have long term impact. 
 We have to understand, she said, that what is happening in politics and the legal system today isnt just about responding to executive orders being signed by the 45th President of the United States, like the travel ban, or even the firing of the former Federal Bureau of Investigation director James Comey. But, the focus needs to be on putting mechanisms in place that will have an impact beyond this moment, the current President, even the current Congress. 
 When you consider what was going on in the country early in Marshalls career, what he did was major. 
 The country was going through some pretty explicit racism, Powell added. He argued cases throughout the south and had to stay in peoples home because staying in a hotel was too dangerous. He had this vision of transforming society. We are not there by a long shot. 
 Which is why Powell is concerned about the language of withdrawing that is so prevalent in the airwaves today  from Brexit to Make America Great Again. 
 The wounded tend to retreat when they are wounded and that is what we are seeing. But withdrawing is a very dangerous space  a space that created two World Wars within 50 years, he said. You cannot hold this society together when you withdraw into tribalism. We are moving in the opposite direction at lightning speed. 
 Austin-Hillery is optimistic. 
 I think the progressive community has really stepped up to the plate, she said. I think that a large swath of the community are doing a really good job at responding with an eye towards ensuring that we have a foundation in place for how we have to behave and how we have to respond. I am not saying it is easy. It is hard, but people are responding and working to ensure our democracy is protecting the people it needs to protect and we are still respecting the rule of law. 
 Follow NBCBLK on Facebook, Twitter and Instagram 
