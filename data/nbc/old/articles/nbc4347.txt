__HTTP_STATUS_CODE
200
__TITLE
Hundreds Injured as Spanish Police Try to Block Catalan Referendum 
__AUTHORS
NBC News
__TIMESTAMP
Oct 1 2017, 6:51 pm ET
__ARTICLE
A man waves an 'Estelada' (Pro-independence Catalan flag) from a balcony after the closing of the 'Espai Jove La Fontana' (La Fontana youth center) polling station, on Oct. 1, 2017 in Barcelona.
Spanish riot police stormed voting stations today as they moved to stop Catalonia's independence referendum, supported by Catalan regional authorities, after it was banned by the central government in Madrid. More than 760 people were confirmed injured as thousands tried to prevent the polling stations from being closed. Spanish officials had previously said force would not be used, but that voting would not be allowed.
People celebrate after the closing of polling stations outside of the 'Espai Jove La Fontana' (La Fontana youth center). More than 5.3 million Catalans were expected to vote.
An election official carries an empty ballot box out of a polling station in Barcelona.
After voting ended Spanish Prime Minster Mariano Rajoy said that there had been no referendum because the great majority of Catalans have decided not to participate.
"They have sided with our democracy and the rule of law," Rajoy said in a news conference, adding: Some have tried to break the rule of law, and we have answered with serenity and sanity.
Plain clothes police officers try to snatch a ballot box from polling station officials at the Ramon Llull university assigned to be a polling station by the Catalan government in Barcelona.
A protester is attended to after being hit by a rubber bullet shot by Spanish National Police near the Ramon Llull university.
Catalans and police clash outside Ramon Llull university.
A police officer pushes a woman away outside the Ramon Llull university.
