__HTTP_STATUS_CODE
200
__TITLE
Harvey Tragedy: Freezing Toddler Found Clinging to Drowned Mother
__AUTHORS
Saphora Smith
__TIMESTAMP
Aug 30 2017, 3:37 pm ET
__ARTICLE
 A shivering toddler was been found clinging to her drowned mother after they were swept into a drainage canal by floodwaters in southeast Texas late Tuesday. 
 Colette Sulcer and her daughter were driving south through central Beaumont  a city some 75 miles northeast of Houston near the border with Louisiana  when their vehicle got stuck in high water, police said. 
 Sulcer, 41, of Beaumont, abandoned the car in a parking lot and tried to flee to safety with her daughter, Beaumont Police Department said in a statement on Facebook. 
 "At some point she was swept into the canal and ended up floating about  mile from her vehicle," a police spokesman said in a statement. "Two Beaumont Police Officers and two Beaumont Fire Rescue divers in a Zodiac boat, spotted the mother floating with the small child. The child was holding on to her mother." 
 They plucked the pair from the water moments before the two slipped under a floating railroad trestle. 
 "Water was up to the trestle and first responders would not have been able to save the child if they had floated under it," the spokesman said. "The mother was unresponsive. The child was responsive but suffering from hypothermia." 
 They tried to revive Sulcer but she never regained consciousness. The child was in a stable condition. 
 While police did not say how old the girl is, the Associated Press reported she was 18-months-old. 
 Related: Harvey Doubling Back After Houston Deluge, Louisiana in Crosshairs 
 Beaumont has been particularly hard hit by the storm. At one point two inches of rain an hour were falling over the city, the National Weather Service reported. 
