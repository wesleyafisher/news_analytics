__HTTP_STATUS_CODE
200
__TITLE
Airlines Make Billions in Bag and Change Fees, Setting a Record
__AUTHORS
Associated Press
__TIMESTAMP
Sep 20 2017, 4:39 pm ET
__ARTICLE
 WASHINGTON  Travelers who check at least one bag when flying domestically are paying more overall than they did before airlines began unbundling fares in 2008 and charging separately for checked baggage, a government watchdog said Wednesday. 
 A report by the Government Accountability Office said airline officials told GAO investigators that base air fares are now lower than before airlines began separately charging passengers for checked bags, reservation changes, priority boarding and other services. But the GAO's review of studies that have examined the effect of bag fees on ticket prices shows that charging separately for bags reduced fares by less than the new bag fee itself. 
 "As a result, customers who paid for checked bags paid more on average for the combined airfare and bag fee than when the airfare and bag fee were bundled together," the report said. "Conversely, passengers who did not check bags paid less overall." 
 Related: Why Booking the Cheapest Flight Isnt Always the Best Strategy 
 One study found that airlines with bag fees lowered fares to appear more competitive and then made up the lost revenue in bag fees. Another study found that declines in airfares amounted to less than the bag fee, so on average the combined total of the fare and bag fee increased. 
 Airlines collected $7.1 billion in revenue from checked bag and changed reservations fees in the federal budget year ending on Sept. 30, 2016, the GAO said. Those are the only fee revenues that airlines are required to report to the government. 
 The Department of Transportation said Wednesday that airlines collected nearly $1.2 in checked bag fees during the seconded quarter of this year. That's a record and the fifth consecutive quarter that bag fee revenues exceeded $1 billion. 
 "At this rate, passengers are going to have to start showing up with a suitcase full of clothes and a suitcase full of money just to get on the plane," said Sen. Bill Nelson, D-Fla., who requested the report. "It's high time the airlines rein in these outrageous fees." 
 Airlines for America, a trade association, said fares are "historically low and have trended that way since deregulation" in 1978. 
 The group, citing federal figures, noted that "we are in the third consecutive year of real declines in domestic airfares, and year after year we continue to see record numbers of flyers taking to the skies." 
 But the report said the Transportation Department's data had serious limitations that may cause fares to appear lower than they actually are. 
 Consumer advocates and online travel agents also told GAO investigators that the proliferation of new fees for services means flyers aren't always able to determine the full cost of their travel and compare prices across airlines before buying tickets. 
 Congress told the Transportation Department to issue new rules requiring quick automatic refunds for bag fees when checked luggage isn't delivered, but the department hasn't yet acted on the directive. 
