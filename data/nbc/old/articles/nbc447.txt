__HTTP_STATUS_CODE
200
__TITLE
The iPhone Turns 10 And Its Still Changing Everything
__AUTHORS
Alyssa Newcomb
__TIMESTAMP
Jun 29 2017, 10:02 am ET
__ARTICLE
 It's a phone... without a keyboard? 
 At the time of its launch, exactly ten years ago today, the iPhone's "missing keyboard" puzzled critics. 
 And by today's standards, the first iPhone's 3.5-inch screen seems tiny, considering how its screen real estate has now expanded by two inches. The iPhone 1 also boasted eight hours of talk time and six hours of internet use, numbers that have been at least doubled on the latest versions. 
 The initial customers could choose between 4 and 8 gigabyte storage on their iPhones, which seem almost hilariously small considering the iPhone is now available in 32, 128 and 256 gigabyte sizes. 
 It couldn't send photos, had no flash and couldn't record videos. And you could only get it on AT&T. 
 The iPhone has certainly come a long way. 
 Steve Jobs called it a "truly magical product" at the time, and little did we know how right he was. Over the past ten years, Apple's powerhouse device has disrupted industries and created new ones. 
 "Its impact is huge," Tuong Nguyen, principal research analyst at Gartner told NBC News. The iPhone "fundamentally changed the way we thought about phones as well as defining what the industry considered a smartphone." 
 Related: Should You Wait...For the iPhone 8? 
 Perhaps Apple's biggest disruptive impact has been felt in the way we buy and consume music. No longer were iPods or other audio devices needed to listen to music when you could simply carry around your library of jams on your smartphone. 
 "You can almost argue they eclipsed themselves in that sense," Nguyen said. Before the iPhone, "they were all about iPods...They phased the out because now, 'everyone has an iPhone.'" 
 Apple, which began as a computer company, also reshaped the very industry it helped to define in its first three decades. 
 "The device also reinforced the notion of a pocket computer with near-PC like performance one can fit in their pocket. This impacted the PC industry and of course, put Nokia and BlackBerry out of hardware," Patrick Moorhead, principal analyst at Moor Insights & Strategy told NBC News. 
 It also changed the way we relied on our phones. No longer were they just something you'd "stick in the glove box in case of an emergency," Nguyen said. They became portable computers we rely on every day. 
 "Computing used to be something you do on a PC  now, who would have guessed ten years later you literally have a computer in your pocket? That is pretty significant," he said. 
 It also ushered in a new app economy with the creation of the App Store in 2008. Now, anyone with coding skills has a chance to make create apps - and reap the rewards. 
 "It used to be: I have to be a major studio, multi-million dollar company," Nguyen said. 
 That app economy has of course spawned ideas that have changed other industries. Think of how often you use Uber or Lyft to get a ride, or a running app or Apple's HealthKit to track your health and fitness goals. 
 With developers still dreaming up new apps and experiences to work alongside expanded capabilities on the iPhone, the app economy, which was worth $1.3 trillion last year, is showing no signs of slowing down. 
 In five years, the app economy will be worth $6.3 trillion, according to a report from App Annie, as more people continue to spend money on and within apps. 
 While Apple has diehard fans who are undoubtedly counting down to the likely release of the next iPhone in September, the company still doesn't have the biggest market share. Apple had a 13.7 percent market share in the first quarter of this year, compared to Samsung's 20.7 percent. 
 When considering operating systems, Apple is in an even further second place. Android, which runs on many lower cost smartphones, has an 86.1 percent market share. Apple's iOS registers 13.7 percent. 
 So for all its accomplishments, the iPhone still has room to grow. Let's see what the next 10 years bring. 
