__HTTP_STATUS_CODE
200
__TITLE
Doctors Wade Through Harvey Floods to Treat Cancer Patient
__AUTHORS
Maggie Fox
__TIMESTAMP
Aug 31 2017, 7:00 pm ET
__ARTICLE
 It was Monday morning, time to infuse his patients immune cells back into his body so they could kill the melanoma tumors threatening his life. 
 But the entrance to Dr. Adi Diabs apartment parking garage in Houston was blocked by water from Hurricane Harveys floods and he couldnt get out that way. The timing of patient John Cormiers treatment was crucial  he was sitting in a hospital bed, his immune system deliberately destroyed by chemotherapy, and the replacement cells wouldnt last long in the lab. 
 So Dr. Diab put on his only boots and waded three miles in water to MD Anderson Cancer Center to hopefully get there in time to start the treatment. 
 There was water all the way. It was not deep all the time, said Diab, a melanoma specialist at MD Anderson. 
 Some parts of the way were, like, one foot deep. He had to backtrack several times, making his usually short walk to the cancer center a lot longer than it would have been otherwise. 
 Related: Harvey Weakens, But Recovery Will be Long 
 I had to go through the park and there was a lot of water there, Diab said. 
 His boots got soaked and he was the last team member to arrive. 
 Cormier had been in the hospital for a week as chemo treatments killed off his bone marrow cells to make room for the new infusion of cells. He is taking part in an experimental approach to treating melanoma. 
 The T-cells are coming from the tumor itself, Diab said. These immune cells are often found attacking the tumor, as the bodys immune cells will. 
 Cancer grows and spreads when there just are not enough immune cells with the equipment to do the job. Cormiers therapy involved finding what are called tumor infiltrating lymphocytes, which have recognized and started attacking the cancer. 
 We harvest those cells from the cancer, Diab said. We grow them into large numbers, and we re-integrate them. These are actually his own immune cells fighting the cancer. 
Harvey can't beat us! @mkwongmdphd (far right) w TIL Team Monday! @MDAndersonNews strong. pic.twitter.com/VHeYkNraiA
 But timing was everything. 
 This treatment is designed ahead of time and it was designed that he get the treatment on that day, Diab said. If you wait longer than that, the cells will not be as good. 
 Cormier knew that. 
 There is a certain window of time in which the cells had to be re-administered to my body and that was right in the middle of the flood, said Cormier, who has been traveling to Houston from his home of Lafayette, Louisiana for the treatment, which now could go ahead as scheduled. 
 My team made it here. I didnt realize that Dr. Diab had waded in. He had some boots on, but he didnt tell me he walked three miles in flood water to get here. 
 Cormier had been wondering what would happen as Harvey lashed rain against the windows of his hospital room and flooded some of the roads around the cancer center, part of a cluster of hospitals known as the Texas Medical Center. 
 The whole team showed up and I was able to get my T-cells transplanted back into my body, he said. It is truly amazing. 
 It takes a fairly sizable team to perform the therapy Cormier is getting, but they all made it in through the devastating flooding, said Laura Sussman, a spokesperson for the hospital. 
 All of the team members were able to make it in by car during a lull in the storm, Sussman said. 
 Dr. Karen Lu, our chief medical officer-- they sent a high water vehicle to get her. It couldn't get to her house because the water was too high, so she insisted she'd walk to it in waist deep water. 
 Cormier said Thursday he was not feeling the effects of treatment just yet.I just feel the comfort of knowing they are there, fighting the cancer, he said. 
 And luckily, because so few staff are in the hospital, what food supplies have been on hand have been plenty to feed Cormier, his wife and sister-in-law. 
 They have a nice gourmet restaurant here, said the slow-talking Cormier. 
 Diab knew his patient was not as relaxed as he may have sounded. 
 This was very tricky for the patient himself. He was in the hospital but he also had to think of his own family fighting the hurricane. That is a source of stress, Diab said. 
 Harvey has hit Louisiana, too, and Cormiers grown son lives in Houston. 
 So he could have used all the support and I would never have failed him on that, said Diab. 
 This is the promise of MD Anderson  you will never walk alone, and I will never let him walk alone. 
 Related: The Unexpected Health Impacts of Harvey 
 Diab cannot say what the outlook is for Cormier, who has stage 4 melanoma  highly advanced. 
 We can never guarantee anything, but I am very optimistic about this patient. He is very brave, very powerful. And, knock on wood, he has not experienced any (serious) side-effects, Diab said. 
 What about the boots? 
 They were made of suede and those were the highest boots I had. They were not waterproof. They are drying out, said Diab. I am not prepared for these things.  
