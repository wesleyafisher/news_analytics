__HTTP_STATUS_CODE
200
__TITLE
Trump Taps Kirstjen Nielsen for DHS Secretary
__AUTHORS
Vivian Salama
Julia Ainsley
Hallie Jackson
__TIMESTAMP
Oct 11 2017, 5:22 pm ET
__ARTICLE
 WASHINGTON  President Donald Trump Wednesday tapped Kirstjen Nielsen, Gen. John Kellys longtime aide, as his pick to be the next Department of Homeland Security Secretary. 
 Nielsen went to work at the White House in September, after Trump named Kelly, then DHS Secretary, as his chief of staff. The president made the announcement in a statement Wednesday afternoon and her nomination now goes to the Senate for confirmation.   
 Nielsen, 45, served as Kellys chief of staff at the Department of Homeland Security at the start of the administration and was a key player in the rollout of the presidents immigration crackdown and travel ban. 
 The officials said that the selection of Nielsen comes as Kelly works to bring order to an administration that has been fraught with controversy and mixed messages since Trump was inaugurated. They spoke on the condition of anonymity since the decision has not been formally announced. 
 "She has Kelly's full faith and confidence," the congressional source said. 
 The decision to select Kellys successor as secretary of Homeland Security took months, in part because the administration had trouble finding someone who was conservative enough on immigration but who could also get confirmed, the congressional source said. But Nielsen will likely be viewed in the same way as Kelly, a general who followed orders, because the two work so closely together. 
 House Homeland Security Committee Chairman Mike McCaul was also considered for the position, but was told in recent weeks he was no longer on the short list, the congressional source said. 
 Acting Homeland Security Secretary Elaine Duke had been rumored to be on the short list, but questions have been raised about Duke's performance since stepping into the role after Kellys departure, according to one White House official, who noted she "ruffled some feathers." 
 The Department of Homeland Security was formed in response to the Sept. 11, 2001, attacks to bring multiple agencies under one roof. Running the department, which includes the countries largest law enforcement agency, U.S. Customs and Border Protection, takes strong managerial skills, former DHS officials have said. 
 Nielsen previously worked as a member of the Resilience Task Force of the Center for Cyber & Homeland Security think tank. She is a graduate of the Georgetown School of Foreign Service and holds a law degree from the University of Virginia. 
