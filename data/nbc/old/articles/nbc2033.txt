__HTTP_STATUS_CODE
200
__TITLE
Spread of H7N9 Bird Flu Worries Officials in China
__AUTHORS
Reuters
__TIMESTAMP
Feb 15 2017, 10:03 am ET
__ARTICLE
 As many as 79 people died from H7N9 bird flu in China last month, the Chinese government said, stoking worries that the spread of the virus this season could be the worst on record. 
 January's fatalities were up to four times higher than the same month in past years, and brought the total H7N9 death toll to 100 people since October, data from the National Health and Family Planning Commission showed late on Tuesday. 
 Authorities have repeatedly warned the public to stay alert for the virus, and cautioned against panic in the world's second-largest economy. 
 Related: CDC Issues Bird Flu Warning 
 But the latest bird flu data has sparked concerns of a repeat of previous health crises, like the 2002 outbreak of Severe Acute Respiratory Syndrome (SARS). 
 "It's mid-February already and we are just getting the January numbers. With the death rate almost catching up with SARS, shouldn't warnings be issued earlier?" said one user of popular microblog Sina Weibo. 
 Other netizens in the Chinese blogosphere worried about the pace of infections, and called for even more up-to-date reports. 
 The People's Daily, the official paper of the ruling Communist Party, warned people in a social media post to stay away from live poultry markets, saying it was "extremely clear" that poultry and their excrement were the cause of the infections. 
 Related: Watch Out for H7N9 Bird Flu, WHO Says 
 "The situation is still ongoing, and our Chinese counterparts are actively investigating the reported cases," the World Health Organization's China Representative Office said in an emailed statement to Reuters. 
 "As the investigation is ongoing, it is premature to conclusively identify the cause for the increased number of cases. Nevertheless, we know that the majority of human cases got the A(H7N9) virus through contact with infected poultry or contaminated environments, including live poultry markets." 
 China, which first reported a human infection from the virus in March 2013, has seen a sharp rise in H7N9 cases since December. The official government total is 306 since October, with 192 reported last month. 
 But others believe the number of infections is higher. 
 The Center for Infectious Disease Research and Policy (CIDRAP) at the University of Minnesota last week estimated China had at least 347 human infections so far this winter, eclipsing the record of 319 seen three years ago. 
 "An important factor in the past waves of H7N9 cases among humans in China has been rapid closure of live poultry markets," said Ian Mackay, a virologist at the University of Queensland in Australia. 
 "This season there seems to have been a slower response to the outbreak, which may be leading to greater numbers of human exposures to infected birds." 
 Related: H7N9 Bird Flu Spreads Like Ordinary Flu 
 The National Health and Family Planning Commission has yet to respond to a request from Reuters seeking comment on the recent bird flu deaths. 
 Most of the H7N9 human infections reported this season have been in the south and along the coast. 
 In Hong Kong, where two of the four patients infected with H7N9 this winter have died, health officials said they would step up checks at poultry farms. 
 H7N9 had spread widely and early this year, but most cases were contained in the same areas as previous years, including the Yangtze River Delta and Guangdong, Shu Yuelong, head of the Chinese National Influenza Center, told state radio. 
 Beijing on Saturday reported its first human H7N9 case this year. The patient is a 68-year-old man from Langfang city in neighbouring Hebei province. 
 A second human case was reported on Tuesday. 
 "It is highly likely that further sporadic cases will continue to be reported," the WHO said. 
 "Whenever influenza viruses are circulating in poultry, sporadic infections or small clusters of human cases are possible." 
