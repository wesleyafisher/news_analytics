__HTTP_STATUS_CODE
200
__TITLE
Cozy Bear Explained: What You Need to Know About the Russian Hacks
__AUTHORS
Josh Meyer
__TIMESTAMP
Sep 15 2016, 7:18 am ET
__ARTICLE
 From Colin Powell to Venus Williams, from the White House to the Democratic National Committee, Americans have been under attack from hackers who U.S. officials believe are tied to the Russian government. The Internet intrusions and data dumps have sowed embarrassment and alarm and raised questions about the safety of our national secrets and even our presidential election. 
 Heres a look at what we know about the hacks, who is behind them and what might be coming next. 
 A wide range of intelligence and law enforcement officials have told NBC News that the recent attacks and the threat of escalation are what keep them up at night, especially the question of what other systems might be penetrated. 
 Lisa Monaco, the top White House adviser on homeland security, said at a conference on Wednesday that the U.S. is responding to the Russian cyberattacks with the same framework it uses to combat terrorism. 
 And NSA chief Mike Rogers told Congress this week the hacking is an issue of great focus and that spy agencies are actively concerned that Russian might try to disrupt the presidential election. 
 Does the Kremlin really want Tim Kaines personal cell number or Simone Biles blood tests? Foreign governments frequently hack their adversaries to gather valuable intelligence, but the boastful leaking of stolen information by the Fancy Bear and Cozy Bear hacker teams reveals there are other motives at play. 
 Experts say Moscow is wielding the hacks as a weapon  to get attention and respect on the world stage, to humiliate Washington, and to shake Americans confidence in their institutions. 
 "It shows the way Russia uses cyber as an instrument of national power and in ways that we don't expect to see here in the U.S.," said Toni Gidwani, a former U.S. Defense Intelligence Agency official who works for ThreatConnect, a cybersecurity firm that investigated the DNC hack. 
 Since the hackers have repeatedly targeted Democrats, some observers have suggested they are trying to help Donald Trump, but officials believe that those orchestrating the attacks are more interested in undermining trust in the entire electoral system. 
 These are the names that cybersecurity firms have given to two separate  and possibly competing  computer espionage groups based in Russia. 
 Fancy Bear, also known as APT 28, has been tied to most of the Russia hacks garnering headlines of late, including against the World Anti-Doping Agency and the Democratic Party. U.S. security officials say it has links to GRU, Russias foreign military intelligence agency 
 Cozy Bear, which is sometimes called APT 29, was blamed for the hacks of the White House, Joint Chiefs of Staff and State Department. Its believed to be tied to the FSB, Russias internal security service. The Russians are believed to be using other groups like Anonymous Poland, Guccifer 2.0 and DCLeaks to make their plunder public, investigators say. 
 A senior Justice Department official told NBC News that forensic investigation shows that Cozy Bear and Fancy Bear are not just hacker groups operating in the orbit of the Russian government but are actually operating as part of the government apparatus. 
 The Kremlin has firmly denied involvement, and for obvious reasons, American officials say they cant release details of the evidence that would show Russia is lying. 
 Its worth noting that previous foreign hacking cases show the U.S. can use forensics to track intrusions to named individuals and file criminal charges against them. Earlier this year, the FBI released mugshots of seven Iranian computer experts it accused of cyberattacks on American banks and a New York dam. 
 Washington hasnt named and shamed the Russians like it has done in other foreign hacking cases. One reason, as NBC reported earlier this week, is that officials dont want to tip off the Russians to U.S. sources and investigative methods. 
 Another is diplomatic: the U.S. wants Russian cooperation in Syria and is worried about driving a bigger wedge between the two countries by hitting back on the cyber front. President Obama also has indicated he has little desire to escalate the situation and spark a Cold War-style hacking arms race. 
 And senior federal law enforcement officials told NBC News on Wednesday that they need to gather a tremendous amount of specific information before officially pointing the finger. 
 At Wednesdays conference, however, Monaco said the U.S. will crack down on its cyber-enemies when the time is right, and she suggested the model will be the same one used in Iran and China hacking episodes, which resulted in federal charges. 
 Nobody should think theres a free pass when youre conducting malicious cyberactivity, she said. I think our reach is long, she added. Sometimes it takes a long time to build a case, but it doesnt deter us from continuing to pursue it. 
 Well, Vladimir Putin isnt likely to get his hands on our nuclear codes if thats what youre worried about. 
 While the hackers have managed to break into the White House email, they only saw unclassified material. And it appears their focus has been on grabbing and posting data, and not secretly manipulating the information  so far. 
 For instance, they may have busted into voter registration databases in Illinois and Arizona, but theres no evidence they have tried to change the outcome of an election. 
 And the U.S. has tried to make the security around more sensitive material impenetrable. 
 What the recent episodes show is that some of those firewalls clearly arent strong enough. Officials in Washington acknowledge that its a day-to-day struggle to stay ahead of the Russians and other foreign adversaries on a relentless hunt for chinks in American armor. 
 As one senior U.S. intelligence official told NBC News on Wednesday, the biggest concern is what Russia might be up to that Washington doesnt know about. 
 They have tremendous cybercapabilities and have demonstrated an ability  and an interest  in using them in a lot of different ways, said the official. Theyve been doing this stuff for a long time, and we should be very, very worried about the Russias capability in the cyber realm. 
