__HTTP_STATUS_CODE
200
__TITLE
Man Accused of Killing Chinese Scholar Facing Tougher Charge
__AUTHORS
Associated Press
__TIMESTAMP
Oct 13 2017, 12:51 pm ET
__ARTICLE
 CHAMPAIGN, Ill.  A man accused of abducting and killing a University of Illinois scholar from China has been arraigned on new charges. 
 Federal court records show that 28-year-old Brendt Christensen appeared before a judge Wednesday afternoon. He previously was charged with abducting 26-year-old Yingying Zhang of Nanping, China. But a grand jury last week returned a superseding indictment charging him with the more serious crime of kidnapping resulting in Zhang's death. 
 The new indictment alleges that Christensen intentionally killed Zhang and that her death involved torture or serious physical abuse. If convicted of the new charge, Christensen would face the death penalty or mandatory life in prison. 
 RELATED: Suspect in Disappearance of Chinese Scholar Could Face Life 
 Zhang disappeared June 9, weeks after arriving at the campus in the central Illinois city of Champaign. Even though her body hasn't been found, authorities believe Zhang is dead. 
 Follow NBC Asian America on Facebook, Twitter, Instagram and Tumblr. 
