__HTTP_STATUS_CODE
200
__TITLE
Rise of Online Shopping Adds to Retailers Gift Return Headaches
__AUTHORS
Jessica Dickler, CNBC
__TIMESTAMP
Dec 24 2015, 12:13 pm ET
__ARTICLE
 The holidays aren't even over yet and shoppers are planning their gift returns. 
 Already, 8 percent of shoppers plan to hit the stores on the day after Christmas to return or exchange gifts they received from others, according to a survey by American Express. 
 Altogether, holiday returns account for about 8 percent of all holiday sales, according to the National Retail Federation. This year retailers estimate that total annual returns will reach $260.5 billion, the NRF said. 
 In a report released just ahead of Black Friday, DynamicAction found that holiday returns had already increased 9.3 percent compared to the prior season. 
 The ever growing number of online shoppers has a significant impact on the number of returns, as most shoppers place orders without ever seeing a product in person and gamble on everything from the size to the style. Led by Amazon, online purchases are at an all-time high this year, according to America's Research Group. 
 These Stores Have the Strictest Return Policies 
 The amount of returning "is going to escalate every year in direct proportion to the holiday shopping that's done online," said Candace Corlett, president of WSL Strategic Retail, a retail consulting firm. 
 "There's clearly a mindset that says, 'I'll order it all and then just take it back,'" Corlett said. "Retailers have created a returns-driven culture because they made it so easy to keep buying with free shipping." 
 As much as a third of all Internet sales get returned, according to retail consulting firm Kurt Salmon, and the rate peaks during the holiday season. Overall, returns are significantly lower at bricks-and-mortar stores, averaging closer to 5 percent. 
 But for retailers, reining in the number of returns after Christmas is a work in progress. Some have attempted to curb the costly rush to return with stricter return policies and shorter windows for returns. The aim for retailers is to have returned items back up for resale in-season, which necessitates a fast turnaround, according to Steve Osburn, Kurt Salmon's supply chain strategist. 
 "Stores are getting stricter in their policies, but they lose customers because of it," noted Britt Beemer, chairman and CEO of America's Research Group. "An easy return policy is very attractive to shoppers." 
 Retailers Are Losing $1.75 Trillion Over This 
 In fact, 97 percent of consumers said that the return process is important to their future intentions to shop with a retailer, according to a report by technology firm Voxware. 
 Although policies vary from store to store, from a two-week window at Apple and Barnes & Noble to much greater leeway at Target and Wal-Mart, shoppers are savvy about the rules, Corlett said. 
 About 2 out of 3 shoppers check the returns policy before making a purchase, according to a report by ComScore and commissioned by UPS. And most customers make returns just in the nick of time, within two to three days before the end of the return window. Not even Santa is that on the ball. 
