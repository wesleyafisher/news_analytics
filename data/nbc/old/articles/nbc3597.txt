__HTTP_STATUS_CODE
200
__TITLE
U.S.: Mosul Talk Helps Civilians, Encourages Defectors
__AUTHORS
The Associated Press
__TIMESTAMP
Oct 10 2016, 4:03 pm ET
__ARTICLE
 BAGHDAD  The spokesman for the U.S.-led coalition helping the Iraqi military on Monday defended recent pronouncements that the operation to retake Mosul is imminent, saying the advanced warning gives civilians hope they will soon be liberated and encourages defections of extremist fighters. 
 Republican presidential nominee Donald Trump has criticized the openness about future battle plans, most recently in Sunday's debate, claiming it has allowed leaders of the ISIS group to flee. 
 As Iraqi forces have moved to encircle Mosul, the country's second-largest city, the Iraqi leadership has promised that IS will soon be pushed from the city that it has firmly held for more than two years. 
 "Today you are closer than ever to being rid of the injustice, gore and brutality of Daesh," Iraqi Prime Minister Haider al-Abadi said in a recent radio address to Mosul's residents, using the Arabic acronym for the Islamic State group. 
 U.S. military leaders refuse to provide details or timelines for the start of the Mosul campaign, but they do discuss ongoing operations to prepare for the battle. 
 Trump criticized such announcements during Sunday's debate with Democrat Hillary Clinton. 
 Related: ISIS' Al-Bayan Radio Station in Mosul is Bombed Into Silence by Iraqi Jets 
 "We have announcements coming out of Washington and coming out of Iraq: We will be attacking Mosul in three weeks or four weeks. Well, all of these bad leaders from ISIS are leaving Mosul," Trump said, using another acronym for the Islamic State. "Why can't they do something secretively, where they go in and they knock out the leadership?" 
 The spokesman for the U.S.-led coalition, U.S. Air Force Col. John Dorrian, is not permitted to address politicians' remarks directly. However, he said Monday that announcing future battle plans helps civilians. 
 "What this does is it gives hope to people who do not want to live under Daesh that they will be liberated soon," Dorrian said, adding that such warnings also encourage defections among lower-ranked IS fighters who are not ideologically loyal to the extremist group and have been coerced to take up arms. 
 "It would be much better to have them lay down their arms than have a bloody battle," Dorrian said. 
 U.S. military officials have said that Iraqi forces and the coalition are battling for towns on the outskirts of Mosul in a broader move to encircle the city and cut off the remaining escape routes that IS fighters have been able to use to flee. 
 The spokesman for Iraq's defense ministry, Yahya Rasool, said Monday that some IS fighters have fled Mosul, but he believes many will dig in and fight. "Daesh will fight in Mosul in order to defend the capital of their terrorist caliphate," he said. 
 On Sept. 29, Dorrian said coalition strikes killed 18 Islamic State leaders in Iraq and Syria during the previous 30 days. Of those, he said, 13 were part of its military intelligence communication networks in Mosul. 
