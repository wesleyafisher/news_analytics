__HTTP_STATUS_CODE
200
__TITLE
Cam Newton Puts Spotlight on the Sexism That Women Covering Sports Often Face
__AUTHORS
Elizabeth Chuck
__TIMESTAMP
Oct 5 2017, 7:50 pm ET
__ARTICLE
 Cam Newton may have thought it was "funny to hear a female" speaking knowledgeably about football during a recent press appearance. But for female sports journalists, many of whom say they're taken less seriously because of their gender, the quarterback's sexist comments are no laughing matter. 
 "We have to do the hard work, the extra work," Lindsay Jones who, as a sports reporter for USA Today, has written about the NFL for a decade, told NBC News on Thursday. "We have to do twice as much interview preparation to be taken half as seriously. And that's unfortunate, but we're used to it." 
 At a Wednesday news conference, Newton, the star quarterback of the Carolina Panthers, smirked as Jourdan Rodrigue, a Panthers beat reporter for The Charlotte Observer, asked him about receiver Devin Funchess. 
 Cam, I know you take a lot of pride in seeing your receivers play well, Rodrigue said. Devin Funchess has seemed to really embrace the physicality of his routes and getting those extra yards. Does that give you a little bit of enjoyment to see him kind of truck-sticking people out there? 
 Related: NFL Calls Newtons Comments Just Plain Wrong and Disrespectful' 
 Before answering the question, Newton replied: "Its funny to hear a female talk about routes. Its funny. 
 His comments prompted an immediate backlash. The NFL released a statement calling them "just plain wrong and disrespectful to the exceptional female reporters and all journalists who cover our league." The Pro Football Writers of America said Newton had "crossed the line." And the Association for Women in Sports Media said they were "very discouraged by Cam Newton's disrespectful remarks and actions directed to a female reporter." 
 Jenny Dial Creech, president of the Association for Women in Sports Media and a sports columnist at The Houston Chronicle, said sexism is widely felt among women who cover sports. 
 "I think if you talk to any female sportswriter on any beat, covering anything, something like this has happened to her at some point," Creech said. "For a lot of women, it's a lot more than once." 
 Some of the sexism is more subtle, Creech said, like coaches and athletes calling journalists "sweetheart" or giving them backhanded compliments about how "surprised" they are by a good question. Other times, it's more blatant. 
 "It does happen so often that sometimes, we just kind of forget that it's there. It's just a part of what we do," she said. 
 Jones echoed that. 
 "We all have stories like this. We all have had incidents like this, whether it's a coach or a player, and certainly people on the Internet have questioned your credentials because you're a woman," she said. "It just usually doesn't happen on this public of a stage with this high profile of a player." 
 It's not clear how sentiment toward female sports journalists has evolved since 1975, when The New York Times' Robin Herman became the first female reporter to enter an NFL locker room. The Pew Research Center, which tracks demographic trends, said it did not have any studies on this and wasn't aware of any that had been done. 
 Despite encountering sexism in their profession, the majority of coaches and players aren't sexist, said Creech, Jones and others. 
 Jackie MacMullan, a longtime sportswriter who in 2010 was the first female recipient of the Curt Gowdy Media Award from the Naismith Memorial Basketball Hall of Fame, told ESPN that she's "always a little taken aback" by sexism in her field. 
 "I'd like to think that Cam Newton is the exception and not the rule. Honestly, I think Cam Newton is a knucklehead," MacMullen told espnW, the sports network's women-centric brand, on Thursday. 
 The fallout from the incident continued on Thursday as Dannon's Oiko brand announced it was cutting sponsorship ties with Newton and Newton's coach, Ron Rivera, told reporters, "I think Cam made a mistake." 
 But no apology has come from Newton himself. 
 Rodrigue did not respond to a media inquiry from NBC News. She tweeted on Wednesday,:"I don't think it's 'funny' to be a female and talk about routes. I think it's my job." 
I don't think it's "funny" to be a female and talk about routes. I think it's my job.
 On Thursday, a tweet from her apologizing for "offensive" tweets she had made five years ago muddied the backlash to Newton's comments. 
 Twitter users posted screenshots of posts she had allegedly sent joking about her father being racist and one she had retweeted that used the N-word. 
 Jones, the USA Today reporter, said Rodrigue's apology didn't take away from the fact that Newton had yet to apologize. 
 "That's pretty disheartening because I was hoping that maybe this could be a learning experience for Cam Newton and the NFL community at large," she said. "I'm hoping NFL teams and other pro teams will use this as in incident of a reminder that we need to improve the media training of our players." 
