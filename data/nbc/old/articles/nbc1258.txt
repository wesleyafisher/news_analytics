__HTTP_STATUS_CODE
200
__TITLE
Oscars Screwup Gives Moonlights Best Picture Award to La La Land
__AUTHORS
Alex Johnson
__TIMESTAMP
Feb 27 2017, 10:31 pm ET
__ARTICLE
 LOS ANGELES  "It's very unfortunate." That's all host Jimmy Kimmel could say Sunday night after the Academy Award for best motion picture was initially given to "La La Land"  even though "Moonlight" was the winner. 
 Warren Beatty, who presented the final award of the evening with Faye Dunaway, his co-star in "Bonnie and Clyde," paused as he looked at the card bearing the name of the winning movie. He handed the card to Dunaway, who called out "La La Land." 
 But a minute or two into the celebration by "La La Land's" cast and crew, producer Jordan Horowitz stepped to the microphone, asked for quiet and said the real winner was "Moonlight." 
 Gasps were heard around the auditorium. 
 "This is not a joke," Horowitz said as the cast and crew of "La La Land" left the stage. 
 PHOTOS: Oscars Best Picture Fiasco Unfolds Onstage at Academy Awards 
 "Moonlight," about a young boy struggling with poverty and his sexuality in Miami, was nominated for eight Academy Awards and also brought a supporting actor Oscar for first timer Mahershala Ali. 
 It represented a nod to diversity, only a year after only white actors were nominated in the top four Academy Awards categories, prompting the #OscarsSoWhite boycott. 
 After several minutes of shock and confusion on stage, Beatty returned to the microphone with an explanation: He and Dunaway apparently had been given the envelope for best lead actress by mistake. That award had gone to Emma Stone for "La La Land" earlier in the evening. 
 "I wasn't trying to be funny," Beatty said. 
 Accountancy firm PricewaterhouseCoopers, known as PwC, is in charge of tallying up the votes and distributing the envelopes. It quickly apologized and pledged to investigate, but the apology was savaged for its use of passive voice  saying the mistake "was made" after the presenters "had been given" the wrong envelope. 
 It wasn't until late Monday night that the firm took full responsibility, issuing a new tweet that directly blamed PwC partner Brian Cullinan for handing a backup envelope for a different category to Warren Beatty and Faye Dunaway  and for not following "protocols for correcting" the error. 
pic.twitter.com/uNGSbhgKFt
 Ali said the fiasco made it "hard to feel joy" over his Oscar win. 
 Related: Highlights From the Star-Filled Oscars Ceremony 
 "It just threw me a bit. It threw me more than a bit," Ali said backstage. "I didn't want to go up there and take something from somebody." 
 The flub was reminiscent of Steve Harvey's mistake when he announced the wrong winner of the Miss Universe pageant in December 2015  as the pageant was quick to remind everyone. 
Have your people call our people - we know what to do. #Oscars #MissUniverse
#Oscars #LALALAND  is the new #MissColombia pic.twitter.com/kqIlpu3Qfw
 Kimmel joked: "I knew I would screw this show up. I really did." 
 Emma Stone, who earlier had been named best actress, described the mistake as "the craziest Oscar moment of all time." 
 Made for just $1.5 million, "Moonlight" is an unusually small Oscar winner. Having made just more than $22 million at the box office as of Sunday, it's one of the lowest-grossing best-picture winners ever  but also one of the most critically adored. 
 Related: 'The Susan Lucci of the Oscars' Finally Ends Losing Streak 
 "La La Land" still collected a leading six awards, adding honors for cinematography, production design, score, the song "City of Stars" and best director to Stone's award. Damien Chazelle, the 32-year-old filmmaker, became the youngest to win best director. 
 Casey Affleck  in one of the night's most closely watched races, won best actor  his first Oscar  for his soulful, grief-filled performance in "Manchester by the Sea." 
