__HTTP_STATUS_CODE
200
__TITLE
Harvey Weakens, Leaves Houston Facing Weeks and Months of Recovery
__AUTHORS
Alex Johnson
Alastair Jamieson
__TIMESTAMP
Aug 31 2017, 1:24 pm ET
__ARTICLE
 Harvey was heading inland and losing steam Thursday, leaving in its wake a trail of death and destruction in Houston and the Gulf Coast that will take months, if not years, to clean up. 
 In its most recent advisory on the deadly storm, the National Weather Service said maximum sustained winds had dropped to 20 mph as the system, now a tropical depression, moved through central Louisiana overnight and headed into northwestern Mississippi. 
 "Harvey should continue to gradually weaken ... [and] is expected to dissipate over the Ohio Valley within 72 hours," the NWS said at 11 p.m. ET Wednesday. 
 In Texas, Galveston and Harris County authorities reported seven more deaths linked to the storm Wednesday night, bringing the statewide total to at least 29. The victims include a Houston family of six who perished when their van was swept away by floodwaters. Harvey was a Category 4 hurricane when it made landfall for the first time late Friday. 
Two explosions have been reported at the flooded Arkema chemical plant in Crosby, Texas.
Memphis, Nashville and parts of Louisiana could see heavy rain and potential flash floods.
 But with as much as eight more inches of rain expected as the system moves farther inland, authorities said they dread finding out how many more remain under the several feet of water that is expected to continue submerging southeastern Texas and southwestern Louisiana for weeks. 
 "We know in these kind of events that, sadly, the death toll goes up," Houston Police Chief Art Acevedo said Tuesday. "I'm really worried about how many bodies we're going to find." 
 The military said Wednesday that two Navy ships and nearly 700 Marines were being sent to the region. Texas has already activated its entire National Guard, and crews from around the country have arrived to help. 
 More than 32,000 residents remain in Texas shelters, authorities said. As many as 40,000 homes may have been damaged in Houston and Harris County alone, County Judge Ed Emmett said. Almost 210,000 people have registered with the Federal Emergency Management Agency for assistance, Texas Gov. Greg Abbott said. 
 Beaumont and Port Arthur, about 85 miles east-northeast of Houston, were watery wastelands on Wednesday as 30 inches of rain unloosed mammoth flash floods. The Robert A. Bowers Civic Center in Port Arthur, where many residents had taken shelter, was flooded. 
 "Many Texans in and around Beaumont [and] Port Arthur are fighting for their lives against an incredible amount of water," Bill Karins, a meteorologist for NBC News, said Wednesday. "This is just as bad, if not worse, than flooding in Houston." 
 Beaumont lost its water supply early Thursday after its main pumping station failed, the city said in a 1:30 a.m. ET update. 
 In Crosby, about 20 miles northeast of Houston, two explosions were reported early Thursday at the flooded Arkema Group chemical plant because refrigerators and backup generators were knocked out, the company warned. The lack of electricity and the high water left "no way to prevent it," said Richard Rowe, chief executive of the firms North America operations. 
 Other industrial facilities, notably some of the many oil refineries along the Gulf coast, may have released as much as 2 million pounds of potentially hazardous airborne pollutants in the Houston area, according to regulatory filings submitted to the Texas Commission on Environmental Quality. 
 "By industry's own estimates, we've seen months' worth of harmful pollution released in less than a week," said Elena Craft, a senior health scientist at the nonprofit Environmental Defense Fund. 
 And Harvey's might will continue to be felt for many months, perhaps years, in the state that contributes nine percent of the U.S. gross domestic product. With an economy of $503 billion, Houston, the nation's fourth-biggest city, accounts for 3 percent of U.S. GDP all by itself. 
#Harvey's river flooding shatters records and could last for many more days: https://t.co/XcAh5Kxlu7 #Harvey2017 pic.twitter.com/rKisSfB467
 The storm shut down the nation's largest refinery operations, halting about 20 percent of the country's daily supply of fuel. 
 Gasoline prices in some parts of the country were already rising  and that was before Colonial Pipeline Co., the biggest fuel transporter in the United States, said it was indefinitely closing its lines carrying gasoline, diesel and jet fuel from Texas to New York Harbor because of damage to refineries and facilities in the Gulf of Mexico. 
 Economists consulted by NBC News gave estimates of total financial losses from Harvey ranging from $48 billion to $75 billion. 
 "At the end of the day, this is likely to be the largest natural disaster by property damage in history," said former Houston Mayor Bill White, who was in office when thousands of people displaced by Hurricane Katrina fled for Houston in 2005. 
