__HTTP_STATUS_CODE
200
__TITLE
Trump Declares Opioid Crisis National Emergency 
__AUTHORS
Ali Vitali
Corky Siemaszko
__TIMESTAMP
Aug 10 2017, 5:38 pm ET
__ARTICLE
 WASHINGTON  President Donald Trump threw the weight of the White House behind the fight against the opioid crisis Thursday and declared it a national emergency. 
 "The opioid crisis is an emergency, and Im saying officially, right now, it is an emergency," Trump said at his golf club in Bedminster, New Jersey. "Its a national emergency. Were going to spend a lot of time, a lot of effort and a lot of money on the opioid crisis." 
 Trump's surprise announcement came two days after he vowed the U.S. would "win" the fight against the epidemic but stopped short of acting on the recommendation of the presidential opioid commission to declare a national emergency. 
 It was not immediately clear what prompted Trump's change of course. But he called the crisis "a serious problem the likes of which we have never had." 
 "You know when I was growing up they had the LSD and they had certain generations of drugs," Trump said. "Theres never been anything like whats happened to this country over the last four or five years. And I have to say this in all fairness, this is a worldwide problem, not just a United States problem." 
 Related: One in Three Americans Took Prescription Opioid Painkillers in 2015, Survey Says 
 Experts said that the national emergency declaration would allow the executive branch to direct funds towards expanding treatment facilities and supplying police officers with the anti-overdose remedy naloxone. 
 It would also allow the administration to waive some federal rules, including one that restricts where Medicaid recipients can get addiction treatment. 
 And while the declaration could put more pressure on Congress to provide additional funding, Northwestern University Pritzker School of Law professor Juliet Sorensen told NBC News earlier this week it would be a rare move for Trump that both Republicans and Democrats could agree on. 
 "Everybody agrees this is a crisis," she said. 
 New Jersey Gov. Chris Christie, who chairs the presidential opioid commission, thanked Trump "for accepting this first recommendation of our July 31 interim report." 
 Related: Mass-Casualty Event: Ohio County Now Tops U.S. in Overdose Deaths 
 "I am completely confident that the President will address this problem aggressively and do all he can to alleviate the suffering and loss of scores of families in every corner of our country," Christie said in a statement. "We look forward to continuing the Commission's efforts and to working with this President to address the approximately 142 deaths a day from drug overdoses in the United States." 
 In Ohio, which has been devastated by the nation's opioid problem, Attorney General Mike DeWine chimed-in with more applause. 
 "Additional resources from the federal government will help hard-hit states like Ohio," said DeWine. 
 DeWine in May filed a lawsuit against five of the biggest drug makers, accusing them of flooding Ohio with prescription painkillers and creating a population of patients physically and psychologically dependent on them. 
 And when those patients can no longer afford or legitimately obtain opioids, they often turn to the street to buy prescription opioids or even heroin, the suit states. 
 But Daniel Raymond of the Harm Reduction Coalition, an advocacy group, said Trump's words "need to be accompanied by actions." 
 "After 200 days into the Trump administration, we have yet to see a clear and consistent strategy emerge," Raymond said. 
 Among other things, Trump stridently supported Republican-backed Obamacare replacement proposals that would have drastically cut Medicaid funds for opioid addiction treatment. And Trump allies like Attorney General Jeff Sessions have proposed dealing with the epidemic by dusting-off tactics from President Ronald Reagan's 1980s so-called war on drugs. 
 "We need to say, as Nancy Reagan said, 'Just say no,'" Sessions said in March. 
 Nearly 35,000 people across America died of heroin or opioid overdoses in 2015, according to the National Institute on Drug Abuse. But a new University of Virginia study released on Monday concluded the mortality rates were 24 percent higher for opioids and 22 percent higher for heroin than had been previously reported. 
 Ali Vitali reported from Washington and Corky Siemaszko reported from New York. 
