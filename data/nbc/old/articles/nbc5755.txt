__HTTP_STATUS_CODE
200
__TITLE
New Peanut Allergy Guidance: Most Kids Should Try Peanuts
__AUTHORS
Maggie Fox
__TIMESTAMP
Jan 5 2017, 3:04 pm ET
__ARTICLE
 Parents worried about peanut allergies now have some surprising new guidance: Give some peanut to your babies. 
 New guidelines out Thursday say that even babies with the highest risk of having a peanut allergy should be given small doses of the nut because it might prevent the allergy from ever developing, 
 Most kids should get a taste of peanut protein by the time they are 6 months old, and they should get regular doses if they dont have an allergic reaction. Those at highest risk should be tested in a specialists office. 
 We actually want all children to have peanut introduced, Dr. Matthew Greenhawt, an allergy specialist at Children's Hospital Of Colorado, told NBC News. 
 There is a window where the immune system isn't going to recognize peanut as dangerous and that we believe happens very, very early." 
 Its a big change from previous guidelines, which recommended that people keep peanuts and peanut products away from their kids completely until they are 3 years old if there is a risk of allergies. 
 The new guidelines from the National Institute of Allergy and Infectious Diseases (NIAID) and other groups follow up on findings that giving peanut to kids early enough in life can train their immune systems so they dont overreact and cause a dangerous allergic reaction. 
 "Living with peanut allergy requires constant vigilance. Preventing the development of peanut allergy will improve and save lives and lower health care costs," NIAID Director Dr. Tony Fauci said in a statement. 
 The new guidelines say most babies can try a little peanut paste or powder  never whole peanuts  at home. 
 High-risk infants are defined as those with severe eczema or an egg allergy. Those babies should be tested at a specialists office when theyre 4 to 6 months old and have started taking solid food. 
 The specialist can watch the infant to make sure nothing dangerous happens when they get a little dose of peanut. The benefits can be enormous. 
 We know that these children with severe eczema and or egg allergy had about an 80 percent reduced chance of developing peanut allergy if peanut was introduced between four to 11 months of life, Greenhawt said. 
 That's a whole generation of children who never have to develop this allergy. 
 Related: Feeding Kids Peanuts Prevents Allergies 
 Even if they have a sensitivity to peanuts, they may not be fully allergic and being fed a small dose of peanut may help prevent the allergy from ever developing, according to the new guidelines. 
 It may scare parents, but it shouldnt, Greenhawt said. 
 We believe the process to be very, very safe, he said. In a study published last year, none of the infants given tiny doses of peanut protein had severe allergic reactions. 
 Moderate-risk infants are those with mild to moderate eczema. They can be fed a little peanut-containing food at home without a doctors help, according to the guidelines being published in the Annals of Allergy, Asthma and Immunology, the Journal of Allergy and Clinical Immunology and elsewhere. 
 Low-risk children with no egg allergy or evidence of eczema can get peanut-containing foods when parents decide but they should get some by the age of 6 months, after they start solid foods. 
 Whole peanuts can choke small children and no child under the age of 4 should get whole peanuts, the groups cautioned. 
 Kelly Schreiner of Marble Hill, Missouri tried it with her daughter Camden, who's 2 years old. Her older brother Zach, now 3, had a peanut allergy he later outgrew and Camden had an egg allergy, so Schreiner was worried. 
 But it worked. Camden got a little peanut in the allergist's office and she never developed a peanut allergy. 
 Related: Peanut Patches can Help Prevent Allergies 
 "It's important to me as a mom so that my kids can go through life without having to constantly watch what they eat," Schreiner told NBC News. "They can eat anything. They can eat peanut butter. We don't have to constantly be reading labels." 
 The new guidelines say family history is not a risk factor. Just because a child has a sibling or other relative with a peanut allergy does not mean he or she is at high risk, the NIAID and other groups said. 
 Whats important is to give a little bit to the babies and watch them carefully for a reaction, according to the guidelines. 
 You're looking for signs that your child didn't tolerate the food, Greenhawt said. 
 It can be anything to a rash to vomiting, or something more severe such as coughing, wheezing vomiting, looking lethargic, looking withdrawn, or going into shock, he added. You need to be on the lookout just like you would like when youre introducing any food. 
 The experts on allergies say that, based on earlier studies, there are not likely to be many babies that young having a reaction to peanut. 
 About 5 percent of Americans have food allergies of some sort, and 1 to 2 percent have peanut allergies. Kids allergic to peanuts can have a life-threatening anaphylactic reaction to even a tiny bit of peanut dust or food containing peanuts. 
 And, for reasons no one really understands, peanut allergies have become more common. 
 Peanut allergy has literally become an epidemic in recent years, and now we have a clear road map to prevent many new cases moving forward,said Dr. Stephen Tilles, president of the American College of Allergy, Asthma and Immunology (ACAAI). 
