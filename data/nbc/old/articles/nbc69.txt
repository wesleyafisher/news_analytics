__HTTP_STATUS_CODE
200
__TITLE
Congressional Black Caucus Pleads for More Help for Virgin Islands
__AUTHORS
Donna Owens
__TIMESTAMP
Oct 12 2017, 9:10 pm ET
__ARTICLE
 WASHINGTON  When Stacey Plaskett joined Vice President Mike Pence recently in touring the U.S. Virgin Islands to assess the damage caused by hurricanes Irma and Maria, among the many changes she noticed was the "brown" and barren landscape  shocking in a tourism destination renowned for its lush foliage, palm trees and crystal-clear waters. 
 Yet there's a bit of green returning to St. Croix, St. Thomas and St. John, Plaskett, the Virgin Islands' non-voting delegate in Congress, said Thursday  a hopeful sign, perhaps, from Mother Nature. 
 Plaskett joined Rep. Cedric Richmond, D-La., chairman of the Congressional Black Caucus, and caucus members Val Demings, D-Fla., Barbara Lee, D-Calif., Sheila Jackson Lee, D-Texas, Gregory Meeks, D-N.Y., Karen Bass, D-Calif., and Yvette Clarke, D-N.Y., at a news conference to stress that it's time to give the roughly 100,000 residents of the U.S. territory renewed hope, too, following back-to-back Category 5 hurricanes that hit a month ago. 
 The damage has been "profound," said Plaskett, a co-chairwoman of the Congressional Caribbean Caucus, along with Clarke and Rep. Maxine Waters, D-Calif. 
 "Three separate islands require our attention," Clarke said. "The federal response has been too slow." 
 The CBC's plea came on the same day the House overwhelmingly approved $36.5 billion in emergency disaster relief, flood insurance and emergency wildfire funding amid a rash of deadly weather events in Texas, Florida, California, the Virgin Islands and Puerto Rico. 
 No Democrat opposed the package; more than 60 Republicans voted against it. The White House said in a statement Thursday, President Donald Trump is pleased with the vote and will continue to work with Congress to provide the resources necessary to recover. 
 The vote dovetailed with delivery of a letter from Virgin Islands Governor Kenneth E. Mapp, who wrote to congressional leaders seeking recovery assistance. 
 Related: Trump Attacks Puerto Rico, Threatens to Pull Emergency Responders 
 "After a comprehensive and thoughtful examination of our damages and recovery requirements, we believe that disaster assistance and support of approximately $5.5 billion would enable the Territory to address the most essential needs of American citizens residing in the US Virgin Islands," Mapp wrote. 
 Irma and Maria hit just 12 days apart, bringing winds above 180 mph and more than 20 inches of rain. 
 The storms "turned our green hillsides nearly black, stripped away leaves and ripped massive trees out by their roots," Mapp wrote. 
 Many structures in the typically scenic islands are gone, reduced to rubble by the storms. Power lines were strewn across roads, utility poles snapped in half like matchsticks, boats lay sunken in the harbors, many thousands of homes stood heavily damaged or destroyed, and some major road systems were impassable. 
 Both Virgin Island hospitals, four schools, two fire stations, a police station and much of the infrastructure have been destroyed, Mapp wrote. 
 Moreover, the terminals at Cyril E. King and Henry Rohlsen airports are heavily damaged. Many government offices are unusable, facilities of the judicial and legislative branches have been damaged, and government operations have been drastically affected. 
 There is no power on St. John and very limited power on St. Thomas and St. Croix. Virtually all of the power distribution infrastructure was destroyed, most telephone lines are down, and cell towers were destroyed. Most homes, hotels and resorts were destroyed or substantially damaged. There is also food scarcity. 
 "We are at the U.S. government's mercy," Mapp wrote. 
 Plaskett said that given the damage, she's not certain that $5 billion will be enough. 
 "I am pleased that my colleagues in Congress have appropriated [monies] ... but let it be known that far more will be necessary for relief," said Plaskett, who indicated that she would like to have further discussions with Mapp. 
 Richmond, the black caucus chairman, said that members would like to tour the territory soon and that he believed other top officials should visit, as well. 
 "We need the speaker to go there," he said, referring to House Speaker Paul Ryan, R-Wis. "We need the president to go there." 
 Related: Black Caucus Vows to 'Root Out Racism' in Federal Policy, White House 
 In addition to the physical destruction, the Virgin Islands' economy has ground to a near-standstill. Officials have calculated the economic losses to key industries, including tourism, at more than $2 billion. 
 Few businesses are operating, officials said, and those that have reopened have done so at highly reduced levels compared to where they were before the storms. 
 Many private-sector employees can't return to work, and the damage will subject governments to unsustainable cash shortfalls that Mapp said "will have a serious impact on the delivery of critical health and safety services." 
 While there's been widespread criticism that the Trump administration failed to respond with urgency, Trump met with Mapp, although he hasn't toured the Virgin Islands. The two met last week aboard the USS Kearsarge. 
 Trump was accompanied by first lady Melania Trump and Chief of Staff John Kelly. Federal Emergency Management Agency Administrator William "Brock" Long, Puerto Rico Gov. Ricardo Rossell and Puerto Rico's delegate in Congress, Jenniffer Gonzlez-Coln, were also in attendance. 
 In a statement, Mapp called the exchange "very productive," adding that he didn't know of "any country on the planet ... today that can respond to a disaster like ... the United States of America." 
 Follow NBCBLK on Facebook, Twitter & Instagram  
