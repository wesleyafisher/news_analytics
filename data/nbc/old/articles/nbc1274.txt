__HTTP_STATUS_CODE
200
__TITLE
Judge Limits Data Feds Can Get About Anti-Trump Website DisruptJ20
__AUTHORS
Alex Johnson
__TIMESTAMP
Oct 10 2017, 8:22 pm ET
__ARTICLE
 A Washington, D.C., judge has limited the data the Justice Department can have about an anti-Trump website, saying Tuesday that he wanted to protect innocent users' First Amendment rights. 
 D.C Superior Court Chief Judge Robert Morin ordered DreamHost, a web hosting company in Los Angeles, to hand over for review all information it has about DisruptJ20, a group of activists who organized protests that led to the arrests of more than 200 people during Donald Trump's inauguration as president in January. 
 Morin said DreamHost must include all information that could identify DreamHost subscribers directly associated with the DisruptJ20.org web account during the months leading up to Jan. 20. But he said the company can redact information that could help identify individual internet users who simply used the site. 
 "While the government has the right to execute its warrant, it does not have the right to rummage through the information contained on DreamHost's website and discover the identity of, or access communications by, individuals not participating in alleged criminal activity, particularly those persons who were engaging in protected First Amendment activities," Morin wrote. 
 The legal battle began when the government filed a warrant in July asking for information on DreamHost's servers about all visitors to DisruptJ20. DreamHost strongly resisted the request, arguing that it would have allowed investigators to identify hundreds of thousands of people who simply visited or merely clicked on the site but had no connection to the Jan. 20 protests. 
 In August, the government backpedaled after the warrant was slammed as a broad violation of free speech protections. It said it was interested only in information "singularly focused on criminal activity," and it sought permission to amend the warrant. 
 In his order Tuesday, Morin set out a detailed procedure for government access to the data. 
 Morin said DreamHost must turn over the redacted data for review by an expert or experts he approves. Prosecutors will be barred from reviewing the materials until Morin signs off on a report he ordered the government to file explaining how it plans to search the data. 
 Only then may prosecutors start going through the dataset  and even then, they must file individual explanations justifying how every piece of information they want to use is relevant to their investigation of the Jan. 20 protests. 
 Christopher Ghazarian, DreamHost's general counsel, called the order "a far cry from the original warrant we received in July." 
 "We're happy to see significant changes that will protect the constitutional rights of innocent internet users," he said. 
 Paul Alan Levy, a lawyer for Public Citizen, a nonprofit activist group that sought to intervene in the case on behalf of five anonymous people, also welcomed the order, calling it "a victory for the First Amendment right of people to plan a protest without worrying about having their identities disclosed to the government." 
 But Levy said in a statement that it was "disappointing" that Morin approved searches of the redacted emails at all, "even though the government never showed that it had a good reason to look at those emails." 
 The U.S. attorney's office didn't respond to a request for comment. It has previously said it wouldn't comment on pending cases. 
 The case is separate from but related to another one involving DisruptJ20, in which the Justice Department wants Facebook to turn over information about three DisruptJ20-related accounts, including its main Facebook page, which has since been renamed Resist This. 
