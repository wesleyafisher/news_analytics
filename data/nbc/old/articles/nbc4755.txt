__HTTP_STATUS_CODE
200
__TITLE
Trump Admin. Makes Gains on ISIS, but Lack of Syria Policy Raises Concern
__AUTHORS
Vivian Salama
__TIMESTAMP
Oct 16 2017, 5:21 pm ET
__ARTICLE
 WASHINGTON The Trump administrations efforts to recapture the ISIS-held Syrian city of Raqqa, the groups de-facto capital, are making enough headway that the operation could wrap up as soon as this month, U.S. officials tell NBC News. 
 But President Donald Trumps administration has yet to reveal its broader Syria policy, raising concern that the country may be left vulnerable to various regional powers after Raqqa falls. 
 Three U.S. officials involved in Syria policy discussions said efforts to shape the nascent administrations broader strategy for the war-torn country have been troubled. National security advisers on one of the White Houses policy coordination committees recently embarked on a roadshow to various government agencies to sell the presidents views. 
 But that effort was ineffective, forcing national security advisers to revise their pitch and delay the delivery of any official recommendations to the White House, the officials said. 
 Among the more contentious issues is how to achieve post-combat stabilization in eastern Syria, purging it of fighters from the Islamic State group but also securing it from Iranian fighters and forces loyal to Syrian President Bashar al-Assad who are looking to take control of the region. The administration is also at odds over what a political transition in Syria would look like. 
 A few of the administrations more conservative members, including the U.S. Ambassador to the U.N., Nikki Haley, favor a plan that leads to ousting al-Assad, but moderate factions of the government feel he is the only thing keeping the country from turning into a black hole engulfing much of the region. 
 While many of the administrations grievances against Iran focus on its support of the Assad regime and of Hezbollah fighters in Syria, advisers have been warning that an Iran-only approach to the Syria conflict could yield dire consequences. 
 One U.S. official described the administrations view of the Syria conflict as Iran-focused, a view many advisers feel underestimates the complexity of the brutal conflict, now entering its seventh year. 
 Military and diplomatic advisers are pressing the White House to take heed of deteriorating relations with Turkey, which, coupled with the opposing influence of Iran and Russia, could deal a drastic blow to efforts to stabilize Syria after the Raqqa operation is complete. 
 The White House did not respond to requests for comment for this story. 
 The U.S. and Turkey recently became embroiled in a consular dispute, mutually scaling back visa services. The American mission in Ankara said last week that it had suspended "non-immigrant visa services in order to "reassess" Turkey's commitment to staff security after the arrest of a Turkish employee at the U.S. Consulate in Istanbul. 
 The Turkish embassy in Washington then responded by suspending "all visa services." 
 The American ambassador to Turkey, John Bass, said Monday that the U.S. did not take the visa decision lightly. "Its a decision we took with great sadness," he said. "We realize that the suspension of visa services will inconvenience people." 
 Losing Turkey as a friend and an ally has significant strategic implications. The U.S. conducts strikes on the Islamic State group daily from bases in Turkey and elsewhere in the region. Even after the militant group is defeated in Raqqa, operations are likely to continue to defeat rogue militant fighters with the Islamic State group or any of the other factions fighting in Syria. 
 One official familiar with the policy planning noted that the consular dispute will complicate efforts to get troops, humanitarian workers and supplies to displaced Syrians through the traditional routes in Turkey. 
 Turkish soldiers recently launched reconnaissance missions in the war-torn Syrian city of Idlib ahead of the establishment of a "de-escalation zone" there, but some see the move as an effort by Turkey to block allied Kurdish forces from reaching the city. 
 "Turkey has more to gain by pressuring us," said Anthony H. Cordesman, a military strategy consultant with the Center for Strategic and International Studies. "It can now play the Russian card against us and is doing it already." 
 Turkey, a NATO ally, considers the main Syrian Kurdish force, the YPG, as terrorists because of their links to Kurdish insurgents in Turkey, whereas Washington considers the YPG its most effective partner on the Syrian battlefield. 
 Earlier this year, U.S. special operations forces were deployed to Tal-Abyad, a Kurdish-controlled city near the Turkish border, to counter a buildup of Turkish forces amid rising tensions. 
 "Its as if the U.S. is on the same team with the Turks and Syrian Kurds, but has also had to referee them," said Nicholas A. Heras, a Middle East security fellow at the Center for a New American Security. "The Trump administration would like to have Turkey on its side, but has decided that if Turkey comes, it comes. If it doesnt, theyll have to manage it alone." 
 Military leaders, frustrated by what they considered micromanagement by the previous administration, have argued for greater leeway on the battlefield in Iraq and Syria. Trump, meanwhile, has expressed reluctance to ramp up troop levels in any of the conflicts hes inherited. 
 While Trump was praised for a decision in April to strike a Syrian government airfield  punishment for Assads use of chemical weapons against civilians  his administration faces the same thicket of problems as the Obama administration did when trying to find a solution in Syria. 
 "The more you come close to defeating ISIS in Raqqa and elsewhere, the more it exposes the fact that we dont have any clear strategy for Iraq and Syria or for dealing with Turkey or the growing Russian influence in the region," Cordesman said. "We may be on the edge of destroying the physical caliphate, but the problems that remain are great indeed." 
