__HTTP_STATUS_CODE
200
__TITLE
It Was Going to Be a Great Year for Texas Farmers  Then Harvey Hit
__AUTHORS
Dartunorro Clark
__TIMESTAMP
Sep 3 2017, 11:01 pm ET
__ARTICLE
 BAY CITY, Texas  This was shaping up to be a record year for cotton farmers in Texas. 
 In 2016, farmers were lucky to harvest one bale of cotton per acre of the profitable crop. A year later, with perfect weather conditions during harvesting season, farmers would get three bales, maybe even four, per acre  the most in more than a decade. 
 But because of Hurricane Harvey's far-reaching devastation  from dense urban centers to outlying rural areas  farmers in several counties in the state along the Gulf Coast were left in soggy ruin. 
 "It was going to be good," said Ginger Beyer, 40, who farms in Bay City with her husband, Richard, 39. "Everyone was super pumped-up for redemption for last year, then the storm blew up out of nowhere." 
 The coastal strip affected by the storm was the second-biggest cotton-producing area in a state that is the No. 1 cotton producer in the country. 
 Sweeping farmlands that grow lush fields of cotton resembling freshly fallen snow, among other crucial crops, now lie nearly paralyzed  a stark illustration of the impact Harvey has had on one of the state's biggest economic drivers. 
 But farmers who are now left picking up the pieces say this is a way of life farmers are going have to deal with going forward. 
 "There's really very little things that you can do," said Russell Boening, president of the Texas Farm Bureau. "We're just in a holding pattern and a wait-and-see position, as far as we know the losses are going to be substantial." 
 Hundreds of thousands of bales of cotton crops were slashed from the almost-record year, the bureau said, but there's still not a clear picture of the complete economic loss. 
 The Beyers have 3,000 areas of farmland scattered here across Matagorda County, which has a population of more than 36,000 residents and is more than an hour away from Houston. 
 Their farms got some of the worst flooding they've ever seen: 25 to 30 inches of rain. The downpour and fierce winds came Aug. 25 and waxed and waned over the next few days. 
 When the waters receded, the farming family saw roughly half their 1,300 acres of cotton ruined because of the storm. They were able to harvest their rice, corn and milo, a variety of sorghum. 
 "We'll go in and harvest what we can," said Richard Beyer, adding there may be hundreds of acres of soybeans and cotton still salvageable. 
 "There's so much we can't take into consideration like quality and weight loss." 
 However, Richard, who has been working the land for 16 years and comes from a family of farmers, estimates that's a $250,000 loss on the cotton, with the overall financial loss is still unknown. 
 "Farming and production agriculture is a business just like any other business ... it's risk versus reward," he said. 
 "I bet for most of us the numbers are going to grow," he added, standing on one of his ruined cotton fields Saturday afternoon with Ginger. 
 Robert Reed, 64, who has been farming for more than 40 years, said devastating storms like Harvey are a reality that those in agriculture are going to have to adjust to, adding there's little anyone can prepare for. 
 "You feel like there's very little you can do because you feel like a little fish in a big bowl," Reed said, walking on his farm Saturday. 
 There's no fail-safe, he added. Many farmers are at the mercy of the weather  as they have been for millennia. 
 He owns roughly 3,000 acres across the county with his son, Robbie Reed, 39. 
 He also grows rice, corn and milo, all of which he was able to harvest before the storm touched down. He said he had people on his farm working around the clock to harvest what they could. 
 His farm then got swamped by 20 inches of rain, and coupled with his son's farmland, lost more than 400 acres of cotton  worth an estimated half a million dollars. 
 But he believes, like the Beyers, that costs could climb. 
 "It's not anything we've ever seen," Reed said, standing with his wife, Debbie, who is from a family of ranchers. 
 "We've got tropical storms, we've got flooding issues before, but nothing like this." 
 His son's house on the farm, where he lives with his wife, Stephanie Reed, 37, and two children, saw 4 feet of flood waters, causing massive damage to the home. 
 The Reeds have insurance on their properties, but when a loss adjuster from their insurance company visited them Saturday, there were more questions than answers. And now, like many affected, they have to wait to see what's covered and what isn't. 
 "It's not gonna be easy," said Debbie Reed, 64. "You pick up the pieces  sweep them up  and you keep going." 
 The Beyers also have insurance, but Richard said they might take a hit because he did not have 100 percent coverage on the crops, which was capped at 75 percent. 
 "I don't think the reality has set in," said Ginger Beyer. "People are cleaning up, not getting much sleep." 
 However, she said, the storm has brought together the farming community  and the community at-large  in ways she's never seen. 
 "Was it devastating? Absolutely," she said. "But I've seen so much good out of people. It devastated everyone, but you can be a victim or can see a silver lining." 
 She and her husband said their faith has guided them when storms like Harvey hits. 
 "This is life," she said. "Storms are going to come through and knock you down. You just got to get back up." 
