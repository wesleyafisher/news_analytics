__HTTP_STATUS_CODE
200
__TITLE
Asian-American Groups Start Mental Health Program for DACA Recipients
__AUTHORS
Monica Luhar
__TIMESTAMP
Oct 10 2017, 1:57 pm ET
__ARTICLE
 A group of Asian-American and Pacific Islander-serving organizations announced the creation of a mental health program for recipients of Deferred Action for Childhood Arrivals (DACA) and their families Thursday, a month after the White House announced that it was ending the program. 
 Ten mental health service providers from the Asian Pacific Policy & Planning Council (A3PCON)  a Los Angeles-based consortium of Asian-American and Pacific Islander (AAPI) groups  said they will provide free counseling, case management, and other mental health services through the DACA Mental Health Project. 
 The groups said they are providing the services in 12 languages: Bangla, Cantonese, Hindi, Japanese, Khmer, Korean, Mandarin, Tagalog, Thai, Urdu, Vietnamese, and English. 
 Connie Chung Joe, co-chair of A3PCON, said it was important for the groups to say they would continue to provide services during a time of uncertainty that has seen some clients shy away from seeking help. 
 RELATED: Asian-American Advocates Blast Trump Decision to End DACA Program 
 We wanted to make it particularly clear that we would find a way to serve DACA recipients regardless of whatever Medi-Cal qualifications or status, without having to [worry] about getting the government involved, she said. 
 The scheduled termination of DACA, which shields young undocumented immigrants from deportation if they meet certain requirements, and anti-immigrant rhetoric have made much of the groups clientele wary of receiving services while increasing stress, Joe explained. 
 She added that some clients might be hesitant about enrolling in government-funded programs because of the fear attached to sharing information. 
 RELATED: Analysis: DACA Boosts Young Immigrants Well-Being, Mental Health 
 Shikha Bhatnagar  executive director of the South Asian Network, one of the collaborating organizations  has also noticed clients dropping out of services, especially when it comes to the renewal of health insurance. 
 They are too afraid to come in, Bhatnagar said. They feel their information might be in jeopardy, and they might be deported. 
 Manjusha Kulkarni, A3PCONs executive director, said that DACA has enabled thousands of young people to come out of the shadows and be integrated into society since its creation in 2012, though there has been a stigma in the AAPI community regarding coming forward and applying for protections. 
 Asians made up 10 percent of the population potentially eligible for DACA, according to a September 2014 report from the nonprofit Migration Policy Institute. But in a 2016 analysis, the institute found that application rates for youth born in Asia were generally very low. 
 According to 2016 federal immigration statistics, four of the 24 top countries of origin for DACA recipients are in Asia  South Korea, the Philippines, India, and Pakistan. 
 RELATED: What Is DACA? Heres What You Need to Know About the Program Trump Is Ending 
 Kulkarni attributes the low application rates in part due to the model minority myth. 
 I do think that this is one other place we see the model minority myth hurting the community because a lot of people unfortunately buy into it ... and it makes it harder for people who dont have status to come out of the shadows and to say, hey, you know what, I had to come here [because] there was political strife in my homeland, she said. 
 The deadline to apply for a two-year renewal of DACA was Oct. 5 for those with permits set to expire before March 5, 2018. 
 A United States Citizenship and Immigrant Services spokesperson told NBC News that as of Oct. 6, approximately 122,000 out of 154,000 DACA recipients who were eligible for renewals had applied. 
 Joe said A3PCONs DACA Mental Health Project is also designed to provide more flexible services and help those who might not have a diagnosed medical health condition but want to speak to a professional due to stress and anxiety. 
 Kulkarni said that families are facing a lot of fear and uncertainty, noting that they can see that the Trump administration has been hostile to immigrants. 
 That hostility can increase levels of anxiety, panic attacks, and depression, she added. 
 They know theyre getting the message loud and clear that they are unwelcome here, so I think this is a very difficult time, and thats why we launched our [mental health] project, Kulkarni said. 
 Follow NBC Asian America on Facebook, Twitter, Instagram and Tumblr. 
