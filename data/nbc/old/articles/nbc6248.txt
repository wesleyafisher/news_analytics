__HTTP_STATUS_CODE
200
__TITLE
The New iPhone Will Have a Curved Screen 
__AUTHORS
Berkeley Lovelace, Jr., Special to CNBC.com
__TIMESTAMP
Feb 28 2017, 9:25 am ET
__ARTICLE
 One model of Apple's next iPhone coming out this year will adopt a flexible display, according to reports. 
 The Cupertino, California-based company has ordered sufficient components to enable mass production, the Wall Street Journal reported, citing people familiar with the matter. The tech giant had been studying flexible OLED displays and asked suppliers for prototypes late last year, the Journal said. 
 CNBC has reached out to Apple for comment. 
 The introduction of a curved display will follow suit with rival Samsung, which has a curved screen on some of its models like the Samsung Galaxy S6 Edge. 
 Analysts have looked toward the much-anticipated September release of the iPhone 8  the 10th anniversary of the release of the iconic device  which is expected to be a significant upgrade. 
