__HTTP_STATUS_CODE
200
__TITLE
Body Clock Researchers Win Nobel Prize
__AUTHORS
Maggie Fox
__TIMESTAMP
Oct 2 2017, 11:53 am ET
__ARTICLE
 Three American researchers won the Nobel Prize in Medicine on Monday for discovering what makes us tick  the molecular circuitry of the body clocks that control life on Earth. 
 Their work, done over decades, helps explain how life adapts to the 24-hour cycle of day, and also how diseases such as cancer arise in the cells. Their discoveries form the basis for countless other research initiatives. 
 Jeffrey C. Hall, Michael Rosbash and Michael W. Young were able to peek inside our biological clock and elucidate its inner workings, the Nobel committee said in its statement. 
 Their discoveries explain how plants, animals and humans adapt their biological rhythm so that it is synchronized with the Earth's revolutions. 
 Related: Gene Mutation Turns People into Martians 
 As often happens, the researchers were awakened with a surprise phone call. 
 "This really did take me by surprise and I really had trouble getting my shoes on this morning," a tie-less, somewhat rumpled Young told a news conference Monday morning. 
 "The phone call at 5:10 this morning destroyed my circadian rhythms by waking me up," Rosbash, 73, told a separate news conference. 
 And they admit they don't quite know much about the most basic effect of clock genes  sleep. 
 "The purpose of sleep is something that remains quite mysterious," Young said. "While we dont know what its for, we know that its important." 
 The Nobel committee keeps its choices secret until the announcement is made each October. Researchers usually suspect they are on the short list, but never know if or when they may be chosen. 
 The prizes often go to teams working together, and never to more than three living people. 
 Most living things have a circadian rhythm. The word comes from Latin terms: circa, for around and dies for day. 
 Hall and Rosbash, both then at Brandeis University in Massachusetts, and Young of Rockefeller University did research that found an elaborate internal clock of genes and the cellular functions that DNA controls working together in much the same way the gears and springs in an old-fashioned timepiece do, with interconnected functions controlling one another. 
 With exquisite precision, our inner clock adapts our physiology to the dramatically different phases of the day, the Nobel committee, which awards the $1.1 million prizes annually, said. 
 The clock regulates critical functions such as behavior, hormone levels, sleep, body temperature and metabolism. 
 And it all started with fruit flies. The three men isolated the period gene in fruit flies, which share many genes with humans, including the period gene. 
 Related: Cancer Cells Hijack Body Clock 
 This is really a testament to fruit flies," Robash said. 
 In 1994 Young discovered a second clock gene, which he named timeless, that helps control the period genes activity. He found a third gene that he named doubletime that helps adjust the 24-hour oscillation of the period gene. 
 Related: Gene Mutation Gives 'Young at Heart' New Meaning 
 Nobel prizes are usually chosen because the work of an individual or a team has led to many other discoveries. Researchers have learned that body clocks are important to health in a variety of ways. 
 Our well-being is affected when there is a temporary mismatch between our external environment and this internal biological clock, for example when we travel across several time zones and experience jet lag, the Nobel committee noted. 
 There are also indications that chronic misalignment between our lifestyle and the rhythm dictated by our inner timekeeper is associated with increased risk for various diseases. 
 Just last April, Youngs team found another gene called CRY1 that helps control whether people are night owls. 
 The clock is complex and each organ is different. The brain, Young said, can be reset by sunlight, while the liver is affected by cues such as meals. 
 But it affects just about every tissue and organ. "The clock, the mechanism we discovered, governs at least half of gene expression," Rosbash said. "That, of course, is why everything from endocrinology, behavior, metabolism  everything falls under the broad umbrella of circadian rhythms." 
