__HTTP_STATUS_CODE
200
__TITLE
Puerto Rico Takes Steps to Boost Electric Power by Next Week
__AUTHORS
Suzanne Gamboa
__TIMESTAMP
Oct 17 2017, 3:12 pm ET
__ARTICLE
 WASHINGTON  The Army Corps of Engineers hopes to have in place by next week  more than a month after Hurricane Maria made landfall in Puerto Rico  two 25-megawatt generators at a plant in San Juan, Puerto Rico, to help stabilize electricity there. 
 The turbines were received Oct. 13, and the Army Corps wants the generators to be operational by Oct. 25, spokeswoman Catalina Carrasco said Tuesday. 
 Carrasco said preparations to install the turbines at the Palo Seco Power Plant in San Juan were slowed by the continuous rains that Puerto Rico has been having. Once installed they will allow for customers to have consistent power while repairs are made. 
 The generators should boost the share of customers with power from the grid from 13.7 percent to 30 percent, Fernando Padilla, project manager for the Corps told El Nuevo Da newspaper in Puerto Rico. The hope is to also make it more stable. 
 Wednesday will mark one month since the hurricane began charging through the island, causing massive devastation to the island's power grid and infrastructure. As of Tuesday, 17.7 percent of the island's electric utility customers had power from the grid, though it's still not reliable. Others in Puerto Rico, though not all, have power through diesel-fueled generators, but only as long as the diesel lasts. 
 RELATED: Puerto Rico in Crisis: A Race Against Time to Evacuate the Infirm  
 Officials want to provide stable power to hospitals and water and wastewater treatment plants in the northern part of San Juan and then connect lines in the southern part of the capital. The Corps told El Nuevo Da it also is important to get power to pharmaceutical companies on the island because they are a key part of its economy. 
 The Corps awarded a $35.1 million contract to Weston Solutions based in West Chester, Pennsylvania, for the work. 
 Also, the Corps awarded a $240 million limited competition contract to Fluor Corp. based in Irving, Texas. The company will provide personnel, technical expertise and equipment for work on the transmission and distribution lines. According to the company's website, it previously built a coal-fired power plant in Guayama, Puerto Rico, that provided power to 9 percent of the island's electrical capacity. 
 Puerto Rico Gov. Ricardo Rossell has set a goal of re-establishing electricity to 30 percent of the island by the end of the month and 95 percent by Dec. 15. 
 Carrasco said that as of Tuesday, the Corps has received $577 million from the Federal Emergency Management Agency for power restoration, and that the figure is likely to rise. 
 Follow NBC Latino on Facebook, Twitter and Instagram. 
