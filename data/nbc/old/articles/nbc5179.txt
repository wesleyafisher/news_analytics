__HTTP_STATUS_CODE
200
__TITLE
9-Year-Old Trained to Use Heroin Overdose Antidote
__AUTHORS
Gadi Schwartz
Elisha Fieldstadt
__TIMESTAMP
Jun 5 2016, 9:58 pm ET
__ARTICLE
 Most kids play with stuffed animals, but a 9-year-old girl in Kentucky uses her dolls to practice a technique that could one day save a life. 
 Audrey Stepp's 26-year-old brother, Sammy, has been struggling with heroin addiction since before she was born. 
 "Audrey just gravitates toward him," said Sammy's and Audrey's mother, Jennifer Punkin-Stepp. 
 Punkin-Stepp said Audrey overheard her one day talking about Naloxone training and how the drug could save Sammy if he overdosed. Naloxone, now sold over the counter, is used to reverse the effect of opiates used in surgery and can also block the life-threatening effects of a narcotic (opioid) overdose, according to the National Library of Medicine. 
 Audrey insisted that she learn to administer the drug in case her big brother ever suffered a heroin overdose. That meant she needed to practice filling and using the needle that delivers the drug. 
 Audrey recently asked her mother what the word "sober" means, but unlike many 9-year-olds, she knows what an overdose looks like. 
 "Their fingernails and their lips would be blue, and they wouldn't wake up," Audrey told NBC News. 
 In case she ever witnesses such a horrific scene, Audrey does dry runs of filling up a syringe and injecting a stuffed animal. 
 Some say antidotes like Naloxone (also known as Narcan) enable drug users, but Dr. Mina Kalfas, an addiction specialist at the Christ Hospital Outpatient Center in Fort Wright, Kentucky, sees it differently. 
 "Dead people can't recover," he said. 
 Opioid-related deaths are on the rise. The most recent data show that 28,000 people in the United States died from opioid overdoses in 2014, more than in any other year on record, according to the Centers for Disease Control and Prevention. 
 "If a kid could save somebody, why not? Instead of having the nightmare of watching somebody die," Punkin-Stepp said. 
