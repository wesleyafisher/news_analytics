__HTTP_STATUS_CODE
200
__TITLE
Clinton Campaign Chairman Podesta to Answer Congressional Questions
__AUTHORS
Andrea Mitchell
__TIMESTAMP
Jun 22 2017, 9:49 pm ET
__ARTICLE
 John Podesta, Hillary Clinton's presidential campaign chairman, will answer questions next week from the House Intelligence Committee, which is investigating alleged Russian interference in last year's presidential campaign, former Clinton campaign officials told NBC News Thursday. 
 Podesta will be interviewed in closed session, with the public and the news media not allowed in, according to the former staffers who spoke on condition of anonymity. 
 Ten years of mail on Podesta's Gmail account were published by WikiLeaks during the closing months of the campaign, a key part of the hacking attack being investigated by congressional committees and special counsel Robert Mueller. 
 U.S. intelligence agencies have concluded that Russia was behind the hack. 
 President Trump himself may be under investigation by Mueller's team for obstruction of justice in relation to the Russia probe. Sources told NBC News earlier this month that the president was being looked at as part of the special counsel's investigation  and Trump himself seemed to confirm that with a tweet, before his lawyer took to the Sunday morning politics shows to deny it. 
