__HTTP_STATUS_CODE
200
__TITLE
Why Youll Still Be Pumping Gas Even as Electric Cars Take Over
__AUTHORS
Paul A. Eisenstein
__TIMESTAMP
Sep 21 2017, 11:57 am ET
__ARTICLE
 With one automaker after another announcing plans to go 100 percent electrified, you might think that your corner gas station will be shutting down in a couple of years. But while battery-powered vehicles will eventually replace high-mileage diesels, that doesnt mean the internal combustion engine is fading away just yet. The reality, industry leaders stress, is that pure battery-electric models, such as the Chevrolet Bolt EV and the Nissan Leaf, will remain relatively niche players for at least the next decade. 
 Though an increasing number of countries, from Norway and India to France, China, and even Germany, have enacted or are considering gas and diesel bans, there is no "silver bullet that can both solve our environmental issues and meet the needs of all drivers, cautioned Dieter Zetsche, the CEO of Daimler. 
 Diesels have clearly taken a hit in the wake of the Volkswagen emissions scandal, with Germanys largest automaker acknowledging that it used a so-called defeat device to illegally pass emissions tests while actually producing up to 40 times more smog-causing pollutants than the law allows with two of its diesel engines. In recent months, several other automakers have been accused of rigging their own oil-burners. 
 But while diesel sales have slipped in Europe, according to data collected by consulting firm IHS Market, the technology is still found in about 47 percent of the new vehicles being sold on the Continent this year. 
 Diesel sales have taken a sharper plunge in the U.S.; VW will no longer offer the technology which, only three years ago, accounted for more than a fifth of its sales. Mercedes-Benz has pulled diesels from the American market indefinitely, and Fiat Chryslers offerings are on hold as the carmaker fights claims by the EPA that it also used a defeat device to rig emissions tests. But several other manufacturers, including Mazda and GM, are adding new diesel models. 
 There are a number of reasons why: 
 Even conventional gas-powered vehicles are becoming so clean and energy efficient that it has become harder and harder to justify the added cost of going 100 percent electric. 
 That could change in the years ahead. Were waiting for the big battery breakthrough, said Brian Bolain, the general manager for marketing at Lexus. 
 Lexus parent, Toyota, is dropping hints that it just might have that breakthrough in the works. As part of a new partnership with Mazda, Toyota is planning to launch a new line of battery-electric vehicles in 2021. And they just might use an alternative to todays most advanced technology, something called the solid state battery. 
 That technology is expected to be lighter, smaller, less expensive, and far more energy dense  another way of saying it could boost range to 400 miles or more between charges. And some experts anticipate that solid state batteries will reduce charging times to as little as 10 minutes, making them a more direct alternative to the internal combustion engine. 
 Related: Just How Climate Friendly Are Electric Cars? 
 Whether those new batteries will actually deliver as promised  and whether they will be ready for prime time anytime soon  remains uncertain. 
 And so, for at least the time being, gas and diesel will remain a part of the automotive mix in order to give drivers the kind of individual mobility they desire. 
