__HTTP_STATUS_CODE
200
__TITLE
Meet the Press Launches New Film Festival with AFI
__AUTHORS
__TIMESTAMP
Aug 2 2017, 8:55 am ET
__ARTICLE
 NBC News Meet the Press with Chuck Todd hits the big screen for the first time this fall with the launch of the Meet the Press Film Festival in Collaboration With AFI. 
 This new event will feature contemporary documentaries  40 minutes in length or less  about policy and people with an emphasis on untold American stories found far from Washington and New York, telling stories from a diversity of perspective (meaning geographic, ideological, religious, cultural or racial diversity). Films presented in the festival will demonstrate a bold commitment to the subject matter, excellence in cinematic craft, and innovation in storytelling. 
 The groundbreaking November film festival will be held in Washington, D.C. The collaboration pairs the gravity of NBC News political reporting with AFIs revered industry history, celebrating the 70th and 50th anniversaries, respectively, of both historical institutions. The commanding journalism of Meet the Press is a powerful fit with AFI, an institute established in 1967 after President Johnsons mandate to bring together leading artists of the film industry, outstanding educators and young men and women who wish to pursue the 20th-century art form as their life's work. It is also known for its acclaimed Washington, D.C.-area documentary festival. 
 More information on this first-ever film festival will be announced in the coming weeks. Submissions are now being accepted here at AFI. 
 
