__HTTP_STATUS_CODE
200
__TITLE
Hispanic Heritage Month: 12 New Books by Latino Authors  
__AUTHORS
Rigoberto Gonzlez
__TIMESTAMP
Oct 6 2017, 2:58 pm ET
__ARTICLE
 The finalists for the National Book Awards were just announced, and it is a thrill to see the names of two Latinx authors: Chicana writer Erika L. Snchezs I Am Not Your Perfect Mexican Daughter, a finalist in the Young Peoples Literature category, and Cuban American writer Carmen Mara Machados Her Body and Other Parties, in the Fiction category. And congratulations to Peruvian American writer Daniel Alarcns The King is Always Above the People, who was in the longlist for fiction. 
 The awards ceremony takes place November 15 in New York City, and we wish Snchez and Machado much luck. 
 It's Latino Heritage Month, and to celebrate this, below is a list of Latinx writers worth noting for their exceptional storytelling and poetry. These dozen books were recently published by small and independent presses. 
 1. Chantel Acevedo, The Living Infinite (Europa Editions)  
 Acevedos page-turning historical novel is based on the life of Princess Mara Eulalia Francisca de Ass y Margarita de Bourbon, a spirited woman with a sense of adventure that defied the gender norms of the Spanish court in the late 19th century. Determined to find a publisher for her revealing memoir, Eulalia sets out for Cuba and then the U.S. during a period of deep social and political changes that charmed and challenged her sensibilities as a woman in the New World. 
 2 Rosa Alcal, MyOther Tongue (Futurepoem) 
 What are the sounds for but to hear what isnt there, writes Alcal in this moving collection of poems in which a speaker navigates the loss of her mother and the birth of her daughter. The legacy of womanhood, the blessings and dangers of the female body, and the shifting cultural identities from one generation to the next, are a few of the threads that stitch together this dazzling journey through one womans matrilineal story. 
 3. Steven Alvarez, The Codex Mojaodicus (Fence Books) 
 This inventive book of poems inspired by the Aztec codices imagines the pre-Colombian landscape on the Arizona-Mexico border territory, where a new language and intercultural mythologies come alive as AZtexts. Wordplay offers surprise and critique with such surprises as turning Mictln (the valley of the dead) into McTln, a multiethnic southwestern town with bars that invite its patrons to dance the achy breaky corazn. 
 4. Daniel Chacn, The Cholo Tree (Arte Pblico Press) 
 Chacns triumphant tale of acceptance and redemption unfolds in the adolescence of Victor Reyes, a Chicano youth who doubts he can break away from what others expect of him: to end up just another menace of the streets, a cholo. The more Victor tries to prove them wrong, the more he confirms their prejudices. Not until he learns to define himself through his intelligence and imagination does he begin to find a way out of the trappings of a self-fulfilling prophecy. 
 5. Stephanie Elizondo Griest, All the Agents and Saints: Dispatches from the U.S. Borderlands (University of North Carolina Press) 
 Skillfully blending reportage and memoir, renowned travel writer Elizondo Griest discovers powerful parallels between the borders of Canada and Mexico as she observes the lives of the Mohawk tribe and of her beloved Texas community. These troubled zones negotiate poverty, violence, and falls through the cracks of international politics yet somehow its people can still make of them a home. 
 RELATED: Think Mexican, Canadian Borders Are That Different? Not So, Author Finds 
 6. Manuel Paul Lpez, These Days of Candy (Noemi Press) 
 Its the age of surveillance, police militarization and the assault on human rights, therefore Lpezs striking book of poems reads like communications from the war-torn Americas, the speaker a beleaguered traveler seeking perspective and hard-won peace: Tonight I want to inject this anger into the moon and watch it implode, because it witnesses the abusive language of baton and cuff and says nothing. 
 7. Daniel A. Olivas, The King of Lighting Fixtures (University of Arizona Press) 
 The 29 short stories in this collection offer a rich and panoramic view of Los Angeles as experienced by the citys insiders, outsiders and even a few inhabitants of the Netherworld. Olivas, the king of flash fiction, flexes both magical realist and realist muscles in service to each succinct but in the end significant beyond its size narrative. The rollercoaster tour through La La Land comes to a screeching halt with the thought-provoking speculative story The Great Wall. 
 8. Joseph Ros, Shadowboxing (Omnidawn) 
 The hard knocks of a Chicanos journey from the mean streets of the Central California Valley to the cruel halls of academia and then back again to reconcile with family history and painful memory fuel this stellar debut book of poems. Ross Josefo is an endearing gentle giant, a misunderstood genius with a startling relationship to the world: I reach for the dial and discover I can bend the moonlight/ into a glowing, smiling-now or crying-later crescent. 
 9. Benjamin Alire Senz, The Last Cigarette on Earth (Cinco Puntos Press) 
 Reflective and revelatory, this book of poems traces the speakers difficult adjustment to a series of life changes: aging, getting divorced, dealing with depression and addiction, falling in love again, this time with another man. Senz shapes heartache into brave and stunning verse: Im singing. No one can/ kill this song inside of me. Not even me. 
 10. Vickie Vrtiz, Palm Frond With Its Throat Cut (University of Arizona Press) 
 For a young queer Chicana with the will to shape her own destiny, escapes to the big cities like San Francisco and L.A. are invigorating but bittersweetshe must still learn to outrun the terrible stories we must/ not pass on. Vrtiz pays tribute to the fighters and the lovers inside each of us in this fearless and remarkable collection of poems that sings about the dark times, about school. About how La Llorona needs a vacation from that riverbed. 
 11. Vanessa Anglica Villarreal, Beast Meridian (Noemi Press) 
 The power and pulse in Villarreals debut collection of poems comes from the courage to write for the great violences hidden inside women/ For the women hidden inside great violences. Her emotional territory is expansive, reaching far into the mythologies of Mexico and the borderlands, the recesses of family traumas, and the plight of personal demons. Taking linguistic, structural, and expressive risks, this book is necessary to survive this nightmare so American. 
 12. Norman Antonio Zelaya, Orlando and other stories (Pochino Press) 
 The son of Nicaraguan immigrants who settled in San Franciscos Mission District, Zelaya deftly captures the burdens and blessings of a working class community in a gentrifying neighborhood. The art of storytelling is also an act of recovery, and Zelayas portraits of struggle, strength, and resilience give voice to people and experiences that have shaped the citys history and cultural identity. 
 Follow NBC Latino on Facebook, Twitter and Instagram. 
