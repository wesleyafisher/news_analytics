__HTTP_STATUS_CODE
200
__TITLE
U.S. Worried North Korea Will Unleash Cyberattacks
__AUTHORS
Ken Dilanian
Andrea Mitchell
Robert Windrem
__TIMESTAMP
Aug 15 2017, 4:14 pm ET
__ARTICLE
 As tensions rise over North Korea's potential nuclear missile threat, U.S. officials and outside experts are increasingly concerned the rogue regime will respond to international pressure by lashing out with a weapon it has already mastered: cyberattacks that can disable corporate networks, steal money from banks and potentially disrupt critical infrastructure. 
 American intelligence officials have long ranked North Korea as one of the world's more dangerous cyber actors, trailing only Russia, China and Iran among U.S. adversaries in its ability to inflict damage via computer networks. 
 In the best known incident in 2014, U.S. intelligence officials say, North Korean hackers attacked Sony Pictures, destroying corporate computers and disclosing sensitive company data. The U.S. accused North Korea of carrying it out in response to a film lampooning North Korean leader Kim Jong-un. 
 Experts say North Korea could deploy the same techniques to inflict harm not just on one company, but on the American economy. 
 "We've been worried for some time that one of the ways that North Korea can retaliate against further escalation of tensions is via cyber, and particularly attacks against our financial sector," said Dmitri Alperovitch, co-founder of Crowdstrike, a cybersecurity firm. "This is something they have really perfected as an art against South Korea." 
 U.S. law enforcement and homeland security officials said in a June 13 analysis that they believe North Korea is targeting the media, aerospace, financial and critical infrastructure sectors in the United States. 
 Related: U.S. Spy Agencies Agree North Korea Can Fit Nuke on Missile 
 "North Korea is capable of deploying malicious cyber capabilities, as they previously demonstrated in the Sony intrusions," one U.S. intelligence official said. Intelligence officials say that while the U.S. has cyberoffensive capabilities to retaliate, it remains vulnerable to attacks. 
 Some attacks are already underway. In June, the Department of Homeland Security published a warning about a North Korea hacking group it dubbed "Hidden Cobra," referred to by some researchers as "Lazarus." 
 "Since 2009, HIDDEN COBRA actors have leveraged their capabilities to target and compromise a range of victims," the warning said. "Some intrusions have resulted in the exfiltration of data while others have been disruptive in nature." 
 In December, the Hidden Cobra group was named a prime suspect in a theft of $81 million from the Bangladesh central bank. That's part of a string of cyber operations that officials believe were designed to raise money for the regime and its weapons programs. 
 Related: Has It Ever Been This Bad With North Korea? Yes, Actually 
 "They've added cybercrime to their portfolio of illicit activity that they have engaged in to raise money for the regime," said Juan Zarate, an NBC News analyst who is a former top Treasury official, National Security Council staffer and deputy national security advisor. 
 "They're absolutely stealing money through these cyber capabilities," said John Hultquist, who leads the intelligence team at FireEye, a cyber security firm. "They're also stealing defense information. So, a decade of targeting defense contractors worldwide, may have helped in some way in gathering enough information to at least speed up their [nuclear weapons] process." 
 Some researchers have linked North Korea to the WannaCry ransomware attack, an outbreak of malware in May reported to have infected more than 230,000 computers in over 150 countries, making data irretrievable in many cases. But the links are not clear enough for the U.S. to have publicly accused North Korea of involvement, multiple officials and private sector analysts told NBC News. 
 Kim Heung-Kwang, a former North Korean computer expert who defected to the south in 2004, told NBC News in an interview in Seoul that the North has trained thousands of military hackers capable of inflicting damage on South Korean and Western infrastructure. 
 "North Korea is able to use its cyber army to attack South Korea and the U.S.," but the lack of internet connectivity in North Korea makes it hard for the U.S. to retaliate, he said. 
 Related: North Korea Ready to Teach U.S. 'Severe Lesson' 
 FireEye has documented a number of distributed denial-of-service (DDoS) attacks on South Korean organizations and others that appear to be connected to North Korea, the firm told NBC News. 
 For example, the firm said, in March 2011, suspected North Korean actors conducted DDoS attacks on the South Korean government, military infrastructure and a U.S. military base in South Korea. 
 In December 2014, the South Korean government reported that power plants operated by Korea Hydro and Nuclear Power were targeted with wiper malware, potentially linked to North Korean actors. 
 While the attacks were not believed capable of affecting the function of nuclear plants, "they could create a sense of panic by altering the function of non-operational networks, hijacking social media accounts associated with critical infrastructure, or spreading alarming SMS messages during a time of armed conflict," FireEye said. 
 Not every expert is convinced that North Korea poses a major cyber threat. 
 Related: U.N. Imposes Tough New Sanctions on North Korea 
 "It's mostly data disruption," said James Lewis, a specialist at the Center for Strategic and International Studies. "The people who haven't done a good job defending themselves are the ones who get whacked. Companies or agencies that haven't protected their data or backed it up." 
 But Kim Heung-Kwang, who taught computer science in North Korea for 20 years before escaping 13 years ago, said North Korean hackers are working every day to perfect new techniques. 
 "They work hard to survive and do not give up," he said. "If they don't give up, maybe someday they might succeed." 
