__HTTP_STATUS_CODE
200
__TITLE
New Reward Offered in Mysterious Disappearance of New Jersey Man David Gipson Smith
__AUTHORS
Rachael Trost
__TIMESTAMP
Sep 25 2017, 4:35 pm ET
__ARTICLE
 David Gipson Smith's family is hoping a new $5,000 reward will tempt whoever knows anything about the 28-year-old's disappearance to come forward. 
 But as 49 days have passed with no answers, the situation seems increasingly dire. 
 "We're just so afraid something horrible has happened to him," Julie Gipson Smith, David's mother, told Dateline. "Something has happened here. The question is what that is." 
 The last time anyone heard from David was in the early morning hours of Sunday, August 6, while he was on a trip from his family's home in southern New Jersey to a friend's residence in Maryland. Early that morning, his phone was either turned off or died, according to police. 
 When his family could not reach him, and he did not return home or go to work, they called police to report him missing. 
 The circumstances surrounding that day and the days prior weigh heavily on David's loved ones. Especially given David's past. 
 After a rough few years, David was on his way back up, according to family. 
 While attending school for PGA Golf Management at Methodist University on a partial scholarship, David had developed a habit of drinking too much, family members said. He returned home after spending two years at the university. 
 "The pressures of school weighed heavily on him," David's mother Julie told Dateline. "He just needed some time to reset and regroup." 
 Battling his addiction has been an ongoing fight for a number of years, according to Doug Smith, David's father. 
 "He struggled with addiction issues, but he has been doing very well lately," Doug told Dateline. "He completed a program in January, and was living with us. He had just purchased a vehicle, though, and we were talking about the next steps for him, which would be to move out." 
 David had been working almost daily as a maintenance worker at a local golf course at the time, and had saved up enough money to purchase a blue Toyota Camry. He finally seemed like himself again, family members said. 
 "It was wonderful as parents to see the clarity come back," Julie said. "For the first time in a long time, he was excited about life." 
 In early August, he told his parents he was going away for the weekend to visit a friendnear Woodbine, Maryland. Neither his mother nor father knew the woman David was visiting, but because he had been doing so well lately, they trusted everything would be all right. 
 The circumstances of what happened after David arrived in Maryland are hazy. 
 Family members told Dateline that the woman David was visiting allegedly told him he couldn't stay at her residence, so she connected him with another friend whose family had a property in nearby Lisbon, Maryland where he may have been able to stay. 
 The woman said she dropped David at that property on the night of August 5, according to police. She reportedly did not stay there with him, and it's unclear why David did not simply head home. 
 What is known is that David's cellphone stopped receiving texts or calls sometime early the next morning. David's father Doug and his mother Julie pulled his phone records from their family's cell provider, and they said the last activity on David's cell was a call at 4:10 a.m. on the 6th. The call was to the woman on whose family property he was staying. 
 When David's father Doug tried to text him just before 8:00 a.m. that morning, the phone was already off. 
 Then, as several days passed with no word or answer from David, his family grew concerned. 
 The family used Davids cellphone records to find the contact information for the women with whom David last spoke. As soon as they could, they made the hour and a half drive down to the Lisbon property where David was reportedly staying. 
 They said they met with the woman, who offered to show them the property, walked around the nearly 22 acres, then phoned police. 
 Authorities searched the property and surrounding areas on foot and used an infrared camera on a helicopter to search from above. A dive team was brought in to search the pond, but nothing significant was found, according to authorities. 
 Doug and Julie said they then retrieved David's car, which was parked in front of one of the women's homes, and drove it back to New Jersey. It has since been processed by authorities. 
 The Howard County Police Department continues to investigate. Spokeswoman Sherry Llewellyn told Dateline that authorities do not suspect foul play, but are considering every possibility, adding they are concerned for Davids well-being because of the length of his absence and hispast history of drug and alcohol use. 
 She said that both of the women with whom David was last in contact, have cooperated with police. 
 The scenarios of what may have happened play out in the minds of David's loved ones. 
 "Our fear is that something catastrophic in nature happened to David," Doug told Dateline. "Of course, he could have decided to walk out on his own. But with the length of time that has passed, especially given how social media is nowadays, walking away seems improbable." 
 David is hard to miss. At 6'4" tall, he's someone who, when he walks into the room, people notice. 
 His family is now hoping that will help someone, anyone, to notice him and call police. 
 "This is all very, very out of character for him," David's mother Julie told Dateline. "We just want him home." 
 David Gipson Smith is described as 6'4" tall, weighing 230 lbs. with light brown hair and brown eyes. Anyone with information regarding his case is urged to call the Howard County Police Department at (410) 313-STOP. 
 You can also visit the Facebook page "Missing: David Gipson Smith," run by a Smith family friend, for more information. 
 A $5,000 reward, raised from various donations, for information leading to Davids whereabouts is being offered by the family. 
