__HTTP_STATUS_CODE
200
__TITLE
Michigan Judge Stays Order Granting Rapist Parental Rights for Victims Son
__AUTHORS
Associated Press
__TIMESTAMP
Oct 10 2017, 6:11 pm ET
__ARTICLE
 A Michigan judge on Tuesday stayed his order that had granted a convicted sex offender joint legal custody of a child born to a woman who says she was raped by the man when she was 12. 
 The woman's attorney raised an objection late last month after the judge signed an order granting sole physical custody of the boy to the 21-year-old mother and joint legal custody to Christopher Mirasolo, 27, who was ordered to pay child support. 
 Sanilac County Probate Judge Gregory Ross set a hearing for next Tuesday. 
 County prosecutors said in a statement Tuesday that the paternity issue arose when the mother requested state assistance for her 8-year-old son. The statement said that while Mirasolo was granted joint legal custody after his paternity was confirmed, the mother was given veto power over any rights he had to see the child. 
 Noting that the order "awarded the mother sole physical custody of the minor child," prosecutors said: "The order is clear that, if the mother does not want the father to have visitation, she does not have to provide it." 
 It remained unclear Tuesday whether prosecutors knew of Mirasolo's criminal history during the assistance and paternity case. 
