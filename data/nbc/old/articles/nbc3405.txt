__HTTP_STATUS_CODE
200
__TITLE
10-Year-Old Florida Girl Pries Leg From Alligators Mouth
__AUTHORS
Alex Johnson
__TIMESTAMP
May 9 2017, 7:51 am ET
__ARTICLE
 A 10-year-old Florida girl fought off an alligator attack by prying open the 9-foot-long beast's mouth and freeing her leg, the little girl and rescue personnel said Monday. 
 The girl, Juliana Ossa, was swimming Saturday afternoon in about 2 feet of water at a lake in Moss Park in Orlando when the alligator bit her, lifeguards said in an incident report filed with the Orange County Department of Parks and Recreation. 
 She was pulled to shore and rushed to Nemours Children's Hospital, where she was treated for lacerations and puncture wounds in the back of her left knee and lower thigh, the state Fish and Wildlife Conservation Commission said. She was back home on Monday, with her leg covered in bandages from hip to toe. 
 "I tried hitting it on its forehead to let me go," Juliana said in an interview on NBC's TODAY. "That didn't work, so I thought of a plan they taught in Gatorland. The guy was wrestling the alligator with its mouth taped, and in this situation it was the other way around. 
 "So I stuck my two fingers up its nose so it couldn't breathe  it had to be from its mouth  and he opened it, so it let my leg out." 
 "I was scared at first, but I knew what to do," she told told NBC affiliate WESH earlier Monday. 
 Juliana's step uncle Steven Rodriguez pulled her out of the water: "When I got her to the shore I let her down and I saw how much blood was coming out of her leg, so I picked her back up and I walked her to the picnic table." 
 Kevin Brito, a paramedic who took care of Juliana, said Juliana stayed calm throughout the ordeal. 
 "She was a tough little girl," Brito told WESH. "She also commented that if something is going to attack her, she has to attack back." 
 Trappers caught the alligator and euthanized it. The beach remained closed Monday as crews evaluated the incident. 
 
