__HTTP_STATUS_CODE
200
__TITLE
Why Apples 10th Anniversary iPhone Is a Game Changer
__AUTHORS
Alyssa Newcomb
__TIMESTAMP
Sep 12 2017, 3:05 pm ET
__ARTICLE
 The world is getting not one, not two, but three new iPhones. 
 The trio of devices, which come 10 years after the first iPhone was released, made their debuts on Tuesday during an event at Apple's brand-new campus in Cupertino, California  and stayed true to the leaks. 
 The new devices are (not surprisingly) called the iPhone 8 and the iPhone 8 Plus. The 10th anniversary phone, which Apple CEO Tim Cook presented separately, is named the iPhone X. 
 The newly designed phones have glass on the front and the back, and represent the tech giant's most durable iPhone yet. The 64GB iPhone 8 starts at $699, the 64GB 8 Plus at $799, and the X at a premium price tag of $999. 
 Many of the new features have already been seen on other smartphones, particularly from Samsung and LG. But Apple will likely put its own spin on the user experience, Geoff Blaber, leader of mobile device software research at CCS Insight, told NBC News. 
 "What we see with Apple is they may not be first with these hardware features, but it tends to be how they are integrated with software," Blaber said. "That is what creates the special Apple value." 
 Related: Samsungs Galaxy Note 8 Ready to Battle Apple, Google 
 Here are the flashiest new features to look for in the new phones, which are available to order on Sept.15 and on sale in stores Sept. 22. 
 The iPhone X is ditching LCD displays in favor of OLED. But don't get too bogged down in the alphabet soup of characters  basically, this means the new iPhone will have a more power-efficient display since they're removing the backlight, which can be a battery drain. OLED displays are also thinner and produce more vibrant colors. 
 Apple is calling the edge-to-edge display the "super-retina display." Phil Schiller, senior vice president of worldwide marketing, said this is the first OLED screen that was good enough to be in an iPhone. 
 While plug-in chargers are still an option, Apple is going wireless. Here's how it works: Just rest the iPhone on a supported inductive charging pad or mat. An electromagnetic field allows a current to flow between the mat and the phone, charging the device. Apple recently joined the Wireless Power Consortium, which works with member tech companies to uphold the Qi wireless charging standard. This means wireless charging will work with existing Qi charging mats, which are already in some cars and popular places, including McDonald's. 
 This year, Apple scooped up the Israeli facial recognition company RealFace. Now, the iPhone X can recognize your facial features, adding yet another way to unlock your phone. 
 "Every time you glance at your iPhone 10," it will detect your face, setting off a high-tech process, including an augmented-reality image and a pattern of 30,000 dots to create a mathematical model of your face. A neural engine can process this, and it all happens in real time, Schiller said. It will also work with Apple Pay. 
 It's unclear how Apple could further use this facial recognition technology, which they call FaceID, and whether it will be opened up to developers who want to add an extra layer of security to in-app transactions. 
 The super sleek X is the first iPhone to ditch the home button, in favor of fluid gesture controls, marking a new chapter in the iPhone's 10-year history. If you need to reach Siri on the X, you can just say "Hey Siri" or press the larger side button. 
 Here's one update you won't have to pay for. While the new iPhones are certainly buzzworthy, Apple's coming iOS 11 update, which includes a slew of augmented reality apps, is stealing plenty of attention. Look for iOS 11 to come out later this month as a free software update for recent generation iOS devices. 
 The available apps could change the way you use your phone  whether it's placing virtual furniture in your living room to see how it looks before you buy it, to playing more Pokemon Go-style games that take virtual characters and place them in your real world. All you have to do is hold up your phone. 
