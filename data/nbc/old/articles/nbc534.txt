__HTTP_STATUS_CODE
200
__TITLE
ISIS Second-in-Command Ayad al-Jumaili Killed in Airstrike: Iraqi Military
__AUTHORS
NBC News
__TIMESTAMP
Apr 1 2017, 2:34 pm ET
__ARTICLE
 BAGHDAD  The ISIS deputy believed to be second-in-command to elusive leader Abu Bakr al-Baghdadi has purportedly been killed, an Iraqi military spokesman said Saturday. 
 Ayad al-Jumaili died in an airstrike conducted by the Iraqis that targeted a meeting of ISIS commanders in Al-Qa'im in the western Anbar province, said Brig. Gen. Yahya Rasool of Iraq's Joint Operation Command. He cited military intelligence. 
 Rasool said ISIS' director of administrative affairs, Salem Mudafar Al-Ajmi, also known as Abu Khatab, was killed in the raid. No date was given for the operation. 
 Rasool also described Jumaili as ISIS' minister of war, but his ranking within the terror group is not officially known and ISIS has made no mention of him previously. He was also known as Abu Yayha, the Iraqis said. 
 U.S. military officials told NBC News they have not confirmed the Iraqi military's report. 
 Iraqi State TV first announced Jumaili's death in an on-screen news flash. 
 In September, the Pentagon said it had confirmed that a U.S. airstrike killed another ISIS second-in-command, Abu Muhammad al-Adnani, who also served as the group's main spokesman. 
 ISIS previously said al-Adnani died in an Aug. 30 strike in al-Bab in Syria's devastated Aleppo province. 
 Al-Adnani's death came less than six months after two other top ISIS officials  finance minister Haji Iman and No. 3 Omar al-Shishani  were also reportedly killed. 
