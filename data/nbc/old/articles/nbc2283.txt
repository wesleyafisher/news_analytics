__HTTP_STATUS_CODE
200
__TITLE
Woman Who Accused Rapper Nelly of Rape Wont Testify, Says System Will Fail Her
__AUTHORS
Associated Press
__TIMESTAMP
Oct 14 2017, 7:32 pm ET
__ARTICLE
 SEATTLE  A woman who said the rapper Nelly raped her on his tour bus in a Seattle suburb last weekend is dropping her pursuit of criminal charges. 
 News outlets report that lawyer Karen Koehler said in a statement Friday that her client wanted to stop the investigation and would refuse to testify in court. 
 Koehler says the woman "wishes she had not called 911 because she believes the system is going to fail her." 
 Auburn police arrested Nelly, whose real name is Cornell Iral Haynes Jr., early Saturday in his tour bus at a Walmart. He was booked into jail on suspicion of second-degree rape and released later that day. 
 He has not been charged with a crime. 
To be absolutely clear. I have not been charged with a crime therefore no bail was required. I was released , pending further investigation.
 Nelly's attorney, Scott Rosenblum, has called the rape claim a "completely fabricated allegation." 
