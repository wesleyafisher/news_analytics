__HTTP_STATUS_CODE
200
__TITLE
Trump Wanted Tenfold Increase in Nuclear Arsenal, Surprising Military
__AUTHORS
Courtney Kube
Kristen Welker
Carol E. Lee
Savannah Guthrie
__TIMESTAMP
Oct 11 2017, 7:23 am ET
__ARTICLE
 WASHINGTON  President Donald Trump said he wanted what amounted to a nearly tenfold increase in the U.S. nuclear arsenal during a gathering this past summer of the nations highest-ranking national security leaders, according to three officials who were in the room. 
 Trumps comments, the officials said, came in response to a briefing slide he was shown that charted the steady reduction of U.S. nuclear weapons since the late 1960s. Trump indicated he wanted a bigger stockpile, not the bottom position on that downward-sloping curve. 
 According to the officials present, Trumps advisers, among them the Joint Chiefs of Staff and Secretary of State Rex Tillerson, were surprised. Officials briefly explained the legal and practical impediments to a nuclear buildup and how the current military posture is stronger than it was at the height of the buildup. In interviews, they told NBC News that no such expansion is planned. 
 The July 20 meeting was described as a lengthy and sometimes tense review of worldwide U.S. forces and operations. It was soon after the meeting broke up that officials who remained behind heard Tillerson say that Trump is a moron. 
 Revelations of Trumps comments that day come as the U.S. is locked in a high-stakes standoff with North Korea over its nuclear ambitions and is poised to set off a fresh confrontation with Iran by not certifying to Congress that Tehran is in compliance with the 2015 nuclear deal. 
 Trump convened a meeting Tuesday with his national security team in which they discussed a range of options to respond to any form of North Korean aggression or, if necessary, to prevent North Korea from threatening the U.S. and its allies with nuclear weapons, according to the White House. 
 The presidents comments during the Pentagon meeting in July came in response to a chart shown on the history of the U.S. and Russias nuclear capabilities that showed Americas stockpile at its peak in the late 1960s, the officials said. Some officials present said they did not take Trumps desire for more nuclear weapons to be literally instructing the military to increase the actual numbers. But his comments raised questions about his familiarity with the nuclear posture and other issues, officials said. 
 Two officials present said that at multiple points in the discussion, the president expressed a desire not just for more nuclear weapons, but for additional U.S. troops and military equipment. 
 Any increase in Americas nuclear arsenal would not only break with decades of U.S. nuclear doctrine but also violate international disarmament treaties signed by every president since Ronald Reagan. Nonproliferation experts warned that such a move could set off a global arms race. 
 If he were to increase the numbers, the Russians would match him, and the Chinese would ramp up their nuclear ambitions, Joe Cirincione, a nuclear expert and an MSNBC contributor, said, referring to the president. 
 There hasnt been a military mission thats required a nuclear weapon in 71 years, Cirincione said. 
 Details of the July 20 meeting, which have not been previously reported, shed additional light on tensions among the commander in chief, members of his Cabinet and the uniformed leadership of the Pentagon stemming from vastly different world views, experiences and knowledge bases. 
 Related: Tillerson's Fury at Trump Required Intervention From Pence 
 Moreover, the presidents comments reveal that Trump, who suggested before his inauguration that the U.S. must greatly strengthen and expand its nuclear capability, voiced that desire as commander in chief directly to the military leadership in the heart of the Pentagon this summer. 
 Some officials in the Pentagon meeting were rattled by the presidents desire for more nuclear weapons and his understanding of other national security issues from the Korean Peninsula to Iraq and Afghanistan, the officials said. 
Departing The Pentagon after meetings with @VP Pence, Secretary James Mattis, and our great teams. #MAGA pic.twitter.com/5d1MSzhjQS
 That meeting followed one held a day earlier in the White House Situation Room focused on Afghanistan in which the president stunned some of his national security team. At that July 19 meeting, according to senior administration officials, Trump asked military leaders to fire the commander of U.S. forces in Afghanistan and compared their advice to that of a New York restaurant consultant whose poor judgment cost a business valuable time and money. 
 Two people familiar with the discussion said the Situation Room meeting, in which the presidents advisers anticipated he would sign off on a new Afghanistan strategy, was so unproductive that the advisers decided to continue the discussion at the Pentagon the next day in a smaller setting where the president could perhaps be more focused. It wasnt just the number of people. It was the idea of focus, according to one person familiar with the discussion. The thinking was: Maybe we need to slow down a little and explain the whole world from a big-picture perspective, this person said. 
 The Pentagon meeting was also attended by Vice President Mike Pence; Treasury Secretary Steven Mnuchin; Gen. Joseph Dunford, the chairman of the Joint Chiefs of Staff; Gen. Paul Selva, the vice chairman; Defense Secretary James Mattis; Deputy Defense Secretary Patrick Shanahan; Stephen Bannon, then Trumps chief strategist; Jared Kushner, the president's son-in-law and senior adviser; and Reince Preibus, then chief of staff. Sean Spicer, then the White House spokesman, and Keith Schiller, who was director of Oval Office operations at the time, also accompanied Trump to the Pentagon that day. 
 Related: Donald Trump Has History of Mixed Messages on Nuclear Weapons 
 Asked for a response to the presidents comments, a White House official speaking on the condition of anonymity, said that the nuclear arsenal was not a primary topic of the briefing. Dana White, spokesperson for the Pentagon said the secretary of defense has many closed sessions with the president and his cabinet members. Those conversations are privileged. 
 President Trump responded Wednesday morning on Twitter: 
Fake @NBCNews made up a story that I wanted a "tenfold" increase in our U.S. nuclear arsenal. Pure fiction, made up to demean. NBC = CNN!
 Later Wednesday, the president said he "never discussed increasing" the size of the nuclear arsenal, repeating his claim of "fake news." 
 "Right now, we have so many nuclear weapons," Trump said at a press availability with Canadian Prime Minister Justin Trudeau. "I want them in perfect condition, perfect shape.  That's the only thing I've ever discussed." Defense Secretary Mattis also called the NBC report "absolutely false." 
 At the time of the meeting, White told reporters that the meeting covered the planet, and that the presidents advisers went around the world, outlining what she described as the challenges and opportunities for the U.S. 
 Two senior administration officials said the presidents advisers outlined the reasons an expansion of Americas nuclear arsenal is not feasible. They pointed to treaty obligations and budget restraints and noted to him that todays total conventional and nonconventional military arsenal leaves the U.S. in a stronger defense posture than it was when the nuclear arsenal alone was larger. 
 Still, officials said they are working to address the presidents concerns within the Nuclear Posture Review, which is expected to be finalized by the end of 2017 or early next year. Hes all in for modernization, one official said. His concerns are the U.S. stopped investing in this. 
 Officials present said that Trumps comments on a significantly increased arsenal came in response to a briefing slide that outlined Americas nuclear stockpile over the past 70 years. The president referenced the highest number on the chart  about 32,000 in the late 1960s  and told his team he wanted the U.S. to have that many now, officials said. 
 The U.S. currently has around 4,000 nuclear warheads in its military stockpile, according to the Federation of American Scientists. 
 The Pentagon is currently undergoing the long-planned posture review. Modernizing the arsenal is a step presidents continuously take that doesnt put the U.S. in violation of treaty obligations, Cirincione said. 
 You dont get in trouble for modernizing," he said. "You do get in trouble if you do one of two things: if you increase the numbers. The strategic weapons are treaty limited. Two, if you build a new type of weapon that is prohibited by a treaty." 
 Related: Tillerson Summoned to White House Amid Trump Fury 
 Its unclear which portion of the Pentagon briefing prompted Tillerson to call the president a moron after the meeting broke up and some advisers were gathered around. Officials who attended the two-hour session said it included a number of tense exchanges. 
 At one point, Trump responded to a presentation on the U.S. military presence in South Korea by asking why South Koreans arent more appreciative and welcoming of American defense aid. The comment prompted intervention from a senior military official in the room to explain the overall relationship and why such help is ultimately beneficial to U.S. national security interests. 
 Trump has been inconsistent with regards to his stance on nuclear weapons. 
 At one of the earliest Republican debates, in December of 2015, then-candidate Trump seemed to stumble through a question about the nuclear triad, the land, air, and sea-based systems present in a traditional nuclear arsenal. 
 Asked three months later about U.S. policy on nonproliferation, Trump said on CNN: "Maybe its going to have to be time to change, because so many people, you have Pakistan has it, you have China has it. 
 When pressed, he allowed, "I dont want more nuclear weapons. 
 But his suggestion that the world could see an increase in nuclear weapons after decades of post-Cold War reductions rattled Americas allies and drew criticism from foreign policy experts and U.S. officials at the time. 
 The president left the Pentagon on July 20, telling reporters the meeting was absolutely great. 
