__HTTP_STATUS_CODE
200
__TITLE
Top Mormon Leader Reaffirms Faiths Opposition to Gay Marriage
__AUTHORS
Associated Press
__TIMESTAMP
Sep 30 2017, 1:56 pm ET
__ARTICLE
 SALT LAKE CITY  A top Mormon leader reaffirmed the religion's opposition to same-sex marriage on Saturday during a church conference watched by members around the world. 
 Dallin H. Oaks, a member of a top governing body called the Quorum of the Twelve Apostles, urged members to follow church teachings that dictate that marriage should be reserved for heterosexual men and women. He said that's the ideal home for children to be raised. 
 Oaks acknowledged that this belief can put Mormons at odds with family and friends and doesn't match current laws, including the recent legalization of gay marriage in the United States. But he told the nearly 16-million members watching around the world that the religion's 1995 document detailing the doctrine  "The Family: A Proclamation to the World"  isn't' a policy statement that will be changed. 
 Related: Excommunication of High-Ranking Mormon Official 'Very Rare' 
 "We have witnessed a rapid and increasing public acceptance of cohabitation without marriage and same-sex marriage. The corresponding media advocacy, education, and even occupational requirements pose difficult challenges for Latter-day Saints," Oaks said. "We must try to balance the competing demands of following the gospel law in our personal lives and teachings even as we seek to show love for all." 
 The speech followed a push in recent years by The Church of Jesus Christ of Latter-day Saints to uphold theological opposition to same-sex relationships amid widespread social acceptance while trying to foster an empathetic stance toward LGBT people. 
 The Mormon church is one of many conservative faith groups navigating the challenges that arise from trying to strike the right balance. 
 After the Utah-based Mormon church received backlash in 2008 for helping lead the fight for California's Proposition 8 constitutional ban on gay marriage, religious leaders spent several years carefully developing a more empathetic LGBT tone. 
 That was interrupted in 2015 when the church adopted new rules banning children living with gay parents from being baptized until age 18. That policy drew harsh criticism from gay church members and their supporters. 
 A year ago, church leaders updated a website created in 2012 to let members know that that attraction to people of the same sex is not a sin or a measure of their faithfulness and may never go away. But the church reminded members that having gay sex violates fundamental doctrinal beliefs that will not change. 
 Related: Amid Backlash, Mormon Church Clarifies Same-Sex Policy 
 Oaks on Saturday reiterated a church belief that children should be raised in heterosexual married households, not by gay parents or couples who live together but aren't married. He lamented that fewer children in the United States aren't raised in what the religion considers the ideal households. 
 "Even as we must live with the marriage laws and other traditions of a declining world, those who strive for exaltation must make personal choices in family life according to the Lord's way whenever that differs from the world's way," Oaks said. 
 The twice-yearly conference is underway without the presence of church President Thomas S. Monson, 90, who is dealing with ailing health. It's the first time in more than a half century that Monson hasn't spoken at the conferences. Before becoming church president in 2008, he served on the Quorum of the Twelve Apostles starting in 1963. 
 Presidents of The Church of Jesus Christ of Latter-day Saints serve until they die. 
 Also missing will be 85-year-old Robert D. Hales, another top leader who was hospitalized in recent days. Hales has been a member of the Quorum of the Twelve Apostles since 1994. 
