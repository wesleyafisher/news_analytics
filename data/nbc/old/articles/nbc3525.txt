__HTTP_STATUS_CODE
200
__TITLE
ISIS Leader al-Baghdadi, Reported Possibly Killed, Presents a Shadowy Figure
__AUTHORS
Tracy Connor
Corky Siemaszko
__TIMESTAMP
Jun 17 2017, 4:18 pm ET
__ARTICLE
 Dead or alive, ISIS leader Abu Bakr al-Baghdadi remains a mystery wrapped in menace. 
 It was the Russians on Friday who announced the possible death of the elusive al-Baghdadi following a May 28 bombing raid on an ISIS target outside Raqqa, Syria. 
 But they gave no explanation for why they waited two weeks to alert the world, the Pentagon had not yet confirmed al-Baghdadis demise, and there was ample reason to be cautious. 
 The America's Most Wanted terrorist kingpin has been reported killed several times before only to turn up later alive and defiant and vowing to unleash "volcanoes of jihad." 
 "Yes, there should be skepticism," NBC News' Ayman Mohyeldin said Friday on TODAY. "There have been several reports he has been killed in the past." 
 While this is the first time the Russians have made this claim, Mohyeldin said it's highly unlikely al-Baghdadi would risk getting taken out by attending a meeting with top ISIS leaders at a location near Raqqa, Syria, as Moscow claims. 
 "This guy is a tactical mastermind," he said. 
 It was al-Baghdadi who turned a ragtag collection of disgruntled Iraqi soldiers and jihadis from around the world into a potent fighting force that committed unspeakable crimes while carving out a caliphate in Iraq and Syria ruled by Shariah law. 
 ISIS in recent months has lost much of its conquered territory to U.S.-backed Iraqi and Kurdish forces and to the Russia-backed Syrian Army. But even dead, Mohyeldin said, he would "continue to inspire" other would-be jihadists. 
 So how did a 45-year-old Iraqi come to have a $25 million bounty on his head? 
 Much of the story is murky. And the fact that al-Baghdadi was, for a time, an American prisoner at Camp Bucca in Iraq does little to clear it up. 
 "They know physically who this guy is, but his backstory is just myth," Patrick Skinner of the Soufan Group, a security consulting firm, told NBC News in an earlier interview. 
 The official jihadi line on al-Baghdadi is that he is an imam from a religious family, that he is descended from noble tribes, that he is a scholar and a poet with a Ph.D. from Baghdad's Islamic University, possibly in Arabic. 
 Al-Baghdadi was born in Samarra. And after the U.S. toppled Saddam Hussein, he is believed to have served as the commander of an insurgent group operating in and around the city of Fallujah and numbering anywhere from 50 to 100 men. 
 By 2005, al-Baghdadi was a prisoner at Camp Bucca where his captors did not consider him particularly dangerous. 
 "He didn't rack up to be one of the worst of the worst," said Col. Ken King, who oversaw Camp Bucca in 2008 and 2009. "The best term I can give him is savvy." 
 King, who first spoke to the Daily Beast, recalled that when al-Baghdadi was turned over to the Iraqi authorities in 2009, he remarked, "I'll see you guys in New York," an apparent reference to the hometown of many of the guards. 
 "But it wasn't menacing," King said. "It was like, 'I'll be out of custody in no time.'" 
 Not long afterward, al-Baghdadi began rising through the ranks of the Islamic State of Iraq, the successor to Abu Musab al-Zarqawi's al Qaeda in Iraq. And when the organization's two leaders were killed in 2010, Baghdadi took the reins and renamed it ISIS. 
 Unlike other militant leaders, al-Baghdadi kept a low profile and avoided making tapes with fiery speeches that could rally the troops but also make him an even bigger target. 
 "When you start making videos and popping off, it increases the chance you're going to get caught or killed," Skinner said. "He's been around five years, and that's like cat years. It's a long time." 
 Al-Baghdadi, which is not his birth name, hid behind a host of aliases and wore a bandanna around his face to conceal his identity from everyone except a very tight inner circle that is believed likely to be comprised only of Iraqis. 
 For the would-be jihadis responding to al-Baghdadi's call to arms, that only added to his allure. 
 "He's managed this secret persona extremely well, and it's enhanced his group's prestige," said Patrick Johnston of the RAND Corporation. "Young people are really attracted to that." 
 Under al-Baghdadi, ISIS burnished its reputation for brutality by beheading western journalists and by taking sex slaves. 
 Two years ago, the family of Kayla Mueller  the American aid worker who died while being held captive by ISIS  reported that al-Baghdadi had raped her repeatedly. 
 Last year, a woman claiming to be an ex-wife of al-Baghdadi offered more insight into the psyche of the terrorist leader. 
 In an interview with the Swedish newspaper Expressen, Saja al-Dulaimi said she knew al-Baghdadi as a university lecturer named Hisham Mohammed. NBC News has not independently confirmed details of the interview. 
 "He was mysterious," she said in the newspaper interview. "He wasn't very talkative." 
 And, she said, he would disappear for days at a time. 
 In the end, al-Dulaimi said she left him because she didnt love him. 
 "He was an enigmatic person," she said. "You couldn't have a discussion or hold a normal conversation with him ... He just asked about things and told me to fetch things. He gave orders, nothing more." 
