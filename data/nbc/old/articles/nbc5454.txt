__HTTP_STATUS_CODE
200
__TITLE
Why NASA Wants to Send a Submarine to Titan
__AUTHORS
Shannon Hall
__TIMESTAMP
Feb 15 2017, 10:30 am ET
__ARTICLE
 Saturns largest moon, Titan, boasts a thick atmosphere, complex weather cycle, and seas shaped by waves and tidal currents  much like Earth. 
 But you wouldnt recognize it. That thick atmosphere is a dense layer of orange smog that swaddles the moon. The weather cycle causes rain to only downpour once every several hundred years. And those seas are full of liquid hydrocarbons in the form of methane and ethane. 
 The paradox  plus Titan's potential to sustain exotic life  has encouraged scientists to dream of better probing the moon. Although NASAs Cassini spacecraft has flown past it several times since 2004  and even dropped the Huygens probe onto Titan's surface in January 2005  scientists are still eager to get their feet wet. 
 Related: Why These Scientists Fear Contact with Space Aliens 
 The latest idea is to send a submarine to explore Titans alien seas. The mission study, with funding from NASA Innovative Advanced Concepts (NIAC), an initiative for projects by scientists who think outside the box, is in its infancy. 
 We do these really advanced almost-science-fiction  but never actually-science-fiction  studies that are meant to show what might be possible in the future, says NIACs Program Executive Jason Derleth. 
 A Titan sub fits that bill perfectly. 
 From Europas subsurface liquid ocean to Enceladuss plumes of liquid water that shoot above the icy moons south pole, the outer solar system is rich with moons begging to be explored. But Titan is too unique to pass up. 
 It's just such a strange and fascinating system, says Jason Goodman, an astronomer at Wheaton College, who is not directly involved in the NIAC study. A lot of our ideas about geography and oceanography kind of get turned on their head when we start thinking about materials other than water. 
 Although scientists have models for how they think these processes work on Earth  and how they should work on other planetary bodies  the opportunity to test those models on Titan, where the gravity of the moon and the viscosity of the liquids is different, could help them better understand these processes on a fundamental level. 
 It's an opportunity to do an experiment on planetary scales, says Ralph Lorenz, an astronomer at Johns Hopkins University and one of the brains behind the NIAC study. 
 Take hurricanes. On the Earth, their intensity is controlled by the heat in the upper layers of the ocean and how that heat is transferred to the atmosphere as the storm stirs the liquid. Scientists know Titan also has a complex weather cycle and that same exchange between the atmosphere and the sea happens on the moon as well. 
 So perhaps studying hurricanes as they swirl over the oily seas on Titan will help scientists better understand these wild processes as well. 
 By comparing these two worlds, Goodman hopes that well start to understand how liquid and land interact throughout the universe  a crucial study in a world that might be dominated by more and more strange worlds that are less like Earth, he says. 
 Then theres the tantalizing question: Are we alone? 
 There are a couple of aspects about Titan that make the exploration of the moon from an astrobiological standpoint very appealing, Lorenz says. Principle among those is that this place is literally awash with organic material: carbon-bearing, nitrogen-bearing, possibly oxygen-bearing compounds. And that's what we're made of. 
 Lab work has shown that if scientists mix together methane and nitrogen  the two elements common in Titans atmosphere  and pass a spark through it, they can recreate the same brown goop they see on Titan. Then, if they add water to the goop, it creates amino acids  the building blocks of life. 
 But has that final step taken place on Titan? Its possible. Titans hydrocarbon lakes sit on a bedrock of water ice, which sits above a liquid water ocean distinct from the hydrocarbon sea on the surface. Should the two interact, they could form life. Lorenz thinks its possible water can sometimes reach the surface in the same way that lava erupts from volcanoes on Earth. He also thinks massive impacts could have unleashed enough heat to melt some of the moons crust, revealing the liquid below. 
 There's a rich set of possibilities for life and biochemistry as we understand it, Lorenz says. Can there be chemistry in Titans sea that have gone down an analogous path? Again, its possible. But only a Titan submarine will be able to answer that question. 
 The NIAC teams first design was a torpedo-shaped submarine that had to surface every time it needed to communicate with Earth, pick up directions, or send back data. But the second design, dubbed a Titan Turtle because of its shell-like design, includes an orbiter that can relay information without having to surface. 
 But even with the orbiter, ground control cant be in constant communication with the turtle, so it will have to be mostly autonomous. You cant joystick the vehicle around, Lorenz explains. You cant fly it in real time like you can with a moon rover. 
 Instead, the submarine has to move dependably and sense the environment around it while carrying out certain tasks. So it would be autonomous much like the Mars rovers are autonomous, which typically receive directions from mission control once a day. 
 The team still needs to flesh out how the turtle would arrive (a fairly simple problem given that weve already landed a probe on Titan) and determine the suite of instruments it would bring along for the ride. The obvious ones include those that will measure the composition and temperature of the lakes. The team is itching to see if there are layers that dont intermix like in the Black Sea. Meanwhile, theyll also search for any surface and subsurface currents, tides, wind, and waves. 
 No submarine is complete without a camera on the mast. When the turtle surfaces, it will image the shoreline and search for beaches, cliffs, and layers. 
 One of the most exciting things is that we see evidence in Cassini data of the seas having dried up in the past and then refilled, Lorenz says. Swimming along the shoreline and diving deep to analyze the sea bed (where different layers might have been laid down on top of each other in different climate cycles) with its robotic arm could help explain this mysterious past. 
 In October, the mission design will complete its second and final phase in the NIAC program. At this point, missions might move to a new home within NASA, or even receive outside funding. Although Derleth hasnt crunched the numbers to see how many projects have received follow-on funding, he has kept track of the total: Over the past five years, a select number of NIAC programs have gone on to receive an additional $200 million. 
 Related: Space Mining: The Intergalactic Gold Rush Is On 
 Lorenz and his team arent sure they will receive follow-on funding from either NASA or other investors. It's a really challenging mission, Derleth says. So I think it will have to win its way into the hearts and minds of scientists before it flies. But it might very well do that because the science that it could achieve is truly groundbreaking. 
 But Goodman is already convinced his project has the potential to achieve its set goals and produce some fantastic science during the journey. 
 I think it's one of the most exciting ideas for exploring the outer planets that's come along in quite some time, he says. We shouldn't underestimate the degree to which let's go drive a submarine around in Titan is going to capture the public's attention and really make us think about how weird and fascinating a place in the solar system can be." 
 For more of the breakthroughs changing our lives, follow NBC MACH. 
 
