__HTTP_STATUS_CODE
200
__TITLE
Did Puerto Ricos Failed Economy Play a Role in Its Devastation?
__AUTHORS
Ben Popken
__TIMESTAMP
Sep 27 2017, 11:44 am ET
__ARTICLE
 Long before Hurricane Maria slammed into Puerto Rico, the island was facing its own monetary maelstrom. 
 The territory is now in its 11th year of recession, and has $74 billion in debt. And, while many of the factors that led to this are years in the making, some of them are down to the simple fact that Puerto Rico is an island, subject to special rules, and caught somewhere between the status of an independent country and a state. 
 Are those same factors at play in the scope of the island's devastation? Would a more stable economy have fared better in the path of a Category 4 hurricane? 
 President Donald Trump seemed to indicate so, when he broke his five-day silence on Puerto Rico's post-Hurricane Maria devastation by tweeting out reminders of the country's debt load and inadequate infrastructure. 
 Critics assailed the remarks as tone deaf, and Mayor Carmen Yuln Cruz of San Juan, Puerto Rico's capital, said it was wrong to mix the issues of the island's worthiness for aid with its longstanding fiscal quagmire. 
 But if the territory's economic issues are to a degree self-inflicted, it is perhaps the result of inefficient governance from Washington, D.C. 
 Despite a massive real estate slump that dated back to 2006, Puerto Rico's government continued to issue balanced budgets for many more years  but its tax revenues fell short of forecasts. Puerto Rico tried to borrow its way out of the hole, in part by issuing debt in the form of bonds. With their high yields and unique tax-exempt status, these bonds attracted mutual and hedge funds. They also drew ordinary investors such as retirees and ordinary Puerto Ricans. Investors liked the potential payoff and believed Puerto Rico would eventually recover. 
 Successive governors tried to "borrow from Peter to pay Paul," taking out loans to sunset old debts and keep the system running. 
 Related: This Dated Law Is Worsening Puerto Rico's Crisis 
 "Puerto Rico hid its fiscal problems in order to try to borrow its way out. Through ever more creative ways it continued to borrow and build up its debt," Brad Setser, a senior fellow at the Council on Foreign Relations, an independent, non-partisan think tank, told NBC News. "But its efforts to disguise its weakening underlying fiscal position ended up digging itself into a deeper hole." 
 The wheels began to fall off in 2014 when the island's credit rating was downgraded by ratings agencies, cutting off its access to new credit. In 2015, the governor declared its debts "unpayable." It began missing some interest payments. 
 Now the power company is effectively bankrupt, with a reported at least $9 billion in debt and no money for upgrades. It was already struggling to find $4 billion to upgrade its old power plants. Now it must deal with 80 percent of its power lines being toppled. 
 Last year, the country signed a "Memorandum of Understanding" to combat the effects of climate change, including boosting resilience to rising sea levels and flooding and to regulate land development that could contribute to erosion. But it's unknown whether they followed through with them and sections of certain communities had already fallen into the water due to rising sea levels even before Maria hit. 
 And years of deferred maintenance from a government that had prioritized dealing with the debt crisis left other key parts of the infrastructure like electrical, water, sewer and gas lines less prepared to cope with the onslaught. 
 When the local utility updated some pipes recently, older ones exploded, unable to deal with the increased pressure. 
 Macroeconomic forces have also added to the country's woes. The country imports oil for nearly all of its energy production and was hit hard from 2005 to 2012 when oil prices doubled. Following the United States' free trade agreements with Latin American and Caribbean states, Puerto Rico's manufacturing sector has faced higher competition. 
 Lucrative tax breaks drew scores of pharmaceutical companies to deploy manufacturing plants on the island. They provided needed jobs but their use of global supply chains limited the washover benefit to local economies and bred dependence on outside businesses. When the U.S. started to phase out the key tax break, plant closures and layoffs followed, with little entrepreneurial ecosystem to fall back on. 
 Because Puerto Rico is not a state, it doesn't have representation in Congress. It's dependent on others in Washington for aid and governance. This means it doesn't get a say in important laws but can be affected by the ones passed there. For instance, it's subject to the Federal minimum wage of $7.25 even though local income and productivity are much lower. Critics argue that this discourages low business develop and entrepreneurship. 
 Perhaps this is why the government is full of red tape and duplicative offices: it gives people a job. In Puerto Rico the government is the largest employer, representing about 26 percent of the total workforce. 
 The island is rife with bureaucratic disarray and lengthy permitting processes that ties up projects and delays new business development. According to a 2016 Reuters investigation, one would-be entrepreneur ditched his plans for building an assisted living facility because of five-year waits to permits. Just to work a single food festival requires "mountains" of government forms. 
 Related: Mass Exodus Feared from Puerto Rico Amid Escalating Crisis 
 But cleaning it up is no easy matter. 
 "It's hard to say 'we're going to fire all the people who work in bureaucracies because they're no longer needed,'" Salim Furth, a research fellow at the right-leaning Heritage Foundation think tank, told NBC News. 
 "I think you should let the dust settle before pointing out someone's lousy policies," he added. "Their politicians are aware of the problems but have a hard time addressing them because there are a lot of entrenched interest groups." 
