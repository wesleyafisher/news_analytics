__HTTP_STATUS_CODE
200
__TITLE
Flu Vaccine Protects About Half the Time, CDC Says
__AUTHORS
Maggie Fox
__TIMESTAMP
Feb 16 2017, 3:00 pm ET
__ARTICLE
 The current flu vaccine is about so-so  it protects about half of people from infection, health officials said Thursday. 
 That's not as good as last year's formula. but it's better than in some past years, the Centers for Disease Control and Prevention says. 
 "Interim influenza vaccine effectiveness estimates for the 201617 season indicate that vaccination reduced the risk for influenza-associated medical visits by approximately half," a CDC team wrote in the agency's weekly report on disease. 
 It lowered the risk of having to go to the hospital or the emergency room by 48 percent, the team calculated. There are actually half a dozen different flu vaccines that are formulated differently and that protect against either three or four flu strains. 
 Related: Pediatricians Advise Against FluMist This Year 
 Taken together, they provided 43 percent protection against the H3N2 viruses that are most common this year and 73 percent effectiveness against influenza B, the team found. 
 Flu has killed 20 children so far this season, the CDC said. "Since influenza-associated pediatric mortality became a nationally notifiable condition in 2004, the total number of influenza-associated pediatric deaths per season has ranged from 37 to 171," the CDC said. 
 That doesn't include the 2009-10 flu year, when H1N1 swine flu was a brand new strain. It killed 358 children that season. 
 In any given year, flu kills up to 50,000 people  mostof them elderly. It puts another quarter-million into the hospital. 
 Flu is still widespread, the CDC team said. "Influenza activity is likely to continue for several more weeks in the U.S., and vaccination efforts should continue as long as influenza viruses are circulating," the CDC said. 
 It's been a moderate flu year so far  not especially deadly and not infecting more people than usual in the annual flu epidemic. 
 Related: New Study Finds Flu Vaccine May Hit the Wrong Target 
 "Elevated influenza activity in parts of the U.S. is expected for several more weeks. Healthcare providers should continue to offer and encourage vaccination to all unvaccinated persons age six months and older." 
 Flu spreads every year because the viruses mutate constantly. In any given season, many different strains will circulate and they'll "drift"  change a little  from one month to the next and from one country to the next. 
 That's why fresh flu vaccine cocktails are needed each year. In addition, the vaccine protects people for only a few months after it's given. 
 The vaccine is a little less effective in Europe this year. 
 A team testing flu patients and people with other illnesses in 12 countries found that the vaccines protected people from infection 38 percent of the time. It was least effective among sick and weak elderly people, the team reported in the journal Eurosurveillance. 
 There are a few cases of unusual influenza infections, as well. The CDC noted a vet who caught H7N2 bird flu from cats infected at a new York City animal shelter  and a patient in Iowa caught H1N2 flu from a pig. 
