__HTTP_STATUS_CODE
200
__TITLE
This Silicon Valley Teardown Just Sold for $2.6 Million
__AUTHORS
Kathryn Dill, CNBC
__TIMESTAMP
May 4 2017, 2:59 pm ET
__ARTICLE
 Another day, another postcard from the increasingly alternative universe that is Silicon Valley. 
 This time, it's the eye-popping story of a 908-square-foot, lovely-seeming home in Palo Alto, California, that hit the market in February for an asking price of $1,927,000. 
 Not only did the Stanford Avenue home, built in 1937 and described by Curbed San Francisco as a "teardown," sell, the site reports it brought in $623,000 over the asking price  that's a final sale price of $2,550,000. 
 It's the most recent, and hardly the most egregious, example of the burgeoning bubble on the Bay. Tech workers have launched many public complaints about the obscene cost of life in the valley: One San Francisco-based Twitter employee who earns $160,000 a year told the Guardian he can barely make ends meet; some Facebook engineers  earning between $100,000 and $700,000 a year  had to ask Mark Zuckerberg for help with their sky-high rent; and one Apple employee said he lived in a garage and used a bucket as a toilet. 
