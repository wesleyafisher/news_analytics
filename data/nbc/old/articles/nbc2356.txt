__HTTP_STATUS_CODE
200
__TITLE
U.S.-Cambodia Sanctions Cause Anxiety for Some Facing Deportation
__AUTHORS
Agnes Constante
__TIMESTAMP
Oct 13 2017, 5:20 pm ET
__ARTICLE
 The federal government has begun detaining Cambodian Americans with orders to be removed from the U.S. in what advocates call one of the bigger roundups in the community's history. 
 Last week, the Southeast Asia Resource Action Center (SEARAC), a group founded in 1979 to help settle post-Vietnam War refugees, issued an alert that the Cambodian government could be preparing to accept a new group of deportees in the coming months. 
 Since then, the organization and Asian Americans Advancing Justice-Asian Law Caucus (ALC), a civil rights nonprofit, said they have received reports of 21 individuals being detained. 
 The enforcement in the Cambodian-American community comes about a month after the Cambodian Ministry of Foreign Affairs and International Cooperation released a statement stating that it planned to interview 26 nationals facing deportation in response to visa sanctions imposed on certain high-ranking officials by the U.S. 
 The detentions have caused uncertainty and stress among some Cambodian Americans. 
 Vanna In, 41, a Cambodian national and resident of Fresno, California, said he has been in deportation limbo for about 16 years. He added that his status has been an emotional and physical roller coaster, particularly because he is married, has three young children, and cares for his mother-in-law. 
 Were walking on eggshells, he said. Were thinking about buying a house. So if we buy a house and they come get me, what happens to my wife and my kids? They cant afford the house. Im the breadwinner. 
 RELATED: U.S. Limits Visas for African, Asian Nations Over Deportations 
 In pleaded guilty for second degree murder as a teenager unaware that doing so could lead to a deportation order. In said he reformed in prison and currently serves as a youth pastor at a local church. He also co-founded a nonprofit in Colorado that serves at-risk young men. 
 If someone considers a place home, why send them to another place? In said, Why uproot families? Why take away somebody's opportunity to give back to America, to make America better? 
 Katrina Dizon Mariategue, immigration policy manager at SEARAC, said that families have told her about having suicidal thoughts concerning the possibility that their loved ones might be taken away. 
 The Cambodian Embassy in Washington, D.C. did not respond to a request for comment. 
 In the statement dated Sept. 14, Cambodia's Ministry of Foreign Affairs and International Cooperation said it had asked to renegotiate the countries' repatriation agreement in October 2016 after "fierce protests from Cambodian-American communities, Cambodians living in the country and some United States congressmen." 
 "The proposal sought to provide opportunity for both parties to discuss and amend the above-mentioned memorandum in line with humanitarian, compassionate and human rights aspects ... with a view to avoiding family separation and encouraging full integration to society," the statement said. 
 Cambodian Prime Minister Hun Sen criticized the U.S., calling its deportation of Cambodian nationals inhumane the same day, according to Reuters. 
 The United States continues to work with the Government of Cambodia to establish a reliable processes for the issuance of travel documents and their acceptance of the prompt, lawful return of Cambodian nationals who are subject to removal from the United States, Immigration and Customs Enforcement (ICE) spokesman Brendan Raedy said in an email. 
 In 1996, the Illegal Immigration Reform and Immigrant Responsibility Act (IIRIRA) expanded the definition of what types of crimes could result in deportation. It also allowed for that new definition to be applied retroactively, resulting in more than 16,000 Southeast Asian Americans, many of whom were refugees fleeing the Vietnam War and Cambodian genocide, receiving orders of removal  78 percent of which were based on old criminal records, according to SEARAC. 
 More than 1,900 Cambodian nationals in the U.S. are subject to a final order of removal, about 1,400 of whom have criminal convictions and 500 of whom have no lawful status to remain in the country, according to ICE. Between October 2016 and Aug. 5, 28 Cambodian nationals were deported. 
 RELATED: NBC Asian America Presents: Deported 
 Since Cambodia and the United States signed a repatriation agreement in 2002, more than 500 Cambodian Americans have been deported due to criminal convictions, Bill Herod, an adviser to the Returnee Integration Support Center, a Phnom Penh-based organization that aids deported Cambodian Americans, said in an email. 
 The most recent deportee arrived in Cambodia in May, Herod added. 
 A state department official said that visa issuance for individuals covered in the sanctions will resume once the secretary of homeland security sends the secretary of state the notice required by law relating to the country's acceptance of its nationals. 
 Follow NBC Asian America on Facebook, Twitter, Instagram and Tumblr. 
