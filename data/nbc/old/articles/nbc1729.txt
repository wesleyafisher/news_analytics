__HTTP_STATUS_CODE
200
__TITLE
The Week in Pictures: May 19 - 25
__AUTHORS
NBC News
__TIMESTAMP
May 26 2017, 8:30 am ET
__ARTICLE
Riot police clash with opposition demonstrators during a protest in Caracas, Venezuela on May 24, 2017.
Nearly two months of anti-government protests have left at least 53 people dead as opposition members demand early presidential elections.
Demonstrators contend President Nicolas Maduro's government is quickly becoming a full-fledged authoritarian regime, and that the constitution rewrite is one more attempt by Maduro to consolidate his power. They are also decrying Venezuela's triple-digit inflation, soaring crime and vast food shortages.
PHOTOS:Crafty Venezuelans Use Fecal Bombs and Bottle Masks Against Maduro
The USS Kearsarge joins The Parade of Ships as it makes its way past the Statue of Liberty on the opening day of Fleet Week on May 24, 2017 in New York. Now in its 29th year, Fleet Week brings more than 3,700 U.S. and Canadian service members to Manhattan through Memorial Day. The event includes ship tours, military demonstrations, musical performances and other events.
Pippa Middleton kisses her new husband James Matthews, following their wedding ceremony at St. Mark's Church in Englefield, England on May 20, as the bridesmaids, including Britain's princess Charlotte and pageboys, including Britain's prince George, walk ahead.
After turning heads at her sister Kate's wedding to Prince William, Pippa Middleton graduated from bridesmaid to bride on Saturday at a star-studded wedding in an English country church. The 33-year-old married financier James Matthews, 41, at a ceremony attended by the royal couple and tennis star Roger Federer, wearing a couture dress by British designer Giles Deacon.
A newborn Patas monkey looks out from an incubator at a zoo in Zhengzhou, China on May 23.
Fans crowd surf at the Rock On The Range music festival in Columbus, Ohio on May 20.
Protesters clash with members of the Bolivarian National Police during an anti-government march in Caracas, Venezuela on May 20.
Faced with mounting unrest, Venezuela's unpopular leftist President Nicolas Maduro vowed on Tuesday to push ahead in July with the formation of a "constituent assembly" to rewrite the constitution before regional elections in December.
PHOTOS:In Venezuela, Upheaval Shows No Signs of Slowing Down
Dark storm clouds loom over Paris and the Eiffel tower at sunset on May 19.
Pope Francis poses with President Donald Trump and his family during a private audience at the Vatican on May 24.
Trump and Francis, two leaders with contrasting styles and differing worldviews, set aside their previous clashes to broadcast a tone of peace for an audience around the globe.
Neither man, unquestionably two of the most famous in the world, repeated their prior criticism of the other. The statements released afterward were deliberately vague and contained only hints of areas of disagreement. Trump smiled broadly, the pope smiled less, but both agreed, at least for a day, to settle on the same message: the need to avoid conflict.
PHOTOS - POTUS Abroad: Trump Family Tours Vatican, Meets With Pope
A monument of Robert E. Lee, who was a general in the Confederate Army, is removed in New Orleans on May 19.
Lee's was the last of four monuments to Confederate-era figures to be removed under a 2015 City Council vote on a proposal by Mayor Mitch Landrieu. It caps a nearly two-year-long process that has been railed against by those who feel the monuments are a part of Southern heritage and honor the dead. But removal of the monuments has drawn praise from those who saw them as brutal reminders of slavery and symbols of the historic oppression of black people.
An injured fan is helped by police and emergency personnel after an explosion after an Ariana Grande concert at the Manchester Arena on May 22. Children were among the 22 people killed in the suicide attack.
PHOTOS:Deadly Explosion Targets Ariana Grande Concert in Manchester
A little girl holds her newborn sister as they wait for their turn during a vaccination campaign by the Syrian Arab Red Crescent (SARC) in Douma, Syria on May 23. According to SARC, the campaign to prevent measles and poliomyelitis was the first of its kind with the participation of 100 volunteers.
An employee walks past a Soviet-made rotary dredge with the productivity rate of 5,250 tons of processed coal a day during operations at the Beryozovsky opencast colliery, near the Siberian town of Sharypovo, Russia on May 23.
Pupy, an African elephant, stands in the doorway of his enclosure at the former city zoo now known as Eco Parque in Buenos Aires, Argentina on May 12.
A year ago the 140-year old Buenos Aires zoo closed its doors and was transformed into a park. The first director decided that the animals should be housed in buildings that reflected their countries of origin. A replica of a Hindu temple was built for the Asian elephants.
This image was released by The Associated Press this week.
A massive landslide along California's coastal Highway 1 buries the road under a 40-foot layer of rock and dirt on May 22. A swath of the hillside gave way in an area called Mud Creek on Saturday, covering about one-third of a mile, half a kilometer, of road and changing the Big Sur coastline.
PHOTOS: Big Sur Landslides Leave Small Canyon Town Stranded
President Donald Trump touches the Western Wall in Jerusalem's Old City on May 22. Trump became the first sitting U.S. president to visit one of Judaism's holiest sites.The historic visit is part of his efforts to highlight "the need for unity among three of the world's great religions" on his first foreign trip.
Trump stood alone with his hand on the wall for several moments before tucking a note between the cracks. The Western Wall is not officially recognized as Israeli territory, and the Trump administration has not been clear about whether they believe it is part of Israel.
PHOTOS: Trump Tours Holy Land
A woman cries while in a rescue boat after losing her baby in the Mediterranean on May 24 off the coast of Lampedusa, Italy.
More than 30 migrants, mostly toddlers, drowned when about 200 people without life jackets fell from a boat into the sea off the Libyan coast before they could be hauled into waiting rescue boats.
Over 1,300 people have died this year on the world's most dangerous crossing for migrants, after boarding flimsy boats to flee poverty and war across Africa and the Middle East.
Sun shines on a rapeseed field in Muenzenberg, Germany, on May 21.
A baby goat climbs on Shawna Suydam as she participates in a goat yoga class in Glendale, California on May 20.
Newly-wed couples attend a group wedding ceremony in traditional Han Dynasty style at Ganzhou, China on May 20. The date is popular for weddings because the pronunciation of the number "520" is similar to that of "I Love You" in Chinese.
People gather for a candlelight vigil at a bus shelter at the University of Maryland in College Park on May 21, where visiting student Richard Collins III was fatally stabbed.
Collins was killed Saturday in what police are calling an unprovoked stabbing that is being investigated as a possible hate crime.
Ringmaster Johnathan Lee Iverson and his sidekick Paulo dos Santos perform in one of their last Ringling Bros. and Barnum & Bailey Circus shows earlier this month. Iverson was the first African-American ringmaster in the show's history.
After nearly 150 years, the show will not go on for the iconic Ringling Bros., which folded up its Big Top for the last time on May 21.
The show was "one of these wonderful dynamic miracles in the annals of time, and that's where it's going," said Iverson. "It'll be in the memories of many people for years to come."
PHOTOS:Goodbye to the Greatest Show on Earth
A visitor uses a UV flashlight to paint on an installation by Greek artist Vassilis Kambouris at the Digital Arts Festival in Athens on May 20.
The Week in Pictures: May 12 - 19
