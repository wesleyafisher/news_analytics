__HTTP_STATUS_CODE
200
__TITLE
Indiana Church Denies First Communion to Girl Who Wanted to Wear a Suit
__AUTHORS
Chelsea Bailey
__TIMESTAMP
Oct 13 2017, 3:45 pm ET
__ARTICLE
 A 9-year-old girl wasn't able to participate in her first Holy Communion because she wanted to wear a suit. 
 "They said, 'We're hearing rumors so we want to know what she's wearing,'" the girl's mother, Chris Mansell said, adding that she felt the school's newly issued dress code requiring all girls to wear long sleeve white dresses was created to single her daughter out. 
 Her daughter, Cady, has a wardrobe that includes bow ties, suspenders, and a tailored Isaac Mizrahi crushed velvet ensemble that makes her look like Beetlejuice. 
 Cady proudly wears her suits for school pictures, daddy-daughter dances and to mass almost every Sunday. So her mom said she was shocked when the priest at St. John the Evangelist School in St. John, Indiana, told her that Cady could not participate in her first Holy Communion if she wore a suit. 
 Administrators at the school, which is under the jurisdiction of the local Roman Catholic diocese, gave the family a choice: Either Cady wears a dress to the communion, or she could have a separate, private ceremony without her friends and classmates. 
 "He said we're raising our daughter wrong for not making her dress in a feminine way," Mansell said. "That's when I decided then and there to fight because they're purposefully excluding my daughter." 
 With the communion just days away, Mansell posted about the incident to the Facebook group "Pantsuit Nation," and the story quickly went viral. 
 "My daughter just wants to wear pants while worshipping the Lord and receiving the Eucharist with her classmates," Mansell wrote in the post. "She's not hurting anyone. However, being excluded and ostracized IS hurting her. 
 But the priest and school administrators held firm and when the first communion ceremony began, Cady was not allowed to participate. Administrators from St. John did not return a request for comment. 
 The family said they are crushed by the schools decision, and have decided to remove Cady and her sister from the parish and place them in another school. 
 Mansell said she thinks her daughter is too young to understand words like gender nonconforming or why people are suddenly more interested in her wardrobe, but Cady does recognize that what happened to her is important. 
 Ive received calls from tons of mothers of little Cadys, she said. She teaches me a lot about courage and confidence and quirkiness and how all of those things are OK. 
