__HTTP_STATUS_CODE
200
__TITLE
New Heart Imaging Method May Predict Heart Attacks
__AUTHORS
Maggie Fox
__TIMESTAMP
Jul 12 2017, 2:27 pm ET
__ARTICLE
 Researchers say theyve developed a better way of scanning someones heart to predict who is most at risk of a heart attack or stroke  long before conventional imaging methods can do it. 
 By the time someone knows he or she has blocked arteries, its too late to do much more than bypass surgery or putting in a stent to prop open the narrowed vessels. Sometimes a heart attack or stroke is the very first symptom. About 750,000 Americans have a heart attack every year. 
 Dr. Charalambos Antoniades of Britains University of Oxford and colleagues reported Wednesday that they have developed a new imaging method that detects inflamed fat cells as they are transforming into the inflamed, hardened plaques that clog up arteries. 
 Our new method also allows detection of those small but inflamed artherosclerotic plaques in our heart arteries that are prone to rupture, therefore are about to cause a heart attack, Antoniades told reporters in a telephone briefing. 
 If the method holds up, doctors could start patients on drugs such as statins far earlier than they do now  in time to save them from ever developing serious heart disease, the researchers report in the journal Science Translational Medicine. 
 Related: New Test Detects Heart Disease Risk, Especially for Black Women 
 Its not just the fat cells that lead to heart disease, the No. 1 cause of death in the United State. Its inflammation, also. 
 Treatments that are known to reduce the risk of heart attacks, such as statin drugs, reduce inflammation much more than any detectable reduction in coronary artery narrowing, said Oxfords Keith Channon, who also worked on the study. 
 But until now, there's been no way to detect inflammation in the coronary arteries. 
 Related: Half of People Who Have Heart Attacks Dont Even Know It 
 Doctors run many different tests to assess someones risk of heart disease. They measure cholesterol, blood pressure and heart rate, they test the electrical signals that keep the heart beating and they may also run imaging tests to check for clogged arteries. 
 But by the time a narrowed artery shows up on a standard scan, it's likely to be already too late to intervene and reverse the narrowing in the artery that has already occurred over many years, Channon said. 
 By detecting those individuals who don't have narrowing in their heart arteries, but they are in the process in developing them because the arteries are inflamed, we may be able to intervene early enough to prevent heart disease. 
 That's why surgeons do heart bypasses. They graft in a vein to bypass blood flow around the clogged part. 
 Related: Why Young Women are Having Heart Attacks 
 The team studied more than 450 people having heart bypass surgery, removing some of the fat clogging their arteries. They compared them to 270 more having angiograms to look for evidence of clogged arteries. 
 They found a way to identify which layers of fat were inflamed and unstable by looking at the size and shape of the cells using computed tomography (CT) scans. 
 We are now further validating our method in larger numbers of patients in large prospective clinical studies to document and confirm the predictive value of this method for future heart attacks, Antoniades said. 
