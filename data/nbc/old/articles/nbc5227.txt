__HTTP_STATUS_CODE
200
__TITLE
Fish Can Recognize Your Face. Really.
__AUTHORS
Maggie Fox
__TIMESTAMP
Jun 7 2016, 6:25 pm ET
__ARTICLE
 Fish can be trained to recognize human faces, researchers reported on Tuesday. 
 Fish dont have the brain structure, called the neocortex, that people and domestic animals use to recognize faces. But the team at the University of Queensland in Australia managed to train fish to tell one human face from another, anyway. 
 The fish were up to 89 percent accurate in telling apart human faces on a computer screen, the team reported in the journal Scientific Reports. 
 We show that archerfish (Toxotes chatareus) can learn to discriminate a large number of human face images, even after controlling for color, head-shape and brightness, they wrote. 
 The fish were already trained to recognize images on a computer screen suspended over the tank, Cait Newport of Queensland and Oxford universities and her colleagues said. 
 They used archerfish, which spit streams of water to knock down insects and other goodies to eat. This means they need to have good vision to start with. This species, known for knocking down aerial prey with jets of water, relies heavily on vision to detect small prey against a visually complex background and demonstrates impressive visual cognitive abilities, the team wrote. 
 So they trained the fish, rewarding them with food pellets when they got it right. The fish became very good at recognizing the different faces. 
 It must be a different brain function than people and other animals use. 
 There is evidence from a range of studies that some non-primate mammals can discriminate human faces. Species which have been tested include sheep, dogs , cows and horses, the team wrote. 
 Related: Bees See Your Face as a Strange Flower 
 However, most animals tested possess a neocortex and have been domesticated, and may, as a result, have experienced evolutionary pressure to recognize their human carers. There is some evidence that animals lacking a neocortex, namely bees and birds, are capable of some degree of human facial discrimination. 
 Crows and pigeons have been shown to recognize human faces especially well. Crows even recognize people in masks. 
