__HTTP_STATUS_CODE
200
__TITLE
Nearly 4 in 10 Would Steer Kids to Play Sports Other than Football
__AUTHORS
__TIMESTAMP
Jan 30 2015, 7:36 pm ET
__ARTICLE
 Ahead of Sundays Super Bowl showdown, nearly four-in-10 Americans  37 percent  say they would encourage their child to play another sport other than football due to concerns about concussions, according to a newly released result from the most recent NBC News/Wall Street Journal poll. 
 By contrast, 60 percent of respondents say they disagree and would back their children if they wanted to play football. 
 These findings are essentially unchanged from an NBC/WSJ poll from a year ago, when 40 percent said theyd encourage their children to play another sport, while 57 percent wouldnt. 
 In the new poll, the percentage preferring their children play a different sport due to concerns about concussions is higher among seniors (51 percent), those with post-graduate degrees (50 percent), liberals (49 percent), Democrats (47 percent), Obama voters (46 percent), urban residents (40 percent), women (40 percent), and those who dont have children under 18 living in their household (38 percent). 
 And its lower among conservatives (28 percent), those ages 18-34 (28 percent), Republicans (30 percent), Romney voters (30 percent), those with a high school education or less (31 percent), men (32 percent), and those who do have children under 18 living in their household (34 percent). 
 The NBC/WSJ poll was conducted Jan. 14-17 of 800 adults, and it has a margin of error of plus-minus 3.5 percentage points. 
