__HTTP_STATUS_CODE
200
__TITLE
California Wildfires Emitted a Years Worth of Car Pollution in Less Than a Week
__AUTHORS
Elizabeth Chuck
__TIMESTAMP
Oct 13 2017, 3:47 pm ET
__ARTICLE
 The wildfires in California released as much air pollution in the Bay Area in a matter of days as motorists in the state normally emit in a year, drastically reducing air quality and creating an immediate health threat. 
 People most vulnerable to breathing problems, such as the elderly or those with asthma or pulmonary disease, are most likely to be affected, experts say, but healthy people are at risk, too. 
 A half-marathon in San Francisco was canceled because of air issues, and the NFL was considering moving Sunday's game between the Oakland Raiders and the Los Angeles Chargers from Oakland. 
 Hospitals around the Bay Area have also experienced an increase in visits for breathing difficulties and chest tightness, The San Francisco Chronicle reported. 
 The fires released more than 10,000 tons of PM 2.5  particles that are 2.5 micrometers in diameter or smaller and are regulated by the Environmental Protection Agency because of their health impact  said Sean Raffuse, an air quality analyst at the Crocker Nuclear Laboratory at University of California in Davis. He based his estimate off of U.S. Forest Service models. 
 That's the same amount of primary PM 2.5 that the EPA estimates come from California vehicles in a year, according to its most recent National Emissions Inventory, Raffuse said. He pointed out cars pollute in other ways, too, but that the wind has blown this much pollution into a populous area is notable, he said. 
 "In the case of large wildfires, it is not that uncommon. It is the proximity to large populations and the wind direction that is making this smoke event worse from a public health perspective," Raffuse said. 
 The air quality levels in the Bay Area this week have been so bad, they have rivaled those in smog-enshrouded Beijing. 
 PM 2.5, which causes haze and can get into lungs, is measured by the air quality index. On a good day, the index is anywhere from zero to 50. In Napa for the last few days, the air quality index topped 200, which is considered "hazardous." 
 Related: Wineries at Risk as Wildfires Rage in Northern California 
 By comparison, the air quality on Friday around 1 p.m. ET in Beijing was 59, a "moderate" reading by the air quality index's scale. 
 The stifling smoke in Northern California has prompted health advisories, canceled outdoor events, and closed schools for days. 
 "Some of the air quality readings are-off-the-chart bad. There are people who didn't even have issues before that are experiencing issues now," said NBC News meteorologist Bill Karins. 
 On Thursday, the Bay Area Air Quality Management District warned that there were "unprecedented levels of air pollution throughout the Bay Area" and cautioned that air quality could either improve at times or get worse very quickly. 
 Related: Death Toll in California Wildfires Rises; Some Survivors Escaped Just In Time 
 Shifting winds have since lowered the levels of pollutants a bit, but Ralph Borrmann, a spokesman for the Bay Area Air Quality Management District, said the numbers were changing "hour by hour." 
 "The worst air quality would be up in Napa, but other parts of the Bay Area are also getting levels which we don't normally see," he said. 
 The Bay Area Air Quality Management District distributed about 20,000 N-95 masks, special filtration masks that can prevent fine particles from being inhaled, Borrmann said. He recommended that residents impacted by heavy smoke stay inside if it's safe to do so, and take shelter in places with filtered air, such as libraries or malls. 
 The blazes started Sunday and have killed at least 31 people and destroyed about 3,500 structures. 
 Part of what makes the smoke from these particular wildfires so noxious, said Jim Roberts of the National Oceanic and Atmospheric Administration's chemical scientist division of the Earth System's Research Laboratory, is the fact that so many buildings burned down. 
 "Those fuels are completely different in several important respects," Roberts said. "You have a lot of plastics and things like carpeting, foam insulation. Those have their own unique emissions and some of them are a lot more toxic than what we normally find in wildfires." 
 Related: Wildfires in California's Wine Country Hit Vulnerable Immigrant Farmworkers 
 How long the pollution sticks around all depends on the wind direction. But experts don't think it will impact the area in the long run. 
 "Most of the stuff that comes out of these fires is what we call short-lived climate species, things like black carbon particles. They'll have an effect, but it's short-term," Roberts said. 
 Rain could also clear the smoke out, but the area still has a couple of weeks until its rainy season begins, meteorologist Karins said. 
 Ryan Kittell, a forecaster at the National Weather Service in Oxnard, California, said the more progress that is made on containing the fires, the less smoke pollution residents will experience. 
 "Obviously we can't predict how the fires are going to behave in the next 24 or 48 hours," he said. "Certainly we're hopeful that no fires get started and the public heeds the message to be really careful with any fire sources." 
