__HTTP_STATUS_CODE
200
__TITLE
Attorney General Sessions Heading to West Virginia, Epicenter of U.S. Opioid Epidemic
__AUTHORS
Corky Siemaszko
__TIMESTAMP
May 11 2017, 6:10 am ET
__ARTICLE
 The nation's top law enforcement officer is heading Thursday to the state where a resident dies every 10 hours of a fatal drug overdose: West Virginia. 
 Attorney General Jeff Sessions is expected to give the opening remarks at a Drug Enforcement Administration summit on the national heroin and opioid plague that killed at least 864 people in West Virginia alone last year, according to the West Virginia Health Statistics Center. 
 Of those deaths, 731 involved an opioid of some kind, available records shows. In 2015, there were 735 deadly drug overdoses, and 635 of them involved at least one opioid. 
 In his address, Sessions "will expand on the themes about opioid abuse" that he laid out in earlier this year during appearances in New Hampshire and St. Louis, Missouri, Justice Department spokesman Peter Baker said in an email to NBC News. 
 Among other things, Sessions has touted the zero-tolerance approach federal prosecutors used back in the 1980s to combat the crack cocaine epidemic, which includes strategies like putting more people in jail. 
 Sessions has also dusted off Nancy Reagan's iconic "Just Say No" slogan, which was central to an anti-drug advertising campaign that is now considered both outdated and ineffective. 
 "We can turn the tide against drugs and addiction again in America just like we did previously," he said. "We have proven that education and telling people the truth about drugs and addiction will result in better choices. Drug use will fall. Lives will be saved. Less money will be going into cartels and the drug gangs, weakening them." 
 Drug cartels have indeed been smuggling opioids like fentanyl from Mexico and China into the United States, but West Virginia's drug problem was caused by U.S. doctors' over-prescribing highly addictive American-made painkillers. 
 The House Energy and Commerce Committee has opened an investigation into the three major drug distribution companies that are being blamed for flooding West Virginia with prescription painkillers  AmerisourceBergen, Cardinal Health and McKesson. 
 Sessions is also well known for his opposition to legalizing marijuana. But Baker wrote, "We do not expect to address marijuana enforcement at the DEA event this week." 
 While scientists are exploring the possibility of using medical marijuana as a possible weapon against opioid addiction, Sessions openly scoffed at that notion at a National Association of Attorneys General Meeting in February. 
 "Give me a break," he said. "You know, this is the kind of argument that has been made out there. Almost a desperate attempt to defend the harmlessness of marijuana. ... I doubt that's true. Maybe science will prove I'm wrong. My best view is we don't need to be legalizing marijuana." 
 The National Institute on Drug Abuse has funded several studies looking into whether "legally protected access to medical marijuana dispensaries is associated with lower levels of opioid prescribing." 
 So far, the results have been mixed. 
 "The most striking finding was that legally protected marijuana dispensaries (LMDs) were associated with lower rates of dependence on prescription opioids, and deaths due to opioid overdose, than would have been expected based on prior trends," NIDA's Eric Sarlin wrote. "On the other side of the ledger, however, LMDs also were associated with higher rates of recreational marijuana use and increased potency of illegal marijuana." 
 West Virginia is at or near the bottom in many of quality-of-life indicators like percentage of residents with college degrees or availability of good jobs, but it has  by far  the highest rate of fatal drug overdoses in the country, according to the Centers for Disease Control and Prevention. 
 "We are seeing an unprecedented rise in the overdose deaths related to opioids," Dr. Rahul Gupta, commissioner of the state Health and Human Resources Department's Bureau for Public Health, said in March when the grim statistics were released. "It seems we have not yet peaked." 
 The rate of fatal drug overdoses in West Virginia was 41.5 cases per 100,000 in 2015, far outpacing second-place finisher New Hampshire, which had a fatal overdose rate of 34.3 per 100,000, and Kentucky, which was third with a rate of 29.9 per 100,000. 
