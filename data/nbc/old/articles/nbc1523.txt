__HTTP_STATUS_CODE
200
__TITLE
Infographic: Emergency Rooms See Increase in Heroin Patients
__AUTHORS
Ronnie Polidoro
__TIMESTAMP
Apr 8 2014, 8:17 am ET
__ARTICLE
 Heroin abuse has nearly doubled in America in the last five years. Attorney General Eric Holder recently called heroin use in this country an "urgent public health crisis," citing that between 2006 and 2010, heroin overdose deaths have increased by 45-percent. 
 Among single-substance visits, heroin accounted for 258,482 of all emergency department visits involving illicit drugs in 2011, according to the Substance Abuse and Mental Health Services Administration. Heroin came second only to cocaine with 505,224 visits. 
 Experts believe the increase in heroin use, in part, is simple economics. As the nation cracked down on prescription drug abuse, pain pills became harder to come by - driving up price. 
 NBC's National Correspondent Kate Snow will have more on the startling climb of heroin use tonight on "Nightly News with Brian Williams." 
