__HTTP_STATUS_CODE
200
__TITLE
Gerrymandering Vs. Geometry: Math Takes On Partisan Districts
__AUTHORS
Matt Rivera
Justin Peligri
__TIMESTAMP
Jun 29 2017, 5:10 pm ET
__ARTICLE
 Computer-powered math has revolutionized everything from manufacturing to medical care. But while on-demand taxi service is optimized by an algorithm, and some 401(k)s are filled with stocks chosen by a mathematical formula, the information age hasnt done much for democracy, lately. 
 If mathematician Moon Duchin has her way, gerrymandered congressional districts could soon be a thing of the past. Duchin is an Associate Professor of Math at Tufts University where she studies geometric theory. As she explained to Chuck Todd in a recent episode of 1947: The Meet the Press Podcast, her latest project is focused on bringing the power of geometry to solve the problem of gerrymandering. 
 Gerrymandering is one of the oldest challenges facing American Democracy. According to some sources, it dates back to 1788, 2 years before the U.S. Constitution was ratified by the last of the 13 new states. 
 In 2016, Duchin realized that her research in geometry could be used to evaluate the level of gerrymandering in a particular district, whether the region was designed to favor party, race or other factors. 
 Ive always done pure math, the kind that is just really distant from any real world problems, Duchin said. During the 2016 primary elections she started thinking about math and voting, and thats when, Duchin says, she caught the bug. 
 It was natural to think about shapes of voting districts. And since then its kind of been a really deep-dive for me, and a change of direction in her academic career. 
 Duchins work has real-world implications. In October, the Supreme Court will hear arguments about gerrymandering in Wisconsin in a case that focuses on the question of whether partisan mapmaking is unconstitutional. The decision could also set guidelines to determine how partisan is too partisan for designing districts. 
 The first question in the Wisconsin case is whether or not gerrymandering took place. Thats where the math comes in: through the use of a formula called the efficiency gap  a measure of how many votes were wasted in a district because they were packed into districts where their vote was too small to matter  mathematicians are trying to show that certain redistricts are discriminatory. 
 Duchin warns, You cant get at the politics without thinking about it mathematically. And certainly if youre just thinking mathematically, you wont get anywhere. 
 Nicholas Stephanopoulos, a Professor at the University of Chicago Law School, and Eric McGhee, a Research Fellow at the Public Policy Institute of California designed the efficiency gap formula thats going before the court. What the Court says about their proposed mathematical test for gerrymandering could go a long way in determining whether or not the justices feel confident in weighing in on the thorny Constitutional question of who is responsible for designing districts. 
 But even if the efficiency gap formula is adopted, there is so much geographic and demographic diversity across the country that one might need to develop different algorithms for different areas. To explore alternatives, Duchins team at Tufts is hosting a conference in August to explore alternate solutions for detecting gerrymandering, and to look at alternative solutions. 
 Duchin points out that using math to solve gerrymandering isnt the end of the road for manipulative maps. The same formula that could be used to create non-partisan districts could easily be turned on its head and used to create extremely well-crafted partisan voting blocks. 
 She warns that, there are so many ways to gerrymander that if you try to actually look for rules to tweak for each possible one, its hard to stay ahead of all the ways to do wrong. 
