__HTTP_STATUS_CODE
200
__TITLE
David Cassidy, Partridge Family Star, Reveals He Has Dementia
__AUTHORS
Don Melvin
__TIMESTAMP
Feb 21 2017, 7:37 pm ET
__ARTICLE
 Former teen heartthrob David Cassidy has revealed that he is suffering from dementia and has quit performing. 
 The onetime star of "The Partridge Family" spoke to People magazine about his health following a weekend concert in California during which he reportedly struggled to remember the lyrics to songs he has been singing for 50 years. The 66-year-old also appeared to fall off the stage at one point. 
 Cassidy gained fame on the television series, which ran from 1970 to 1974. He played Keith Partridge, the oldest of five children in a musical family that formed a band. 
 He was blindingly handsome, with a fresh-scrubbed look and hair that was long yet perfectly coiffed. After the show's run ended, Cassidy went on to a solo musical career. 
 Cassidy told People that he had watched his grandfather struggle with dementia, and had also seen his mother disappear into dementia until she died at age 89. 
 I was in denial, but a part of me always knew this was coming, he said. 
 On Tuesday, Cassidy sent out a tweet thanking those who had reached out to him following the announcement. 
Thank you to everyone who has reached out offering their love and support.
 "Thank you to everyone who has reached out offering their love and support," he wrote. 
 Over the past decade, Cassidy has struggled with personal problems, including three arrests for drunk driving, divorce and bankruptcy. 
 He joins a growing list of '60s and '70s stars who are now facing dementia. 
 In 2014, Malcolm Young, a founding member of rock band AC/DC, announced through a representative that he was suffering from "a complete loss of short-term memory." He retired from the band and was placed in a home specializing in dementia. 
 And in 2011, country music star Glen Campbell announced he had been diagnosed with Alzheimer's disease. He embarked on a farewell world tour that lasted over a year. Now 80, Campbell lives in a care facility. 
 Earlier this month, Cassidy said on his official website that he would continue touring through the end of 2017 before retiring. 
 But, though his website still lists concert dates for the rest of this year, he told People that he would now quit performing. 
 I want to focus on what I am, who I am and how Ive been, without any distractions, he said. I want to love. I want to enjoy life. 
 
