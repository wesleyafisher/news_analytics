__HTTP_STATUS_CODE
200
__TITLE
Three Men Charged With Plotting ISIS-Inspired Attack in New York
__AUTHORS
Jonathan Dienst
David Paredes
Joe Valiquette
__TIMESTAMP
Oct 6 2017, 4:33 pm ET
__ARTICLE
 Three men have been charged with plotting to carry out ISIS-inspired attacks on music concerts, landmarks and crowded subways in New York, authorities said. 
 The FBI arrested Abdulrahman El Bahnasawy, 19, a Canadian citizen, while traveling to the United States from Canada in the summer of 2016, and he has pled guilty to terrorism offenses; Talha Haroon, 19, an American living in Pakistan, was arrested in Pakistan; and Russell Salic, 37, a Philippine citizen, was arrested in his home country and is also expected to be sent to the United States for trial, federal prosecutors said. 
 Federal prosecutors said the three mens goal was to kill and injure as many people as possible. FBI and NYPD officials said the suspects were arrested before obtaining any weapons  although El Bahnasawy had acquired bomb-making materials and secured a cabin to try to build bombs. They described the plot as inspired by the ISIS terror group but said it was more aspirational than operational. 
 Click Here to Read the NBCNewyork.com Version of This Story 
 Officials said the three men communicated via Internet messaging applications and planned to commit bombings and mass-shootings during 2016s Ramadan in New York City. They allegedly intended to detonate bombs in Times Square and in the citys subway system, while also shooting civilians at specific concert venues. 
 The news comes days after a lone gunman with no link to any terror group opened fire on a concert crowd in Las Vegas, killing 58 people. Terrorists have killed concertgoers in England and France. 
 During their preparations in the spring of 2016, El Bahnasawy and Haroon communicated often via internet messaging on their cell phones. An undercover agent who posed as an ISIS supporter earned their trust and infiltrated their communications, the court documents said. 
 The men sent the agent images of maps of the subway system that were marked with their intended targets. 
 "[W]e seriously need a car bomb at times square (sic)," El Bahnasawy allegedly told the undercover agent after sending him a photo of Times Square. "Look at these crowds of people!" 
 Times Square saw a failed car bomb attempt by self-proclaimed terrorist Faisal Shahzad in 2010 and the 2009 Zazi plot was set to be a series of backpack bombings on the subways. 
 Both men allegedly told the undercover agent that they were connected with an ISIS affiliate in Khorasan Province in Pakistan. According to the court documents, El Bahnasawy told the undercover agent that "[t]hese Americans need an attack" and added that the hoped to "create the next 9/11." 
 Haroon allegedly said that he planned to cause great destruction to the filthy kuffars by our hands. 
 Salic, the individual arrested in the Philippines, provided El Bahnasawy and Haroon financial support, sending the men $423 to fund the attack, the court documents said. FBI officials said he told their undercover that he was "desperate" to join ISIS in Syria. 
 The three men are charged with multiple terrorism charges including conspiring to use a weapon of mass destruction. They face life in prison. 
 Although officials said the plot was discovered months ago, prosecutors were not prepared to reveal the arrests until Friday, in part because federal agents wanted to be certain no others were involved. 
 Former FBI Supervisor JJ Klaver said terrorism investigations require a balance "between protecting the integrity of the investigation and letting the public know the vital information they need to determine whether or not they feel safe going to a particular venue." 
 Sources familiar with the case said this was not a typical law enforcement sting operation, but the FBI and the NYPD became aware of the plot and introduced an undercover operative to help monitor it. 
