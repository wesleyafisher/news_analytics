__HTTP_STATUS_CODE
200
__TITLE
Robert Durst Had Map of Cuba When He Was Busted
__AUTHORS
__TIMESTAMP
Mar 23 2015, 7:13 pm ET
__ARTICLE
 Multimillionaire murder suspect Robert Durst had $117,000 in cash mailed to his New Orleans hotel room, where investigators also found a map of Cuba, an investigator testified Monday during a bail hearing. 
 The 71-year-old real-estate heir who was the subject of HBOs The Jinx failed in his bid to be released from jail a week after he was hit with a double-whammy of charges  a murder rap out of California and a drug-and-gun case from Louisiana. 
 Still, defense lawyer Dick DeGuerin claimed victory, saying he had learned new details of the prosecution's case and secured a date for a preliminary hearing. 
 "I didn't have any hope at all the judge was going to set a bail bond," he said outside the courthouse. "All in all, I think this has been a very good day for us." 
 The three-hour court session on whether Durst should stay behind bars revealed new details about his March 14 arrest, including how the FBI and LAPD were keeping tabs on him when he left his Houston apartment with five pieces of luggage earlier this month. 
 Clad in an orange jumpsuit, Durst looked frail as he heard how investigators trying to tie him to the 2000 murder of his Beverly Hills confidant, Susan Berman, tracked his cellphone pings as he drove across Texas. 
 They asked the Louisiana state police to use license plate readers to track whether he crossed state lines, but cops never got any hits on his three cars. Instead, the LAPD discovered he was at the J.W. Marriott in New Orleans because he called his voicemail twice from there. 
 After he was arrested for the murder of Berman on a warrant from Los Angeles, Durst was hit with other charges by New Orleans authorities who say they found a .38 caliber Smith & Wesson and five ounces of pot in his room. The inventory also included a chest-length "flesh-toned mask with salt and pepper hair" and 446 $100 bills. 
 Officials said a parcel been shipped to him at the hotel with $117,000 in cash in it. He also had several maps, including one for Cuba, which does not recognize an extradition treaty with the United States. 
 Dursts lawyers say he's innocent and that his March 14 bust was orchestrated to coincide with the finale "The Jinx"  which chronicled the 1982 disappearance of his first wife, Bermans murder, and the dismemberment of an elderly neighbor Durst says he killed in self-defense. 
 One of the key characters in the HBO production is TV personality Jeanine Pirro, who investigated the disappearance Kathie Durst as the Westchester County, New York, district attorney and who was in the courtroom for the start of the hearing. 
 After the defense objected that she was a potential witness  and said it would call her  she was ordered out. She later returned with her lawyer, Mary Ellen Roy, who said she was there as a journalist and should be allowed to stay. 
 "Frankly, I don't buy that, judge," Durst's lawyer, Dick DeGuerin, said. 
 Assistant District Attorney Chris Bowman countered that the defense had "made Ms. Pirro into a bogeyman." 
 After a brief recess, the judge ruled that Pirro would not testify at the hearing and could return to the courtroom, and testimony resumed about the search of the hotel room. 
 Legal experts say felony charges stemming from the gun and drugs found there could prove to be a bigger headache than the harder-to-prove murder rap for the 71-year-old Durst. 
 "If Robert Durst is sentenced to 20 years clearly, 15 years, even 10 years, in his physical and mental condition and his age, it is essentially a life sentence," New Orleans criminal defense lawyer Chick Foret, who is not representing Durst, told NBC News. 
