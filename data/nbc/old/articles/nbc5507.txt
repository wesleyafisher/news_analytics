__HTTP_STATUS_CODE
200
__TITLE
The App Store Turns 9 With Big Changes on the Horizon
__AUTHORS
Alyssa Newcomb
__TIMESTAMP
Jul 10 2017, 2:00 pm ET
__ARTICLE
 Apple's big cash cow  OK, one of them  is celebrating its ninth birthday. 
 The world got its first look at Apple's App Store on July 10, 2008. Since then, it's generated an estimated $100 billion in revenues, of which about $70 billion has been paid out to developers and $30 billion has gone to Apple. 
 The App Store not only changed how we interact with  and rely on  our phones, but it also gave rise to a new economy. Now, anyone with coding skills has a chance to make create apps  and reap the rewards. 
 "It used to be: I have to be a major studio, multi-million dollar company," Tuong Nguyen, principal research analyst at Gartner, told NBC News last month. 
 That app economy has of course spawned ideas that have changed other industries. Think of how often you use Uber or Lyft to get a ride, or a fitness app to record your runs, or a social media network to keep in touch with friends. 
 Related: The iPhone Just Turned 10 and Its Still Changing Everything 
 With developers still dreaming up new apps and experiences to work alongside expanded capabilities on the iPhone, the app economy, which was worth $1.3 trillion last year, is showing no signs of slowing down. 
 In five years, the app economy will be worth $6.3 trillion, according to a report from App Annie, as more people continue to spend money on and within apps. 
 The App Store is also getting a new look that might entice you to spend a little more. With the launch of iOS 11, the App Store will debut with redesigned pages, separate games from apps, and have a "today" tab, allowing for more discovery within the store. 
