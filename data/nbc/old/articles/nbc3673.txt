__HTTP_STATUS_CODE
200
__TITLE
Pink: Stronger Than Heroin, but Legal In Most States
__AUTHORS
Andrew Blankstein
__TIMESTAMP
Oct 15 2016, 5:05 am ET
__ARTICLE
 Editor's Note: An earlier version of this story included a photo of Ryan Ainsworths brother and mistakenly identified him as Ryan, who is deceased. NBC News has removed the photo and regrets the error. 
 Two 13-year-old boys in the ski town of Park City, Utah died within 48 hours of each other in September, likely overdosing on a powerful heroin substitute that had been delivered  legally  to their homes by the U.S. mail, and is now turning up in cities across the nation. 
 Ryan Ainsworth was found dead on his couch two days after his best friend Grant Seaver passed away. I wish I had been better warned, sang one of their friends at a massive memorial service. But now its too late. 
 The death toll could have been worse, say investigators, since as many as 100 Park City students had apparently been discussing the drug Pink on SnapChat and other social media. 
 This stuff is so powerful that if you touch it, you could go into cardiac arrest, Park City Police Chief Wade Carpenter told NBC News. The problem is if you have a credit card and a cell phone, you have access to it. 
 Pink, better known by chemists as U-47700, is eight times stronger than heroin, and is part of a family of deadly synthetic opioids, all of them more powerful than heroin, that includes ifentanyl, carfentanil and furanyl fentanyl. By themselves or mixed with other drugs, in forms ranging from pills to powder to mists, theyre killing thousands of people across the country, say law enforcement and health officials. The powerful, ersatz opioids are part of a surge of synthetic drugs, including bath salts and mock-ups of ecstasy, being shipped into the U.S. from China and other nations. 
 So far, however, only four states have made Pink illegal. It can still be ordered legally on-line and delivered to your home. The internet has many websites a Google search away where the drug is available for as little as $5 plus shipping. 
 Melissa Davidson, mother of a Park City teen who had friends in common with the dead boys, showed NBC News on her home computer screen how easy it was to find the drug for sale with just a few keystrokes. "Look! There are like pages and pages that you can buy this stuff online." 
 According to the U.S. Centers for Disease Control, total opioid overdose deaths nearly quadrupled between 1999 and 2014, rising from 8,050 to 28,647. The portion of those deaths caused by synthetic opioids, however, rose almost twice as fast, from just 730 in 1999 to 5,544 in 2014. 
 Because of the surge in opioid-related deaths, and the regular appearance of new synthetics on the market, there is a time lag in toxicology reports from coroners, and the possibility that some deaths are mistakenly linked to other, better known substances. But Pink, a relative newcomer among the synthetics, has been implicated in 80 deaths across the country in just the past nine months, according to Pennsylvania-based NMS Labs, which conducts forensic toxicology tests. 
 The Drug Enforcement Administration said it is aware of confirmed fatalities associated with U-47700 in New Hampshire, North Carolina, Ohio, Texas, and Wisconsin. Though its own tally is only 15 deaths, an agency spokesperson said the number was probably higher because of challenges and delays in reporting. 
 On Sept. 7, the DEA took initial steps toward banning the drug nationally by giving notice of its intent to schedule the synthetic opioid temporarily as a Schedule 1 substance under the federal Controlled Substances Act. 
 Some states arent waiting for a permanent federal ban. In late September, Florida Attorney General Pam Bondi signed an emergency order outlawing the drug after it was tied to eight deaths in recent months. Florida joins Ohio, Wyoming and Georgia in outlawing the compound and other states are looking to do the same. 
 In some states, law enforcement is just learning about a threat that is especially challenging because so many transactions are done by computer and through the mail. And the chemists who manufacture the drugs can invent new variants as fast as the states can outlaw them. 
 The hardest part is when something new comes up, and no one in the country or world has seen it in a forensic setting yet and trying to decide what that actual structure or drug is, said Bryan Holden, senior forensic scientist with the Utah Department of Public Safety. Sometimes we have had cases where the substance sat for months and months -- no one had ever seen it before, and until someone else sees it or manufactures it then we kind of know what it is. 
 The DEA has been using so-called temporary bans more and more often to combat designer synthetic drugs have made their way into the U.S. from China and other parts of the world. The U47700 ban allows them three years to research whether something should be permanently controlled or whether it should revert back to non-controlled status. 
 But experts say the most effective prevention may start in the home, at the computer and the mailbox. 
 Im worried about you, Melissa Davidson told her 17-year-old daughter Jane. 
 Jane, however, was worried about her friends at school. I cant imagine the kids Im in math class with, just not being there one day. One bad decision can have permanent consequences. 
