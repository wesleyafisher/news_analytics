__HTTP_STATUS_CODE
200
__TITLE
Terry Crews Revelation Sheds Light on Black Male Sexual Victimization
__AUTHORS
Foluke  Tuakli
__TIMESTAMP
Oct 12 2017, 6:07 pm ET
__ARTICLE
 As female celebrities continue to come forward alleging sexual assault by the Hollywood movie mogul Harvey Weinstein, male actors such as Terry Crews are also speaking out on their experiences with sexual assault. 
 On Tuesday, theactor andformer bodyguard posted a series of tweets illustrating his experience with sexual harassment, showing that victims of sexual violence come in every size, gender, and color. 
 Reports estimate that one in six men in the U.S. have experienced sexual violence in their lifetime with one study conducted by the Centers for Disease Control and Prevention found that 16 percent of males were sexually victimized by the age of 18. 
 According to the Bureau of Justice Statistics National Crime Victimization Survey, 11 percent of total sexual assault victims are male. 
This whole thing with Harvey Weinstein is giving me PTSD. Why? Because this kind of thing happened to ME.  (1/Cont.)
 David Lisak, public speaker and co-founder of 1in6, a national nonprofit devoted to helping men who were sexually abused as children, said coming forward is "a gift to other men and a gift to that mans community." As a survivor of childhood sexual abuse, Lisak said he also understands the importance of talking about these experiences. 
 "It shows other men who have suffered sexual violence that it is possible to come forward," he said. "To the individual that comes forward, it is also an act of liberation, essentially casting off that shame that was really never his to carry anyways." 
 According to Lisak, the inordinate pressure to uphold manhood essentially makes men safer targets for predators, adding, There is no question that women and girls are more frequently victimized sexually, but the number of men who are victimized is actually really substantial. 
 Race also adds another layer to the subject. According to a report by the Bureau of Justice Statistics, African-Americans are at a greater risk of rape or sexual assault than other racial/ethnic groups (except American Indians), with even higher risks among low-income and urban residents. 
 Related: James Van Der Beek, Terry Crews Back Harvey Weinstein Accusers 
 Josh Zeke Thomas, son of NBA legend Isiah Thomas, knows this very well. The recording artist and DJ said he was sexually assaulted by his basketball teammates when he was 12, and again in his 20s by a man he met on Grindr. 
 He initially kept what happened to him from his friends and family. The stigma being a strong black man is not just to be mentally strong, but also to be physically dominant ... having someone come and take your manhood by being raped  its the loss of that strength. Its the loss of blackness, Thomas told NBC News. Thats not right, but it is something that is ingrained in being black  you have to have thick skin. 
A photo posted by NBC News (@nbcnews)
 In No Secrets, No Lies: How Black Families Can Heal From Sexual Abuse, author and journalist Robin D. Stone cites African-Americans distrust of institutions, fear of being perceived as gay, and strong beliefs about concealing family business lead to a lack of reporting among black men. 
 Thomas is currently on a cross-country college tour speaking about sexual assault. As the first male ambassador for the National Sexual Violence Resource Center, Thomas said he's working hard to mitigate stigma and foster support for victims of all demographics  work that begins with open conversations. 
 I wanted to thank Terry for lending his voice to a hard topic  to a topic that were going to solve if we keep on bringing awareness to this issue, Thomas said. 
 Follow NBCBLK on Facebook, Twitter & Instagram 
