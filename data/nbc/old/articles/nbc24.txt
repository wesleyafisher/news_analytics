__HTTP_STATUS_CODE
200
__TITLE
College Student Crowdfunds $82,000 for Puerto Rico Hurricane Relief
__AUTHORS
Yelena Dzhanova
__TIMESTAMP
Oct 15 2017, 7:34 pm ET
__ARTICLE
 Puerto Rico is still in shambles three weeks after Hurricane Maria, but a 22-year-old college student is going above and beyond to help. 
 The most recent aid to Puerto Rico after Maria was delivered by Rosana Guernica, a junior at Carnegie Mellon University in Pittsburgh. Guernica raised tens of thousands of dollars through crowdfunding and reserved a plane to deliver emergency supplies to Puerto Rico, her home. She bought thousands of pounds of supplies, such as baby formula, water, batteries and medicine. 
 Guernica started soliciting donations on YouCaring, a crowdfunding site, one week after Maria hit, and she raised $7,000 in 24 hours, according to The Associated Press. By the following day, she had raised $11,000. So far, 786 donors have contributed to the cause, and donations exceed $82,000. Her YouCaring page was shared 1,800 times on Facebook. 
 "We are desperate to help the island and don't seem to know how. No one person can solve these issues. No one government can help the island," Guernica wrote in a description on YouCaring. 
 On Oct. 4, she used $20,000 of the donations to buy and transport more than 1,000 pounds of supplies to Puerto Rico. She also returned with six evacuees. 
 Guernica boarded a plane to Puerto Rico with a handful of volunteers from Carnegie Mellon and 2,000 pounds of supplies on Saturday. It was her second trip to the island since her crowdfunding efforts began, according to a tweet sent out through Carnegie Mellon's official Twitter account. 
CMU student Rosana Guernica is on her 2nd trip to #PuertoRico today w/team of CMU volunteers! They're bringing supplies & evacuating people. pic.twitter.com/kYEQ1v0Qn4
 "The supplies being collected are being held in inventory throughout the US and in the ports of San Juan," Guernica wrote in the YouCaring description. "By the time normal distribution channels open, it will be too late for the people who needed it most." 
 Three weeks after Maria, 90 percent of the island's residents still lack power. The storm hit the island with strong gusts of winds traveling at 155 mph. Residents have been struggling with food and water shortages, in addition to downed infrastructure. 
