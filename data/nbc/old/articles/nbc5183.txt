__HTTP_STATUS_CODE
200
__TITLE
As Heroin Epidemic Grows, So Does Rehab Wait 
__AUTHORS
Charlie Gile
__TIMESTAMP
Oct 21 2015, 4:26 pm ET
__ARTICLE
 As the U.S. heroin epidemic grows, so does the wait list for federally-funded rehab. 
 President Barack Obama listened to family members talk about the anguish caused by addiction when he traveled to Charleston, West Virginia on Wednesday to participate in a community discussion on the prescription drug abuse and heroin epidemics. The president also announced that federal agencies will work to ensure that prescribers of opiate-based painkillers, which are often a gateway to heroin, get better training on the risks. 
 "We want to make sure the whole country understands how urgent this problem is," the president said. 
 In Massachusetts, substance abusers have to wait weeks to get help. In Florida, it's a month. 
 Wait times are as long as 18 months in Maine. 
 One Ohio woman couldnt wait. Thats why she asked a judge to send her to jail so she could get clean. Theres no help out there anymore, Kayla Dempsey told NBC affiliate WFMJ. Theres a three-month waiting list for any rehab around here because of the heroin epidemic. 
 Portland, Maine resident Shawn Cross spent four months in jail to kick his opioid habit. The jails here are basically detox facilities, Cross told the Portland Press Herald. 
 A report from Snohomish County jail in Washington said that heroin had caused the jail to become the countys largest de facto detox center over the last two years. 
 In Manatee County, Florida, recovering opioid addict Brandilyn Karnehm told the Bradenton Herald that rehabilitation clinics are key. Jail is not rehabilitation or recovery, Karnehm says. Yeah, you get clean some of the time, but you dont get any of the tools that you need to stay clean. 
 Nationwide, only 11 per cent of substance abusers get help from treatment centers, according to a 2012 study in American Journal of Drug and Alcohol Abuse. Indeed, the individuals in need of treatment cite long wait lists as a primary reason for not accessing it, the study says. 
 Even when people sign up for wait lists, they will only tolerate one month on average. 40 per cent of people on a wait list will drop off in two weeks, according to a 2008 study. 
 Thats why leaders in the political and entertainment world are advocating an intervention approach that focuses early on treatment. 
 Related: White House Announces Program to Combat Rise in Heroin Deaths 
 In August, in response to the national rise in fatal heroin overdoses, the Obama administration unveiled a plan aimed at emphasizing treatment rather than prosecution of addicts. 
 The program would initially be funded for $2.5 million by the White House Office of National Drug Control Policy through five "High Intensity Drug Trafficking Areas" and cover 15 states, administration officials said. 
 The plan would focus on tracing the sources of heroin, where a deadly opiate additive is blamed for a rising share of recent overdose deaths is being added. The plan also pairs law enforcement officials with public health workers in an effort to address the causes of the problem. 
 The initiative came in reaction to that sharp increase in heroin use and deaths, particularly in New England and other Northeastern states, which will be covered in the plan. Heroin overdose deaths in the United States nearly quadrupled between 2002 and 2013, fueled by lower costs as well as increased abuse of prescription opiate painkillers, U.S. health officials said in July. 
 The White House has also named September National Alcohol and Drug Addiction Recovery Month. 
 The heroin epidemic is so acute that politicians campaigning in the areas hardest hit often reference the trend in their stump speeches. 
 Republican presidential candidate Carly Fiorina has talked openly about losing a stepdaughter to alcohol and prescription drug addiction. Democratic presidential candidate Hillary Clinton and Republican contender Sen. Rand Paul of Kentucky have addressed the issue in speeches and have vowed to make the matter a key aspect of their campaigns. 
 Republican candidates Gov. Chris Christie of New Jersey and former Florida Gov. Jeb Bush have also addressed the issue. 
 Related: Pregnant and Hooked: How One Program Helps Heroin Addicts  
 On Capitol Hill in August, Piper Kerman of Orange is the New Black fame echoed the same sentiments as she testified at a House Oversight hearing on the Bureau of Prisons. Intervening in that addiction cycle is the single most important thing, Kerman says. It cant be accomplished with a prisoner in a jail cell. 
 After a rash of heroin overdose deaths in the area, one Massachusetts police department is taking an innovative approach to making sure that substance abusers can get help immediately. 
 The Gloucester Police Department adopted a policy in May where addicts seeking help will not be charged with a crime and will be immediately placed in a rehabilitation program. 
 We will assign them an "angel" who will be their guide through the process, Gloucester Police Chief Leonard Campanello wrote in a Facebook post this summer announcing the policy. Not in hours or days, but on the spot. The department works with two area clinics to accommodate people looking to take advantage of this policy. 
 As a part of the program, nasal Narcon, a drug that counteracts opioid overdoses, was made available at no cost. The police department pays for the drug with money seized from drug dealers. We will save lives with the money from the pockets of those who would take them, Campanello said on Facebook. 
 Since the program started in June, 109 people have received treatment at a total cost of about $5,000 to the department. 
 People are desperate. Addiction is a disease of desperation, said Melinda Campopiano, medical officer for the Substance Abuse Treatment at the Substance Abuse Mental health Services Administration (SAMHSA). 
 Campopiano said that people seeking help may be able to shorten their rehab wait times by simply widening their search using tools online. Look at the SAMHSA treatment locator, Campopiano said. Often, peoples knowledge of what treatment is available in their community is limited to what theyve heard about through somebody else. 
 Addicts detoxing in jails is not ideal but inevitable, Campopiano said. In that case, jails need to be given the tools to effectively handle substance abusers as they go through withdrawals. 
 Jails are already providing an important part of the response to opiate use and overdose, Campopiano said. Certainly Gloucester is a standout. 
 Although it may seem like substance abusers are taking extreme measures, Campopiano said it is important for officials to empathize with those seeking help. 
 If we could get inside their skin, we wouldnt think that they were being at all unreasonable. 
