__HTTP_STATUS_CODE
200
__TITLE
Baby Dies Days After Parents Suspected Drug Overdose Deaths
__AUTHORS
Associated Press
__TIMESTAMP
Dec 24 2016, 3:31 pm ET
__ARTICLE
 JOHNSTOWN, Pa.  An infant died in her bassinet of dehydration and starvation three or four days after her parents died, also at home, from suspected drug overdoses, authorities said. 
 Jason Chambers, 27, Chelsea Cardaro, 19, and 5-month-old Summer Chambers were found dead Thursday in Kernville, about 60 miles east of Pittsburgh. 
 Cambria County coroner Jeffrey Lees said that the parents had been dead for about a week when the discovery was made. 
 The infant died three or four days after them, he said. 
 Related: America's Heroin Epidemic 
 Jason Chambers was found on the first floor, and Cardaro in a second-floor bathroom, authorities said. The infant died in her bassinet in a second-floor bedroom, they said. 
 Toxicology tests are pending for the adults but officials said heroin overdoses are suspected and there was evidence of drugs at the scene. 
 District Attorney Kelly Callihan said Friday that she believed that the man and woman died within minutes of each other. 
 "We think that because, if not, one or the other would have called for help," she said. 
 The family was last seen on Dec. 11. Neighbors believed they had taken a planned trip to New York, where they had lived until recently. 
 Related: Heroin 'Safe Zones': Coming to the United States? 
 "It's an unfortunate incident where they both possibly overdosed at the same time - and being from out of town, not having anybody in town - it was too long for anybody to notice that they were missing," said Johnstown police Capt. Chad Miller. 
 Miller said emergency responders had been called to the home in November to treat the man after an overdose. 
 Child welfare workers later went to the home and met with the mother and father, with the baby present, she said. 
 "They checked out the house, and it was appropriate to a child living there. There was plenty of food and the child seemed well taken care of," she said. 
