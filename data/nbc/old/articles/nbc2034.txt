__HTTP_STATUS_CODE
200
__TITLE
Patient Gets Seizure From Solving Sudoku Puzzle
__AUTHORS
Felix Gussone, MD
__TIMESTAMP
Oct 19 2015, 12:03 pm ET
__ARTICLE
 Sudoku puzzles give your brain a hard time: Every digit from 1 to 9 must appear in each of the nine horizontal rows, in each of the nine vertical columns and in each of the nine boxes. 
 For many of us, this can be a reason for a headache, but in the very rare case of a German man, a Sudoku puzzle even caused a seizure. 
 In a new case study from the University of Munich, published in the Journal of the American Medical Association, Dr. Berend Feddersen introduces a student who was 25 years old when he was buried by an avalanche during a ski tour. For 15 minutes, he didn't get sufficient oxygen, which irreversibly damaged certain regions of his brain. 
 "He had to be resuscitated, but was extremely lucky that he survived," says Feddersen, the author of the study. 
 Weeks after the accident, when the young man was ready to go to rehabilitation, something bizarre happened: When the patient solved Sudoku puzzles, he suddenly had seizures of his left arm  something the medical world hadn't seen before. 
 Go here for video of the Sudoku-related seizure 
 Feddersen explains: "In order to solve a Sudoku, the patient used regions of his brain which are responsible for visual-spatial tasks. But exactly those brain parts had been damaged in the accident and then caused the seizures once they were used." 
 This particular case is an example of what doctors call reflex epilepsy, according to Dr. Jacqueline French, professor of neurology at NYU Langone School of Medicine. 
 "You have to have the epileptic focus first  for example because of an injury of your brain  and then seizures like that can happen," she says. 
 In the meantime, the patient from the case study stopped solving Sudoku puzzles for good and has been seizure-free for more than five years. "Fortunately, he can do crossword puzzles. He never had problems with those," Feddersen says. 
