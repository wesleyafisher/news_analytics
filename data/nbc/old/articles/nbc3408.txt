__HTTP_STATUS_CODE
200
__TITLE
Country Star Jason Aldean Resumes Tour After Vegas Shooting
__AUTHORS
Associated Press
__TIMESTAMP
Oct 13 2017, 12:56 am ET
__ARTICLE
 TULSA, Okla.  Country star Jason Aldean brought the party back Thursday in his return to the stage following the deadly mass shooting that broke out while he was performing in Las Vegas, but the fun was tempered by the sting of the tragedy. 
 Three songs into his show in Tulsa, Oklahoma, the singer launched into a five-minute speech that honored the 58 killed and nearly 500 hurt in the deadliest mass shooting in modern U.S. history. But Aldean took a defiant tone in telling concertgoers to resist living in fear, and he called for more of the national unity he's seen since the attack. 
 "These people are going to continue to try to hold us down," Aldean said. "To those people that keep trying to do that, I say (expletive) you, we don't really care." 
 Fans agreed. They pumped their fists and held up American flags as Aldean continued. 
 "I want to play the show for you guys that the people in Las Vegas came to see and didn't get a chance to," he said. 
 Aldean canceled shows in California last week to mourn those killed Oct. 1 at the outdoor Route 91 festival. He resumed his tour in an arena where concertgoers walked through metal detectors, and police presence was visible. 
 The singer visited shooting victims still in a Las Vegas hospital Sunday. The day before, Aldean performed Tom Petty's "I Won't Back Down" on "Saturday Night Live" in tribute to the victims and the late rock superstar. 
 Petty died the day after the shooting in Los Angeles after suffering cardiac arrest. 
 Authorities have said Stephen Paddock targeted the country music festival, opening fire from the 32nd floor of the Mandalay Bay hotel before killing himself. They are still trying to determine a motive. 
