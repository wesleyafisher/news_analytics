__HTTP_STATUS_CODE
200
__TITLE
U.S. Settles Suit Over Trumps First Travel Ban, Allowing Blocked Travelers to Reapply
__AUTHORS
Alex Johnson
__TIMESTAMP
Sep 1 2017, 12:11 am ET
__ARTICLE
 Travelers who were barred from entering the United States during the less than 24 hours that President Donald Trump's first executive order on immigration was in effect can reapply for visas under a court settlement filed Thursday. 
 The settlement  which was agreed to over a conference call Tuesday and filed Thursday in U.S. District Court in Brooklyn, New York  addresses the predicaments of an undisclosed number of travelers who were blocked from entering the country in the hours between Trump's issuance of the travel order on Jan. 27 and a federal judge's issuance of a temporary injunction the next day halting enforcement of the order. 
 The January suit was filed by Iraqi refugees Hameed Khalid Darweesh and Haider Sameer Abdulkhaleq Alshawi. Because it was a class action, the injunction temporarily halted enforcement of the travel order, which sought to block admission of people from Iraq and six other predominantly Muslim nations for 90 days and was characterized by opponents as a "Muslim ban." 
 The Supreme Court is scheduled to hear arguments in October on a challenge to a second, narrower order that Trump issued in March. 
 Darweesh was an interpreter working on behalf of the U.S. government in Iraq from 2003 to 2013 and had received a special Iraqi immigrant visa to relocate to the United States. Alshawi was barred even though his wife and child were already lawful permanent U.S. residents. 
 In agreeing to settle the original litigation, the Justice Department said Thursday it would notify people overseas who were blocked on Jan. 27 and 28 that they have three months to reapply for visas with Justice Department assistance. Darweesh and Alshawi dropped their claims and agreed not to seek attorneys' fees and other legal costs. 
 In an op-ed published in The Washington Post after his detention and rejection in January, Darweesh wrote, "This was not the America I knew." But Thursday, he said, "The United States is a great country because of its people." 
 "I'm glad that the lawsuit is over," he said in a statement issued through the American Civil Liberties Union, one of several civil rights groups that represented him and Alshawi. "Me and my family are safe; my kids go to school; we can now live a normal life. I suffered back home, but I have my rights now. I'm a human." 
 In a brief statement, the Justice Department said: "Although this case has been moot since March, when the president rescinded the original executive order and issued a new one that does not restrict the entry of Iraqi nationals, the U.S. government has elected to settle this case on favorable terms." 
 Lee Gelernt, deputy director of the ACLU's Immigrants Rights Project, said that while the settlement "closes one chapter in our challenge to Trump's efforts to institute his unconstitutional ban, we continue our legal fight against Muslim ban 2.0 at the Supreme Court in October." 
