__HTTP_STATUS_CODE
200
__TITLE
Weinstein Co. in Talks for Sale to Colony Capital Following Scandal
__AUTHORS
Claire Atkinson
__TIMESTAMP
Oct 16 2017, 1:23 pm ET
__ARTICLE
 Harvey Weinsteins movie and TV company got a lifeline on Monday. 
 Private equity firm Colony Capital, run by Thomas Barrack, said in a statement that it is negotiating with Weinstein Co., for a "potential sale of all or a significant portion, of the companys assets. 
 Weinstein Co., which makes TV series Project Runway, for A&E Networks Lifetime, and has produced a string of Oscar-winning movies, found itself in dire straits after it lost key partners such as Amazon after a series of women made allegations of sexual assault and harassment by its CEO Harvey Weinstein. Weinstein has denied having any non-consensual relationships. 
 Colony Capital said in a statement that it already has a preliminary agreement to provide an immediate capital infusion, into the New York-based company. 
 The Weinstein Company is holding a board meeting Tuesday to decide how to proceed. Co-founders Bob and Harvey Weinstein own 42 percent of the firm. The balance of Weinstein Co. is owned by advertising firm WPP Group and luxury goods company, LVMH among others. 
 On Friday, according to a source familiar with talks, said Weinstein would contest his firing from the company. He is going to call into the board meeting on Tuesday to argue over the board's decision to fire him, a knowledgeable source told NBC News. 
 Board member, Tarak Ben Ammar said in a statement released to the press: We are pleased to announce this agreement and potential strategic partnership with Colony Capital. We believe that Colonys investment and sponsorship will help stabilize the Companys current operations, as well as provide comfort to our critical distribution, production and talent partners around the world. 
 Barrack is a friend to President Donald Trump and was a major financial backer of his presidential campaign before leading his inaugural committee. 
