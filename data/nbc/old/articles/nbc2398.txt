__HTTP_STATUS_CODE
200
__TITLE
War on ISIS: U.S.-Backed Iraqi Forces Squeeze Fighters in Mosuls Old City
__AUTHORS
Saphora Smith
__TIMESTAMP
Jul 1 2017, 7:14 am ET
__ARTICLE
 U.S.-backed forces squeezed ISIS militants holding out in their Iraqi stronghold of Mosul Saturday as more civilians fled the intense fighting. 
 Troops have been battling for months to liberate the ISIS-held city with fighting concentrated in the narrow streets of Mosuls Old City in recent weeks. 
 Col. Patrick Work, who commands most of the American advisers helping to fight ISIS, told NBC News Chief Foreign Correspondent Richard Engel that U.S.-backed troops had delivered severe blows to the militants. 
 This is a catastrophic setback for ISIS, no matter what the enemy says. They cant claim glory and get defeated by the Iraqi security forces, he said. 
 Iraqi Prime Minister Haider al-Abadi already declared victory over the caliphate in Iraq Thursday after Iraqi troops captured the destroyed Grand al-Nuri Mosque. 
 Related: ISIS Revenue Plummets as Militants Lose Ground in Iraq, Syria 
 The capture of the 850-year-old mosque was considered a big symbolic victory to Iraqis. It is where ISIS leader Abu Bakr al-Baghdadi announced its caliphate almost three years ago. 
 The return of Nuri mosque and minaret of Hadba today after being [destroyed] by ISIS marks the end of Daesh State falsehood, said al-Abadi, adding we will continue chasing ISIS until we kill and arrest [every] last one of them. 
 Fighting remained intense, however. Commanders of counter-terrorism units fighting their way through the warren of narrow streets say die-hard ISIS fighters are dug in among civilians and the battle ahead remains challenging, according to Reuters. 
 Work told NBC News it is hard to know how many ISIS fighters were left inside the Old City. 
 Im not sure it really matters because the Daesh fighters that are in there are going to fight to the death, he said, referring to ISIS by an Arabic acronym. 
 Tens of thousands of civilians remain trapped in the city with dwindling supplies of food, water and medicine, according to Reuters. 
 Saphora Smith reported from London. 
