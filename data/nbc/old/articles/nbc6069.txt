__HTTP_STATUS_CODE
200
__TITLE
Hackers Breached American and European Power Companies, Utilities
__AUTHORS
Reuters
__TIMESTAMP
Sep 6 2017, 7:13 am ET
__ARTICLE
 Advanced hackers have targeted United States and European energy companies in a cyber espionage campaign that has in some cases successfully broken into the core systems that control the companies' operations, according to researchers at the security firm Symantec. 
 Malicious email campaigns have been used to gain entry into organizations in the United States, Turkey and Switzerland, and likely other countries well, Symantec said in a report published on Wednesday. 
 Related: The Hacking of America 
 The cyber attacks, which began in late 2015 but increased in frequency in April of this year, are probably the work of a foreign government and bear the hallmarks of a hacking group known as Dragonfly, Eric Chien, a cyber security researcher at Symantec, said in an interview. 
 The research adds to concerns that industrial firms, including power providers and other utilities, are susceptible to cyber attacks that could be leveraged for destructive purposes in the event of a major geopolitical conflict. 
 In June the U.S. government warned industrial firms about a hacking campaign targeting the nuclear and energy sectors, saying in an alert seen by Reuters that hackers sent phishing emails to harvest credentials in order to gain access to targeted networks. 
 Chien said he believed that alert likely referenced the same campaign Symantec has been tracking. 
 He said dozens of companies had been targeted and that a handful of them, including in the United States, had been compromised on the operational level. That level of access meant that motivation was "the only step left" preventing "sabotage of the power grid," Chien said. 
 However, other researchers cast some doubt on the findings. 
 While concerning, the attacks were "far from the level of being able to turn off the lights, so there's no alarmism needed," said Robert M. Lee, founder of U.S. critical infrastructure security firm Dragos, who read the report. 
 Lee called the connection to Dragonfly "loose." 
 Dragonfly was previously active from around to 2011 to 2014, when it appeared to go dormant after several cyber firms published research exposing its attacks. The group, also known as Energetic Bear or Koala, was widely believed by security experts to be tied to the Russian government. 
 Symantec did not name Russia in its report but noted that the attackers used code strings that were in Russian. Other code used French, Symantec said, suggesting the attackers may be attempting to make it more difficult to identify them. 
