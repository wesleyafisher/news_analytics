__HTTP_STATUS_CODE
200
__TITLE
New Measles Vaccine is Needle-Free
__AUTHORS
Maggie Fox
__TIMESTAMP
Apr 27 2015, 5:50 pm ET
__ARTICLE
 Scientists have formulated a needle-free vaccine against measles and say the little stick-on patch could be the answer to fighting measles  and perhaps other diseases such as polio, too. 
 The Centers for Disease Control and Prevention calls the patch a game-changer and is helping the team at Georgia Institute of Technology make it work against a range of germs. 
 The dream would be to mail the vaccines out to people to give themselves, or send them out with teams of minimally trained technicians to give to people living in hard-to-reach areas, says Mark Prausnitz, a professor of biomolecular engineering at the Georgia Tech, whos leading the vaccine patch team. 
 The idea of needle-free vaccines isnt new, but theyre tough to formulate so that they work just right. Then they must be tested for Food and Drug Administration approval, a lengthy and expensive process. 
 But the rewards would be enormous, says Prausnitz. 
 Vaccines need to get out to a lot of people around the world and there real barriers if you have to refrigerate the vaccine, if you have to have experienced nurses giving the vaccine, he told NBC News. 
 The patch doesnt have to be kept at a constant cool temperature, it doesnt need to be administered by anyone with special training and it doesnt involve the disposal of dangerous needles. 
"You wont lose vaccine that you are going to have to throw away because it wasnt properly refrigerated."
 And it would be similar in price to current vaccines, Prausnitz says. 
 The cost savings will be in the logistics of administering the vaccine  you wont lose vaccine that you are going to have to throw away because it wasnt properly refrigerated, he said. Governments and charities that deliver vaccines around the world wouldnt have to pay highly trained nurses and they wouldnt have to deal with needle disposal. 
 Prausnitz has been working on microneedle patches against flu. The CDC helped his team come up with a formula against measles. 
 The patch is less than an inch square is covered with tiny, dissolvable needles that soak into the skin in minutes. 
 It took some work, Prausnitz said. The additives that we put into the vaccine had to be different. And measles is made using a live but weakened vaccine, and it took some tinkering to make a formula that would keep that virus viable on the patch instead of in a vial full of liquid. 
 But in animal tests, it worked well, the team recently reported in the journal Vaccine. Now they need to test it in people, and need someone to sponsor those tests. 
 Microneedle technology could move the Global Vaccine Action Plan forward by leading to improved protection against other diseases, including polio, influenza, rotavirus, rubella, tuberculosis and others, CDC said in a statement. CDC is also collaborating with Georgia Tech to see if microneedles could be used to administer inactivated polio vaccine. 
 Measles infects about 20 million people globally every year and it kills more than 140,000 people, mostly children, every year. 
 Its been eliminated in the United States, but cases get imported from affected countries. An outbreak of measles linked to Californias Disneyland made 147 people sick before it ended earlier this month. 
 Imported cases can take hold when infected people come into contact with unvaccinated or under-vaccinated people  and communities of vaccine doubters and vaccine refusers are vulnerable, CDC says. 
 Could needle-free vaccines help overcome resistance to getting vaccinated? Some experts think so. 
 Theres a whole spectrum of reasons why some people dont get vaccinated, Prausnitz said. 
 The negative experience of going to the doctor and getting a shot, that is part of it. It may be that for some of these people on the fence, this better experience with the patch gets them to the other side of the fence. 
