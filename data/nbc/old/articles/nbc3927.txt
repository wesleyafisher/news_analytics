__HTTP_STATUS_CODE
200
__TITLE
Former Equifax CEO Blames One IT Guy for Massive Hack
__AUTHORS
Ben Popken
__TIMESTAMP
Oct 5 2017, 11:38 am ET
__ARTICLE
 Disgraced former Equifax CEO Richard Smith spent the week giving an apology tour on Capitol Hill, explaining to lawmakers exactly how his credit bureau failed to prevent a historic data breach that allowed 145.5 million Americans to have their personal financial history made public. 
 While Smith said he was personally "ultimately responsible for what happened" he also blamed a single unnamed person in the IT department for not updating, or "patching" one Equifax's "portals" after the credit reporting giant was alerted to the security gap in March. 
 "An individual did not ensure communication got to the right person to manually patch the application, Smith testified before the Senate Banking Committee on Wednesday. 
 He also said the company's scanning software, which looks for unpatched systems, didn't find the hole  all of this despite "investments approaching a quarter of a billion dollars in security, Smith acknowledged. 
 Related: The One Move to Make After Equifax Breach 
 Legislators took turns blasting the ex-CEO during his three days of back-to-back testimony, lambasting him for the historic breach and for Equifax's "haphazard" and "sloppy" response to it. 
 "The criminals got everything they need to steal your identity," said Texas Congressman Jeb Hensarling, Chairman of the House Financial Services Committee, in his opening remarks Thursday. "This may be the most harmful attack on a company's personal information the world has ever seen." He called for action by Congress, the Federal Trade Commission, the Consumer Financial Protection Bureau, and other regulators to hold the company to account and prevent future breaches. 
 "The status quo is clearly failing consumers," said Hensarling. 
 Members of the Senate Banking Committee also took issue with the fact that Equifax was just awarded a $7 million government contract to help prevent IRS fraud. 
 "That looks like we're giving Lindsay Lohan the key to the minibar," said Senator John Kennedy of Louisiana. 
 Lawmakers took Smith to task on several other fronts: the failure to notify customers for a month that their data had been stolen, executives selling company stocks the day before Equifax notified the FBI of the breach, and a "glitchy" remediation website and customer support lines. 
 Related: FTC Launches Equifax Probe, Websites and Phones Jammed With Angry Consumers 
 "Because of this breach, consumers will spend the rest of their lives worrying about credit history," said Massachusetts Senator Elizabeth Warren. "But Equifax will be just fine, it could actually come out ahead!" 
 She and other lawmakers pointed out how Equifax could stand to make more money from consumers rushing to purchase identity theft protection products like Lifelock. It happens that Lifelock is a client of Equifax and pays Equifax for the data it uses to power its services  which means consumers are paying a customer of Equifax's who pays Equifax to protect themselves from Equifax's failings. 
 Senator John Kennedy (R-La) said of the paradox, "I don't pay extra at the restaurant to prevent a waitress from spitting in my food!" 
 Smith said Equifax was offering free credit report freezes, had developed a new "credit lock" service for consumers, beefed up staffing, reduced call center times, and contracted with security forensics to fix the problems. 
 Then "Monopoly Man" showed up. 
Forced arbitration rip-off clauses give corporations like #Equifax more power than the Monopoly Man could have ever imagined. pic.twitter.com/R3pdT0KBOM
 The watchability of one of the hearings immediately soared when a protester dressed as Rich Uncle Pennybags  complete with top hat, monocle and mustache  sat in directly behind Smith. Whenever Smith spoke, Monopoly Man nodded approvingly, smoothing his mustache and adjusting his monocle. At one point he even wiped his brow with a sheaf of bills. 
 Advocacy group Public Citizen, which arranged the stunt, said it was intended to draw attention to the company's use of arbitration clauses that force consumers to give up their legal right to sue. 
