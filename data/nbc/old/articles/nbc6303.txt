__HTTP_STATUS_CODE
200
__TITLE
Virtual Reality Tours, Facebook Ads: The New Reality of College Outreach
__AUTHORS
Amy DiLuna
__TIMESTAMP
Aug 28 2017, 1:40 pm ET
__ARTICLE
 When Andover, Massachusetts school superintendent Sheldon Berman noticed his son browsing colleges, he noted something unusual. 
 The teen was doing it all from his couch. 
 He was watching tour videos on YouTube, messaging his friends, comparing and contrasting reviews, receiving answers to his questions  and was close to making a decision  without moving a muscle. 
 "He was out in front of it. He analyzed the data that they had available, the majors, the professors, reputation, what the ratio was of girls to boys," Berman said of his sons strategy. "It was pretty interesting to see." 
 Of course, no school-savvy family would forego the traditional in-person visits, and Berman and his son did make the campus-to-campus trek before finally landing at his chosen spot. 
 To parents, the process of researching colleges will (as with most things) look vastly different from what it was in their day. In the past, a college's marketing effort may have been limited to a few pamphlets that showed up in the mail. But these days, schools are turning on the charm. 
 There are two major players in college standings: yield rates and retention rates. Yield is how many accepted students actually accept a school back (the leader for the class of 2020, in case youre wondering, is Brown University). And retention is how many matriculate as a freshman  and then return for sophomore year. 
 Both are highly fought-for, and many experts believe that even elite schools will have to start working harder to maintain theirs in upcoming years as the race for top students intensifies. 
 As a result, the outreach has changed. Parents of sophomores and juniors, especially those whove begun taking standardized tests, may notice an uptick in material coming at them. Thats because the College Board, which administers the SAT, shares optional data that students and parents provide. 
 In addition to the ubiquitous virtual tours, colleges are advertising on music-streaming services, on YouTube, Facebook, and have even joined Snapchat  in the case of Dartmouth College and others, whose newspapers are available via the app's Discover section. 
 For college counselors, the marketing strategies raise a new question: How do you ensure students see past the bells and whistles to find schools that are right for them? 
 "The ethical thing is to educate the student on what is appropriate research," said Juaquin Moya, a college admissions counselor for IvyWise. "It does get a little murky in terms of what resources youre using and understanding what may be biased or may not be accurate." 
 When done right, creative marketing efforts really work. Two years ago, prospective students who got into the Savannah College of Art and Design received virtual reality headsets that gave them a 360-degree tour of campus. 
 This year, the catalog came to life thanks to a scanning-enabled app that conjures dazzling videos and animations. 
 The high-tech admissions tactic actually encouraged students to make a real-life visit, said Dave Hanak, SCADs executive director of Interactive Services, and the school considers it a huge success. 
 One thing that we took a leap on was the fact that when we did the VR tours, back in 2015, there was a fear behind it of, if we send this information to the students, theyre not going to want to come to us, theyre going to see it in their virtual environment. We saw the exact opposite impact. Which was fantastic. They ended up coming to our admission events. 
 SCAD was able to reach a global audience, while staying true to the school spirit of innovation and creativity. 
 No matter what information is flying at your high school-aged kid, college counselors urge students to do their research independently and through reputable sources  and sort carefully through what comes at them via social media or promotional messages. 
 When you talk about how youre being bombarded by constant emails  these gimmicky approaches to entice someone to apply  help your student understand what their needs are first, said Moya. Then, go through the research and understand whats really a reliable source and whats perhaps less credible. 
 In other words, said Moya, remember that regardless of catchy taglines, what matters is that its the right institution for you. 
 What I love about college counseling is helping young people come into their own and use the process as sort of a self-discovery. 
