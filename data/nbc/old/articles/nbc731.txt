__HTTP_STATUS_CODE
200
__TITLE
Trial Begins for Ahmed Abu Khatallah, Accused Benghazi Terrorist
__AUTHORS
Pete Williams
__TIMESTAMP
Oct 2 2017, 8:19 am ET
__ARTICLE
 WASHINGTON  The trial begins Monday for the only person ever charged in a U.S. court with taking part in 2012's deadly attack on the U.S. diplomatic compound in Benghazi, Libya. 
 Ahmed Abu Khatallah faces murder and terrorism charges, accused of playing a lead role in the siege that killed US ambassador Chris Stevens and three other Americans. 
 Prosecutors say Khatallah was among 20 people who stormed the U.S. mission with machine guns and grenade launchers, set it on fire, and later attacked an annex. Stevens was killed along with three other U.S. government employees  Sean Smith, Tyrone Woods, and Glen Doherty. 
 Khatallah is also accused of helping to turn away emergency responders and supervising the plunder of documents and computers in the compound. 
 Benghazi figured prominently in the 2016 presidential campaign. Donald Trump repeatedly said Hillary Clinton failed to respond quickly to the attack. 
 "Instead of taking charge that night," he told a rally in April, "she decided to go home and sleep. Incredible," Republicans in Congress held nearly three dozen separate hearings on the subject. 
 A separate State Department review said although "systematic failures and leadership and management deficiencies" left the compound vulnerable, no one in the department was at fault. 
 If convicted, Khatallah could face life in prison. 
 Former Attorney General Loretta Lynch decided last year that the government would not seek the death penalty. A Justice Department official said she reached that conclusion "after reviewing the case information and consulting with" prosecutors on the case. 
 A month after the Sept. 11, 2012 Benghazi attack, Khatallah seemed unconcerned about efforts to find and arrest those responsible, talking for two hours with a New York Times reporter in Benghazi. 
 But his freedom ended in 2014 when he was snatched from a Libyan seaside villa during a daring nighttime raid by a team of U.S. special forces and FBI agents. They sped away in a small boat, taking him to a waiting US Navy ship in the Mediterranean, the USS New York, where he was held for a 13-day trip to the United States. On board, he was questioned by US intelligence and then by a separate team of FBI agents. 
 "I bet he thought he was completely safe and wasn't looking over his shoulder, only to have some Americans show up and bring him back to face justice, said John Carlin, who was assistant attorney general for the Justice Departments National Security Division during the Obama administration. "It's not easy to go find someone in a remote village halfway across the world and then be ready to hold them accountable in a US court of law." 
 Khatallahs lawyers argued that he was denied his rights to an attorney onboard the Navy ship, deprived of a speedy appearance before a judge, and was questioned under coercion. But federal judge Christopher Cooper, after holding several hearings on the capture and interrogation, ruled that the statements made to the FBI on the ship could be admitted at the trial. 
 "No member of the FBI interrogation team or the prosecution was involved in the intelligence interviews," the judge said, finding that Khatallah waived his Miranda rights and freely talked to the agents, "because the conditions Abu Khatallah experienced prior to waiving his rights were not coercive." 
 It was the first ruling by a federal judge on the constitutionality of questioning a terrorism suspect at sea, a practice the US has used more than once after making arrests overseas. 
 The trial for Khatallah, now 46, is expected to last six weeks. 
