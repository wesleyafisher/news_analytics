__HTTP_STATUS_CODE
200
__TITLE
A.G. Eric Holder: Police Should Carry Heroin Antidote Naloxone
__AUTHORS
__TIMESTAMP
Jul 31 2014, 1:26 pm ET
__ARTICLE
 Attorney General Eric Holder is urging the nation's law enforcement agencies to have workers who are likely to come across drug users carry the heroin antidote naloxone, which can restore breathing in the middle of an overdose. The Justice Department says an average of 110 people in the U.S. die from drug overdoses every day  more than those killed by gunshot wounds or car crashes. 
 The shocking increase in overdose deaths illustrates that addiction to heroin and other opioids, including some prescription painkillers, represents nothing less than a public health crisis, Holder said. I am confident that expanding the availability of naloxone has the potential to save the lives, families and futures of countless people across the nation. Seventeen states have already increased access to naloxone, resulting in 10,000 OD reversals since 2001, the agency said. 
Kudos to #FDNY FFs from Engine 157, who today saved a patient on Pulaski Ave, #StatenIsland, using the drug Naloxone. pic.twitter.com/4hc5Z42uLL
