__HTTP_STATUS_CODE
200
__TITLE
Ohio Lawmaker Reveals Her Sons Are Battling Opioid Addiction
__AUTHORS
Corky Siemaszko
__TIMESTAMP
Jun 1 2017, 2:26 pm ET
__ARTICLE
 The lieutenant governor of Ohio revealed a secret Thursday that many other moms in her state share  her kids have been battling opioid addiction. 
 Like many Ohioans, my family is struggling with addiction, Mary Taylor told the Dayton Daily News. The opioid crisis has, you know, come in through my front door. We are like, Im sure, many others who have been dealing with it. 
 Taylor, a Republican, did not say how her sons  Joe, 26, and Michael, 23  became hooked on drugs. 
 But when asked if she had advice for other parents facing the same crisis, she said, Just because a doctor gives you a prescription for anything, in this case an opiate, a pain pill  you have to be vigilant as a parent over that. 
 There are sports injuries and kids, when they get old enough, get their wisdom teeth out, she said. 
 Taylor went public a day after Ohio Attorney General Mike DeWine filed a lawsuit against five prescription painkiller manufacturers who said helped create a population of patients physically and psychologically dependent on them. 
 There have been times where we as a family have been in crisis, said Taylor, whose family's ordeal included two drug overdoses at home, numerous trips to hospital emergency rooms, and several failed tries at drug rehab. 
 Taylor said one of her sons is still getting treatment, but did not specify which one. 
 Related: Ohio Sues Big Pharma, Blaming Drug Makers for Causing Opioid Epidemic 
 We are fortunate to have found the type of treatment that was necessary, to restore sanity in our family, and to help my sons get into a good place and get their lives back on track, she said. 
 Like any mom, Taylor said, you worry about your kids. 
 You want them to be happy and healthy and, in our case, sober, she said. And its hard. 
 But, Taylor said, theres hope and right now her sons are doing very well. 
 Both Taylor and DeWine are running for governor of Ohio. 
 When asked by NBC News how Taylors boys became addicted and why she was revealing her familys private ordeal now, press secretary Michael Duchesne released a statement that reads, in part: 
 She is a mom first and a public official second and the health of her sons is and always will be her primary concern, the statement said. While she was willing to share some of her familys struggles in the hopes that her story can help others, she is concerned that shining a further spotlight on her sons lives will not be beneficial to their long term recovery at this time. 
 Related: Trump Admin to Pay Cash Promised by Obama to Fight Opioid Crisis 
 Ohio is one of the states that has been hardest hit by the opioid epidemic. DeWines lawsuit notes that many people become addicted by taking legally prescribed painkillers. 
 And when those patients can no longer afford or legitimately obtain opioids, they often turn to the street to buy prescription opioids or even heroin, the suit states. 
 Earlier this year, the capital city of Columbus and surrounding Franklin County were been averaging one deadly overdose per day, most often the result of addicts shooting up heroin cut with fentanyl, a powerful painkiller that is up to 50 times more powerful than heroin. 
