__HTTP_STATUS_CODE
200
__TITLE
How Cardi Bs Unconventional Success Is Impacting the Music World
__AUTHORS
Karu F. Daniels
__TIMESTAMP
Oct 17 2017, 5:58 pm ET
__ARTICLE
 With her profanity-laced single "Bodak Yellow," rapper Cardi B could be considered 2017s accidental pop star. 
 Born Belcallis Almanzar, the stripper turned reality TV sensation, is representing a new breed of a fierce female prototypes  who takes no prisoners and makes no apologies in the process. Die-hard hip-hop aficionados have already been familiar with this brazen type of broad-like behavior on wax. 
 A little over 25 years ago  following the successes of veteran female rap acts like Salt N Pepa, JJ Fad, Queen Latifah and MC Lyte,  Columbia Records brought forth the Mark Sexx-produced duo BWP (short for: Bytches With Problems), whose songs like "Kotex," and "2 Minute Brother" made even the most ardent free -speech proponents blush. 
 These bold female voices came in the wake of Roxanne Shante, who was arguably the first of the ilk to break ground in rap music in 1984. And, like Shante, the often-forgotten, short-lived hip-hop careers of H.W.A. (short for: Hoez With Attitude) and BWP did provide an opportunity for new avenues for others like them to follow a few years later, most notably in the forms of Lil Kim, Foxy Brown, Missy Elliott, Trina, and Eve. 
 And with the breakout "Bodak Yellow," its clear that people cant seem to get enough of this pint-sized incarnation of ghetto girl realism all these years later. 
 Last month, Cardi B made pop music history when she became the first female rapper without a featured artist to earn a #1 single on Billboards Hot 100 chart in nearly 20 years with her debut song. On Oct. 2, Billboard reported the single  which was being sold for $0.69 at digital retailers  topped the charts for a second consecutive week. The third week on top set a new record for all female rappers. 
 The rise of Cardi B is really remarkable, Chuck Creekmur, CEO of rap music site AllHipHop.com, told NBC News. I know a lot of rap purists have issues with her, but I think she is funny, relatable, engaging and even sweet to a certain degree. Quite frankly, the fact that she causes people to talk about her so much almost insures that she will be successful." 
 Cardi B has become a symbol for the regular degular shmegular," as she refers to herself, to people who werent born with platinum spoons in their mouths  or famed family pedigree. Cardi B earned her stripes in her own way  building up millions of followers on social media. 
 Related: Dominican Rapper Cardi B Is Officially Number One in the Charts 
 Though she had a few rap cameos and noteworthy appearances before "Bodak Yellow," it wasnt until her reality TV stint on Mona Scott Youngs popular VH1 franchise "Love & Hip Hop that the masses took notice. 
 It used to go the other way: rappers would go to reality shows. Now its reality show stars coming out as rappers, Creekmur added. People have issue with it, but didn't mind if drug dealers and thugs being rappers. 
 Though shes gotten her fair share of criticism for the naughty lyrical content and light and lively flow style of "Bodak Yellow," the chart-topping single helped Cardi B bubble up in the zeitgeist in more ways than one. 
 NFL athlete turned civil rights symbol Colin Kaepernick thanked her for her support during her appearance at the "MTV Video Music Awards, and Janet Jackson has incorporated "Bodak Yellow" into a routine during her "State of The World" tour. 
 Taking a global approach to the songs marketability, a Latin Trap version of the "Bodak Yellow" has surfaced, which prompted comedian J. Anthony Brown to mimic it on Steve Harveys nationally syndicated radio show during an episode of his "Murdered The Hits" segment. 
 Christian rapper Keya Smith also made a music video to accompany her gospel/hip hop version of "Bodak Yellow" in response to a Bodak Yellow Challenge that has went viral. 
 Related: Rapper Takes Social Media by Storm with #BookPhoneChallenge 
 Nielsen ranks the song as the #1 single on its Top Ten List. During the 2017 BET Hip Hop Awards, she took home trophies for Hustler of the Year, Single of the Year and Best New Artist. 
 As for why the masses are connecting to Cardi B so much, Creekmur attributed it to her relatable appeal. 
 I think everybody loves or loathes somebody that speaks the real, even if its loud, brash and unapologetic, he said. But I also think people love a good story and they also love an underdog. Cardi B is both. She was not supposed to make it, because of her past and career path, but she did. And she's honest. 
 Follow NBCBLK on Facebook, Twitter and Instagram 
