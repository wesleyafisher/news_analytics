__HTTP_STATUS_CODE
200
__TITLE
National Intel Director Reveals ISIS Attack Plan
__AUTHORS
Ken Dilanian
Corky Siemaszko
__TIMESTAMP
May 23 2017, 11:44 am ET
__ARTICLE
 The bloody mayhem in Manchester could be a taste of the ISIS terror to come. 
 That was the dire warning from Director of National Intelligence Dan Coats on Tuesday in his appearance before the Senate Armed Services Committee and a report by the agency. 
 They claim responsibility for virtually every attack, Coats told the panel after ISIS declared it was behind the suicide bombing at an Ariana Grande concert in Manchester, England, that left 22 dead and dozens more injured. We have not verified yet the connection. 
 The attack came straight from the ISIS playbook that Coats warned lawmakers about. 
 "We anticipate that ISIS will be in transition over the coming year, shifting toward more traditional terrorist operations rather than conventional military engagement in Iraq and Syria, Coats warned. ISIS will continue to lead, enable and inspire terrorist attacks, both unilaterally and with the assistance of its formal branches and networks. 
 Why? Because ISIS is losing on the battlefield and it has lost much of the territory it held in Iraq and Syria. 
 Still, the ISIS narrative will continue to inspire lone actors, making homegrown violent extremist attacks and propaganda an enduring threat, Coats warned. Lone actors will continue to maximize impact with low-budget attacks that do not require significant resources or outside training. 
 Part of the reason for the change in strategy is that ISIS is running short on money, although it remains a potent fighting force, Marine Lt. General Vincent Stewart said in a Defense Intelligence Agency report prepared for the committee. 
 "Coalition airstrikes against ISIS oil assets and a weakened tax base resulting from territorial losses have reduced ISISs total revenues," the report states. 
 "Reductions in revenue have been partially offset by falling costs associated with fewer combatant forces and declining territorial control. Despite its loss of terrain and resources, ISIS retains strong military capabilities, leadership, and command and control, and it remains capable of presenting a strong defense against numerically superior forces, even when its opponents are supported by the counter-ISIS coalition and Iran." 
 So ISIS will be going after targets in hard to defend places where lots of people congregate, like the Manchester Arena, which has a capacity of 21,000. 
 European tourist sites, such as cultural monuments, transportation hubs, shopping malls, and restaurants will almost certainly continue to be targeted because they are easily accessible, Coats warned. In the past year, ISIS use of unmanned aerial systems (drones) for surveillance and delivery of explosives has increased, posing a new threat to civilian infrastructure and military installations." 
