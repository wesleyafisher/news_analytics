__HTTP_STATUS_CODE
200
__TITLE
Filmmaker Befriends Lead Russian Doping Scientist
__AUTHORS
Oluwatomike Adeboyejo
__TIMESTAMP
Aug 3 2017, 6:04 pm ET
__ARTICLE
 Few people would draw a line from Lance Armstrongs confession of using performance enhancement drugs to the scandalous discovery of Russias government-funded doping program. But thats exactly what Bryan Fogel does in his new documentary, Icarus. 
 In the most recent episode of 1947: The Meet the Press Podcast, filmmaker Bryan Fogel tells Chuck Todd how his documentary started with an unusual personal experiment. 
 An amateur cyclist, Fogel wanted to see what would happen if he used the same performance enhancing drug regimen as Lance Armstrong. When he started asking around about how to build a personal doping program, he ended up talking to Dr. Grigory Rodchenkov, the charismatic one-time director of Moscows Anti-Doping Centre. 
 At first, Fogel was surprised by Rodchenkovs participation. This is the guy who ran Russias lab. That was startling to me but in light of what I ended up learning in retrospect not so starling, said Fogel. 
 For years, Rodchenkov was instrumental in Russias government-funded doping program. His program allowed Russia to overwhelm the competition during the 2014 Sochi Winter Olympics, where Russia won 33 medals. 
 At almost the same time that Fogel approached Rodchenkov, Russia was coming under increased international scrutiny for cheating. The overlap between the Russian doping program and its official anti-doping office led to international investigations by the World Anti-Doping Agency, and the International Olympics Committee. When their investigations were completed, more than one hundred Russian athletes were banned from the 2016 Olympic games in Rio. 
 In the fallout, Rodchenkov became a scapegoat for state-sponsored action. 
 Essentially he was doing his job and his job was to help Russian athletes dope and avoid positive detection and to that extent there was never anti-doping in Russia, said Fogel. 
 In the film, Fogel is seen accompanying Rodchenkov as he talks to Western journalists and lawyers, confirming allegations of Russian cheating. Rodchenkovs confession came at a cost. Today, hes a part of the U.S. Department of Justices witness-protection program over fears that Russia may want to cause him harm for exposing their doping program. 
