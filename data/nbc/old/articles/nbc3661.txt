__HTTP_STATUS_CODE
200
__TITLE
Heroin Dealer Gets Stiff Sentence for Causing Spate of Overdoses With Fentanyl-Laced Drugs
__AUTHORS
Corky Siemaszko
__TIMESTAMP
Apr 17 2017, 8:12 pm ET
__ARTICLE
 An Ohio drug dealer who sold heroin laced with elephant tranquilizer at a West Virginia housing project was hit Monday with some heavy arithmetic  more than 18 years in prison for causing 28 drug overdoses during a six hour time period. 
 Bruce Benz Griggs was slapped with the stiff sentence in Huntington, West Virginia federal court by U.S. District Judge Robert Chambers after pleading guilty in January to distribution of a quantity of heroin, according to a release from federal prosecutors. 
 Griggs, 22, was caught on video selling the heroin at the Marcum Terrace development in Huntington on Aug. 15 that sparked a spate of overdoses. 
 Nobody died, but police later determined that the heroin had been laced with fentanyl, the powerful painkiller that killed Prince, and carfentanil, which is a sedative used to calm elephants that is 100 times more potent than fentanyl. 
 Griggs, who is from Akron, was arrested a week later after he was reportedly identified by several of the victims. 
 His lawyer, Carl Hostler, insisted Griggs was an "inexperienced" drug dealer who had no idea he was peddling juiced heroin. 
 He never had any intention of putting anyone in the hospital," Hostler said in a sentencing memorandum obtained by the Charleston Gazette-Mail. "Multiple hospitalizations make this a serious offense. It can be partially explained by lack of knowledge and the fact that Mr. Griggs was just a pawn in a bigger scheme." 
 In court, Griggs apologized to Huntington residents for his crimes. But Chambers told Griggs he was lucky nobody was killed and credited the Huntington Police Department and first responders with saving 28 lives. 
