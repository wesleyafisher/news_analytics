__HTTP_STATUS_CODE
200
__TITLE
Jason Aldean Pays Tribute to Las Vegas Victims, Sings Tom Petty Song on SNL
__AUTHORS
Phil Helsel
__TIMESTAMP
Oct 8 2017, 1:46 am ET
__ARTICLE
 Jason Aldean, the musician who was on stage when a gunman opened fire on a crowd of Las Vegas concert-goers this week, opened "Saturday Night Live" with a tribute to those affected by the massacre and a musical nod to recently-deceased rock legend Tom Petty. 
 The show skipped the traditional joke-filled cold open and began with Aldean on stage. 
 "This week we witnessed one of the worst tragedies in American history," Alden said. "Like everyone, I'm struggling to understand what happened that night, and how to pick up the pieces and start to heal. 
 "So many people are hurting. They're our children, parents, brothers, sisters, friends  they're all part of our family. So I want to say to them: We hurt for you, and we hurt with you. But you can be sure that were going to walk through these tough times together, every step of the way, he said. Because when America is at its best, our bond, and our spirit  it's unbreakable." 
 The ambush shooting on a crowd of concert-goers Sunday in Las Vegas killed 58 people. It was the deadliest mass shooting in modern U.S. history. The gunman who opened fire from the 32nd floor of a nearby hotel apparently killed himself as police closed in. A motive has not been determined. 
 After addressing the shooting on "Saturday Night Live," Aldean performed Petty's hit "I Wont Back Down. Petty died on Monday after going into cardiac arrest at his home in Malibu. 
