__HTTP_STATUS_CODE
200
__TITLE
Backpage Critics Find Surprise Ammunition in Philippines Raid
__AUTHORS
Anna R. Schecter
Kenzi Abou-Sabe
__TIMESTAMP
Jul 1 2017, 6:53 pm ET
__ARTICLE
 It was a daring raid carried out with military precision in a remote city of the Philippines  an operation that yielded surprise ammunition for a legal battle over the classified ad site Backpage, which has been accused of promoting sex trafficking. 
 The strike involved two planes, 14 vehicles, sheriffs and lawyers, computer forensic experts, and armed guards with their firearms discreetly tucked away in shoulder bags. 
 The mastermind was the U.S. company CoStar, a multibillion-dollar enterprise that runs Apartments.com. The target: the headquarters for a firm called Avion where hackers were believed to be stealing proprietary real-estate photos and information on behalf of an industry rival. 
 Wielding a court order, the teams swept through Avion's sprawling facility  eight hours from Manila and accessible by a single road  and emerged with 262 hard drives containing 35 terabytes of data. 
 CoStar says the trove contained proof its intellectual property was being ripped off, a claim its competitor denies. 
 But there was also something far more disturbing: indications that Backpage used Avion to drum up business in the sex trade overseas, undermining its claim that it does nothing more than host and moderate ads. 
 "We got 1.7 million photographs," Curtis Ricketts, senior vice president of CoStar, told NBC News of the December 2016 raid. 
 "It was building picture, building picture, porn, porn, prostitute ad, building picture and then  bang!  a picture that couldn't be anything other than, you know, child pornography. 
 "We just immediately shut the computer off, picked the phone up, called the attorneys. They called the FBI," Ricketts said. 
 Accompanying the photos were thousands of documents linking Avion to Backpage, which has been vigorously fighting a slew of charges in the U.S. that it promotes prostitution and child exploitation. There is no evidence the child pornography was linked to Backpage, but the advertisements for sex clearly were. 
 CoStar chief executive Andrew Florance said he had never even heard of Backpage, so when the name kept turning up in the Avion documents, he and Ricketts looked it up on Google. 
 One of the first things they clicked on was a 90-second trailer for "I Am Jane Doe," a documentary that follows girls who say they were pimped out through Backpage ads. Next came stories about a 20-month U.S. Senate investigation that found Backpage complicit in trafficking. 
 The company's legal woes also include criminal charges refiled last year by the state of California against its CEO and two founders and a civil lawsuit now headed for trial in Washington state after the courts declined to dismiss it. 
 Related: Backpage Pulls Adult Ads, Accuses Government of Censorship 
 Backpage's defense is that it's not responsible for ads posted on the site, based on Section 230 of the 1996 Communications Decency Act which says that online service providers cannot be held liable for content provided by third parties. 
 The company also says that it does what it can to crack down on illegal activity by hiring moderators to flag problematic content. 
 But, according to Florance, "when we looked at what we saw in the Philippines, we saw something that did not go with that storyline at all." 
 "They appeared to us to be actively engaged in looking worldwide to try to find prostitutes to get them to bring their wares to be sold on Backpage," he said. 
 The material seized by CoStar shows Avion worked to promote adult ad business on behalf of Backpage overseas, including in the United Kingdom and Australia. 
 It included audio recordings of Avion workers contacting people who posted sexually explicit ads on rival escort sites and offering them a free ad on Backpage. 
 An Avion employee manual includes step-by-step instructions on how to use a fake IP and email address to pose as someone responding to an adult ad on a rival site. It details how to create escort ads on competitor sites, by copying photos from real Backpage ads, to widen the pool of customers and then invite them to Backpage. 
 There was a spreadsheet of emails written by Avion employees to people who had viewed ads on rival sites, redirecting them to Backpage. "Hi cutie!  I moved my post to backpage," reads one email. 
 The only Avion activity explicitly occurring in the U.S. that was found in the data involved "moderating" or policing adult ads for content. An email sent from a Backpage account to Avion employees reads: "I was reviewing deleted ads and the crew seems to be doing a great job catching bad stuff. Here are a few removals that I restored. Some only needed pics removed." 
 Avion CEO Von Nagasangan told NBC News he could not comment because of a non-disclosure agreement he signed with Backpage. 
 Liz McDougall, an attorney for Backpage, declined to comment on what was found in the Philippines. 
 CoStar said it turned over the Avion material to the FBI. NBC News obtained the data from Romanucci & Blandin, the law firm representing Yvonne Ambrose  whose daughter was killed after allegedly being trafficked through Backpage  in a suit against the company. 
 Mary Mazzio, the director of "I Am Jane Doe," said she hopes the seized data is a wakeup call for Backpage's defenders. 
 "You can pretend to go along with this fallacy that this [Backpage] is a passive website with people posting content on their own," Mazzio said. "[But] what the data does is it completely upends that fallacy. 
 "Here's the reality: Look what's happening offshore. Look what this company is doing, particularly with respect to children. This is gruesome." 
