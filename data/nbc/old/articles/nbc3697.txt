__HTTP_STATUS_CODE
200
__TITLE
Las Vegas Shooting: What We Know About the Victims So Far
__AUTHORS
Tim Stelloh
Kalhan Rosenblatt
Saphora Smith
__TIMESTAMP
Oct 6 2017, 12:19 am ET
__ARTICLE
 One was an emergency room nurse who'd gone to Las Vegas for a wedding anniversary  and died protecting his wife from gunfire. Another was a military veteran who served in Afghanistan. 
 A police records technician from California, also went to the Route 91 Harvest country music festival on Sunday night, and she, too, became part of the soaring death toll in the deadliest mass shooting in modern U.S. history. 
 The Clark County coroner's office released a full list of the victims on Thursday night. Here's what we know about them: 
 Hannah Ahlers 
 Ahlers, 34, got married at 17 and was the mother of three children, according to The Washington Post. She lived in Beaumont, California, and was a fan of four-wheeling and watching her daughter play volleyball, The Post reported. 
 Ahlers was struck in the head with a bullet at the music festival, Lance Miller, her brother, told The Post. 
 Heather Alvarado 
 Alvarado, 35, was the wife of Cedar City, Utah, firefighter Albert Alvarado. 
 "Our thoughts and prayers go out to the entire Alvarado/Warino family," the fire department said. An account has been opened at the State Bank of Southern Utah in Heather Alvarado's name for all those who wish to contribute, the department added. 
 Dorene Anderson 
 Anderson, 49, of Anchorage, Alaska, was a self-described stay-at-home mother and wife. 
 Her husband's employer, Alaska Housing Finance Corp., shared a statement from the Anderson family on its Facebook page. 
 "She (Dorene) was the most amazing wife, mother and person this world ever had. We are so grateful and lucky for the time that we did have with her. We are greatly appreciative and want to thank everyone for the thoughts and prayers you have been sending us," the statement said. 
 Carrie Barnette 
 Barnette, 30, was a longtime Disneyland worker who'd gone to Las Vegas to celebrate a friend's birthday, the Los Angeles Times reported. 
 "She was always generous and helping everybody in every way," her mother, Mavis Barnette, told the paper. "She loved her nieces and nephews and her sister and brother." 
A senseless, horrific, act, and a terrible loss for so many. We mourn a wonderful member of the Disney family: Carrie Barnette. Tragic.
 Disney Chief Executive Robert Iger wrote on Twitter on Monday night: "A senseless, horrific, act, and a terrible loss for so many. We mourn a wonderful member of the Disney family: Carrie Barnette. Tragic." 
 Jack Beaton 
 Beaton, 54, the father of two children, was killed while shielding his wife from the gunfire, his son Jake Beaton wrote on social media. 
 Beaton and his wife were in Las Vegas celebrating their 23rd wedding anniversary. 
 "He put Laurie on the ground and covered her with his body and he got shot I don't know how many times," Beaton's father-in-law Jerry Cook told The Telegraph. "Laurie was saying he was bleeding through the mouth, bleeding profusely, she knew he was dying. He told her he loved her. Laurie could tell he was slipping. She told him she loved him and she would see him in heaven." 
 Before he died, the elder Beaton posted a photo of himself with a group of smiling, jovial friends at the concert. 
 Steven Berger 
 Berger, 44, a financial adviser and basketball player from Shorewood, Minnesota, attended the concert with his roommate and other friends. 
 His mother, Mary, told The Associated Press that his roommate saw Berger, the father of three children, get shot and fall to the ground but couldn't get to him because the crowds were being herded out of the venue. 
 Candice Bowers 
 Bowers, 40, was killed Sunday doing what she loved  dancing to country music  her family said in a statement on the crowdfunding website GoFundMe. 
 Bowers, the mother of three chidren, will be greatly missed by all who knew and loved her, her family added in a statement. 
 Related: Stories of Heroism Emerge From Las Vegas Massacre 
 Denise Burditus 
 Burditus, 50, of Martinsburg, West Virginia, was the mother of two children and a grandmother of four. 
 "There wasn't many times where she didn't have a smile on her face," her husband of 32 years, Tony Burditus, told The Washington Post. "She never met a stranger." 
 Sandy Casey 
 Casey, 34, of Manhattan Beach, California, was a special education teacher who'd worked for the city's unified school district for nine years, NBC Los Angeles reported. 
 A country music lover, she'd gone to the festival with her fianc and some friends, and they'd been near the stage when she was shot in her lower back, according to The Washington Post. 
 Her boyfriend tried carrying her to safety  dodging gunfire along the way  but eventually she stopped breathing, The Post reported. 
 "This is unbelievably sad and tragic," district Superintendent Mike Matthews said, according NBC Los Angeles. "We lost a spectacular teacher who devoted her life to helping some of our most needy students." 
 Andrea Castilla 
 Castilla, 28, a makeup artist from Huntington Beach, California, was celebrating her 28th birthday in Las Vegas  where her boyfriend was planning to propose marriage. 
 Castilla was holding hands with her sister when she was shot in the head, according to a GoFundMe page created by her aunt, Marina Parker. 
 "My sister was really happy. She was living her life and had so many dreams and aspirations," Adam Castilla, her brother, told The Washington Post. "She didn't have one bad bone in her body. Every time I saw her, she managed to make everything good." 
 Denise Cohen 
 Cohen, 57, lived in Oxnard, California, with her boyfriend, Derrick "Bo" Taylor, who was also killed. 
 Cohen's son told NBC Bay Area that his mother radiated a light that touched everyone she knew. 
 Austin Davis 
 Davis, 29, was a pipe fitter. The Washington Post reported that the last text Davis sent his mother read, "I kinda want to come home, I love home." 
 Thomas Day Jr. 
 Day, 54, was a country music fan, the father of four adult children and a grandfather of two. He moved to Las Vegas three years ago after having raised a family in Corona, California. 
 Day was described as a "great family man" and a "fun-loving boy" by his father, Thomas Day Sr. 
 The elder Day said he learned of his son's death when he got a frantic phone call from his grandchildren. 
 "They were standing right there, and they said he and another young man there both took a bullet in the head," he told The Associated Press. "Everybody started running for cover, and the guy kept shooting." 
 Christiana Duarte 
 Duarte, 22, a recent graduate of the University of Arizona, was part of the Sigma Kappa sorority, university President Robert C. Robbins said in a statement. 
 "I know I speak for the UA community in expressing our deepest condolences for Christiana's family and in asking for their privacy to be respected," Robbins added. 
 Stacee Etcheber 
 Etcheber, 50, had attended the concert Sunday with her husband, Vincent, a San Francisco police officer, NBC Bay Area reported. 
 As bullets rained down on the crowds from the 32nd floor of the Mandalay Bay Casino and Hotel, Vincent told her to run as he stayed behind to help the wounded, according to the San Francisco Police Officers Association. 
 But Vincent later couldn't find his wife among the panicked crowd, the association said. 
 San Francisco Police Chief William Scott said Etcheber "was taken in a senseless act of violence" while her husband "heroically rushed to aid shooting victims." 
 Brian Fraser 
 Nick Arellano left his family at the festival Sunday morning because he had to race back for his first day at the University of California. 
 His mother, his father and his new wife were still at the festival when the first shots were fired, Arellano, 25, an Air Force engineer, told NBC Los Angeles. 
 Later that evening, he got a call saying his father, Fraser, 39, had died in the gunfire. 
 Arellano said that his father was too good for this world and that said he had even ordained himself to perform Arellano's wedding in July. 
 "Love and respect is the result of the best a man has to offer," said Arellano, who said he hoped to honor his father by caring for his three siblings and his widowed mother. 
 Keri Galvan 
 Galvan, 31, died in her husband's arms after having been shot in the head, according to NBC Los Angeles. 
 "I didn't notice right away," said her husband, Justin Galvan, a former Marine. "I started CPR right away, and I did everything I could." 
 Galvan, the mother of three young children, was "an amazing supermom  that's what you could best describe her as," her sister Lindsey Poole said. 
 Dana Gardner 
 Gardner, 52, of San Bernardino, California, was killed in Sunday's attack, NBC Los Angeles reported. 
 She worked for San Bernardino County as a deputy recorder for more than 25 years, a county official confirmed. 
 Angie Gomez 
 Gomez, 20, graduated from high school two years ago, had just gotten a job as a certified nursing assistant and was attending the festival to celebrate, The Washington Post reported. 
 Citing a family friend, the paper reported that she was shot three times  once in the shoulder and twice in the arm. She died before her longtime boyfriend could get her to a hospital. 
 School officials in Riverside, California, described her on Monday as a determined but convivial student who loved theater and choir. 
 "We are shaken and saddened by this news," the district said in a statement. "Angie was a loyal friend who loved her family and will be forever missed by all who knew her." 
 Rocio Guillen 
 Guillen, 40, had recently given birth to her fourth child and was engaged to be married when she died, NBC Los Angeles reported. 
 Her son Marcus Guillen said he immediately texted his mother's fianc, Jaksha, who replied to say his mother had been shot in the leg. 
 It wasn't until the next morning that Marcus found out that his mother had died. He said that he felt angry that his mother, who had overcome paralysis while pregnant with his younger brother, couldn't have survived the shooting. 
 "She was a fighter, a great mother," he told The Associated Press. 
 Charleston Hartfield 
 Hartfield, 34, was a sergeant first class with the Nevada Army National Guard and a soldier in the 100th Quartermaster Company, headquartered in Las Vegas. 
 "Sgt. 1st Class Hartfield epitomizes everything good about America," said Brig. Gen. Zachary Doser, commander of the Nevada Army National Guard. 
 The Las Vegas ReviewJournal reported that Hartfield was a Las Vegas police officer who was off duty at the time of the shooting. 
 He was also the author of "Memoirs of a Public Servant," published earlier this year. 
 The book, which details Hartfield's career as a police officer in Las Vegas, says he worked in law enforcement for 11 years and on active military duty for 16 years. 
 It also says Hartfield was a husband and father. 
 Chris Hazencomb 
 Hazencomb, 44, of Camarillo, California, was a sports junkie who graduated from Thousand Oaks High School, according to the Ventura County Star. 
 Jennifer Irvine 
 Irvine, 42, a lawyer, was a snowboarder and had a black belt in taekwondo, according to The Washington Post. 
 She was with a group of friends when she was shot in the head, her friend Kyle Kraska, sports director of KFMB-TV of San Diego, told The Post. 
 Kraska described Irvine as a "ball of energy" who often organized weekend trips and social get-togethers. 
 Kraska told The Post that he hoped his friend died instantaneously from the bullet wound. 
 "Her life ended singing and dancing and smiling," he said. 
 Nicol Kimura 
 Kimura, 36, of Placentia, California, was attending the festival with a close-knit group of friends, who called themselves a "framily." 
 Ryan Miller, who was with Kimura and started a GoFundMe page for her family, confirmed her death to NBC News. 
 "Nicol's heart was bigger than most human beings, her spirit was beautiful, her laugh was infectious, and she just had a way of making every time we gathered an awesome one," Miller wrote on the GoFundMe page. 
 Miller wrote on Facebook that his children loved Kimura because she was the "fun crazy aunt everyone wants." 
 "Nicol is never to be replaced. There will always be an empty seat at our framily dinners," he wrote. 
 Jessica Klymchuk 
 Klymchuk, 34, of Alberta, was the mother of four children and worked as a librarian, a bus driver and an educational assistant at a Roman Catholic elementary school, St. Stephen's. 
 Klymchuk was in Las Vegas with her fianc at the time of the shooting, according to The Globe and Mail. Her children remained at home. 
 "She was a very good mother," Margaret Klymchuk, her grandmother, told The Globe and Mail. "She raised four beautiful children." 
 Carly Kreibaum 
 Kreibaum, 33, of Sutherland, Iowa, was a mother of two who went missing in the aftermath of the shooting. 
 "At this time my family and I would just like some privacy as we take the necessary time to grieve," family member Sarah Rohwer told The Washington Post. 
 Rhonda LeRocque 
 LeRocque, 42, of Tewksbury, Massachusetts, was also among the dead, NBC Boston reported. 
 "We've lost a gem," her mother, Priscilla Champagne, told the station Monday. 
 Jennifer Zeleneski, LeRocque's half-sister, said: "She didn't deserve this. Her family doesn't deserve this. She was an amazing person, a great mom, great wife. She always had something nice to say when you needed it." 
 Victor Link 
 Link, 55, was a loan processor from San Clemente, California. 
 He was a music lover and the father of a 23-year-old son, a nephew, Vincent Link, told The Bakersfield California. He attended the music festival with his fiance, Lynn Gonzales. 
 "It doesn't feel real," Vincent Link said. "It's hard to grasp that he's gone." 
 Jordan McIldoon 
 McIldoon, 23, of British Columbia, was attending the festival with his girlfriend. He was a mechanic apprentice about to start trade school, and he was the only child of Al and Angela McIldoon, according to CBC News. 
 "We only had one child," they told the site. "We just don't know what to do." 
 McIldoon died in the arms of Las Vegas bartender Heather Gooze, according to CBC News. She told the site that she stayed with him for more than five hours and spoke with his mother and girlfriend. 
 "I wouldn't want anybody to leave," Gooze told CBC News. "You know, I couldn't just leave him by himself." 
 Kelsey Meadows 
 Meadows, 28, was a graduate of Fresno State University in California who became "a gifted teacher who demonstrated a skill and passion for her chosen profession," history Professor Lori Clune said in a statement. 
 Calla Medig 
 Medig, 28, was at the concert with her roommate when she was killed. 
 Medig's mother, Louise Hayes, told Global News that she and her husband left for Las Vegas on Monday night to identify her daughter's body. 
 Sonny Melton 
 Sonny, 29, and his wife, Heather, were celebrating their wedding anniversary at the music festival when gunfire rang out, NBC affiliate WSMV of Nashville, Tennessee, reported. 
 Sonny, an emergency room nurse, "saved my life," Heather Melton said. "He grabbed me and started running when I felt him get shot in the back." 
 "I want everyone to know what a kindhearted, loving man he was, but at this point, I can barely breathe," Heather said. 
 Pati Mestas 
 Mestas, 67, who lived in Menifee, California, was a country music fanatic, and Jason Aldean was one of her favorite singers, the Press-Enterprise newspaper of Riverside, California, reported. 
 Tom Smith of San Antonio, Texas, Mestas cousin, described her to the Press-Enterprise as "very likeable, very outgoing. Focused on family and very sensitive to other people's concerns or problems or issues. She was genuinely a very nice person to have a conversation with and to share both fortunate and unfortunate events." 
 Nancy Barton, a friend, said on a GoFundMe page established for the family: "Pati was one great lady, loved her family and country music. She was at the concert in Las Vegas enjoying the music that she waited so long to go to ... happy and excited." 
 Austin Meyer 
 Meyer, 24, of Marina, California, was a Seaside High School graduate and was attending Truckee Meadows Community College in Reno, Nevada. 
 "He had dreams of opening his own auto repair shop after graduation. He was excited to get married and start a family," Veronica Meyer, his sister, told NBC affiliate KSBW of Salinas, California. 
 Adrian Murfitt 
 Murfitt, 35, a commercial fisherman, had traveled to Nevada from his home in Anchorage, Alaska, after a successful season on the water, The Associated Press reported. 
 Murfitt "was happy to pay some things off and had made some really good money," Shannon Gothard, his sister, told the AP. He "decided to go out and celebrate and go to the concert and treat himself to something nice and fun." 
 Gothard told the AP that her family spoke with a friend who was with her brother when he died. 
 Rachael Parker 
 Parker was a records technician with a decade-long career at the Manhattan Beach Police Department in Los Angeles. She'd gone to Las Vegas with a colleague, a department spokeswoman said. 
 The colleague  an officer with the department who wasn't identified  was shot but survived, said the spokeswoman, Kristie Colombo. 
 Parker, 33, "will be greatly missed," Colombo said. 
 Jenny Parks 
 Parks, 36, of Lancaster, California, was a kindergarten teacher and the mother of two children, according to The New York Times. 
 She leaves behind her husband, Bobby, who was injured in Sunday's mass shooting, a friend, Jessica Maddin, told The Associated Press. 
 Parks and her husband were high school sweethearts, and she had become a teacher for the Lancaster School District in California, the news agency reported. 
 "It breaks my heart," Maddin said. "People go to concerts to have a good time, connect with others and escape the tragedies of this world." 
 Carolyn Lee Parsons 
 Parsons, 31, of Seattle, was a staffing manager at the recruiting company Ajilon in Seattle, according to her LinkedIn page. Mary Beth Waddill, a spokeswoman for LinkedIn, told The Associated Press that the company was respecting the family's privacy but may release a statement on Parsons' death. 
 Parsons posted "Night made!" early Saturday on Facebook after seeing the singer Eric Church at the concert. 
 A friend, Carolyn Farmer, wrote in a post sharing Parson's comments: "I feel peace knowing she was living life until her last moments." 
 Lisa Patterson 
 Patterson, 46, the mother of three children, attended the festival with three friends, her husband, Bob, told NBC Los Angeles. She was the only one not to walk out alive. 
 Her friends, who were unhurt, saw her get shot, Bob said. 
 He said that he texted his wife on Sunday evening but that she didn't respond. He waited all night before driving to Las Vegas at 6 a.m. (9 a.m. ET) the next day but only found out that she was dead by the end of Monday. 
 In addition to her husband, Patterson leaves behind two daughters and a son. 
 John Phippen 
 Phippen, 56, was a New York native who moved to Santa Clarita, California, where he ran a home remodeling company called JP Specialties. 
 He attended the concert with his son Travis, according to the Los Angeles Times. 
 "He was my best friend," Travis told the Times. "He never did anything wrong to anybody. He was always kind and gentle. He was the biggest teddy bear I knew." 
 Travis, who was shot in the arm, drove his father to Sunrise Hospital & Medical Center, where he died. 
 Melissa Ramirez 
 Ramirez, 26, of Littlerock, California, was a member service specialist for AAA. She enjoyed traveling and football. 
 Ramirez, who graduated from California State University at Bakersfield with a degree in business, was "the best cook in the family," her cousin, Yesenia Mancilla, told The Washington Post. 
 Jordyn Rivera 
 Rivera, 21, was a fourth-year student at California State University-San Bernardino and a member the national health education honor society Eta Sigma Gamma, according to the Las Vegas Review-Journal. 
 CSUSB shared a link on Facebook to a GoFundMe page set up for Rivera's funeral costs. School President Toms D. Morales said in a statement: "This is a devastating loss for the entire CSUSB family. In this time of grief, our thoughts and prayers are with Jordyn's family, friends and all who knew her." 
 Quinton Robbins 
 Robbins, 20, took his girlfriend on a date to the music festival, and when he began clutching his chest, she thought that Robbins  a diabetic  had low blood sugar levels, The Washington Post reported. 
 She didn't realize he'd been struck in the chest, Gaynor Wells, Robbins' grandmother, told the paper. 
 Robbins was the oldest of three children and a student at the University of Nevada, Las Vegas. 
 He was "just a jewel," Robbins said, according to The Post. 
 Cameron Robinson 
 Robinson, 27, of St. George, Utah, was attending the festival with his boyfriend, Robert Eardley. Robinson was shot in the neck. 
 Trina Gray, who raised Robinson from the time he was a small child, told The Washington Post that his death felt like a "cosmic joke." 
 In recent weeks, Gray's home was flooded by Hurricane Harvey. Her mother also recently died. She said she is now trying to explain the series of tragedies to Robinson's 4-year-old nephew. 
 Tara Roe 
 Roe, 34, was the mother of two young children, according to CBC News. 
 Val Rodgers, her aunt, called her niece a "beautiful soul" and a "wonderful mother." Roe worked as an educational assistant for the Foothills School Division in Alberta. She spent more than a decade as a model with Sophia Models International, the agency said in a statement posted on Facebook. 
 "We are saddened, shocked and pray for everyone affected by this tragedy," the agency said. "She was always a friendly face and had a very caring spirit." 
 Lisa Romero-Muniz 
 Romero Muniz had worked for the Gallup-McKinley County, New Mexico, school district since 2003 and was most recently a discipline secretary at Miyamura High School in Gallup. 
 Superintendent Mike Hyatt called her "an incredible, loving and sincere friend, mentor and advocate for our students." 
 "She was outgoing, kind and considerate," he said. "We will miss all these attributes that she brought and shared." 
 Christopher Roybal 
 Debby Allen went to the music festival with her son, Christopher Roybal, 28, a veteran who served in Afghanistan. But they got there at different times. When Allen texted Roybal asking for his location, she never heard back. 
 And when the gunfire began, they were on opposite sides of the stage, Allen said. 
 "I was trying to run towards wherever I thought he might be," she said. "This man wouldn't let me  he kept pulling me away saying, 'You can't run towards the gunfire.'" 
 Allen later found a firefighter who'd been walking behind her son when he was hit. 
 "He told me my son was shot in the chest  that he said, 'I was hit, I'm hit,'" Allen said. "They all bent down when the gunfire began, and my son just fell back." 
 Brett Schwanbeck 
 Schwanbeck, 61, the father of two children and grandfather of five, was killed running for cover with his fiance Anna Orozco. 
 Orozco told the Washington Post that she and Schwanbeck, lifelong friends from Arizona, were scheduled to marry in January. 
 Schwanbeck was a retired big-rig truck driver who was happy outdoors riding ATVs or fishing, Orozco told The Post. 
 "He was a fun-loving, hard-living man," she told the paper. "He enjoyed life, and hed help out anyone who needed help. ... He was such a big, important part of my life." 
 Bailey Schweitzer 
 Schweitzer, 20, of Bakersfield, California, was a former high school cheerleader and worked as a receptionist at Infinity Communications, according to The Bakersfield Californian. 
 Colleagues said Schweitzer was enjoying Las Vegas so much that she planned to return for her 21st birthday in April. 
 Co-workers left a single candle at the avid country music fan's desk after learning of her death, the paper reported. 
 Laura Shipp 
 Shipp, 50, of Las Vegas was at the concert with her son, Corey Shipp, 23, her boyfriend and some friends, said Steve Shipp, her brother. 
 Shipp moved to Las Vegas five years ago from Thousand Oaks, California, and she remained a rabid fan of the Los Angeles Dodgers, her brother told the Ventura County Star. He said he remembered his sister as a happy woman with "lots and lots of friends." But it was her son, a U.S. Marine, to whom she was completely devoted, he said. 
 "She was his world, and he was hers." 
 Erick Silva 
 Silva, 22, of Las Vegas, was a security guard at the Route 91 Harvest festival. 
 His uncle, Rob Morgan, told The Washington Post that believes Silva was one of the first people killed in the shooting. 
 Morgan described Silva as a helpful person who worked 18-hour shifts, bought food for the homeless and held yard sales to pay his mother's bills. 
 Susan Smith 
 Smith, 53, was an elementary school office manager and district fixture in Simi Valley, California  someone who never missed a dance recital and always had a smile on her face. 
 "It's numbing," a family friend, Suzanne Smith, told NBC Los Angeles. "It doesn't seem real." 
 Simi Valley School District spokeswoman Jake Finch told The Washington Post that she'd been at the festival with friends when she was killed. 
 Brennan Stewart 
 Stewart, 30, of Las Vegas was an avid country fan who wrote and played country music, his sister-in-law Kelly Stewart told the Las Vegas Review Journal. 
 She said Brennan Stewart shielded his girlfriend when the shooting began at the Route 91 Harvest festival. 
 "Brennan was the kind of guy who always put others before himself; including up to the moment he lost his life," his family said in a statement to the Review-Journal. 
 Derrick "Bo" Taylor 
 Taylor, 56, was a lieutenant at a California correctional facility, the state Department of Corrections and Rehabilitation confirmed. 
 Taylor, who was camp commander at the Ventura Conservation Camp, had worked for the department for more than 25 years, the statement added. He lived in Oxnard, California, with his girlfriend, Denise Cohen, who also was killed in the shooting. 
 "There are no words to express the feeling of loss and sadness regarding Bo's passing," Warden Joel Martinez wrote in a memo to staff. "Bo's loss will be felt throughout the prison, conservation camps and Department." 
 Neysa Tonks 
 Tonks, 46, worked for a Southern California-based technology company and had three children. Her employer, Technologent, confirmed Monday that she died. 
 On a fundraising page, friends recalled her jovial side, posting photos of her wrapped in toilet paper or flashing a goofy grin for the camera. 
 "Neysa was always down to be silly," one friend wrote. "More memories than I can count are of her laughing and she had the BEST laugh!! She will be GREATLY and DEEPLY missed!!!" 
 Michelle Vo 
 Vo, 32, a former administrative assistant from San Jose, California, was described as "a sweet soul" with a bright smile, according to NBC Bay Area. 
 Kurt Von Tillow 
 Von Tillow, 55, was from Cameron Park, California, a small town in the Sierra Foothills outside Sacramento. Von Tillow was shot dead Sunday night; two relatives were wounded but were expected to survive, NBC affiliate KCRA of Sacramento reported. 
 Von Tillow was memorialized at a local country club on Monday, and relatives and friends led a procession of golf carts to his home. 
 There, a U.S. flag was attached to a fence and bouquets of flowers were strewn across the grass. 
 Bill Wolfe Jr. 
 Wolfe, 32, was a wrestling coach in Shippensburg, Pennsylvania, the police department said in a Facebook post. 
 "It is with the most of broken hearts, the families of Bill Wolfe Jr. and his wife, Robyn, share that Bill has been confirmed to be among the deceased as a result of the mass attack in Las Vegas," the statement read. 
 Wolfe and his wife were celebrating their wedding anniversary, according to PennLive.com, the website of the Patriot-News newspaper of Harrisburg. 
 A GoFundMe page created by Shippensburg Wrestling had raised more than $8,500 for his family as of Tuesday morning. 
 CORRECTION (Oct. 3, 2:15 p.m.): An earlier version of this article misidentified the mother of one of the victims, Rhonda LeRocque. The mother's name is Priscilla Champagne; she does not share the same name as Rhonda LeRocque. 
 CORRECTION (Oct. 6, 12:05 a.m.): An earlier version of this article reported the incorrect age for Brian Fraser. According to the Clark County, Nevada, coroner's office, he was 39. 
