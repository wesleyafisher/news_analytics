__HTTP_STATUS_CODE
200
__TITLE
Heres How Sleep Loss Can Affect Alzheimers
__AUTHORS
Maggie Fox
__TIMESTAMP
Jul 11 2017, 2:26 pm ET
__ARTICLE
 A single night of interrupted sleep causes an increase in brain proteins believed to cause Alzheimer's disease, researchers reported Monday. 
 They believe their research shows that sleep helps the body clear away the compounds, called amyloid and tau, and that interrupting sleep may allow too much of them to build up. 
 The study, published in the journal Brain, doesn't show that poor sleep causes Alzheimer's, but it adds one more piece to the puzzle of what causes dementia. 
 "When people had their slow-wave sleep disrupted, their amyloid levels increased by about 10 percent," said Dr. Yo-El Ju of Washington University in St. Louis, who led the study. 
 Many other studies have linked poor sleep with earlier onset of dementia and especially Alzheimer's. And a study published last week in the journal Neurology found that levels of amyloid, the protein that clogs the brains of Alzheimer's patients, rose with poor sleep. 
 Related: Researchers Seek Test to Predict Alzheimer's 
 This study sought to identify the most important phase of sleep. Ju and colleagues recruited 17 healthy adults for the study. 
 "What we did was allow people to sleep a normal amount of time, but we prevented them from getting deep sleep or what is called slow-wave sleep," Ju told NBC News. 
 "When we interrupted just the slow-wave sleep part, they still had an increase in amyloid. So this tells us it's getting the deep slow-wave sleep that's important for reducing the levels of amyloid." 
 More than 5 million Americans have Alzheimer's, and the number is expected to grow as the population ages. There's no cure, and treatments do not work well. Drugs such as Aricept, known also as donezepil, and Namenda can reduce symptoms for a time, but they do not slow the worsening of the disease. 
 Related: Experimental Drugs Offer Hope, No Slam Dunk, for Alzheimer's 
 There's no guaranteed way to prevent dementia, but exercise, healthy diet, specific types of brain training and controlling blood pressure can all help. 
 No one's been able to conduct a study to prove that a good night's sleep helps, but many people who go on to develop dementia have complained of poor sleep in the years before. 
 For their experiment, the researchers had their volunteers show up in a controlled sleep lab. Half were allowed to sleep normally, and half were constantly kept in shallow sleep. 
 "As soon as they got into slow-wave sleep, they got a beep. And the beeps got louder and louder and louder until they came out of the deep sleep," Ju said. 
 "It was pretty harsh." 
 This went on for the entire night. The volunteers did not realize their sleep had been interrupted. 
 In the mornings, the volunteers had their spinal fluid analyzed. "When people had their slow wave sleep disrupted, their amyloid levels increased by about 10 percent," Ju said. 
 The volunteers also wore sleep monitors to measure their sleep at home. Those who slept poorly for a week at home had measurably higher levels of a second Alzheimer's associated protein called tau. 
 Related: How Can You Prevent Alzheimer's?  
 "We were not surprised to find that tau levels didn't budge after just one night of disrupted sleep while amyloid levels did, because amyloid levels normally change more quickly than tau levels," Ju said. "But we could see, when the participants had several bad nights in a row at home, that their tau levels had risen." 
 Amyloid is naturally produced in the brain, and researchers know that it can caused clogs called plaques. People with more plaques often have memory and thinking problems and dementia  but not always. 
 So the amyloid link is not entirely clear. Ju has a theory. 
 It may be that interrupted sleep leads to increased brain activity and increased amyloid production," she said. 
 "Amyloid is released by brain cells all the time when they fire their synapses," she said. 
 When they rest, they don't release the amyloid. Ju thinks the brain may clear out excess levels of amyloid during deep sleep. 
 "When people are in a nice, deep sleep, they get a period of time when, with the normal clearance mechanisms working, the levels of amyloid decrease," she said. "If levels are increased over years, they are more likely to cause the clumps called plaques, which don't dissolve." 
 Studies in mice show it takes only an excess of about 10 percent of amyloid to cause amyloid plaques to form. 
 Next, the team will study whether treating obstructive sleep apnea  a common cause of sleep disruption  will improve people's slow-wave sleep and affect amyloid levels. People with sleep apnea have a higher risk of developing dementia. 
