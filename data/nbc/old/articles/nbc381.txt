__HTTP_STATUS_CODE
200
__TITLE
After Weinstein Allegations, Rose McGowan Emerges as Major Voice
__AUTHORS
Daniel Arkin
__TIMESTAMP
Oct 14 2017, 12:21 pm ET
__ARTICLE
 Rose McGowan does not shy away from battle. In recent years, the actress has eviscerated what she has called the "systemic abuse of women in Hollywood"  decrying harassment on film sets, declaring that she was "assembling an army" to challenge a male-dominated industry. 
 This week McGowan emerged as the most fierce and unrelenting critic of Harvey Weinstein, the Hollywood titan who stands accused of sexual harassment and assault of women over at least three decades. McGowan, 44, has accused Weinstein of raping her. 
 In dozens of blistering social media posts, including one that led to a temporary but controversial suspension from Twitter, McGowan has blasted what she describes as a pervasive culture of mistreatment and silence in Hollywood, urging other women to come forward with their own stories of assault and calling on men to "stand up." 
 "This is about a power structure that needs to be brought down," McGowan tweeted last Saturday, two days after The New York Times published a bombshell report detailing allegations of sexual misconduct against Weinstein, who has since been fired from The Weinstein Company, the studio he co-founded. 
 McGowan reached a previously undisclosed settlement with Weinstein in 1997 "after an episode in a hotel room" during the Sundance Film Festival, The Times reported. A legal document reviewed by The Times said the $100,000 settlement was "not to be construed as an admission" by the producer; it was meant to avoid litigation and "buy peace." (McGowan did not comment to the newspaper.) 
 Weinstein's representatives have said in a previous statement that "any allegations of non-consensual sex are unequivocally denied" by him and that he did not retaliate against women who rejected his advances. 
 McGowan came of age in Italy, the second-eldest child of American hippies who fell in with a cult called the Children of God. The free-love communes in which she grew up had a sinister side, she later recalled. 
 "You were kept in the dark so you would obey," McGowan told People magazine in 2011. "I remember watching how the men were with the women, and at a very early age I decided I did not want to be like those women. They were basically there to serve the men sexually." 
 McGowan and her family eventually broke free of the cult and fled to the United States. After a few years of drifting in the Pacific Northwest, McGowan moved to Los Angeles to gain a toehold in the film industry. She landed a few roles in Gen-X indie films, and one critic called her a "Kewpie doll with a Lolita pout." 
 McGowan, whose representative did not reply to an interview request from NBC News, got her big break in "Scream," a 1996 parody of teen slasher flicks released by Miramax, the film company co-founded by Weinstein and his brother, Bob. The movie, a surprise box-office smash, became a cult classic. McGowan was swiftly typecast as an edgy ingnue, with a public image that was part Goth and part Jazz Age flapper. 
 She spent five years on the WB television series "Charmed," about a trio of witches, and cropped up in the tabloids in the late 1990s during her high-profile relationship with rocker Marilyn Manson. She later appeared in 2007's "Grindhouse," a gory throwback to 1970s exploitation films that was released by Dimension Films, a label under the umbrella of The Weinstein Company. 
 In the last few years, McGowan has steadily shifted gears, taking on fewer acting roles to focus on directing. She also wrote a memoir, "Brave," due out in February 2018. The product description on Amazon bills the book as an "empowering manifesto" from a "feminist whistleblowing badass" who "was born in one cult and came of age in another, more visible cult: Hollywood." 
