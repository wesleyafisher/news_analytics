__HTTP_STATUS_CODE
200
__TITLE
Texas Woman Slips Handcuffs, Takes Police SUV on 100-MPH Chase
__AUTHORS
Alex Johnson
__TIMESTAMP
Sep 6 2017, 2:30 am ET
__ARTICLE
 Texas police published video Tuesday showing a shoplifting suspect slipping out of her handcuffs, stealing a police SUV and leading officers on a breathtaking high-speed chase. 
 The woman, identified as Toscha Fay Sponsler, 33, of Pollok, Texas, ran away from Lufkin police officers responded to a call of a possible shoplifter at a beauty supply store on Saturday, police said in a statement Tuesday. After the officers ran her down on foot, Sponsler was cuffed behind her back and buckled into a seatbelt in the back of a patrol SUV, police said. 
 Then the action movie started. 
 Video from the SUV's internal camera records Sponsler deftly wriggling out her handcuffs, keeping a wary eye out and playing possum whenever someone in uniform passes by the window. When she gets the chance, she clambers over into the front seat and speeds off, with officers vainly trying to run her down on foot. 
 Video from the unit's dash camera records Sponsler speeding and veering across lanes, at one point swerving to avoid a spike strip that a roadside officer tosses into the street. 
 After what police said was a 23-minute pursuit at speeds hitting 100 mph, officers maneuver Sponsler into making a hard turn into a residential yard more than 20 miles away in the town of Zavalla, where she loses control of the SUV and finally comes to a stop. Officers break through the driver's-side window and toss Sponsler on the ground, where they cuff her again. 
 The video ends with a clip of a Lufkin officer drilling bolts into a unit and a notation that the department has reinforced the partition between the front and back seats of its units. It boasts the hashtag #FOOLMEONCE. 
 Sponsler remained in the Angelina County Jail on $18,000 bond on Tuesday night, charged with five felony counts of escape with the threat of a deadly weapon, aggravated assault, unauthorized use of a vehicle, possession of a controlled substance and evading arrest, according to jail records. 
 The alleged deadly weapon was a police shotgun mechanically locked to the SUV, which pursuing officers said they saw Sponsler reach for repeatedly. 
