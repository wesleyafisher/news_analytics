__HTTP_STATUS_CODE
200
__TITLE
Digital Devices OK Even for Toddlers, Doctors Say
__AUTHORS
Erika Edwards
Maggie Fox
__TIMESTAMP
Oct 21 2016, 3:21 pm ET
__ARTICLE
 Digital devices are not inherently harmful to children and can often be used for good, pediatricians said Friday in a change from earlier guidance. 
 Updated advice to parents suggests that toddlers as young as 18 months old can handle the occasional session with a tablet computer, but an hour a day is plenty for kids under 5. 
 And parents shouldnt feel guilty about letting their kids use smartphones or tablets, the American Academy of Pediatrics says in new guidelines. The trick is to use them wisely and to use them not only as educational tools, but in ways that bring the family together. 
 It's a change from earlier guidelines that said kids shouldnt have any screen time before age 2  meaning no computers, smart phones, television or video. 
 "We've changed our age guidelines a little bit," said Dr. Jenny Radesky, who wrote the new guidelines for the Academy. She said the new guidelines were tweaked based on the latest studies. 
 "The literature now shows that around as young as around 18 months, toddlers, can start learning very basic concepts from media if they using it together with an adult," Radesky, an expert in child development at Boston Medical Center, told NBC News. 
 "And the adult then uses media as a teaching tool to then help that child apply that knowledge to the rest of the world around them," she added. "We are saying if you want to start introducing media as young as 18 months, that's fine, but use it together with your child and choose really good media. 
 Related: How About a 'Digital Allowance' for Kids? 
 And the phrase "screen time" doesnt really apply, either, she said. Plus, if its being used to communicate directly, thats different. "We're saying explicitly infants, toddlers, any age  totally fine to Skype or video chat," Radesky said. 
 And the academy has begun naming content providers that are good for kids. 
 "So we're saying, go to Sesame Workshop, go to PBS Kids, go to Common Sense Media," she said. "We trust those products because they were developed with the input of psychologists and child development experts." 
 Common sense still should play a role, Radesky says. 
 "We're asking parents to unplug during family routines," she said. "It might be mealtimes, it might be bedtimes, it might be whatever works best for your family. But we would really like parents to be conscious of putting away distractions and being able to build one to one play and connections with their kids because that is hugely important for child development." 
 Related: Do Educational Apps Keep Kids Sharp? 
 And the academy reminds parents that too much use of TV, video and computers for entertainment can mean kids exercise less, eat more and gain too much weight. 
 "We're recommending that children under six keep it to about an hour of entertainment screen time per day because they need so much other time in their day to sleep and play and get outdoors and have conversations and just figure stuff out on their own," she said. 
 And, of course, kids need to unplug at least an hour before bedtime so they can sleep, and parents need to ensure that digital devices do not interfere with homework. 
 "Apps and websites get a lot of their money from advertising. They really want to build engagement with their user and make it prolonged, make it something that's habit-forming," Radesky noted. "So I think its not surprising that some teenagers and parents say they feel addicted." 
 But parents shouldnt feel guilty about following rules or being perfect, she said. 
 "If you're saying, 'Hey this is something we do together. We do it for fun, we do it to laugh together, we do it to create something together, we do it to connect our families across country,' then you're creating a meaning around your house," Radesky said. 
 "It's a way to integrate (digital media) into our lives in a healthy way, instead of always looking at it as a risk factor," she said. 
