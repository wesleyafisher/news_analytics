__HTTP_STATUS_CODE
200
__TITLE
Brave New World: Why (and When) Well Go From Drivers to Passengers
__AUTHORS
Paul A. Eisenstein
__TIMESTAMP
Jul 24 2017, 2:42 pm ET
__ARTICLE
 Starting a new work week can be grueling. But what if you could tap a button on your smartphone to have a car pull up moments later, you slip inside, falling into a plush recliner like the one you wish you had in your living room. Within moments, youre catching a little extra sleep while the driverless vehicle navigates its way to your office. 
 That might seem like a scene out of a science fiction movie, but its likely to become reality sooner than you realize. In Phoenix, Google spin-off Waymo is already letting residents participate in a pilot ride-sharing program using a small fleet of self-driving Chrysler Pacifica minivans. 
 Related: Now You Can Take a Ride in a Self-Driving Car 
 The first fully autonomous vehicles are expected to go on sale by 2020, and completely driverless models may follow by as early as 2022, according to plans laid out by both Ford and Daimler. 
 The convergence of self-driving vehicles and ride-sharing services will do more than make commuting easier, according to a new study by computer tech giant Intel. It envisions these emerging technologies will transform the nation as radically as the arrival of the original automobile did early in the 20th Century  in the process creating a passenger economy that will generate revenues of as much as $7 trillion by 2050. 
 We are heading towards a situation where we humans become riders, instead of drivers, said Doug Davis, an Intel vice president and co-author of the new study. At the end of the day, this presents a huge opportunity. 
 Its often said that the automobile has shaped the face of America, allowing the creation of the nations sprawling suburbs and creating a vast network of drive-through restaurants, malls, and gas stations. Weve already begun seeing a modest return to urban living as Americans grow tired of long, slow commutes. But the new transportation era could trigger even more population shifts. 
 By making it easier and more affordable to get around without having to have a home with a garage, it could encourage more people to return to cities. On the other hand, autonomous vehicles are expected to make roads safer and flow more smoothly, which might encourage more people to move even further out into exurbs and rural areas. 
 We could see a situation where people are (quite) willing to commute several hours a day, said Greg Lindsay, one of the survey participants and a senior fellow at the non-profit New Cities Foundation, during an Intel conference call. 
 Various studies issued this year, including ones from consultancies Boston Consulting Group and RethinkX, have projected that by 2030 anywhere from 25 percent to as much as 95 percent of the miles Americans travel by road will be done in driverless, electrified vehicles operated by ride-sharing services. 
 The BCG study estimated that in many parts of the country, within the next decade or two, it will cost less than half as much to use a driverless ride-sharing service as it would to own a car. One of the other advantages is that this will democratize transportation, especially in poorer communities where residents often find it difficult to get good jobs because they cant afford a car and dont have access to mass transit. 
 But here's where Intels study pokes at the great unknown. Just as the arrival of the Model T encouraged roadside services to pop up and grab commuters and suburbanites going from Point A to Point B, might entrepreneurs come up with a whole new landscape of mobility-as-a-service opportunities, the authors ask. 
 Related: Self-Driving Cars Will Turn Intersections Into High-Speed Ballet 
 If they arent driving, passengers would be able to nap, play, dine or put in a little extra work time. But the study anticipates the possibility that instead of just summoning up a generic Uber or Lyft a passenger could have a ride-sharing vehicle pull up at the end of the workday with a hot meal waiting, or their shopping all done and waiting in the trunk, perhaps even a stylist to give you a haircut or a manicure and pedicure on the way home. 
 The $7 trillion figure forecast for mobility services in 2050 breaks down into 
 One of the other big questions is who will benefit from the emergence of the passenger economy? Today, carmakers are focused on selling sheet metal, and traditional manufacturers  such as Ford, Toyota and Volkswagen  likely will continue to produce a major share of the cars on the road. But how many cars will be needed is unclear. Recently ousted Ford CEO Mark Fields, a proponent of the passenger economy, said he expects sales could drop sharply over the next 10 to 15 years as ride-sharing catches on. 
 To make up for lost revenue, said the Intel report, Carmakers may ultimately vie (with services like Lyft and Uber) to operate particular networks of vehicles for particular cities. And they will focus more on generating service business than manufacturing and car sales. 
 But new players, including up-start carmakers like Tesla, tech firms like Waymo, and ride-sharing firms like Uber and Lyft, all will be seeking to dominate, or at least grab a piece of this huge new economic pie. 
 Whoever comes to lead the new passenger economy, it will reconfigure time and then space, to some extent, and will ultimately lead to changes in land use and services, said Lindsay, resulting in a massive reconfiguring of the U.S. 
