__HTTP_STATUS_CODE
200
__TITLE
Equifax Fallout: FTC Launches Probe, Websites and Phones Jammed With Angry Consumers
__AUTHORS
Ben Popken
__TIMESTAMP
Sep 13 2017, 3:39 pm ET
__ARTICLE
 In an unusual move, the FTC announced Thursday it had launched a probe into the Equifax data breach, turning up the heat on the credit bureau after it revealed that data on 143 million Americans had been stolen after hackers exploited a months-old computer server bug. 
 "The FTC typically does not comment on ongoing investigations, Peter Kaplan, acting director of public affairs for the agency, said in a statement. "However, in light of the intense public interest and the potential impact of this matter, I can confirm that FTC staff is investigating the Equifax data breach." 
 The move by the top consumer watchdog only underscores the severity of the data heist and the importance for consumers to take immediate steps to secure their identities. Experts say the strongest, though not most painless, way to protect yourself after the breach that is to freeze your credit  but the surge in demand has overloaded the credit bureaus' abilities to handle the influx of requests. 
 As of Thursday morning, all three major credit reporting agencies intermittently gave error messages and prevented consumers from filing online requests to have their credit reports frozen. 
 Equifax's website said "System Currently Unavailable - Error 500" and suggested consumers try contacting the other credit bureaus. 
 At one point, TransUnion's website couldn't be accessed at all. Then it put up an error page featuring a stock photo of a model sitting at a computer, alongside the caption, "The website is temporarily unavailable.Please check back later." 
 Experian's website simply said, "Loading..." 
 Security expert Bob Sullivan said it's okay to wait a few days until the systems improve. Just make sure you don't get lured by confusing website copy on the credit bureau websites into signing up for unnecessary and costly monitoring services. 
 Consumers have rushed to put these "freezes" on their credit report following the massive Equifax breach that exposed the data on over 143 million Americans, about half the total U.S. population. These freezes  far more secure than credit monitoring alone  can prevent hackers from stealing your identity to open up a credit card or new account in your name. Once a consumer's credit report is frozen, the only person who can "thaw" it is the account holder, using a specially designated PIN code. 
 Here's how to put a freeze on your report. Note that you'll need your credit report "thawed" if you're shopping for a big purchase, like a new car or refinancing your house. The automated process takes a few minutes by phone. And it can cost you $10 to put the freeze back on. But expert say freezing your report is your best shot at protection. 
 Emily Lynch, a 38-year-old nurse from San Jose, California, had her identity stolen 10 years ago. Having spent years since then polishing her score to a sterling "high 800's," she's eager to avoid a repeat. 
 "It was a total nightmare," she told NBC News. "It was a full-on investigation with the postal service inspector and all three credit bureaus. It took days and months dealing with that." 
 But when she filed a credit freeze with Equifax after the recent breach was announced, the website said it was unable to process her request. When she called the automated credit freeze phone line, she was told that she had already had a credit freeze placed on her report. She never got a PIN code though, and now has no way of unlocking it. 
 RELATED: How to Freeze Your Credit Report 
 She says when she called customer service they recommended she try "tomorrow" because it was a "computer glitch." When she asked how she would get a PIN, she says the customer service representative told her, "I don't know." 
 Lynch was stunned. 
 "This is one of the one of the top three credit agencies, you're hiring people to work the phone for a huge mistake, probably one of the largest ever for this industry, and you're hiring people who have no idea what the answers are?" she said. 
 Lynch is just one of many consumers feeling frustration with Equifax's response to the massive hack. 
 "Just tried to place a credit freeze on the Equifax phone line," wrote Michael Munn, a 59-year-old small business owner from Colleyville, Texas in an email to NBC News. "After entering all the info in the prompts, it comes back and tells you Were unable to process your request. Please mail in your request.'" 
 His and other consumers' access issues have also expanded to annualcreditreport.com, a way under federal law to request a free copy of your credit report once a year from each of the three credit bureaus. Several readers said they received error messages there, too, when they tried to get a copy of their report. 
 Still other consumers complained of phone lines that rang and rang with no one picking up. 
While some wondered whether the credit bureaus were intentionally throwing up roadblocks in order to avoid losing money by being able to run credit reports and other analytics for corporate clients at will.
"I can't imagine them doing something like that unless there's a legitimate systemic reason, like they don't want their sites to crash," wrote John Uhlzeimer, a consumer credit expert who worked at Equifax in the 1990s, in an email to NBC News. "We have state and federal rights to do these things so they can't just shut down their systems to prevent us from placing alerts and setting freezes."
Consumers also have questions about whether mailing in their name, date of birth, address, and social security number is a secure way to request a credit freeze.
But privacy experts say there's nothing unsafe about using the mail. The question is what happens with your data once Equifax or the other bureaus have it.
"The big question is whether there are appropriate mail handling procedures at Equifax," New York University law professor Florencia Marotta-Wurgler told NBC News in an email.
"Who has access to these requests with sensitive information, is the location secure, is there a system by which the information is entered into a secure system, are all of these measures complying with state of the art security in the same way other sensitive information (like medical records, tax information, financial information) is?"
Equifax and Experian did not answer emailed questions from NBC News.
In an op-ed published in USA TODAY Tuesday, Equifax CEO Richard F. Smith described the breach as "the most humbling moment in our 118-year history."
"Consumers and media have raised legitimate concerns about the services we offered and the operations of our call center and website. We accept the criticism and are working to address a range of issues," he wrote. "We will make changes."
The company also offered consumers a year of free credit monitoring, an offer later expanded to include waiving credit report freeze fees for the next 30 days.
In an emailed statement to NBC News, TransUnion spokesman David Blumberg wrote, "The unprecedented number of consumers contacting us after the Equifax announcement has impacted our ability to respond to consumers as we would like."
"We have taken several steps to increase capacity and communication to support concerned consumers, such as adding agents, keeping our call center open through the weekend and authorizing overtime," he wrote.
 But statements like that aren't likely to mollify some angry customers. 
Your website doesn't work, and neither does your phone line.  How is this acceptable, considering your company is holding my credit hostage?
 For long-time consumer advocates, it comes as no surprise that the reliability and robustness of the consumer-facing side of these information behemoths should lag so far behind marketplace standards. 
 "You're not the customer, you're the commodity," said Chi Chi Wu, an attorney with the National Consumer Law Center. 
