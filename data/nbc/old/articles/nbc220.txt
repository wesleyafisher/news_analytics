__HTTP_STATUS_CODE
200
__TITLE
Georgia Police Arrest 5, Including 2 Law Enforcement Officers, in 1983 Murder
__AUTHORS
Reuters
Yelena Dzhanova
Jay Varela
__TIMESTAMP
Oct 14 2017, 2:07 pm ET
__ARTICLE
 A decades-old investigation in Georgia into the murder of a black man in 1983 culminated in the arrests of five white people on Friday, including two law enforcement officers charged with hindering the probe, officials said. 
 The body of Timothy Coggins, 23, was found on Oct. 9, 1983, in a grassy area near power lines in the community of Sunnyside, about 30 miles south of downtown Atlanta. 
 He had been brutally murdered and his body had signs of trauma, the Spalding County Sheriffs Office said in a statement. 
 Investigators spoke to people who knew Coggins, but the investigation went cold, Spalding County Sheriff Darrell Dix said at a news conference. 
 This past March, new evidence led investigators from the Georgia Bureau of Investigation and Spalding County to re-examine the case. 
 Dix did not provide details on the nature of the evidence, saying more tips were received after authorities, over the summer, announced to the media the case was re-opened. 
 Some witnesses confessed they lived with knowledge about the case for years, but were afraid to come forward, Dix said. 
 It has been an emotional roller coaster for everybody that was involved, Dix said. 
 Police arrested five people on Friday in connection with the slaying. Frankie Gebhardt, 59, and Bill Moore Sr, 58, were each charged with murder, aggravated assault and other crimes. 
 Authorities did not immediately say where Gebhardt and Moore lived. At a hearing Saturday morning, the two were denied bond, the Sheriff's Office said. 
 Related: Follow Other Cold Cases That Remain Unsolved 
 Gregory Huffman, 47, was charged with obstruction and violation of oath of office, Dix said. Huffman was a detention officer with the Spalding County Sheriffs Office but his employment was terminated after he was arrested. Huffman was being held on a $25,000 bond for the violation of oath of office and $10,000 for obstruction. 
 Lamar Bunn, a police officer in the town of Milner, which is south of Spalding County, was also arrested and charged with obstruction, as was Sandra Bunn, 58. She is Lamars mother, according to NBC affiliate WXIA. Lamar and Sandra were both released Friday night on bond of $706.75. 
 Investigators are convinced the murder was racially motivated, Dix said. 
 There is no doubt in the minds of all investigators involved that the crime was racially motivated and that if the crime happened today it would be prosecuted as a hate crime, the Sheriffs Office said. 
 Dix said that "without getting into detail, you hear people talk about overkill and this was definitely overkill. It was meant to send a message and it was brutal." 
 Several members of Coggins family appeared at the news conference Friday where authorities announced the arrests. 
 The family held out for justice all this time, said Heather Coggins, a niece of the victim. 
 Even on my grandmothers death bed, she knew that justice would one day be served, she said. 
 It was not immediately clear if any of the five arrested people had an attorney, and they could not be reached for comment. 
