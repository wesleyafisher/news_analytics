__HTTP_STATUS_CODE
200
__TITLE
Irmas Storm Surge Swallows Jacksonville With Record Floods
__AUTHORS
Jon Schuppe
__TIMESTAMP
Sep 11 2017, 8:53 pm ET
__ARTICLE
 About 400 miles north of Hurricane Irma's first landfall in South Florida, the coastal city of Jacksonville is being swallowed by water, with heavy rain and a record storm surge turning streets into churning rivers and wind-whipped waves crashing through windows. 
 The city government issued a flash flood emergency Monday morning, ordering people to go inside and stay there. Dozens of people were rescued from their homes. Mayor Lenny Curry and the local sheriff's office asked people to put white flags outside their homes to signal for help. 
 The warnings applied mostly to communities along the St. Johns River, where storm surges were expected to push water 4 to 6 feet above normal high tides. Flood levels along the river in downtown Jacksonville had already surpassed previous highs recorded in 1864. 
 That was before the afternoon high tide. 
 Photos: After Irma: Floridians Awake to Damage and Darkness 
 "It's bad now. It's going to continue to get worse," Angie Enyedi. a meteorologist for the National Weather Service, said at a briefing Monday morning. 
 By late afternoon, the water was beginning to recede, but the evening high tide was still looming. 
 Related: How to Help Hurricane Irma Victims 
 Enyedi described the confluence of the storm surge, the 10 to 20 inches of rain and the winds from the south pushing water upstream on the river as a "trifecta effect." 
 Authorities in Jacksonville, population 880,000, said Monday morning that a number of people were injured by structure fires and trees crashing through homes but that no deaths had been reported. Thousands of people remained without power late Monday afternoon. 
 The flooding in northern Florida was happening as Irma weakened to a tropical storm, moving into Georgia late Monday afternoon. 
 Authorities confirmed that two people had been killed so far in Georgia, bringing the number of U.S. deaths so far to bringing the number of U.S. deaths so far to 10, including three in Puerto Rico and four in the U.S. Virgin Islands last week. 
 The Georgia Emergency Management Agency confirmed to NBC News that one person was killed in a weather-related incident in Worth County, in south Georgia near Albany. The victim, a man, fell through the roof of his shed, which he'd climbed to clear debris, the sheriff's office told NBC station WXIA of Atlanta. 
 A second man died when a tree fell onto his home in Sandy Springs, a northern suburb of Atlanta, the city government said, calling the incident "stark reminder about how dangerous the current weather is for all." 
 South of Jacksonville, in Clay County, people were being rescued from flooded homes. 
 John Ward, the emergency operations manager of Clay County, said Monday morning that crews had saved 46 people, with an undetermined number still stranded, The Associated Press reported. 
 The flooding in Jacksonville had been expected, at least to a point. The city urged voluntary evacuation of low-lying areas last Wednesday, before the storm hit. But Irma's impact surpassed what officials expected. 
 "Now it's time to heed our warnings," Curry said. 
 The flooding could continue for about a week as the storm surge persists and tides rise and fall, with upstream water trying to flow back into the ocean, authorities said. 
 The massive storm's impact was felt farther north of Florida, even as it moved northwest, away from the coast. 
 Downtown streets flooded with rushing water in Charleston, South Carolina. Although the city wasn't in the storm's direct path, it took a one-two punch of heavy rain and a storm surge, leaving the water no place to drain, said Ron Morales, a meteorologist in the National Weather Service's Charleston office. 
 Ari Sarsalari, a meteorologist for The Weather Channel, agreed Monday night that "the biggest issues, I would say right now, are [in] Charleston." 
 "We've got high tide coming up in a little bit, and we've already got water that's really far inland," Sarsalari said. 
 Storm surges in some parts of northern Florida up to coastal South Carolina have exceeded readings recorded last year during Hurricane Matthew, which killed at least 30 people in five coastal states. 
