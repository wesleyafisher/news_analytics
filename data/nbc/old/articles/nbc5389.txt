__HTTP_STATUS_CODE
200
__TITLE
Western Wildfires: Experts from Australia Will Help U.S. Firefighters
__AUTHORS
Alex Johnson
Alastair Jamieson
__TIMESTAMP
Aug 21 2015, 7:32 am ET
__ARTICLE
 More than 70 firefighting experts from Australia and New Zealand will travel to the United States to help tackle deadly wildfires across the West as local officials warned they could not keep up with the spread of the flames. 
 The international contingent will travel to Boise, Idaho, on Sunday for a briefing and then be sent to areas including California, Oregon and Washington. 
 A mandatory evacuation order was issued Thursday night for the town of Tonasket in north-central Washington, about 60 miles northeast from where three firefighters were killed. Conditions deteriorated in the area Thursday with high winds and high temperatures. 
 Nearly 29,000 firefighters  3,000 of them in Washington  are battling some 100 large blazes across the drought and heat-stricken West, many so large they are visible from space. 
Thoughts and prayers are with those affected by the wildfires in the Northwest. pic.twitter.com/q6yqb697bN
 One of the largest  the 16,000 acre Twisp River fire  grew rapidly Thursday and has merged with the 88,161-acre Okanogan Complex of fires, the state Public Lands Commission said. 
 "We cannot keep up. We do not have the resources. It's nonstop," Okanogan County Chief Sheriff's Deputy Dave Rodriguez said. "It's all burning, and we don't have the resources available. We cannot get out and put boots on the ground for all these fires." 
 The 56 experts from Australia and 15 others from New Zealand include specialists such as heavy machinery operators, task force leaders, supervisors, airbase managers, safety officers and strike team leaders, said Craig Lapsley, emergency management commissioner for the Australian state of Victoria. 
 The deployment will allow them to support other countries in their time of need, he said in a statement. The crews  are very experienced in dealing with large wildfires having handled fires on similar terrain across Australia. 
 The overseas intervention came as the U.S. Forest Service identified the three firefighters killed in Washington as Tom Zbyszewski, 20, Andrew Zajac, 26, and Richard Wheeler, 31. 
 The men, all members of an engine crew from Okanogan-Wenatchee National Forest in central Washington, died Wednesday after their vehicle crashed and was overtaken by flames near the town of Twisp. The fire spread quickly and erratically, driven by wind and feeding on drought-parched land. 
 Zbyszewski was a physics major at Whitman College in Walla Walla, Washington, and was fighting wildfires this summer to raise money for school, his parents, both of whom have also worked for the Forest Service, told NBC News. 
 "He loved Whitman and Walla Walla. He just loved that college," said Zbyszewski's mother, Jennifer, a recreation management specialist for the Forest Service. 
 "I was already proud of him as a mom, but then to see him in the workplace and see how much people liked and respected him  I was so proud as a mom," she said. 
 The bodies were removed from the scene by a procession of emergency vehicles late Thursday. Firefighters along the route held their hands and helmets over their hearts. 
 The blazes have "burned a big hole in our state's heart," Washington Gov. Jay Inslee said Thursday, describing the outbreak as an "unprecedented cataclysm." "These are three big heroes protecting small towns," he said, urging residents to "thank a firefighter." 
