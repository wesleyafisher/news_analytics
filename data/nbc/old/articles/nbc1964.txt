__HTTP_STATUS_CODE
200
__TITLE
Robert Durst Transferred to Mental Health Unit at Elayn Hunt Correctional Facility
__AUTHORS
__TIMESTAMP
Mar 18 2015, 9:41 am ET
__ARTICLE
 Accused killer Robert Durst, the subject of the HBO documentary series "The Jinx," was transferred Tuesday night to a Louisiana state prison equipped with a mental health unit for unspecified medical reasons, authorities said. 
 The 71-year-old real estate scion remains locked up on felony weapons and drug charges. He is awaiting extradition to Los Angeles on a murder charge in the 2000 killing of his confidant Susan Berman. 
 Durst's move out of a New Orleans jail to the Elayn Hunt Correctional Facility in St. Gabriel, south of Baton Rouge, was completed at 10 p.m. ET, Orleans Parish Sheriff's Office spokesman Philip Stelly told NBC News. 
 The transfer came after a court hearing Tuesday morning. After that hearing, Durst's attorney, Dick DeGuerin, proclaimed his client's innocence. 
 DeGuerin said the HBO series was being unfairly used to prosecute him. "The warrant that was issued in California was issued because of a television show and not because of facts," he said. 
 Watch the Dateline special "Robert Durst: Inside the Long, Strange Trip" at 8 p.m. ET Thursday  
 Durst was found with five ounces of marijuana and a .38 caliber revolver, police said. He is not permitted to carry a gun because he had previously been found guilty of skipping bail in a different case. Before Durst can be sent to Los Angeles, he is expected to attend a bail hearing Monday. 
 Police on Tuesday searched the Houston home of the eccentric millionaire, leaving Durst's home in a 17-story condominium building at about 8:30 p.m. local time and carrying two white cardboard document boxes, The Associated Press reported. One of the officers was wearing a Los Angeles Police Department badge. They declined to comment to the AP. 
 The Associated Press contributed to this report. 
