__HTTP_STATUS_CODE
200
__TITLE
Death Toll in California Wildfires Rises to 31; Some Survivors Escaped Just In Time
__AUTHORS
Chiara Sottile
Daniel Arkin
__TIMESTAMP
Oct 13 2017, 10:51 am ET
__ARTICLE
 SONOMA, Calif.  Teri Reynolds-Thompson is newly homeless. 
 The retired nurse lost everything when one of the monstrous fires ravaging Northern California swept into her cul-de-sac. She, her two kids and their two dogs are now sleeping on Red Cross cots inside the gymnasium at Sonoma Valley High School. 
 "My house is burned to the ground, and I have no insurance," said Reynolds-Thompson, 65, wiping tears from under her blue-rimmed glasses. 
 Reynolds-Thompson is one of countless Californians whose lives have been ripped apart by the wildfires that have devastated California wine country, killing at least 31 people and destroying more than 3,500 homes and structures. Exhausted firefighters are working around the clock to beat back the 21 blazes raging simultaneously across the region. 
 Law enforcement and emergency officials confronted their own grim task on Thursday: the search for the missing and the dead. 
 Related: 'Like an Atom Bomb Hit': Santa Rosa Residents Confront Devastation 
 Authorities were trying to track down about 400 people who remained missing, and they were beginning the somber work of recovering bodies from incinerated homes, Sonoma County Sheriff Rob Giordano told reporters. 
 Identification of bodies may be difficult and could take some time, Giordano warned. Officials have found some remains intact, but others are "nothing more than ash and bones," he said. 
 It was unclear how many on Sonoma County's list of missing were duplicates or even people who are actually safe. 
 Napa County, meanwhile, continued to direct people to search through a website hosted by the American Red Cross. 
 Getting information has been disjointed, with the public relying partly on separate media updates throughout the day that are broken out by county and agency. 
 "It really calls into question a better response," state Sen. Bill Dodd of Napa said about the handling of missing-person reports. "Maybe there's some best practice when we're done with this that we can try to make sure that there is a better clearinghouse." 
 Firefighters remain on guard as erratic wind gusts were expected to hit wine country starting Friday, said Ken Pimlott, director of the California Department of Forestry and Fire Protection, or Cal Fire. Those conditions could throw fire crews back on their heels and whip flames into more fury. 
 "It's going to continue to get worse before it gets better," Pimlott said Wednesday. 
 Cal Fire officials said at least 191,400 acres had burned so far  an area almost the size of New York City. Firefighters from across California and Nevada were called in as reinforcements. 
 Napa County Fire Chief Barry Biermann said at a news conference Thursday that some of the fires had combined and that "until we get them contained, they're all going to be a problem." 
 Photos: From Above, California Wine Town an Ashy Wasteland 
 But dry air and gusty winds up to 45 mph were making it harder for firefighters to make significant gains as many of the infernos continued to burn out of control. 
 For longtime residents whose lives have been shattered, the future is a thick fog. 
 "We are in trouble now," said Reynolds-Thompson, who canceled her fire insurance to pay for a costly dental procedure for her son. "Where are we going to live? What are we going to do?" 
 It was 9 a.m. Monday when Reynolds-Thompson, who can't walk and uses a wheelchair, saw the fire rush toward her home. 
 "We thought it was going the other way, so we didn't pack anything," she said. "We never thought we were in any danger. It was the wind that did it." 
 Her children, Gabriel, 34, and Cassandra, 38, barely had time to grab their mother's pink purse before the fire was "lapping at the door," she said. 
 With no time to get her wheelchair, Reynolds-Thompson's children held her up and carried her out of the house as the eucalyptus tree in their front yard ignited in flames and crashed down on their wooden fence. 
 Reynolds-Thompson was one of nearly 300 people who crowded into the high school gym Thursday. Most had nowhere else to go. 
 Sonoma resident Anne Shapiro has been working 12-hour volunteer shifts since Monday. Shapiro, also a retired nurse, has helped evacuees fill prescriptions and has assessed patients with high blood pressure and allergies. 
 a 
 She said the gym had become a makeshift home for those who lost everything or were desperate to escape the city's acrid air. 
 "There are signs on the [Sonoma] plaza today, and it makes me cry, but they say, The love here is thicker than smoke,'" Shapiro said. "And it's true." 
 Chiara Sottile reported from Sonoma. Daniel Arkin reported from New York. 
