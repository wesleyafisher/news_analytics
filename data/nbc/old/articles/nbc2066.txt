__HTTP_STATUS_CODE
200
__TITLE
Cancer Pen Could Detect Tumors During Surgery in Seconds
__AUTHORS
Maggie Fox
__TIMESTAMP
Sep 7 2017, 8:20 am ET
__ARTICLE
 A handheld pen can detect cancer cells within seconds, speeding up diagnosis and helping surgeons more accurately remove tumors, researchers reported Wednesday. 
 The probe works in real time, and is at least as accurate as removing a tissue sample and sending it to a pathologist, the team at the University of Texas at Austin reported. 
 The pen uses a little drop of water to make the analysis and doesnt require any cutting of tissue, the team reported in the journal Science Translational Medicine. They hope it can help make for less invasive surgery that gets every piece of tumor while also leaving behind as much healthy tissue as possible. 
 "If you talk to cancer patients after surgery, one of the first things many will say is 'I hope the surgeon got all the cancer out,' " Livia Schiavinato Eberlin, an assistant professor of chemistry who led the work, said in a statement. 
 "It's just heartbreaking when that's not the case. But our technology could vastly improve the odds that surgeons really do remove every last trace of cancer during surgery." 
 Related: Musician Plays Sax During Brain Tumor Surgery 
 The team designed the pen and programmed a mass spectrometer to detect compounds that make lung, thyroid, ovary and breast tumors different from healthy tissue. 
 They said it was accurate 96 percent of the time. 
 "Any time we can offer the patient a more precise surgery, a quicker surgery or a safer surgery, that's something we want to do," said James Suliburk, head of endocrine surgery at Baylor College of Medicine, who worked with the team. 
 "This technology does all three. It allows us to be much more precise in what tissue we remove and what we leave behind. 
 The team tested their pen on more than 250 samples of human tumors, calibrating the molecular signature of each type of tumor. Tumor cells have different activity than normal cells do, and researchers have been working for decades to use mass spectrometry to try to identify those differences quickly. 
 "Cancer cells have dysregulated metabolism as they're growing out of control," Eberlin said. "Because the metabolites in cancer and normal cells are so different, we extract and analyze them with the MasSpec Pen to obtain a molecular fingerprint of the tissue 
 They also tried the pen out in live mice growing human breast tumors. 
 The device, named MasSpec Pen, enables controlled and automated delivery of a discrete water droplet to a tissue surface for efficient extraction of biomolecules, the team wrote. 
 Related: Blood Test May Find Cancer Before Symptoms Show 
 Currently, the most often used technique to diagnose cancer is by a pathologist looking at a frozen sample of tissue under a microscope. Its a highly skilled job but can be time consuming and prone to errors. 
 Waiting for a pathologist can be especially nerve racking for a surgeon who wants to know on the spot if all the tumor has been removed, and even worse for a patient who is told he or she must go back for more surgery because all of the tumor was not removed. 
 Many women diagnosed with breast cancer, for example, undergo breast-conserving surgery, which involves removing the lesion of interest with a rim of normal tissue and preserving the rest of the breast, Eberlins team wrote. 
 One of the greatest challenges a breast cancer surgeon faces is determining the delicate boundary between cancerous and normal tissues. 
 Their idea is for a surgeon to be able to use the pen during surgery to test the remaining tissue after a tumor is removed to see if its healthy. It takes just a few seconds for it to sample a tumor with a drop of water that is then sucked through a tube to the mass spectrometer itself. 
 The researchers are the first to say it will take much more testing until their new device is validated. A few hundred samples is not enough to say for sure it accurately distinguishes tumor from healthy tissue. 
 Plus, the use of mass spectrometry to diagnose cancer is itself still highly experimental. 
 And their live tests were in mice, not in human patients. 
 The team, working with funding from the National Cancer Institute and the Cancer Prevention Research Institute of Texas, have filed patents for the technology. 
