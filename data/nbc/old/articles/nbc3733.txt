__HTTP_STATUS_CODE
200
__TITLE
Trump Prepares New Iran Strategy, Including Bid to Alter Nuclear Deal
__AUTHORS
Mike Memoli
Hallie Jackson
Vivian Salama
Carol E. Lee
__TIMESTAMP
Oct 6 2017, 6:34 pm ET
__ARTICLE
 WASHINGTON  President Donald Trump is preparing to declare that the international nuclear accord with Iran is no longer in Americas national security interests, making a symbolic reversal of another of his predecessors signature initiatives but one that may largely leave the policy in place. 
 The president plans to announce the move next week as he unveils what aides say is a comprehensive new strategy for Iran, looking beyond its nuclear program to focus on other destabilizing activities in the Middle East, government officials tell NBC News. 
 The president told reporters last month that he had made a decision about whether to again certify Irans compliance with the terms of the 2015 nuclear agreement, but did not reveal what that decision was. 
 The White House press secretary, Sarah Huckabee Sanders, said Friday that the announcement of a new comprehensive strategy would be announced in the coming days and would look at all the bad behavior of Tehran, not just its nuclear program. 
 The Iranian regime supports terrorism and exports violence, bloodshed and chaos across the Middle East, Trump told reporters before a meeting with senior military leaders at the White House on Thursday evening. That is why we must put an end to Iran's continued aggression and nuclear ambitions, he said. They have not lived up to the spirit of their agreement. 
 Administration officials have also spent the past week consulting with key lawmakers about the new strategy. The president is "leaning toward seeing if he can strengthen U.S. law" on not just the nuclear program but the overall posture toward Iran, a senior administration official said. "We think there are ways to work with Congress to do more" to address the full range of Iran challenges. 
 Several Democratic senators attended a briefing at the White House on Wednesday night in what one senior administration official called a due diligence exercise to allow lawmakers to offer feedback about their approach. White House chief of staff John F. Kelly in particular has worked to ensure that appropriate stakeholders have a forum, that official told NBC News. 
 Legislation passed in 2015 requires the president to certify Iranian compliance every 90 days, something to which President Barack Obama reluctantly agreed in the face of bipartisan concern about whether Iran could be trusted to remain in accord with the agreement. 
 Trump has now twice provided that certification, even as he maintains the agreement was flawed and that Iran has violated the spirit of it. When the White House last certified Irans compliance in July, it stated that Iran had not taken any action, including covert activities, that could significantly advance its nuclear weapons program. 
 It was the initial recommendation by Secretary of State Rex Tillerson to certify in April that first ruptured his relationship with the president. The letter to Congress was sent just before a midnight deadline. 
 Ahead of the next certification deadline on Oct. 15, all indications are that is still the case. But officials say the president is considering using a separate provision of the 2015 legislation to withhold certification. 
 That assessment would trigger a sanctions review process in Congress, one that the White House hopes to use as leverage to enlist Americas European allies in new negotiations with Iran, though its unclear if the president would weigh in on whether Congress should vote to reimpose sanctions. 
 NBC News reported in September that the president was leaning toward a strategy that would ultimately put the question of whether to fully withdraw from the nuclear accord, referred to as the JCPOA, in the hands of Congress. 
 Sen. Tom Cotton, R-Ark., one of the chief critics of the nuclear agreement, advocated this approach in a speech in Washington this week. 
 The president should decline to certify, not primarily on grounds related to Irans technical compliance, but rather based on the long catalog of the regimes crimes and perfidy against the United States, as well as the deals inherent weakness, he said. The world needs to know were serious, were willing to walk away, and were willing to reimpose sanctions  and a lot more than that. And theyll know that when the president declines to certify the deal, and not before. 
 Cotton met privately with Trump at the White House on Thursday. 
 National Security Adviser H.R. McMaster told senators this week that while Trump was poised to decertify Irans compliance, he will not ask Congress to pass new sanctions. That would suggest the move may be aimed to appeal to the presidents base but with little effect, since it does not lead to either withdrawal from the Iran deal or new sanctions  both of which would isolate the U.S. and anger allies. 
 Without reinstating sanctions that have been waived in return for Irans agreeing to suspend its nuclear program, the accord would effectively be left in place. 
 "This is more of a political maneuver than a strategic decision, Sen. Jack Reed of Rhode Island, the top Democrat on the Senate Armed Services Committee, said Friday on MSNBC's Andrea Mitchell Reports. 
 Testifying Tuesday before the committee, Defense Secretary James Mattis said the U.S. should stay with the agreement if it can confirm that Iran is abiding by it. 
 I believe, at this point in time, absent indications to the contrary, it is something the president should consider staying with, he said. 
 But he said certifying that the overall accord with Iran is in the national interest goes into a broader definition of national security. 
 The president has to consider, more broadly, things that rightly fall under his portfolio of looking out for the American people in areas that go beyond the specific letters of the JCPOA," Mattis said. "In that regard, I support the rigorous review that he has got going on right now." 
 As part of briefings with lawmakers, the administration is working to convince them that while it concedes Iran is in compliance, it is engaging in destabilizing activities across the region, from supporting Syrian President Bashar al-Assad, to funding and arming Hezbollah fighters, and that it should be punished through crippling sanctions. Congress already imposed new sanctions this year as part of a legislative package that targeted Russia, but the administration may seek additional measures. 
 A senior administration official said the broader Iran strategy that Trump signed off on will include a host of new moves to more aggressively confront Irans actions in the region. 
 Certainly sanctions will be a part of it, the official said, though they wont be aimed at Irans nuclear program. The president will not call on Congress to reinstate nuclear-related sanctions. 
 The United States has been doing a woefully inadequate job at countering Irans malign behavior, the official said. 
 Both administration officials and lawmakers recognize that the president could ultimately change course on the Iran deal. 
 A lot can change between now and Oct. 15, one lawmaker told NBC News on the condition of anonymity because discussions with the administration have not been made public. 
 The president has two broad avenues for altering U.S. participation in the nuclear agreement, each of which would trigger additional policy questions the administration must address. Under the terms of the JCPOA, the U.S. could accuse Iran of violating the agreement and cease implementation of the deal, including reinstating all sanctions that had been in effect prior to its effective date of Jan. 16, 2016. 
 But the preferred option appears to be one in which the U.S. remains party to the agreement, and acts within the framework of that 2015 legislation initially imposed as a check on the Obama administrations ability to unilaterally enter into the agreement. 
 Decertifying that the Iran deal remains in the national interest launches a fast-track legislative process for potential renewed sanctions, but most Democrats in the Senate, and a handful of Republicans, might oppose that even if the president recommends it. 
 One possible separate legislative effort would be to revise the 2015 Iran oversight legislation to permanently do away with subsequent 90-day certification requirements. The new legislation could also add clauses unrelated to the nuclear program to give Congress or the president new tools to sanction Iran for ballistic missile tests. 
