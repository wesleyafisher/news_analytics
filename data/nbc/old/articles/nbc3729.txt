__HTTP_STATUS_CODE
200
__TITLE
Motion Picture Academy Ousts Harvey Weinstein Amid Sexual Misconduct Allegations
__AUTHORS
Phil McCausland
__TIMESTAMP
Oct 14 2017, 5:37 pm ET
__ARTICLE
 Embattled movie mogul Harvey Weinstein was expelled Saturday from the ranks of the Academy of Motion Picture Arts and Sciences in the latest show of solidarity by Hollywood against a onetime titan of the industry. 
 The academy's 54-member board said in a statement that they had "voted well in excess of the required two-thirds majority" to oust Weinstein, who has seen a wave of Hollywood actresses in recent days accuse him of sexual misconduct over the past three decades. 
 Members of the board include well-known stars such as Tom Hanks, Whoopi Goldberg, Laura Dern, Michael Mann, Steven Spielberg and Lucasfilm President Kathleen Kennedy. Nearly half of the members of the academys board of governors are women. 
 "We do so not simply to separate ourselves from someone who does not merit the respect of his colleagues but also to send a message that the era of willful ignorance and shameful complicity in sexually predatory behavior and workplace harassment in our industry is over," the academy said. 
 The film organization, which hosts the Oscars, came under pressure to expel the influential media mogul after the New York Times published an Oct. 5 report that alleged the Hollywood producer had abused his position of power to sexually harass and assault women, including high-profile actresses such as Ashley Judd, Angelina Jolie and Gwyneth Paltrow. 
 Three women also made explosive accusations in a New Yorker report that included published audio of a 2015 New York Police Department sting in which Weinstein appeared to admit to groping Filipina-Italian model Ambra Battilana Gutierrez. 
 Related: After Weinstein Allegations, Rose McGowan Emerges as Major Voice 
 More than three dozen women  including successful and aspiring actresses, former employees, and models  have come forward with stories of Weinstein's alleged sexual misconduct. In a tweet Thursday, actress Rose McGowan accused Weinstein of raping her. 
 Weinstein's representatives have said in a previous statement that "any allegations of non-consensual sex are unequivocally denied" by him and that he did not retaliate against women who rejected his advances. 
 Weinstein was fired from The Weinstein Co. on Sunday, a television and movie company he co-founded with his brother, Bob Weinstein. 
 Their companies have been involved with the production or distribution of more than 80 Oscar-winning films. Harvey Weinstein also won a statue as a producer for the 1999 Best Picture winner, "Shakespeare in Love." 
 A source close to the Hollywood producer told NBC News that Weinstein planned to challenge his firing. 
 Bob Weinstein, now the sole head of The Weinstein Co., told The Hollywood Reporter in a blistering interview Saturday that his brother would not return under any circumstances. 
 "I cannot control other peoples actions," Bob Weinstein said. "But he was fired by the board, okay? I was on that board. I fired him. He can fight. It will be a losing fight." 
 Bob Weinstein emphasized that he only knew that his brother was having a number of extramarital affairs, but he did not know that Harvey Weinstein had allegedly committed the various acts of sexual misconduct. 
 Still, Bob Weinstein said, he had disowned his brother long ago. 
 "I havent had a relationship with him as a brother for many, many years," Bob Weinstein added. "But I'm ashamed that he is my brother, to be honest, and I am ashamed that these are his actions. I'm not thinking at this moment about dissecting what may or may not have affected any past situation. I'm so in the here and now and feel sick." 
 The academy in its statement didn't mention whether it would strip Harvey Weinstein of his past Oscar win or if it has the ability to do so. 
 Whats at issue here is a deeply troubling problem that has no place in our society, the academy said. "The Board continues to work to establish ethical standards of conduct that all Academy members will be expected to exemplify." 
