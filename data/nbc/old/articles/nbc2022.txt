__HTTP_STATUS_CODE
200
__TITLE
Study Linking Early Miscarriage to Flu Vaccine Puzzles Doctors
__AUTHORS
Maggie Fox
__TIMESTAMP
Sep 14 2017, 9:12 am ET
__ARTICLE
 Vaccine experts are puzzling over a study that appears to link one particular flu vaccine with early miscarriages. 
 Its far too soon to say the vaccine actually did cause miscarriages, and they say the study, paid for by the Centers for Disease Control and Prevention, did not find anything definite. Only 17 women had miscarriages that might be linked with vaccination. But its a troubling signal that they are following up on. 
 In the meantime, pregnant women are still urged to get flu vaccines because they and their unborn babies are at high risk from actually getting flu  and thats something thats been confirmed by many studies over a long time in tens of thousands of women. 
 This study does not and cannot establish a causal relationship between repeated influenza vaccination and (miscarriage) but further research is warranted, James Donahue and Edward Belongia of the Marshfield Clinic Research Institute in Wisconsin and colleagues reported in the journal Vaccine. 
 An earlier, similar study done by the same team found no evidence linking vaccines with miscarriage. 
 "We dont want people to panic over this headline. Get your flu shot. Its safe," said Dr. Laura Riley, an obstetrician at Massachusetts General Hospital and Harvard Medical School. 
 Related: Flu, Fever in Pregnancy Linked With Autism 
 Its a tricky issue because of the renewed and increasingly vocal anti-vaccine movement. Support is still small for such groups, which allege the government and researchers, along with many journalists, are colluding to cover up what they say are the dangers of vaccines. Public health experts fear such groups will seize on a small study such as this one as proof vaccines are dangerous. 
 Dr. Amanda Cohn, senior adviser for vaccines at the CDC, said the open publication of the study shows theres no cover-up and proves that public health agencies are watching out for vaccine safety. 
 Really, this study highlights how strong our vaccine safety surveillance is, Cohn said. This is exactly the type of signal that we see because we do such a diligent job focusing on flu vaccine safety. Clearly, we need to study this more. 
 The study included 485 women who had miscarriages in 2010, 2011 and 2012 and compared them to 485 pregnant women who did not. 
 Related: Studies Show Flu Vaccines Safe for Pregnant Women 
 They found those who had been vaccinated against influenza 28 days before the miscarriage, and in the first trimester of pregnancy, were more likely to have had a miscarriage. When they looked more closely at the data, the Marshfield team found this only held among women who had also been vaccinated the previous flu season. 
 And it was only women who got the H1N1 vaccine  which was newly introduced in 2009. 
 We didnt expect to have these results at all, said Donahue. 
 Related: Mom's Flu Shot Protects Baby, Too 
 Once we started realizing that the results were unexpected and we were totally surprised by it, we sort of figured this was going to cause a bit of a stir. 
 The team is now looking at more data involving more women over more years. The Advisory Committee for Immunization Practices, which decides vaccination advice, discussed the findings in 2015 and will discuss them more at a meeting in October. 
 This study is evidence that we have a strong and highly functional vaccine safety monitoring system in the U.S., Belongia said. "This is exactly how science works." 
 Single studies almost never lead to changes in policies or recommendations, because they may have errors. "So what we have here is a signal," said Belongia. "Its not definitive. It might be wrong. I have no doubt that we will eventually sort this out and understand whats going on, but that takes time. 
 Its challenging to understand in part because women often dont even know they have miscarried if it happens in the first trimester. About 40 percent of U.S. pregnancies are unexpected, and theres little information on how many women have miscarriages if they happen early in pregnancy. 
 Theres also very little data on vaccination during the first trimester. Pregnant women are strongly urged to get flu vaccines but that can often happen during the second and third trimesters. 
 We dont specifically recommend that women get vaccinated during their first trimester. We just say get vaccinated at some time during their pregnancy, Cohn said. 
 Women who are worried should talk to their doctors, she said. 
 Related: More Studies Show Vaccines Do Not Cause Autism 
 There are, however, many studies showing vaccination in general during pregnancy is not only safe, but beneficial to both the mother and the baby. 
 And pregnant women are especially vulnerable to flu. During the 2009 H1N1 flu pandemic, pregnant women were more than four times as likely to be hospitalized with influenza than the general population, a CDC team found. Although pregnant women comprise 1 percent of the population in the United States, they accounted for approximately 5 percent of all 2009 H1N1-related deaths in the United States, they wrote in a 2012 report. 
 We know that flu vaccine is safe and effective at preventing serious infection in pregnant women and their unborn children, Cohn said. It is really important that women continue to get vaccinated. 
