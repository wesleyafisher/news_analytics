__HTTP_STATUS_CODE
200
__TITLE
Williams Bum Shik Kim on Glasses-Themed Essay: Im Not One-Dimensional
__AUTHORS
Amy DiLuna
__TIMESTAMP
Jun 5 2017, 9:36 am ET
__ARTICLE
 As part of an as-told-to essay for College Game Plan's How I Got In series, Williams College student Bum Shik Kim reflects on his college journey. 
 Palisades Park, New Jersey 
 A passionate student of physics and law who knows the value of standing out from the crowd. 
 Theres a practicality to applying to college, and a sort of intuitiveness to it. You have to write a common application essay. You have to take the SAT and subject tests. Those are thing youre going to need to struggle through. Those were things in my control so tried to do the best I could. 
 The more intuitive thing is just to be yourself. If I was an admissions officer, what would I want in me? I want to be unique, I want to stand out. Because who wants to be normal? 
 Im the son of first-generation immigrants from Korea. My parents only idea of a good college in America was the Ivies. 
 I had never heard of Williams until a high school teacher mentioned that her friend was an alum. One day at a college fair the Yale table was overwhelmed. But the admissions guy for Williams wasnt talking to anyone. He just drew me in, so I applied early decision. 
 Have an advocate in school. School is hard; youre going from 8 a.m. to 3 p.m. and youre emotionally taxed. I was fortunate to have three or four teachers in high school that I could go to for recommendations or to vent to. If you can build up the habit of seeking out advocates in high school, it becomes super handy in college when youre more on your own and away from family. 
 I own seven pairs of glasses, including purple ones, because Im at Williams. Im a liberal arts kid. I love physics but I also love political science. So I talked about my different career trajectories. 
 My orange glasses stood for the high school physics teacher I wanted to be, the quirky weird science guy. White were for me imagining myself as a Supreme Court justice, since it matches with the black robes. Teal was being the congressman, passionately debating on the House floor. 
 I used the colored glasses as a metaphor for all the careers I could see myself thriving in. Im telling the committee Im not one-dimensional, and the lasting message put in the essay was that every time I put on a different pair of glasses, these dreams and goals can change, and Im OK with that. I want college to be a place where I can explore these things and help prepare myself for whats to come. 
 I went to a high school where you specialize in things. I was a law and justice major, but I also worked as a consultant designing a physics curriculum. I was Quiz Bowl captain, I did a lot of volunteering, I did a summer program with the New Jersey Governors School in the Sciences, I interned at the Princeton Plasma Physics Lab, I played alto sax, and was in the National Honors Society. 
 I didnt seek these things out thinking, I want to be a physics and law kid. I just kind of liked it all. And if you like it, it follows that when youre interviewing for college your enthusiasm for that topic naturally shows. You dont have to worry about being like I was the president of this. There are plenty of students out there like that. 
