__HTTP_STATUS_CODE
200
__TITLE
Fentanyl in NYC: How Officials Are Battling the Opioid Crisis in Trumps Hometown
__AUTHORS
Corky Siemaszko
__TIMESTAMP
Aug 13 2017, 2:01 pm ET
__ARTICLE
 The opioid epidemic that President Donald Trump has vowed to defeat is knocking on his front door. 
 Four suspected drug dealers and nearly 20 pounds of heroin and fentanyl were seized from a Manhattan apartment six subway stops away from Trump Tower earlier this month. 
 In June, the Drug Enforcement Administration made its biggest fentanyl bust ever in New York City when its agents seized 40 pounds of the deadly painkiller from a Bronx hotel room and arrested a Colorado man suspected of smuggling it into the city. 
 And in recent weeks, police in the borough of Staten Island  a Republican stronghold in this overwhelmingly Democratic city  reported 29 drug overdoses, 13 of them fatal. 
 But so far, Trump has said nothing publicly about the plague, which police say killed 1,374 people in his hometown last year  437 more than in 2015 and more than the number of people killed by car crashes and homicides combined. 
 And it wasn't lost on New York Mayor Bill de Blasio that Trump declined at first to declare a national emergency. 
 It's troubling that it took the President this long to see the deaths of 1,300 New Yorkers and tens of thousands of Americans as an emergency, De Blasio spokeswoman Olivia Lapeyrolerie said. We couldn't wait for federal action any longer, which is why we launched our own plan to combat this epidemic using proven health and safety efforts. Hopefully, the Trump administration will put its money where its mouth is and actually help save lives." 
 Related: The Awful Arithmetic of America's Overdoses May Have Gotten Worse 
 The Trump administration did not respond to several questions from NBC News about the response to opioids in New York City. On Thursday, the president reversed course and announced he would declare a national emergency in the battle against opioids. The presidential commission on opioids had previously recommended such a move, which will allow the executive branch to direct funds towards additional treatment facilities and waive some federal restrictions on Medicaid recipients getting treatment. 
 Some of those measures mirror what New York is trying to achieve with its program, Healing NYC, a $38 million initiative launched earlier this year aimed at reducing opioid overdose deaths by 35 percent over the next five years. 
 As part of its strategy, the city is in the process of distributing 100,000 naloxone kits to the Department of Health and Mental Hygiene, the NYPD and other first responders and boost its ongoing Save a Life, Carry Naloxone public awareness campaign. 
 We are already seeing results from our effort, Lapeyrolerie wrote. In 2017, (the) NYPD saved nearly 300 lives thus far with naloxone. 
 The city, according to Lapeyrolerie, is also expanding opioid treatment facilities and creating additional mental health clinics in high-need schools that account for a disproportionate share of suspensions and mental health issues, which can be precursors for substance misuse. 
 On the enforcement side, the NYPD has created Overdose Response Squads to target drug dealers and has assigned 84 detectives and hired 50 lab technicians to combat this epidemic and disrupt the supply of opioids before they come into the city. 
 NYPD Commissioner James O'Neill, however, cautioned when the city's strategy was unveiled, that you can't arrest your way out of this problem. 
 Related: Discarded Syringes From Heroin Crisis Creates Environmental Problems 
 When we interview those who have been fortunate enough to not die after an overdose we don't lock them up, he said. This is about teaching everyone from school age kids to adults with major substance abuse problems to make good decisions, to resist peer pressure, and to live their lives in a positive, healthy, and productive way. I'm confident New York City can lead the way in this. 
 Heroin, however, is hardly new to New York City. It was a scourge back in the 1920s. And it was the subject of William Burroughs' searing first novel Junky, which was published in 1953. 
 In the 1970s, New York State passed the Rockefeller Drug Laws, which were named after then-Gov. Nelson Rockefeller and which mandated a minimum 15-years-to-life sentence for anybody convicted of selling two ounces of heroin. 
 But while that filled the jails with low-level drug dealers, a disproportionate number of whom were minorities, it did little to dampen the demand of New Yorkers in desperate need of a fix. To this day, the Big Apple accounts for about a third of all the heroin seized in the U.S., according to the DEA. 
 Recently, the NYPD moved to shut down a shooting gallery that was operating out in the open in the South Bronx after it was exposed by The New York Daily News. 
 Fentanyl, the drug that killed Prince last year and which the DEA says is 25 to 50 times more powerful than heroin, is also no stranger to the city. 
 It's always kind of been here, but not at the level we have now, said Ric Curtis, anthropology professor at the John Jay College of Criminal Justice. Especially the last year. 
 Fentanyl first appeared in the South Bronx back in the 1990s, he said. 
 Remember Tango and Cash? asked Curtis, referring to a batch of fentanyl-laced heroin that killed five people in New York City in 1991, prompting the NYPD to send out cars with loudspeakers warning junkies not to buy it. 
 Related: America's First Opioid Court Focuses on Keeping Users Alive 
 But fentanyl did not immediately make the same kind of inroads into New York City that it did in opioid-ravaged Rust Belt states, Curtis said. That's because big-city junkies don't get hooked on drugs the way people do in suburban and rural areas, he said. 
 We don't follow the same pattern that's in the heartland, where people get hooked on prescription painkillers and then move on to heroin, he said. Historically, in New York City, people start off on heroin. Some drug abusers will do heroin for many years until they get on Medicaid and then they wean off the heroin and move on to prescription pills. 
 Fentanyl, however, is upending that pattern. 
 Because heroin is so cheap, suppliers are lacing it with fentanyl to get more bang for the buck, Curtis said. 
 Typically, a bag is $10 and that's been the standard price since the 1970's, he said. It's the only product that inflation didn't hit. 
 Most drug dealers don't even know they're selling fentanyl-spiked heroin, Curtis said. And if people suddenly start overdosing, the junkies don't run away from the dealers  they clamor for more. 
 Frankly, there's nothing like a couple of overdose deaths to spur sales, he said. If somebody overdosed from your stuff, it means it's extra powerful. Boom, all of a sudden they can't get enough of the stuff. 
 That's not a new thing, either. 
 Back in 1991, when Tango and Cash was wreaking havoc, the New York Times reporter Evelyn Nieves tracked down a then 39-year-old heroin addict named Richie who said: When an addict hears that someone O.D.'d, the first question they ask is: 'Where'd they get it?' Because they want to find some of it for themselves. 
 And that, say experts like Curtis, is the hardest habit to break. 
