__HTTP_STATUS_CODE
200
__TITLE
Trump Threatens to Cancel Iran Nuclear Deal If Its Not Strengthened
__AUTHORS
Adam Edelman
__TIMESTAMP
Oct 13 2017, 3:49 pm ET
__ARTICLE
 President Donald Trump on Friday threatened to terminate the Iran nuclear deal if Congress doesn't strengthen it, warning the agreement was merely a "temporary delay" in Tehran's quest to obtain nuclear weapons. 
 In a speech at the White House laying out what he called a "new strategy" for dealing with Iran, Trump also accused Iran of violating both the letter and spirit of the deal and said the U.S. would impose new sanctions on the Islamic Revolutionary Guard Corps, Iran's elite security and military organization. 
 By declining to certify that Iran is in compliance with the 2015 nuclear agreement, Trump put its future squarely in the hands of Congress, which will now have to decide whether to attach new conditions to the agreement or reimpose sanctions on Iran with regard to the country's nuclear program. Those sanctions were lifted as part of the agreement, and reimposing them would effectively destroy the deal. 
 Trump, who throughout his campaign and his presidency has expressed intense disdain for the deal, made clear Friday that he wouldnt hesitate to cancel it if complications with Congress arose in moving forward on toughening it. 
 "In the event we are not able to reach a solution working with Congress and our allies, then the agreement will be terminated," Trump said, adding that American participation "can be canceled by me, as president, at any time." 
 "I can do that instantaneously," the president told reporters after his speech. 
 When a reporter reminded Trump he has said that he planned to rip up the deal, he responded: "I may do that. The deal is terrible." 
 Trump, in his address, contended that Iran was "not living up the spirit of the deal"  in contrast to a statement from his own secretary of state, Rex Tillerson, on Thursday that the country was "technically compliant" with it. 
 Trump argued that, under the deal, the U.S. "got weak inspections in exchange for no more than a purely short-term and temporary delay in Iran's path to nuclear weapons" and that "in just a few years," Iran "can sprint towards a rapid nuclear weapons breakout." 
 He also ripped Irans leaders as leading a "fanatical regime" that is spreading "death, destruction and chaos around the globe." 
 Trump said he wanted Congress to specifically look at the deals sunset clauses, under which the agreement expires 10 years after it goes into effect, as well as details regarding how the deal is enforced and over the nation's development of ballistic missiles. 
 Ahead of Trump's speech Friday, Sen. Bob Corker, R-Tenn., chairman of the Foreign Relations Committee, released details of what he called a "legislative strategy to address bipartisan concerns" about the Iran deal that would not violate U.S. commitments. Corker's office said the plan would require "the automatic 'snapback' of U.S. sanctions should Iran violate enhanced and existing restrictions on its nuclear program." 
 But getting a decision on sanctions out of Congress  which has been unable to agree on any significant legislation this year  is considered unlikely, meaning the status quo on the Iran agreement could remain. 
 In a briefing with reporters on Thursday afternoon, Tillerson and National Security Adviser H.R. McMaster explained that the administration's goal would be for Congress to create "trigger points," including those mentioned in Trump's speech, for Iran that would mandate the reimposition of sanctions if Tehran doesnt meet specific revised criteria. 
 McMaster told senators last week that while Trump was poised not to certify Irans compliance, the president would not ask Congress to pass new sanctions. That would suggest Trumps move may be aimed primarily at appealing to his base, since it would not lead to either withdrawal from the Iran deal or new sanctions  both of which would isolate the U.S. and anger its allies. 
 Without reinstating sanctions that were waived in return for Irans agreeing to suspend its nuclear program, the accord would effectively be left in place. 
 In his speech Friday, Trump outlined a more aggressive overall strategy for Iran, focusing on the countrys "destabilizing influence" in the region, including its support for terrorism and militants. 
 He also called for additional sanctions to be levied on the Islamic Revolutionary Guard Corps, the hardline military wing that had already been sanctioned for weapons proliferation under prior administrations. 
 One of the big issues of concern to Tehran has been how the president would treat the IRGC. This is also of concern to European countries that do business with shell companies actually owned by the corps. 
 According to a senior administration official, Trump intends to designate the corps as a supporter of terrorism. The administration is required to make the designation under legislation Trump signed in August covering sanctions against Iran, Russia and North Korea. Although officials had until Oct. 31 to decide, they included the designation in Friday's speech on the larger Iran strategy. 
 Those in the administration who are worried about Iran misinterpreting or overreacting are eager to emphasize the distinction between being a supporter of terrorism and an actual terrorist organization. They also emphasize that it has no practical effect because of other existing designations against the Revolutionary Guards, but that it could enrage the Iranians. 
Trump's announcement Friday was met with resistance from the international community. Key allies,including Britain, France and Germany  known as the E3  as well officials from Russia, warned that Trump's move could isolate the U.S.
Democrats also criticized the move. Former Vice President Joe Biden wrote in a lengthy Facebook post that  unilaterally putting the deal at risk does not isolate Iran. It isolates us. House Minority Leader Nancy Pelosi, D-Calif., tweeted that Trump's "threats against the #IranDeal are a grave mistake" and "threaten America's security & credibility at a critical time."
And last month, Iranian President Hassan Rouhani told NBC's Lester Holt in an interview that decertification is tantamount to withdrawing from the agreement and would make no one ... trust America again.
 The president's decision comes ahead of a deadline Sunday that triggers a 60-day window for lawmakers to determine whether to reimpose sanctions related to Iran's nuclear program that were lifted as part of the 2015 agreement. 
 Legislation passed in 2015 requires the president to certify Iranian compliance every 90 days, something President Barack Obama reluctantly agreed to in the face of bipartisan concern about whether Iran could be trusted to remain in accord with the agreement. 
 Trump, who often denounced the nuclear agreement on the campaign trail, once describing it as "a deal at the highest level of incompetence, twice provided that certification before Friday, even as he maintained that the agreement was flawed and that Iran has violated "the spirit" of it. 
