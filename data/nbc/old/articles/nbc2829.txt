__HTTP_STATUS_CODE
200
__TITLE
Pregnant Women Should Avoid Zika Area in Florida as Infections Rise
__AUTHORS
Felix Gussone, MD
__TIMESTAMP
Aug 1 2016, 1:58 pm ET
__ARTICLE
 The Florida Department of Health has identified 10 more people who likely contracted Zika virus through a mosquito bite, Governor Rick Scott said Monday  bringing the total number of people with locally transmitted Zika to 14. Pregnant women are advised to avoid travel to the area just north of downtown Miami, health officials said. 
 Women who are considering getting pregnant and have traveled to the area should delay for eight weeks, Dr. Thomas Frieden, director of the Centers for Disease Control and Prevention said during a call Monday. 
 "Any pregnant women who traveled to this area on or after June 15 should speak to their doctors," Frieden said. "Pregnant women who live or work there should be tested during their first and second trimesters." 
 Pregnant women are also advised to use condoms for the duration of the pregnancy. 
 Two of the confirmed infections in Florida are women and 12 are men. 
 According to the DOH, active Zika transmissions are still limited to a small one square-mile area north of downtown Miami, the same area where the first 4 cases last Friday were identified. Among the 10 new individuals with locally acquired Zika infection, six didnt show any symptoms and were discovered by health workers going door-to-door to collect urine samples to test for the virus. 
 So far, the Florida Health Department has tested more than 200 individuals in Miami-Dade and Broward counties who live or work near the people that have been confirmed with likely mosquito-borne transmissions. 
 In light of the fears of a growing outbreak of the disease, Governor Scott has asked the CDC to assist the Florida Department of Health in the fight against local spread of Zika. 
 Following todays announcement, I have requested that the CDC activate their Emergency Response Team to assist DOH in their investigation, research and sample collection efforts. Their team will consist of public health experts whose role is to augment our response efforts to confirmed local transmissions of the Zika virus," Governor Scott in a statement. 
 Related: Zika: A Tale of Two Cities 
 Staying away from areas where mosquitoes are circulating is the best way to avoid being bitten. Repellents and sprays containing DEET are among the most effective, the CDC advises. DEET is safe for pregnant women and for children to use. 
 The most common symptom of Zika is a raised rash. People also report fever, muscle aches and red eyes while others don't remember any symptoms at all. There's no cure for Zika infection but fortunately for most people, the infection is mild and clears up in about a week. 
 Related: Here's What Zika Infection Looks Like 
