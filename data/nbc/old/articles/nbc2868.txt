__HTTP_STATUS_CODE
200
__TITLE
College-Bound? It's Time to Get Your Shots and Have 'The Talk'
__AUTHORS
Susan Donaldson James
__TIMESTAMP
Aug 7 2017, 9:22 am ET
__ARTICLE
 Immunization record? Check. Meningitis and flu shots? Check. But did you have that talk about sex, drugs and alcohol? 
 Going to college is a big step into the adult world  but its also the first exposure to the germs that come with living in close quarters, as well as the social temptations that come with it. 
 Keeping healthy during the college years can be a challenge. 
 The great thing about college is that it happens during adolescence, said Dr. Lonna Gordon, a specialist at Mount Sinai Adolescent Health Center in New York City. Its a time for exploration and learning about new thingsbut that can pose some health risks and harm, if you are not careful. 
 Ingrid Rizo, a single mom from Roxborough, Colorado, says she thinks she has things covered as she prepares to send her daughter Lina off to the University of Missouri to study journalism in the fall. 
 This week they have been scurrying around making sure all her immunizations are current. 
 Rizo has also had conversations with Lina about substance abuse, pregnancy risk and STDs. Before she leaves, Ill probably buy a box of condoms and make sure to replace them when she is back for Christmas, she said. But I dont want to know if theyve been used! 
 Rizo still worries. 
 I'm torn between my pride in her and my desperate sadness about moving her to 10 hours away, Rizo, 48, told NBC News. 
 I was always a little bit uncomfortable about her going so far away, she said. One day she said to me, what if I am sick, who will take care of me? 
 One of her biggest concerns is that the college health center doesnt take the familys health insurance and Lina is a triathlon athlete. Cycling, these kids are wheel-to-wheel and there are a lot of accidents. 
 Lina is also native American, and Rizo, an adoptive mom, worries about the micro-aggressions that come with being a minority student in a new environment. 
 I know I am thinking about all the worst scenarios, she said. Legitimately, I think her health will be fine. And Lina is definitely not as nervous as I am. 
 But when she is on her own, my eyes and disapproval will not be there, she said. Who knows if it will work? 
 Beyond social temptations, a college career can be derailed by poor nutrition or lack of sleep, say experts. 
 Kate Carter, a 34-year-old mother from New Hampshire said her step-daughter dropped out of college after three emergency room visits for chest colds and untreated urinary tract infections. 
 She was used to me cooking daily and was suddenly on her own, said Carter. I doubt she was unique in that she ignores symptoms until she's very sick. 
 Libby Caruso, director of the health center at The College at Brockport in New York, says, Getting good sleep can help  all around. 
 Theyll be less susceptible to infection and more likely to make better decisions, said Caruso. Sleep amount and quality has diminished in all of us, college students, especially. 
 With all this in mind, heres a checklist so your student can have the healthiest college experience: 
 Immunizations: 
 We dont want an outbreak of pertussis on a college campus, said Gordon, While most adolescents will do fine  a five or 10-day bad cold that will keep you out of class  those with problems of the heart and lungs may end up hospitalized. 
 This year, the college hopes to get it right, said health center director Caruso, with clinics in the student union, library, dorms and athletic center. Its really helpful if parents encourage their kids to be immunized and they can have more influence than they sometimes think. 
 Social hazards: 
 Sexually-transmitted diseases: 
 Know your medical history: At 18, freshman are legally adults and colleges dont have to share medical information with parents. This can be a challenge in emergency situations, said Gordon. But if you go to the doctor, you can invite anyone into the conversation. 
 For kids with chronic medical conditions, its important to have a sense of student health for urgent care needs, but perhaps also having a doctor in the area to collaborate [with college doctors]. 
