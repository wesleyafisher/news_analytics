__HTTP_STATUS_CODE
200
__TITLE
Opioid Crisis: NIH Launches New Push to Fight Epidemic
__AUTHORS
Maggie Fox
__TIMESTAMP
May 31 2017, 5:22 pm ET
__ARTICLE
 The National Institutes of Health announced a new push Wednesday to fight the opioid crisis, with fresh efforts to develop better drugs to fight addiction, to treat pain, and to stop overdoses. 
 The giant agency  the worlds biggest funder of medical research  is meeting with drugmakers to figure out the best and fastest ways to come up with a better solution to treating pain than the current addictive opioid class of drugs. 
 Every day, more than 90 Americans die from opioid overdoses, NIH director Dr. Francis Collins and Dr. Nora Volkow, head of the NIHs National Institute for Drug Abuse, wrote in a special report for the New England Journal of Medicine. 
 Collins said the NIH will be meeting with a dozen or more drug companies over the coming weeks to develop better overdose drugs; better drugs to treat opioid addiction; and pain drugs that not only work better, but that are not addictive. 
 Some may be reformulations of drugs that we already know can be used for treatment of overdoses, Collins told reporters. Some of them will be totally new ideas about pathways that have been recently discovered. 
 One example: Narcan nasal spray, a quick-acting formulation of naloxone (Adapt Pharma Incs Narcan) that was approved by the Food and Drug Administration in 2015. The opioid antagonist blocks the effects of the opioid drugs, which slow breathing, sometimes fatally. NIH and the drug's maker collaborated to develop it and get it approved. 
 Related: Trump Admin to Pay Cash Promised by Obama to Fight Opioid Crisis 
 The NIH will now work with private partners to develop stronger, longer-acting formulations of antagonists, including naloxone, to counteract the very-high-potency synthetic opioids that are now claiming thousands of lives each year, Collins and Volkow wrote. 
 Industry is very interested and committed, Collin added in the phone briefing. 
 Getting there may not be easy. Reformulating opioids to deter abuse doesnt improve their efficacy, and opioids havent been shown to relieve pain or improve functioning any better than nonopioid medications or nonpharmacologic treatments  which is the primary reason the CDC recommended those alternatives for managing chronic noncancer pain, Dr. William Becker and Dr. David Fiellin, both of Yale University, wrote in a separate commentary in the New England Journal of Medicine. 
 Related: Ohio Sues 5 Pharma Companies over Opioid Abuse Crisis 
 President Donald Trump has suggested slashing the NIH budget, but leaders in Congress have said theres no way theyll do that and Trump himself has also said fighting the opioid addiction crisis is a priority. 
 This will be something we put very high on the list of things we just have to do, Collins said. 
 Drug companies and science alike have failed to keep moving, in part because it took a while to realize that new formulations of synthetic opioids, such as fentanyl, were even more deadly and addictive than heroin and morphine. 
 It was partly because we believed as recently as 20 years ago that we had this problem solved, Collins said. 
 The holy grail was to develop an opioid that was not addictive  but its failed completely, Volkow added. One big mistake was believing that if people were taking opioids to treat pain, they would not become addicted, Volkow said. 
 Related: Opioid Abuse Epidemic Worse Than Ever, Surgeon General Says 
 However, advances in science might make it easier to design better drugs. Molecular level imaging of the nerve receptors involved in pain may allow for the precise design of drugs that can block pain without having side effects. 
 Collins also pointed to an extremely rare condition making people insensitive to pain. Its caused by a mutation in a gene called SCN9A. 
 A drug that blocked that pathway, Collins said, might be able to block pain effectively without the side-effects of opioids. Its still early days to know how that might work, he admitted. 
 Without taking on the politics of fighting drug abuse, Volkow and Collins also noted that its important to ensure that people get the treatments they need. Its pretty obvious that if we are going to have new drugs, people need access, Collins said. 
 And Volkow pointed to Canadian research showing that providing people with safe sites to inject drugs not only helps prevent the transmission of diseases, but gets people into treatment for their addiction. The extent to which this can be applied to the U.S. has not been studied, she said. 
 Separately, the Health and Human Services Department said it was releasing $70 million authorized by Congress last year as part of the Comprehensive Addiction and Recovery Act. 
 Governments and groups must apply for the money, which is available for three broad areas: 
 We are committed to bringing everything the federal government has to bear on this health crisis," HHS Secretary Tom Price said in a statement. 
