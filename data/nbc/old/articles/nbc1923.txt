__HTTP_STATUS_CODE
200
__TITLE
Daimlers Smart Cars Are Going All-Electric in U.S. Market
__AUTHORS
Paul A. Eisenstein
__TIMESTAMP
Feb 14 2017, 1:25 pm ET
__ARTICLE
 Daimler's Smart brand has confirmed it will stop selling gasoline-powered vehicles in the U.S. and Canada, focusing exclusively on an electrified version of its two-seat Fortwo model. 
 It is the latest development in a series of setbacks for Smart, which has continued to lose ground despite a redesign last year. The timing was unfortunate, according to industry analysts, coming at a point when gasoline prices were hovering at barely $2 a gallon and millions of North American buyers have been migrating from passenger cars to utility vehicles and other light trucks. 
 News of the decision to go all-electric came in the form of a letter sent by Dietmar Exler, the CEO of Mercedes-Benz USA, to Smart dealers. Declaring Smart has always been synonymous with innovation, Exler added that "Developments within the micro-car segment present some challenges for the current smart product portfolio. He concluded that, "A dedicated focus on the electric drive in the U.S. and Canada provides a logical step to support a sustainable, zero emissions future." 
 The Smart brand has struggled almost from the day it was conceived in 1994  originally as a joint venture with Swiss watch maker Swatch, which quickly dropped out of the project. 
 Related: Tesla Briefly Idling California Plant to Prep for Model 3 Launch  
 Daimler, better known for its high-line Mercedes-Benz brand, decided to target the emerging market for minicars, a segment that was expected to become especially popular in Europe, with its crowded streets. But the development process was slow and problem plagued and Smart failed to meet a number of its original objectives. 
 The brand came to Canada in 2004, but took another four years to cross the border into the United States, initially distributed through a unit set up by entrepreneur Roger Penske. After a burst of initial sales, demand plunged and Penske transferred the operation back to Daimler. 
 Sales continued to slide, even as gas prices began to rise. The one sign of progress came in 2014 with the introduction of a second-generation battery model, the Smart ED, or Electric Drive. It captured a full 25 percent of the brands U.S. sales in its first year. 
 New Design, New Hopes 
 Daimler had hoped to kick-start the Smart brand with the launch of a complete redesign of the little Fortwo model for the 2016 model-year. To hold down costs and boost economies of scale, the new model was developed as part of an ongoing partnership with the Renault-Nissan Alliance  the French brand selling its own minicar as the Twingo. 
 Sales have since picked up momentum in Europe, but Smart sold just 6,211 of the two-seaters in the U.S. last year, a 16 percent dip from 2015. Worldwide, Smart sales were 144,470, or barely 7 percent of the Daimler passenger vehicle total. 
 Related: Ford Investing $1B in Self-Driving Company 
 A new battery version of the Smart Fortwo  as well as a larger, four-seater not sold in the U.S.  was introduced at the Paris Motor Show last September. 
 Smart is going all-in on battery power, declared Daimler AG Chairman and CEO Dieter Zetsche, as the newest models drove onto the stage. 
 The Smart ED  offered as both coupe and convertible  is no Tesla Model 3 fighter. The driveline is as downsized as the Smart Fortwo itself. Its three-phase synchronous motor, powered by a 17.6 kilowatt-hour lithium-ion battery, spins out 80 horsepower and 118 pound-feet of torque. Surprisingly, the numbers that Smart quotes indicate the 2017 makeover will actually be slower than the previous version of the ED, taking 11.4 seconds for the coupe to hit 60, and 11.7 seconds for the cabriolet. Top speed is governed at 81 mph. 
 Range runs less than 80 miles, or barely one-third of what the Chevrolet Bolt EV delivers per charge. But Smart officials contend that shouldnt be a problem since their primary audience is an urban motorist who wont drive far during a typical day. 
 Related: GM Exploring Options for Additional Battery-Electric Vehicles 
 One big improvement comes to the fore when its time to plug the new ED models in. Charging times have been cut by half, Zetsche noted during the Paris news conference, from 5.5 to 2.5 hours using a 240-volt charger. On a standard U.S. household 110V outlet, however, it will take you a full 22 hours to top the battery off. One big challenge for Smart, according to industry analysts: many potential urban buyers might not have access to a charging station or even a standard AC outlet. 
 The decision to go all electric nonetheless comes at a time when proponents of battery technology are seeing signs of increased interest in the U.S. market. But Smart has to hope it can make the switch work or it could come entirely unplugged. 
