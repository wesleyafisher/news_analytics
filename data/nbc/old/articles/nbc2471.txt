__HTTP_STATUS_CODE
200
__TITLE
UN: More Than 1 Million Could Flee Mosul Ahead of Siege
__AUTHORS
Richard Engel
Alastair Jamieson
Corky Siemaszko
__TIMESTAMP
Oct 17 2016, 3:22 pm ET
__ARTICLE
 QAYYARAH, Iraq  While Iraqi and Kurdish forces advanced on Mosul, the United Nations warned Monday that more than a million residents might try to get out before the fighting starts  and that ISIS could use them as "human shields." 
 "Already, the operation is so large [that] we're struggling to reach people who need help," said Lise Grande, the U.N. resident and humanitarian coordinator for Iraq. "There isn't an organization in the world that can handle population movement of more than 150,000 people at one time." 
 Related: 1 Million Could Be Driven From Homes by Battle for Mosul 
 Stephen O'Brien, the U.N. under-secretary general for humanitarian affairs, said he was "extremely concerned" that families were at risk of "being caught in crossfire or targeted by snipers." 
 "Tens of thousands of Iraqi girls, boys, women and men may be under siege or held as human shields," he said in a statement earlier. "Thousands may be forcibly expelled or trapped between the fighting lines." 
 But while the United Nations painted a dire picture, there was cheering as Iraqi government tanks rolled across the desert to begin the long-awaited offensive to end the city's brutal two-year occupation by ISIS. 
 Despite a sky blackened by burning oil wells deliberately set ablaze by retreating ISIS fighters, morale was high and soldiers flashed victory signs as huge columns of military vehicles moved on Mosul. Some troops even danced. 
 "The hour has struck," Iraqi Prime Minister Haider al-Abadi said as he announced the move. "The campaign to liberate Mosul has begun." 
 The operation was assisted by U.S.-led airstrikes, including four strike near Mosul  destroying six tunnel entrances, five supply caches and two artillery systems  and a fifth strike near Qayyarah, in which two ISIS-controlled buildings were hit. 
 The massive and complex military operation will be the largest in Iraq since U.S> troops left in 2011 and, if successful, the biggest blow yet to ISIS. 
 While many militants have already fled, forces advancing from all sides of the city face danger from booby traps and improvised explosive devices. An Iraqi news crew witnessed a suicide attack targeting Kurdish Peshmerga forces. 
 Brig. Helgord Hekmat, a spokesman for the Kurdish forces, said 4,000 Peshmerga launched from about 60 miles east of the city and had retaken nine villages. 
 "They are advancing to Bartella and liberating all areas on the way to that location," he told NBC News. 
 Iraqi forces were also advancing toward the city and had freed 10 villages to the south and southeast of Mosul, Brig. Yahya Rasool, a joint operation spokesman, told NBC News. Rasool said people in two villages, Alhod and Lazaga, killed four militants before army and federal police personnel arrived  evidence, he said, that people are ready to cooperate with Iraqi forces. 
The hour has struck. The campaign to liberate Mosul has begun. Beloved people of Mosul, the Iraqi nation will celebrate victory as one
 A source inside Mosul told NBC News that ISIS militants and their families had "disappeared from most parts" in the east of the city and that unknown groups were trying to kill any remaining jihadis. Iraq's second-largest city has been under ISIS rule for more than two years since government forces retreated. 
 It is still home to up to 1.5 million civilians, according to U.N. estimates. 
#MosulOffensive: Peshmerga troops raise Kurdistans flag in liberated villages. pic.twitter.com/nRvnkNDsYk
 Abadi said that ISIS would be "punished" for its crimes and that the province's cities and villages would be rebuilt. 
 "We will bring life back to Mosul and all other areas around Mosul," he added. 
 In a statement, Defense Secretary Ashton Carter called the offensive a "decisive moment in the campaign to deliver [ISIS] a lasting defeat." 
 Brett McGurk, the State Department official coordinating the effort against the group, said it would liberate Iraqis from "two years of darkness." 
 Army Lt. Gen. Stephen Townsend, commander of Combined Joint Task Force Operation Inherent Resolve, said in a statement that the operation could take "weeks, possibly longer." 
 Iraqi Brig. Gen. Haider Fadhil told The Associated Press that more than 25,000 troops, including paramilitary forces made up of Sunni tribal fighters and Shiite militias, were taking part. 
 The role of the Shiite militias has been particularly sensitive, as Nineveh, where Mosul is located, is a majority-Sunni province and Shiite militia forces have been accused of carrying out abuses against civilians in other operations in majority-Sunni parts of Iraq. 
 Fadhil voiced concern about potential action from Turkish troops based in the region of Bashiqa, northeast of Mosul. 
 Turkey sent troops to the area late last year to train anti-ISIS fighters there. But Baghdad has seen the Turkish presence as a "blatant violation" of Iraqi sovereignty and has demanded that the Turks withdraw, a call Ankara has thus far ignored. 
 
