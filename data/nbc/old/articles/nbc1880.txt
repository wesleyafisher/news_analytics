__HTTP_STATUS_CODE
200
__TITLE
Hurricane Harvey: Houston Imposes Overnight Curfew to Fight Looting, Break-Ins, Lowlives
__AUTHORS
Alex Johnson
__TIMESTAMP
Aug 29 2017, 9:29 pm ET
__ARTICLE
 Houston authorities imposed an overnight curfew Tuesday, incensed that criminals impersonating law enforcement officers in the wake of Hurricane Harvey were victimizing the city's hardest-hit residents. 
 The curfew, which takes effect immediately and will continue indefinitely, was initially set from 10 p.m. to 5 a.m. CT, but shortly thereafter, Mayor Sylvester Turner modified it to begin at midnight. 
 With the police department strained to the limit after the historic storm, thousands of people are on their own, and "the reality is there are some people who might be inclined to take advantage of this situation," he said. 
 "I don't want them to worry about anybody breaking into their homes or looting while they are away," he said. 
 The Harris County District Attorney's Office said 14 people have been arrested on suspicion of looting, but Police Chief Art Acevedo said there had been scores of other reports of even worse behavior. 
 "We've had armed robbers going around yesterday robbing our community, victimizing them again  Harvey wasn't enough," Acevedo said, calling the perpetrators "lowlives" (sic) and promising: "We're coming after you, so good luck." 
 Specifically, people wearing T-shirts claiming to represent Immigrations and Customs Enforcement and the U.S. Homeland Security Department's Homeland Security Investigations division. He said real agents of both agencies don't wear T-shirts, so Houstonians should demand to see ID or call 911 before they open their doors. 
 "This is Texas. We're tough. We're a really good city. We're a welcoming city," he said. "But we're not a city that tolerates people taking advantage of people at their lowest point." 
 Acevedo said his officers would begin pulling back from nonstop search-and-rescue missions "to focus on what the Houston Police Department does best, which is going after criminals and keeping the people of Houston safe ... white Houston, black Houston, brown Houston, rainbow coalition Houston." 
 Related: Veteran Houston Police Officer Drowns While Responding to Harvey 
 "One great thing about this city [is that] no matter what neighborhood you live in, rich or poor, the very, very vast majority of the people in that community are good people," he said. 
