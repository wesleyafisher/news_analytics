__HTTP_STATUS_CODE
200
__TITLE
Federal Health Officials Get High Marks for Harvey Response
__AUTHORS
Maggie Fox
__TIMESTAMP
Sep 5 2017, 9:37 am ET
__ARTICLE
 The federal health response to Hurricane Harvey gets high marks from disaster experts, and they credit years of preparation. 
 By the time Harvey made landfall on the southeast Texas coast last Friday, the Health and Human Services Department had pre-positioned 460 staffers from the National Disaster Medical System to be ready to roll in and help. 
 Theres a 250 bed Disaster Medical Assistance Team (DMAT) station at the Houston Convention Center, where at least 9,000 people are holed up after floodwaters swept swathes of the flat plains and even flatter streets and neighborhoods of the low-lying city. 
 It's not just the DMAT. The Public Health Service has been activated, the Indian Health service has people who are here, said Dr. Terri Schmidt, chief medical officer for the Incident Response Coordination Team. 
 It's a wide range of different responses. 
 Related: The Unexpected Health Effects of Hurricane Harvey 
 HHS sent more than 50,000 pounds of medical equipment and supplies within days, the Federal Emergency Management Agency said, and contracted for air and ground ambulances. 
 HHS is helping evacuate hospital patients and is assisting those who rely upon electricity-dependent medical equipment, the department said in a statement. 
 HHS has activated its Disaster Distress Helpline, a toll-free call center, that is available at 1-800-985-5990 to aid people in coping with the behavioral health effects of the storm and help people in impacted areas connect with local behavioral health professionals. 
 Related: Houston Shelters Struggle to Help Evacuees 
 Its a lot better than the notoriously slow and disorganized response to Hurricane Katrina, which devastated New Orleans and much of the Gulf Coast 12 years ago this week, in 2005, experts said. 
 The most striking thing to me is a quite remarkable improvement in the capacity of the federal agencies that are responsible for preparedness and response, said Dr. Irwin Redlener, director of the National Center for Disaster Preparedness at Columbia University. 
 Thats quite different from president Bushs heck of a job, Brownie moment, which reflected not only a complete case of incompetence but a lack of awareness by the president about what was going on. 
 Related: Where is Disgraced Former FEMA Chief Michael Brown Now? 
 Redlener gives some credit to the Trump administration, but says the Obama administration and career HHS officials deserve far more. FEMA director Brock Long has been in place only since June, and Dr. Robert Kadlec, Assistant Secretary for Preparedness and Response (ASPR) at HHS, took office just days before Harvey hit. 
 But years of preparation helped them hit the ground running. 
 During the Obama administration, both the ASPR and FEMA were significantly upgraded, Redlener said. 
 The people who preceded Kadlec and Long were extraordinarily competent. 
 Those would include former FEMA director Craig Fugate, now a disaster consultant in Florida, and former ASPR Dr. Nicole Lurie, now at the Indian Health Service. 
@POTUS44 questioned me after Sandy, are we adapting to future risk due to climate change? That became a mission, stop building to the past. https://t.co/bG0HS4fuAs
 Craig Fugate was one of the most competent administrators in history, and Nicki Lurie was, also. Kadlec and Long came into highly functioning agencies and that credit goes to Obama. 
 Fugate said he was not going to second guess his successors. 
 "We did a lot to make sure there weren't gaping big holes in the organization. We wanted to make sure the team was ready to go, the funding was ready to go," he told NBC News. 
 "We spent a lot of time on the transition. We wanted to leave them a good team and capability." 
 There are two levels of federal workers  career civil servants, and political appointees who change with each administration. "If there is any credit, it is actually to the the career folks," Fugate added. 
 "Congress also gave quite a bit of attention to FEMA." 
 One person who gets no praise  President Donald Trump. 
 "There is a striking contrast to me between the effective, assertive actions of the key federal disaster agencies and the rather extraordinary lack of compassion and understanding on the part of the president himself," Redlener said. 
 "Donald Trump is a distant, disconnected president who happens to have really good people in the key agencies." 
 For instance, Redlener said Kadlec apparently had no hesitation to counter Harvey, in contrast to the uncoordinated response by state and local officials. It is a hodgepodge of random decision-making on a very local level that makes it difficult to have a uniform plan, Redlener said. 
 And the federal government cannot force groups to cooperate. "They cant come in and take over. A lot of people think they can, but they cant," said Rich Serino, a former FEMA deputy administrator under Obama who is now a consultant. 
 PHOTOS: A Look Back at Hurricane Katrina's Wrath 
 The way that the federal government works, is we only come in when the local response is overwhelmed, it can no longer do the job, said Schmidt. "We're here to support them. They were doing a great job. They've been doing it all over the community then they began to use up their resources. 
 Serino said state and local officials will have to do most of the work, along with nonprofits and individuals. 
 "Its not one person, one program, one department that can do it alone," he said. Serino says state authorities have improved their response. "Theyve been able so far to inspire the public," he said. 
 But experts agreed that the jury is still out on the medical response to Harvey. 
 "It's still too early to tell how the response to Harvey is going, said Dr. Gabor Kelen, director of emergency medicine at Johns Hopkins Medicine and of the Office of Critical Event Preparedness and Response. 
 The federal, state and local response appears to be taking a more proactive approach from previous storms of similar magnitude. We haven't been hearing the same high volume of frustration that we encountered in 2005 in the wake of Hurricane Katrina." 
 Related: Obama Speaks 10 Years After Katrina 
 A lot will depend on whats found as Harveys floodwaters recede. 
 First, are we doing things that will minimize fatalities? Are the specific lifesaving strategies working? Redlener asked. 
 More than 1,800 people died in Katrina, and many bodies were found in the attics and front rooms of flooded homes, some months after the storm. 
 I dont know what the final fatalities numbers are, Redlener said. 
 We see houses submerged to the roof. Where are the families? Are they OK? Did they evacuate? Did they drown? We just dont know. 
