__HTTP_STATUS_CODE
200
__TITLE
Harvey Weinsteins College Moves to Revoke His Honorary Degree
__AUTHORS
Dan Corey
__TIMESTAMP
Oct 11 2017, 6:03 pm ET
__ARTICLE
 Film executive Harvey Weinstein's alma mater moved to rescind the honorary degree it gave him after a wave of Hollywood actresses came forward to accuse the producer of sexual harassment and other women alleged that he forced himself on them. 
 Officials at the University at Buffalo, which is part of the State University of New York (SUNY) system, asked the SUNY board revoke the honorary doctorate of humane letters, which the school bestowed on Weinstein in 2000. Weinstein attended the university from 1969 to 1973. 
 "The university is well aware of the allegations involving Mr. Weinstein," university spokesman John DellaContrada said in a statement. "The university has initiated the process, pursuant to the SUNY Board of Trustees policy, for the revocation of a SUNY honorary degree." 
 Weinstein was fired by the company he co-founded after explosive back-to-back reports by The New Yorker and The New York Times added significant details to a Times report last week of instances of alleged sexual harassment by Weinstein spanning decades. 
 Only SUNYs 18-member Board of Trustees can revoke an honorary degree, according to SUNY policy. 
 Related: Cara Delevingne Comes Forward With Sexual Harassment Claims Against Harvey Weinstein 
 The University of Buffalo announced its decision a day after a Change.org petition led the University of Southern California to reject a $5 million donation from Weinstein to support women filmmakers. 
 Rutgers University in New Jersey, meanwhile, has chosen to keep a $100,000 donation from Weinstein to help fund the school's new chair honoring feminist icon Gloria Steinem. 
 "Harvey Weinstein and the H. Weinstein Family Foundation contributed a gift of $100,000 in honor of his late mother, who shared Gloria Steinem's hopes for female equality," Rutgers spokeswoman Karen Smith told NJ.com. "We can think of no better use of this donation than to continue this important work." 
 Opinion: Firing Harvey Weinstein Does Not Get His Company Off the Hook 
 In 2004 and 2005, Disney donated $22,750 on behalf on Miramax to support a media study scholarship at Buffalo, DellaContrada said. The entire gift went toward scholarships that were awarded to university students in 2005. 
 But Weinstein never made a personal donation to Buffalo, DellaContrada said. 
