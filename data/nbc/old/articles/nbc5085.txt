__HTTP_STATUS_CODE
200
__TITLE
In Mosul Operation, ISIS Hit With More Airstrikes Than Ever Before
__AUTHORS
F. Brinley Bruton 
__TIMESTAMP
Oct 24 2016, 10:14 am ET
__ARTICLE
 America and its allies have launched more airstrikes against ISIS in the past week than at any other time in its ongoing fight against the extremists, according to President Barack Obama's counter-ISIS envoy. 
One week into #Mosul operation, all objectives met thus far, and more coalition airstrikes than any other 7-day period of war against #ISIL.
 Just one week after the start of a concentrated effort to recapture the key Iraqi city of Mosul from ISIS, the military coalition has retaken 78 villages and towns as of Monday, according to Brig. Yahya Rasool, a spokesman for the Joint Operation Command. That has brought Iraqi and Kurdish forces closer to the city itself. 
 The coalition dropped 1,776 bombs, artillery rounds and rockets on ISIS targets in the area around Mosul from Oct. 17, when the operation began, through Sunday, Air Force Col. John Dorrian, a U.S. military spokesman, told NBC News. 
 Related: U.S. 'Definitely' Not Behind Strike on Mosque: Military 
 The attacks helped destroy 136 extremists "fighting positions," 18 tunnels, 82 vehicles, 26 car or truck bombs and 60 artillery pieces and mortars, he said. 
 "The attacks have also killed hundreds of Daesh fighters who are attempting to block the Iraqi advance to liberate Mosul," Dorrian added, using another name for ISIS, which conquered swaths of Syria and Iraq in 2014. 
 The campaign aimed at crushing ISIS' base in Iraq may turn out to be the largest battle since a 2003 U.S. invasion toppled dictator Saddam Hussein and triggered years of chaos and turmoil. 
 About 1.5 million people are thought to still be in Mosul, Iraqs second-largest city and ISIS' last major urban stronghold. The fighting has forced about 6,000 people to flee their homes, according to aid agencies, and it may well uproot more than a million, according to the United Nations. 
 Also Monday, ISIS expanded it attacks against Iraqi army and Kurdish forces to relieve pressure on fighters repelling the Mosul offensive. 
 
