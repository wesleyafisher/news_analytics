__HTTP_STATUS_CODE
200
__TITLE
Better Care Act is No Such Thing, Top Medical Journal Says
__AUTHORS
Maggie Fox
__TIMESTAMP
Jun 27 2017, 9:07 pm ET
__ARTICLE
 The Senates answer to Obamacare repeal may be called the Better Care Reconciliation Act, but its no such thing, editors of the New England Journal of Medicine said Tuesday. 
 The highly influential medical journal joined a growing chorus of groups representing doctors and other healthcare professionals who oppose the GOPs efforts latest attempt to repeal and replace the Affordable Care Act, widely known as Obamacare. 
 The U.S. Better Care Reconciliation Actis not designed to lead to better care for Americans, wrote, Dr. Jeffrey Drazen, the editor-and-chief of the widely known medical journal. 
 Like the House bill that was passed in early May, the American Health Care Act (AHCA) would actually do the opposite: reduce the number of people with health insurance by 22 million, raise insurance costs for millions more, and give states the option to allow insurers to omit coverage for many critical health services, they wrote. 
 Independent analysis from the Congressional Budget Office forecasts higher premiums for many people, several areas that provide no choice at all of health insurers for people who dont have employer-sponsored coverage or Medicare, and a move to bare-bones plans that cover very little. 
 The bill does, however, cut taxes to the tune of $700 billion. These range from taxes on medical devices and tanning parlors to income taxes. 
 What would get better under the BCRA is the tax bill faced by wealthy individuals, which would be reduced by hundreds of billions of dollars over the next decade, the NEJM editorial board wrote. 
 Related: Vote on GOP Health Plan Delayed 
 Health insurance is largely regulated by the states. The ACA imposed an additional layer of federal regulation, which sought to stop the once common practice of offering cheap, meager health plans that covered very little medical care cost, that capped coverage if people started requiring pricey care and that forced customers to pay stiff upfront deductibles. 
 The CBO analysis and work done by other independent groups such as the Kaiser Family Foundation show that if states were to get waivers under the proposed new laws, theyd go for it. Theres a concern there could a race to the bottom, Kaisers Karen Pollitz told reporters on a conference call. There would be pressure, I think, in every state for legislatures to adopt these waivers. 
 Related: CBO Says Senate Bill Would Leave 22 Million Uninsured 
 That worries physician groups and the NEJM board. They say as doctors on the front lines of providing medical care, theyve seen firsthand what happens when people dont have good health insurance. 
 While Republicans often characterize their plan as offering more choice, the medical groups and other advocates point out that people do not want to pay for plans that provide little or no coverage, which could likely be the case. 
 In some states, health plans could become largely worthless, particularly for patients with preexisting conditions, they wrote. 
 More comprehensive plans would end up becoming extremely expensive, the CBO said. 
 As Americans know all too well from the pre-ACA era, many underinsured and uninsured people would risk being bankrupted by health care costs or would die for lack of access to needed care, Drazen and colleagues wrote. 
 Related: Protests Against Latest Obamacare Repeal Effort 
 Women, low income individuals, opioid addicts, and many others stand to suffer under the Senate proposal, the NEJM board said. That echoes what other medical groups predict. 
 The amount of money set aside by BCRA for treatment of the disease of addiction is no better than pocket change, the American College of Emergency Physicians said in a statement. 
 Like many U.S. physician and hospital organizations that are speaking out against the BCRA, we whole-heartedly oppose sacrificing Americans health care and health to further enrichment of the wealthy, Drazen and colleagues concluded. 
#healthcare is about people's lives-- 22 million lives that will not have their basic health needs covered @MiddayTomHall
 Medical groups that have joined forces against the bill include the: 
 They plan to hit Capitol Hill Wednesday to make the case. 
 The American Medical Association has also spoken out strongly against GOP proposals. 
 We believe that Congress should be working to increase the number of Americans with access to quality, affordable health insurance instead of pursuing policies that have the opposite effect, the group said in a letter Tuesday. 
