__HTTP_STATUS_CODE
200
__TITLE
Women Skip Doctor Visits and Miss Their No. 1 Health Risk
__AUTHORS
Erika Edwards
Maggie Fox
__TIMESTAMP
Jun 22 2017, 8:05 pm ET
__ARTICLE
 Too many American women are missing out on help fighting the single thing most likely to kill them  heart disease, researchers said Thursday. 
 And one surprising reason women skip medical visits completely: theyre hoping to drop a few pounds first. 
 A new survey published in the Journal of the American College of Cardiology found that that 45 percent of women questioned were not aware that heart disease is the No. 1 killer of women. And while most had seen a physician or other medical provider in the past year, just 40 percent said they got a heart health assessment. 
 Of the 1,011 women surveyed, 63 percent admitted they sometimes put off medical visits and 45 percent said they canceled or postponed an appointment because they wanted to lose weight first, the team at Cedars Sinai Heart Institute said. 
 Tracy Solomon Clark of Gardena, California said she had done it in the past. 
 The goal was to lose a few pounds, Solomon Clark, a regular spokesperson for the American Heart Association, told NBC News. 
 And then I would go to the doctor. 
 Related: Why Heart Attacks Are Striking Healthy Young Women 
 It never occurred to her that her weight could be dangerous. Shes had bypass surgery to help her blood get by clogged arteries and stents placed to prop open other clogged blood vessels. 
 I thought this was an old man disease, Solomon Clark said. Surely it wouldnt happen to me. 
 Worse, many doctors dont seem to realize, either. Dr. Noel Bairey Merz of Cedars Sinai in Los Angeles and colleagues talked to 300 physicians and found only 39 percent of the primary care physicians said heart disease would be their top health concern for female patients. 
 The majority of physicians did not feel well-prepared to discuss or to manage heart health in women, Bairey Merz said. 
 Related: Here's How Stress Might Cause Heart Attacks 
 American Heart Association CEO Nancy Brown says its not surprising behavior. 
 Cardiovascular diseases cause one in three deaths among women each year  more than all cancers combined, she said. 
 Eighty percent of heart disease and stroke is preventable, yet womens heart disease is underdiagnosed, under-researched and underfunded, Bairey Merz and colleagues wrote. 
