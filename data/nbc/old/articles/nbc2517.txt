__HTTP_STATUS_CODE
200
__TITLE
Pennsylvania Mom Charged After Overdosing on Heroin While Pregnant
__AUTHORS
Corky Siemaszko
__TIMESTAMP
Jun 29 2017, 2:20 pm ET
__ARTICLE
 A troubled Pennsylvania woman was charged Thursday with aggravated assault on an unborn child after she allegedly overdosed on heroin  while seven months pregnant. 
 Kasey Rose Dischman survived her brush with death and her daughter was delivered via an emergency C-section, according to an arrest affidavit. 
 But the baby is on life support and not expected to make it. 
 We have an infant thats probably not going to survive,  Lt. Eric Hermick of the Pennsylvania State Police told local reporters. 
 Dischman, who is 30 and lives in Butler, overdosed about a week after she was released from the Butler County Prison, where she had been serving a six month sentence for shoplifting, a prison official said. 
 She has been jailed here ten times since 2006, the official said. Most of it is retail thefts stemming from drug use, a couple of burglaries (also from drug use) and possession charges. 
 Dischman was found passed out in the bathroom of her apartment around 3 a.m. on June 23 by her 36-year-old live-in boyfriend Andrew Lucas, police said. 
 Unable to revive her, paramedics rushed Dischman to Butler Memorial Hospital where she went into cardiac arrest in the emergency room, the affidavit states. 
 When she was finally able to talk, Dischman stated that she found a stamp bag of heroin under her couch and injected the heroin in the bathroom of her apartment, according to the papers. Dischman explained that it was her first time using the drug since being released from jail for retail thefts on June 23. 
 Investigators later discovered that Dischman had been given a prescription for the anti-overdose medication Narcan after she was released from prison. 
 "Dischman tested positive for opioids and alcohol," the affidavit states. 
 Lucas, however, told police a different story. 
 Lucas denied any drug activity and related that he did not move anything within the scene or attempt to destroy evidence, the affidavit states. Based on my experience, Lucas appeared to be under the influence of drugs. 
 Lucas is the father of the newborn, the records state. He and Dischman also have a 6-year-old daughter who lives with them. He has not been charged with a crime. 
