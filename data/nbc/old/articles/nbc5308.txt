__HTTP_STATUS_CODE
200
__TITLE
The Week in Pictures: June 9 - 16
__AUTHORS
NBC News
__TIMESTAMP
Jun 16 2017, 1:00 pm ET
__ARTICLE
Catholic girls take part in a Corpus Christi procession in Port-au-Prince, Haiti, on June 15, 2017.
People hold their hands above their heads as they are evacuated by police in the area of a UPS facility after a shooting in San Francisco on June 14.
A UPS driver killed three people and wounded two others before turning his weapon on himself as police approached.
Police maintain a security cordon as a huge fire engulfs the Grenfell Tower early on June 14 in west London. Authorities said Friday that least 30 people were killed in the fire and have warned that the death toll was likely to rise.
PHOTOS: Fire Rages Through London Apartment Tower
A U.S. Army carry team moves the transfer case containing the remains of U.S. Army Cpl. Dillion C. Baldridge at Dover Air Force Base in Delaware on June 12.
Cpl. Baldridge, of Youngsville, North Carolina, was one of three U.S. soldiers killed when an Afghan soldier opened fire in Afghanistan's Nangarhar Province in an apparent insider attack.
Visitors check out the waterfall known as Gooafoss on June 1 near Lake Myvatn, Iceland. Iceland's tourism industry continues to thrive. Just eight years ago, Iceland welcomed approximately 464,000 tourists and by last year nearly 1.7 million people visited the nation.
Photo released on June 12.
Russian police officers carry away a protester at an opposition rally in Moscow on June 12. Thousands of Russians protested against corruption, part of opposition leader Alexei Navalny's long-shot drive to unseat President Vladimir Putin at the ballot box next year.
PHOTOS: Russian Police Arrest Over 1,000 in Corruption Protests
Angel Ayala, left, and Carla Montanez mourn a friend killed in the mass shooting at the Pulse nightclub as people gathered outside the club on June 12, the one-year anniversary of the attack in Orlando, Florida. Omar Mateen killed 49 people and wounded 53 before being killed himself by police in a shootout at the club.
PHOTOS: Mourners Mark First Anniversary of Pulse Massacre
A great white pelican cools off in a pond at a zoo in Dresden, Germany, on June 12. Great white pelicans predominantly breed in sub-Saharan African and northern India.
Members of the Republican and Democratic congressional baseball teams gather for a bipartisan prayer before the start of their annual game at Nationals Park on June 15 in Washington.
House Majority Whip Rep. Steve Scalise is in critical condition following a shooting a day earlier during a practice for the Republican team.
The Week in Pictures: June 2 - 9
