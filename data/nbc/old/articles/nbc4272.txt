__HTTP_STATUS_CODE
200
__TITLE
Fentanyl Crisis: Miami Boy, 10, Believed to Have Died After Exposure to Drug
__AUTHORS
Erik Ortiz
Andrew Kozak
__TIMESTAMP
Jul 18 2017, 4:42 pm ET
__ARTICLE
 A 10-year-old Miami boy who died last month is believed to have had fentanyl in his system  spurring investigators to trace how someone so young could come into contact with a synthetic opioid ravaging communities across the country. 
 Preliminary toxicology tests indicate the boy had the painkiller in his body when he collapsed at home and died, Miami-Dade State Attorney Katherine Fernandez Rundle said Tuesday. The Miami Herald reported about the boys June 23 collapse and death and the possible link to the drug on Monday, and identified him as Alton Banks. 
 "At this point, we have every reason to believe that their preliminary findings are that it was a mixture of heroin and fentanyl that killed this little boy," Fernandez Rundle said at a news conference Tuesday. 
 It's unclear how Alton came across the fentanyl, which has prompted warnings by law enforcement agencies that breathing in, or even touching a small speck of it, can be fatal. 
 Investigators do not believe the boy came into contact with the drug at his home, Fernandez Rundle said. The Miami Herald said the home is in the Overtown neighborhood, which is considered "ground zero" for the city's opioid epidemic. 
 Alton had returned home from a neighborhood pool earlier in the day and was vomiting, according to the newspaper. He was found unconscious later that night and rushed to a hospital where he was pronounced dead. 
 "We believe that it was somewhere between the park or the pool or the sidewalk, or maybe he touched something, Fernandez Rundle said. 
 Darren Caprara, a spokesman for the Miami-Dade County Medical Examiner's Office, said Tuesday that samples in Alton's case were still being tested. 
 "We are aware of the public safety issues in play here and we are doing everything we can to get the results as accurately and quickly as we can," Caprara said. 
 The fifth-grader's death comes about two months after an Ohio case grabbed national headlines when a police officer said he accidentally overdosed on fentanyl following a drug arrest. 
 Patrolman Chris Green of the East Liverpool Police Department said in news reports that he came into contact with fentanyl  which is about five times as strong as heroin  when he searched the car of two suspected drug dealers. 
 East Liverpool Police Chief John Lane said Green was wearing the required gloves and mask to do the search. Afterward, another officer noticed a white powder on Green's shirt, and Green brushed it off with his bare hand. 
 Related: Ohio Cop Accidentally Overdoses During Drug Call 
 "I'm not sure he even realized this was drugs," Lane told NBC News at the time. The police chief said Green had passed out and required four doses of Narcan  a medication used to treat a narcotics overdose  to revive him. 
 Following the incident, the Drug Enforcement Agency in June put out new guidelines for how law enforcement should handle the drug. Deputy Attorney General Rod Rosenstein warned: "The spread of fentanyl means that any encounter a law enforcement officer has with an unidentified white powder could be fatal." 
 But the lone act of Green touching fentanyl may not have set off his illness, some doctors have suggested, and he also may have inhaled the substance. 
 The American College of Medical Toxicology released a statement last week trying to calm jitters. 
 "To date, we have not seen reports of emergency responders developing signs or symptoms consistent with opioid toxicity from incidental contact with opioids," its board wrote. "Incidental dermal absorption is unlikely to cause opioid toxicity." 
 Fernandez Rundle commented about Alton's case this week because of its unusual nature and to solicit tips. 
 Of the more than 850 people in Florida who died from fentanyl or a related strain in the first half of 2016, just nine were under the age of 18, according to the Miami-Dade Medical Examiner's Office. 
 Alton "was out playing, like we want all our children to do," Fernandez Rundle said, according to the Miami Herald. "We're anxiously hoping that someone comes forward to help us solve this horrific death." 
 The boy's mother, Shantell Banks, was informed of the preliminary findings last week. She was too distraught to speak to the Miami Herald in depth, but said her son was a "fun kid" who wanted to become an engineer and loved the Carolina Panthers. 
 The Florida Legislature addressed the epidemic, passing a law that imposes stiff minimum mandatory sentences on dealers caught with 4 grams (0.14 ounces) or more of fentanyl or its variants. The law also makes it possible to charge dealers with murder if they provide a fatal dose of fentanyl or drugs mixed with fentanyl. The new law goes into effect Oct. 1 
