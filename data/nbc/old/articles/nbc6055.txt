__HTTP_STATUS_CODE
200
__TITLE
Appeals Court Strikes Down FAA Registry of Recreational Drones
__AUTHORS
Alex Johnson
__TIMESTAMP
May 23 2017, 8:58 pm ET
__ARTICLE
 Federal rules requiring owners to register recreational drones with the government are illegal, and only Congress can fix them, a federal appeals court has ruled. 
 The Federal Aviation Administration issued rules in October 2015 requiring registration of drones  or "unmanned aerial vehicles" (UAVs) in governmentese  weighing more than a half-pound. The rules took effect in January 2016. 
 The half-pound threshold is light enough to cover many of the small remote-controlled drones you can buy at the department store, and John Taylor of Washington, D.C., sued in February 2016, arguing that another provision of federal law bars the government from regulating model aircraft. 
 In a ruling dated Friday, a three-judge panel of the U.S. Court of Appeals for Washington agreed. 
 "The 2012 FAA Modernization and Reform Act provides that the FAA 'may not promulgate any rule or regulation regarding a model aircraft,' yet the FAA's 2015 Registration Rule is a 'rule or regulation regarding a model aircraft,'" Judge Brett M. Kavanaugh wrote for the court (PDF). 
 "Statutory interpretation does not get much simpler," he wrote. 
 If the FAA wants to regulate drones, the court said, it must persuade Congress to repeal or change the 2012 law. 
 "Perhaps Congress should do so. Perhaps not. In any event, we must follow the statute as written," Kavanaugh wrote. 
 In a statement, the Academy of Model Aeronautics (AMA), an interest group for model airplane enthusiasts, welcomed the decision. 
 "For decades, AMA members have registered their aircraft with AMA and have followed our community-based safety programming," the organization said. "Federal registration shouldn't apply at such a low threshold that it includes toys. It also shouldn't burden those who have operated harmoniously within our communities for decades, and who already comply with AMA's registration system." 
 But the the ruling disappointed the Association for Unmanned Vehicle Systems International. 
 "A UAS [unmanned aircraft systems] registration system is important to promote accountability and responsibility by users of the national airspace, and helps create a culture of safety that deters careless and reckless behavior," it said. 
 Mike Elliott, a drone salesman and service agent for Drone Services Hawaii in Honolulu, told NBC affiliate KHNL on Tuesday that the ruling was already causing confusion, leading owners to call his business for clarification. 
 "Commercial operators will still need to register, as it was before," Elliot said. "Just the hobbyist side right now is not required to register." 
