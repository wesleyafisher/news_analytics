__HTTP_STATUS_CODE
200
__TITLE
South Carolina Man Accused of Enslaving Mentally Disabled Cafeteria Worker
__AUTHORS
Associated Press
__TIMESTAMP
Oct 12 2017, 2:29 pm ET
__ARTICLE
 CONWAY, S.C.  A South Carolina restaurant manager has been charged with abusing and enslaving a mentally challenged employee, according to information released Wednesday by federal authorities. 
 Bobby Paul Edwards, 52, of Conway pleaded not guilty to one count of forced labor, federal prosecutors said. He was ordered held without bail. 
 Edwards used abuse and threats to force 39-year-old John Christopher Smith to work as a J&J Cafeteria cook from 2009 until 2014, authorities said. 
 Court documents describe beatings with a belt, choking, slapping, punching with a closed fist and burning with tongs used in hot grease. 
 Smith has been diagnosed with delayed cognitive development that results in intellectual functioning significantly below average. He filed a federal lawsuit in 2015 against Edwards and the restaurant owner, saying he wasnt paid or given time off or benefits. 
 The lawsuit also accused Edwards of repeated abuse, saying he hit Smith with objects including a frying pan and forced him Smith to work, to the point the man was so weak he had to be carried home. 
 Saying some witnessed the alleged abuse, the lawsuit notes that Edwards went after Smith with a belt buckle for being too slow to replenish food items on the buffet line. 
 Plaintiff was heard crying like a child and yelling, No, Bobby, please! according to the suit, which accused the cafeterias owner of knowing about the alleged abuse but doing nothing to stop it. 
 Edwards attorney didnt respond to outlets requests for comment. State assault charges against him are still pending. 
 Follow NBCBLK on Facebook, Twitter & Instagram 
