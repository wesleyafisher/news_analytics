__HTTP_STATUS_CODE
200
__TITLE
American Troops Have Operated Inside of Mosul, Coalition Confirms
__AUTHORS
Courtney Kube
__TIMESTAMP
Jan 4 2017, 12:58 pm ET
__ARTICLE
 American troops have been operating alongside Iraqi forces inside of ISIS-held Mosul, a coalition spokesperson acknowledged for the first time Wednesday. 
 They have been in the city at different times, Col. John Dorrian said during a teleconference briefing Wednesday morning. 
 The American and coalition troops have been operating as advisers to the Iraqi troops, who are at the forefront of the fighting, and have stayed behind the forward line of troops, Dorrian said. 
 In the past couple of weeks, the number of advisers has roughly doubled to about 450 troops operating with Iraqi command elements, Dorrian said, adding that the increase came in conjunction with the second phase of the battle for Mosul. 
 Related: ISIS Bombs, Mines Pose 'Unprecedented' Threat in Liberated Areas Near Mosul 
 Dorrian described the fight for Iraq's second-largest city as slow going, saying that its going to take some time. 
 ISIS has had more than two years to prepare and fortify their stronghold, and the Iraqis are now faced with clearing more than 200,000 buildings in the city  a process that involves clearing each one from the roof, through every room and closet, and down into the tunnels between the structures. 
 Dorrian described the current fight in the eastern part of the city as extraordinarily dangerous, adding later that once the Iraqis cross the river and enter the west it is going to be a very tough fight there as well. 
