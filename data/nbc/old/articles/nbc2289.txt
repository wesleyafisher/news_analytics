__HTTP_STATUS_CODE
200
__TITLE
Lillian Ross, Legendary Journalist for The New Yorker, Dead at 99
__AUTHORS
Associated Press
__TIMESTAMP
Sep 20 2017, 6:34 pm ET
__ARTICLE
 NEW YORK  Lillian Ross, the ever-watchful New Yorker reporter whose close narrative style defined a memorable and influential 70-year career, including a revealing portrait of Ernest Hemingway, a classic Hollywood expos and a confession to an adulterous affair, has died at age 99. 
 Ross died early Wednesday at Lenox Hill Hospital after suffering a stroke, said Susan Morrison, The New Yorker's articles editor. In an email statement to The Associated Press, New Yorker editor David Remnick called Ross a groundbreaking writer. 
 "Lillian would knock my block off for saying so, she'd find it pretentious, but she really was a pioneer, both as a woman writing at The New Yorker and as a truly innovative artist, someone who helped change and shape non-fiction writing in English," Remnick wrote. 
 Hundreds of Ross' "Talk of the Town" dispatches appeared in The New Yorker, starting in the 1940s, when she and Brendan Gill wrote about Harry Truman's years as a haberdasher, and continuing well into the 21st century, whether covering a book party at the Friars Club or sitting with the daughters of Richard Rodgers and Oscar Hammerstein II as they watched a Broadway revival of "South Pacific." After the death of J.D. Salinger in 2010, Ross wrote a piece about her friendship with the reclusive novelist and former New Yorker contributor. 
 Her methods were as crystallized and instinctive as her writing. She hated tape recorders ("fast, easy and lazy"), trusted first impressions and believed in the "mystical force" that "makes the work seem delightfully easy and natural and supremely enjoyable." 
 "It's sort of like having sex," she once wrote. 
 Ross' approach, later made famous by the New Journalists of the 1960s, used dialogue, scene structure and other techniques associated with fiction writers. She regarded herself as a short story writer who worked with facts, or even as a director, trying to "build scenes into little story-films." In 1999, her 1964 collection of articles, "Reporting," was selected by a panel of experts as one of the 100 best examples of American journalism in the 20th century. The group, assembled by New York University, ranked it No. 66. 
 Short and curly-haired, unimposing and patient, Ross tried her best to let the stories speak for themselves, but at times the writer interrupted. 
 In a 1950 article, Ross joined Hemingway in New York as he drank champagne with Marlene Dietrich, bought a winter coat and visited the Metropolitan Museum of Art, flask in hand. She presented the author as a volatile bulk of bluster and insecurity, speaking in telegraphic shorthand ("You want to go with me to buy coat?") and even punching himself in the stomach to prove his muscle. 
 Ross was friendly with Hemingway  she liked most of her subjects  but her article was criticized, and welcomed, as humanizing a legend. "Lillian Ross wrote a profile of me which I read, in proof, with some horror," Hemingway later recalled. "But since she was a friend of mine and I knew that she was not writing in malice she had a right to make me seem that way if she wished." 
 Not long after, Ross went to Hollywood to report on director John Huston as he worked on an adaptation of Stephen Crane's Civil War novel "The Red Badge of Courage." She soon realized that the movie was more interesting than any one person: She was witness to a disaster. Ross' reports in The New Yorker, released in 1952 as the book "Picture," were an unprecedented chronicle of studio meddling as MGM took control of the film and hacked it to 70 minutes. 
 Praised by Hemingway among others, "Picture" was a direct influence on such future Hollywood authors as John Gregory Dunne ("Studio") and anticipated the nonfiction novel that Truman Capote perfected a decade later with "In Cold Blood." Huston's daughter, the actress Anjelica Huston, became a lifelong friend. 
 Deeply private even around her New Yorker colleagues, Ross did step out in 1998 when she published "Here But Not Here," a surprising and explicit memoir of her long-rumored, 40-year liaison with New Yorker editor William Shawn, a mating of secret souls allegedly consummated in a bedroom that Dietrich once used as a clothes closet. 
 "We were drawn to each other from the first by all the elusive forces that people have been trying to pin down from the beginning of time," Ross wrote. 
 While involved with Shawn, Ross adopted a son, Erik, who in later years would accompany his mother on assignments. Her New Yorker work was compiled in several books, most recently "Reporting Always." 
