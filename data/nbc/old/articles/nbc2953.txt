__HTTP_STATUS_CODE
200
__TITLE
A Better Treatment for Alzheimers: Exercise
__AUTHORS
Maggie Fox
__TIMESTAMP
Aug 4 2015, 3:52 pm ET
__ARTICLE
 Exercise can prevent Alzheimers disease, and now research shows it works as a great therapy, as well. 
 Vigorous exercise not only makes Alzheimers patients feel better, but it makes changes in the brain that could indicate improvements, researchers told the Alzheimers Association International Conference on Thursday. 
 "Regular aerobic exercise could be a fountain of youth for the brain," said Laura Baker of the Wake Forest School of Medicine, who led one of the studies. 
 "Exercise or regular physical activity might play a role in both protecting your brain from Alzheimer's disease and other dementias, and also living better with the disease if you have it," added Maria Carrillo, chief science officer for the Alzheimer's Association. 
"No currently approved medication can rival these effects."
 "It's the first time that we can see that exercise can actually enable one to live better with this disease." 
 Baker worked with 70 patients with mild cognitive impairment, which can lead to Alzheimers, and diabetes, which can raise the risk. 
 Her team assigned them to work out at community facilities, doing either supervised aerobics  usually on a treadmill  or stretching for 45 minutes to an hour, four times a week. 
 Over the six months of the study, the researchers tested verbal recall, decision-making, looked at spinal fluid and blood, and did MRI brain scans. 
 Levels of tau, a protein associated with Alzheimers, fell in those who exercised vigorously, Baker told the conference. Exercisers had better blood flow in the memory and processing centers of their brains and had measurable improvement in attention, planning, and organizing abilities referred to as executive function. 
 "These findings are important because they strongly suggest a potent lifestyle intervention such as aerobic exercise can impact Alzheimer's-related changes in the brain," Baker said in a statement. "No currently approved medication can rival these effects." 
 It fits in with a study that found two years of exercising, eating healthier food and brain training can boost peoples memory function. 
 In a second study, Dr. Steen Hasselbalch of the University of Copenhagen in Denmark and colleagues randomly assigned 200 Alzheimers patients to either do an hour of vigorous exercise three times a week or continue their normal lives for four months. 
 As with Bakers group, they got people up to 70 to 80 percent of maximum heart rate for at least half of each session. Thats what most experts consider vigorous exercise. 
 They used a test of thinking and memory called the Symbol-Digit Modalities test and also checked symptoms of depression, activities of daily living and quality of life. 
 The exercisers had far less anxiety, irritability, and depression than those who didnt work out. Those who exercised the most and the hardest scored significantly better on the Symbol-Digit Modalities test. 
 "While our results need to be verified in larger and more diverse groups, the positive effects of exercise on these symptoms that we saw in our study may prove to be an effective complement or combination with antidementia drugs, Hasselbalch said. 
"Regular aerobic exercise could be a fountain of youth for the brain."
 This calls for further study of multimodal treatment strategies, including lifestyle and drug therapies." 
 More than 5 million Americans have Alzheimers, and this is expected to snowball as the population ages. Theres no cure, and treatments do not work well. Drugs such as Aricept, known also as donezepil, and Namenda can reduce symptoms for a time but they do not alter the course of the disease. 
 Researchers have some hints that drugs can reduce the brain-clogging plaques that are a hallmark of Alzheimers but they have not shown conclusively that the drugs can help symptoms or prevent disease and such drugs are years away from getting onto the market. 
 A third study presented at the conference suggested exercise can reduce symptoms of other types of dementia as well 
 Dr. Teresa Liu-Ambrose of the University of British Columbia in Canada and colleagues got 71 people with vascular dementia caused by "silent" or "mini-strokes" to either exercise or continue normal activities. 
 Those who walked at a moderately brisk pace for an hour, three times a week not only did better on memory and attention tests, but they lost weight and lowered their blood pressure. 
