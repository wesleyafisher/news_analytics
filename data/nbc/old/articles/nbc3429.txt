__HTTP_STATUS_CODE
200
__TITLE
Flint Mayor Rips Trump in Solidarity Letter to San Juan
__AUTHORS
Jon Schuppe
__TIMESTAMP
Oct 5 2017, 4:59 pm ET
__ARTICLE
 The mayor of Flint, Michigan, still enduring a polluted-water crisis, sent a letter of solidarity to the mayor of hurricane-battered San Juan, Puerto Rico, this week in which she commiserated over their treatment by the government and bashed President Donald Trump. 
 "I am utterly disappointed and outraged by the lack of active engagement and support from this administration during the recent visit," Flint Mayor Karen Weaver wrote to San Juan Mayor Carmen Yulin Cruz on Wednesday. 
 Weaver said she felt a "special sisterhood" with Yulin Cruz because both women felt forced to publicly challenge the federal government to do more to help their communities. 
 Weaver was elected in 2015 on a promise to solve her city's water crisis, which dates to 2013, when the city started drawing improperly treated drinking water from the Flint River. After denials from government officials, it was revealed that the new system was leaching lead. The city still uses bottled water and filters as it replaces thousands of tainted service lines, Weaver said. 
 Related: Trump Throws Paper Towels to Hurricane Victims in Puerto Rico 
 Yulin Cruz became the voice of Puerto Rico's crisis last week, when she made an impassioned call for government help days after Hurricane Maria tore apart the island. She criticized dismissive remarks by the head of the Department of Homeland Security and stoked an angry response from Trump, who criticized her for "poor leadership ability" and said she and other leaders "want everything to be done for them." 
 She and Trump met briefly during the president's visit to Puerto Rico on Tuesday, when Trump said Puerto Rico, bankrupt before the storm, had thrown the federal budget "out of whack." 
 "President Trumps phrase, 'They want everything to be done for them,' broke my heart, and the hearts of many Americans," Weaver wrote. 
 She added: "The people need a voice, and as elected officials, we are the voice of the people that we took an oath to serve. I commend you for shouting, screaming, yelling if necessary to get the response the people of San Juan deserve. It is our duty to our citizens. It is our families, our friends, and our neighbors, we are serving." 
 
