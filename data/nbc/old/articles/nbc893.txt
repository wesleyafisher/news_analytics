__HTTP_STATUS_CODE
200
__TITLE
Six Must-Watch Movies at the New York Latino Film Festival
__AUTHORS
Sandra Guzman 
__TIMESTAMP
Oct 12 2017, 12:01 pm ET
__ARTICLE
 The New York Latino Film Festival opened on Wednesday night in New York City for a four-day run with a tightly curated number of films that explore a wide range of Latino themes. Saturday Church, a glorious coming out musical set in the Bronx kicks off the festival and El Sptimo Dia, a tender story of Mexican bike deliverymen in Brooklyn closes it. A highlight of this years festival will be three films by Puerto Rican filmmakers, including the greatly anticipated autobiographical movie chronicling Calle 13 Residentes epic journey around the world. 
 There are so many great films this year covering a wide spectrum of the Latino experience, from a psychological thriller set in the world of undocumented women, to LGBT stories, to explorations of color and race, even Gloria Estefan has a film, said an excited Calixto Chinchilla, founder of the festival, now in its 14th year. 
 Chinchilla is particularly proud that this year the festival will showcase a record number of Puerto Rican films. 
 The resilience of these filmmakers and determination to ensure that we had the films and to want to come despite the horrors of Hurricane Maria has be inspiring, he said. I hope people come out just to support them he said of the hurricane ravaged islanders. 
 Here are the highlights:  
 Saturday Church, which kicks off the festival, is a gorgeous coming out musical based in the Bronx that is a cross between La La Land and Moonlight. The film tells the story of a young gay teen who finds family and community in the gay scene in downtown New York. There are gorgeous images of glorious singing and dancing in an urban landscape featuring black and brown LGBT characters. 
 Most Beautiful Island is a psychological thriller set in world of undocumented women. Directed and written by Ana Asencio, the film is shot on Super 16MM giving it a creepy, voyeuristic feel. It tells the story of one harrowing day of undocumented immigrant Luciana who struggles to makes end meet in a tough city. 
 En el Sptimo Dia, a tender comedy about the Mexican bike deliverymen who deliver food all day. The guys featured in the film didnt want to do much press because they didnt want their families to think they were rich, laughs Chinchilla. We had a hard time with publicity they work so many hours. 
 Puerto Rican filmmakers: 
 Residente is an autobiographical film about the life of Rene Perez Joglar, one half of the alternative rap duo Calle 13. The 90-minute film takes viewers on an epic journey around the world with the outspoken human rights activist. The images and music alone is worth the time. 
 Vico C: La Vida del Filosofo tells the story of reggaeton pioneer Vico C, from his early days as a young rapper, to his battle with drugs, imprisonment and redemption through the church. 
 Angelica is a terrific meditation on race in Puerto Rico written and directed by Marisol Gomez-Moukad. A young fashion designer returns to the island to care of her ill father after a long absence and is forced to reevaluate her relationship with her mother and the island itself. Shown through the lens of a Black Puerto Rican woman the film is searing in its honestly about race and the wounds of racism. 
 Follow NBC Latino on Facebook, Twitter and Instagram.  
