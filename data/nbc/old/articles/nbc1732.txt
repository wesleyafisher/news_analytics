__HTTP_STATUS_CODE
200
__TITLE
The Week in Pictures: April 28 - May 5
__AUTHORS
NBC News
__TIMESTAMP
May 5 2017, 11:30 pm ET
__ARTICLE
A Marine ducks into Marine One's rotor wash at the Wall Street heliport as President Donald Trump arrives in New York on May 4, 2017.
Trump flew to New York to join Australian Prime Minister Malcolm Turnbull aboard the USS Intrepid to commemorate the 75th anniversary of the Battle of the Coral Sea.
Trump hadn't set foot in New York since leaving on Jan. 19 for the inauguration.
People walk on a hill covered with nemophila flowers in full bloom at Hitachi Seaside Park in Hitachinaka, Ibaraki Prefecture on May 3. Visitors to the park can witness an estimated 4.5 million nemophila in bloom until the end of May.

French presidential candidates Marine Le Pen, left, and Emmanuel Macron, far right, prepare for their televised debate moderated by French journalists in La Plaine-Saint-Denis, France on May 3, 2017.
After an election campaign like no other, France is about to have a president like no other: either Le Pen, a far-right populist who could reshape Europe's post-war order and become France's first female leader, or Macron, a brainy upstart who's daring the French to gamble on a startup-style new political construction.
The outcome of Sunday's presidential runoff hinges on the millions of voters repelled by them both, who must make a choice  whether to hand Macron his expected victory, or stay home and risk handing Le Pen a surprise win. That choice will ripple across Europe's open borders, through global financial markets, across the battlefields of Syria and Ukraine and the around the halls of U.N. diplomacy.

An opposition activist clashes with police during protests against Venezuelan President Nicolas Maduro on May Day, in Caracas on May 1.
An opposition demonstrator who was run over by a National Guard vehicle is dragged away by a fellow demonstrator during a protest against Venezuelan President Nicolas Maduro, in Caracas on May 3. Venezuela's angry opposition rallied Wednesday vowing huge street protests against Maduro's plan to rewrite the constitution and accusing him of dodging elections to cling to power despite deadly unrest.
A demonstrator catches fire after the gas tank of a police motorbike exploded during clashes in a protest against Venezuelan President Nicolas Maduro, in Caracas on May 3.
At least 38 deaths have been reported by various sources in five weeks of anti-government protests, though the public prosecutor puts the tally at 37. Another 700 people have been injured.
A man rides a wave in the Pacific Ocean waters of La Pampilla beach in Lima, Peru on March 2. As most Lima residents prepare to sleep, a handful of hardcore surfers descend on the only beach in Peru where they can ride the waves at night.
This photo was released by The Associated Press this week.
French soldiers patrol near the Eiffel Tower as part of the "Sentinelle" security plan in Paris on May 3.
A rescued baby otter at the Adobe Mountain Wildlife Center in Phoenix, Arizona on April 20.
A baby otter was nursed back to health after being rescued by utility workers who found it struggling to get out of a canal on the outskirts of Phoenix. The 4-week-old otter was dehydrated, hungry and infested with fleas when it was discovered. 
The Salt River Project crew took the otter to a wildlife center, where it was fed trout mash mixed with kitten's milk and nursed back to health.
They were working on a road by the Arizona Canal when they noticed the otter that was too small to use steps carved into the bank.
This photo was released by the Arizona Game and Fish Department this week.



A Chinese police officer stands guard during a sandstorm overlooking Tiananmen Square on May 4 in Beijing. Sandstorms are common in northern China during the spring season and are caused when heavy winds from Mongolia in the north brings sand and pollutants that can blanket Chinese cities and cause air quality to deteriorate.
Members of a dance club, with many retirees, dance at Lenin park in downtown Hanoi, Vietnam on May 4.
A Sudanese refugee child with disabilities is massaged at the Doro refugee camp, in Maban, near Bunj, South Sudan, on May 3. Masseuses from the local community, trained by the NGO Jesuit Refugee Service, provide massages to disabled refugee children three times a week to reduce their stress in the camp and improve their mobility.
President Donald Trump turns to House Speaker Paul Ryan as he gathers with Congressional Republicans in the Rose Garden of the White House after the House of Representatives approved the American Healthcare Act, to repeal major parts of Obamacare and replace it with the Republican healthcare plan, in Washington on May 4.
Smoke rises as members of the Iraqi Army clash with ISIS fighters at a front line north west of Mosul, Iraq on May 5.
The front lines in western Mosul have inched forward for months as ISIS fighters have used a claustrophobic battle space and hundreds of thousands of civilians as human shields to slow Iraqi troops.
A protester is arrested as he participates in a May Day demonstration on May 1 in New York. Across the country and world people protested, marched and stayed home from work on the traditional day of workers rights.
Member of the Reyes family hug as they are reunited for three minutes as U.S. Border patrol agents open a single gate to allow families to hug along the Mexico border as part of Children's Day in Mexico, at Border Field State Park in San Diego, California on April 30.
Goldie Hawn and Kurt Russell kiss at a ceremony honoring the couple each with stars on the Hollywood Walk of Fame on May 4 in Los Angeles.
The Rustic Barn is covered in debris after tornadoes flattened homes, uprooted trees and flipped several pickup trucks in Canton, Texas on April 30.
Tornadoes hit several small towns in East Texas, killing four people. The storms cut a path of destruction 35 miles long and 15 miles wide in Van Zandt County.
Parts of the Midwest and the South were recovering Monday after a weekend round of storms, winds, hail and isolated tornadoes killed at least 14 people.
PHOTOS:Residents Survey Tornado-Battered Towns in Texas
Members of Delta Sigma Theta Sorority, Inc. walk up steps at the North Carolina General Assembly in Raleigh, N.C., on May 3. The Sorority visits the legislature annually to voice their concerns to lawmakers. In 1913, the founders of Delta Sigma Theta performed their first public act by participating in the Women's Suffrage March in Washington, D.C.
White House Press Secretary Sean Spicer and Chief of Staff Reince Priebus watch as President Donald Trump presents the U.S. Air Force Academy football team with the Commander-in-Chief trophy in the Rose Garden of the White House in Washington on May 2.
Peace rally participant, Dennis Ojogho, stands for a portrait at the intersection of Florence and Normandie, on the 25th anniversary of the L.A. riots, on April 29 in Los Angeles.
Twenty-five years ago, a jury acquitted four white police officers in the beating of black motorist Rodney King, sparking looting and violence that would turn into one of the deadliest race riots in American history.
On Saturday, hundreds of people marked the anniversary with marches advocating peace and hope.
Two girls wearing traditional Sevillian dresses talk during the "Feria de Abril" (April Fair) in Sevilla on April 30. The fair dates back to 1847 when it was originally organized as a livestock fair but has turned into a week of flamenco dancing, music and bullfighting.
Israeli children play on an army tank displayed by the Israeli military as part of Independence Day celebrations, in Givatayim, near Tel Aviv, Israel on May 2. Israel celebrated 69 years since the modern Jewish state was formed.
