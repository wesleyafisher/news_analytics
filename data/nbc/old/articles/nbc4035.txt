__HTTP_STATUS_CODE
200
__TITLE
Scientists Home in on Eczema-Causing Germs
__AUTHORS
Maggie Fox
__TIMESTAMP
Jul 5 2017, 3:00 pm ET
__ARTICLE
 Researchers trying to figure out how bacteria might cause eczema say theyve found an important new clue: Specific strains of Staphylococcus bacteria seem to cause more severe symptoms. 
 Kids with eczema tended to have larger populations of Staphylococcus aureus living on their skin, while kids without the itchy condition tended to have more of a related bacteria called Staphylococcus epidermidis, a team reported in the journal Science Translational Medicine. 
 And the team at the National Institutes of Health narrowed down the bad bacteria using genetic sequencing. 
 The hope is to figure out if the staph bacteria are causing the eczema or are a side-effect, and whether they worsen the symptoms. Eventually, it might be possible to use the knowledge to develop better treatments. 
 Atopic dermatitis (eczema) is a common inflammatory skin disorder in industrialized countries, affecting 10 to 30 percent of children, Julia Segre of the NIHs National Human Genome Research Institute and colleagues wrote. 
 Patients with atopic dermatitis suffer from chronic, relapsing, intensely itchy, and inflamed skin lesions and have an increased likelihood of developing asthma and/or hay fever. 
 Bacteria are a longtime suspect, and one treatment for eczema is a bleach bath to kill them off. But its not usually a long-term fix. 
 Related: NIH Tests 'Good' Bacteria to Fight Eczema 
 One team at NIH is working on using benign bacteria to crowd out the staph, while another NIH-funded team is trying to use probiotics to see if that restores a healthy skin balance. 
 But its important to figure out just who the bad actors are, and thats what Segres team did. 
 They tested 18 children, 11 of whom had eczema. They scraped off the microbes living on their skin during flare-ups and outside of flare-ups, and sequenced the genomes of everything they found. 
 Staph bacteria were definitely the most variable and seemed most associated with eczema flare-ups, they found. 
 The more severe atopic dermatitis patients were markedly colonized with a single clade of S. aureus during disease flares, they wrote. 
 We found that less severe (eczema) patients were colonized with more methicillin-resistant strains, whereas the more severe (eczema) patients were primarily colonized with methicillin-sensitive strains. 
 Its not clear why. Methicillin-resistant Staphylococcus aureus (MRSA) has become very common across the U.S. 
 But understanding the differences between the various types of bacteria living on healthy skin and itchy skin could lead to ways to prevent allergies, the NIH team hopes. 
 Because eczema develops so early in life, scientists believe that the bacteria that take up residence on a babys skin can influence whether that child develops allergies. Getting rid of bad bacteria may help prevent the development of allergies, researchers believe. 
 Eczema is caused by a combination of genetic, immune and environmental factors. 
 There are many treatments out there now, from simple skin lotions to steroid creams and immune suppressant drugs like tacrolimus. But none of these works for everybody, and the immune suppressant drugs can raise the risk of cancers such as lymphoma. 
 The latest approved treatment, Dupixent, is a twice-a-month injection that will cost $37,000 a year. 
