__HTTP_STATUS_CODE
200
__TITLE
Two Nevada Gun Shops Say Stephen Paddock Passed Background Checks
__AUTHORS
Tracy Connor
Ken Dilanian
__TIMESTAMP
Oct 3 2017, 6:58 am ET
__ARTICLE
 Two Nevada gun shops confirmed Monday that they sold firearms to Mandalay Bay shooter Stephen Paddock in the last year and said he passed all required background checks. 
 It was unknown if the weapons Paddock bought from the gun shops, New Frontier Armory in North Las Vegas and Guns and Guitars in Mesquite, were used in the casino massacre. 
 Police said they found 19 firearms, along with explosives and thousands of bullets, at Paddock's home in Mesquite. Another 23 guns were found in his hotel room, officials said. 
 David Familglietti of New Frontier said that Paddock purchased a rifle and a shotgun in the spring and that agents from the Bureau of Alcohol, Tobacco, Firearms and Explosives interviewed the employee who handled the sale. 
 "The rifle was not fully automatic, and a shotgun isn't capable of shooting from where he was," Famiglietti told NBC News when asked whether it was possible that the guns were used in the mass shooting. 
 "He's only shopped there once, so it wasn't someone we knew personally," he said. 
 Related: What We Know About the Las Vegas Victims So Far 
 "We're very sad about the news of this tragedy. We're in the business of selling firearms legally and took all precautions on this sale, as we do with all sales. My staff takes their job very seriously, and if there were any 'red flags,' the sale would have halted immediately." 
 He added, "All state and federal laws were followed, and an FBI background check took place and was passed by the buyer." 
 Christopher Sullivan, general manager of Guns & Guitars, did not say what weapons Paddock, who lived in Mesquite, had bought. 
 "All necessary background checks and procedures were followed, as required by local, state, and federal law. He never gave any indication or reason to believe he was unstable or unfit at any time," Sullivan said in a statement. 
 "We are currently cooperating with the ongoing investigation by local and federal law enforcement in any way we can," he said, adding, "We mourn for this tragedy and our thoughts and prayers are with the families of the lost and injured." 
 At least three other gun shops and ranges in the Las Vegas area said they had been in contact with law enforcement about Paddock but declined to discuss any purchases or visits he made. 
 Related: One Minute Vegas Was Rocking. The Next, It Was 'World War III' 
 Paddock's younger brother, Eric, said he never knew Stephen to have a fascination with guns and thought he owned just a couple of handguns that he kept in a safe. 
 "Do I believe that he should have been able to get a machine gun?" Eric asked NBC News. "Let me loose on whoever sold him the machine gun. He should not have a machine gun." 
 Authorities have not said whether the gunman used a machine gun  a fully automatic weapon  to shoot scores of people within a few minutes. 
 The suspect had two "bump-stocks" that could have turned semi-automatic weapon into a fully automatic one, the Associated Press reported, citing two officials familiar with the investigation. 
 Bump stocks are a modification for legal semi-automatic rifles, and they are widely for sale. The Alcohol, Tobacco, Firearms and Explosives agency has concluded that because they use the rifles recoil to allow the shooter to rapidly depress the trigger, they do not constitute an illegal modification to create a machine gun. 
 Investigators are seeking to determine whether Paddock used the bump stocks to modify weapons used in the mass shooting, the Associated Press reported. 
 Experts who have listened to recordings of the long bursts of gunfire that erupted inside the Mandalay Bay said there was a strong possibility that the gunman used some sort of modification. In addition, police said some of the weapons they seized at his home had been modified. 
 Authorities have not said where the weapons were obtained. One possible reason for the delay: The gun-tracing system in the United States relies on paper records and even microfilm. 
 Even though buyers must fill out forms with gun dealers, those records are not fully searchable by computer. Often, investigators tracking a serial number have to start with the manufacturer and manually trace the gun's history before they arrive at the most current purchase. 
