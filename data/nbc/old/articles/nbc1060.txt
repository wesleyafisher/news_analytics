__HTTP_STATUS_CODE
200
__TITLE
Netflix to Plow Billions More Into Original Content
__AUTHORS
Claire Atkinson
__TIMESTAMP
Oct 16 2017, 9:25 pm ET
__ARTICLE
 Netflix said Monday that it would spend as much as $8 billion on content next year to continue growing its giant subscriber base and outpace its competitors. 
 Last year, when Netflix predicted that it would spend $6 billion on content in 2017, only ESPN was spending more  $7.3 billion, according to a report from Boston Consulting Group and SNL Kagan. 
 Netflix, based in Los Gatos, Calif., reported its third-quarter earnings on Monday evening, saying it added 4.45 million subscribers overseas and 850,000 in the United States during the three months that ended Sept. 30, for a total of 109.3 million customers around the world. 
 The company is facing new competition in the video space, however, and not just as traditional TV networks launch their own apps. Disney is taking its content from Netflix to its own direct-to-consumer video service, and a growing number of other technology companies are hustling into the space with their checkbooks. 
 Facebook, Apple and Amazon have also demonstrated that they want to get into the streaming business in a big way. Apple said it would spend up to $1 billion on content, while Facebook said much the same thing and launched a "Watch" tab on its homepage. Facebook is reportedly a contender for the English Premiere League, while Amazon airs Thursday night NFL games. 
 Netflix's announcement comes the same day that Samsung said it would also begin investing in online TV. 
 In a letter to shareholders it released Monday night, Netflix said it had $17 billion in content commitments over the next few years. The company has signed deals not just to buy content but also to sign top-shelf creators who would come up with the next round of must-see shows. (In August, it lured "Grey's Anatomy" creator Shonda Rhimes from Disney with a development deal.) 
 The company's original series "Narcos," a drama about the drug trade in Colombia, was the most popular digital original program on any subscription video service in the United States during the week of its third-season launch on Sept. 1, according to the digital measurement firm Parrot Analytics. 
 "Our future largely lies in exclusive original content that drives both excitement around Netflix and enormous viewing satisfaction for our global membership and its wide variety of tastes," the investor letter said. It said investment in Netflix originals is more than a quarter of its total content budget in 2017 and "will continue to grow." 
 Netflix said this month that it plans to raise its $9.99-a-month two-stream high-definition plan for new subscribers by $1. 
 CORRECTION (Oct. 17, 8 a.m.): An earlier version of this article misspelled where the Netflix drama "Narcos" takes place. It is Colombia, not Columbia.  
