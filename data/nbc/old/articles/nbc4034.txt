__HTTP_STATUS_CODE
200
__TITLE
Sexual Bullying May Lie Deep in Our DNA, Baboon Study Shows
__AUTHORS
Maggie Fox
__TIMESTAMP
Jul 7 2017, 9:15 pm ET
__ARTICLE
 Male baboons are mean. 
 They beat up on females for no apparent reason, biting them hard enough to make them bleed and harassing them in general. 
 And this bullying seems to pay off. The aggressive male baboons were more likely to mate with their victims when they became fertile, researchers reported Thursday. 
 They say their findings add to growing evidence that sexual aggression may be hard-wired into our genes. It's only one small piece of the puzzle, but the researchers say understanding the roots of such behavior can help explain why it's so common among people. 
 "Sexual violence occurring in the context of long-term heterosexual relationships, such as sexual intimidation, is widespread across human populations," the team wrote in their report, published in the journal Current Biology. 
 But do people do this because we are unique? Is it a product of our emotions or morals, or perhaps a result of how children are raised? Or does it come from deep in the DNA? 
 Thursday's study adds a little evidence to the idea that human sexual violence lies in our genes. 
 That's because humans and baboons are related, coming from a common ancestor that lived tens of millions of years ago. 
 A different study, published in the same journal in 2014, found that sexually aggressive chimpanzee males were not only more likely to win mating rights, but to actually father offspring  presumably spreading their sexually aggressive DNA even more broadly. 
 Very little work had been done with more distantly related primates, so Alice Baniel of the Institute for Advanced Study in Toulouse and colleagues studied two troupes of baboons living in Namibia. 
 They observed a lot of male aggression. 
 Fertile females came in for a lot more of it than pregnant females or new mothers did. Attacks by males were a major source of injury for fertile female baboons, they reported. 
 "When I was in the field and observing the baboons, I often noticed that males were directing unprovoked attacks or chases toward females in estrus," Baniel said in a statement. 
 "They also maintained close proximity and formed a strong social bond with one particular cycling female, from the beginning of their cycle until the end." This type of behavior is called mate-guarding. 
 Related: Uber Investigates Sexual Harassment 
 The bullying paid off. A male who chased and beat up on a certain female over time was more likely to mate with her when she entered a fertile phase, the team found. 
 They called it "a form of long-term sexual intimidation." 
 It was not overt rape. 
 "We found no support for short-term sexual harassment: the probability of copulation did not increase in the five to 20 minutes following male aggression for either unguarded or mate-guarded females," they wrote. 
 Why does it matter? If such behavior is found in humanity's closest relatives  the chimpanzees  and other social primates, it suggests roots deep in evolutionary history, as opposed to behavior that has arisen recently. 
 "Because sexual intimidation  where aggression and matings are not clustered in time  is discreet, it may easily go unnoticed," Baniel said. 
 Related: Woman Catcalled 100 Times in Creepy Video 
 "It may therefore be more common than previously appreciated in mammalian societies, and constrain female sexuality even in some species where they seem to enjoy relative freedom." 
 There's another factor  size. The behavior may be more common in species whose males are markedly bigger than females, such as chimpanzees, baboons and humans. 
 Humanity's other close relative, the bonobo, doesn't have this sexual size difference, and bonobos are notoriously egalitarian when it comes to sex. 
 "This study adds to growing evidence that males use coercive tactics to constrain female mating decisions in promiscuous primates," Baniel said. 
 "Such behavior, previously reported only in chimpanzees, may therefore occur in a wider range of primates, strengthening the case for an evolutionary origin of human sexual intimidation," Baniel's team concluded. 
 Baniel plans to study the two troupes further. She's hoping at least some of the females stand up for better relationships. 
 "I would like to understand if several mating strategies could coexist among males, i.e., being chosen by females versus intimidating them," she said. 
