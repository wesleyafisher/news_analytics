__HTTP_STATUS_CODE
200
__TITLE
More Americans Killed by Guns Since 1968 Than in All U.S. Wars  Combined
__AUTHORS
Chelsea Bailey
__TIMESTAMP
Oct 4 2017, 4:12 pm ET
__ARTICLE
 More Americans have died from gunshots in the last 50 years than in all of the wars in American history. 
 Since 1968, more than 1.5 million Americans have died in gun-related incidents, according to data from the U.S. Centers for Disease Control and Prevention. By comparison, approximately 1.2 million service members have been killed in every war in U.S. history, according to estimates from the Department of Veterans Affairs and iCasualties.org, a website that maintains an ongoing database of casualties from the wars in Iraq and Afghanistan. 
 Sunday's massacre in Las Vegas  which left 59 dead and 530 others injured  is the deadliest mass shooting in modern American history. And such attacks are becoming more common. 
 Related: Trump: Las Vegas Shooter 'Sick' and 'Demented Man' 
 "What we've seen in Las Vegas is an uniquely American scene," former FBI agent Ali Soufan said on MSNBC. "The aftermath of such traumatic events have become an all too familiar scene in our society and in our politics, unfortunately." 
 The June 2016 shooting at Pulse Nightclub in Orlando, Florida, which killed 49 people, is now the second deadliest attack, followed by the 2012 massacre at Sandy Hook Elementary in Newtown, Connecticut, which killed 26 people, most of them children. 
 Related: Silencers, armor-piercing bullets: Congress looks to roll back gun laws 
 After the Sandy Hook shooting, a tearful President Barack Obama announced a series of executive actions aimed at curbing violence in America. The orders included a measure to overturn a 20-year-old amendment that prevented the CDC from conducting federally funded research into how gun violence affects Americans. 
 But despite the elimination of the ban, the agency has remained hesitant to comprehensively research one of the most divisive issues in America. Many activists blame the influence that the NRA and other powerful pro-gun lobbies seem to have on some members of Congress, and the agency's fear that funding could be reduced or revoked. 
 Related: The War in Afghanistan: By the Numbers 
 As President Trump departed Tuesday for a visit to Puerto Rico, he ignored shouted questions on whether the Las Vegas shooting should prompt stricter gun control laws. But he did have harsh words for the suspected gunman, Stephen Paddock. 
 "He's a sick man, a demented man, lot of problems, I guess," Trump said. "We're dealing with a very, very sick individual." 
