__HTTP_STATUS_CODE
200
__TITLE
Ex-NFL Star Aaron Hernandez Had Brain Damage, Doctors Say
__AUTHORS
Maggie Fox
__TIMESTAMP
Sep 22 2017, 8:44 am ET
__ARTICLE
 Aaron Hernandez, the former NFL star convicted of murder who hanged himself in his prison cell in April, had a damaging brain disease linked with repeated concussions, doctors said Thursday. 
 An examination by Dr. Ann McKee of the Boston University School of Medicine showed that Hernandez had chronic traumatic encephalopathy, or CTE, which can cause memory loss, impaired judgment and, sometimes, violent behavior. 
 Based on characteristic neuropathological findings, Dr. McKee concluded that Mr. Hernandez had chronic traumatic encephalopathy (CTE), Stage 3 out of 4, (Stage 4 being the most severe), Boston University's CTE Center, where McKee is the director, said in a statement. It added that a second neuropathologist confirmed the diagnosis. 
 Related: Judge Throws Out Hernandez Murder Conviction 
 Hernandez, a former tight end for the New England Patriots, also had early brain atrophy and other damage, the CTE Center's statement said. 
 McKees research has shown that CTE, found in scores of former NFL players, could explain seemingly inexplicable behavior, the university said. 
 Her research has demonstrated that CTE is associated with aggressiveness, explosiveness, impulsivity, depression, memory loss and other cognitive changes, it said. 
 Related: Hernandez's Brain Held by Medical Examiner 
 After Hernandez, 27, was found dead in his cell, friends and associates said he had shown no evidence of suicidal intentions. He was serving a life sentence for the 2013 murder of his friend, semipro football player Odin Lloyd. Hernandez had just been acquitted in a separate double slaying. 
 This summer, McKees team studied the brains of 202 former football players after they died and found CTE in 177 of them. The NFL has said little about the matter. 
 Boston University said it would have no further statement on its findings. 
