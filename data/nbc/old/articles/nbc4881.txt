__HTTP_STATUS_CODE
200
__TITLE
New Obesity Gene Identified for Some People of African Descent
__AUTHORS
Maggie Fox
__TIMESTAMP
Mar 14 2017, 11:13 am ET
__ARTICLE
 Researchers have found a genetic variation unique to some people of African descent that can add about six pounds of extra weight. 
 About 1 percent of people of African heritage carry the genetic variation, the team at the National Institutes of Health reported. 
 And those who have it are more likely to be obese: more than 55 percent of people with the mutation were obese, compared to 23 percent of those who dont have it. 
 People of purely European descent dont have this variation, the team reported in the journal Obesity. 
 By studying people of West Africa, the ancestral home of most African-Americans, and replicating our results in a large group of African-Americans, we are providing new insights into biological pathways for obesity that have not been previously explored, said Ayo Doumatey of the National Human Genome Research Institute, part of the NIH. 
 Related: Why Latino Genes Make it Hard to Fight Obesity 
 The team started with 1,500 people taking part in a diabetes study in Nigeria, Ghana, and Kenya. They did whats called a genome-wide association study, sequencing all their DNA. 
 They found the mutation in a gene called SEMA4D. Its associated with inflammation and while most people in the world dont have mutations in the gene, 1 percent of the West Africans did. 
 Related: Do These Genes Make Me Look Fat? 
 We replicated this finding in 1,411 West African samples and 9,020 African American samples, they wrote. 
 Studies looking for obesity genes in other countries hadnt found this mutation, probably because they hardly ever include many African-American or African volunteers. Many genes have been found that affect obesity. They vary from person to person and vary in their influence on weight or on diseases associated with obesity, such as diabetes. 
 Related: America's Obesity Epidemic Hits New High 
 We wanted to close this unacceptable gap in genomics research, said NHGRIs Charles Rotimi, who led the work. 
 Its important: More than two-thirds of Americans are overweight or obese but rates are even higher among African-Americans. Nearly 48 percent of African-Americans are overweight or obese. 
 Researchers know genetics play a large role in this. 
 Eventually, we hope to learn how to better prevent or treat obesity, Rotimi said. 
