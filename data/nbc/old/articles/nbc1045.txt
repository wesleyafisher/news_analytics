__HTTP_STATUS_CODE
200
__TITLE
Somalia Bomb Attack Killed Two U.S. Citizens, State Department Says
__AUTHORS
Dan Corey
__TIMESTAMP
Oct 17 2017, 5:23 pm ET
__ARTICLE
 At least two U.S. citizens were among the 276 people killed in a huge truck bomb blast last weekend in Mogadishu, Somalia's capital, the State Department said Tuesday.  
 It is believed to be the single deadliest attack ever in the Horn of Africa nation. At least 300 people were wounded in the blast Saturday, which occurred on a busy street near key ministries. 
 "We want to extend our deepest condolences to all Somalis, especially those who lost their friends and family in the senseless and barbaric attacks, including at least two U.S. citizens who were killed," State Department spokeswoman Heather Nauert said. "We further wish for a speedy recovery for all of those who were injured." 
 A U.S. military plane carrying medical and other supplies landed in Mogadishu on Tuesday, the State Department said. The supplies were being distributed to Somali hospitals and trauma centers responding to the attack. 
 The U.S. Agency for International Development's Office of Foreign Disaster Assistance also arranged for the delivery of emergency medical supplies, along with the deployment of emergency medical teams, the State Department said. 
 Photos: Somalis Search for Survivors After Mogadishu Truck Bomb Blast 
 The United States has previously condemned the explosion, saying: "Such cowardly attacks reinvigorate the commitment of the United States to assist our Somali and African Union partners to combat the scourge of terrorism." 
 Somali President Mohamed Abdullahi Mohamed declared three days of mourning and joined thousands who responded to hospitals' desperate pleas by donating blood. 
 The government called the attack a "national disaster" and blamed it on al-Shabab, an al Qaeda-linked extremist group that frequently targets high-profile areas with bombings. The group hasn't commented. 
 Related: America's Military Role in Africa Is Growing 
 "They don't care about the lives of Somali people, mothers, fathers and children," Prime Minister Hassan Ali Khaire said. "They have targeted the most populated area in Mogadishu, killing only civilians." 
 The U.S. military increased drone strikes and similar efforts earlier this year to combat al-Shabab. The extremist group has also been battling the Somali military and more than 20,000 African Union forces within Somalia. 
 The truck bomb went off two days before the head of U.S. Africa Command was in Mogadishu to meet Mohamed. It was also two days after Somalia's defense minister and army chief resigned for undisclosed reasons. 
