__HTTP_STATUS_CODE
200
__TITLE
Disrupting the Cow: This Plant-Based Burger Smells, Tastes, and Bleeds Like the Real Thing
__AUTHORS
Chiara Sottile
Jo Ling Kent
__TIMESTAMP
Jul 8 2017, 1:13 pm ET
__ARTICLE
 Pat Brown wants every piece of meat consumed in the world to be made entirely from plants. Hes going after the carnivores  and the meat industry that serves them red, bloody, marbled meat. 
 The scientist and chief executive behind the plant-based burger from Impossible Foods, Brown doesnt particularly care that vegetarians and vegans rave about its flavor. Hes on a mission to recreate the texture, smell, and flavor of meat that carnivores crave  while cutting down on the waste in meat production. 
 These yeast are making a protein thats naturally made by soybean roots, said Brown, motioning to strains of yeast in various tubes. You can see, maybe, some of them are redder than others, he pointed out to NBCs Jo Ling Kent on a recent tour of the Impossible Foods Silicon Valley lab and test kitchen. 
 That protein is heme, and its what Brown calls the magic ingredient in his plant-based burger. The burgers other ingredients are wheat, potatoes, coconut oil, some soy, xantham gum, amino acids, and a few vitamins. 
 Combine those ingredients with heme  made from genetically modified yeast  and, in Browns words, Voil, a burger. 
 The magic ingredient not only gives the burger a reddish color, but also is responsible for its meaty flavor. 
 The flavor of a meaty burger is actually a reaction in our mouth with the fats naturally present in our saliva that produces aroma molecules that smell and taste like blood. Impossible Foods has engineered heme to catalyze that chemical reaction, so that diners biting into this plant-based burger perceive the same bloody flavor. 
 The burger currently costs about the same as ground organic beef, and is not available in supermarkets. But the company plans to expand production as fast as the science can keep up  starting with bringing the burger to more restaurants across the country. 
 During NBC's visit, Brown and his lab staff were working to optimize strains of yeast to produce insane amounts of heme  all part of Impossibles vast expansion plans. 
 The Impossible Burger debuted in a few restaurants in the summer of 2016, and recently broadened into the Umami and Hopdoddy restaurant chains  bringing the total number of locations to 33. 
 The company has also just unveiled a more than 66,000-square-foot production and distribution facility in Oakland, California. 
 Were scaling up rapidly," Brown told NBC News. "Were going to increase our production from thousands of pounds [of plant-based meat] a month to more than a million pounds a month by the end of the year. Were not gonna stop until basically weve accomplished that goal. 
 In the Impossible Foods lab and test kitchen, Brown and his team of 160 are dedicated to ensuring that scaling up their product doesnt harm its quality  which means ongoing testing. Lab staff in white coats, masks and goggles conduct blind experiments into how the meat forms in ones hands as compared to "real" beef. 
 But, Brown says, beef is only the beginning. The company seeks to replace all animals, in addition to fish and even dairy products, with Impossibles methods. Its food scientists can already create chicken, pork, fish and yogurt entirely from plants. 
 Brown says the Impossible Burger requires just 5 percent of the land needed for meat from a cow, produces 12.5 percent of the greenhouse gases, and uses only one-quarter of the water. 
 Right now, the use of animals as a food production technology occupies about half of every square inch of land on earth, said Brown in his companys test kitchen. It uses and pollutes more water than any other industry. 
 But Brown knows that people love their meat. 
 According to the OECD-FAO Agricultural Outlook, the global consumption of meat has spiked 21 percent over the last two decades, to 68 million tons a year. 
 "For billions of people around the world, meat is not only an important source of nutrition, its an essential part of the pleasure of life, said Brown. Its completely unrealistic to think that we can ask them to give it up. 
 So instead of asking meat lovers to abandon their food of choice, Brown was determined to figure out a better way to make meat that will please their palettes. In short, to use a Silicon Valley term, he is disrupting the cow. 
 You think that the best possible way to make a food that delivers what consumers value in meat is to, you know, grow up a cow and kill it and chop it into bits. But in fact, when you think about it in terms of flavor, affordability, and nutrition, there are much better ways to make it, said Brown, walking past an instrument that analyzes the aromas of individual molecules. 
 Celebrity Chef Chris Cosentino, known for his whole animal cooking and love of meat, has been serving the Impossible Burger at his San Francisco restaurant, Cockscomb, for more than six months. 
 I never thought anything like that would exist in my lifetime, said Cosentino, who serves the burger for huge lunchtime crowds. 
 Take your fingers and rub em together, and you feel the fat on it. Right? said an enthusiastic Cosentino in his Cockscomb kitchen, admiring the meatless burger patty he just formed. It has the feeling texturally of ground beef. And it has that fat content on your fingers of ground beef. 
 Cosentino is emphatic that he serves the burger because its the right thing to do for the planet  his kids and his grandkids included  but also because its the closest vegetarian option to beef hes ever tasted. 
 Im 100 percent adamant about only serving what tastes great, says Cosentino, grilling the burger with a satisfying sizzle and adding lettuce, Gruyre cheese, caramelized onions and pickles. 
 Im not gonna serve my customers a gimmick, he says, inserting an IMPOSSIBLE flag on a toothpick into the bun as the last touch. 
 During this lunch service, most Impossible burger-eaters at Cockscomb said they could taste the difference between the plant-based burger and its meaty counterpart, but said the taste is so close, they were happy to eat the more environmentally friendly option. 
 The excitement from the customer base, its just been nonstop. [They] want more, want more, want more, said Cosentino, noting many patrons will come to his restaurant especially for the burger. 
 Brown says his company is ready to keep up with demand  as well as the company's ambitious expansion goals. Asked about his master plan, he replied: Make a better product, let the market operate, and lets see who wins. 
