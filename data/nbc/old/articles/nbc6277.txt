__HTTP_STATUS_CODE
200
__TITLE
Las Vegas Businesses Step Up to Help Shooting Victims
__AUTHORS
Ben Popken
__TIMESTAMP
Oct 3 2017, 4:16 pm ET
__ARTICLE
 After Sunday's mass shooting, businesses in Las Vegas are stepping up to offer meals and accommodations to victims, their friends and family, and first responders  all on the house. 
 MGM, the Palms and many other hotels and casinos are offering free rooms for victims, with some extending the offer to friends and family who have traveled to Las Vegas to help; Uber and Lyft are providing free rides to and from nearby hospitals and blood donation centers; daycare facilities are giving their employees time off and giving their teachers training on how to talk to children about the tragedy; and wily locals have even banded together to figure out ways to help the victims. 
 Vegas food writer and stand-up comedian Jason Harris is just one local who has rallied businesses to help out: His informal food program, The First Food Responders, delivers food and meals from over a dozen different top-notch restaurants to victims, hospitals and first responders. Harris estimates theyre doing about 1,000 free meals a day. 
 Harris was on the famed Strip when the shooting started  and two of his friends were hit. As the numbness and shock faded he started looking for a way to help. 
 Photos Capture Chaos of Concert Massacre 
 Everyone is just using their strengths; my strengths are I know a lot of these chefs and restaurants and I just started to put it together, Harris told NBC News. He gets a call on his cellphone of another hungry place and taps into his network to order up hot meals and sandwiches and get them delivered. 
 If X group needs Y food at what hour, hes there, Harris said. His group has coordinated donations from Aureole, Delmonico Steakhouse, Metro Pizza, Momofuku and more, along with nearly the entire local food truck community to bring food and lift spirits. 
 Related: A Surreal Air Settles on the Las Vegas Strip 
 The famously hospitable Vegas entertainment industry is also assisting, in its own way. The Nevada brothel Moonlite BunnyRanch has pledged to keep its doors open. Owner Dennis Hof said in a statement, The working girls will be there to lend compassion and tenderness to the men and women that need it during this sorrowful time. 
 For those in more need of traditional grief management, several therapy and crisis centers said they would be offering free mental and financial counseling to those affected. 
