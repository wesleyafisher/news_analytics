__HTTP_STATUS_CODE
200
__TITLE
U.S. Fears New Threat From ISIS Drones
__AUTHORS
Robert Windrem
__TIMESTAMP
May 24 2017, 3:16 pm ET
__ARTICLE
 U.S. officials are increasingly concerned that ISIS may use drones in terrorist attacks, according to congressional testimony on Tuesday. 
 Marine Corps Lt. Gen. Vincent R. Stewart, director of the Defense Intelligence Agency, told the Senate Armed Services Committee that the terrorist group has begun using "unmanned aerial vehicles," aka drones, for both surveillance and attacks. 
 "In the past year, ISIS's use of unmanned aerial systems (drones) for surveillance and delivery of explosives has increased, posing a new threat to civilian infrastructure and military installations." Stewart testified. 
 Although the group has used drones on the battlefield in Iraq and Syria, Stewarts comments came during a discussion of the worldwide threat posed by ISIS and how it may expand, and soon after the terror group released a video highlighting its use of drones. 
 Related: Director of National Intelligence Reveals ISIS Attack Plan 
 Stewart did not disclose any technical details about ISIS drones, but another U.S. intelligence official said the group has been using off-the-shelf products and even hobbyist aircraft configured with cameras and makeshift bombs. 
 The official described the ISIS drones as "Frankenstein concoctions" tasked with "surveillance, dropping ordnance and interfering with adversarys aerial assets," like helicopters. 
 Just last week, ISIS released a 44-minute video largely focused on the battle for Mosul that includes copious drone footage. The theme of the video is ISIS innovation, with drones the most prominent example of how ISIS research and development continues to thrive despite admitted losses of territory in places like Mosul. 
 Drones watch from above as ISIS suicide bombers driving explosive-laden, heavily armored trucks carry out a dozen attacks on coalition armor. The drones follow the armored trucks, or suicide tanks, as they sidle up to moving tanks and detonate, or burst through security cordons at military depots and blow up multiple vehicles. In one snippet, a drone is also shown diving into a target. 
 Shawn Henry, co-founder of the cybersecurity firm Crowdstrike and a former FBI official, says the drone footage itself also serves a military purpose. Packaged in a slickly produced propaganda video, it dazzles would-be jihadis. 
 "Its helpful in recruiting, its helpful in radicalizing, its helpful in fund-raising," said Henry, an NBC News counterterrorism analyst. "Their attacks are all in part to do those sorts of things." 
