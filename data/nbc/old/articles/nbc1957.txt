__HTTP_STATUS_CODE
200
__TITLE
Linda Zevallos, Who Once Dated Robert Durst, Sensed Something Was Wrong
__AUTHORS
__TIMESTAMP
Mar 24 2015, 8:52 pm ET
__ARTICLE
 A Dallas woman who once dated millionaire murder suspect Robert Durst said she ended the six-month courtship in 2000 after "sensing something was wrong," she told NBC News. 
 Linda Walker Zevallos said her dates with Durst had already stopped by the time one of his close friends, Susan Berman, was found fatally shot in her Beverly Hills, California, home on Christmas Eve of that year. Durst was charged with first-degree murder this month in Berman's death, and remains jailed in Louisiana awaiting extradition. 
 Zevallos said she met Durst in April 2000 on a flight from New York City, where his family runs a real estate empire, to Dallas. He called her often, she said, and he would sometimes swoop in and take her out for fancy meals. He even met her then-13-year-old son. But, she said, Durst never divulged his past involving the unsolved disappearance of his first wife in 1982. 
 "(Durst) was nice when I was with him, pretty much," Zevallos said. "But if he didnt get his way, then he was a little mean. But then he'd turn around and apologize later." 
 After one particular date, Zevallos said, he sent her a handwritten letter. "Good morning. Last night was fun and special and I will not forget it. I kinda expected the burglar alarm to go off while we were peeping in your new house," the letter says, ending with, "Your friend, Bob." 
 The letter was written in green ink and included Durst's Wall Street mailing address in New York City  the same address found on a letter that Durst sent to Berman and was featured in the HBO documentary series, "The Jinx." Zevallos said she has since shared the letter with Los Angeles police. 
 Even after cutting off contact, Zevallos claims that Durst would continue to call her. He went on to marry Debrah Lee Charatan, a New York real estate broker, in December 2000. Three years later, Durst would stand trial for  and be acquitted of  murdering a neighbor in Galveston, Texas, adding another layer to the strange saga. 
 "It's shocking that I knew someone like that and I was involved with somebody like that, and I brought him in and met my children," Zevallos said. "It's shocking." 
