__HTTP_STATUS_CODE
200
__TITLE
Bodies of Civilians Dumped Near Marawi, Philippines After ISIS Clashes
__AUTHORS
Reuters
__TIMESTAMP
May 28 2017, 4:30 am ET
__ARTICLE
 MARAWI CITY, Philippines - Bodies of what appeared to be executed civilians were found in a ravine outside a besieged Philippines city Sunday as a six-day occupation by ISIS-linked rebels took a more sinister turn. 
 The eight dead men, most of them shot in the head and some with hands tied behind their backs, were laborers who were stopped by the militants on the outskirts of Marawi City while trying to flee clashes, according to police. 
 Nine spent bullet casings were found on a blood-stained patch of road at the top of the ravine. Attached to one of the bodies was a sign that said "Munafik" (traitor). 
 The discovery confirms days of speculation that Maute rebels had killed civilians during a bloody takeover of Marawi City, that the military believes is aimed at winning the Maute recognition from ISIS as a Southeast Asian affiliate. 
 The army deployed additional ground troops over the weekend and dispatched helicopters to carry out rocket strikes on Maute positions as fighters held buildings and a bridge deep inside a predominantly Muslim city where few civilians remained. 
 At least 41 militants were killed and 13 military as of Saturday, according to the army. The number of civilian dead was unknown. 
 The fierce resistance of the Maute gunmen and the apparent executions of civilians will add to growing fears that subscribers to ISIS radical ideology are determined to establish a presence in the southern Philippines, with the support of extremists from Indonesia and Malaysia. 
 Fierce battles restarted on Sunday as ground troops engaged Maute fighters with heavy gunfire. Plumes of smoke were seen on the horizon and helicopters fired at least eight rockets on rebel positions. 
 Tens of thousands of people have fled Marawi since Tuesday, when militants went on the rampage seizing a school, a hospital, and a cathedral. 
 Christians were taken hostage, according to church leaders, and more than 100 inmates, among them militants, were freed when rebels took over two jails. 
 Zia Alonto Adiong, a local politician who is coordinating efforts to get people out of the city, said there were bodies of dead civilians in Marawi. He criticized the military for conducting air strikes and for hampering efforts to evacuate civilians. 
 "Some have no food at all. Some fear for their lives," he said. "This is a conflict that has gone beyond proportion. The magnitude of the degree of the damage and the people that are affected ... it's really massive." 
