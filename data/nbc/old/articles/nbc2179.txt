__HTTP_STATUS_CODE
200
__TITLE
Vigil Marks 25th Anniversary of Murder of Massachusetts Woman Susan Taraskiewicz
__AUTHORS
Rachael Trost
__TIMESTAMP
Sep 17 2017, 8:46 pm ET
__ARTICLE
 Several dozen people gathered in the rain at a park in Saugus, Massachusetts Thursday to mark the 25th anniversary of a woman's murder. 
 Her name was Susan Taraskiewicz. And her brutal murder remains unsolved. 
 "I survived these years through hope, faith and the support of many of you here today," Marlene Taraskiewicz, Susan's mother, told the crowd. "If we keep praying and hoping, I know we will get justice for Sue." 
 The last time anyone saw Susan alive was on September 12, 1992 at her shift as a supervisor for Northwest Airlines at Logan International Airport. 
 The 27-year-old is believed to have left to pick up sandwiches for coworkers. 
 She never returned to work or arrived at home. 
 Her vehicle was found two days later, parked at a local auto-body shop on Route 1A in a neighboring town. Susan's badly beaten body was discovered in the trunk. She had also been stabbed. 
 Susan had been feeling uneasy for some time at her job, according to authorities. The pretty brunette had allegedly been facing harassment at the hands of her male colleagues.  
 Not long after her death, several of those colleagues were implicated and charged in connection to a credit card scam. One theory is that someone involved in the scam believed Susan had knowledge of it, and her death was, in part, because of it.  
 "The facts and circumstances of Susan's brutal killing, including whether it was connected to people involved in the credit card thefts and sexual harassment or to other individuals, remains under active investigation," Suffolk County District Attorney officials wrote in a statement in 2014. 
 There have been renewed efforts to solicit information from the public in Susan's case over the past few years. 
 In 2016, Clear Channel Outdoor donated electonic billboard spaces with information about the case and a photo of Susan. 
 Massachusetts State Police also worked with Susan's mother Marlene in 2014 to produce an online video about the case, hoping increased awareness would persuade whoever knows something to come forward. 
 "Please come forward, it's not too late. It's never too late to solve a murder," Marlene said in the video. "A murder case is alway open. I am a very healthy woman and I am not going away. You're going to look over your shoulder until the day that I go, but I will get you and I will get justice for my Susan." 
 Anyone with information about Susan's case is urged to call the Massachusetts State Police at (617) 727-8817. 
