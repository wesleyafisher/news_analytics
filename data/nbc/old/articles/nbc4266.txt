__HTTP_STATUS_CODE
200
__TITLE
Trump Vows U.S. Will Win Fight Against Opioid Crisis
__AUTHORS
Ali Vitali
Corky Siemaszko
__TIMESTAMP
Aug 8 2017, 6:04 pm ET
__ARTICLE
 WASHINGTON  President Donald Trump vowed Tuesday the U.S. would "win" the battle against the heroin and opioid plague, but stopped short of declaring a national emergency as his hand-picked commission had recommended. 
 Instead, Trump promised to "protect innocent citizens from drug dealers that poison our communities." 
 "The best way to prevent drug addiction and overdose is to prevent people from abusing drugs in the first place," he said alongside administration officials at his golf club in Bedminster, New Jersey. "I'm confident that by working with our health care and law enforcement experts, we will fight this deadly epidemic and the United States will win." 
 Trump announced no new policies in the fight against opioids after promising a "major briefing" in a tweet Tuesday morning. 
 Last week, the presidential opioid commission, chaired by New Jersey Gov. Chris Christie, urged Trump to declare a national emergency and noted that America is enduring a death toll equal to September 11th every three weeks. 
 It recommended, among other things, expanding treatment facilities across the country, educating and equipping doctors about the proper way to prescribe pain medication, and equipping all police officers with the anti-overdose remedy Naloxone. 
 Trump did not address any of the recommendations. Instead, the president repeated his well-worn pledge to be "very, very strong on our Southern border." 
 "We're talking to China, where certain forms of man-made drug comes in and it is bad," Trump said. "We are speaking to other countries, and we're getting cooperation." 
 Asked later why Trump didn't make the declaration, Health and Human Services Secretary Tom Price said "all things are on the table." 
 "Most national emergencies that have been declared have been focused on a specific area, time-limited problem" like the Zika outbreak or Hurricane Sandy, Price said. "So we believe at this point resources that we need, or focus we need to bring to bear at this point, can be addressed without a declaration." 
 If Trump had declared a national emergency, it would have sent "a message about the scope of the problem, Northwestern University Pritzker School of Law professor Juliet Sorensen told NBC News earlier. 
 It also would have been the rare Trump move that both Republicans and Democrats could agree on. It is one of the few issues that does have bipartisan congressional support, she said. 
 Trumps declaration would have allowed the executive branch to direct federal funds to do things like expand treatment facilities," Sorensen said. As of now, Medicaid puts a cap on the number of beds each treatment facility can have, so legions of patients who need help get turned away." 
 The president would have also been able to direct funds to equip police with Naloxone and provide the FBI, the Drug Enforcement Agency and other law enforcement agencies with more money to focus on the manufacturers and smugglers of dangerous drugs like fentanyl, which is a powerful painkiller that packs 25 to 50 times more punch than straight heroin, Sorensen said. 
 In addition to Price, the Trump briefing was attended by First Lady Melania Trump, top adviser and son-in-law Jared Kushner, Counselor to the President Kellyanne Conway, and other White House officials. 
 Not present was Christie, who is on vacation in Italy. 
 Trump spoke on the same day that it was revealed that officials in and around Cincinnati, Ohio reported treating some two dozen people for heroin overdoses over the weekend. Police suspect the drugs were laced with fentanyl. 
 This surge shows that we cant keep using a Band-Aid to fight this epidemic, City Council President Yvette Simpson said. It is a devastating public issue. 
 Nearly 35,000 people across America died of heroin or opioid overdoses in 2015, according to the National Institute on Drug Abuse. But a new University of Virginia study released on Monday concluded the mortality rates were 24 percent higher for opioids and 22 percent higher for heroin than had been previously reported. 
 Ali Vitali reported from Washington, and Corky Siemaszko from New York. 
