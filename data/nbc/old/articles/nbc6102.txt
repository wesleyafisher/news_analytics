__HTTP_STATUS_CODE
200
__TITLE
Google Takes a Giant Leap Onto Apple, Amazon, Samsungs Turf
__AUTHORS
Alyssa Newcomb
__TIMESTAMP
Oct 4 2017, 4:25 pm ET
__ARTICLE
 SAN FRANCISCO -- Search giant Google is nudging its way into the tech gadget space that Amazon, Apple, and Samsung are fighting to dominate. 
 CEO Sundar Pichai has previously said Google is making a "big bet" on hardware, and at an event in San Francisco on Wednesday, he showed where that vision stands one year after Google's first massive hardware release. 
 During the nearly two-hour long event, Google executives introduced the second iteration of its Pixel and Pixel XL smartphones, wireless earbuds that can do real-time translations, a high-end Chromebook computer, an improved virtual reality headset, and a clip-on camera equipped with artificial intelligence. 
 Related: Get Ready for the Era of Augmented Reality 
 But perhaps the star of the lineup were two new Google Home speakers, positioning Google to go head to head with Amazon. 
 "Amazon should be concerned as Googles AI is much better than theirs," Patrick Moorhead, principal analyst at Moor Insights & Strategy, told NBC News. 
 The $49 Google Home Mini speaker packs smart features into a device smaller than a donut and is a natural rival to Amazon's $49.99 Echo Dot. Meanwhile, the $399 Google Home Max is designed to appeal to audiophiles who may also be interested in Apple's HomePod speaker, which also boasts room-filling sound quality. 
 "With computing and search becoming increasingly pervasive, Google is taking the view that it requires deeper involvement in hardware, Geoff Blaber, leader of mobile device software research at CCS Insight told NBC News. 
 But with just 81 shopping days left before the holidays, will Google be able to give its competitors a run for their money? 
 The company struggled to keep up with Pixel demand last year. Even Google's hardware chief joked about the shortage, mentioning, "Wish we had a few more to go around." 
 The new $649 Pixel 2 and the $849 Pixel XL 2 have two features that will help differentiate them from Apple's latest, the iPhone 8 and iPhone X, and Samsung's recently released Galaxy Note 8. 
 Google is eager to play up Pixel's highly rated camera, saying Wednesday the phones would be the first to have Google Lens, a new feature that allows users to point the phone's camera at anything to better understand what it is. Aside from the camera, the phone's assistant can also be triggered with a simple squeeze of its sides. The function was demonstrated onstage, with the presenter using it to quickly take a selfie with the audience. 
