__HTTP_STATUS_CODE
200
__TITLE
Georgia Boys Kidney Transplant Delayed After Donor Dad Is Arrested
__AUTHORS
Erik Ortiz
Stephanie Giambruno
__TIMESTAMP
Oct 17 2017, 10:29 am ET
__ARTICLE
 A 2-year-old Georgia boy born without kidneys found a perfect match in his father. But a plan for a kidney transplant that could help the boy live a normal and healthy life was unexpectedly delayed after the father was arrested last month. 
 While the father, Anthony Dickerson, 26, was released from jail on Oct. 2  a day before son A.J.'s scheduled surgery  the boy's fate remains in limbo, the family says, because of apparent red tape between police and the hospital. 
 "He's only 2, he don't deserve this," A.J.'s mother, Carmellia Burgess, said through tears to NBC affiliate WXIA. "We've been waiting for so long for this, and we were right at the front door." 
 Dickerson, who was on probation following a history of theft and forgery charges, was arrested Sept. 28 after police in Gwinnett County, outside of Atlanta, found him in possession of a firearm and said he fled officers. 
 The next day, Burgess said, she notified police to say that her son was going to have a kidney transplant and that Dickerson was the donor. 
 In the meantime, Burgess and Emory University Hospital, where the surgery was scheduled, sent a letter to the Gwinnett County Jail asking that Dickerson be escorted to Emory for necessary pre-surgery blood work. 
 Related: Researchers Grow Kidney, Intestine From Stem Cells 
 "If this is not a possibility, we will need to reschedule the surgery," a hospital official wrote. 
 The Gwinnett County Sheriff's Office said in a statement to NBC News that it did acknowledge the letter, and that it "worked very diligently" to ensure Dickerson could be freed before the Oct. 3 surgery. 
 Deputy Shannon Volkodav added that one of Dickerson's bonds was even lowered by a judge to $2,000 to help him. 
 But while everything seemed to be on course to have A.J.'s surgery, Burgess said the hospital's transplant center sent a puzzling letter halting the operation. 
 In the letter, Emory said it wanted evidence that Dickerson was complying with his parole officer for the next three months. After that, a doctor said, he could be re-evaluated for the transplant program in January. 
 It's unclear why the hospital changed the plan. 
 In a later statement, an Emory spokeswoman said its guidelines for organ transplants are "designed to maximize the chance of success for organ recipients and minimize risk for living donors." 
 "Transplant decisions regarding donors are made based on many medical, social, and psychological factors," she said. 
 The hospital declined to comment on Dickerson's case. 
 Dickerson, meanwhile, told WXIA that his son still needs the transplant and that he shouldn't suffer for the sins of his father. 
 "What do he got to do with the mistake I made? Nothing," Dickerson added. 
