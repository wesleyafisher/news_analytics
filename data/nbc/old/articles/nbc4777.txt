__HTTP_STATUS_CODE
200
__TITLE
FAA Orders A380 Engine Inspections After Midair Failure, Emergency Landing
__AUTHORS
Alastair Jamieson
__TIMESTAMP
Oct 13 2017, 5:24 am ET
__ARTICLE
 The Federal Aviation Administration on Thursday ordered visual inspections of some Airbus A380 superjumbo engines after one blew apart on a Los Angeles-bound flight last month. 
 It issued an emergency airworthiness directive requiring owners and operators of Engine Alliance GP7200 series engines to visually inspect the engines and remove the fan hub if defects are found. The engines are manufactured by a 50-50 joint venture between General Electric and Pratt & Whitney. 
 A380 planes powered by the rival Rolls-Royce Trent 900 engine are not affected by the order. 
 The FAA said the measure was prompted by the Sept. 30 failure of a fan hub on Air France flight AF66 over over Greenland which prompted an emergency landing in remote eastern Canada. 
 Such uncontained engine failures  in which machinery and other parts break away at high velocity  are rare. The FAA directive said the failure could lead to an uncontained release of the fan hub, damage to the engine, and damage to the airplane midair. 
 Some parts of the engine were retrieved by helicopter in Greenland on Oct. 6 and dispatched to French accident investigators in Paris. 
 Five hundred passengers on the Paris-Los Angeles flight spent several hours aboard the stricken plane in Goose Bay, Labrador, awaiting a replacement aircraft. 
 EA declined immediate comment. 
 GP7200 engines account for 60 percent of the global market share of A380 engines currently in service, according to Corrine Png, the CEO of transport research firm Crucial Perspective. 
 In addition to Air France, the affected airlines include Emirates, the world's largest A380 operator, as well as Etihad Airways, Qatar Airways and Korean Air Lines. 
 "An investigation to determine the cause of the failure is ongoing and we may consider additional rulemaking if final action is identified," the FAA said in a statement. 
 Depending on the number of flight cycles, the inspections must be performed within the next two to eight weeks. 
