__HTTP_STATUS_CODE
200
__TITLE
Fall of Mosul Would Mean End of ISIS Caliphate in Iraq: U.S. General
__AUTHORS
Richard Engel
__TIMESTAMP
Oct 18 2016, 5:27 pm ET
__ARTICLE
 Qayyarah Airbase, South of Mosul, Iraq  The American ground force commander in Iraq, Maj. Gen. Gary Volesky, said U.S.-backed Iraqi forces have momentum on their side in the battle to drive ISIS out of its Iraq stronghold, the northern city of Mosul. 
 Volesky, speaking to NBC News in his first interview since the Mosul offensive began earlier this week, expressed confidence that the terror group will lose Mosul and with it, its caliphate, or Islamic state, in Iraq. 
 "ISIS has said, this is the crown jewel of Iraq and its idea of a caliphate," Volesky said. Theres not going to be a caliphate, if there ever really was one. So for ISIL, this is going to be a key loss for them and it will be a loss" 
 Related: The Battle for Mosul Won't End With the Ouster of ISIS 
 Volesky praised Iraqi troops for advancing quickly toward Mosul and said so far the resistance they have meet has been moderate. 
 "Iraqis have the momentum," he said. "They know it and they want to get there as quickly as they can." 
 The Iraqi army has a lot riding on the success of the Mosul operation. ISIS was able to swiftly take control of Mosul two and a half years ago when the Iraqi army collapsed in the city, running from the fight and abandoning their weapons. 
 Volesky doesnt expect that to happen this time. 
 "They're much more confident and they've learned from these last two years," he said. "Make no mistake, they've learned." 
 Related: Villages Retaken as Bid to Free 1.5M From ISIS Begins 
 Volesky said the capture of Mosul would not mean the end of ISIS. Its fighters are expected to retreat into the desert and conduct terror attacks and try to establish an insurgency, he said. Volesky estimates there are between 3,000 to 5,000 ISIS fighters in Mosul, but he said some have been leaving. 
 "I'll tell you there are a lot fewer Daesh today than there were yesterday. There will be fewer tomorrow than today," he said, referring to another name by which ISIS is sometimes called. 
 Despite the successes thus far, Volesky said its just too early to know how long the Mosul operation will take. Most estimates have ranged from a few weeks to several months. 
