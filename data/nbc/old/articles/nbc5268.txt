__HTTP_STATUS_CODE
200
__TITLE
Pope Francis Canonizes Two Fatima Siblings
__AUTHORS
NBC News
__TIMESTAMP
May 13 2017, 7:49 pm ET
__ARTICLE
Pope Francis prays at the grave site of Portuguese shepherd children Jacinta and Francisco Marto at the Sanctuary of Our Lady of Fatima on May 13, 2017 in Fatima, Portugal. According to the Vatican, the pair, along with an older cousin named Lucia de Jesus dos Santos, had witnessed apparitions of the Virgin Mary. The site was declared a pilgrimage site after the Roman Catholic Church validated the children's visions.
The statue of the Virgin Mary is shoulder carried by faithful prior to the start of a mass at the Sanctuary of Our Lady of Fatima Saturday on May 13, 2017 in Fatima, Portugal. According to the Vatican, approximately 500,000 worshipers attended the Mass.
Pilgrims walk on their knees on their way to the Sanctuary of Our Lady of Fatima on May 12, 2017 in Fatima, Portugal. Many worshipers crawl on their knees as they make their way to the holy site.
Pope Francis leads a candle light vigil prayer at the Sanctuary of Our Lady of Fatima on May 12, 2017 in Fatima, Portugal. A good majority of the population of Portugal are Christian who follow Catholicism.
The faithful attend a candle light vigil prayer at the Sanctuary of Our Lady of Fatima on May 12, 2017 in Fatima, Portugal.
Worshippers hold candles during the "Blessing of the Candles" ceremony on May 12, 2017 in Fatima, Portugal.
Priests wait for the start of the ceremony of canonization at the Sanctuary of Fatima on May 13, 2017 in Fatima, Portugal. Worshipers traveled from both near and far to celebrate the Mass.
Pope Francis arrives in the popemobile at the Sanctuary of Our Lady of Fatima on May 12, 2017 in Fatima, Portugal.
Pope Francis, second right, speaks with Portuguese bishops upon his departure from the Casa do Carmo before leaving the Fatima Sanctuary on May 13, 2017 in Fatima, Portugal.
Portuguese President Marcelo Rebelo de Sousa kisses the hand of Pope Francis upon his arrival at Monte Real air base on May 12, 2017 in Leiria, Portugal.
Pope Francis waves to devotees inside the popemobile on his way to Fatima Sanctuary after he landing in a Portuguese Air Force helicopter at Fatima Municipal Stadium on May 12, 2017 in Fatima, Portugal.
