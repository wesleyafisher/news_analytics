__HTTP_STATUS_CODE
200
__TITLE
Sebastian Kurz, 31, Is Set to Take Power in Austria
__AUTHORS
Associated Press
__TIMESTAMP
Oct 16 2017, 5:54 am ET
__ARTICLE
 Austrian Foreign Minister Sebastian Kurz is poised to become the first millennial to lead a European country following his party's victory in a national election Sunday. 
 While no party won a majority, the 31-year-old is most likely to be sworn in as Austria's next chancellor  and Europe's youngest leader  after the tough coalition government negotiations that lie ahead. 
 He defies the traditional stodgy image of politicians and mostly goes without a tie, works standing behind a desk and flies economy class. He has a girlfriend, but is private about his life outside politics. 
 Near-final results from Sunday's balloting put his conservative People's Party comfortably in first place, with 31.4 percent of the vote. The right-wing Freedom Party came in second with 27.4 percent. The center-left Social Democratic Party of Austria, which now governs in coalition with People's Party, got 26.7 percent. 
 Becoming head of government would be the next leap in a political career that started eight years ago when Kurz, then studying law, was elected chairman of his party's youth branch. 
 Smart and articulate, he eventually caught the eye of People's Party elders. He was appointed state secretary for integration, overseeing government efforts to make immigrants into Austrians, in 2011. Two years later, at the age of 27, he became Austria's foreign minister  the youngest top diplomat in Europe. 
 He hosted several rounds of talks between Iran and six other countries on Tehran's nuclear program, meeting Russian Foreign Minister Sergei Lavrov, Secretary of State John Kerry and other power-brokers. Other international events further boosted his visibility and party influence. 
 When a new wave of migrants and refugees seeking to relocate to Europe became a continent-wide concern in 2015, Kurz recognized Austrian voters' anxiety over unchecked immigration involving large numbers of Muslim newcomers. 
 He called for tougher external border controls, better integration and stringent control of "political Islam" funded from abroad. He also organized the shutdown of the popular overland route through the West Balkans many newcomers were using to reach the EU's prosperous heartland. 
 Kurz and his traditionally centrist party had drifted considerably to the right of their Social Democratic government partners, making governing difficult. Kurz's moment came when both agreed this spring to an early national election. 
 The People's Party, then lagging in third place and long seen as a stodgy old boys network, made him leader. Kurz set out to reinvent the party's image after securing guarantees for unprecedented authority. 
 The youthful, Vienna-born politician turned out to be the tonic the party needed, helping it shrug off criticism that it's been part of the political establishment for decades. 
 Noting that his center-right party had triumphed over the rival Social Democrats only twice since the end of World War II, Kurz called Sunday's election a "historic victory." 
