__HTTP_STATUS_CODE
200
__TITLE
When Did We Stop Being America? Puerto Ricans Angry, Dismayed Over Trump Tweets 
__AUTHORS
Sandra Lilley 
Suzanne Gamboa
Daniella Silva
Carmen Sesin
__TIMESTAMP
Oct 13 2017, 7:22 am ET
__ARTICLE
 President Donald Trump's Twitter comments on Thursday threatening to pull federal resources from Puerto Rico as the U.S. territory grapples with the lack of basic necessities elicited shock and alarm on the island and the mainland. 
 San Juan Mayor Carmen Yuln Cruz urged "every American that has love, and not hate in their hearts, to stand with Puerto Rico and let this president know we WILL NOT BE LEFT TO DIE." 
 Gov. Ricardo Rossell tweeted that U.S. citizens in Puerto Rico were "requesting the support that any of our fellow citizens would receive across our nation." 
 There was worry over the impact of the president's tweets; critics said that as head of the federal government, his statements could be interpreted as less than a full commitment to the island's recovery. 
 "If I'm being told to do everything to fix this, but if I also hear 'I really don't want it fixed, it's their fault,' then you're not going to be willing to stand on that cliff and take that leap," said Nancy Santiago Negrn, who held several senior positions in the Obama administration. "Hesitation can cost us lives." 
 With people drinking potentially contaminated river water, eating plants because of a lack of food and the official death toll mounting, Santiago Negrn said Trump's comments left her "petrified." 
 "When did we stop being America? " she asked. 
 Isabel Rulln, the managing director of Puerto Rico non-profit ConPRmetidos said Puerto Ricans were still in dire need of basic necessities. 
 "The basic needs for food, shelter and safety of millions of U.S. citizens in Puerto Rico remain at a critical stage," she said. "We still greatly need boots-on-the-ground help precisely to stabilize the island's foundations and allow for the implementation of long-term solutions for a healthy, dynamic and self-sufficient community." 
 Danny Vargas, a Latino Republican strategist and Air Force veteran who is of Puerto Rican descent, said that he had "almost given up trying to interpret any of this  a lot of this is just noise." 
 But he said he's confident that the military, the National Guard and FEMA will continue to focus on their efforts until they are told to do otherwise. 
 "Vice President Pence said last night that the federal government will be in Puerto Rico until the job is done," said Vargas, referring to remarks Pence made at a Hispanic Heritage celebration Wednesday night. 
 RELATED: Trump Attacks Puerto Rico, Threatens to Pull Emergency Responders  
 But Frank Mora, a former Pentagon official who is now director of the Latin American and Caribbean Center at Florida International University, drew a comparison between the U.S. response to the 2010 earthquake in Haiti and Trumps tweets about Puerto Rico. 
 President Obama, at the time, was very clear with us when he said these folks wont be forsaken, Mora said, pointing out that Puerto Rico is a U.S. territory and Haiti is a foreign country. The U.S. military remained in Haiti for about six months after the earthquake, Mora said. 
 Mora said that he found it "disheartening" that only a few weeks after hurricanes hit Puerto Rico, the president was "making excuses ... and putting the blame on the Puerto Ricans, at a time, when they, Americans, need us more than ever." 
 It sort of saddens me to think that this is what the American government stands for," he said. 
 Michelle Lujan Grisham, D-N.M., chairwoman of the Congressional Hispanic Caucus, said in a statement that the president's "suggestion that we will cut and run on our fellow Americans is disgraceful." 
 Trump tweeted that Puerto Rico's financial crisis was "largely of its own making," once again putting Puerto Ricans on the defensive about its fiscal situation while recovering from a worst-in-a-century natural disaster. 
 Sylvia Manzano, a Houston-based political scientist, said Trump's tweets display an antagonism, in particular to Latinos, that he has not shown elsewhere. 
 "There are states that are in debt and rely on federal programs  we don't cut them off because of a tough economic situation," Manzano said. 
 A Kaiser Family Foundation poll shows that Americans want more done for Puerto Ricans. 
 Sixty-two percent of the American public said Puerto Ricans affected by the storm were not getting the help they need, and a little more than half, 52 percent, said that the federal response has been too slow. The poll was conducted Oct. 4-8. 
 Philipe Schoene Roura, editor of Caribbean Business, Puerto Rico's main financial newspaper, said Trump's remark was "irresponsible and misguided." 
 "It's not a fair comment to make," Schoene Roura said. "Many people were involved in allowing the debt to run to the levels at which it stands, including people in Puerto Rico, but also Wall Street and Washington." 
 Trump's comments ignore structural issues such as the fact that the island shoulders more of its health spending than U.S. states and has lost crucial tax incentives that helped create jobs. 
 "The first matter at hand has to be reconstruction and saving the lives of people, Schoene Roura said. "It's a race against time." 
 For some lawmakers, Trump's comments called into question his fitness for office. 
 "By suggesting he might abdicate this responsibility for our fellow citizens in Puerto Rico, Mr. Trump has called into question his ability to lead, said Rep. Nydia Velzquez, D-N.Y. 
 CORRECTION (Oct. 11, 2017, 10:50 p.m.): An earlier version of this article misspelled the first name of the editor of Caribbean Business. He is Philipe Schoene Roura, not Philip 
 Follow NBC Latino on Facebook, Twitter and Instagram. 
