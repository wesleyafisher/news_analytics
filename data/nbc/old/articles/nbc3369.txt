__HTTP_STATUS_CODE
200
__TITLE
Five Die While Using Obesity Devices, FDA Says
__AUTHORS
Maggie Fox
__TIMESTAMP
Aug 14 2017, 3:41 pm ET
__ARTICLE
 At least five people have died soon after being fitted with balloons designed to help them lose weight, the Food and Drug Administration said Thursday. 
 The FDA says it doesnt know if the devices or the surgery to implant them is to blame but issued an alert to doctors to closely monitor patients who get them. 
 All five reports indicate that patient deaths occurred within a month or less of balloon placement, the FDA said in a statement. 
 In three reports, death occurred as soon as one to three days after balloon placement. At this time, we do not know the root cause or incidence rate of patient death, nor have we been able to definitively attribute the deaths to the devices or the insertion procedures for these devices. 
 Related: Gas-Filled Balloon May Offer New Weight Loss Option 
 One of the devices is a balloon that can be placed in the stomach and filled with saline water, the Orbera balloon made by Apollo Endo-Surgery. The other is a dual balloon system made by ReShape. Apollo, which says it has sold 277,000 of the devices globally, said it was preparing a statement. 
 Apollo said it reported the deaths to the FDA. "There have been five reported deaths of patients who had received the Orbera intragastric balloon in four different countries since Orbera's FDA approval in August of 2015," the company said in a statement. 
 "Apollo self-reported all five cases to the FDA as part of its Global Product Surveillance program." 
 The FDA says it's looking into two other deaths. 
 The Agency has also received two additional reports of deaths in the same time period related to potential complications associated with balloon treatment (one gastric perforation with the Orbera Intragastric Balloon System and one esophageal perforation with the ReShape Integrated Dual Balloon System), the FDA added. 
 The FDAs adverse report system is very broad and is not designed to immediately prove a device or drug caused a complication. It is meant to gather information about anything that happens while someone is using a device or drug, or soon after, so that investigators can determine cause and effect. 
 Related: Balloon You Can Swallow Fights Obesity 
 "For the first patient in Brazil, the cause of death was reported to be a heart attack. An autopsy was performed but the final report was not provided to us. There was no evidence provided of a devicerelated malfunction or causation in this case," Apollo said in its statement. 
 "For the second patient in Brazil, the hospital reported that a nondevice related gastric perforation occurred, causing complication and eventual death." 
 Causes of death for a patient in Britain and one in the U.S. were not known, the company said. A patient who died in Mexico had a heart abnormality but it's not clear what caused the death. 
 Gastric balloons are among many different devices on the market to treat severe obesity. They aim to reduce how much a person can eat by filling the stomach, closing off part of the stomach or even surgically reducing stomach volume. 
 The need is great for different ways to treat obesity. 
 Obesity drugs have not been big commercial successes although several are now on the market. Several drugs were removed from the shelves years ago after they were found to cause problems such as stroke. 
 Diets are equally bad at helping people keep off the pounds for long periods of time. But surgical procedures to shrink the stomach have been very successful. One study showed the surgery helped people to lose enough weight to reverse their type-2 diabetes. 
 The American Diabetes Association and other groups have endorsed bariatric surgery as a way to treat type-2 diabetes. 
