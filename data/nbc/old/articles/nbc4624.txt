__HTTP_STATUS_CODE
200
__TITLE
Cannes Palme d'Or Goes to Ruben Ostlund's 'The Square'
__AUTHORS
Associated Press
__TIMESTAMP
May 28 2017, 3:23 pm ET
__ARTICLE
 CANNES, France  The Cannes Film Festival awarded its coveted Palme d'Or award to Ruben Ostlund's "The Square" on Sunday, while Sofia Coppola became only the second woman to win the best director award. 
 "Oh my god! OK," the Swedish filmmaker exclaimed after he bounded onto the stage to collect the prestigious Palme. 
 Coppola won best director for "The Beguiled," her remake of Don Siegel's 1971 Civil War drama. The first  and until now only  female winner of the prize was Soviet director Yuliya Ippolitovna Solntseva in 1961. 
 Dominic West, Elisabeth Moss and Claes Bang star in "The Square." Bang plays the curator of an art museum, who sets up "The Square," an installation inviting passers-by to acts of altruism. But after he reacts foolishly to the theft of his phone, the respected father of two finds himself dragged into shameful situations. 
 Diane Kruger was named best actress and Joaquin Phoenix best actor as the prestigious festival celebrated its 70th anniversary. 
 Kruger was honored for her performance in Fatih Akin's "In the Fade." In the drama, she played a German woman whose son and Turkish husband are killed in a bomb attack. 
 "I'm overcome," Kruger said. "Thank you a thousand times." 
 Phoenix was recognized for his role in Lynne Ramsay's thriller "You Were Never Really Here," in which he played a tormented war veteran trying to save a teenage girl from a sex trafficking ring. 
 The actor wore sneakers on stage as he collected the prize. He said his leather shoes had been flown ahead of him. 
 He apologized for his appearance, saying the prize was "totally unexpected." 
 Spanish filmmaker Pedro Almodovar presided over the competition jury. 
 The French AIDS drama "120 Beats Per Minute" won the Grand Prize from the jury. The award recognizes a strong film that missed out on the top prize, the Palme d'Or. 
 The jury also presented a special prize to celebrate the festival's 70th anniversary, to actress Nicole Kidman. 
 Kidman wasn't at the French Rivera ceremony, but sent a video message from Nashville, saying she was "absolutely devastated" to miss the show. 
 Jury member Will Smith made the best of the situation, pretending to be Kidman. 
 He fake-cried and said in halting French, "merci beaucoup madames et monsieurs." 
