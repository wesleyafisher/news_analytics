__HTTP_STATUS_CODE
200
__TITLE
Twin Plagues: Meth Rises in Shadow of Opioids
__AUTHORS
Jon Schuppe
__TIMESTAMP
Jul 5 2017, 12:20 pm ET
__ARTICLE
 America can't quit its meth habit. 
 After a brief lull caused by a crackdown on domestic manufacturing techniques, the highly addictive stimulant is blooming across the country again, this time in the shadows of the opioid epidemic. 
 Because meth kills slowly, and at lower rates, it isn't getting the attention that many researchers, law enforcement officials and health workers say it deserves. They worry it will eventually overwhelm the country as heroin, fentanyl and prescription painkillers have. 
 Some states are fighting both epidemics at once. 
 "All of a sudden, it's everywhere again," Wisconsin Attorney General Brad Schimel said. 
 Schimel commissioned a study of meth in his state, which estimated that its use had jumped by at least 250 percent since 2011, a pace that could overtake heroin. "We are entering another full-blown epidemic with meth," he said. 
 Ohio, a focal point of the opioid epidemic, is also battling a meth resurgence, particularly in rural areas, authorities have said. Reports indicate the same happening in Texas, Montana, Minnesota, Oklahoma, Iowa and South Dakota. 
Meth Cases in Wisconsin More than Tripled in 10 Years
Prevelance of the drug increased across rural areas of the state
2010
2011
2014
2015
2016
2012
2013
May, 2017
Cases per year by county
0
15
30
45
60
75
90
Wisconsin Statewide Intelligence Center
 Researchers point out that meth addiction has always been a big problem in America. What's changed, they said, is a switch to mass production in Mexico, an increase in potency and affordability, and deeper penetration by drug cartels into vulnerable communities. 
 And, unlike opioids, there is no proven medical treatment for meth addiction. 
 "We can do therapy and that sort of thing, but we don't have a magic pill," said Jane Maxwell, who researches drug abuse at the University of Texas at Austin. 
 Maxwell has documented a meth spike in her home state that includes a rise in deaths, treatment-center admissions, poison-center calls, and toxicology lab submissions. She believes meth is driving an uptick in HIV cases. 
Heroin Use Rose in Metro Areas at a Pace Comparable to Meth
Cases are concentrated in Milwaukee and surrounding metropolitan areas
2010
2011
2014
2015
2016
2012
2013
May, 2017
Cases per year by county
0
15
30
45
60
75
90
Wisconsin Statewide Intelligence Center
Meth Cases in Wisconsin 
More than Tripled in 10 Years
Cases per year by county
0
15
30
45
60
75
90
Heroin
Meth
2010
2011
2012
2013
2014
2015
2016
May, 2017
Wisconsin Statewide Intelligence Center
 "I'm concerned that as all this money goes into treating opioid users, what are we going to do for meth users?" 
 Underlying the crisis is a growing concern that America focuses considerable resources on curtailing the supply of drugs, and punishing those who sell and abuse them, with little impact on the demand. 
 "People using meth aren't just going to stop," said Henry Brownstein, director of the Center for Public Policy at Virginia Commonwealth University. His exploration of American meth markets resulted in the 2014 book "The Methamphetamine Industry in America: Transnational Cartels and Local Entrepreneurs." 
 Brownstein and his co-author, Timothy Mulcahy, charted the evolution of meth during the early 2000s from a hyper-local enterprise, produced in small labs and sold and shared among families and friends, to a sophisticated network driven by Mexican drug trafficking organizations. 
 That switch was largely attributed to the Combat Methamphetamine Epidemic Act, which placed tight controls of over-the-counter cold medicines used to cook meth. The number of meth labs in America dropped precipitously after that law went into effect in 2006, according to authorities. So did federal prosecutions of meth offenses. So did the amount seized by police. So did the number of meth users, and the number of hospital admissions, federal data shows. 
 But entrepreneurial traffickers found ways to fill the void. 
 Their innovations led to the growth of Mexican "superlabs" where a cheap and highly potent version of meth was mass-produced for shipment by cartels over the border, ending up in the hands of mid- and low-level American dealers across the South, West and Midwest. 
 Almost immediately, the signs pointing to meth's decline began to flip. 
 As the Mexican-made meth saturated drug markets, the price fell dramatically in 2007, and continued to decline for most of the next decade, according to data collected by the U.S. Drug Enforcement Administration. At the same time, the purity of meth consumed in the United States rose to nearly 100 percent. 
 The estimated number of meth users, meanwhile, rose from its low point of 314,000 in 2008 to 569,000 in 2014, according to the Substance Abuse and Mental Health Services Administration. The agency changed the way it measured meth use after that, so it is not possible to make a fair comparison with more recent years. 
 At the same time, fatal overdoses involving meth more than doubled from 2010 to 2014, according to the most recent data available from the National Center for Health Statistics. 
 And meth seizures exploded, from 7,063 kilograms in 2006 to 44,077 kilograms in 2015, according to the White House Office of National Drug Control Policy. 
 "We're seeing it pour across the border in bigger quantities," said Mark Conover, the deputy U.S. Attorney in Southern California, where the vast majority of Mexican meth enters the United States, hidden in cars and containers and sniffed out by dogs at ports of entry and highway checkpoints. Border seizures in San Diego County jumped from 3,585 kilograms in 2012 to 8,706 kilograms in 2016. "It used to be that loads of 20, 30, 40 pounds were big for us. Now we have 200-pound loads." 
 In Arizona, seizures are up by about 140 percent over the past five years, said Douglas Coleman, the special agent in charge of the Drug Enforcement Administration office there. He blamed the Mexican cartels not only for mass producing high-potency meth, but also for blocking their Colombian rivals out of the American cocaine market, a strategy that involves luring coke customers to meth. 
 "The average user has no issue switching to meth because they need that stimulant," Coleman said. 
 Meantime, the number of people sentenced to federal prison for meth offenses has risen to levels far beyond any other drug, according to the U.S. Sentencing Commission. In 2015, meth offenders made up the highest proportion of federal drug offenders in 27 states, mostly in the South, Midwest and West. 
 Those trends point to the inadvertent consequences of the 2006 law, said Mulcahy, a vice president at NORC, a research agency at the University of Chicago. They provide a counter-argument to traditional American thinking about drug policy: use enforcement crackdowns as a tool to jack up the price of illegal drugs, and reduce demand, he said. 
 "Now we're at a place where we're going completely backward," Mulcahy said. 
 Travis Linnemann, an Eastern Kentucky University sociologist and author of the book "Meth Wars," said he suspected that meth use hasn't really changed that much in America, and cautioned that law enforcement's emphasis on the drug may reflect broader priorities related to the war on drugs, border security and restricting immigration. "That's not to say cartels don't traffic meth, but they've been doing it for a long time," he said. 
 In many rural and struggling communities, Linnemann said, meth is often singled out as a reason for deteriorating conditions, when much bigger forces  the decline of family farms, the rise of corporate agriculture  are also responsible. 
 In Wisconsin, those elements are closely linked. 
 Routed from trafficking hubs around Minneapolis/St. Paul, meth is ravaging areas of western Wisconsin that had weathered the first wave, officials say. Things have gotten so bad that local government agencies are running out of places to put children taken from the custody of their hopelessly addicted parents. 
 That includes Dunn County, a rural and manufacturing community of 44,000 that has been overwhelmed with meth. Meth addiction there spans generations, and breaks apart families, forcing child-welfare officials to remove children from their homes, health workers say. 
 Kristin Korpela, head of the Dunn County Department of Human Services, recalled having one of her social workers visit a young mother in jail, where she voluntarily gave up her three kids. 
 "Meth," the mom told the social worker, "makes you forget that you ever had children." 
 Next door, in Chippewa County, Human Services Director Larry Winter has seen out-of-home placements for children surge from 28 in 2013 to 103 in just the first five months of 2017. Three-quarters of those placements are related to meth abuse, he said. 
 Such punitive measures, along with arrests, do little to curb the epidemic  and may make things worse, he said. 
 "People who may want to come forward to do something about their addiction are afraid they will get charged and DHS will take their kids," Winter said. "My staff tells me that if they could keep the family and kids together, and have the resources to do it safely, recovery from the addiction is possible and it's going to reduce the risk, because parents are willing to stay engaged. However, once we make that decision to take those kids into custody, it is very difficult to keep the parents engaged, and often times theyre AWOL." 
 Meanwhile, in eastern Wisconsin, the opioid crisis continues to flourish. 
 John Kumm, an analyst in the FBI's Milwaukee office, said the two epidemics appear to be moving toward the middle of the state. Already, authorities have documented meth users to turn to heroin to take the edge off their highs, and heroin addicts who turn to meth because there's less change of an overdose. 
 This year, the number of criminal meth cases has eclipsed those involving heroin or fentanyl, Kumm said. 
 "The biggest point is there is a dual crisis here, and we're sure it's not just in Wisconsin," he said. 
 Schimel, the attorney general, likened the twin plagues to holes at each end of a rowboat, with law enforcement authorities and treatment providers trying to bail out water, with more continuing to pour in. 
 The patch, in this case, is an effective prevention strategy, which Wisconsin is still trying to develop, he said. 
 "With opiates, when we discovered the nature of that epidemic  and we're still working to get out way out of it  we kind of thought this is as bad as it gets," Schimel said. "And then meth came along." 
