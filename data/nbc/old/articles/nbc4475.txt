__HTTP_STATUS_CODE
200
__TITLE
Las Vegas Shooting: What Was the Motive Behind Stephen Paddocks Deadly Spree?
__AUTHORS
Erik Ortiz
Julia Ainsley
Elizabeth Chuck
__TIMESTAMP
Oct 3 2017, 9:32 pm ET
__ARTICLE
 Unlocking the motive of the gunman who slaughtered dozens of concertgoers in Las Vegas remained a top priority Tuesday for investigators, who searched for clues in the shooter's home and sought to interview those who knew him, including a live-in girlfriend. 
 Inside Stephen Paddock's suite on the 32nd floor of his casino hotel, police found a sledgehammer and 13 suitcases, according to an internal law enforcement document seen by NBC News and senior law enforcement officials. 
 Investigators believe Paddock used the sledgehammer to smash two windows from which he unloaded a hail of bullets into a crowd enjoying a country music festival Sunday night, officials said. He killed at least 58 people and injured 530 others in the worst mass shooting in modern American history. Authorities said Tuesday night that the widely reported number of 59 deaths includes the gunman. 
 With seemingly little to go on, piecing together Paddock's life and any grievances or hatred he harbored is essential, said former FBI profiler Clint Van Zandt. 
 "All of the normal indicators that we would normally see, people two or three days later say, 'A-ha, now I understand, I know what was going on in this guy's life,'" Van Zandt said on "TODAY." In Paddock's case, "we don't know." 
 Clark County Sheriff Joseph Lombardo told reporters Monday, "We have no idea what his belief system was. I can't get into the mind of a psychopath." 
 Investigators said his residence in the desert community of Mesquite, northeast of Las Vegas, didn't immediately turn up notes, emails or social media postings explaining his actions, but a computer from the home was found. Nineteen firearms, along with explosives and thousands of bullets, were also discovered there  on top of the 23 recovered from his suite at the Mandalay Bay Resort and Casino. 
 Related: What We Know About the Las Vegas Victims So Far 
 At first glance, Paddock's quiet existence didn't sound off alarms. 
 While his brother told reporters Monday that Paddock was "not a normal guy," he also said Paddock didn't have any known drug or alcohol problems or extreme political or religious views. (ISIS suggested in a widely discounted claim that Paddock was a soldier of the terror group.) 
 Police said the shooter had only received a citation, and two Nevada gun shops said he passed the required background checks in the past year to purchase firearms. 
Gunman's position on 32nd floor of Mandalay Bay resort gave unobstructed firing line to 22,000-person concert below. https://t.co/c4WeultaCW pic.twitter.com/TbhOCMwtQb
 Eric Paddock described his brother as a high-stakes gambler who made millions through investing and owning properties. On several occasions, he gambled more than $10,000 per day  and played with at least $160,000 in the past several weeks at Las Vegas casinos, according to multiple senior law enforcement officials. 
 "He'd grouse when he'd lost. He never said he'd lost $4 million or something," Eric Paddock said from his home in Orlando, Florida. "I think he would have told me." 
 The brothers' father, however, was widely known to law enforcement. Benjamin Hoskins Paddock was on the FBI's Top Ten Most Wanted list in the 1970s for robbing banks and escaping prison. He was described as "psychopathic" in an arrest warrant. 
 At 64, Paddock was almost 30 years older than the average mass shooter, according to experts. 
 Criminologists said while there's no "typical" profile for one, Paddock  and the attack he carried out  was unusual in several ways. 
 "Definitely the age is interesting. I don't think we've had too many in their 60s," said Mary Muscari, an associate professor at Binghamton University in upstate New York and a forensic nurse and criminologist. "The other thing that stood out was his method: Most of the shooters want to be face-to-face with their victims. This is like a sniper attack, which is unusual." 
 Regardless, investigators will treat the case the same as any other massacre, said Dr. Tod Burke, professor of criminal justice at Radford University in Virginia and a former Maryland police officer. 
 "Police, as they do their investigation, they want to find out a little bit more about this person's background. Did they have anything that would have been a red flag to family, friends, or law enforcement, such as a change in behavior, something that wasn't part of their normal routine?" Burke said. "Did they talk about things, like they were going to commit suicide and take others with them?" 
 He added that Paddock may not have mentioned his specific Las Vegas plans, but could have imparted other hints to those who knew him, such as giving away personal belongings. And it's important to speak with acquaintances beyond Paddock's family circle, Burke said. 
 Related: Las Vegas Shooter Wired $100,000 to Philippines Last Week 
 "Sometimes, families are the last people to know," he said. 
 Police have yet to speak with Paddock's girlfriend, Marilou Danley, who was in her native Philippines when the attack occurred, senior law enforcement officials said. She is not believed to be involved in the shooting "at this time," they added. 
 Paddock had wired $100,000 to an account in the Philippines in the week before the shooting, officials said, although it's unclear whether the money was intended for Danley, her family or another purpose. 
 She remains crucial, experts say, because she'll have insight into Paddock's motivations and mindset. 
 "She's the one who's going to say, 'You know, I saw those 30 or 40 guns in the house, and I said, What are you going to do with these?'" Van Zandt said. 
