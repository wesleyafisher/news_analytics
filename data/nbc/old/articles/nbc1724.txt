__HTTP_STATUS_CODE
200
__TITLE
The Week in Pictures: June 23 - 30
__AUTHORS
NBC News
__TIMESTAMP
Jun 30 2017, 7:12 pm ET
__ARTICLE
A young boy plugs his ears as his sister and babysitter watch planes land at Reagan National Airport from Gravelly Point Park in Arlington, Virginia on June 29.
A police officer looks over a large sinkhole that swallowed a Toyota Camry in St. Louis on June 29. No injuries were reported and the car's owners told a local TV station they were returning from the gym when they found their car in the crater. It wasn't immediately clear what caused the collapse.
A Chinese Crested dog named Rascal attends the 2017 World's Ugliest Dog contest at the Sonoma-Marin Fair on June 23 in Petaluma, California.
Turkish riot police officers block access to Istikjlal avenue as LGBT rights activists try to gather for a parade, which was banned by the government, in Istanbul on June 25.
Organizers of the 2017 Istanbul LGBTI+ Pride had vowed to march in central Taksim Square, using a Turkish hashtag for "we march," despite the ban on gay pride observances ordered by the Istanbul governor's office for the third year in a row.
Police used tear gas to disperse the crowds and activists said plastic bullets were also used. Riot-control vehicles and buses were dispatched to the area. Turkey's official Anadolu news agency said "an estimated 20 people" were detained after protesters did not heed warnings to disperse because the march did not have a permit.
PHOTOS:Istanbul Police Enforce Ban on Pride Parade
A young boy reacts as his hair is shaved off by a Buddhist monk during an ordination ceremony at Wat Benchamabophit in Bangkok on June 23.
The annual ceremony this year had 349 hill tribe men and young boys getting ordained as monks and novices at the temple to mark the beginning of the three-month Buddhist Lent or Khao Pansa which begins on July 9, and also to honor the late Thai King Bhumibol Adulyadej. In Thailand every Buddhist man is expected to become a monk during some period of his life.
A member of the Libyan coast guard stands on a boat as migrants attempting to reach Europe are rescued off of Zawiyah on June 27. More than 8,000 migrants have been rescued in waters off Libya during the past 48 hours in difficult weather conditions, according to Italy's coast guard.
The interior ministers of France, Germany and Italy will meet in Paris on Sunday to discuss ways to help Italy, which has been struggling with the masses of migrants arriving on its shores and has demanded solidarity from other members of the European Union. Italy has seen more than 500,000 migrant arrivals since 2014, including 82,000 so far this year.
Muslim worshipers leave a group prayer service during Eid al-Fitr, which marks the end of the Muslim holy month of Ramadan in Brooklyn, New York on June 25.
Senate Minority Leader Sen. Chuck Schumer of N.Y. and other democratic senators hold photographs of constituents who would be adversely affected by the proposed Republican Senate healthcare bill during a news conference outside the Capitol in Washington on June 27.
Twenty-two million Americans would lose insurance over the next decade under the U.S. Senate Republican healthcare bill, a nonpartisan congressional office said on Monday, complicating the path forward for the already-fraught legislation.McConnell, short of votes, unexpectedly abandoned plans to whisk the measure through his chamber this week.
A dragonfly lands on a lotus flower bud in Echo Park Lake in Los Angeles on June 26.
The lotus flowers at the lake have returned following a two-year rehabilitation project where the lake was drained and restored and the lotus beds replaced.
Italy's former Prime Minister Silvio Berlusconi plays with a dog during the television talk show "Porta a Porta" (Door to Door) in Rome on June 21.
This image was released by Reuters this week.
Nepalese transgender women participate in the first national transgender dance competition in Kathmandu, Nepal on June 25. The event was organized by the Blue Diamond Society, an LGBT rights group.
Syrians gather around an ice-cream truck amid destroyed buildings on the last day of the fasting month of Ramadan in the rebel-held town of Douma, Syria early on June 25.
Relatives mourn at the site of a landslide in Xinmo village in China's Sichuan province, on June 25.
A second landslide struck the village in southwest China where rescue workers have been looking for nearly 100 people buried over the weekend by a massive wave of rocks and debris.While no further casualties were reported, the second landslide sets back rescue teams searching for 93 people missing since early Saturday, when rugged mountains flanking the village gave away and buried its residents.
PHOTOS:Day Two in the Search for Victims Buried in China Landslide
Rescue workers try to remove the body of a victim at the site of a landslide in Xinmo village, in China's Sichuan province on June 24.
Rescuers dug through earth and rocks for a second day on Sunday in an increasingly bleak search for some 118 people still missing after their village in southwest China was buried by a huge landslide. Rescuers have pulled 15 bodies from the avalanche of rocks that crashed into 62 homes in Xinmo, a once picturesque mountain village nestled by a river in Sichuan province.
A truck burns after being set on fire during a demonstration by opposition activists against the government of President Nicolas Maduro in Caracas on June 23.
An Iraqi Special Forces soldier exchanges fire with ISIS militants in the Old City of Mosul, Iraq on June 30.
Iraqi troops were clearing up a key neighborhood in Mosul on Friday, commanders said, a day after making significant gains against Islamic State militants in the city and after the country's prime minister declared an end to the extremist group's self-proclaimed caliphate.
Some 300 IS fighters are thought to remain holed up inside the last Mosul districts, along with an estimated 50,000 civilians, according to the United Nations.
Zeid Ali, 12, left, and Hodayfa Ali, 11, comfort each other after their house was hit and collapsed during fighting between Iraqi forces and ISIS militants in Mosul, Iraq on June 24. The Ali cousins said some of their family members are still under the rubble.
China and Kong Hong national flags are displayed outside a shopping center in Hong Kong on June 28 to mark the 20th anniversary of Hong Kong handover to China. Hong Kong marks 20 years under Chinese rule on July 1.
China said on Friday the joint declaration with Britain over Hong Kong, which laid the blueprint over how the city would be ruled after its return to China in 1997, was a historical document that no longer had any practical significance.
In response, Britain said the declaration remained in force and was a legally valid treaty to which it was committed to upholding.
Devotees wear costumes made of banana leaves as they head to church to attend mass as part of a religious festival in honor of St. John the Baptist, also known locally as the "mud people" festival in Aliaga town, Philippines on June 24.
Farmers coated in mud paraded in Philippine villages on Saturday to mark one of the Catholic nation's most colorful religious festivals.
The Week in Pictures: June 16 - 23
