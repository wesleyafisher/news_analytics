__HTTP_STATUS_CODE
200
__TITLE
Heres What You Can Do About That Equifax Data Breach
__AUTHORS
NBC News
__TIMESTAMP
Sep 11 2017, 6:21 pm ET
__ARTICLE
 The Equifax security breach may have exposed private information belonging to almost half the U.S. population, so you should definitely pay attention. 
 Credit reporting agencies work differently from other data companies, so while you may never have dealt with Equifax, their servers were still likely to have your data. 
 Here are some tips to help you protect yourself from consumer protection expert Bob Sullivan, an NBC News contributor and man behind The Red Tape Chronicles. Sullivan has more tips on his site. 
 Freezes also generally cost money (the rules vary by state; Trans Union has a grid showing you the varying fee levels by state and consumer criteria), and they can be a hassle, because when it comes time to get a mortgage or an auto loan, consumers sometimes don't remember the procedure to "thaw" their reports. 
 
