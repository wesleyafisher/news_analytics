__HTTP_STATUS_CODE
200
__TITLE
What Trump Heard From Puerto Ricans? He Says, Only Thank Yous 
__AUTHORS
Ali Vitali
__TIMESTAMP
Oct 3 2017, 7:03 pm ET
__ARTICLE
 WASHINGTON  President Donald Trump spent the day in Puerto Rico on Tuesday and highlighted what he describes as the federal government's effective response to the storm despite criticism of a sluggish effort since Hurricane Maria devastated the island nearly two weeks ago. 
 Asked if he heard any criticism during his time on the island, Trump told reporters on Air Force One on the way back to Washington that it was the opposite. 
 "I think it's been a great day. We've only heard 'thank yous' from the people of Puerto Rico," he said. "Great, great visit. Really lovely." 
 Earlier, Trump, who was accompanied on the trip by first lady Melania Trump, attended a briefing with with San Juan Mayor Yulin Cruz, who has been sharply critical of Trump, and other local officials. The president shook the mayor's hand. 
 In a brief exchange Cruz, who Trump has accused of playing politics, the mayor told the president, "It's not about politics." 
 Later, while touring a neighborhood outside San Juan, Trump told a family, "Your governor and your mayor have done, really, a fantastic job." 
 At that briefing, Trump praised Puerto Rico Gov. Ricardo A. Rossell for speaking positively about the federal government's efforts on the island. 
 The governor is "not even from my party," Trump said. "Right from the beginning, this governor did not play politics. He didn't play it at all." Instead, the president said, the governor's praise was a sign that he was "saying it like it was." 
 Citing the number authorities had used for almost two week, Trump compared the "16 people" who died in Puerto Rico as a result of the storm to the "thousands" that perished after Hurricane Katrina hit New Orleans in 2005. 
 After Trump departed, Puerto Rico Gov. Ricardo Rossell updated the death toll to 34. 
 The relief efforts have come at a cost, Trump pointed out to local officials at the briefing, saying that the disaster has "thrown our budget a little out of whack" but adding that it's worth it for all of the lives saved. 
 The president also called out FEMA Administrator Brock Long, grading him "A+" for recent relief efforts in Texas and Florida. "Brock has been unbelievable," Trump said. 
 But, Trump added, Puerto Rico's recovery "has been the toughest one." 
 Trump pointed out the strength of the hurricane that hit the island  "a Category Five, which few people have ever even heard of" that he said "hit land and, boy, did it hit land." 
 Residents, many of whom have been without water, electricity, housing and basic necessities, told NBC News ahead of Trumps arrival that he has been late in coming and that the federal governments response has been slow. 
 Over several stops on Tuesday, Trump took part in handing supplies out to Puerto Ricans in need and even walked door to door in one residential neighborhood outside San Juan, greeting families who were outside their homes. While distributing food, paper towels, and batteries at the Cavalry Chapel, the president remarked, "There's a lot of love in this room." 
 While the president has lauded first responders in Puerto Rico throughout the crisis, he has also lashed out at local leaders, including Cruz, who has criticized the governments response and made public pleas to Trump to send more help. 
 Over the weekend, Trump accused Cruz of "such poor leadership" and partisan-motivated attacks against him as he charged that some in Puerto Rico want "everything to be done for them when it should be a community effort." In later tweets, he accused "politically motivated ingrates" and "fake news" of not recognizing the "amazing work" that the federal government has done so far in Puerto Rico. 
 Departing the White House earlier Tuesday, the president insisted recovery efforts were as good as those in Florida and Texas despite the "tougher situation." 
 While the administration has mobilized federal efforts to help, there has been widespread criticism about the speed and execution of government recovery efforts in Puerto Rico  with some even likening the situation to New Orleans and Hurricane Katrina under President George W. Bush. 
