__HTTP_STATUS_CODE
200
__TITLE
CVS to Limit Opioid Prescriptions to 7-Day Supply
__AUTHORS
Shamard Charles, M.D.
__TIMESTAMP
Sep 29 2017, 4:13 pm ET
__ARTICLE
 CVS Pharmacy will limit opioid prescriptions to a seven-day supply for certain conditions, becoming the first national retail chain to restrict how many pain pills doctors can give patients. 
 When filling prescription for opioid pills, pharmacists will also be required to talk to patients about the risks of addiction, secure storage of medications in the home and proper disposal, the retail pharmacy chain said Thursday. 
 The move by CVS to limit prescription opioids like OxyContin or Vicodin to a seven-day supply is a significant restriction for patients  the average pill supply given by doctors in the U.S. increased from 13 days in 2006 to 18 days 2015, according to a recent report from the Centers for Disease Control and Prevention.  
 'We are further strengthening our commitment to help providers and patients balance the need for these powerful medications with the risk of abuse and misuse, Larry J. Merlo, President and CEO, CVS Health said in a statement. 
 CVS, which manages medications for nearly 90 million customers at 9,700 retail locations, plans to roll out the initiatives to control opioid abuse as of February 1, 2018. 
 Related: One in Three Americans Took Prescription Opioid Painkillers in 2015, Survey Says 
 Daily dosage limits will be based on the strength of the opioid and CVS pharmacists will require the use of immediate-release formulations of opioids before extended-release opioids are dispensed, lowering the risk of tolerance to the highly addictive drugs. 
 Related: Opioid Prescriptions Are Down But Not Enough, CDC Finds 
 CVS  which was the first large retail pharmacy chain to stop selling cigarettes in 2014  is now hoping to help curb the opioid epidemic in the United States. One in three Americans, or 91.8 million Americans used opioid pills in 2015, according to a recent survey by the National Institute on Drug Abuse. More than 15,000 people died from prescription opioid overdose in 2015, according to the CDC. 
 With counseling on secure storage and disposal of opioid pills, the CVS pharmacists may be able to limit easy access to the pills. Five percent of adults surveyed told the National Institute on Drug Abuse researchers they took opioids without their doctors permission, often getting the prescription meds for free from friends or relatives. The CVS drug disposal collection program will expand to 1,550 units, with the addition of kiosks at 750 retail pharmacies nationwide, adding to 800 units previously donated to law enforcement. 
 In addition, CVS has pledged to increase its commitment to community health centers by bolstering contributions to medication-assisted treatment programs by $2 million. 
 The CVS announcement comes on the heels of a special publication released by the National Academy of Medicine, "First, Do No Harm," which calls on the leadership and action of doctors to help reverse the course of preventable harm and suffering from prescription opioids. 
 Simply restricting access to opioids without offering alternative pain treatments may have limited efficacy in reducing prescription opioid abuse, said representatives from NIDA. But the move is a major part of the multi-dimensional solution needed to conquer opioid abuse and a long awaited step in the right direction. 
