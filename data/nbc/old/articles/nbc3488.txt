__HTTP_STATUS_CODE
200
__TITLE
Author, Immigration Advocate Among 2017 MacArthur Fellows
__AUTHORS
Lakshmi Gandhi
__TIMESTAMP
Oct 11 2017, 12:29 pm ET
__ARTICLE
 A Pulitzer Prize-winning novelist, a New York Times journalist, a social justice activist, and several historians and computer scientists are among this years MacArthur fellows and recipients of "genius" grants. 
 Awarded by the John D. and Catherine T. MacArthur Foundation, MacArthur fellows are chosen after demonstrating extraordinary originality and dedication in their creative pursuits and a marked capacity for self-direction." Each recipient receives a no strings attached" grant, according to the foundations website. The $625,000 award is paid out over the course of five years. 
.@viet_t_nguyen is a fiction writer, cultural critic, and 2017 #MacFellow: https://t.co/2Md1wQ3TkS pic.twitter.com/Dlu2BANycq
 Viet Thanh Nguyen, whose novel about Vietnam War refugees, The Sympathizer, received the 2016 Pulitzer Prize for Fiction, was one of the fellows who received the award on Wednesday morning. 
 RELATED: Pulitzer Winner Viet Thanh Nguyen on the U.S., Vietnam, and Why History Cant Be Erased 
 In the novel, I wanted to refuse the role of the victim for the Vietnamese people or depict them solely as victims, but to depict them as people with complex subjectivities and histories, Nguyen said in 2016. 
Very few things in life leave me speechless. Getting this call did. I'm honored, grateful to hv a platform to expose scourge of segregation. https://t.co/o5WUYv1JT1
 Fellow grant recipient Nikole Hannah-Jones is a staff writer for The New York Times Magazine and a co-founder of the Ida B. Wells Society for Investigative Reporting, which is devoted to increasing the number of reporters and editors of color in journalism. Her work primarily centers on the continuing segregation of public schools in the United States. 
 Social justice organizer Cristina Jimnez Moreta is also a grant recipient. A former undocumented immigrant, she is now the executive director of United We Dream, a group that is focused on undocumented youth and their families. 
Congratulations @UNITEDWEDREAM 's Cristina Jimnez! Continue your fierce advocacy. @macfound https://t.co/5n8UGykPaR
 The complete list of 2017 MacArthur fellows is below: 
 Follow NBC Asian America on Facebook, Twitter, Instagram and Tumblr.  
