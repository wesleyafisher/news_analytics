__HTTP_STATUS_CODE
200
__TITLE
Battle for Mosul: Shiite Militias Launch Offensive to Seal Off Western Front
__AUTHORS
Stephen Kalin
Ahmed Rasheed, Reuters
__TIMESTAMP
Oct 29 2016, 8:00 am ET
__ARTICLE
 SOUTH OF MOSUL, Iraq  Iraqi Shi'ite militias said on Saturday they had launched an offensive towards the west of Mosul, an operation that would tighten the noose around ISIS' Iraq stronghold but could inflame sectarian tension in the mainly Sunni region. 
 The battle for Mosul is expected to be the biggest in the 13 years of turmoil unleashed in Iraq by the 2003 U.S.-led invasion which toppled former president Saddam Hussein, a Sunni Muslim, and brought Iraq's majority Shi'ite Muslims to power. 
 A spokesman for the Shi'ite militias, known as the Hashid Shaabi (Popular Mobilisation) forces, said thousands of fighters "started operations this morning to clean up the hotbeds of Daesh (Islamic State) in the western parts of Mosul." 
 Related: Mosul Campaign: What You Need to Know About Anti-ISIS Fight 
 The city is by far the largest held by the ultra-hardline Sunni ISIS and its loss would mark their effective defeat in Iraq, two years after their leader Abu Bakr al-Baghdadi declared a caliphate overlapping Iraq and Syria from the pulpit of a Mosul mosque. 
 The Shi'ite militias aim to capture villages west of Mosul and reach the town of Tal Afar, about 55 km (35 miles) from the city, the Hashid spokesman said. Their goal is to cut off any option of retreat by Islamic State insurgents into neighboring Syria or any reinforcement for their defense of Mosul. 
 The Iran-backed and battle-hardened paramilitaries bring additional firepower to the nearly two-week-old campaign to recapture Iraq's second largest city from the jihadist group. 
 Iraqi soldiers and security forces and Kurdish peshmerga fighters have been advancing in the last 12 days on the southern, eastern and northeastern fronts around Mosul, which remains home to 1.5 million people. 
 The United Nations has warned of a possible humanitarian crisis and a potential refugee exodus from Mosul  though the start of operations on the city's western flank could leave Mosul's civilians with no outlet to safety, even if they are able to escape Islamic State control. 
 Villagers from outlying areas around Mosul have told Reuters that women and children were being forced to walk as human shields alongside retreating ISIS fighters as they withdrew into the city this week. 
 Iraqi and Western military sources say there had been debate about whether or not to seal off Mosul's western flank. Leaving it open would have offered Islamic State a chance to retreat, potentially sparing residents from a devastating, inner-city fight to the finish. 
 Related: ISIS Forcing Civilians to be Human Shields in Mosul: Reports 
 Some civilians fleeing Mosul have also used the roads to the west to escape to Qamishli, in Kurdish-controlled northern Syria. Others, from villagers just outside Mosul, have exploited the confusion of the conflict to flee in the other direction. 
 "Some people fled the other day so we took a chance. Daesh fired two bullets at us but they missed and we made it," said Ahmed Raad, 20, from the village of Abu Jarbuaa northeast of Mosul, who had found refuge at a peshmerga base. 
 Slow progress in the south 
 Around 30 km (20 miles) south of Mosul, Iraqi rapid response forces entered the village of al-Shura, once a significant base for ISIS where the jihadists enjoyed strong support. 
 An officer told a Reuters correspondent on the southern front that most of the ISIS insurgents appeared to have pulled back north towards Mosul, leaving just a small number to try to slow the advancing security forces. 
 Air strikes were continuing in the area on Saturday morning and the sound of artillery could be heard, as well as gunfire. ISIS returned fire with machine guns and mortars. 
 Nearly two weeks into the Mosul campaign launched by Prime Minister Haider al-Abadi, the advance along the Tigris river valley south of Mosul has been slower than on the eastern front, where Kurdish peshmerga fighters and an elite army unit have reached within a few km (miles) of Mosul. 
 Saturday's announcement by the Shi'ite militias added another force to the coalition of fighters seeking to crush Islamic State in Iraq, but will also raise concerns about the role the Popular Mobilisation fighters will play. 
 Targeting the ISIS-held town of Tal Afar, close to Turkey and home to a sizable ethnic Turkmen population with historic and cultural ties to Turkey, will alarm Ankara. The Popular Mobilistion forces say many of Tal Afar's Turkmen population are Shi'ites. 
 Human rights groups have warned of possible sectarian violence if the Shi'ite paramilitaries seize areas where Sunni Muslims form a majority, which is the case in much of northern and western Iraq. 
 The militias, formed in 2014 to help push back Islamic State's sweeping advance, officially report to Abadi's Shi'ite-led government, but have close links to Iran. 
 Related: Motley Crew of Anti-ISIS Forces Is 'Dangerous Cocktail' 
 Amnesty International says that in previous campaigns, they committed "serious human rights violations, including war crimes" against civilians fleeing Islamic State-held territory. 
 In July, the United Nations said it had a list of more than 640 Sunni men and boys reportedly abducted by a Shiite militia in Falluja, a former militant bastion west of Baghdad, and about 50 others who were summarily executed or tortured to death. 
 The Abadi government and the Popular Mobilisation forces say a limited number of violations had occurred and were investigated, but they deny abuses were widespread and systematic. 
 (Additional reporting by Michael Georgy near Bashiqa; writing by Dominic Evans; editing by Mark Heinrich) 
