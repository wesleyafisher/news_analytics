__HTTP_STATUS_CODE
200
__TITLE
Challenges Pile Up for Republicans on Tax Reform
__AUTHORS
Leigh Ann Caldwell
__TIMESTAMP
Oct 11 2017, 12:25 pm ET
__ARTICLE
 WASHINGTON  The easy part is over. 
 After six months, Republican leaders have released the broad outline of their tax reform plan, a policy area that unites a party stricken with divisions and distractions that have stymied their ability to achieve a major legislative accomplishment so far this year. 
 Now comes the hard part. The outline was the first step toward a significant restructuring of the nation's tax code, but it comes later in the calendar year than expected and remains light on specifics that could become stumbling blocks to passage. 
 Most Republicans have rallied around it, hailing it as a great start. 
 But with just one month until their self-imposed deadline of Nov. 13 to release a legislative text with details, Republicans are once again facing the challenge of passing a major piece of legislation that may end up in constant danger of falling apart. 
 The stakes are even higher after a summer in which the party failed to pass health care reform, leaving lawmakers eager for an accomplishment they can take home to voters in next year's midterm elections. 
 How big are the stakes? "Essential," says Sen. Pat Roberts, R-Kan. "If we dont do tax reform, we might not be in the majority, he said. 
 Congressional Republicans are highly aware of the importance of successfully passing tax reform. Roberts' warning about losing their majorities in Congress is something members are also hearing from donors and constituents, who expect the GOP to get something done on taxes. 
 As tax reform efforts pick up steam in the coming weeks, an army of lobbyists and special interest groups are preparing to deploy inside the halls of Congress, and members will inevitably be looking out for their own constituents, ingredients that make political calculations unpredictable. GOP leaders, including President Donald Trump, tried their hardest to put political pressure on individual members during the health care debates but were unable to put together the 50 votes needed to pass it in the Senate. 
 Adding to the mounting political and policy challenges as Republicans try to piece together tax legislation is a growing intra-party spat between President Donald Trump, his supporters and GOP members that Republicans on Capitol Hill say could threaten their success. 
 The very public spat between President Trump and Sen. Bob Corker, R-Tenn., is the most recent internal feud that has roiled the party, and put tax reform supporters on edge. Trump has downplayed its eventual impact on tax reform, saying it won't factor in on the final votes. 
 I don't think so. I think we're well on the way, Trump said on Tuesday. "The people of this country want tax cuts. They want lower taxes. We're the highest taxed nation in the world." 
 But Republicans on Capitol Hill say that a distracted president doesn't help the difficult task at hand and that attacking a critical member of the Senate is no way to build a coalition. 
 And Corker, a key player in the tax debate, has little to lose. He is retiring at the end of this term and has said that hes not an automatic yes on any tax legislation, demanding that a bill be revenue neutral. 
 Im not voting for a bill that produces a penny of deficit, Corker told NBC News recently. 
 And the Trump-Corker episode is just the most recent for the president. Trump has publicly criticized other key congressional Republicans, like Senate Majority Leader Mitch McConnell, Sen. John McCain of Arizona and Speaker Paul Ryan, just to name a few. 
 Republicans on the Hill are routinely frustrated that Trumps public feuds with members of his own party or his off-topic rants are interfering in the legislative progress. Aides privately sigh about the president's unpredictability and his penchant for steering the conversation away from Republican priorities. 
 Those fights don't even address the actual policy challenges tax reform faces. Corkers legislative concern is one shared by fellow fiscal hawks who dont want the estimated $1.5 trillion worth of tax cuts to blow a hole in the budget. And Republicans in the House are much more concerned with the impact on the budget than their colleagues in the Senate, making it a difficult issue to reconcile. 
 Rep. Mark Walker, R-N.C., chairman of the 150-member conservative Republican Study Committee, said in a recent interview that he would struggle with voting for any tax reform that adds to the deficit. 
 It would be inconsistent for us not to have a problem, Walker said of his fellow conservative House members. 
 While Walker would prefer a tax bill that doesnt add to the deficit, he didnt draw a line in the sand, allowing for an opening. 
 Another major challenge facing Republicans is pitting red-state Republicans against blue-state Republicans. While it didnt specifically state it, the GOP framework would eliminate the tax deduction enjoyed by residents of high-tax states who can deduct their state and local taxes from their tax bill. Its a deduction that benefits higher-income residents in New Jersey, New York, California and other higher taxed states, and Republicans from those states have come out against the plan for that reason. 
 Republicans are discussing a range of options, including keeping the deduction or a compromise such as phasing it out or capping it so people below a certain income can still benefit from it. 
 We have to respect the fact that the people that provide our majority reside in these states, and thats already very difficult territory for Republicans," said Rep. Tom Cole, R-Okla. "You dont want to weaken your most vulnerable members by asking them to vote for something thats an anathema to their folks at home." 
 While the impact on the budget and the state and local tax deduction are the two biggest issues facing the party right now, the challenges will grow as more details emerge. A common refrain around the halls of Capitol Hill is that every line in the tax code has a lobbyist fighting to preserve it. 
 Lawmakers say that the lobbyists have been pretty quiet so far, but they expect that to change as details emerge. 
 You have to deal with people coming here  theyre doing their jobs  but if we spend too much time trying to accommodate this exemption and not look at the broader package, thats where you start losing votes and thats when you start eroding impact that you can have on the package, said Sen. Thom Tillis, R-N.C. 
 Some political organizations are already spending millions to help tax reform pass. The super PAC associated with Ryan, the American Action Network, has launched millions of dollars worth of television ads in targeted congressional districts promoting tax reform. 
 And the Charles and David Koch-backed group, Freedom Partners Chamber of Commerce, has made tax reform its No. 1 issue after the defeat of Obamacare repeal. They unveiled a six-figure ad and a website on Wednesday to name the industry groups that are lobbying to keep specific tax provisions. 
 "We want to make it clear that we aren't going to sit idly by and let crony politicians, businesses and lobbyists jeopardize it," James Davis, Freedom Partners' executive vice president, said of tax reform. "Americans deserve a tax code that puts their interests ahead of special interests, and shining a light on this process is the best way to ensure they get one." 
 As the challenges mount, Republicans are hopeful for party cohesion. 
 "You can have all the most beautiful wonderful goals in the world but if were not able to check the box and deliver for the American people ... What does it mean for all of us?" said Walker, the conservative House leader. 
 The need for a legislative victory could overcome all of the Republicans' concerns, since failure would risk their majority in Congress and their re-election chances. 
 This one needs to happen, Cole said of tax reform. If we dont achieve this, then I think the electoral consequences will be dire indeed. 
