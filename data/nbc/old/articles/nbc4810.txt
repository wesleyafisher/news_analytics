__HTTP_STATUS_CODE
200
__TITLE
Worried About Elder Financial Abuse? How to Protect Your Parents
__AUTHORS
Sharon Epperson, CNBC
__TIMESTAMP
Apr 20 2016, 9:51 am ET
__ARTICLE
 As your parents get older, roles sometimes reverse. You may find you now need to care for your aging parents, especially when it comes to managing and protecting their finances. 
 Elder financial abuse is a growing concern among seniors and their adult children. Sometimes family members and caretakers are the culprits. Just one financial scam can cost the average senior about $30,000  a big hit for any nest egg. So how can you protect your parents' finances? Here are five ways to help. 
 It's often not an easy conversation to have, but it needs to happen  as soon as possible. 
 Ask your parents where they keep their money. Discuss when they would want you to step in and manage their day-to-day finances. To prepare, find out where they keep important paperwork, including wills and deeds, and who their financial, legal and tax professionals are. If they do not have a financial adviser, lawyer or accountant, talk to them about finding professionals who can work with all of you. 
 Read More from CNBC: Are You on Track for Retirement? 
 There may be signs that they are losing track of their finances. Keep an eye out for piled-up bills and notices from creditors. Check their bank account for suspicious activity, including significant withdrawals or unusual purchases. Also look for suspicious changes in their wills or powers of attorney. 
 Predators love to target the elderly, and they do it mostly over the phone. Educate your parents about potential scam tactics. Tell them to never give out money or information over the telephone. 
 Many scams begin with someone calling to inform you that you won something, like a lottery or sweepstakes. Another popular scam is someone posing from the IRS threatening to initiate legal proceedings for past-due balances. 
 Read More from CNBC: Early Retirement Might Be in Your Future 
 To avoid unsolicited calls, encourage your parents to sign up for the Do Not Call Registry and the Direct Marketing Association's service to reduce junk mail. Remind them to never share their Social Security or Medicare numbers, and explain that the IRS only contacts individuals via regular mail for tax matters. 
 Tell them to never sign a document that they don't understand. Have a trusted and unbiased professional assist your parents when they are entering contracts, signing legal documents, as well as making investment decisions. 
 The National Center on Elder Abuse suggests signing up for AARP's Fraud Watch, checking out AARP's interactive national fraud map and looking into resources offered by the Financial Fraud Enforcement Task Force. 
