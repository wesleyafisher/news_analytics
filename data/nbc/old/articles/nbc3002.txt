__HTTP_STATUS_CODE
200
__TITLE
Fracking May Worsen Asthma in People Living Nearby
__AUTHORS
Associated Press
__TIMESTAMP
Jul 18 2016, 5:26 pm ET
__ARTICLE
 Fracking may worsen asthma in children and adults who live near sites where the oil and gas drilling method is used, according to an 8-year study in Pennsylvania. 
 The study found that asthma treatments were as much as four times more common in patients living closer to areas with more or bigger active wells than those living far away. 
 But the study did not establish that fracking directly caused or worsened asthma. There's also no way to tell from the study whether asthma patients exposed to fracking fare worse than those exposed to more traditional gas drilling methods or to other industrial activities. 
 Fracking refers to hydraulic fracturing, a technique for extracting oil and gas by injecting water, sand and chemicals into wells at high pressure to crack rock. Environmental effects include exhaust, dust and noise from heavy truck traffic transporting water and other materials, and from drilling rigs and compressors. Fracking and improved drilling methods led to a boom in production of oil and gas in several U.S. states, including Pennsylvania, North Dakota, Oklahoma, Texas and Colorado. 
 Related: Rising Levels of Gas Found Near Fracking Sites 
 Sara Rasmussen, the study's lead author and a researcher at Johns Hopkins University's Bloomberg School of Public Health, said pollution and stress from the noise caused by fracking might explain the results. But the authors emphasized that the study doesn't prove what caused patients' symptoms. 
 More than 25 million U.S. adults and children have asthma, a disease that narrows airways in the lungs. Symptoms include wheezing, breathing difficulties and chest tightness, and they can sometimes flare up with exposure to dust, air pollution and stress. 
 Previous research has found heavy air pollution in areas where oil and gas drilling is booming. 
 Industry groups responding to the new research said air samplings measured by Pennsylvania authorities near natural gas operations during some of the study years found pollutant levels unlikely to cause health issues. But samplings were limited and didn't reflect potential cumulative effects of emissions from the drilling sites. 
 The new study was published Monday in JAMA Internal Medicine. 
 The researchers noted that between 2005 and 2012, more than 6,200 fracking wells were drilled in Pennsylvania. They used electronic health records to identify almost 36,000 asthma patients treated during that time in the Geisinger Health System, which covers more than 40 counties in Pennsylvania. Evidence of asthma attacks included new prescriptions for steroid medicines, emergency-room treatment for asthma and asthma hospitalizations. 
 Related: EPA Says Fracking Has No Widespread Effect on Drinking Water 
 During the study, there were more than 20,000 new oral steroid prescriptions ordered, almost 5,000 asthma hospitalizations and almost 2,000 ER asthma visits. 
 Those outcomes were 50 percent to four times more common in asthma patients living closer to areas with more or bigger active wells than among those living far away. 
 The highest risk for asthma attacks occurred in people living a median of about 12 miles from drilled wells. The lowest risk was for people living a median of about 40 miles away. 
 Dr. Norman H. Edelman, senior scientific adviser for the American Lung Association, called the study "interesting and provocative." But he said it only shows an association between fracking and asthma, not a "cause and effect," and that more rigorous research is needed. 
 "Asthma is a huge problem," he said. "Anything we can do to elucidate the causes will be very useful." 
