__HTTP_STATUS_CODE
200
__TITLE
Up to 9 Million Floridians Could Be Without Power After Irma, Some for Weeks
__AUTHORS
Elizabeth Chuck
__TIMESTAMP
Sep 8 2017, 4:09 pm ET
__ARTICLE
 Utility officials are predicting up to 9 million Floridians could be without power after Irma strikes, and are weighing drastic measures before the storm  including shuttering both of the state's working nuclear plants. 
 Florida Power & Light (FPL), the nation's third-largest electric utility, warned on Friday that Irma, a Category 4 hurricane, could cut power for weeks. 
 "We are looking at outages that are probably going to impact 4.1 million of our customers. That's about 9 million people," the utility's president and CEO Eric Silagy told reporters Friday. "That would be unprecedented for us." 
 The utility has already positioned equipment and workers to help bring electricity back up and running as fast as possible, but fears the force of Irma's winds may snap concrete poles, Silagy said. 
 "We're preparing for the worst. With these kinds of winds, we're not looking for restoration," he said. "In some parts of our territory, we will be looking at having to rebuild it." 
 Related: Irma Threatens To Wash Away Florida Buildings After Tearing Through Caribbean 
 FPL, which runs the two nuclear plants, was closely watching the forecast and if hurricane force winds were predicted to hit either plant, "we will shut down those plants 24 hours before the onset," Silagy said. 
 One of the facilities, the Turkey Point Nuclear Generating Station in Homestead, weathered "a direct hit" from Hurricane Andrew 25 years ago, Silagy said. The plant suffered millions of dollars in damage, but remained functional. Homestead is about 40 miles south of Miami. 
 The other facility, in St. Lucie, is more than 130 miles north of Miami. 
 "These are probably the most highly engineered and strongest facilities of any kind in the world," Silagy said. "We're not taking any chances." 
 Shutting down the nuclear plants would not affect power to Floridians served by FPL. But Silagy cautioned that Irma was forecast to be so damaging, utility officials anticipated that outages "will not be measured in days, but weeks." 
 Related: Zoos Hunker Down as Hurricane Irma Takes Aim 
 "I want to be very clear about the level of damage that's possible with a storm of this kind of magnitude," Silagy said. "This is a storm that's been unprecedented in both strength and in size so far, and we're hoping that the winds diminish before it makes landfall." 
 Irma strengthened to a Category 5 hurricane by 2 a.m. Saturday ET and was moving over Cuba's Camaguey Archipelago, after earlier weakening slightly to a Category 4. The storm was about 275 miles southeast of Miami and had sustained winds of 160 mph. 
 At least 23 people were killed after the hurricane swept through parts of the Caribbean this week. It is forecast to be near the Florida Keys Sunday morning and near the southwestern coast of Florida by Sunday afternoon. 
 The storm is the most powerful Atlantic hurricane on record. 
