__HTTP_STATUS_CODE
200
__TITLE
Iraqi Forces Approach Mosul Amid Warning Battle Wont Be Picnic
__AUTHORS
Reuters
__TIMESTAMP
Oct 31 2016, 8:59 am ET
__ARTICLE
 An elite Iraqi army unit advanced towards the built-up area of the ISIS stronghold of Mosul on Monday after two weeks of fighting to clear surrounding areas of the insurgents. 
 Military commanders said that the U.S.-backed offensive to recapture Mosul  the largest military operation in Iraq since the U.S.-led invasion that toppled Saddam Hussein in 2003  could still take weeks and possibly months. 
 "The battle of Mosul will not be a picnic," Hadi al-Amiri, leader of the Badr Organisation, the largest Shiite militia fighting with Iraqi government forces, said from the southern frontline. "We are prepared for the battle of Mosul even if it lasts for months." 
 Related: Defeating ISIS in Mosul Will Open Door to Many Challenges 
 Troops from the Counter Terrorism Service (CTS) were moving in on Gogjali, an industrial zone on Mosul's eastern outskirts, and could enter it later on Monday, an officer from the U.S.-trained unit told a Reuters correspondent just east of Mosul. 
 The zone lies under a mile from the administrative border of Mosul. 
 The capture of Mosul would mark the militants' effective defeat in the Iraqi half of the caliphate that ISIS leader Abu Bakr al-Baghdadi declared two years ago from the city's Grand Mosque. 
 It is still home to 1.5 million residents, making it four of five times bigger than any other city they seized in both Iraq and Syria. 
 ISIS militants has been fighting off the offensive with suicide car bombs, snipers and mortar fire. 
 They have also set oil wells on fire to cover their movements and displaced thousands of civilians from villages toward Mosul, using them as "human shields," U.N. officials and villagers have said. 
 
