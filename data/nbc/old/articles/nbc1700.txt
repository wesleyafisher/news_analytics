__HTTP_STATUS_CODE
200
__TITLE
So Your Boss Is Offering You a Bonus! Why Thats Not Always a Good Sign
__AUTHORS
Martha C. White
__TIMESTAMP
Sep 20 2017, 1:49 pm ET
__ARTICLE
 More bad news for American workers: Not only are they looking at another year of minimal raises, but companies are also cutting back on bonuses for the rank-and-file to more lavishly reward top performers. 
 A post-recession shift away from fixed raises to bonuses and other variable pay helped companies keep their fixed costs down. This can be beneficial for workers in that it reduces the risks of mass layoffs, since companies can curtail bonuses if business slows, but it leaves employees more exposed to macroeconomic vagaries, according to Andy Challenger, vice president at Challenger, Gray & Christmas. 
 When employees have this performance-based variable pay, theyre just not as protected from large economic cycles than they were previously. But it allows companies to weather the storm, he told NBC News. 
 Aon Hewitts 2017 U.S. Salary Increase Survey finds that companies expect to give an average of 3 percent raises, a number that has stayed roughly the same for the past six years. 
 In spite of a strengthening economy and increased job creation we just arent seeing any impact from that in terms of company spending on raises, said Ken Abosch, broad-based compensation leader at Aon Hewitt. 
 For the past few years, the silver lining for workers facing minimal salary increases has been growth in companies budget for variable pay, but that amount has fallen to the lowest level since 2013, at 12.5 percent of payroll costs. 
 Related: How to Negotiate a Higher Salary 
 Even as the pot of variable pay money dwindles, a growing number of companies today are using performance-based metrics to dole out both salaries and bonuses. Aon Hewitt found that 40 percent have reduced or entirely done away with pay increases for lower-performing employees, and 15 percent have ramped up their performance targets. 
 In general, organizations are trying to make efforts to direct the compensation dollars they have at their highest performers, Abosch said. If youre an elite performer, youre going to be taken care of at most organizations. 
 In response to the growing competitiveness of the labor market, PayScale found that 22 percent of companies gave retention bonuses this year, up from just 8 percent in 2014. The percentage giving hiring bonuses soared to 27 percent this year from 8 percent in 2014. 
 But the flip side, for the vast majority of workers who are good but not great, is that they might see raises as well as bonuses dwindle, or disappear entirely. 
 Companies are doing a better job differentiating salary increases for higher performers and lower performers, said John Bremen, a managing director at Willis Towers Watson. Its not uncommon for companies to give lot of 0 percent increases to fund higher increases for star employees, he said. I think theyre finding they need to do everything in their power to retain those high performers, even if that means they have to give zeroes to some. 
 What companies are trying to do is increase differentiation of their bonus payouts, trying to pay their top performers even more, said Brian Kropp, HR practice leader at CEB, now Gartner. That money has to come from somewhere. It means the average employee is going to get an even smaller bonus. 
 Workers today see a strong economy with a booming stock market and healthy corporate profits, and they expect to be compensated accordingly. In 2017, employees are expecting their bonus to be about 1 percent higher than it was last year, Kropp said. Its a real problem, which is that if companies are going to cut bonuses and employees are expecting a bigger bonus, theres a huge mismatch. 
 Related: Wage Growth Is Still Behind the Curve 
 In more competitive fields like technology, where PayScale found that nearly 40 percent of companies give hiring bonuses, this could lead to increased turnover, Kropp said, and even if workers stay put, morale will suffer. You risk potentially disengaging a huge segment  your core performers, your people that get stuff done, he warned. Theyre just not going to work as hard. 
 In terms of engagement, theres no question, Bremen said, that doling out skimpier bonuses to a larger swath of workers will hurt. At a time like this, companies need all of their workers engaged, he said. 
 This short-sightedness could cost companies in the long run, Kropp said. Youre saving on a hard cost, but youre creating a soft cost in the lack of employee productivity. 
