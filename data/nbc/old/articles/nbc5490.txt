__HTTP_STATUS_CODE
200
__TITLE
Facebook, Google Halt Potential Ads Targeting Anti-Semitic, Hateful Content
__AUTHORS
Erik Ortiz
__TIMESTAMP
Sep 15 2017, 3:40 pm ET
__ARTICLE
 Facebook and Google are taking aim at advertisers by disabling their abilities to pinpoint users interested in anti-Semitic subjects and other hate-filled content. 
 Facebook's restriction comes after news organization ProPublica said Thursday it paid $30 to have its articles specifically promoted to users who expressed interest in the topics "Jew hater," "How to burn jews" or "History of 'why jews ruin the world.'" 
 It reportedly took Facebook no more than 15 minutes to approve all three of the targeted ads. 
 ProPublica said the anti-Semitic categories were created by an algorithm and they were removed by Facebook after the company was made aware. The media outlet added that its test exposed that such anti-Semitic targeting could happen, although it was not clear if it was actually occurring. 
 Facebook, as part of its review, noted in a post Thursday that it saw a "small percentage" of users entering "offensive responses" in their education or employer fields on their profiles. That allowed ProPublica to go after such users with anti-Semitic subjects  not through an algorithm. 
 "ProPublica surfaced that these offensive education and employer fields were showing up in our ads interface as targetable audiences for campaigns. We immediately removed them," Facebook said. "Given that the number of people in these segments was incredibly low, an extremely small number of people were targeted in these campaigns." 
 A Facebook executive reiterated in a statement that hate speech is not permitted on the platform and it bars advertisers from discriminating against people based on religion and other attributes. 
 Related: Facebook Is Hiring 3,000 More People to Keep the Network in Check 
 "However, there are times where content is surfaced on our platform that violates our standards. In this case, we've removed the associated targeting fields in question," said Rob Leathern, Facebook's product management director. "We know we have more work to do, so we're also building new guardrails in our product and review processes to prevent other issues like this from happening in the future." 
 In a follow-up statement, Facebook added that it had removed self-reported targeting fields in user profiles "until we have the right processes in place to help prevent this issue.We want Facebook to be a safe place for people and businesses, and we'll continue to do everything we can to keep hate off Facebook." 
 ProPublica said it was able to direct its Facebook ads to almost 2,300 people who expressed interest in certain anti-Semitic categories, but also widened its audience to include those who mentioned the "Schutzstaffel," a paramilitary organization under Adolf Hitler and the "NaziParty." 
This is what automated anti-semitism looks like: pic.twitter.com/DdoZoxfLxP
 When the news outlet looked for analogous advertising categories for other religions, such as "Muslim haters," it said "Facebook didn't have them." 
 In a similar move, Google announced Friday it had "turned off" potentially offensive wordings in its keyword suggestions tool, which is made available to advertisers who want to reach a certain audience. 
 Sridhar Ramaswamy, Google's senior vice president of ads, said in a statement that it had previously rejected certain keywords, but "we didn't catch all these offensive suggestions. That's not good enough and were not making excuses." 
 BuzzFeed News first reported Friday that Google's ad platform  the world's largest of its kind  had automatically provided keyword phrases such as "black people ruin neighborhoods," "the evil jew" and "jewish control of banks." 
 But just because a keyword is suggested to an advertiser doesn't mean a targeted ad will be approved, and Google removes ads that violate its terms of service. 
 Related: Russian Operation Spent $100K on Issues Ads During 2016 Campaign 
 The search giant has previously changed its algorithm after people spoke out about how typing in certain racist and anti-Semitic phrases in the search bar resulted in autopopulated offensive terms. 
 Facebook ads were under scrutiny earlier this month as well after the company told Washington lawmakers investigating alleged Russian meddling in last year's election it found a Russian firm had spent $100,000 on targeted ads. 
 While those ads didn't prop up any specific candidate, they focused on polarizing social issues, including gay rights, immigration and race, Facebook's chief security officer said in a post. 
 Earlier this year, Facebook CEO Mark Zuckerberg said the company was hiring about 3,000 people to combat the problem of harmful content on the popular site, including shutting down hate speech and child exploitation. The announcement came after users posted controversial content with its new live-streaming tool. 
