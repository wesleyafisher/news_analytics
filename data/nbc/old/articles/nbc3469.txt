__HTTP_STATUS_CODE
200
__TITLE
Tech Gift Guide: The Best Gifts for Gamers
__AUTHORS
Andrea Smith
__TIMESTAMP
Dec 16 2016, 2:13 pm ET
__ARTICLE
 If youre waiting for a great time to buy gaming and entertainment devices, this is it. The holiday season bring deep discounts on everything from 4K TVs to bundled gaming consoles to wireless headphones and all the other tech thats on the holiday wish list. 
 Weve rounded up some tech gift ideas sure to please everyone  including maybe yourself! 
 Smart TV, from about $700 
 Top of many familys holiday wish list is a new TV, so make sure to get one with the latest 4K UHD picture quality as well as brains. 
 Samsungs KU7000 4K UHD Smart TVs range in size from 40 to 65 and deliver stunning 4K resolution with its High Dynamic Range (HDR) Premium technology. That means youll get the brightest picture and enhanced contrast. 
 Its also a smart TV with built-in Wi-Fi so you can quickly and easily switch between watching live TV, streaming video, gaming, and other apps using just one remote. It has Netflix and Amazon video built right in so theres no need for a separate Roku or streaming media box. 
 Blu-ray Player, about $280 
 If youve got the 4K UHD TV and are just waiting for awesome content to watch, Samsungs UBD-K8500 Blu-ray player will enhance the viewing experience. Its a 4K Ultra HD Blu-ray player with HDR technology, so youll see those deeper colors and brighter contrasts in your content. 
 It plays the latest 4K Blu-ray HDR discs in amazingly realistic quality, but will also upscale older, lower-resolution DVDs during playback. It also has Netflix, Amazon, and plenty of other streaming apps built right in. 
 Noise-Canceling Headphones, about $350 
 For those looking for peace and quiet away from the noise of travelers or family gatherings, the Bose QuietControl 35 Noise-Canceling Wireless Headphones are just the thing. Bose put its active noise-canceling technology into a pair of wireless Bluetooth headphones with a battery that lasts up to 20 hours. Thats great news for iPhone 7 users looking to use Bluetooth to connect to their phones. 
 The around-the-ear style earcups are comfortable enough to wear on long plane rides, with volume, music and Bluetooth pairing controls located below the earcup. Long battery life, a great fit and the ability to block out noise make these headphones a must-have for music lovers. 
 Streaming Media Player, about $30 
 For the TV watcher on your list who isnt streaming video yet, the new Roku Express is an easy-to-set up, affordable gift for first-time users. The Express model is for people with an HDMI port on their TV and offers the intuitive interface, a simple remote and HD video from popular channels and services including Amazon Prime Video, Netflix, HBO and more. 
 If your recipient doesnt have an HDMI port, for $10 more you can get the Express+, which comes with that old familiar composite A/V cable. 
 Long-time streamers whove upgraded to 4K TV will love the Roku Premiere edition, boasting a quad-core processor and the ability to stream 4K UHD video. 
 Pocket Projector, about $150 
 If you know someone who likes to show off their holiday pictures and videos to friends and family, get the Pocket Projector Micro from Brookstone. It connects to devices like smartphones, tablets and computers via an HDMI compatible cable and projects up to a 50 image on any wall. 
 Its great for displaying vacation pictures or having a spontaneous movie night. As the name implies, its pocket-sized, so easy enough to take on the road or move from room to room. The battery lasts about two hours before needing recharging. 
 Classic Video Gaming, about $60 
 If youre of a certain age, chances are you grew up playing Nintendo. Give the gift of nostalgia with Nintendos NES Classic Edition console. This is a mini version of the original cartridge-based system, and comes pre-loaded with 30 classic faves like Donkey Kong, Galaga, and Super Mario Bros. 
 Its a modern take on the retro 8-bit game system, complete with HDMI video support so you can hook it up to the big TV and play Zelda all day. It comes with one NES Classic controller but you can add another and hey, its a great chance to show the kids you were cool once, too. 
 Next Gen Video Gaming, about $300 
 If Call of Duty is more their thing, Sonys new PS4 Slim is a slimmed-down, (by 40 percent) updated version of the original gaming system, with new physical power and eject buttons that are easier to use and a quieter overall operation. 
 The new design makes it easier to stand on its end or hide away in a cabinet. The PS4 Pro model has support for 4K video and HDR output if your TV has that functionality. 
 Gaming Accessories, about $100 
 The Logitech G403 Prodigy Wireless gaming mouse has a classic design and familiar feel, but most of all we like that its not overloaded with buttons: great for a less than hard core gamer. The real appeal is the 2.4GHz wireless connection and gaming speed, meaning theres no lag. Using Logitechs Gaming Software, users can personalize the six programmable buttons, change the illumination color, and monitor the battery life. Game on! 
 For the Xbox gamer in your life, give the gift of control. The Xbox Design Lab lets users customize and design their own wireless gaming controller. There are millions of color combinations to choose from in addition to personalized engraving. If you know their color preferences, create one as a gift or send a digital code to your favorite gamer. 
 Family Gaming, about $90 
 For the family that plays together, Lego Dimensions is a building and gaming adventure that will keep family game night going for days. 
 The LEGO Dimensions Starter Pack comes with all you need; including building bricks, a Lego Dimensions videogame, a toy pad and 3 Lego minifigures. Once kids build a base, they put their minifigures on the toy pad and watch them magically enter the gaming world. From there they journey through the adventure, fighting to save Lego Worlds, mixing and matching unlikely allies from different universes, like Batman and Gandalf. Because who doesnt want to see Gandalf riding in a Batmobile? 
