__HTTP_STATUS_CODE
200
__TITLE
Anheuser-Busch Sends 51K Cans of Water to Firefighters in West
__AUTHORS
Elisha Fieldstadt
__TIMESTAMP
Aug 24 2015, 8:35 pm ET
__ARTICLE
 Firefighters battling a spate of wildfires in the West will get a delivery from beer maker Anheuser-Busch this week, but the 12 ounce cans wont be filled with brew, theyll be filled with water. 
 The maker of Budweiser, Stella Artois, Becks and more is sending 2,156 cases of emergency drinking water  51,744 cans  to Washington state in the coming days, Anheuser-Busch said in a statement Monday. The American Red Cross and Chelan County Public Works will distribute the cans to "communities most in need" in the Pacific Northwest, the statement said. 
 About 1,250 people are working to extinguish 16 wildfires in Washington alone, while California, Oregon, Idaho, Montana and Colorado officials struggle to allocate limited resources as blazes ravage parts of those states. 
UPDATE: More than 700 @WANationalGuard members are serving on multiple #WAWildfires (1/4) pic.twitter.com/THZDX9YfET
 Firefighters and relief workers are in need of safe, clean drinking water, and Anheuser-Busch is in a unique position to produce and ship emergency drinking water, said Peter Kraemer, vice president of Supply for Anheuser-Busch. 
 Related: 'Kind of Intoxicating': Wildfire Crews Find Beauty Amid the Flames 
 In May, Anheuser-Busch completely halted beer production at its Georgia brewery in order to produce drinking water for those displaced by heavy storms and flooding in Texas and Oklahoma. 
CA Fire Summary for 8/24 - Over 11,300 firefighters are battling 16 wildfires across CA. http://t.co/yiwYNlZZOJ pic.twitter.com/9FTjsA5c10
 
