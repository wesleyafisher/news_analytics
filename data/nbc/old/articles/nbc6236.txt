__HTTP_STATUS_CODE
200
__TITLE
CES 2017: As Robots Learn to Become More Human, Are We More Robotic?
__AUTHORS
Alyssa Newcomb
__TIMESTAMP
Jan 5 2017, 7:41 am ET
__ARTICLE
 They. Are. Robots. But. They. Don't. Talk. Like. This. 
 The halting speech and stiff movements seen in robots of the past, whether in movies, on television and in real life, is now largely history. 
 We're now entering an era where robots are seemingly human and friendly with their responses  as with the artificially intelligent Amazon Echo and Google Home or Apple's Siri. But are humans equally friendly in their responses? 
 "There are many ways one can interact with a robot, [whether it's] a smart watch, phone, tablet and even a PC," Patrick Moorhead, principal analyst at Moor Insights & Strategy, told NBC News. "Across all of those, the most direct way to do this would be voice. But to be a good experience, it needs to be pretty accurate." 
 SoftBank Robotics introduced its humanoid, Pepper, in Japan nearly three years ago. Since then she's been a friendly helper in various public venues and recently crossed the Pacific to begin taking over in the United States, beginning with shopping malls in the Bay Area. 
 "Technology has moved to the point where we have reverted to one of the original consumer behaviors, which is talking to each other," Steve Carlin, general manager and vice president of business development for the Americas at SoftBank Robotics, told NBC News. "But now we're talking to our devices and our technology in a really seamless way." 
 And that power has now been put in the hands of developers. 
 Since Pepper's introduction, SoftBank has opened a number of developer's kits, most recently Android in May of last year, allowing developers to let their imaginations run wild creating new experiences for Pepper. 
 At the Consumer Electronics Show in Las Vegas, NBC News was able to check out some of the different experiences that have been developed for Pepper, who is now, by the way, a true polyglot, fluent in 20 languages. 
 "We're really excited to see what other people can do," Omar Abdelwahed, head of studio at SoftBank Robotics, told NBC News. "As many resources as we have internally, it will not compare to the mass of developers in the world." 
 SoftBank takes care of the robotics part, including the human-like gesticulations Pepper has when she talks, and her ability to actually read the emotions of the person with whom she's engaging. 
 Everything else is largely left up to developers, who can choose if they want to integrate these features into their Pepper experience. While Pepper is already hard at work providing help and customer service in stores around the world, SoftBank also sees a place for her in the home. 
 With an army of Peppers spread out throughout a Las Vegas suite, NBC News was able to check out a variety of experiences. 
 Related: The Biggest Tech Trends to Look For at CES 2017 
 At the bar, Pepper was set up to ask a series of questions, allowing her to recommend the perfect beverage. She could then go a step further by communicating with a smart bar to dispense the perfect libation. 
 Pepper can be the dealer in "Cards Against Humanity," reading the cards and connecting with players through the cloud. Another Pepper gave Las Vegas recommendations via Yelp's platform. 
 But party mode Pepper may be the most fun. She dances and helps you compose your own song through a virtual interaction. After the song is recorded, Pepper is able to connect to Sonos speakers and use colored smart lights to play your song and put on a show. 
 While "the train has already left the station," when it comes to consumer robots, Carlin said, developers will be crucial to pushing the limits of personal robotics. And of course, much of the public will need to get acquainted with their new robot pals to the point where eventually, talking to any robot is just second nature. 
 "Like all new technology, there will be a lot of experimentation," Moorhead said. "And over time, I think some consumers will be more comfortable using a robot in retail and restaurant environments." 
