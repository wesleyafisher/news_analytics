__HTTP_STATUS_CODE
200
__TITLE
Facebooks Oculus Is Having a Rough Start to 2017
__AUTHORS
Alyssa Newcomb
__TIMESTAMP
Feb 10 2017, 2:55 pm ET
__ARTICLE
 Facebook's virtual reality company, Oculus, isn't having the best start to 2017. 
 First, the company lost a $500 million lawsuit stemming from allegations of intellectual property theft. And now, there are reports that as many as 200 Oculus demo stations will be closing at Best Buy. 
 While an Oculus representative said the closures were due to "seasonal changes," Business Insider reported an internal memo from a third-party contractor that said the changes were due to "store performance." 
 "Were making some seasonal changes and prioritizing demos at hundreds of Best Buy locations in larger markets. You can still request Rift demos at hundreds of Best Buy stores in the U.S. and Canada," Oculus spokeswoman Andrea Schubert told NBC News. 
 Related: Facebook to Pay $500 Million Over Virtual Reality Lawsuit 
 "We still believe the best way to learn about VR is through a live demo. Were going to find opportunities to do regular events and pop-ups in retail locations and local communities throughout the year," she said. Oculus still has 300 demo locations in Best Buy and various other opportunities around the U.S. 
 Patrick Moorhead, principal analyst at Moor Insights & Strategy, said the shift doesn't necessarily mean there is a lack of interest. 
 "VR is in its infancy and you can segment it today into mobile, PC, and console," he said. Since Oculus is PC-based virtual reality, it is "the most real but the hardest to set up." 
 "I dont necessarily see this as evidence that consumers arent interested in PC VR," said Moorhead. "I see this as Best Buy not necessarily being the best place to show off PC VR in its current state." 
 "We're always testing, trying and changing things in our stores to give our customers the best opportunity to experience and shop for technology," Best Buy told NBC News. "We'll continue to have Oculus demos at hundreds of stores across the country, and stores that no longer have demos will continue to have Rift headsets and Touch controllers available for purchase." 
 While the shift may reflect the growing pains that come with an industry still in its infancy, Oculus faced a setback earlier this month when it lost a $500 million lawsuit over allegations of intellectual property theft. 
 Facebook CEO Mark Zuckerberg testified during the trial that he was not aware of intellectual property claims against Oculus when Facebook purchased the startup for $2 billion in 2014. 
 At issue was whether Oculus founder Palmer Luckey "commercially exploited" code and trade secrets from video game maker ZeniMax to build his product. 
 According to the lawsuit, the Oculus Rift headset was "primitive" until John Carmack  who founded a company owned by ZeniMax  began working with Oculus, where he was allegedly able to improve on the headset by using knowledge from his previous work under ZeniMax. 
 Sheryl Sandberg, Facebook's chief operating officer, told CNBC she was "disappointed in certain elements of the decision." She said Facebook was "considering our options to appeal," and the verdict was "not material to our financials." 
 While both incidents haven't made for the smoothest start to 2017, there is one big positive on the horizon for Oculus. Mark Zuckerberg recently announced he was hiring Hugo Barra, a former Google and Xiaomi executive, to head up virtual reality at Facebook. 
 "Hugo shares my belief that virtual and augmented reality will be the next major computing platform," Zuckerberg said last month. "They'll enable us to experience completely new things and be more creative than ever before. Hugo is going to help build that future, and I'm looking forward to having him on our team." 
 Zuckerberg also shared his excitement on Thursday, posting new photos of the technology being developed inside Oculus' research lab in Redmond, Washington. 
 "The goal is to make VR and AR what we all want it to be: glasses small enough to take anywhere, software that lets you experience anything, and technology that lets you interact with the virtual world just like you do with the physical one," he wrote. 
