__HTTP_STATUS_CODE
200
__TITLE
Monty Hall, Host of 'Let's Make A Deal,' Dies at 96
__AUTHORS
Variety
__TIMESTAMP
Sep 30 2017, 6:55 pm ET
__ARTICLE
 LOS ANGELES  Monty Hall, who co-created and hosted the game show "Let's Make A Deal," died as a result of heart failure Saturday in his home in Beverly Hills, Calif. He was 96. 
 "Let's Make a Deal" premiered in 1963 and has continued its successful run largely uninterrupted up to present day. 
 Hall hosted various game shows and other programs in his early career until he developed "Let's Make a Deal" with creative partner Stefan Hatos. The show became legendary for its audience members dressing up in outlandish costumes in order to attract Hall's attention in the hopes of being given the opportunity to win big. 
 Hall and Hatos produced several other game shows under their production company through the '70s and '80s. Hall continued to host "Let's Make a Deal" for almost all of its 5,000 episodes, as it traversed from NBC, to ABC, and finally nighttime syndication. The show was revived by CBS in 2009 with host Wayne Brady, and continues to air. 
 The show became so popular in pop culture that it spawned "the Monty Hall Problem," a thought experiment in probability that involves three doors, two goats, and a prize. 
 Hall received a star on the Hollywood Walk of Fame in 1973, and, as a Canadian native, was named to the Order of Canada in 1988. 
 Born Monte Halparin, he was raised in an Orthodox Jewish family in Winnipeg, where he started his career in radio. 
 He is survived by three children: actress Joanna Gleason, who confirmed his death; TV exec Sharon Hall Kessler and TV producer Richard Hall; a brother and five grandchildren. His wife Marilyn died in June. 
