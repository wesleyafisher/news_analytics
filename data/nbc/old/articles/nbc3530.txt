__HTTP_STATUS_CODE
200
__TITLE
Why Duterte Declared Martial Law in Southern Philippines Over ISIS-Linked Group
__AUTHORS
Erik Ortiz
__TIMESTAMP
May 27 2017, 2:32 pm ET
__ARTICLE
 A lakeside city in the southern Philippines has been rocked by violence after ISIS-linked militants took hostages from a Catholic church and defiantly raised black flags synonymous with the terror group. 
 The clash in Muslim-majority Marawi, where firefighting erupted Tuesday, has led to at least 44 deaths involving militants, soldiers and police, according to the country's Armed Forces. Some civilians were reportedly being used as human shields, while others, including children, were evacuated with the help of the military. 
 President Rodrigo Duterte had to cut short his trip to Moscow with Russian President Vladimir Putin  whom Duterte last fall hailed as his "favorite hero"  in order to deal with the growing conflict. 
 Duterte's response has included declaring martial law across the southern tier of the Philippines and is warning he could expand it to the entire island nation of 100 million. 
 What happens next is being closely watched for how Duterte  dubbed "The Punisher" for his extreme positions and boasting of personally killing criminals in the nations drug war  handles a fraught situation that could potentially destabilize any chance for meaningful peace. 
 For the ISIS-linked fighters, it's independence. 
 They are known as Abu Sayyaf, armed militants who reside in the southern Philippines  home to a mostly Muslim population compared to the rest of the nation, which is predominantly Catholic. 
 Abu Sayyaf, which translates to "bearer of the sword" in Arabic, was previously part of the larger political organization Moro National Liberation Front, which over the decades has held talks with the Philippine government for autonomy for the indigenous Moro communities. But Muslim factions want more  they want to secede  and have captured and killed foreigners, including Americans, in their pursuit for full independence. 
 In 2014, Abu Sayyaf's Arabic-speaking commander, Isnilon Hapilon, pledged fidelity to ISIS leader Abu Bakr al-Baghdadi, the self-declared leader of a caliphate straddling Syria and Iraq. 
 Hapilon, who is believed to be 51, has been on the FBI's most-wanted terrorists list since 2006 and has a $5 million bounty on his head. The U.S. has indicted him for terrorist acts against American citizens as an Abu Sayyaf deputy. 
 Philippine officials and the military don't believe the terror group has any actual foothold in the country. 
 They believe rebel groups are merely capitalizing on ISIS's ruthless reputation in order to win influence and push ahead its own agenda of a separate Islamic state. 
 A military spokesman said Thursday there has been no "concrete evidence of material support" from ISIS, but local groups might still be asking for help. Officials worry also that foreign fighters from Malaysia and Indonesia could be infiltrating the southern Philippines in order to establish a Southeast Asian caliphate. 
 Joining Abu Sayyaf in overrunning Marawi  with a population of 200,000, about the size of Montgomery, Alabama  is another splinter group called Maute, which also pledged loyalty to ISIS and is part of a loose alliance that has declared Hapilon its leader. 
 The raising of black ISIS flags in Marawi is meant to signal how serious the groups are in conveying their ties to the terror group, observers say. 
 Duterte has experience with rebels because he hails from the southern city of Davao, which was hit by a deadly bombing in September blamed on the Maute. He has vowed to eradicate extremists by being "10 times" more brutal. 
 "I'll put at stake my honor, my life and the presidency," Duterte said during a speech last August. 
 Government troops were sent on a mission to raid Hapilon's hideout. That prompted the militants to call for backup from the Maute, who sent about 50 gunmen to the city. 
 The militants forced their way into the Marawi Cathedral and captured a Catholic priest, 10 worshipers and three church workers, officials said. Some buildings in the city were torched. There were also reports of blackouts and gunmen taking over hospitals, city hall and the local jail. 
 But soldiers have been setting up checkpoints as they help residents evacuate. Hapilon is still believed to be somewhere in the city, military officials said Friday. 
 Duterte declared martial rule for 60 days in the Mindanao region, the southernmost major island of the country where Marawi is located. 
 As a result, the president has the ability to send the military anywhere in the region to search homes, impose a curfew and make arrests without a court-issued warrant. 
 Duterte had brought up the possibility of martial law on several occasions since becoming president, citing the need to combat terrorism and the drug trade. But the Constitution requires that martial law be imposed because of either an invasion or rebellion  and Duterte has labeled the violence in the south as the former. 
 Related: What You Need to Know About the Controversial Philippine President 
 Human rights groups are worried that Duterte, who is already accused of carrying out unlawful extrajudicial killings, could be opening up his administration to abuses of power. 
 While addressing Philippine troops Friday, he reportedly said he "alone would be responsible" for any consequences in declaring martial law. 
 He also joked to the troops that if they rape up to three women, he would claim responsibility for that as well. The president is known for making crude remarks decried as offensive, and he previously joked about rape before his presidential election win last year. 
 He has the power under the country's 1987 Constitution to declare martial law for up to 60 days, but any extension requires approval from Congress. 
 The idea of martial law dredges up a dark chapter in the Philippines' history. In 1972, President Ferdinand Marcos' declaration suspended civil rights and essentially plunged the democracy under an authoritarian-like regime. 
 His power lasted until 1986, after Filipinos held a series of mass street demonstrations known as the "People Power Revolution." 
 Marcos' rule was marked by extrajudicial killings, arrests and unexplained disappearances of the opposition. 
 But unlike Marcos, Duterte no longer has the ability under martial law to suspend the Constitution, close Congress or supplant the civil courts, said Carl Baker, the director of programs at the think tank Pacific Forum CSIS in Hawaii. 
 While Duterte appears to have support in Congress for exacting martial law across the south, it's "unlikely" he would extend it nationwide "without some further action that would provide additional justification," Baker said. 
 Josh Kurlantzick, a fellow for Southeast Asia at the Council on Foreign Relations, said anything is possible under Duterte, who has been talking about rebel fighters advancing northward. 
 "He's definitely amping up the fear," Kurlantzick said. 
 Potentially, very badly, according to Kurlantzick. 
 The Philippine military and special forces' track record is "terrible" in the south, which has been ravaged by terrorist clashes, he added. In one of the biggest disasters, dozens of Philippine commandos were killed in 2015 during a predawn raid gone wrong to capture a terrorist bomb expert. 
 In this case in Marawi, "both the Army and Duterte can just storm the place and accept any casualties," Kurlantzick said. "I don't expect weeks and weeks of standoff." 
 Duterte suggested Friday that he's open to seeing the military take out just about anyone who's found attacking soldiers with unauthorized weapons. 
 "My order to the troops is to kill all those who are not authorized by the government to carry firearms, who fight back; kill them all," he reportedly said. 
 But, he clarified, if those who aren't hardliners want, then, "Let's talk peace." 
 Although longstanding allies, the United States and the Philippines had an increasingly strained relationship when President Barack Obama was in office. 
 President Donald Trump, however, appears to be on better terms with Duterte. The pair shared a phone conversation last month in which Trump praised Duterte for doing a "great job" handling his nation's drug war and revealed the U.S. had two nuclear submarines near North Korea, according to a transcript of a conversation obtained by The Washington Post. 
 The White House on Thursday condemned the violence in Marawi. 
 "These cowardly terrorists killed Philippine law enforcement officials and endangered the lives of innocent citizens," a statement said, adding, "The United States is a proud ally of the Philippines, and we will continue to work with the Philippines to address shared threats to the peace and security of our countries." 
