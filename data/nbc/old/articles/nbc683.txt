__HTTP_STATUS_CODE
200
__TITLE
I Was Fighting With Everyone: Heroins Poisonous Impact
__AUTHORS
__TIMESTAMP
Apr 7 2014, 4:41 pm ET
__ARTICLE
 The number of heroin users has doubled in the past five years, a trend driven in part by the rising cost of prescription medications. And in Vermont, the number of people seeking treatment for heroin addiction has skyrocketed 250 percent since 2000. 
 NBC News spoke with three patients at Green Mountain Family Medicine in Rutland, Vt., who were visiting the clinic for Suboxone, a drug treatment for opioid dependence. Watch the video below to hear their stories. (Full names are being withheld to protect their identities.) 
 Tonight, well have more about Americas growing epidemic on Nightly News with Brian Williams as part of NBC News new series on heroin addiction. 
