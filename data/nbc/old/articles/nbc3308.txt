__HTTP_STATUS_CODE
200
__TITLE
Trump Just Made It Easier for Employers to Refuse to Pay for Birth Control
__AUTHORS
Maggie Fox
__TIMESTAMP
Oct 9 2017, 12:55 pm ET
__ARTICLE
 The Trump administration loosened Obama-era birth control requirements on Friday, saying most providers of health insurance could refuse to pay for an employees birth control if the provider shows sincerely held religious or moral objections. 
 The new regulations, which take effect immediately, protect religious groups such as the Little Sisters of the Poor from litigation if they refuse to provide contraceptive coverage, but widen the pool of those shielded to include nonprofits, for-profit companies, other nongovernmental employers, and schools and universities. 
 "These rules will not affect over 99.9 percent of the 165 million women in the United States," the Health and Human Services Department said in a statement. 
 "No American should be forced to violate his or her own conscience in order to abide by the laws and regulations governing our health care system, HHS spokeswoman Caitlin Oakley said. Todays actions affirm the Trump administrations commitment to upholding the freedoms afforded all Americans under our constitution. 
 One of the most controversial  and most welcomed  requirements of the 2010 Affordable Care Act was a regulation for health insurance plans to provide birth control coverage free to patients. 
 Related: Most Americans Support Birth Control Mandate 
 The thinking behind it was to lower costs and improve health by helping more women get contraception, because pregnancy is much more expensive and dangerous to a womans health than using birth control, and because upward of 40 percent of pregnancies in the U.S. are unplanned. 
 Some religious groups objected and sued. The Roman Catholic Church opposes most methods of birth control, and some other religious groups object to certain forms of birth control. 
 Related: Birth Control Mandate Next on Trump Agenda 
 The move fulfills a promise made by President Donald Trump in a Rose Garden ceremony in May when he signed an executive order that he said restored religious freedoms, said Roger Severino, director of the Office for Civil Rights at the Health and Human Services Department. 
 That was a promise made, and this was a promise kept, Severino told reporters. The new rule, he said, provides relief to those who have been under the thumb of the federal government. 
 Medical and legal groups immediately objected. 
 By taking away womens access to no-cost birth control coverage, the rules give employers a license to discriminate against women, said Fatima Goss Graves, president of the National Womens Law Center. This will leave countless women without the critical birth control coverage they need to protect their health and economic security. We will take immediate legal steps to block these unfair and discriminatory rules. 
 The American Civil Liberties Union said it would file suit immediately. 
 No woman should ever be denied health care because her employer or universitys religious views are prioritized over her serious medical needs, said Kate Rochat, an ACLU member and law student at the University of Notre Dame, a Catholic university, who says she stands to lose access to contraceptive health care because of the rule. 
 Massachusetts Attorney General Maura Healey also sued. 
 "Among other things, many women are likely to turn to MassHealth  the states Medicaid plan  for coverage, which will place a financial burden on the state," Healey's office said in a statement. 
 "Todays complaint alleges that the new rule is unconstitutional by allowing the federal government to endorse certain religious beliefs over a womans right to make choices about her own health care." 
 Most U.S. women use birth control at some point. 
 Contraception is a medical necessity for women during approximately 30 years of their lives, said Dr. Haywood Brown, president of the American Congress of Obstetricians and Gynecologists. 
 It improves the health of women, children and families as well as communities overall; reduces maternal mortality; and enhances economic stability for women and their families. All Americans deserve the ability to make personal health care decisions without intrusion from their employers or the government. 
 Related: Yes,You Have to Pay for Birth Control, Feds Say 
 HHS said it calculated that only organizations that had already sued would take advantage of the new rule. 
 "The regulation leaves in place preventive services coverage guidelines where no religious or moral objection exists  meaning that out of millions of employers in the U.S., these exemptions may impact only about 200 entities, the number that that filed lawsuits based on religious or moral objections," HHS said. 
 But Gretchen Borchelt, vice president for reproductive rights and health at the NWLC, disputed this. 
 "We dont know how many employers are going to try and get out of the benefit once they know they are allowed to do it," she said. 
 It works against their interest, Borchelt added, but some employers may incorrectly believe they are saving money. "We know from the past that insurance plans will do whatever they can if they think they can save money," Borchelt said. 
 "Birth control coverage saves money in the long run because employers dont have to pay for pregnancy and related costs," she added. "We know that when birth control was added to insurance plans, it didnt change the premiums. There shouldn't be cost concerns." 
 The new rule is part of a broader package changing the federal approach to religious liberty. Except in the narrowest circumstances, no one should be forced to choose between living out his or her faith and complying with the law, Attorney General Jeff Sessions said in a memorandum sent government wide. 
 Related: Contraception Fell, Medicaid Births Rose When Texas Defunded Planned Parenthood 
 Birth control allows people to decide when and whether to have children and is a fundamental right, said Cecile Richards, president of the Planned Parenthood Federation of America. 
 The Trump administration just took direct aim at birth control coverage for 62 million women. This is an unacceptable attack on basic health care that the vast majority of women rely on. With this rule in place, any employer could decide that their employees no longer have health insurance coverage for birth control," Richards said. 
 Birth control is not controversial  its health care the vast majority of women will use in the course of their lifetime." 
 Most Americans get their health insurance through an employer, and most employers voluntarily provide contraceptive coverage, HHS officials noted. "These rules do not alter multiple other federal programs that provide free or subsidized contraceptives for women at risk of unintended pregnancy," the rule, published in the Federal Register, adds. 
