__HTTP_STATUS_CODE
200
__TITLE
For Governor Christie, the Battle Against Opioid Addiction Is Personal
__AUTHORS
Corky Siemaszko
__TIMESTAMP
Mar 29 2017, 4:22 pm ET
__ARTICLE
 For New Jersey Gov. Chris Christie, the epiphany came around the time his longtime friend died alone in a West Orange motel with empty bottles of Percocet  and a bottle of vodka to wash the prescription painkillers down  on the nightstand. 
 By every measure that we define success in this country this guy had it, Christie said in November 2015, during what may have been the most heartfelt moment of his brief and ultimately futile presidential run. 
 Great looking guy, well-educated, great career, plenty of money, beautiful, loving wife, beautiful children, great house, he had everything, Christie said. 
 He was also a drug addict and he couldnt get help and hes dead, he said. It can happen to anyone. 
 Perhaps because the stigma remains, Christie has never publicly identified his fallen friend. 
 From various New Jersey news reports, we know that Christies pal was a 52-year-old former Seton Hall University School of Law classmate and that he was married to the governors family doctor. We also know he was the father of three daughters and that his family and the Christies often vacationed together. 
 But Christies doomed friend, who got hooked on painkillers around 2006 when he was being treated for a bad back, has made the opioid epidemic raging in parts of the Garden State  and across the Rust Belt  deeply personal for the Republican governor. 
 And its the painful experience of losing somebody to drugs that Christie brings with him to his new post as head of the White House commission to combat drug addiction. 
 "The opioid initiative is one that's incredibly important to every family in every corner of this country," Christie said Wednesday on TODAY. 
 He said his state is grappling with a rise in the rate of drug overdose deaths, which have surpassed murders and automobile accidents. 
 "What we need to come to grips with is addiction is a disease and no life is disposable. We can help people by giving them appropriate treatment," Christie added. 
 Roseanne Scotti of the New Jersey Drug Policy Alliance said the governor has already made addiction a centerpiece of his administration. 
 He does seem to get this on a different level, Scotti told NBC News. Hes taken a public health approach to this. He didnt just push this lock people up and throw away the key kind of stuff. 
 Under Christie, a former U.S. Attorney, New Jersey expanded its medical marijuana program to cover veterans who suffer from PTSD and school children struggling with Tourette Syndrome, Scotti said. 
 Christie also signed legislation that allows pharmacists to sell the heroin overdose antidote Narcan without a prescription. And he inked a good Samaritan bill that protects people who call 911 for help during an overdose from prosecution  after first vetoing it. 
 He spoke very eloquently about how when the bill first got to his desk he was looking at it like a prosecutor, Scotti said. And afterwards he began thinking about it as a parent, how he came around to thinking it was a good idea. He said, 'This is the only time Ive changed my mind on something like this.' 
 Assemblyman John Wisniewski, a Democrat running for governor, takes a far less rosy view of Christies efforts to combat opioid addiction in New Jersey. He took particular aim at the television ads Christie has made promoting the states anti-drug efforts. 
 While opioid abuse is a significant issue in New Jersey and we must be vigilant in making resources available for any addict who wants to get clean and healthy, there are grave concerns about the self-serving way Gov. Christie is going about it, he said in a statement. Governor Christie has misappropriated critical health care funding for women and infants to create television ads starring himself. The idea that putting Christies face on television is the best way to reach addicts and end the scourge of addiction defies common sense. 
 Wisniewski also noted that President Donald Trump appointed Christie to head the commission on the eve of the sentencing of two former aides who were convicted in the "Bridgegate Scandal" of engineering lane closures on the George Washington Bridge to punish a Democratic mayor for not endorsing the governors reelection. 
 This is an obvious and transparent ploy to divert the conversation away from what will be his lasting legacy: traffic problems in Fort Lee, he said. 
