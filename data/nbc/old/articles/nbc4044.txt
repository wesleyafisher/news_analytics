__HTTP_STATUS_CODE
200
__TITLE
Can Vitamin C Prevent Leukemia?
__AUTHORS
Maggie Fox
__TIMESTAMP
Aug 24 2017, 9:03 am ET
__ARTICLE
 Heres another reason to make sure youre eating plenty of vitamin C: Two new studies out in the past week show that the vitamin may help prevent blood cells from going bad and causing some types of leukemia. 
 But theres no evidence that doses beyond the recommended daily requirement of the vitamin help even more. In fact, third study out this week provides a cautionary note: male smokers who took vitamin B supplements actually raised their risk of lung cancer. 
 Like most recent studies on vitamins, the researchers say the take-home message is to get plenty of nutrients from food, not from supplements. 
 What our data say is that its important to get 100 percent of your daily vitamin C requirement but there no evidence from our results that there is any added benefit from having megadoses of vitamin C, said Dr. Sean Morrison, a Howard Hughes Medical Institute investigator at the University of Texas Southwestern who led one of the studies. 
 So this is not one of those things where if a little bit is good, more must be better. Our data just suggest that its important to get 100 percent. 
 The tests were done on cells and on mice that lacked vitamin C. It only took a little to restore cells to proper function. 
 Anyway, Morrison added, When you eat a huge amount of vitamin C, you just pee it out. 
 Related: Vitamin C May Help Cancer Treatment 
 His team found that vitamin C helps cells in the bone marrow grow. Thats good when its within normal limits, but if immature cells called stem cells start over-multiplying, the result can be cancer. 
 We discovered that stem cells soak up more vitamin C than other blood -forming cells, said Morrison, whose results were published in the journal Nature. 
 Vitamin C, when its depleted, is limiting for an important tumor-suppressor enzyme in the blood forming system called TET2, he added. "So less vitamin C means less TET2 to stop tumors from forming. The vitamin is suppressing the development of leukemia." 
 Related: Vitamin Pills Don't Prevent Heart Disease or Cancer 
 The American Cancer Society projects that 62,130 people will be diagnosed with leukemia in 2017 and 24,500 will die from it. But there are many types of leukemia, and the findings of the study mostly apply to a specific type called acute myeloid leukemia, which affects mostly adults. 
 The setting where we are most concerned about vitamin C and the link to cancer is in the context of old people, Morrison said. Vitamin C deficiency is actually much more common in old people than it is in children. 
 And older people also often have a condition that makes them prone to leukemia. 
 In at least 10 percent of people by age 70, there are a small number of mutated blood-forming stem cells that have transformed partway into leukemia cells that outcompete the other blood-forming cells, Morrison said. 
 This is considered a pre-leukemic condition because those people are at increased risk of progressing to leukemia, but not all of them will. 
 Related: You're Still Not Eating Enough Vegetables 
 Now the team is working with the Centers for Disease Control and Prevention to compare cancer cases with surveys of what people eat and drink to see how common cancer is in people with low vitamin C intake  and to see which particular cancers might be most important. 
 This doesnt mean all leukemia can be blamed on a lack of vitamin C, Morrison stressed. 
 It has been known for decades that people, men in particular, with below-average levels of vitamin C are at increased risk of cancer. Thats where the recommendation comes from that people should eat lots of fruit, Morrison said. 
 The second vitamin C study also found the vitamin  found in citrus fruit, potatoes, tomatoes and broccoli  was important to TET2. 
 The team at Perlmutter Cancer Center at NYU Langone Health found evidence that vitamin C may re-program mutated stem cells, causing them to reenter a normal pathway in which they mature and eventually die, instead of becoming virtually immortal cancer cells. 
 "We're excited by the prospect that high-dose vitamin C might become a safe treatment for blood diseases caused by TET2-deficient leukemia stem cells, most likely in combination with other targeted therapies," NYUs Dr. Benjamin Neel, who worked on the study, said in a statement. 
 It is known that leukemia cells often delete this TET2 enzyme and thats how they are able to grow out of control, Morrison commented. 
 Both teams used genetically engineered mice for the bulk of their experiments and are now working now to make sure the research translates into humans. 
 Related: Big Study Finds Most Alternative Therapies Are Bogus 
 Its clear that people need the right nutrients to prevent cancer but its also clear that taking supplements or otherwise trying to get megadoses can backfire. 
 A study published in the Journal of Clinical Oncology this week showed that male smokers who took the largest doses of vitamin B supplements nearly doubled their risk of lung cancer. 
 The study of 77,000 people showed no increased risk among women, but one landmark study from 1996 found that smokers who took beta-carotene, which the body turns into vitamin A, had a higher risk of lung cancer than smokers who didnt take it. 
 Our data shows that taking high doses of B6 and B12 over a very long period of time could contribute to lung cancer incidence rates in male smokers. This is certainly a concern worthy of further evaluation," said one of the researchers, Theodore Brasky of Ohio State University. 
