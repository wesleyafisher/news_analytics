__HTTP_STATUS_CODE
200
__TITLE
American Richard Thaler Wins the 2017 Nobel Prize for Economics
__AUTHORS
Associated Press
__TIMESTAMP
Oct 9 2017, 7:31 am ET
__ARTICLE
 STOCKHOLM  University of Chicago academic Richard Thaler was awarded the Nobel prize for economics on Monday. 
 The $1.1-million prize was given to the American for his "understanding the psychology of economics," Swedish Academy of Sciences secretary Goeran Hansson said Monday. 
 The Nobel committee said as a pioneer in behavioral economics, Thaler has built a bridge between economics and psychology to show a "more realistic analysis of how people think and behave when making economic decisions." 
Tweet Id not present
 It said his research has expanded economic analysis by considering three psychological traits: Limited rationality, perceptions about fairness and lack of self-control. 
 Speaking by phone to a news conference immediately after he was announced as the prize winner, Thaler said the most important impact of his work is "the recognition that economic agents are humans" and money decisions are not made strictly rationally. 
 Thaler told the news conference that he will likely use the prize money in ways consistent with his research. 
 "I will say that I will try to spend it as irrationally as possible," he said. 
 Outside of academia, Thaler has made a cameo in the movie "The Big Short" to discuss an economic phenomenon known as the "hot hand fallacy"  in which a person's success is seen as a sign he would be successful in other fields  with pop star Selena Gomez. 
 Asked at the news conference if he thought this observation applied to the U.S president, he said: "As to President Trump, I think he would do well to watch that movie." 
 Thaler joins a strong cohort of American Nobel winners this year. 
 Last Monday, the medicine prize went to three Americans studying circadian rhythms: Jeffrey C. Hall, Michael Rosbash and Michael W. Young. 
 Three U.S. scientists  Rainer Weiss, Barry C. Barish, Kip S. Thorne  won the Nobel Prize in physics on Tuesday. 
 And on Wednesday, Joachim Frank, a German-born American biophysicist, became one of the three laureates awarded the Nobel Prize in chemistry. 
 The economics prize is something of an outlier  Alfred Nobel's will didn't call for its establishment and it honors a science that many doubt is a science at all. 
 The Sveriges Riksbank (Swedish National Bank) Prize in Economic Sciences in Memory of Alfred Nobel was first awarded in 1969, nearly seven decades after the series of prestigious prizes that Nobel called for. 
 Despite its provenance and carefully laborious name, it is broadly considered an equal to the other Nobel and the winner attends the famed presentation banquet. 
