__HTTP_STATUS_CODE
200
__TITLE
Rapper Bow Wow Mocked After Posting Photo of Private Plane
__AUTHORS
Nick Bailey
__TIMESTAMP
May 11 2017, 8:04 am ET
__ARTICLE
 It seemed just another ordinary day for the star behind one of the many lifestyles of the rich and famous that appear on Instagram to make the rest of us jealous. 
 Rapper Shad Moss, AKA Bow Wow and formerly known as Lil Bow Wow, posted a photo of a luxury car parked on an airport tarmac in front of a private plane with the caption: Travel Day. NYC press run for Growing Up Hip Hop. Lets gooo. 
A photo posted by NBC News (@nbcnews)
 His post suggested he was at DeKalb-Peachtree Airport in Georgia. 
 But not long later, a Snapchat post emerged alleging the rapper was actually on a standard commercial flight at the same time he bragged and put his jet photo on Instagram. 
Bow Wow on IG vs Bow Wow in real life  pic.twitter.com/5fZsv3zApg
 Eagle-eyed fans soon traced the original Instagram post back to a stock photo of a private plane used by a Fort Lauderdale limousine company. However, Bow Wow added a filter to that image before posting it. 
 The company the 30-year-old borrowed the image from is based hundreds of miles away from where he was actually taking off. 
 Its not the first time Bow Wow has been caught out on social media. In a now-deleted tweet last November, he claimed to have passed up the chance to watch the Atlanta Falcons play from a private suite as he was too tired from filming a TV show. 
 Fans quickly pointed out that the NFL team had a bye week when he posted the claim, making the suite offer unlikely. 
 So far the rappers response to the fallout appears to be another Instagram post stating: My hustle is non stop. I never stop hustling. 
 Social media users worldwide have been less reserved, with the #bowwowchallenge proving popular. 
 It involves users posting a pair of images side-by-side, with the first seemingly a glamorous representation of their life and the second image depicting the harsh reality. 
Hella Bottles For Tonight  #Bowwowchallenge pic.twitter.com/i9F65tmBJr
With my man  #bowwowchallenge pic.twitter.com/NyaUQXJaot
Blessed  My new ride at the car wash. #BowWowChallenge pic.twitter.com/unoT3kQWeU
 
