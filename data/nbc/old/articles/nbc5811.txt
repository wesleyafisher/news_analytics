__HTTP_STATUS_CODE
200
__TITLE
Plan for All-Female Lord of the Flies Remake Sparks Social Media Backlash
__AUTHORS
Daniella Silva
__TIMESTAMP
Aug 31 2017, 4:41 pm ET
__ARTICLE
 A reported planned all-female film adaptation of the classic novel "Lord of the Flies" drew immediate backlash on social media, with critics arguing the gender-flipped reboot is missing the point  and that its being written and directed by two men. 
 Film makers Scott McGehee and David Siegel have made a deal with Warner Bros. to write and direct a film based on the William Golding novel, Deadline reported on Wednesday. In the novel, a group of schoolboys who become stranded on an island slowly descend into barbarism and brutality. 
 "We want to do a very faithful but contemporized adaptation of the book, but our idea was to do it with all girls rather than boys," Siegel told Deadline. 
 It is a timeless story that is especially relevant today, with the interpersonal conflicts and bullying, and the idea of children forming a society and replicating the behavior they saw in grownups before they were marooned, he added. 
 Related: Has Ghostbusters Always Been Politicized? 
 The announcement was met with criticism on social media and some said re-adapting the novel with girls misses the mark on some of its central themes of power and masculinity. 
 Best-selling author and Purdue University professor Roxane Gay said in a series of posts on Twitter that the narrative of "Lord of the Flies" was "intrinsically connected to toxic masculinity" and took issue with the all-female remake being helmed by two men. 
 "If you want to tell a story about toxic femininity fine do that. But using Lord of the Flies as the container is lazy and stupid," Gay wrote. 
An all women remake of Lord of the Flies makes no sense because... the plot of that book wouldn't happen with all women.
 Others echoed the critiques 
GOOD: A female-centric Lord of the Flies!BAD: A female-centric Lord of the Flies written by... two men.https://t.co/26CBGu4lMj
lord of the flies is about the pitfalls of masculinity. don't bring the women into this, we don't care about your ass-mar
uhm lord of the flies is about the replication of systemic masculine toxicity every 9th grader knows thisu can read about it on sparknotes https://t.co/EQFyuSA3MV
[flies into frame on a broom]the thing about lord of the flies is that it's about systemic male violence + how it replicates[flies away]
 The iconic novel has been previously adapted as a film in 1963 and 1990. Representatives for McGehee and Siegel did not immediately return requests for comment from NBC News. 
 McGehee told Deadline that crafting the story with girls instead of boys "shifts things in a way that might help people see the story anew." 
 Siegel and McGehee have also directed together 2012's "What Maisie Knew," 2005's "Bee Season" and 2001's "The Deep End." 
 The announcement of the deal comes as Hollywood has been making a series of female-centric reboots and spinoffs, including the remake of "Ghostbusters," and the planned female-focused "Ocean's 8" set for release next year. 
 And earlier this summer, the film "Wonder Woman" made waves and set records as the highest grossing film directed by a woman, according to the Associated Press. The film has surpassed $400 million domestically, the AP reported earlier this month. 
