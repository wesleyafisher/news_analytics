__HTTP_STATUS_CODE
200
__TITLE
Battle for Mosul Morphs Into Fierce Urban Combat
__AUTHORS
__TIMESTAMP
Nov 7 2016, 9:32 am ET
__ARTICLE
 MOSUL, Iraq  As Iraqi forces struggle to secure the gains they made over the past week on Mosul's eastern edge, the fight against ISIS has quickly morphed into close-quarters urban combat. 
 With it, casualties among Iraq's troops and civilians are spiking. 
 On Friday, when Iraqi forces first pushed into Mosul proper, Derek Coleman, an American volunteer medic worked alongside Iraqi special forces at the field clinic. 
 That day, he said he helped treat 44 casualties before he lost count. 
 "I think the Iraqi (military) got an awakening when they pushed too far forward," Coleman said, flipping through a notebook he uses to keep records of his cases. In all, 12 soldiers died at the clinic on Friday, he said. "We had two Humvees just loaded with dead bodies." 
 Iraq's military does not release official death tolls. 
 Behind Coleman, hundreds of civilians slowly poured out of Mosul on foot. Women and children held white flags made of scraps of dishtowels, torn clothing and pillowcases. Iraqi soldiers gathered them on street corners and loaded them on trucks set for camps for the displaced. 
 At least twice on Sunday, ISIS mortar rounds targeted the fleeing civilians gathered around the aid station, sending medics running for cover behind nearby buildings. A few hundred yards away, along the same road used by civilians, a car bomb was hit by an airstrike and exploded before it could reach its target. 
 At times the flow of wounded civilians and soldiers was constant, with vehicles racing from the front lines to reach the cluster of cots beside two ambulances. 
 Pete Reed, another American volunteer at the clinic said they expect high casualties. 
 "It's a huge urban environment," he said. "Imagine trying to go into Brooklyn or Charlotte, North Carolina, where you have huge buildings." 
 Reed added: "Now imagine that from every door, window, that kind of gunfire can come out of it at any moment." 
 Car bombs are "ripping these things apart. Humvees aren't built ... (to withstand a car bomb). Nothing's built for a suicide vehicle," Reed said. 
 Iraqi government forces have been leading an effort to expel ISIS, and numerous U.N. agencies have been mobilizing to help civilians who are expected to flee Mosul in massive numbers. About 1 million people are believed to be still living in the ISIS-held city. 
 Many of those still in Mosul feel trapped, including those in districts which the army entered last week. 
 "We still can't go out of our houses.... mortars are falling continuously on the quarter," a resident of the Quds neighborhood on the eastern edge of the city told Reuters by telephone on Sunday. 
