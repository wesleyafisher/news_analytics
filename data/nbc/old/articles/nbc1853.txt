__HTTP_STATUS_CODE
200
__TITLE
Pence Pledges Government Help in Wake of Harveys Devastation
__AUTHORS
Phil Helsel
__TIMESTAMP
Aug 31 2017, 11:04 pm ET
__ARTICLE
 Vice President Mike Pence visited storm-stricken Texas on Thursday and said he is confident Congress will move quickly to approve immediate assistance in the wake of massive flooding caused by Hurricane Harvey. 
 "No Texan should doubt as they apply for available federal assistance, no small business or business thats affected should doubt that this administration, this Congress will come together and make sure those resources are there," Pence said in Corpus Christi after surveying the damage. 
 Some 100,000 homes are estimated to have been affected by Harvey, which roared ashore as a Category 4 hurricane late Friday, causing damage along the Gulf coast and swamping the Houston area and eastern Texas with nearly 50 inches of rain in some places, Homeland Security Adviser Tom Bossert said Thursday. 
 "That's a big number," Bossert said. A supplemental appropriations request to Congress will be made for funding, and the administration will then make a second request once more is known, he said. 
 At least 38 deaths have been blamed on the hurricane and its aftermath as of Thursday night. Patients at a hospital in Beaumont, east of Houston, were evacuated via helicopter Thursday. The city experienced widespread flooding. Rescues across the region continued. 
 Two Republican congressional sources told NBC News that the Trump administration is expected to send an initial request for at least $5.5 billion for aid, likely $5 billion for the FEMA Disaster Relief Fund and $500 million for a small business loan fund. 
 An administration source said this is "in the ballpark" and said there could be additional money added to the request. Two sources said the request could come as soon as Friday but might not be formally made until next week. 
 In Crosby, northeast of Houston, a flooded chemical plant exploded and more explosions were feared. The fire at the Arkema Inc. plant prompted evacuations of residents within a mile and a half of the facility. 
 The center of Harvey, now a tropical depression, was 30 miles northwest of Greenwood, Mississippi, as of 5 p.m. ET Thursday, but the National Hurricane Center said that flooding continued across far eastern Texas and western Louisiana. 
 Meanwhile, the storm could be the costliest natural disaster in the nation's history. Pence said that the recovery would likely take years. The storm's impact in the refinery-rich region was expected to cause gas prices to increase. Authorities in some Texas cities urged people not to panic about gasoline supplies. 
 Pence said that as of Thursday morning more than 311,000 people have registered for disaster assistance. He said more than $530 million in assistance has been distributed. 
 "The sheer magnitude of this storm, its impacts that we saw on the ground and from the air are not lost on anyone here," Pence said. 
 "The long term challenge here will be ... getting people back into communities and into their homes, Pence said. The Federal Emergency Management Agency has prepositioned some 2,000 manufactured homes and the government has ordered another 4,000, he said. 
 Related: Harvey Victims Begin Returning Home to Damage and Uncertainty 
 And Texas Gov. Greg Abbott on Thursday increased the number of National Guard troops called to help to 24,000. Bossert said that 28 search-and-rescue teams and task forces from 16 states have sent help to Texas. 
 "I believe that thats the first time weve activated all the task forces since 9/11," Bossert said. He said that life-saving operations were continuing. 
 President Donald Trump has pledged to donate $1 million in personal funds for storm relief efforts, White House press secretary Sarah Huckabee Sanders said. Trump visited Texas on Tuesday. 
 Pence, asked whether the Trump administration would seek offsets in budget cuts to pay for disaster relief funding, said that Trumps top priority right now is focusing on rescue and recovery efforts. 
 "Were going to be working very diligently and already have begun to work with the Congress to make sure that full resources to backstop those efforts, to provide for recovery and ultimately to provide for rebuilding communities that have been devastated by Hurricane Harvey is there," Pence said. 
 He said he would leave future decisions to Trump and Congress as the process moves forward. 
 Estimates are that around 200,000 customers were without power in Texas Thursday, 11,000 in Louisiana. In Beaumont, a main pump station and wells were flooded and around 120,000 people were without water, federal and local officials said. 
 In the city of Dickinson, southeast of Houston, residents began to return Wednesday, in some cases finding devastation. The town of around 19,000 had been under a mandatory evacuation order. 
 "This handle was the only thing sticking out of the water," Chris Rhoads, pointing to a ruined Harley Davidson motorcycle in his garage, told NBC Dallas-Fort Worth. 
 "When I left, the water was barely getting into the house. When I came back, it was waist-high," Rhoads said. 
