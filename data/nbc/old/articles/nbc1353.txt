__HTTP_STATUS_CODE
200
__TITLE
Questions Surround Foreign Teen Found After ISIS Defeat in Mosul
__AUTHORS
Andy Eckardt
Carlo Angerer
Matthew Grimson
__TIMESTAMP
Jul 19 2017, 10:12 am ET
__ARTICLE
 MAINZ, Germany  Authorities in Germany are working to establish whether a teenage girl who was among five women captured in the former ISIS stronghold of Mosul last week is a 16-year-old German thought to have fled to Iraq after being radicalized online. 
 Iraqi forces found a teenager of foreign origin hiding in the cellar of a house in the Old Mosul neighborhood as they routed ISIS from the key city last week. 
 The girl was at first believed to be Yazidi  a religious minority persecuted by ISIS in northern Iraq  because she spoke little Arabic and had fair skin, officials says. 
 "This girl is not Iraqi and security officials are working to identify her nationality," an officer from Iraq's Joint Operation Command, who did not want to be identified, told NBC News. "The five of them are jihadi women who served with ISIS, he added, without providing further details. 
 Related: U.S. and Iraq Declare Victory Over ISIS in Mosul 
 A senior Iraqi security official, speaking on the condition of anonymity, told NBC News that they were working with German officials to establish whether the young woman is Linda W., a 16-year-old girl who went missing last summer and was under investigation for supporting ISIS. 
 The girl had made contact with ISIS via chat rooms and Facebook, and had traveled to Istanbul, Turkey, from Frankfurt with a forged letter from her parents, according to German authorities. 
 Lorenz Haase, a spokesman for the Dresden prosecutor's office in eastern Germany, told NBC News that "there is no pending arrest warrant in Germany for the adolescent" but she could face extradition if her identity was confirmed. Haase also noted the girl could face charges in Mosul. 
 Reuters reported that another Iraqi official said he believed the teenager was not German but of Slavic origin, possibly Russian. He said the young woman had been taken to a hospital for burns and was in the custody of Iraqi security services, adding that they would likely hand her over to her country's diplomatic mission and not kept in Iraq. 
 Linda had been living in the small eastern German town of Pulsnitz with her mother and step-father, according to German media reports. She reportedly had good grades in school, but suddenly decided to wear a headscarf in class last year. 
 Photos: Iraqis Celebrate Bitter Victory Over ISIS in Ruins of Mosul 
 Pulsnitz mayor Barbara Lueke told NBC News that Lindas school had identified conspicuous behavior and had informed her parents at an early stage but social structures are missing that could cope with these problems. 
 Linda is an example that shows this radicalization can happen in any part of the country where you have internet access, Lueke said. Here it was a case of radical Islam, but it could have also been right-wing extremism, alcohol or drugs. 
 Germany's BfV domestic intelligence agency said 930 people have left the Germany to join ISIS and other terrorist groups in recent years. About 20 percent of those are women, with isolated cases of individuals being under the age of 18. 
 Lindas family was being counselled by Hayat (Turkish and Arabic for "Life"), an organization with a rehabilitation program for people involved in radical Salafist groups or on the path of a violent jihadist radicalization, including those travelling to Syria and other combat zones, the group told NBC News. 
 Andy Eckardt and Carlo Angerer reported from Mainz, Germany, and Matthew Grimson reported from London. 
