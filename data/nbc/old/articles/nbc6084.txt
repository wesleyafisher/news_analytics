__HTTP_STATUS_CODE
200
__TITLE
App Lets You Find Your Dating Doppelgnger, Catch a Cheating Spouse
__AUTHORS
Alyssa Newcomb
__TIMESTAMP
Jun 23 2017, 7:50 am ET
__ARTICLE
 A new app is harnessing artificial intelligence to find the dating profile of just about any face your heart desires. 
 Want to date someone who looks like Chris Hemsworth? Plug in the Thor star's photo, an age range, zip code and you'll be treated to a bevy of faces resembling the Aussie actor. The Dating.ai app pulls from various dating websites, including Tinder, Match, and Plenty of Fish. Click on one and it will take you to their dating profile. 
 Heath Ahrens, co-founder of Dating.ai, declined to tell NBC News exactly how the company is pulling from various dating sites, but said it's all "publicly available information." 
 "The point was to put something together to see how people would use it and whether it was even an interesting idea," Ahrens told NBC News. "A lot of times you have an idea for an app and then you put it out there and no one cares. We wanted to see if we could we use this technology in dating." 
 Related: And Now, a New Way to Be Judged on Tinder: Your Spotify Playlist 
 The app launched in stealth mode two months ago. It's free to play with some celebrity faces, but for $7.99 per month (that's after a free one-week trial), you can input any face you want. That's led to some interesting use cases, Ahrens said. 
 "One of the guys who works here actually sent a link to his friend early on and his friend ran his girlfriends picture and found her. She had forgotten to take her profile down," he said. "Those kind of use cases are ones that are happening organically." 
 While that may have been an innocent mistake, Ahrens said the technology can be used to find cheaters and  on the more fun end of the spectrum  doppelgngers. 
 With 15,000 people and counting using the app, he said many people are sharing how they've found their look-alike. (That includes those who are curious to see who they look like of the opposite gender.) 
 A Tinder representative told NBC News they've contacted Dating.ai to say the app is violating their terms. Tinder said it was told Dating.ai will "address the issue." 
 "We take the security and privacy of our users seriously and have tools and systems in place to uphold the integrity of our platform. Tinder is free and used in more than 190 countries, and the images that we serve are profile images, which are available to anyone swiping on the app," the company said in a statement. "We continue to implement measures against the automated use of our API, which includes steps to deter and prevent scraping." 
 Controversy aside, the technology was impressive when NBC News took it for a spin. Plug in a photo and you'll see plenty of people who look just like you. However, one caveat: We weren't able to find single friends in the mix, despite plugging in their photos, exact age, and zip codes. Ahrens said this is because the system is currently experiencing a bit of a backlog. Other than that, it's still incredibly fun to play with. 
 "In a general sense, AI and this type of technology is something that people are really interested in but they rarely get a chance to see or touch," Ahrens said. "Everyone knows Facebook uses this tech to auto tag you, but as a user you dont get to play with that. This is enterprise-class tech in the hands of consumers, and were excited to see what people do with it." 
