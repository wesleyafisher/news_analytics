__HTTP_STATUS_CODE
200
__TITLE
This Plant Makes Boba. A Startup Wants to Use It to Clean Up Plastic.
__AUTHORS
Kristi Eaton
__TIMESTAMP
Oct 4 2017, 1:55 am ET
__ARTICLE
 Kevin Kumala was on a sabbatical from studying in the United States when he decided to visit his native Indonesia and clear his head with surfing and the countrys beaches. 
 But on his return, Kumala no longer saw the white sandy beaches and calming turquoise waters he had grown up with in Kuta, Indonesia. 
 Instead the whole landscape was covered in plastic waste. 
 It was so dramatic, Kumala said. It was no longer the Kuta that I know." 
 According to a 2015 study published in the journal Science, Indonesia added between 480,000 and 1.29 million metric tons of plastic waste to the oceans in 2010 due to poor waste management and littering, ranking second in the world behind only China. 
 The study also found that 83 percent of the plastic waste in Indonesia was mismanaged, meaning it was disposed of incorrectly. The United States ranked 20th on the list, and only two percent of its waste was mismanaged. 
 Kumala said his aha moment came when he saw a turtle pass by with its neck entangled in plastic. It was at that moment that he decided to stop pursuing a medical degree at UCLA to focus on the protecting the environment. 
 He returned to Indonesia and studied the viability of a business as well as the overall plastic pollution problem in Indonesia, and in 2014, he opened Avani Eco, which produces a range of biodegradable products to replace plastic. 
 It was just so obvious that the opportunity to do something was so wide open, he said. The goal was always to find a potential replacement for these disposable plastic products. 
 The company produces bags made out of starch from the cassava root, an abundant plant found mostly in tropical countries that is used to make tapioca. The bags are biodegradable and compostable within 180 days, Kumala said, and even the ink on the bags is made of alcohol and soy. 
 The company also uses waste from sugar cane to create bowls, plates, and takeaway boxes for a variety of hotels. 
A photo posted by NBC News (@nbcnews)
 Straws are another focus for the company, which offers two solutions to plastic: a paper straw that utilizes a soy-based wax lining and a straw derived from cornstarch. 
 They are much more eco-friendly and both products are biodegradable and compostable, Kumala said. 
 Avani Eco is always undergoing research and development, Kumala added. While many of the companys customers are in the hospitality industry, Avani Eco hopes to eventually go inside rooms with eco-friendly products including combs, toothbrush, razor kits, and shampoo bottles. 
 In total, the company has replaced close to 200 tons of plastic in Indonesia alone, Kumala said, and the company is on track to quintuple that figure, replacing 1,000 tons of plastic with compostable items, in 2017. 
A photo posted by NBC News (@nbcnews)
 While Avani Eco is based in Indonesia, the company focuses on countries all over the world. Kumala noted that Kenya and Sri Lanka recently banned plastic. Sri Lanka banned the sale of plastic bags, cups, and plates after the islands largest dump collapsed, killing 32 people. Kenya passed a law calling for fines of up to $40,000 and four-year prison sentences for the use of plastic bags. 
 Meanwhile on Bali, which is part of Indonesia, two sisters convinced the islands governor to ban plastic bags by 2018. 
 Every day you wake up there is a new city or country that is actually taking this matter very seriously and actually banning plastic, Kumala said. 
 Follow NBC Asian America on Facebook, Twitter, Instagram and Tumblr. 
