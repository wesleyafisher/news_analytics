__HTTP_STATUS_CODE
200
__TITLE
Too Many People Stop Their Lifesaving Statins, Doctors Say
__AUTHORS
Shamard Charles, M.D.
__TIMESTAMP
Aug 6 2017, 12:26 pm ET
__ARTICLE
 Statins save lives. Then why do nearly half of patients stop taking the cholesterol-lowering medications against their doctors orders? 
 After suffering a near-fatal heart attack in April, Boris Pinhas, a 54-year-old computer programmer in Queens, New York, was prescribed a statin for a second time. He had been prescribed a low-dose of atorvastin, or Lipitor, for slightly elevated cholesterol in 2013 but gave it up soon after picking up his prescription. 
 He wasnt having side effects and he knew that the medication was protecting his heart, but he didn't like that he would have to take the medication for the rest of his life. That decision landed him in the ER. 
 "I was taking it for a while, but you lose track of time," Pinhas said. "And then one day I found myself sweating at the office and losing consciousness." 
 An estimated 73 million American adults have high levels of low-density lipoprotein (LDL), or bad, cholesterol, according to the Centers for Disease Control and Prevention. Statins such as Lipitor, Mevacor, Crestor and Zocor are among the most widely prescribed drugs in the world and have helped millions of patients avoid heart disease and stroke. In fact, about 28 percent of American men and women over age 40 take a statin, the CDC finds. 
 But new research published last week shows that only 61 percent of those who were prescribed a statin were still using the drug after three months  and only 55 percent remained on the drug after six months. 
 No symptoms are associated with high cholesterol levels, so its difficult to convince patients that they need to take a medication for prevention, before the catastrophe happens, said Dr. Steven Nissen, chairman of Cardiovascular Medicine at the Cleveland Clinic. 
 Related: More People Should Get Statins, Report Finds 
 In the new study, published in the Annals of Internal Medicine, researchers tracked over 28,000 patients in two Massachusetts hospitals and found that three in 10 stopped taking statins after experiencing side effects, which were presumed to be due to the drugs. 
 Some 8.5 percent of those who stopped their medication had a cardiovascular event, such as a heart attack or stroke, within four years  compared to 7.6 percent of those who continued taking statins. 
 Those numbers are alarming, said NBC lead correspondent Dr. John Torres. We dont want patients quitting their statins without their doctors supervision." But,"we also have to acknowledge that there are multiple reasons why people stop their statins." 
 According to data from the Mayo Clinic, almost a third of patients who take statins report side effects, which can contribute to people stopping their medication. In 2012, the Food and Drug Administration updated labeling on statins to include warnings about confusion and memory loss, elevated blood sugar leading to Type 2 diabetes, and muscle weakness. 
 The Internet may also be to blame. For all the scientific evidence that statins are safe and effective, common headlines that turn up during a Google search include: 5 reasons Not to Take Your Statin; The Grave Dangers of Statin Drugsand the Surprising Benefits of Cholesterol'; and 'Why Ive Ditched Statins for Good.' 
 Misinformation, coupled with real side effects, can contribute to the "nocebo" effect, a phenomenon where someone is conditioned to anticipate a negative effect from an experience. 
 The new study concludes that most patients who initially experience statin-related symptoms can actually tolerate long-term use of the drug by either taking a lower dose, or by switching to a different type of statin. 
 Related: Bacteria May Be the Reason Some Foods Cause Heart Disease, Stroke 
 Nissen believes that educating patients is key. 
 "A third of patients don't even fill the script," he said. Doctors need to help patients understand why they are on the medications, their treatment options, how the drug works, and possible side effects. 
 "This is called shared decision making and this might help patients buy-in," Nissen said. Nissen isn't associated with the new research, but wrote an editorial for the journal against "statin denial". 
 Statins are generally prescribed for life  with the exception of people who lose a substantial amount of weight through bariatric surgery and experience a dramatic decrease in unhealthy cholesterol levels. 
 Most doctors agree that high levels of unhealthy cholesterol are primarily influenced by our genes and lifestyle changes alone, while helpful, won't eliminate the risk of heart disease. 
 Patients need to understand that high cholesterol is governed by your genetics," said Nissen. "You cant pick who your parents are. Diet alone can decrease your bad cholesterol by 15 percent, but that is not the 40-50 percent reduction that we look for." 
 When someone stops taking a statin prescription, cholesterol levels go back up, along with the risk for a heart attack or stroke. After his emergency situation, Pinhas, a father of two, understands that. He's taking statins again and feels better. 
 "I go to the gym when I can and I take my meds," said Pinhas. "I experience some muscle aches but it's tolerable. I know I have to take these for the rest of my life. We'll see." 
