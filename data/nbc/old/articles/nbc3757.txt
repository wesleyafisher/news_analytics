__HTTP_STATUS_CODE
200
__TITLE
CIA, Other Spy Agencies Agree North Korea Can Fit Nuclear Weapon on Missile
__AUTHORS
Andrea Mitchell
Ken Dilanian
__TIMESTAMP
Aug 10 2017, 12:22 pm ET
__ARTICLE
 WASHINGTON  The CIA and other key U.S. intelligence agencies agree with the assessment that North Korea has miniaturized a nuclear weapon to place atop a ballistic missile, U.S. officials told NBC News. 
 The disclosure adds to the emerging picture about a new intelligence estimate that has significantly ratcheted up tensions between the U.S. and North Korea. 
 The news that North Korea had passed a significant milestone toward achieving a nuclear-armed missile that could hit the United States appeared to prompt a threat from President Trump to respond with "fire and fury," which spurred a counter threat from the North Koreans to attack the U.S. territory of Guam. 
 The Washington Post, which revealed the new intelligence Tuesday, said that it had been completed by the Defense Intelligence Agency, a unit of the Pentagon that specializes in examining the military capabilities of U.S. adversaries. It was unclear whether other agencies had signed on, and some observers noted that the DIA has a history of generously estimating the capacity of some opposing militaries. 
 Related: North Korea Can Fit Nuclear Weapon on a Missile Now, Officials Believe 
 But U.S. officials told NBC News that other agencies, including the CIA and the Office of the Director of National Intelligence, agree with the assessment. What has yet to be learned is what confidence level various agencies ascribe to the analysis  low, medium or high. 
 U.S. officials have also been pushing back on the idea that they were surprised by the development, saying that it had been expected for some time. But outside experts, including NBC News analyst Juan Zarate, who is a former top Treasury official, National Security Council staffer and deputy national security advisor, have said that it appears intelligence officials did not anticipate the speed of North Korea's advances. 
 Successful miniaturization does not mean North Korea now has a reliable nuclear-armed missile that can hit the U.S., but it puts the country further along that path. 
