__HTTP_STATUS_CODE
200
__TITLE
Senate Subpoenas Former Trump Adviser Carter Page
__AUTHORS
Marianna Sotomayor
Kasie Hunt
__TIMESTAMP
Oct 17 2017, 4:32 pm ET
__ARTICLE
 WASHINGTON  The Senate Intelligence Committee has subpoenaed documents and testimony from Carter Page as part of its investigation of Russia's alleged intervention in the 2016 election, a source directly familiar with the matter told NBC News. 
 The committee expects Page will invoke his Fifth Amendment rights and refuse to answer questions, the source said. 
 It's a dramatic switch for Page, who earlier this year seemed eager to participate in the Senate probe. Page has said he was interviewed by the FBI in March about potential ties between Donald Trump's presidential campaign and Russians. 
 In an email, Page told NBC News he plans to testify  though he is asking to do it in public on November 1, when the committee has scheduled an open hearing with social media giants including Twitter, Facebook and Google. 
 "I'm cooperating with everyone in DC who might want my assistance," Page said in an email. He called the Russia investigation a "witch hunt that was sparked by the dodgy dossier in the months prior to November 2016." 
 He did not confirm or deny that he had been served with a subpoena from the committee, after being repeatedly asked the question. 
 Page served as a foreign policy adviser to then-candidate Trumps campaign and he has drawn scrutiny for meeting with the former Russian ambassador Sergey Kislyak during the Republican National Convention last year. 
 During the campaign's transition to the White House, former press secretary Sean Spicer denied that Trump knew Page personally. 
 The subpoena comes after months of Senate Intelligence Committee interviews with people of interest in the Russia probe. Earlier this month, Chairman Richard Burr, R-N.C., and Vice Chairman Mark Warner, D-Va., announced the committee has analyzed over 100,000 pages of documents in the course of the investigation. 
 Burr and a spokeswoman for Warner declined a request for comment. "I don't talk about what we do or didn't do with subpoenas," Burr told reporters Tuesday. 
 Earlier this year, senators on the committee granted Burr and Warner blanket authority to issue subpoenas in the Russia investigation. Burr has previously told reporters that subpoenas could be issued if people of interest do not work with the committee. 
 "Everything has been voluntary up to this point, and weve interviewed a lot of people, and I want to continue to do it in a voluntary fashion," Burr said at a press conference in May. "But if in fact the production of things that we need are not provided, then we have a host of tools." 
 The committee has already issued three subpoenas to Trumps former national security adviser General Michael Flynn and his companies. 
 Page has repeatedly denied he has inappropriate ties to Russia and claims he did nothing wrong in the course of advising the Trump campaign. 
