__HTTP_STATUS_CODE
200
__TITLE
GOP Lawmakers Open Door to Arctic Drilling 
__AUTHORS
Alex Seitz-Wald
__TIMESTAMP
Oct 17 2017, 10:05 am ET
__ARTICLE
 WASHINGTON  Tucked into the budget bill the Senate is likely to pass this week is a provision that could quietly open the door to drilling in Alaskas Arctic National Wildlife Refuge  a controversial issue that has been the subject of fierce debate in the past. 
 The fight over arctic drilling was front-page news and became a major campaign issue when the Bush administration and congressional Republicans made their last major push for it in 2005, and it later gave Sarah Palin her famous "drill, baby, drill!" catchphrase. 
 The newer effort, however, has largely flown under the radar, concerning environmentalists since it's part of filibuster-proof legislative process that Republicans hope to use to pass tax reform. 
 "This is kind of a back-door attempt to open up the Arctic Refuge," said Athan Manuel, the director of the Sierra Club's Lands Protection Program. 
 Despite decades of trying, Alaska Republicans have yet to succeed in getting Congress to allow drilling in ANWR, as the refuge and the controversy are often known. But they see an opening, thanks to a Republican-controlled Congress and favorable administration under President Donald Trump. 
 "The road to energy dominance goes through the great state of Alaska," Interior Secretary Ryan Zinke said in a speech last month at the Heritage Foundation. 
 Zinkes department, which oversees oil and gas drilling on federal lands, recently moved to allow new testing of oil reserves in the refuge, a practice that environmentalists say is harmful and one they may try to block in the courts. 
 Advocates say drilling would help secure U.S. energy independence, lower gas prices, create jobs and help the state of Alaska, which depends on petroleum revenue to fund its budget. Critics dispute that and say drilling would permanently mar one of Americas last pristine wildlands. 
 The Senate plan includes a provision asking the Senate Energy and Natural Resources Committee to find $1 billion in new revenue, which Alaska Sen. Lisa Murkowski, a Republican who chairs that committee, is expected to do by proposing drilling in ANWR. 
 "This provides an excellent opportunity for our committee to raise $1 billion in federal revenues while creating jobs and strengthening our nations long-term energy security, Murkowski said in a statement. 
 The House last week passed a budget with a similar provision directing the House Natural Resources Committee, of which Alaska GOP Rep. Don Young is a senior member, to find new revenue. 
 Opponents like Tiernan Sittenfeld of the League of Conservation Voters view the procedure as an admission from pro-drilling lawmakers that they wouldn't be able to get a bill passed through the normal process. 
 "It's clearly so unpopular that they're trying to sneak it through," said Sittenfeld. "Our opponents have been trying to drill in this iconic place for more than 30 years. They always fall short." 
 Environmentalists hope to win over not only Republicans who oppose arctic drilling on its merits, but those who think a public fight on drilling will be a distraction from their goal of passing tax reform and those who object to the end-run process. 
 "The effort to open the Arctic Refuge to development is a long-debated and highly controversial issue that we do not believe belongs in a responsible budgeting process," six moderate Republicans in the House wrote the leaders of the House Budget Committee. 
 The entire budget process is likely to take months, and the ANWR fight will most likely enter into a more public phase soon. 
