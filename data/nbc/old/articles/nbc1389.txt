__HTTP_STATUS_CODE
200
__TITLE
U.S. B-2 Bombers Strike ISIS Camps Near Sirte, Libya: Officials
__AUTHORS
Courtney Kube
Alastair Jamieson
__TIMESTAMP
Jan 20 2017, 2:03 am ET
__ARTICLE
 U.S. bombers carried out airstrikes on two ISIS camps in Libya overnight, defense officials said Thursday, part of an operation targeting militants driven out last year from their coastal stronghold. 
 B-2 aircraft struck jihadis about 28 miles southwest of Sirte, which lies halfway between Tripoli and Benghazi. 
 One U.S. official told NBC News that "several dozen" militants were believed to have been killed in the strikes. 
 The operation was carried out in cooperation with Libyan government forces, according to the Pentagon. 
 Many of the targets had previously been inside Sirte until it was liberated from ISIS late last year. 
 ISIS was believed to have about 5,000 fighters in the area at the height of its influence, according to estimates from the head of the United States Africa Command. However, that number is now believed to be around 2,000. 
 Precision airstrikes in support of Libyan government forces against ISIS were launched in August 2016. 
 
