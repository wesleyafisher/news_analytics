__HTTP_STATUS_CODE
200
__TITLE
Suit: U.S. Firms Bribes Funded Iraq Militia Attacks on Americans
__AUTHORS
Ken Dilanian
Carol E. Lee
__TIMESTAMP
Oct 17 2017, 2:02 pm ET
__ARTICLE
 WASHINGTON  A group of American veterans filed a federal lawsuit Tuesday against U.S. and European drug companies and medical device makers, accusing them of supporting an Iran-backed Iraqi militia that killed and wounded hundreds of Americans. 
 The suit is filed under a law that allows Americans injured by terrorism overseas to seek civil damages. The more than 100 plaintiffs include injured U.S. Iraq war veterans and their families. The defendants are either parent companies or subsidiaries of AstraZeneca plc (AZN); Johnson & Johnson (JNJ); Pfizer Inc. (PFE); Roche Holding AG (RHHBY); and the General Electric Company (GE). 
 The 203-page lawsuit accuses the companies of paying bribes to officials of Iraq's health ministry that benefited the Mahdi Army, an Iranian-backed militia that the suit says worked closely with Hezbollah, a Lebanese group that has been designated a terrorist organization by the U.S. government. 
 The companies sold Iraq's government-run healthcare system millions of dollars worth of drugs and medical devices, and paid the bribes, the suit alleges, as a cost of doing business. 
 After the U.S. invasion, Iraq's political factions divvied up government departments under a political spoils system, and the Mahdi Army's political wing took control of the health ministry. The state-owned drug purchasing monopoly, Kimadia, demanded a 20 percent "religious tax" from the vendors on every contract, the lawsuit said. 
 "That policy required, as a general matter, companies doing business with Kimadia to pay commissions of at least 20 percent on every contract, the lawsuit says. "This was sometimes paid in the form of in-kind bribes but was often paid in cash as well. Such cash payments were typically called 'commissions'  the Iraqi euphemism for bribes. From 2004-2013, it was standard practice for companies dealing with (the Ministry of Health) to pay 'commissions' on every major sales contract." 
 The in-kind bribes were in the form of what were known as "free goods," including drugs and medical devices, that Iraqi officials would sell on the black market, says the lawsuit, which specifically accuses each defendant of paying such bribes. 
 The suit says the defendants paid the bribes even though they knew or should have known that the money was going to a dangerous armed faction. 
 "Defendants knew or recklessly disregarded that their transactions helped fund Jaysh al-Mahdi attacks on Americans," the suit says, using an Arabic term for the Mahdi Army. 
 In a statement, Pfizer said it "categorically denies any wrongdoing." Roche said it had not been served with the suit and could not comment. A GE spokesperson said the company became aware of the complaint Tuesday and is "thoroughly reviewing the allegations." The other companies did not immediately respond to requests for comment. 
 The Mahdi Army, led by Muqtada al-Sadr, is an Iranian-backed militia that opposed the American invasion and occupation of Iraq. It was never designated a terrorist organization by the U.S., a fact that could pose a legal hurdle for the plaintiffs. The suit argues that it acted as a terrorist organization and worked closely with Hezbollah, a designated terrorist organization. 
 Sadr posed a problem for the American war effort from the start, when he allegedly stabbed to death a U.S.-backed political opponent in 2003. American authorities issued a warrant for his arrest on murder charges but never executed it, fearful of the backlash it might cause among his Shiite supporters. 
 The U.S. thrice contemplated operations to kill Sadr but decided against doing so, a former CIA officer with personal knowledge told NBC News. 
 Sadr and his militia fought on and off for years against U.S. forces. In 2004, after the U.S. closed his newspaper, his forces seized control of Shiite areas around Iraq, leading to fighting that killed at least 19 Americans. 
 The lawsuit cites media and government reports asserting that the Mahdi Army worked closely with Hezbollah to attack American forces, including with sophisticated Iranian-build roadside bombs that penetrated U.S. armor. 
 Those bombs, known as explosively formed penetrators, or EFPs, killed 196 Americans and wounded 861 from 2005 to 2011, according to a U.S. military study cited in the lawsuit. 
 The suit was brought by the Washington, D.C.-based law firms of Sparacino & Andreson PLLC and Kellogg, Hansen, Todd, Figel & Frederick, PLLC. 
 One of the plaintiffs' lawyers, David C. Frederick, represented NFL players in their suit against the NFL over brain injuries. That case resulted in a $765 million settlement fund for players. 
 Some of the defendants have been accused in the past of violating the U/S. law against bribing foreigners to secure business. 
 In 2010, GE paid $23.4 million to the federal government to settle allegations by the Securities and Exchange Commission that its subsidiaries had violated the Foreign Corrupt Practices Act in connection with alleged kickbacks paid in Iraq under the United Nations' oil-for-food program. The GE subsidiaries were selling medical and water purification equipment to the Iraqi government. 
 The kickbacks were paid from 2000 to 2003 and consisted of cash, computer equipment, medical supplies, and services to the Iraqi Health Ministry or the Oil Ministry, the government said at the time. 
 In 2011, Johnson & Johnson agreed to pay a $21.4 million penalty to resolve criminal FCPA charges with the Justice Department, and another $48.6 million in disgorgement and prejudgment interest to settle the SEC's civil charges, in a case alleging improper payments to doctors in Greece by J&J subsidiary DePuy Inc. 
 Last year, AstraZeneca PLC agreed to pay more than $5 million in penalties after the SEC accused it of violating the books and records and internal controls provisions of the Foreign Corrupt Practices Act in connection with allegedly illegal payments made by subsidiaries in China and Russia to boost drug sales. AstraZeneca settled the enforcement action without admitting or denying the SEC's findings. 
