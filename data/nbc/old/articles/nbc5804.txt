__HTTP_STATUS_CODE
200
__TITLE
Eminem Attacks Trump in Four Minutes of Furious Freestyle Rap
__AUTHORS
Richie Duchon
__TIMESTAMP
Oct 11 2017, 2:11 am ET
__ARTICLE
 Eminem unleashed a fierce attack on President Donald Trump in a rap video broadcast during the BET Hip-Hop Awards on Tuesday night that went viral almost as soon as it was posted. 
 Over the more-than-4-minute verbal barrage dubbed "The Storm," Eminem, whose real name is Marshall Mathers, attacks Trump for his response to violence in Charlottesville, Virginia, his repeated assailing of NFL protesters and his war of words with North Korea. 
 In the video recorded Friday in the rapper's hometown of Detroit, according to Variety, a hooded Eminem cyphers in a parking structure as nine others look on in the background. 
 Eminem launches blistering attacks on the president, calling him a "kamikaze" that could cause "a nuclear holocaust" and a "racist 94-year-old grandpa." 
 Of Trump's series of tweets two weeks ago opposing NFL players' protests during the national anthem, Eminem said: 
This is his form of distraction
Plus, he gets an enormous reaction
when he attacks the NFL, so we focus on that
Instead of talking about Puerto Rico or gun reform for Nevada
All of these horrible tragedies and he's bored and would rather
cause a Twitter storm with the Packers
 Eminem issued an ultimatum to his fans who might be supporters of the president. 
And any fan of mine who's a supporter of his
I'm drawing in the sand a line
You're either for or against
And if you can't decide who you like more and you're split
On who you should stand beside
I'll do it for you with this
 The rapper follows the line with a middle finger to the camera. 
 At one point, Eminem raises a fist in the air, which he says is for Colin Kaepernick, the former San Francisco 49ers quarterback who was the first in the NFL to protest what he says is racial injustice in the United States by kneeling during the singing of the national anthem in a 2016 preseason game. 
 "I appreciate you @Eminem," Kaepernick tweeted Tuesday night. 
 Tuesday night's video wasn't the rapper's first attack on Trump. 
 During the 2016 presidential election, Eminem called Trump a "loose cannon" who doesnt have to answer to anyone in a nearly 8-minute flow titled, "Campaign Speech." 
