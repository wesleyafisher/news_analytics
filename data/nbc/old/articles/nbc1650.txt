__HTTP_STATUS_CODE
200
__TITLE
United Airlines Domestic Flights Grounded for 2 Hours by Computer Outage
__AUTHORS
Alex Johnson
Jay Blackman
__TIMESTAMP
Jan 22 2017, 9:18 pm ET
__ARTICLE
 All of United Airlines' domestic flights were grounded for more than two hours Sunday night because of a computer outage, the Federal Aviation Administration said as scores of angry travelers sounded off on social media. 
 International flights weren't affected by the ground stop, which the FAA said was issued at United's request. 
 "We're working to get flights on their way," United said in a statement shortly after 9 p.m. ET. "We apologize for the inconvenience to our customers." 
 U.S. officials told NBC News that the Aircraft Communications Addressing and Reporting System, or ACARS, had issues with low bandwidth. No further explanation was immediately available for what United described only as "an IT issue." 
United airlines is experiencing technical problems this evening. Please check your flight status to see if your flight is affected.
I so hate leaving my kids & then @united makes it worse by giving me my 8th cancellation or https://t.co/WWmY9ESLHT
Massive computer outage for @united causing delays all over the map. Currently stuck on tarmac in Denver. Damn you, Putin!
Just my luck. Flying the day there's a system outage on @united ????
 This is a developing story. Refresh this page for updates. 
