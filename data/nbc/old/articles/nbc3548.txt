__HTTP_STATUS_CODE
200
__TITLE
Ex-Guantanamo Detainee Jamal Al-Harith Became Suicide Bomber: Reports
__AUTHORS
Mark Hanrahan
__TIMESTAMP
Feb 22 2017, 9:47 am ET
__ARTICLE
 LONDON  A former Guantanamo Bay inmate who won a compensation after suing over his detention reportedly carried out a suicide bombing at a military base in Iraq. 
 A statement released by the militant group said a fighter it identified as Abu Zakariya al-Britani was behind a truck attack near Mosul on Sunday. It included a photograph of the alleged bomber. However, NBC News has been unable to verify whether the attack actually occurred. 
 NBC News has confirmed that the family of Jamal al-Harith identified him as the man in the picture distributed by ISIS that is said was al-Britani. 
 Born Ronald Fiddler, al-Harith is a 50-year-old British citizen and Muslim convert who was released from Guantanamo Bay in 2004. 
 Two British security officials told The Associated Press that the man in the ISIS photo was al-Harith. Reuters cited three Western security sources as saying it was highly likely that al-Britani was the bomber and now dead. NBC News was not able to independently confirm those accounts. 
 According to the AP, al-Harith went on a religious retreat in Pakistan in October 2001. In the days before the U.S.-led military campaign in Afghanistan began, he decided to return to Europe by land, but was captured on the Afghan border and handed over to the Taliban, who charged him with being a British spy. 
 He was subsequently freed by Northern Alliance forces, then turned over to U.S. troops and sent to Guantanamo Bay. Al-Harith said that he was tortured while at the detention facility. 
 He was released in 2004, along with four other British detainees who were held over alleged links to al Qaeda and the Taliban. The U.K. government lobbied for their release. 
 Al-Harith was one of 16 men who subsequently sued the British government, alleging that it was aware of or complicit in their treatment while in U.S. custody. 
 The group received a cash settlement in 2010, but details of the exact amount of individual payments are unclear. 
 The Associated Press reported the 16 were paid a total of 10 million pounds (now worth $12.4 million) in compensation. NBC News could not independently confirm that figure. 
 Britains Foreign Office would not comment on the identity of the man pictured in the ISIS photograph. 
 In a statement to NBC News, a spokesman said: The U.K. has advised for some time against all travel to Syria, and against all travel to large parts of Iraq  it is extremely difficult to confirm the whereabouts and status of British nationals in these areas. 
 If al-Harith is confirmed to have participated in a suicide bombing in Iraq, experts said that it fits the groups template for using foreign fighters in its propaganda strategy. 
 [ISIS] really wants to be the most global of jihadist organizations," Jonathan Russell, head of policy at the Quilliam Foundation, a London-based counter-extremism group, told NBC News. "They enjoy claiming that they have recruits from over 80 countries, because it shows that their appeal is global." 
