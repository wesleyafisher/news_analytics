__HTTP_STATUS_CODE
200
__TITLE
Strangers Help Search for Missing 20-Year-Old Man in New Jersey
__AUTHORS
Bianca Hillier 
__TIMESTAMP
Oct 16 2017, 6:08 pm ET
__ARTICLE
 Sometimes, young adults want time away from their parents. So on a Friday night, when 20-year-old Cody MacPherson told his mom to call him back the next day, she didnt think anything of it. 
 He told me he was with a girl, Codys mom, April Berry, told Dateline. 
 Even the next morning, Saturday, when Cody didnt answer, April wasnt too concerned. 
 Its not entirely unusual. I figured he was still with the girl, she said. [But] one of my family members called me on Sunday morning and asked if I had heard from him. They were pretty worried because Cody usually reaches out to somebody in the family. 
 That, April told Dateline, is when the familys investigation began. Codys phone was going straight to voicemail, though, and nobody could get ahold of him. Since April lives in Indiana, and Cody had been living in New Jersey with his great-uncle on his fathers side, April asked Codys stepmother, Missie, to go to the house where Cody was last seen to ask where he was. 
 The girl [who last saw Cody] told Missie that she didnt know where he was, April said. She said she last spoke to him at 4:00 a.m. on Saturday when he said he was leaving to go home. But his great-uncle said he never came home that night. 
 While April left to travel to Browns Mills to help with the search efforts, Codys brother called local police to report him missing. 
 On the morning of September 24, 2017, Cody was reported missing to our agency, Lt. Jay Watters, Patrol Division Commander of the Pemberton Township Police Department, told Dateline. We do have a detective assigned to this case and [he] did develop some leads as far as where he was last seen. Those were immediately followed up on and are still being followed up on. 
 The police came out immediately to look for him, April told Dateline from New Jersey. The only thing that bothers me is that they cant tell me anything about the investigation, so were just dangling here. 
 Lt. Watters told Dateline thats because Codys case is an open and active investigation. 
 There have been canine searches and ground searches, Lt. Watters said. As of right now, those searches have produced nothing of significance. 
 My gut is telling me that something bad did happen to him, because Cody wouldve at least reached out to somebody by now. He wouldve called and said, Mama, Im fine, April told Dateline. All his clothes are here. His medical paperwork, his high school diploma, his pictures of his daughter he wouldnt leave that behind. 
 April says shes overwhelmed by the amount of support Codys case has received. 
 I cant believe how many people care, she said. So many people are coming out and searching. Every day. People who dont even know him. 
 April also told Dateline its important for Cody to be found, as he had just started turning his life around for the better. 
 He has been arrested before, but that was why he came back to New Jersey  to start over and make money, so he could go back to Indiana and make a life for him and his daughter, she said. He really loved her and that was his plan. 
 He is very funny, goofy. A little bit of a tough guy at times, April added. He always showed he cared about people and would sit down and talk to people if they had issues. 
 The Pemberton Township Police Department is still conducting searches for Cody, but says one of the most important things at this point in a missing persons investigation is for people to come forward with any information they have on the case. 
 I understand that people are afraid to say something, April told Dateline. Theyre afraid theyll be called a snitch. But we really need help, because were running in circles right now. 
 Cody MacPherson is described as 5 11, approximately 140 pounds, with brown hair and brown eyes. He was last seen wearing a black shirt and baseball cap, blue jeans and green and black Nike sneakers. He also has tattoos on his chest, shoulder and hand. 
 Anyone with information is asked to call the Pemberton Township Police Department at 609-894-3310. If youd like to remain anonymous, you can call their confidential tip line at 609-894-3352. 
