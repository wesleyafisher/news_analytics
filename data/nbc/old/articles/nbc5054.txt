__HTTP_STATUS_CODE
200
__TITLE
Execution Leaflets Bring New ISIS Terrorism Fear to Pakistan
__AUTHORS
Mushtaq Yusufzai
__TIMESTAMP
Feb 14 2017, 6:32 am ET
__ARTICLE
 PESHAWAR, Pakistan  High in a remote, mountainous region of Pakistan, fearful residents have found leaflets containing ISIS propaganda that threaten attacks against their communities. 
 The sheets feature the black flag of ISIS, alongside pictures of an execution and armed militants waving automatic rifles as they parade through streets, thought to be in Iraq or Syria. 
 It's not clear who distributed the leaflets in the Kurram tribal area, and their origin cannot be independently verified by NBC News. 
 Pakistan's government says ISIS has no real organizational presence in the country, and the papers may have been produced by lone sympathizers rather than the group's more central elements. 
 However, American officials warned last year that ISIS is attempting to establish a foothold across the border with Afghanistan, and the group has claimed responsibility for several deadly attacks. 
 The leaflets' appearance has worried recipients that the influence of the militant group is spreading into new areas. 
 "We found these pamphlets in the morning," Amjad Hussain, 46, a tribesman in the town of Sadda, told NBC News. "I believe the militants came to our village in the night and distributed the pamphlets." 
 Another tribesman, Javed Hussain, 43, said that even though their hostile region has been targeted by other militant groups in the past, the specter of ISIS was more threatening still. 
 "The whole world knows about ISIS and its cruelties," he said, an illustration of just how effective the organization's propaganda machine is at spreading fear across the globe. 
 The text is in Pashto, a language spoken in some parts of Pakistan and Afghanistan. It warns Shiite Muslims, who make up most of the population, that they are now targets for ISIS, which practices an extreme version of Sunni Islam. 
 "We are going to launch attacks against the Shiite community ... by the help of Allah, we will soon clear these areas of infidels," said the leaflets, which were sent to NBC News by local tribesman Hussain Ali. 
 It also claimed that ISIS was "in contact with our fellow mujahideen"  an Arabic term meaning "holy warriors"  and that they would help coordinate these attacks on Shiites. 
 ISIS has teamed up with local Pakistani militants in the past. In October, the group cooperated with a radical organization called Lashkar-e-Jhangvi in an assault on a police academy in the city of Quetta that left 63 people dead. 
 More than 300 miles north, Kurram is no stranger to violence, with groups linked with al Qaeda and the Pakistani Taliban carrying out attacks in recent years. The latest of these came last month when the Pakistani Taliban claimed a bomb blast that killed 30 people at a fruit and vegetable market in Parachinar, the province's main administrative town. 
 But there's no evidence ISIS has ever planned to carry out an attack in Kurram  until now. 
 In response to the leaflets, locals held a meeting Thursday and demanded the country's security forces come up with a strategy to nip the potential threat in the bud. 
 "This is very, very serious threats to our community," local resident Akbar Hussain said. "We demanded of the government and military authorities to put strict security measures on the Pakistan-Afghanistan border so the terrorists can't enter Pakistan and kill our innocent people." 
 This threat is made worse, he said, because the security forces have confiscated weapons from local villagers in an attempt to curb violence in this restive region. 
 "Our border with Afghanistan is long and porous," Hussain added. "The terrorists can enter anywhere if proper security measures are not made." 
 Ikramullah Khan, the top administrative official from Kurram province, said he was aware of the leaflets and the case was being investigated. 
 Officials from Pakistan's government and military declined to comment when contacted by NBC News. 
 Pakistan's government officially denies that ISIS has any meaningful presence in the country, although it does admit the group enjoys sympathizers and supporters. 
