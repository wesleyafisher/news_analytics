__HTTP_STATUS_CODE
200
__TITLE
Looming Tie Leads to Rare Move from SCOTUS
__AUTHORS
Pete Williams
__TIMESTAMP
Mar 29 2016, 4:04 pm ET
__ARTICLE
 In a highly unusual move that could be designed to head off a 4-4 tie, the U.S. Supreme Court today asked new questions in one of the term's most contentious cases  the contraceptive requirements of Obamacare. 
 Less than a week after hearing arguments in the case, the court directed lawyers for the Obama administration and religious groups who challenge it to address a possible remedy. The prospect of a tie appeared possible after last week's argument. 
 Earlier in the day, the U.S. Supreme Court reached a tie vote in a case involving the nation's public sector unions. The tie vote leaves a lower court ruling standing  a victory for the unions because it rejected a challenge to the dues requirements. 
 Related: Supreme Court Tie Spells Win for Unions in Fee Case 
 In the Obamacare case, two dozen religiously affiliated schools, seminaries, hospitals, and charities from around the nation are challenging a provision of the Affordable Care Act that requires employers to include coverage for contraceptives in their health care plans. 
 Houses of worship and their auxiliaries are completely exempt from the requirement. The law allows religiously affiliated organizations to opt out of directly providing contraceptive coverage. 
 But the groups claim that the accommodation still makes them complicit in providing access to birth control and abortion, which violates their religious views. 
 Related: Supreme Court Appears Headed for Tie in Obamacare Case 
 During last week's argument, the court's three most conservative justices appeared to agree. "Hijacking is an apt description of what the government wants," said Chief Justice John Roberts. 
 In an order issued Tuesday, the court asked the lawyers to submit written briefs addressing whether the coverage could be provided through the group's insurance companies without any actual notice to the government. 
 Read: Text of Supreme Court Order 
 The lawyers were told to consider allowing the groups to "inform their insurance company that they do not want their health plan to include contraceptive coverage of the type to which they object on religious grounds." 
 The groups "would have no legal obligation to provide such contraceptive coverage, would not pay for such coverage, and would not be required to submit any separate notice to their insurer, to the federal government, or to their employees," the court's order said. 
 Related: Supreme Court Vacancy Complicates Debate Over Obamacare, Birth Control 
 Under such an arrangement, the court said, the insurance company would tell the group's employees that it would provide cost-free coverage separately, not through the group's health plan. 
 The court said the briefs must be submitted quickly, by April 12, with reply briefs from each side due by April 20. 
 Under the current law that's being challenged, an organization that objects on religious grounds to providing the coverage must notify the Department of Health and Human Services in writing and provide information about its health insurance plan. The government then notifies the insurer that it must assume the responsibility of providing contraceptive coverage at no cost to the beneficiaries. 
 Sister Constance Veit of the Little Sisters of the Poor, a group of nuns in Washington, DC acknowledged that under the accommodation, they would no longer be paying for contraceptive coverage. 
 "That's never been the point with us. The services would still become a part of our health plan, and that's just something we can't agree to," she told NBC News. 
