__HTTP_STATUS_CODE
200
__TITLE
Putting Together the Puzzle of What Happened After a Breach
__AUTHORS
Alyssa Newcomb
__TIMESTAMP
Sep 26 2017, 3:42 pm ET
__ARTICLE
 Equifax is being hammered with criticism for its handling of a data breach affecting 143 million people  the worst in U.S. history. 
 CEO Richard Smith abruptly retired on Tuesday, joining the company's CIO and CSO, who also departed after the massive hack was made public. 
 "There was a time when executives could get away with it," said Robert Siciliano, CEO of IDTheftSecurity. However, that era passed after a 2013 Target breach that affected as many as 41 million customers: The resulting fallout ultimately led to the resignation of the company's CEO and CIO. 
 Related: Could Europe Teach the U.S. a Lesson About Cyber Regulation? 
 While the CEO isn't the first line of defense when it comes to security management, the responsibility for a botched handling of a breach can often rest on his or her shoulders - and in the most egregious cases, Siciliano said the board may quietly ask them to resign or retire - or risk a public firing. 
 There are still plenty of questions to be answered about Equifax's handling of the breach, and the role that its top executives played. Already, Smith must testify before Congress next Wednesday, and Equifax currently faces an investigation by the Federal Trade Commission, the FBI, the CFPB, and the SEC, in addition to a grab bag of consumer lawsuits. 
 Dealing with a massive breach of customer data is a scenario security experts say they dread, but also one that must be met with a swift and thorough response, along with clear and contrite communication. 
 Before the headlines come out about a data breach, security teams are usually working nonstop in the hours, days, and months after an incident to piece together what exactly happened. 
 "My general experience is it takes multiple days or weeks to really get your arms around what has happened and to balance this against what the adversary is going to do with the data," Michael Daniel, president of the Cyber Threat Alliance and former cyber security coordinator in the Obama administration, told NBC News. 
 Every company's security infrastructure  and each breach  are undoubtedly different. However, security experts say there are usually some commonalities as to what's happening in those crucial days, weeks, months before the company tells the public. 
 Bob Lord, who joined Yahoo in 2015 as chief information security officer, described what it felt like to uncover the magnitude of two separate user account breaches the company suffered in 2013 and 2014, but did not publicly disclose until late last year. 
 If youre familiar with that effect that Alfred Hitchcock perfected  where things look like theyre sort of telescoping out. And you can still see everything but you still have this weird parallax going on, Lord said at TechCrunch Disrupt New York in May. I remember feeling that when I was putting all of the different pieces together. And thats not a great feeling. 
 Kristy Edwards, director of product management at Lookout, a mobile security company, said companies "see hundreds to many thousands of technical security events in their systems every day." 
 Figuring out what is an indicator of an actual, serious incident instead of just "noise," is part of the challenge, she said. That's when most companies will call their incident response team of security professionals into action. 
 "They'll do a risk analysis to assess the sensitivity of the data involved and the possible harm that could result," she said. 
 One of the first steps, Edwards said, is to take a forensic image of the server. This will allow the incident response team to uncover evidence from many complex computer systems used by the company. 
"As the investigation continues, it looks a little like a police investigation you see on TV," she said.
That could mean interviewing employees, examining CCTV footage and physical access logs. The executive team will be notified, as will legal counsel and law enforcement.
When it's evident a serious breach occurred, that also usually means hiring a third-party security firm to provide checks and balances to what the incident response team uncovers. The first look at what might have happened is often deceiving.
"The first review is not always right," Daniel said. "The return on the value of the data decreases rapidly but on the other hand you also very frequently learn a lot more when you really dig into the forensics. This stuff is often really complicated. Figuring out what happened is really challenging."
While all of this unfolds, the public is usually left in the dark about the news of a breach, but usually for good reason, Siciliano said.
"There have been times when they thought a breach was fully contained and it just brought more negative attention to their situation," he said.
And when it does come time to finally tell the public, the security experts NBC News spoke with said a clear communications strategy about what is known and what the company has done to fix it is vital, because they can expect plenty of questions.
"Good investigations leave no stone unturned as the team determines the who, what, when, where, and why of the situation," Edwards said.
