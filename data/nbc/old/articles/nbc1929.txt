__HTTP_STATUS_CODE
200
__TITLE
Mortgage Applications Rocket as Borrowers Rush to Lock in Rates Before Fed Hike
__AUTHORS
Diana Olick, CNBC
__TIMESTAMP
Mar 15 2017, 7:28 am ET
__ARTICLE
 Mortgage interest rates moved to the highest level since 2014 last week, as the Federal Reserve indicated it will more than likely increase short-term interest rates at its meeting Wednesday. 
 That lit a fire under homeowners who clearly saw this as a last chance to refinance at the lowest rates. 
 Total mortgage application volume rose 3.1 percent last week from the previous week. The seasonally adjusted tally from the Mortgage Bankers Association remains 12 percent lower than a year ago, when refinance volume was much more robust. 
 Refinance volume, however, did move 4 percent higher for the week, increasing to 45.6 percent of total applications. Refinance activity remains 27 percent lower versus a year ago. 
 "Surprisingly, refinance application volume increased for the week, perhaps a sign that homeowners see rates moving away from them and are moving to lock in now before rates increase further," said Mike Fratantoni, MBA's chief economist. 
 The average contract interest rate for 30-year fixed-rate mortgages with conforming loan balances of $424,100 or less increased to 4.46 percent, from 4.36 percent. 
 Mortgage applications to purchase a home, which are less sensitive to weekly rate moves, rose 2 percent for the week and are 6 percent higher than the same week a year ago. Mortgage rates were lower last year, but consumers are feeling slightly better about the economy today than a year ago and more millennials are aging into their homebuying years. 
 Related: Easy Ways to Save a Bundle on Your Next Home 
 "February's job report showed strong job growth and faster wage growth. We expect that the benefits from growing household incomes will continue to outweigh the headwind of slightly higher mortgage rates," Fratantoni said. "We continue to forecast strong growth in home sales this year." 
 The adjustable-rate mortgage share of activity increased to 8.2 percent of total applications, the highest level since October 2014. Adjustable-rate loans offer lower interest rates and are becoming more popular as rates rise and affordability weakens. 
 Mortgage interest rates continued to move higher Monday, as the yield on the 10-year Treasury bond, which mortgage lenders loosely follow, rose again. It held steady Tuesday, despite another rise in bond yields, but that is likely due to anxiety over the Fed's decision. The Fed's rate increase is expected, but the commentary from Fed Chairman Janet Yellen about the future is still up in the air. 
 "We won't know if the Fed's actual forecasts are faster or slower than expected until we see how markets react at 2 p.m. ET," said Matthew Graham, chief operating officer at Mortgage News Daily. "If the Fed accelerates less than expected, there is still a chance for mortgage rates to hold the line at the current ceiling (4.375 percent for top-tier, 30-year fixed scenarios for the average lender). If forecasts outpace expectations, rates could move higher quickly." 
