__HTTP_STATUS_CODE
200
__TITLE
Google Is Shaking Up Mobile Search With a Smart New Feed
__AUTHORS
Alyssa Newcomb
__TIMESTAMP
Jul 19 2017, 1:11 pm ET
__ARTICLE
 The next time you open Google's mobile app to search, expect to be greeted with an experience that feels less like the old Google you know  and something more along the lines of Facebook's News Feed. 
 Google introduced its feed in December, but new smart machine learning features rolled out on Tuesday take it one step further, creating a tailored experience. 
 The smart feed draws on your previous interactions with Google, your location, and what's trending in your area to deliver information you might want or need  without even having to ask for it. 
 The feed is arranged in "cards," so you can expect to see weather, sports scores, and news topics Google thinks are relevant to you. Some of the cards also link to other Google services, such as a video you may enjoy watching on YouTube. 
 Certain types of results, such as sports teams or television shows, will also have a "follow" button you can select when you search for it, ensuring you stay in the know. 
 Related: Google Has a New, Smarter Way to Help You Find a Job 
 While Google may know many of us almost a little too well, getting rid of topics you don't want to appear is easy. Just click on the three dots in the upper right corner of a card and select "Not interested." 
 And of course, if you'd rather not use the smart new features, you can opt out of the experience. 
 The majority of Google searches now come from mobile, making its search product of huge importance to the company, especially as it competes with Facebook, Amazon, Twitter and others for attention. 
 Google, of course, makes most of its revenue from selling advertisements. The current iteration of the new feed doesn't show sponsored cards but could provide another opportunity for Google to monetize. 
 "Right now we are heads down on making the feed work and have no plans around monetization," a Google representative told NBC News. 
 The current update is available in the English app for iOS and Android in the U.S.  with international versions coming in the future. 
