__HTTP_STATUS_CODE
200
__TITLE
Judge OKs Plea Deal for Robert Durst to Serve 7 Years, 1 Month in Prison
__AUTHORS
The Associated Press
__TIMESTAMP
Apr 27 2016, 10:33 am ET
__ARTICLE
 NEW ORLEANS  A federal judge in New Orleans has approved a plea agreement for real estate heir Robert Durst to serve 7 years, 1 month in prison on a weapons charge. 
 Judge Kurt Engelhardt approved the sentence Wednesday. The 72-year-old Durst agreed to the sentence as part of a guilty plea in February. 
 Durst still faces a separate murder charge in California. He is accused of killing a female friend in 2000 to keep her from talking to prosecutors about the disappearance of Durst's first wife in 1982. Durst has steadfastly maintained his innocence. 
 Real estate heir Robert Durst will soon learn whether a federal judge accepts a 7-year-and-one-month sentence on a weapons charge that's kept him in Louisiana pending a murder trial in California. 
 Judge Kurt Engelhardt will say Wednesday whether he approves that sentence, which the 72-year-old Durst accepted as part of his guilty plea in February. 
 He's charged in California with killing his friend Susan Berman in 2000. His attorneys have said repeatedly that Durst is innocent, doesn't know who killed Berman, and wants to prove it. 
 The most recent such statement was in a motion Monday asking U.S. District Judge Kurt Engelhardt to recommend that Durst serve his time at Terminal Island, California, about 30 miles from downtown Los Angeles, where Durst faces trial in Berman's death. 
