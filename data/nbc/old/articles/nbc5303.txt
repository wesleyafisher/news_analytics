__HTTP_STATUS_CODE
200
__TITLE
The Week in Pictures: July 14 - 21
__AUTHORS
NBC News
__TIMESTAMP
Jul 21 2017, 7:00 pm ET
__ARTICLE
Su-30 SM jets with the aerobatics team Russian Knights perform during the MAKS International Aviation and Space Show in Zhukovsky, outside Moscow on July 21.
An air tanker drops fire retardant on flames as firefighters continued to battle the Detwiler fire in Mariposa, California on July 19. The Detwiler fire has burned 110 square miles and destroyed 99 structures, 50 of them homes, forcing more than 4,000 people from homes in and around a half-dozen Gold Rush-era communities along the California mountains and foothills west of Yosemite National Park.
PHOTOS:California Wildfire Threatens Gold Rush-Era Towns
Wrestlers compete during the Highland Games in Inveraray, Scotland on July 18. The games celebrate Scottish culture and heritage with field and track events, piping, highland dancing competitions and heavy events, including the world championships for tossing the caber.
A man goes zorbing inside a giant plastic ball during a warm summer's day on the Vltava River in Prague, Czech Republic on July 19. Meteorologists predict summer temperatures of around 89 degreesFahrenheitin the Czech Republic over coming days.
Young women dressed as Quinceaneras walk through the Texas Capitol to visit lawmakers as they protest SB4, an anti-"sanctuary cities" bill, in Austin, Texas on July 19.
Texas' special session continues and conservatives in the legislature plan to work on anti-abortion measures, school vouchers and defanging local ordinances in Texas' big and liberal cities.
A firefighting aircraft flies over areas burnt by wildfires around Split, Croatia on July 18. Croatian firemen try to keep the wildfires under control along the Adriatic coast that have damaged and destroyed houses in villages around the city of Split.
An Atlantic Puffin holds a mouthful of sand eels on the island of Skomer, off the coast of Pembrokeshire, Wales on July 18.
People ride camels in the desert in Dunhuang, China on July 18, as stage 10 of The Silkway Rally continues.
Dancers from the group Lemi Ayo pose at an Afro-Brazilian festival, held next to the Valongo slave wharf, entry point to the Americas for nearly one million African slaves, on July 15, 2017 in Rio de Janeiro, Brazil.
The Valongo site was designated UNESCO heritage status on July 9 and the festival marked the distinction. The wharf was only recently discovered in 2011 during renovations in Rio's port district ahead of the Rio 2016 Olympic Games. Brazil is estimated to have received four million African slaves in total, approximately 40 percent of the total enslaved people transported to the Americas.
Lukasz Kubot (L) of Poland and Marcelo Melo (R) of Brazil celebrate winning against Oliver Marach of Austria and Mate Pavic of Croatia during their Men's Doubles final match for the Wimbledon Championships at the All England Lawn Tennis Club, in London, England on July 15.
Fireworks light the sky above the Eiffel Tower in the French capital of Paris on July 14 as part of France's annual Bastille Day celebrations.
PHOTOS:American in Paris: Trump is Guest of Honor at Bastille Day Parade
The Week in Pictures: July 7 - 14
