__HTTP_STATUS_CODE
200
__TITLE
How Working with Glass Helped Artist Kazuki Takizawa Open Up About Mental Health
__AUTHORS
Monica Luhar
__TIMESTAMP
Oct 12 2017, 1:15 pm ET
__ARTICLE
 The first time artist Kazuki Takizawa visited a glassblowing studio, he was captivated by its versatility and how molten glass transformed in an instant. Then a student at the University of Hawaii at Manoa, Takizawa said he fell in love with the medium and how working with it was both dangerous and required grace. 
 When [glass] is molten, its liquid, its really magical, and its very technical, he told NBC News. I was completely hooked. I also was drawn to the danger and thrill involved in the process of forming molten glass. 
 Takizawa found more than a favorite medium: Growing up in Thailand and Hong Kong, Takizawa said he often felt like he had trouble communicating due to language barriers. Art  especially glass  helped him better illustrate emotions he was not able to communicate in words. 
 I started my career as a student using other mediums, and I think I stuck with glass because of its qualities, which I really love to use as part of my language, Takizawa said. For example: transparency. And historically glass has been a medium as a vessel/container.  Theres no other medium that can really express that beauty. 
 Today, the 31 year old uses his work to communicate his experience battling mental illness while encouraging others to speak about mental health, which he noted can be stigmatized in Asian-American communities. 
 Asian Americans are less likely to seek mental health services than the general population, according to a 2007 study published in the American Journal of Public Health. The report found that 8.6 percent of surveyed Asian Americans sought help compared to 17.9 percent of the general population. 
 Takizawas own introduction to conversations about mental health didnt come until college, when a classmate told him she was concerned he might be depressed. He said he initially shrugged off the comments, but decided to see a doctor a few weeks later and was diagnosed with bipolar disorder. 
 In August, Takizawa gave a public talk about mental health at the California-based Nibei Foundation as well as the Tokyo Glass Art Institute in Tokyo, Japan. Several of his works have also dealt with mental health. 
 I have been using my work as a tool to tell a story of how I learned about depression, mental illness and suicide, he said. Being a glass artist, I am able to reach out to the art communities who may not have otherwise had the chance to talk about mental health in such a manner. 
 After college, Takizawa went through a period of depression where he felt he was unable to help himself or his brother, who Takizawa said was experiencing suicidal thoughts at the time. 
 Takizawa isolated himself for a while in order to maintain his own health. But art felt like a constant in his life. It was through art that I was able to face some of the emotions that were hard to face, Takizawa said. I think creating art and using it as an outlet to express has been an integral part of my mental health. 
A photo posted by NBC News (@nbcnews)
 One of his early installations includes Auric Shelter, which was part of his honors thesis in 2010. The piece is an 11-foot shell-like structure made up of more than 300 custom-colored glass panels. Takizawa hand-picked colors that he learned could be soothing. 
 The colors of the panels were selected based on chromotherapy studies, he said. While going to regular therapy sessions to battle my depression as a student, I was very fascinated in other forms of healing. 
 One of Takizawas favorite pieces is 2010s Re-puzzling Me, an installation that touches on his struggle with depression and thoughts of suicide at the time he created it. He said he didnt have the courage to speak about feeling suicidal at the time, so he made an art piece instead. 
 For Re-puzzling Me, Takizawa dropped a glass shell from a bridge and then glued hundreds of shattered pieces back together as a symbol of rebirth and hope. The sculpture is presented next to a length of cloth that he used to measure the distance of the fall. 
 Obviously, the glass shattered into millions of pieces, and [I] put them back together just to kind of express the emotional process that I sometimes go through when I feel like I shattered into millions of pieces, he said. 
 Another installation, Breaking the Silence, features hundreds of off-centered glass vessels placed close together while a water system drips from overhead. The water creates a harrowing sound and causes the glasses to fall and collide, creating a domino effect that then shatters the containers. 
 Its become a very great tool for me to speak about suicide and the glass water hitting the glass literally breaks the silence  the tinkering silence and also the breaking sound, said Takizawa, adding that the installation sheds light on his caring for his brother. 
 Takizawa also drew on his experience volunteering at a mental health services agency, where he took calls for the National Suicide Prevention Lifeline at Didi Hirsch Mental Health Services, for the piece. 
 One of the things I learned from their workshop is, statistically, if you lose someone through suicide around you, you are more likely to die by suicide, he said. That was the domino effect I wanted to express in my art. 
 According to data from the Centers for Disease Control, suicide was the 10th leading cause of death in the U.S. in 2015, the latest year that data is available, claiming the lives of more than 44,000 people. 
 People talk about killing each other, but we dont talk about...us killing ourselves...I think its a pretty substantial public health issue, said Takizawa, noting that the rate for suicide is higher than homicide according to the CDC. 
 In addition to glass, Takizawa has also worked with wood, stone, metal, and fiber, among other materials. 
 Takizawa said he is inspired by patterns in nature and often explores the concept of protecting ones life through his use of egg shells, sea shells, and similar objects that symbolize shelter and protection, noting a time in his life when he felt extremely isolated and wary of going outside. 
 An introvert, Takizawa said he has learned how to put himself in uncomfortable situations and be more vocal about his art and mental health through public talks. Since becoming an artist and speaking publicly about his diagnosis with bipolar disorder, Takizawa said he has met many inspiring people who live with mental illness; theyve inspired him to keep creating. 
 I really believe that what I am doing as an artist is creating positive change around me, and will continue to change as I develop as an artist, Takizawa said. For me, mental illness is very difficult to understand. Its not like getting a scrape and you bleed. It all starts in your head and until you start seeing the symptoms and behaviors, its all invisible. 
 Follow NBC Asian America on Facebook, Twitter, Instagram and Tumblr. 
