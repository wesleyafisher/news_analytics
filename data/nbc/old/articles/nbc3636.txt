__HTTP_STATUS_CODE
200
__TITLE
Opioid Prescriptions Are Down But Not Enough, CDC Finds
__AUTHORS
Maggie Fox
__TIMESTAMP
Jul 6 2017, 4:20 pm ET
__ARTICLE
 Doctors are cutting back on opioid prescriptions but not by nearly enough, federal health officials reported Thursday. 
 The number of prescriptions for the painkillers has tripled since 1999, driving the opioid addiction and overdose epidemic, the Centers for Disease Control and Prevention reported. 
 This means doctors have to cut back even more, saving opioids for acute pain  after surgery or for people with incurable cancer and other terminal conditions, for instance. Arthritis, back pain and other long-term conditions should be treated with other methods, CDC Acting Director Dr. Anne Schuchat said. 
 The CDC surveyed opioid prescriptions and broke them down county by county. 
 Despite significant decreases, the amount of opioids prescribed in 2015 remained approximately three times as high as in 1999 and varied substantially across the country, the CDC team said in their report. 
 Related: One in Five Insured Americans Got an Opioid Last Year 
 They used a measurement called morphine milligram equivalents  their best way to come up with one single way of describing the different drugs and different doses. 
 They found the number and amount of opioids prescribed peaked in 2010, at 782 morphine milligram equivalents per person that year. By 2015, that had fallen slightly to 640 per person. 
 Its way too much, Schuchat said. That 640 level is the same as if everyone in the country is on round-the-clock opioid medication for three weeks, she said. 
 Its still too high. We are heading in the right direction but we have a long way to go. 
 Theres no question the U.S. is suffering from an opioid abuse epidemic. Opioid overdoses killed more than 33,000 people in 2015, the CDC said. 
 Among opioid-related deaths, approximately 15,000 (approximately half) involved a prescription opioid, the CDC team said. 
 In addition, an estimated 2 million persons in the United States had opioid use disorder (addiction) associated with prescription opioids in 2015. 
 The survey also confirmed what many other reports have shown about the opioid abuse epidemic: Its hitting counties where people are mostly white, have lower incomes, less education and more unemployment. The more doctors and dentists there are per capita, the more likely people are to get prescriptions for opioids. 
 People in the worst-hit areas have higher rates of diabetes, arthritis and disability and have higher rates of suicide, the CDC team found. 
 Other research shows that once people become addicted, they often turn to cheaper street drugs such as heroin. 
 Theres no one single factor to blame for the epidemic. When synthetic opioids like as OxyContin and fentanyl hit the market in the 1990s, many doctors thought they were less addictive than opiates such as morphine or codeine. Manufacturers aggressively marketed them, and the pain community got organized and vocal in demanding them. 
 Clinicians did start to practice with the idea of trying to eliminate pain, Schuchat told NBC News. Pain isnt really a vital sign but many people started to track it. 
 The CDC has been trying to get doctors to prescribe opioids only when absolutely necessary, and to prescribe as low a dose as possible for the shortest time possible. 
 Related: Did This Letter Help Fuel the Opioid Crisis? 
 Previously, opioids had primarily been reserved for severe acute pain, postsurgical pain, and end-of-life care, the CDC team, led by health economist Gery Guy, wrote in the report. 
 This change in prescribing practice increased the amount of opioids prescribed for three reasons. First, opioid use for chronic non-cancer pain increased the number of opioid prescriptions. Second, the use of opioids to treat ongoing chronic conditions increased the average lengths of time for which opioids were prescribed, they added. 
 And the longer a patient takes an opioid, the higher the dose he or she needs. That raised dosages over time. Together, these changes placed more persons at risk for opioid use disorder and overdose, the CDC team wrote. 
 Schuchat said CDCs new guidelines have helped and she said the agency is beginning a new push to educate doctors and patients about when opioids are needed and when they are not. 
 Related: FDA Asks Drug Company to Pull its Opioid 
 CDC guidelines call for using acetaminophen, nonsteroidal anti-inflammatory medications such as ibuprofen, exercise therapy, and cognitive behavioral therapy for chronic pain. 
 What happens with chronic pain is that opioids, even high-dose opioids, wont make chronic pain go away, Schuchat said. 
 They are especially uncalled for in treating arthritis pain. 
 We think the most effective intervention for arthritis is actually exercise. We think that appropriate use of opioids can include post-operative or major surgical interventions and sometimes after injury. But its really short-term usage as opposed to for chronic pain, Schuchat said. 
 Regulation has also helped, the CDC team said. 
 In 2011 and 2012, Ohio and Kentucky, respectively, mandated that clinicians review Prescription Drug Monitoring Program data and implemented pain clinic regulation, the team noted. 
 There was a drop in opioid prescribing in 85 percent of Ohio counties and 62 percent of Kentucky counties by 2015. 
 In Florida, where multiple interventions targeted excessive opioid prescribing from 2010 through 2012, (e.g., pain clinic regulation and mandated Prescription Drug Monitoring Program reporting of dispensed prescriptions), the amount of opioids prescribed per capita decreased in 80 percent of counties from 2010 to 2015, the CDC team wrote. 
 During this time, Florida also experienced reductions in prescription opioid-related overdose deaths. 
 The American Medical Association said the study shows patients need to be able to easily get alternatives to opioids. 
 "Physicians must continue to lead efforts to reverse the epidemic by using prescription drug monitoring programs, eliminating stigma, prescribing the overdose reversal drug naloxone, and enhancing their education about safe opioid prescribing and effective pain management," Dr. Patrice Harris, chair of the AMA's opioid task force, said in a statement. 
