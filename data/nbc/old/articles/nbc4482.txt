__HTTP_STATUS_CODE
200
__TITLE
Las Vegas Shooter Seemed to Use a Machine Gun. But How?
__AUTHORS
Jon Schuppe
__TIMESTAMP
Oct 3 2017, 7:08 am ET
__ARTICLE
 The gunman opened fire from more than 1,200 feet away, raining rounds onto a concert crowd at a frequency that sounded to experts like it came from a gun common to the battlefield but rarely used in crimes: a fully automatic rifle. 
 But how did Stephen Paddock, who killed dozens and injured hundreds from a 32nd-story Las Vegas hotel window late Sunday, get such firepower, particularly when new machine guns have been banned for civilian use for more than 30 years? 
 There are basically three different ways to produce gunfire like that: use an older machine gun, customize a semi-automatic rifle to shoot like one, or use techniques or devices that mimic fully automatic fire. 
 Little of it is legal. 
 Law enforcement officials said Paddock had 23 firearms in his hotel room, but they have not said what kind. 
 Related: Las Vegas Shooting: 58 Killed, More Than 500 Hurt  
 A fully automatic firearm, commonly known as a machine gun, is extremely difficult to obtain. It fires continuously when the trigger is pulled, as opposed to a semi-automatic gun, which fires once per trigger pull. 
 Getting a machine gun requires special permission from the Treasury Department, a process that includes an FBI background check and approval from the buyer's local police department. A 1986 federal law prohibits civilians  people who are not members of law enforcement or the military  from possessing newly made machine guns. But those made before 1986 are allowed, and they can cost $20,000 on the private market. 
 Related: Las Vegas Shooter's Position in Mandalay Bay Room Amplified Massacre 
 New or not, all machine guns must be registered with the federal government. As of April, 630,019 machine guns were registered to civilians and police agencies in the United States  including 11,752 in Nevada  according to the Bureau of Alcohol, Tobacco, Firearms and Explosives. That number does not include military inventory. 
 Of the civilians, "they're mostly collectors, people who like to fire them at the range," said Robert Spitzer, a gun policy expert at the State University of New York at Cortland. 
 Because they are so rare and hard to come by, machine guns are hardly ever used in crimes, researchers say. Although there are no official statistics on such incidents, the ATF says it performed traces on 765 machine guns in 2015, a number that includes guns used in crimes but also those seized in other circumstances. A 2013 study by the Bureau of Justice Statistics reported that no more than 3 percent of prison inmates had a "military-style semi-automatic or fully automatic" gun at the time they were arrested. 
 There are at least two instances of machine guns used in killings in recent history: one by an off-duty Ohio police officer in 1988 and a 1992 case, also in Ohio, in which a disgraced doctor murdered a former colleague. 
 "They're extremely rare, and you almost never seen them in connection with guns collected by police," said Gary Kleck, a gun researcher at Florida State University. "Most big-city police departments, if you asked them the last time they had a crime where a fully automatic weapon was used, most will say they don't know of any. And in places where they are reputed to have been used, like Miami in the early 1980s, if you look at records, it's, say, two or three incidents over a period of years and often involve someone with unusual access to fully automatic weapons, like a trafficker from Colombia." 
 Related: Photos Capture Chaos of Concert Massacre 
 That having been said, it's not hard to find ways to shoot machine guns  especially in Nevada, a state with very permissive gun laws and a number of gun ranges that advertise packages that allow thrill seekers to fire them. Their machine guns are commonly obtained at the request of local law enforcement officials who have sought access to them for training purposes, said Nick Leghorn, senior editor at The Truth About Guns, a blog where he reviews firearms, and a frequent guest at such facilities. 
 At the same time, it's possible, but illegal, to convert a semi-automatic rifle, like the popular AR-15, into a fully automatic gun. That requires a high level of technical skill, although there are more rudimentary techniques that risk the gun's exploding, Leghorn said. 
 Clark County Sheriff Joe Lombardo said Monday he believed the weapons used by the shooter may have been modified from semi-automatic to fully automatic, but Lombardo said he had not seen the guns himself. 
 The desire among gun enthusiasts to increase the firepower of semi-automatic rifles has fed a market for devices that mimic automatic fire while technically hewing to the prohibition against machine guns. A number of them are advertised and celebrated on YouTube. 
 One is called a trigger crank. Another is known as a bump stock. Both use mechanical means to engage the gun's trigger faster. ATF closely reviews such products, and while they are legal, the agency has rejected others that it says make guns fully automatic. 
 The suspect had two "bump stocks," the Associated Press reported, citing two officials familiar with the investigation. Investigators are seeking to determine whether Paddock used the bump stocks to modify weapons used in the mass shooting, the Associated Press reported 
 Whatever gun Paddock, 64, used from his perch at Mandalay Bay Resort and Casino, it was not meant for choosing specific targets at the Route 91 Harvest festival, because while fully automatic gunfire can travel great distances  1,000 yards to 2 miles  it isn't very accurate, Leghorn said. 
 "It's used in the military to put rounds over the tops of heads of people who are shooting at you to get them to stop so you can move forward," Leghorn said. "You can't aim, especially from a distance and height like that. It wouldn't be aimed fire. It would be indiscriminate." 
