__HTTP_STATUS_CODE
200
__TITLE
Iraqi Families Flee Mosul as Army Battles ISIS
__AUTHORS
NBC News
__TIMESTAMP
Oct 24 2016, 4:34 pm ET
__ARTICLE
Iraqi families displaced by the ongoing operation by Iraqi forces to retake the city of Mosul from ISIS gather in an area near Qayyarah on Oct. 24.
The UN refugee agency is preparing to receive 150,000 Iraqis fleeing fighting around Mosul within the next few days, its chief said.
Women and children displaced by the conflict gather near Qayyarah on Oct. 24.
The Mosul campaign, which aims to crush the Iraqi half of the caliphate declared by ISIS in Iraq and Syria, may be the biggest battle yet in the 13 years of turmoil triggered by the U.S.-led invasion of Iraq in 2003, and could require a massive humanitarian relief operation.
Some 1.5 million residents remain in the city and worst-case forecasts see up to a million being uprooted, according to the United Nations. U.N. aid agencies said the fighting has so far forced about 6,000 to flee their homes.
Iraqi forces flash the victory sign as they stand on an infantry fighting vehicle loaded on a truck driving through the Al-Shura area, south of Mosul, on Oct. 24.
Iraqi forces advancing on Mosul faced stiff resistance from the Islamic State group Monday, despite the U.S.-led coalition unleashing an unprecedented wave of air strikes to support the week-old offensive. Federal forces and Kurdish peshmerga fighters were moving forward in several areas, AFP correspondents on various fronts said, but the jihadists were hitting back with shelling, sniper fire, suicide car bombs and booby traps.
