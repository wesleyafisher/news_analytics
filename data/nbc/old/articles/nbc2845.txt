__HTTP_STATUS_CODE
200
__TITLE
Cuddling Preemies Kangaroo Style Helps Into Adulthood
__AUTHORS
Maggie Fox
__TIMESTAMP
Dec 12 2016, 2:39 pm ET
__ARTICLE
 Cuddling small and premature babies in a style known as kangaroo mother care helps them in life decades later, researchers reported Monday. 
 They found that babies held upright and close to bare skin and breastfed, instead of being left in incubators, grew up with fewer social problems. They were far less likely to die young. 
 Its a reassuring finding for parents who may worry that tiny and premature babies are safer in an incubator than in their arms, the team wrote in their report, published in the journal Pediatrics. 
 Kangaroo mother care was first described in Colombia, and the team of experts there who first showed it was safe did a 20-year follow-up to see how the babies fared as they grew up. They tracked down 494 of the original 716 children who were born prematurely from 1993 to 1996 and randomly assigned to get either kangaroo mother care or standard handling. 
 Related: More U.S. Moms are Breastfeeding Their Babies 
 The effects of kangaroo mother care at one year on IQ and home environment were still present 20 years later in the most fragile individuals, and kangaroo mother care parents were more protective and nurturing, Dr. Nathalie Charpak and colleagues at the Kangaroo Foundation in Bogota, Colombia, wrote in their report. 
 At 20 years, the young ex-kangaroo mother care participants, especially in the poorest families, had less aggressive drive and were less impulsive and hyperactive. They exhibited less antisocial behavior, which might be associated with separation from the mother at birth, they added. 
 Kangaroo mother care may change the behavior of less well-educated mothers by increasing their sensitivity to the needs of their children, thus making them equivalent to mothers in more favorable environments. 
 Twenty million babies are born at a low birth weight every year around the globe, the World Health Organization reports. The U.S. has one of the highest rates of pre-term and low-weight births  about one in 12 births, according to the March of Dimes. 
 It defines low birthweight as being when a baby is born weighing less than 5 pounds, 8 ounces. 
 Most of these small babies are premature and they are at high risk of dying, of developing cerebral palsy, or having learning disabilities, and they can grow up more prone to a range of diseases. 
 High-tech care can help, but WHO promotes the simpler, low-tech approach alongside modern medical care  or instead of it in some poor settings. 
 Kangaroo mother care is care of preterm infants carried skin-to-skin with the mother. It is a powerful, easy-to-use method to promote the health and well-being of infants born preterm as well as full-term. Its key features are: early, continuous and prolonged skin-to-skin contact between the mother and the baby; exclusive breastfeeding (ideally); it is initiated in hospital and can be continued at home; small babies can be discharged early; mothers at home require adequate support and follow-up, WHO said. 
 It is a gentle, effective method that avoids the agitation routinely experienced in a busy ward with preterm infants. 
 And its safe, WHO added. Almost two decades of implementation and research have made it clear that kangaroo mother care is more than an alternative to incubator care." 
 Related: Too Many Babies Die on Their First Day 
 Charpaks team found the babies randomly assigned to get this treatment were 39 percent more likely to live into adulthood. They had stayed in school longer and earned more as adults. 
 It didnt work miracles. Children with cerebral palsy were equally likely to have symptoms whether they had the kangaroo care or not, and more than half the people in the entire group needed glasses. The children given standard care had higher math and language scores in school, while IQ levels were about the same in both groups. 
 But overall, the findings support the benefits of kangaroo mother care, the team concluded. 
 Our long-term findings should support the decision to introduce kangaroo mother care to reduce medical and psychological disorders attributable to prematurity and low birth weight, they wrote. 
 We suggest that both biology and environment together might modulate a powerful developmental path for these children, impacting until adult age, they added. 
 We firmly believe that this is a powerful, efficient, scientifically based health intervention that can be used in all settings. 
