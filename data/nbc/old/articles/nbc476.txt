__HTTP_STATUS_CODE
200
__TITLE
Analysis: Caitlan Coleman Hostage Rescue Hints at New Era With Pakistan
__AUTHORS
Wajahat S. Khan
__TIMESTAMP
Oct 14 2017, 8:02 am ET
__ARTICLE
 ISLAMABAD, Pakistan  The rescue of American hostage Caitlan Coleman and her family by Pakistan's military may prove to be a big step toward improving strained ties between Washington and its nuclear-armed ally. 
 Hours after details of the operation to free the Pennsylvania native from a horrific five-year ordeal emerged Thursday, statements by Pakistani authorities, the State Department and even President Donald Trump all praised the benefits of intelligence sharing and cooperation. 
 That appeared to indicate a positive turn in a relationship that has been fast deteriorating since the start of the Trump presidency. Before being elected to the White House, Trump had repeatedly tweeted that Pakistan "is not our friend." 
 This rescue is an example of what intelligence sharing and mutual respect can do, said Maj. Gen. Asif Ghafoor, a Pakistani military spokesman. It should now be clear to the Americans that cooperation works. Coercion and confrontation dont. 
 The Pakistan military said that it took action after being alerted by U.S. intelligence that Coleman, her Canadian husband, Joshua Boyle, and their three children were being moved across the border from Afghanistan. 
 They had been held captive by a Taliban-linked group. Boyle gave a harrowing account of their plight to reporters Friday, saying captors had killed their infant daughter and raped Coleman. 
 For years, the Pakistanis have been blamed by the U.S. for not doing enough in their counterterrorism efforts to end the war in Afghanistan  the longest military engagement in America's history. Islamabad has rejected the criticism, saying it has also suffered being an ally of Washington in the war against terror. 
 The country's military blames the Pakistani Taliban for the deaths of at least 60,000 civilians and 6,000 troops during its decade-long battle against the government. 
 In August, Trump warned that Pakistan had much to lose if it failed to cooperate with the U.S. in Afghanistan. He also expressed a desire to see India  Pakistans archrival  become an active stakeholder in stabilizing Afghanistan. 
 That set alarms ringing in Islamabad. 
 Trump's warning was followed by Secretary of State Rex Tillerson saying that Pakistan was also in jeopardy of losing its status as a major non-NATO ally if it continued to drag its feet. That would cut off the country from future weapons deals and military benefits. U.S. lawmakers also chimed in with threats of sanctions. 
 Pakistan potentially has a lot to lose: The U.S. has provided Islamabad with more than $33 billion in direct economic and military aid since 2002, according to the Congressional Research Service. 
 In response, the Pakistanis postponed a visit to Washington by Foreign Minister Khawaja Asif. And two administration officials  Alice Wells, the acting assistant secretary of state, and Lisa Curtis, the National Security Councils senior director for South Asia  were also "uninvited" from visiting Islamabad, according to a senior Foreign Office official. 
 By early this month, the warnings and demands from Washington for Pakistan to do more had prompted a change in approach. As the media turned virulently anti-American and protests against the U.S. started gathering momentum, Interior Minister Ahsan Iqbal lashed out at the U.S. and told a crowd in Islamabad on Oct. 3 that the answer for those asking us to 'do more' is 'no more.' Enough is enough. 
 Related: Nuclear-Armed Pakistan Is Paralyzed by Legal Gridlock 
 But on Thursday, both Wells and Curtis were in Islamabad for meetings with Pakistani diplomats and generals when details of Coleman and her family members' successful rescue were disclosed. 
 Pakistani officials said infantry and intelligence personnel had tracked and shot the tires of an SUV that had crossed the border from Pakistan, thanks to a tip-off by U.S. intelligence. 
 News of the rescue produced glowing praise of Pakistan from Trump and Tillerson. 
 "This is a positive moment for our country's relationship with Pakistan," the president said in a statement. "The Pakistani government's cooperation is a sign that it is honoring America's wishes for it to do more to provide security in the region." 
 That represented a softer tone toward Pakistan from Trump. 
 In January 2012, he tweeted: "Get it straight: Pakistan is not our friend. Weve given them billions and billions of dollars, and what did we get? Betrayal and disrespectand much worse. #TimeToGetTough" 
When will Pakistan apologize to us for providing safe sanctuary to Osama Bin Laden for 6 years?! Some "ally."
 Analysts say that as long as Pakistan is assured that America is not acting as a proxy for India in efforts to dominate the region, it is likely to assist with U.S. demands. 
 For too long, the mutual convergence of the two countries has been held hostage by the bitterness of real and perceived betrayals on both sides, said Mosharraf Zaidi, a former foreign policy adviser to Pakistan's government. If Pakistan is confident that it will not be tarred and feathered and will instead be celebrated for its contributions, the bilateral relationship will only improve. 
 Related: Here's Why Pakistan Finally Arrested Alleged Terror Mastermind 
 Shahzad Chaudhry, a retired vice marshal of the Pakistan air force, said the country's intervention in the Coleman case illustrated anew the mutual benefits of cooperation. 
 "The essence in this episode is of intelligence sharing, Chaudhry said. When in the know, Pakistan will act." 
 But retired Lt. Gen. Asad Durrani, a former chief of Pakistan's powerful intelligence agency, ISI, was more skeptical, saying the country should have learned a lesson after helping the Americans find Osama bin Laden. 
 "When did doing a favor to the U.S. ever get us any dividends?" he asked. "We played poodle after 9/11, but know what happened thereafter. They want us to chase their enemies and make them ours." 
 He added, "Being the enemy of the U.S. is dangerous, but being its friend is fatal." 
