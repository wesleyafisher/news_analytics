__HTTP_STATUS_CODE
200
__TITLE
Trump Attacks Puerto Rico, Threatens to Pull Emergency Responders
__AUTHORS
Adam Edelman
__TIMESTAMP
Oct 13 2017, 8:20 am ET
__ARTICLE
 President Donald Trump slammed Puerto Rico on Thursday, saying its power grid and infrastructure were a "disaster" before two hurricanes hit last month and he threatened to pull federal emergency management workers from the storm-ravaged island. 
 His comments drew furious responses from Democrats who charged he is treating Americans there like "second-class citizens." 
 Trump tweeted, "'Puerto Rico survived the Hurricanes, now a financial crisis looms largely of their own making.' says Sharyl Attkisson. A total lack of........accountability say the Governor. Electric and all infrastructure was disaster before hurricanes." 
 (It wasn't clear why Trump referred to Attkisson, a conservative journalist who has repeatedly defended him.) 
 "Congress to decide how much to spend.......We cannot keep FEMA, the Military & the First Responders, who have been amazing (under the most difficult circumstances) in P.R. forever!" Trump wrote in another Tweet. 
"Puerto Rico survived the Hurricanes, now a financial crisis looms largely of their own making." says Sharyl Attkisson. A total lack of.....
...accountability say the Governor. Electric and all infrastructure was disaster before hurricanes. Congress to decide how much to spend....
...We cannot keep FEMA, the Military & the First Responders, who have been amazing (under the most difficult circumstances) in P.R. forever!
 Trump's latest attacks on Puerto Rico immediately touched off harsh criticism from Democrats. 
 The barrage of tweets come just weeks after Puerto Rico was slammed by Hurricane Maria. FEMA said that as of Tuesday, 21 days since Maria made landfall, 84 percent of people on the island remained without electricity and only 63 percent had potable water. Many others have been without housing and basic necessities. 
 Senate Minority Leader Chuck Schumer of New York tweeted a question to the president: "Why do you continue to treat Puerto Ricans differently than other Americans when it comes to natural disasters?" 
 "FEMA needs to stay until the job is done and right now, it's not even close to done. There is still devastation, Americans are still dying. FEMA needs to stay until the job is done," he added. 
 House Minority Leader Nancy Pelosi of California posted, "We don't abandon Americans in their time of need." 
 "PR & USVI need MORE help, not less, from the federal govt," Pelosi wrote, referring to the U.S. Virgin Islands. At a press conference later in the morning, Pelosi called Trump's tweets "heartbreaking." 
 Rep. Darren Soto of Florida told NBC News: "Trump continues to treat Americans in Puerto Rico as second-class citizens. He wouldnt be saying this about federal recovery efforts in Texas or Mississippi." 
 House Minority Whip Steny Hoyer of Maryland said: "It is shameful that President Trump is threatening to abandon these Americans when they most need the federal governments help. He has a responsibility to our fellow citizens in Puerto Rico and on the U.S. Virgin Islands to ensure that every federal resource is made available to assist in recovery and rebuilding for as long as it takes." 
 Reaction from Puerto Rico was swift, too. 
 In a lengthy statement, San Juan Mayor Carmen Yuln Cruz, who Trump attacked after she made headlines criticizing his response to the disaster, asked "every American that has love, and not hate in their hearts, to stand with Puerto Rico and let this President know we WILL NOT BE LEFT TO DIE." 
 In addition, the island's governor, Ricardo Rossell, tweeted that "U.S. citizens in Puerto Rico are requesting the support that any of our fellow citizens would receive across our Nation." 
 Just over 24 hours after his original tweets, Trump returned to Twitter Friday to say he would "always be" with the people of Puerto Rico, but repeated his original claim regarding "how bad things were" on the island before the hurricanes. 
 "The wonderful people of Puerto Rico, with their unmatched spirit, know how bad things were before the H's. I will always be with them!" he tweeted Friday. 
 Maria Gonzalez, who has remained stranded in a remote area of western Puerto Rico, broke down in tears this week as she told NBC News that she had cancer and was in grave need of a generator. 
 She offered a desperate plea to FEMA: "Come on! Do what you're supposed to do." 
 FEMA says that 19,000 civilian and military service personnel are working in Puerto Rico and the U.S. Virgin Islands. The agency is hosting job fairs in Ponce to hire 1,200 people for the relief effort. 
 Later Thursday, the acting secretary of the Department of Homeland Security, Elaine Duke, is planning a return visit to the Puerto Rico amid the announcement Wednesday that FEMA is expanding its leadership team there. 
 Nevertheless, criticism has steadily mounted over the Trump administrations response to what is being called an unfolding humanitarian crisis, with some likening the situation to the aftermath of Hurricane Katrina. Rossell raised the death toll from the storm to 43. 
 But Trump himself has repeatedly lashed out at Puerto Rican officials, including Cruz, who has pleaded for more federal assistance. 
 On Sept. 30, Trump tweeted that Cruz had "poor leadership ability" and said Puerto Rican officials "want everything to be done for them when it should be a community effort." 
 Days later, during an Oct. 3 visit to the island, Trump said U.S. relief efforts in Puerto Rico, bankrupt before the storm, had "thrown our budget a little out of whack" and the president was filmed casually tossing rolls of paper towels into a crowd at a disaster relief center. Cruz has criticized Trump for the incident, calling it "terrible and abominable." 
 Vice President Mike Pence, however, said during his own visit to the island last week that the White House would stick with Puerto Ricans for as long as the recovery would take. 
 "We are with you today, we will be with you tomorrow, we will be with you every day until Puerto Rico rebuilds and recovers bigger and better than ever before," Pence said last Friday. 
