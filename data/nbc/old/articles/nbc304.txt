__HTTP_STATUS_CODE
200
__TITLE
Anti-Vax Message Gets Meaner on Social Media
__AUTHORS
Maggie Fox
__TIMESTAMP
Oct 11 2017, 10:45 am ET
__ARTICLE
 Its getting ugly out there on social media. 
 Anti-vaccine activists are attacking pediatricians head to head on Twitter, Facebook and other social media platforms, and theyre not hesitating to make their attacks personal. 
 A few fed-up doctors are fighting back, both online and, more recently, in the courts. 
 One pediatrician, Dr. Eve Switzer of Enid, Oklahoma, filed a defamation suit last year against former Oklahoma ophthalmologist Jim Meehan and Oklahomans for Vaccine and Health Choice, saying they publicly and falsely accused her of failing to provide informed consent to parents about vaccinations. 
 Anti-vaxxers have gotten out of control, Switzer told NBC News. 
 As a pediatrician, the way that it has affected me personally, my practice and my staff, even, I really felt like I needed to have some sort of remedy and my only remedy was a legal remedy. 
 It's difficult enough to counter the continual questions raised by vaccine skeptics and activists who, increasingly, say all vaccines are unsafe for all children. 
 Worries about autism, a preservative called thimerosal, the spacing of vaccines, vaccine ingredients and whether kids get too many vaccines have all been repeatedly and thoroughly addressed, but that doesnt stop users of social media from bringing them up again and again. 
 Going up against the anti-vaccine movement is a thankless task for a number of reasons. For one, the goalpost is moving, said Dr. Peter Hotez, director of the Texas Children's Hospital Center for Vaccine Development and dean of the school of tropical medicine at Baylor College of Medicine. 
 So if you can explain why MMR (measles, mumps and rubella) vaccine doesnt cause autism, theyll turn around and say well it must be thimerosal in vaccines. If you debunk this, then theyll say well we are spacing vaccines too close together and if you debunk that and then its aluminum in vaccines, he added. 
 I call it the global health whack-a-mole. 
 Using social media takes it to a new level. 
 Just as online media allow people to stay in their own political echo chambers, viewing and sharing only their own beliefs, use of social media is allowing vaccine skeptics to harden their messages. Facebook feeds offer continual validation with video after video of parents who say they believe vaccines gave their children eczema or asthma or even killed their babies  claims made with no medical backing. 
 Related: Vaccine Skeptic Message Gets Bolder 
 And on Twitter, any post about the benefits of vaccines  all supported by decades of science  is often immediately attacked. 
 Most recently, a Detroit-area mother jailed for a week for refusing to follow a court order that involved vaccinating her kids got an instant support group. Although the judge in the case, Circuit Court Judge Karen McDonald, made it clear the sentence was about disregarding a court order that Bredow had initially agreed to, Bredow herself and anti-vaccine groups framed it as having taken a stand for her beliefs. 
"...Judge McDonald makes clear her forced vaccination agenda." #RebeccaBredow https://t.co/W6Lt5HO33e
 In reality, there is no legitimate debate about the safety or benefits of vaccines. While no medical group claims vaccines are 100 percent safe, the Centers for Disease Control and Prevention, National Institutes of Health, and dozens of academic researchers independent of the federal government have shown the benefits of vaccines both to individual children and adults and to the population at large. 
 Childhood vaccination saves up to 3 million lives every year, but 1.5 million people still die each year from diseases that could have been prevented by vaccines, the nonprofit Sabin Vaccine Institute says. 
 But you might not know that if you take a look at social media. 
 Its more systematic and more aggressive and more effective, said Hotez, who is also a pediatrician, the parent of a young woman with autism, and a vocal advocate for vaccination. 
 Theres a new element to the anti-vaccine group that has sort of an alt-right flavor to it. 
 Research backs this up. 
 Theodore Tomeny of the psychology department of the University of Alabama and colleagues tracked Twitter activity and found more than 270,000 tweets containing anti-vaccine statements between 2009 and 2015  half of all the tweets they found about vaccines in general. 
 California, Connecticut, Massachusetts, New York and Pennsylvania were particularly rich in anti-vaccine Twitter users. 
 We found that anti-vaccine sentiment seems to be prevalent on Twitter, Tomeny said. It also seems to cluster geographically. We observed spikes over time that seemed to be associated with current events like one of the recent measles outbreaks in California. 
 They not only repeat untrue rumors about vaccines, but accuse government and academic doctors of colluding with pharmaceutical companies to cover up the dangers of vaccines, without real evidence to back up these accusations. 
 Pediatricians, public health experts and others would do well to monitor this type of social media use and counter its influence, Tomeny and colleagues wrote in the October issue of the journal Social Science & Medicine. 
 The instinct for some may be to just turn them off. 
 Thank god for the mute and block function, Hotez said. But Hotez says its also important to counter some of the more viral wrong stuff. 
 Related: Detroit Mom Jailed, But Was it Really About Vaccines? 
 I think its very important to get the facts out there. So I have put a lot of effort into explaining why vaccines dont cause autism, he said. 
 The argument that measles is not dangerous  I think that is important to refute. The allegation that I am making millions of dollars off my vaccine  I think its important to refute that. 
 Texas Children's took over work from the Sabin Vaccine Institute to develop vaccines to prevent parasitic diseases such as hookworm and schistosomiasis. They dont seem to understand that these are vaccines for the worlds poorest people that will never make any money, Hotez said. 
 Its not difficult to find personal attacks against Hotez on the internet, including blogs that outright call him a liar. Dr. Paul Offit of Childrens Hospital of Philadelphia is a veteran of such personal attacks that even extended to threats against his kids. 
 Related: Vaccine Expert Takes on Alternative Medicine Industry 
 But with universal access to social media giving a platform to anyone with a smartphone, any pediatrician who is vocal about the anti-vaccine movement, or who even posts something public about the benefits of vaccines, can be targeted. 
 Tomeny said it may be the impersonal aspect of social media. Perhaps its the fact that it is not face-to-face communication, he said. 
 You are not having to view the other persons reactions and temper your statements based on it. 
 Switzer has found that to be the case. While she finds it easy to answer polite questions from parents in her office, shes had a different experience online. 
 Most of what I get thats confrontational is on social media, she said. 
 This is a group of people who when I say anti-vaxxers, these are the people I am talking about  the people who are confrontational. They adamantly believe that vaccines arent safe or effective. They really prefer to perpetuate myths about them, things that science just doesnt support. 
 New parents who may not be familiar with the medical evidence may hear some of these claims and wonder if they are true. 
 Sometimes all it takes is implanting that teeny little bit of doubt into a parent, Switzer said. 
 So if we ignore these anti-vaccine people with all of the nonsense that they are putting on social media, I really feel like that it gives them a greater voice. I think more pediatricians should be involved in putting stuff out on social media to counter the misleading and just blatantly false information that they use. 
 So Switzer is not only taking the case public. Shes also suing, and another group of pediatricians say they have filed a formal complaint again Meehan with the Oklahoma State Board of Medical Licensure. 
 We, the undersigned, wish to file a formal complaint against James C. Meehan, MD, for his unprofessional behavior on social media directed at each of us, the complaint reads. It's signed by eight physicians and medical students, including Dr. Christopher Johnson, a pediatric critical care specialist in Santa Fe who lists a torrent of abusive tweets from Meehan in the complaint, as well as Dr. Alastair McAlpine of Red Cross Children's Hospital in Cape Town, South Africa, also a Meehan target. 
 Lyle Kelsey, executive director of the board, said he could not confirm or deny that the report had been filed until the board decided to investigate, a process, he said, that could take months. 
 But its not hard to find Meehans combative posts on Twitter. 
Look who's talking...The most condemnable and hypocritical pediatrician in America. pic.twitter.com/cWx7rz2rdg
 The level of ignorance & amount of misinformation parroted by these medical doctors should & will haunt them as the truth of harm is exposed, Meehan says in one. Meehan, an ophthalmologist by training, says he now has a wellness practice in Tulsa. 
 Scott, only a bottom of the med school class, weak, bought, anti-science, vax pusher look (sic) you would argue there is no debate re vaccines, Meehan blasts in another directed at Dr. Scott Krugman, chairman of the Department of Pediatrics at MedStar Franklin Square Medical Center in Perry Hall, Maryland. 
 I will not be engaging, but this is a nice example of what this crew does to pediatricians on a daily basis, Krugman said. 
 Meehan is just as combative in person. No one wants to debate me, he told NBC News. 
 Related: Experts Have Found the Truth About Vaccines - Over and Over 
 Hotez said its fruitless to enter a debate when facts are disregarded. 
 And, he and other experts point out, the anti-vaccination arguments are doing real harm. Earlier this month a study published in the Journal of the American Medical Association showed measles outbreaks are probably caused by people failing to vaccinate their children. 
