__HTTP_STATUS_CODE
200
__TITLE
Trump: Ask Gen. Kelly Whether Obama Called When His Son Died
__AUTHORS
Ali Vitali
__TIMESTAMP
Oct 17 2017, 7:42 pm ET
__ARTICLE
 WASHINGTON  President Donald Trump on Tuesday defended his false claim that his predecessor didn't call the families of service members killed in action by alluding to former Gen. John Kelly's son, a Marine who died in Afghanistan. 
 "You could ask General Kelly, did he get a call from Obama?" Trump said in a radio interview with Fox News' Brian Kilmeade. 
 Kelly, who came on as Trump's chief of staff in July, does not often speak about the son he lost in 2010. 1st Lt. Robert Michael Kelly, 29, was killed in combat in Afghanistan after stepping on a landmine. 
 "I don't know what Obama's policy was. I write letters and I also call," Trump said, adding he has called "virtually everybody" during his past nine months as commander in chief. 
 Trump's comments Tuesday come a day after he falsely claimed that President Barack Obama did not call the families of service members killed in action after being asked why he had not yet addressed the deaths of American service members killed in Niger earlier this month. 
 When pressed by NBC News on how he could make that claim, Trump said he was told that Obama "didn't often" call families of fallen service members. 
 "President Obama, I think, probably did sometimes, and maybe sometimes he didn't. I don't know. That's what I was told. All I can do  all I can do is ask my generals," he said. 
 According to a 2011 profile in The Boston Globe, Kelly has avoided speaking publicly about his son so as not to draw attention. 
 "We are not inclined to make ourselves out to be any different, just because Im a lieutenant general in the Marines," Kelly said then. "We are just one family. It's not worse for us; it's not easier for us." 
 A White House official told NBC News on Tuesday that Obama did not call Kelly after the death of his son. But a person familiar with the breakfast for Gold Star Families at the White House on May 30, 2011, told NBC News that Kelly and his wife attended the private event and were seated at first lady Michelle Obamas table. 
 A former senior Obama administration official disputed Trump's initial claim on Monday that Obama didn't call Gold Star families, calling it "wrong." 
 "President Obama engaged families of the fallen and wounded warriors throughout his presidency through calls, letters, visits to Section 60 at Arlington, visits to Walter Reed, visits to Dover, and regular meetings with Gold Star Families at the White House and across the country," the ex-official told NBC. 
 Trump has now spoken to the families of the four troops killed in Niger, the White House confirmed Tuesday. 
 "He offered condolences on behalf of a grateful nation and assured them their family's extraordinary sacrifice to the country will never be forgotten," Press Secretary Sarah Huckabee Sanders said. 
