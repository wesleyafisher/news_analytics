__HTTP_STATUS_CODE
200
__TITLE
How to Balance Your Business With Your Creativity
__AUTHORS
Cassandra Girard
__TIMESTAMP
Oct 13 2017, 11:18 am ET
__ARTICLE
 When Jennifer McGarigle brought her vibrant creations to market over 20 years ago, the world of artisan floristry was thriving. As the owner of Floral Art, based in Los Angeles, California, she went from the set of the Oprah Winfrey Show to Hollywoods red carpets. Today, the floral industry looks dramatically different, but McGarigle is one of the surviving few with an ever-blooming business. 
 As a creative force first, McGarigle has always felt the challenge of figuring out how to balance her visionary side with the business owner in her. Ive had a lot of trial and error, says McGarigle. Im obsessed with making things, so Ive got to put myself in check and say, Whats the strategy here and whats the plan? 
 Now, after two decades in business, McGarigle says shes finally learned a thing or two about balancing business with creative arts. Here are a few of her tips: 
 McGarigle says it helps to create separate environments  one to handle the nuts and bolts of your company and another that allows your imagination to run wild. 
 Setting up a system like this has enabled her to keep everything in focus and her business moving forward. I do some of my work from my home office and thats my brain time, says McGarigle. When I am connected with my team and our productions, I am at the studio. Its two different worlds. 
 McGarigle also takes a unique approach to problem solving on the business front that appeases the artist in her. When I come up against challenges, Ive found that if I turn it into a design challenge, I become inspired, says the floral artist. Then it becomes fun and its not overwhelming. 
 Something else that doesnt overwhelm McGarigle is seeing Floral Art grow throughout the digital age. As social media became more and more important for any business, McGarigle immediately recognized that the worlds of Instagram and Pinterest were not her strengths. She knew that staying in her creative element was key. I dont feel compelled to keep my phone at my hip, says McGarigle. Thats not my favorite thing. Id rather be right here, right now, versus thinking about what something is going to look like on camera. 
 She didnt ignore the clear need for someone to grab hold of the social media reins. I do understand the marketing value of social media, which is a good reason to outsource, says McGarigle. Its less to manage, especially on a topic that Im not an expert on. 
 In 2009, McGarigle took a bold step, expanding her line to include furniture and accessories. At the time, she wasnt too concerned about the potential revenue. Designing these pieces has become part of an ongoing passion project. 
 Im going to make them no matter what, says the floral furniture designer. I will build it into my business plan. 
 By taking these unexpected creative roads, without having to focus on the dollars and cents, McGarigle has continued to fuel her love of floral design, and she is just as inspired as the day she launched her business back in 1993. 
 In recent years, McGarigle has opened the doors to her 5500 sq. ft. space to the public. She holds monthly floral design workshops there. 
 Attendees at these evening classes will not only learn some of McGarigles highly sought after techniques, but also get to take home their very own arrangement. 
 When people come in, we have a social half hour, says McGarigle. And then we do the arrangements, step-by-step. Each night has a theme based on anything from a specific flower or color, to places and holidays. The cost for an individual workshop is $150. 
 Although Floral Art used to have a retail space, and McGarigle has launched pop-up shops from time to time, she is happy waiting for just the right moment to open another brick-and-mortar. Were not dependent on it, says McGarigle. Nevertheless, shes not opposed to potentially opening up another retail store in the future. 
 I think its a really nice opportunity to let people share the experience of how all of our items come together, in the Floral Art Way. 
