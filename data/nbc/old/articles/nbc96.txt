__HTTP_STATUS_CODE
200
__TITLE
Texas Balloon Crash: FAA and NTSB Clash Over Pilots Disclosure Rules
__AUTHORS
Alex Johnson
__TIMESTAMP
Aug 2 2016, 2:43 am ET
__ARTICLE
 Federal regulations don't appear to have required the pilot of the hot air balloon that crashed in Texas, killing all 16 people aboard, to disclose his past substance abuse issues  an apparent loophole that the lead investigator of the crash called "unacceptable" on Monday. 
 "You need a medical certificate before flying solo in an airplane, helicopter, gyroplane or airship," the Federal Aviation Administration says in its medical regulations for would-be pilots. That certificate must disclose any conditions "that would prevent you from becoming a pilot." 
 But if you're going to pilot a balloon or glider, "you don't need a medical certificate," according to the regulations, which specify: "All you need to do is write a statement certifying that you have no medical defect that would make you unable to pilot a balloon or glider." 
 The FAA told NBC News that it had no record of any action against the pilot in Saturday's crash, Alfred G. "Skip" Nichols IV, 49, or against his company, Heart of Texas Hot Air Balloon Rides. 
 Missouri court records show, however, that Nichols had an extensive record of driving while impaired and other driving violations dating to 1997, some of them felonies. He had so many violations that while he was licensed to pilot a balloon, at least one court decided he was ineligible to drive a car until June 2020. 
 And Nichols' court file includes a 2013 letter from the FAA itself saying it was investigating his latest DWI case and asking for copies of the paperwork. A spokesman for the FAA told NBC News that some records are expunged after a couple of years. 
 Related: Texas Balloon Pilot Had DWI Record, But Friend Says He Was Sober 
 Wendy Bartch of Ballwin, Mo., a close friend for almost 30 years, told NBC News that Nichols struggled with substance abuse for many years, but Bartch said he'd straightened out his life and was "100 percent sober." That makes it unclear whether his alcohol problems in the past made him "unable to pilot" now, and whether he still needed to report them. 
 Robert Sumwalt, a member of the National Transportation Safety Board, wouldn't speculate on a cause of Saturday's crash, including the possibility of pilot error. 
 But the lack of equal medical disclosure "goes back to the issue of oversight of commercial balloon pilots," Sumwalt said, asking, "Should they be held to a higher standard?" 
 To Sumwalt, the answer is yes  and the NTSB told the FAA two years ago, only to be turned down. 
 According to the April 2014 recommendations, "the NTSB believes that air tour balloon operators should be subject to greater regulatory oversight." 
 "The NTSB concludes that passengers who hire air tour balloon operators should have the benefit of a similar level of safety oversight as passengers of air tour airplane and helicopter operations," the agency said. 
 And some within the FAA agree. 
 According to an internal FAA report obtained Monday by NBC News, 29 hot air balloon accidents were reported from March 2010 through August 2012. More than half of them  16  "appear to have been commercial tour operations involving multiple passengers," the report said, adding: "[t]his data provides ample justification for enhanced FAA oversight." 
 "In comparison to other forms of aviating, hot air ballooning is the most volatile," according to the report. And yet, "there is no requirement under the Federal Aviation Regulations" that require tour balloon pilots to submit to drug or alcohol testing, or even to annual training, it said. 
 "Considering that commercial balloon pilots fly up to two dozen passengers (depending upon balloon size), these airmen should be subject to the same requirements imposed on airplane and helicopter air tour pilots," it said. 
 Sumwalt said Monday: "We do see this discontinuity, this disparity, in this level of oversight requirements. We do not feel that the FAA's response to our oversight recommendation was acceptable." 
 Nichols and 15 passengers were killed when the balloon crashed into a pasture near Lockhart, Texas, apparently after having struck power lines, NTSB investigators said. A fire broke out, but it remains unclear whether it erupted before or after the balloon hit the wires. 
 The initial investigation has indicated no existing problems that might have led to the crash, Sumwalt said. Nichols had obtained a proper weather report, which showed clear skies with patchy fog, and the weight load on the balloon appeared to be within its safe flying limits, he said. 
 A maintenance log for the balloon ends in September 2015, Sumwalt said. Current logs were likely aboard the balloon and were destroyed when it crashed, he said. 
 Sumwalt said 14 cellphones were recovered from the scene and were being examined in Washington, D.C., for any clues to what might have happened, while the wreckage itself is being examined in Texas. 
