__HTTP_STATUS_CODE
200
__TITLE
North Korean Defector Overcomes Past to Become Fulbright Scholar
__AUTHORS
Alexander Smith
__TIMESTAMP
Oct 13 2017, 5:23 am ET
__ARTICLE
 SEOUL, South Korea  Kim Seong Ryeol says he was aged 10 when he witnessed his first public execution. 
 He recalls it being a monthly event in his North Korean hometown, with police gathering crowds near a local market where they would shoot or hang people accused of criticizing the regime. 
 More than two decades later, Kim finds himself in a different world  sitting in a futuristic, glass-clad skyscraper in the heart of Seoul, the capital of neighboring South Korea. 
 He is one of around 30,000 North Koreans who have escaped their totalitarian regime by fleeing south. 
 As a boy he used to dream of killing American soldiers, but no more. The 32-year-old was recently awarded a Fulbright scholarship to do a Ph.D. in the U.S. He's also become a Christian, a religion he knew next to nothing about before leaving his homeland. 
 But his new life hasn't been quite what he hoped for. 
 Kim is one of many defectors who say that since crossing the border they have faced painful discrimination from South Koreans. 
 "The first time I arrived at the airport, I thought, 'It's heaven,'" Kim says, looking out over Seoul's hazy, high-rise skyline from the 17th-floor cafe where he met NBC News this week. "But the reality of life in Seoul wasn't really that easy  I was depressed and disappointed. There are so many barriers that you have to overcome." 
 North and South Korea technically remain at war. They both would like to see the peninsula reunified, albeit with drastically different ideas about what the end result should look like. 
 South Korea gives defectors like Kim special treatment when they arrive. They are taken to a screening center to make their claims are legitimate and not a security risk, before being given automatic South Korean citizenship. 
 Next they are taken to a state re-education center called Hanawon, or "House of Unity." There they are given a 12-week course covering South Korean life, laws, culture  as well as lessons in democracy and capitalism. 
 Kim went through the process when he came here with his mom and sister in 2004. He was aged 19. 
 Related: N. Korea Defectors Find Unlikely Haven in London Suburb 
 It took him a while to realize that Seoul was not "one of the poorest places in the world," as he had been taught. 
 "It really confused everything to find out that South Korea is actually a highly developed society," he says. "Even simple things like the subway or bank accounts ... I had to study it closely and pay attention to everything." 
 In March, a study by the National Human Rights Commission of Korea found 45 percent of North Korean defectors have faced discrimination since coming here, according to South Korea's Chosun Ilbo newspaper. 
 Kim's first experience of this was at school. 
 "My classmates in South Korea didn't want to include me in teamwork projects because they thought I really lacked understanding of technology, how to write, and the knowledge of classics and history," he says. "North Korea's education is not modernized and many things are based around propaganda to make sure people worship the Kim family." 
 He said this made him angry, but added: "It actually motivated me to go further and try to become more like what South Koreans expected." 
 When it came to finding a job, his resume would be accepted time and again, but he claims that when it came to the interview stage employees would reject him when they learned he was North Korean. 
 "Seventy companies I applied to, all rejected. Seven-zero, all at the interview level," he says. "You have interviewers saying, 'Explain about your high school and middle-school life,' but we don't have that kind of experience in North Korea. So you have to say, 'I come from North Korea and I want to contribute to your company.' At that level, interviewers are very confused." 
 Kim has been left frustrated and disappointed by his treatment in South Korea but it would be inaccurate to say his time in the country has been all bad. 
 In 2015, after completing his undergraduate studies, he won a scholarship from the Open Society Foundation, a New York-based grant-making organization, to do a master's degree in unification studies at a South Korean university. 
 And he has gone from hating the U.S. to actually going there in person. 
 "When I was little, every book, every curriculum, they always mentioned that America is the enemy," he says. "When I was little I dreamed about fighting Americans." 
 But in 2009, he attended a school run by Youth With a Mission, a Texas-based Christian missionary organization. 
 "It broadened my perspective, when I met my American friends, and made me think differently, to see that the world is global," he says. 
 His affinity with the U.S. hasn't stopped there. In August he was awarded a Fulbright scholarship to study a Ph.D. in the U.S. starting next year. 
 "I haven't chosen a university but I would like to go to the University of Pennsylvania or Columbia University," he says. "But these are really good schools, these are in the Ivy League, and I have to meet the requirements of these schools." 
 His love for the U.S. doesn't extend to Donald Trump, however, especially after the president threatened during a speech last month to "totally destroy" North Korea. 
 "It makes me angry when he says that," Kim says, his polite voice raising. "There are many, many ways to approach North Korean problems. Why only say, 'Destroy'? I mean, there's a lot of options you can choose." 
 Kim says he wants Trump  and the American public in general  to see beyond the North Korean regime and realize most of the country's 25 million people are blameless civilians. Any war on the Korean peninsula would almost certainly mean a huge loss of life on both sides of the border, and plenty of Kim's friends and relatives are still stuck inside the North. 
 "The people, they're really kind and just... normal," he says. "They try every day to only focus on their life. That's all." 
