__HTTP_STATUS_CODE
200
__TITLE
Last Empty County Gets an Obamacare Insurer
__AUTHORS
Associated Press
__TIMESTAMP
Aug 24 2017, 3:38 pm ET
__ARTICLE
 The lone county currently at risk of going uncovered on the federal health laws insurance exchanges has landed an insurer. 
 CareSource will step up to provide health insurance coverage in Paulding County, Ohio, in 2018, The Associated Press has learned. The company and state Department of Insurance planned to announce the arrangement Thursday. 
 The most recent national analysis by the Kaiser Family Foundation identified Paulding, in northwest Ohio just south of Toledo, as the final county still at risk of lacking a provider when 2018 signups begin Nov. 1. About 10 million people, including 11,000 Ohio residents, currently are served through HealthCare.gov and its state counterparts, a system created under the federal Affordable Care Act. 
 Earlier this year, well over 40 mostly rural counties faced the prospect of having no options for their exchanges. Insurers who withdrew cited steep losses and uncertainly over the future of President Barack Obamas 2010 Affordable Care Act. 
 Insurers have been pulling back from the exchanges after getting stung by heavy losses and struggling to attract enough young, healthy customers to balance all the claims they get from people who use their coverage. 
 In Ohio alone, 20 of 88 counties lost insurers. State officials had previously announced coverage in the 19 others. 
 Rural counties, in particular, have been particularly uninviting for them because they usually have a smaller, older customer base and a care provider like a hospital system with a dominant market position. That can make it difficult to negotiate payment rates. 
 While insurers have made preliminary plans to sell coverage on the exchanges next year, they still have about a month to back out. 
 Insurers are worried, in particular, about the fate of billions of dollars in payments from the government to cover cost-sharing reductions for customers with modest incomes. These payments reimburse insurers for lowering deductibles and other out-of-pocket expenses for customers. They are separate from the income-based tax credits that help people buy coverage. 
 The federal government announced last week that it will make these payments for this month, but their future is unclear. President Donald Trump has repeatedly threatened to end them, and insurers say premiums will soar for some of their plans if this happens. 
 Related: Next Steps on Obamacare are Murky 
 The Marketplace provides vital health care coverage to more than 10.3 million Americans and we want to be a resource for consumers left without options, she said in a statement. Our decision to offer coverage in the bare counties speaks to our mission and commitment to the Marketplace and serving those who are in need of health care coverage. 
 Ohio State Insurance Director Jillian Froment said working through the challenge of covering affected counties has been a priority of her staff in recent weeks. 
 There is a lot of uncertainty facing consumers when it comes to health insurance and these announcements will provide important relief, she said. 
 The department plans to work with insurers in the coming weeks to finalize the products and rates that will be available on the exchange in 2018. A review is expected to be completed by early September, and insurers must sign contracts with the federal government by late September to sell coverage on the federal exchange. 
