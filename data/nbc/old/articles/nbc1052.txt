__HTTP_STATUS_CODE
200
__TITLE
Trumps Net Worth Plummets on Forbes Richest List
__AUTHORS
Dartunorro Clark
__TIMESTAMP
Oct 17 2017, 11:44 am ET
__ARTICLE
 President Donald Trump's net worth took a dive on Forbes list of the wealthiest Americans. 
 The president dropped by $600 million  and 92 spots  to No. 248 this year, to an estimated net worth of $3.1 billion, according to Forbes, which released its new list on Tuesday. Trump tied with Evan Spiegel, the 27-year-old co-founder of Snapchat. 
 Last year, the president ranked No. 156 with a net worth of $3.7 billion. 
 Forbes said it spent months digging through financial disclosure forms, public property records and interviewing dozens of people to estimate the president's net worth, which has long been a source of contention. 
 The president has claimed he is worth $10 billion. The president has declined to personally release his tax returns, something every president since Richard M. Nixon has done. 
 Forbes has been tracking Trump's vast fortune since 1982, when the 400 wealthiest list debuted. 
 The magazine attributed the dip in Trump's wealth to "a weakening in the New York City retail and office real estate market," according to a press release. 
 Forbes reported that "the values of several Manhattan properties, particularly those on or near Fifth Avenue, have dropped, shaving nearly $400 million off his fortune." 
 Trump also took a financial hit due to his 2016 presidential campaign, spending $66 million, and $25 million to settle his Trump University lawsuit, Forbes reported. 
 The president's various golf properties, such as the one in Miami and Scotland, also had financial woes. 
 Trump did have a silver lining, seeing an uptick in assets at his hotel-condo tower in Las Vegas and his stake in a downtown San Francisco office building, Forbes said. 
 Microsoft founder Bill Gates ranked No. 1 with a net worth of $89 billion, and Amazon CEO Jeff Bezos ranked No. 2 with $81.5 billion. 
