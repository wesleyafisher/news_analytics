__HTTP_STATUS_CODE
200
__TITLE
Hate Crime Charged in Fatal Stabbing of Black Maryland Student
__AUTHORS
Phil Helsel
__TIMESTAMP
Oct 17 2017, 6:30 pm ET
__ARTICLE
 A grand jury in Maryland returned a hate crime indictment against a white University of Maryland student charged with fatally stabbing a black student at another college in May, the prosecutor said. 
 Sean Urbanski, 22, had already been charged with murder and faced a potential sentence of life in prison in the fatal stabbing of Richard Collins III, a student at Bowie State University who had been commissioned as a second lieutenant in the Army. Collins was visiting friends at the University of Maryland's campus in College Park when he was killed. 
 Prince George's County State's Attorney Angela D. Alsobrooks said Tuesday that the grand jury returned an indictment on an additional charge of hate crime resulting in death after digital evidence was found on Urbanski's phone and in other places. 
 "What we're saying is that race, that Lieutenant Collins' death, that he was killed because of his race," Alsobrooks said at a news conference. She declined to say what kind of digital evidence was found, saying it would be presented at trial. 
 It had previously been reported that Maryland authorities and the FBI were evaluating hate crime charges after it was discovered that Urbanski was a member of a Facebook group featuring bigoted posts called "Alt-Reich: Nation." 
 Collins father, Richard Collins Jr., has said that authorities told him his son was as killed while he was waiting for an Uber driver to pick him up along with a couple of his friends from the University of Maryland. Police have said Urbanski screamed at him to move before stabbing him in the chest. 
 Collins was a few days shy of graduation when he was killed. 
 The hate crime addition means Urbanski could face a maximum sentence of life in prison without parole, plus 20 years. Alsobrooks said prosecutors would seek the maximum penalty. 
A statement from Bowie State @presbreaux on the hate crime indictment in the death of 2nd Lt. Richard Collins III. pic.twitter.com/yqng1X310D
 Alsobrooks said she informed the Collins family of the new charge. "This family is grieving, like any other family," she said. "I don't know that there is anything that can ever be done to really completely heal a wound like this, when they lost their son." 
 Relates: Slain Maryland Student Honored at Commencement Ceremony 
 An attorney for Urbanski did not return a call for comment. 
