__HTTP_STATUS_CODE
200
__TITLE
Cuba Resort Coast Flooded, Houses Toppled as Hurricane Irma Moves On
__AUTHORS
Associated Press
__TIMESTAMP
Sep 10 2017, 8:28 am ET
__ARTICLE
 CAIBARIEN, Cuba  Hurricane Irma ripped roofs off houses and flooded hundreds of miles of coastline as it raked Cuba's northern coast after devastating islands the length of the Caribbean in a trail of destruction. 
 As Irma left Cuba late Saturday and directed its 130 mph winds toward Florida, authorities on the island were assessing the destruction and warning of staggering damage to keys off the northern coast studded with all-inclusive resorts and cities, as well as farmland in central Cuba. 
 There were no immediate reports of deaths in Cuba  a country that prides itself on its disaster preparedness  but authorities were trying to restore power, clear roads and warning that people should stay off the streets of Havana because flooding could continue into Monday. 
 Residents of "the capital should know that the flooding is going to last more than 36 hours, in other words, it is going to persist," Civil Defense Col. Luis Angel Macareno said late Saturday, adding that the waters had reach at about 2,000 feet into Havana. 
 As Irma rolled in, Cuban soldiers went through coastal towns to force residents to evacuate, taking people to shelters at government buildings and schools  and even caves. 
 Video images from northern and eastern Cuba showed uprooted utility poles and signs, many downed trees and extensive damage to roofs. Witnesses said a provincial museum near the eye of the storm was in ruins. And authorities in the city of Santa Clara said 39 buildings collapsed. 
 Related: Hurricane Irma Bears Down on Florida 
 More than 5,000 tourists were evacuated from the keys off Cuba's north-central coast, where the government has built dozens of resorts in recent years. 
 Civil Defense official Gregorio Torres said authorities were trying to tally the extent of the damage in eastern Cuba, home to hundreds of rural communities and farmland. 
 In Caibarien, a small coastal city about 200 miles east of Havana, winds downed power lines and a three-block area was under water. Many residents had stayed put, hoping to ride out the storm. 
 Before slamming into Cuba, Irma had caused havoc in the Caribbean, where it ravaged such lush resort islands as St. Martin, St. Barts, St. Thomas, Barbuda and Anguilla. 
 Many of Irma's victims fled their battered islands on ferries and fishing boats for fear Hurricane Jose would destroy or drench anything Irma left untouched, but Jose veered away before doing much more damage. 
 On the Dutch side of St. Martin, an island divided between French and Dutch control, an estimated 70 percent of the homes were destroyed by Irma, according to the Dutch government. 
 Prime Minister William Marlin said about 1,600 tourists had been evacuated and efforts were being made to move 1,200 more. 
 Related: Hurricane Irma Leaves Path of Destruction in Caribbean 
 Marlin said many countries and people have offered help to St. Maarten, but authorities were waiting on the weather conditions to see how it could be coordinated. Authorities are still trying to determine the extent of damage to the island but he said 28 police officers lost homes during the storms. 
 The U.S. State Department helped more than 500 Americans fly out of St. Martin, starting with those in need of urgent medical care, spokeswoman Heather Nauert said. 
 Some islands received a last-minute reprieve from Jose as it passed by. 
 The U.S. National Hurricane Center downgraded a hurricane warning for Barbuda and Anguilla. A hurricane watch also was discontinued for nearby Antigua. 
 Looting was reported on St. Martin. Curfews were imposed there and on St. Barts, and French and Dutch authorities announced plans to send hundreds more troops and police to keep order. 
 
