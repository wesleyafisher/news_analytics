__HTTP_STATUS_CODE
200
__TITLE
College Scholarships for Video Games? Its Happening
__AUTHORS
Amy DiLuna
__TIMESTAMP
Jun 20 2017, 8:18 am ET
__ARTICLE
 For any parent who has fretted about their kids playing video games all day, heres a reason to relax. 
 Esports  team video game competition  is coming to more college campuses and some schools are treating these digital leagues like traditional sports, complete with recruiting, on-campus arenas and scholarships to entice students. 
 Theres a phrase out there  esports is the wild, Wild West, said Michael Brooks, Executive Director of the National Association of College eSports, an organization that formed nearly a year ago with an aim toward governing the sport at a collegiate level. "It was very true, and is still true to a certain extent right now. But consolidation is occurring in the amateur and pro space very rapidly." 
 Just how popular has the industry become? Esports finals sold out the Staples Center and Madison Square Garden in an hour. 
 "This is a massive space and learning about it is like going behind a curtain, said Glenn Platt, Armstrong Chair in Interactive Media Studies and faculty director of the varsity esports team at Miami University in Ohio. "Most people have no clue its there, and then once you go behind the curtain youre like, This is a billion-dollar industry.'" 
 And while success in the professional realm is akin to making it big in the NBA or NFL  i.e., your chances are slim  the prospect of a substantial scholarship and experience that translates to a post-college career should be tempting to parents and students alike. 
 As the industry continues to grow, jobs are being created. They range from marketers to game makers to league staffers. Esports can offer experiences that reach beyond the games themselves. 
 "Games are everywhere now...Even your car can give you points for how well youre driving," said Platt. "As more and more of the world around us becomes 'gamified' and uses gaming techniques, people and companies are going to need students who are versed in this." 
 Though esports often fall under the umbrella of traditional athletics on college campuses, there are some major differences. For one, unlike traditional college athletics, there is no rule preventing a professional from joining a college team. The NCAA doesnt regulate esports, though according to Platt, these sorts of conversations are happening. 
 Another issue involves the fact that the games themselves  "League of Legends," "Overwatch," "Counter-Strike: Global Offensive," "Hearthstone" and more  are owned by publishers, which means intellectual property laws could come into play. 
 Esports have been around for 20 years, but college teams are relatively new. 
 Robert Morris University in Illinois was the first school to recruit and financially entice students for an esports league. When Kurt Melcher, the school's director of esports, first approached the administration about creating a program, he was met with some skepticism. 
 I put a proposal together and brought it to our administration. I did get a little bit of eye-rolling or shrugging at first. 
 RELATED: Who Lived With a Celebrity? NBC Correspondents Share College Tales 
 But they trusted Melcher, and funded the project. 
 That was in 2014, and for the 2017-18 academic year, they anticipate around 90 students will be playing five different titles. A varsity scholarship pays for 70 percent of tuition, and students must maintain a 2.0 grade point average. 
 "The goal was never to have a semi-professional team or to go get the best mercenaries around who want to play at this level," said Melcher. "It was to create thought-sharing and a large group of like-minded kids, which it is." 
 Though the arenas are essentially computer labs, they are tricked out with the latest technology and high-speed streaming capabilities. 
 Parents concerned that their kids muscles will atrophy from sitting in a chair for eight hours a day, six days a week (yes, thats how much these teams practice) shouldnt worry. 
 The team at Maryville University in St. Louis, which won a national championship this year, follows a workout regimen and in the fall, a new coach will implement a nutritional program. 
 Is all this making you feel old? Join the club. 
 I imagine this is a lot what it was like talking about soccer in America 15 or 20 years ago, said Platt. 
 Getting people on board with that is hard. And when we talk to parents, one of the advantages we have of having an academic program is that we can say 'look, our students who are getting game degrees are actually getting jobs.'" 
