__HTTP_STATUS_CODE
200
__TITLE
Zika Might Affect Adult Brains, Too, Study Finds
__AUTHORS
Maggie Fox
__TIMESTAMP
Aug 18 2016, 6:59 pm ET
__ARTICLE
 The Zika virus, previously thought only to be a big threat to developing babies, might also affect adult brains, researchers reported Thursday. 
 Tests in mice suggest the virus can get to and damage immature brain cells in adults  something that indicates Zika infection may not be as harmless for grown-ups as doctors have believed. 
 It will take much more study to know if human beings infected by Zika are at risk, but the report, published in the journal Cell Stem Cell, adds another disturbing twist to the Zika saga. 
 Zika is known to home straight in on developing nerve cells, especially brain cells, when it infects a fetus. The result is devastating birth defects  a small head, known as microcephaly, profound brain destruction, and sometimes less obvious brain damage. 
 Related: There's More Evidence Zika Goes Straight to the Brain 
 Babies often miscarry and if they survive, they have permanent brain damage. Theres no cure. 
 Other viruses are known to prefer brain cells and nerve cells  herpes viruses are a well known example  but Zika is surprising scientists more the more they study it. Theres a lot we dont know about Zika, Centers for Disease Control and Prevention director Dr. Thomas Frieden said Thursday. 
 Hongda Li of the Howard Hughes Medical Institute and The Rockefeller University in New York and colleagues used an experimental strain of mice for their tests. 
 They knew Zika preferred immature brain cells called stem cells and they were working on recent discoveries that adult brains also carry these immature brain cells. 
 Two areas in the adult mouse brain contain neural stem cells: the subventricular zone of the anterior forebrain and the subgranular zone of the hippocampus, they wrote in their report. 
 And sure enough, Zika got into those brain regions when the mice were infected and appeared to kill some of the stem cells in there. 
 Related: Can I Ever Get Pregnant? And Other Zika Questions 
 Our data therefore suggest that adult as well as fetal neural stem cells are vulnerable to Zika virus neuropathology, they wrote. Thus, although Zika virus is considered a transient infection in adult humans without marked long-term effects, there may in fact be consequences of exposure in the adult brain. 
 It's not clear what that might mean to people affected by Zika. Researchers are only beginning to study it and its long-term effects. 
 Adult brains are less vulnerable to damage than those of developing babies, but brain damage can cause epilepsy, personality changes, depression and dementia. 
 Zikas not necessarily believed harmless to adults. The evidence suggests that 75 to 80 percent of those infected never know it, suffering mild symptoms at most. The worst affected usually have had muscle aches, headaches, a rash and red eyes. 
 It can also cause Guillain-Barre syndrome  a paralyzing disorder that a few people suffer after a variety of infections. Puerto Rico reported Thursday that 30 people had suffered Guillain-Barre in the Zika epidemic there so far. 
 Related: Zika Virus Hotspot in Houston 
 Lis team said their findings may help explain cases of Guillain-Barre. 
 Infection of neural progenitor cells in stem cell niches may relate to the emergent cases of Zika-linked Guillain-Barre syndrome (GBS), they wrote. 
