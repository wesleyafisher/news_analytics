__HTTP_STATUS_CODE
200
__TITLE
Mohamad Khweis, American Who Bolted From ISIS, Now Indicted 
__AUTHORS
Tracy Connor
__TIMESTAMP
Nov 3 2016, 2:09 pm ET
__ARTICLE
 A Virginia man described as an ISIS defector has been indicted on charges of helping a terrorist organization and will be arraigned in federal court Friday, nearly eight months after he was captured by Kurds in Iraq. 
 Mohamad Jamal Khweis has been held without bail since he was brought back to the United States to face charges of conspiracy and providing material support to ISIS. 
 "He will be pleading not guilty," said his attorney John Zwerling. "And we will go from there." 
 Khweis, 27, told his family he was going to Europe for vacation last winter. Then in March, the American-born son of Palestinian immigrants surfaced in Kurdish territory with a story of fleeing ISIS. 
 Related: Captured American Says Time With ISIS Was 'Bad Decision' 
 In an interview with Kurdish TV after his capture, he claimed he made "a bad decision" to follow a young woman to Mosul and ended up in ISIS training facilities. 
 "I wasn't thinking straight, and on the way there I regretted," he said in the heavily edited interview. 
 "I don't see them as good Muslims," Khweis said of ISIS. "I wanted to go back to America." 
 But in court papers, U.S. authorities said Khweis told Kurdish authorities and the FBI while he was still overseas that he started researching ISIS months before he went to Europe and tried to contact a extremist cleric once he was there. 
 Related: FBI Says American Defector 'Gave Himself' to ISIS 
 "During the interview, the defendant stated he 'gave himself' to ISIL and that they controlled him. The defendant stated he was aware that ISIL wants to attack and destroy the United States," an affidavit from an FBI agent says. 
 "The defendant stated that ISIL wants America to be taken over." 
 The circumstances of his statement are likely to be the subject of debate as the case goes forward. A Justice Department official has told NBC News that if they were made under duress or after rough treatment by the Kurds, the defense could move to have them thrown out of court. 
