__HTTP_STATUS_CODE
200
__TITLE
Security Experts Predict Changes in the Wake of Las Vegas Massacre
__AUTHORS
Jackson Hudgins
__TIMESTAMP
Oct 16 2017, 3:55 am ET
__ARTICLE
 As more details emerge of Las Vegas shooter Stephen Paddocks calculated attack, security professionals predict changes are in store both for destination hotels and major music festivals. 
 Though they emphasized that the massacre targeting the Route 91 Harvest Music Festival would have been difficult to expect and almost impossible to plan for, multiple security consultants and professionals said festival promoters would likely think twice before hosting events in the shadow of large buildings again. 
 "Before 9/11, no one ever thought about planes being taken over and being flown into buildings, but now anytime you go the airport, youre going through TSA. Its the same thing," said Jason Porter, a vice president at Pinkerton, a corporate risk management firm. I think there are going to be some pretty decent changes coming down the pipe." 
 Many festivals are already in remote, open areas purely out of logistical convenience. But some, like Route 91, or Ultra in downtown Miami, are nestled into urban spaces, surrounded by buildings. Once part of their appeal, their locations may now be considered liabilities. 
 Porter emphasized that the kind of security work it takes to protect against an elevated shooter  like suveilling tall buildings and scanning them for potential threats throughout an event, or even renting out rooms that could be used by a gunman  is extremely costly, and may not be worth the trouble. 
 "You have to determine what's a reasonable amount of security that's going to provide safety for our patrons, but also be within a reasonable budget," he said. 
 Related: Las Vegas Gunman Shot Security Guard Before Opening Fire on Crowd 
 In the Oct. 1 shooting in Las Vegas, Paddock, 64, opened fire from the Mandalay Bay resort and casino, killing 58 people before apparently killing himself. Hundreds were injured. It was the deadliest mass shooting in modern American history. 
 Peter Yachmetz, a 29-year veteran of the FBI who runs his own physical security and risk mitigation consulting group, agrees that preventing such a shooting would be difficult. 
 "What you could consider, which I think would not go very far, is that maybe you could have snipers on other buildings," he said. "But even with that, what are you really going to do if you see someone pop open a window?" 
 James Dambach, director of executive protection and investigations at Echelon Protection and Surveillance, and who had a lengthy career in law enforcement with the Philadelphia Police Department, said the calculus has clearly changed for such events. But he stopped short of suggesting the events leave city centers completely. 
 "It's something to be considered for each individual event. It adds another layer of security, in dealing with hotels and people coming in and out of buildings," Dambach said. "But if the terrorists or perpetrators force us to move these things then they actually win." 
 Related: Endangered House Republican to Introduce Bill to Ban 'Bump Stocks' 
 Investigators have not determined a motive in the Las Vegas attack. They say Paddock moved "an armory" of guns into the hotel room, and that a note was found with numbers on it believed to be calculations of shooting ranges, drops and distance for higher precision in shooting into the concert crowd. 
 Yachmetz, who has a background in behavioral assessment, was critical of the Mandalay Bay's preparedness. "I think they need to increase their security," Yachmetz said. "How did he get all this stuff up into the room? How did he get so many long weapons and build a platform?" 
 He predicted that hotel staff would receive increased training in identifying suspicious people, and warned travelers that they should begin to expect increased, airport-style security at hotels in destination cities. 
 "My opinion is that they need to get used to it very quickly, because it's coming, not only bag checks, but magnetometers as well," Yachmetz said. 
 Another question is whether these large-scale gatherings can balance the need for heightened security without fundamentally dampening the atmosphere that draws attendees year after year, or disrupting the flow of everyday life in the cities where they are hosted. 
 "In a hospitality-type event you have to strike a balance between secure and inviting," Porter said. "If you lock down an event 100 percent, then you have people that don't want to attend and therefore it's unsuccessful." 
 The threat was made all the more real when it was revealed that Paddock had rented rooms at other hotels adjacent to different festivals in the months and weeks before the attack in Las Vegas, including a 21-story hotel on the edge of Chicagos Lollapalooza festival in Grant Park, and at The Ogden in Las Vegas, which overlooks the Life Is Beautiful festival downtown. Both rentals coincided with the dates of the respective events. 
 While Paddock did not check into the Chicago hotel, Clark County Sheriff Joe Lombardo confirmed that Paddock had not only rented a room at The Ogden during Life is Beautiful, but that officials were reviewing footage of his actions there. Life Is Beautiful draws more than 100,000 people to 18 square blocks in downtown Las Vegas. 
 In a statement to NBC News, MGM spokesperson Debra DeShong said, "As our security team is working tirelessly to protect the safety of our guests and facilities, MGM Resorts has elevated its level of security to add to the level of comfort and safety of our guests and employees." 
