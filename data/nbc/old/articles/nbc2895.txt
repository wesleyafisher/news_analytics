__HTTP_STATUS_CODE
200
__TITLE
CDC Warning: Flu Viruses Mutate and Evade Current Vaccine
__AUTHORS
__TIMESTAMP
Dec 4 2014, 4:56 am ET
__ARTICLE
 Much of the influenza virus circulating in the United States has mutated and this years vaccine doesnt provide good protection against it, federal health officials are warning. 
 Flu seasons barely starting, but most cases are being caused by a strain called H3N2 this year, the Centers for Disease Control and Prevention said in a health warning issued to doctors Wednesday night. 
 The flu vaccine protects against three or four strains of flu  theres always a mix of flu viruses going around  and H3N2 is one of them. But the strain of H3N2 infecting most people has mutated and only about half of cases match the vaccine, CDC said. 
 Flu viruses do this all the time. The mutations are called drift and vaccines, formulated months in advance, often don't protect well against drifted viruses although they may provide a little of what's called cross protection. 
 "We are recommending strongly still that people who havent been vaccinated get vaccinated. Every year vaccine is the best way to protect yourself against the flu," the CDC's flu expert Dr. Joe Bresee told NBC News. 
 Though reduced, this cross-protection might reduce the likelihood of severe outcomes such as hospitalization and death, CDC said in a health advisory to doctors. And the vaccine still protects against half the circulating H3N2,as well as H1N1 flu and the B strains. 
"Cross-protection might reduce the likelihood of severe outcomes such as hospitalization and death."
 So CDC sticks with its advice that everyone six months and older should get a flu vaccine every year. 
 But because even vaccinated people may be more vulnerable than usual, CDC is reminding doctors they can and should use two antiviral drugs to treat flu and in some cases to prevent it. Theyre called Tamiflu and Relenza, and giving them to people within a day or two of infection can keep them out of the hospital and reduce how long theyre sick. 
 For people in nursing homes and other institutions where flu is a big danger, doctors should think about using the drugs to prevent infection, CDC said. 
 An influenza outbreak is likely when at least two residents are ill within 72 hours, and at least one has laboratory confirmed influenza, CDC said. Staff and residents alike can take the drugs daily for at least two weeks and for at least a week after the last known case of flu, CDC says. 
 As of November 22, influenza activity has increased slightly in most parts of the United States. Surveillance data indicate that influenza A (H3N2) viruses have predominated so far, with lower levels of detection of influenza B viruses and even less detection of H1N1 viruses, CDC said. 
As of November 22, influenza activity has increased slightly in most parts of the United States."
 Last season, it was H1N1 flu that affected the most people. 
 Flu usually hits the very young and the very old the hardest. Depending on the season, it kills anywhere between 4,000 and 50,000 people a year in the United States. 
 This year, the CDC expects up to 159 million flu vaccine doses to be available. Flu season usually peaks in the United States in January and February. 
 Why not just make a new flu vaccine? It takes too long, Bresee says. "Its tempting to think that we can make a new vaccine for the strains that pop up late in the year like this, but flu vaccines take a long time to make," he said. "We decide what is going to be in this flu vaccine for this season in the U.S. way back in February. It takes about six months for the vaccine to be made." 
 That's why CDC and the vaccine makers are working to come up with better flu vaccines that can be made faster, and eventually, for vaccines that could protect against all strains of flu. 
