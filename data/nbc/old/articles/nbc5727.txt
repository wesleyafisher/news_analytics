__HTTP_STATUS_CODE
200
__TITLE
#GOTscience: Game of Thrones Plot Raises a Grisly Burning Question
__AUTHORS
Alan Boyle
__TIMESTAMP
Apr 13 2015, 2:07 pm ET
__ARTICLE
 HBO's "Game of Thrones" TV series has had its share of gruesome deaths, ranging from beheadings and throat-slittings to a bare-hands head-crushing. But being burned alive? 
 "Bad way to go," a character from the swords-and-sorcery show says during this week's season premiere. 
 Throughout history, death by burning has been portrayed as one of the worst ways to go. It makes for horror-inducing scenes not only on "Game of Thrones," but also during episodes of PBS' "Wolf Hall" docudrama about King Henry VIII and the medieval milieu of 16th-century England. 
 As medieval as it sounds, death by burning is still a phenomenon in the 21st century  to such an extent that the physiology can be studied by forensic scientists. The victims range from the Jordanian jet pilot who was killed by ISIS terrorists this year, to the Africans and Haitians targeted by the practice known as "necklacing," to January's killing of a newborn in New Jersey. 
 The conventional wisdom is that most fire deaths are caused by asphyxiation rather than the burning itself  but Valerie Rao, chief medical examiner for the city of Jacksonville, Florida, said that's not always the case for an execution-style burning. 
 "It depends on how fast this happens," Rao told NBC News. "If you are burned to death, it's basically the burns that cause the death." 
 Rao's analysis of the "Forensic Pathology of Thermal Injuries" for Medscape makes for difficult reading, and even more difficult viewing. The flesh is burned black, with "skin splits" exposing soft tissue. The effects of the heat on muscles warp the body into a boxer's "pugilistic" attitude. Black soot clogs the airway. 
 Historical accounts of the burning of Joan of Arc suggest that she died of smoke inhalation, but add that her body had to be set aflame two more times to burn it down completely to ash. 
 Other accounts report that the victims of a death-by-burning sentence were sometimes given the option of being strangled first, to spare them the extreme pain associated with the burning of the air passages. Yet another option involved tying a small barrel of gunpowder to the prisoners, to give them a quick and merciful death when the flames licked up. 
 During her 35 years of forensic work, including 19 years in Miami, Rao has seen the effects of death by burning on drivers trapped in fiery automobile collisions, the targets of drug-related car bombings, and the victims of necklacing inflicted by bad elements in Florida's Haitian community. 
 "Initially it's sad," she said. "But after a while, you say to yourself, 'If I'm going to do this, I'm going to have to separate the emotion from the work.'" 
 Are there any lessons to be learned by TV viewers who are getting a sanitized look at the grisly phenomenon? For Rao, the takeaways are far more pedestrian than Jon Snow's exploits on "Game of Thrones," but also far more realistic: Install carbon monoxide detectors and smoke alarms in your home, change their batteries regularly, and don't use gas-powered generators or barbecues indoors. 
 "Those are very useful lessons that we've learned," Rao said. 
 Got scientific questions about scenes from "Game of Thrones"? Flag them with the Twitter hashtag #GOTscience. 
