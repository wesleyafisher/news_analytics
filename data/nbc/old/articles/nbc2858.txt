__HTTP_STATUS_CODE
200
__TITLE
5 Things You Didnt Know About Norovirus, the Nasty Stomach Flu
__AUTHORS
Maggie Fox
__TIMESTAMP
Jan 30 2017, 2:28 pm ET
__ARTICLE
 It's flu season, and people in most states are being hit with a double whammy of influenza and norovirus  the winter vomiting virus also commonly known as the stomach flu. 
 The Centers for Disease Control and Prevention says 2017 is shaping up to look like an average season for both viruses, which typically come to a peak between late January and March. 
 Doctors often focus on warning patients about influenza, in part because it can be so deadly and in part because there are vaccines to prevent it. Norovirus is a nasty bug, but usually flies under the public health radar. Here are five things that may surprise you: 
 While they are completely different viruses, norovirus and influenza are both RNA viruses, meaning they use RNA instead of DNA to replicate. That makes them both highly mutation-prone, which in turn makes it hard for the human immune system to defend against them. Thats why you can get sick from norovirus year after year, says Dr. Aron Hall, CDCs norovirus expert. 
 Every few years we see a new strain become predominant, Hall said. Exposure to one strain of norovirus does not necessarily protect you against all strains. 
 Norovirus is enclosed by a structure known as a capsid. Alcohol cannot get through it, which is why alcohol-based hand sanitizers do not kill norovirus. 
 Its resistant to many common disinfectants, Hall said. CDC recommends using bleach to kill it, including chlorine bleach or hydrogen peroxide. Thats why health departments often require restaurants to use bleach to clean countertops and kitchen surfaces. 
 Related: Why Washing Your Hands Isn't Enough to Fight Norovirus 
 Its also able to survive being dried out. It can persist on surfaces for several days even at room temperature, Hall said. Soap and water can wash it away, but it takes really hot water to kill it. Hand-washed dishes are especially likely to carry the virus, and it can spread even in ordinary laundry, so if someone is sick, its important to use very hot water and bleach to destroy virus that could be on any clothing, sheets or towels. 
 Just like influenza, norovirus is still being produced in your body after you get over symptoms from a bout. So people can spread it after theyve returned to work. 
 Norovirus spreads via the fecal-oral route, so if people do not wash their hands very carefully after they have recovered from a bout of norovirus, they can spread it to others. 
 Even once you feel better, you should still stay home at least one to two days, Hall advises. 
 Because it spreads even after people feel better, patients can and do go back out into the world while they are still infectious. 
 Combine a sticky, hard-to-kill virus with invisible spread by people who dont feel sick, and it makes a recipe for exponential spread. 
 Related: Vomiting Machine Shows Why Norovirus Spreads So Fast 
 Restaurant workers usually get little or no paid sick leave, so many workers come in sick, or too soon after theyve recovered, and they can spread the virus to hundreds of customers. Food handlers, dishwashers, even staff who bus and clear tables, all can spread the germ. 
 One ill food worker or even a worker who recovered has the potential to expose literally hundreds of people, Hall said. 
 And vomiting once can create an aerosol of virus that settles on surfaces all around. 
 In 2010, nine soccer players all got sick from a plastic shopping bag that got norovirus splashed on it. 
 The makes norovirus the most common cause of acute gastroenteritis  stomach upset  in the United States. It makes 21 million people sick every year in the United States  70,000 on average get sick enough to go to the hospital. As many as 800 people die, mostly elderly patients who become dehydrated. 
 While norovirus is nowhere near the killer that influenza is, several teams of researchers are nonetheless working on vaccines to prevent it. Globally, norovirus kills 200,000 people a year. 
 Its difficult in part because the virus mutates, and in part because the virus lives in the gut and its hard to make vaccines that work there. 
 Drugmaker Takeda has a vaccine thats being tested in people now. At least one study has shown that about 20 percent of people of European origin have a genetic mutation that protects them from common norovirus strains, something that might help in development of better vaccines. 
