__HTTP_STATUS_CODE
200
__TITLE
Irma Threatens to Wash Away Florida Buildings After Tearing Through Caribbean 
__AUTHORS
Erik Ortiz
Corky Siemaszko
Alex Johnson
Jason Cumming
__TIMESTAMP
Sep 8 2017, 11:06 am ET
__ARTICLE
 Hurricane Irma continued its deadly and destructive path through the Caribbean on Friday morning, and is still on track to slam into Miami this weekend. 
 The storm, which went from a Category 5 to a 4, remained powerful, with winds of 150 mph, according to an 11 a.m. ET National Hurricane Center bulletin. Irma was about 405 miles southeast of Miami, with potential landfall in Florida appearing more likely for Sunday morning. 
 Irma grazed the Turks & Caicos Islands earlier Friday as it swept toward the southeastern Bahamas and Cuba  promising more devastation after flattening homes and cutting power to a string of Caribbean islands and killing at least 12 people. 
 A swath of South Florida remained under mandatory evacuations, including coastal communities in Miami and Palm Beach. In addition, Gov. Rick Scott said Friday morning that evacuation is mandatory for those living in cities south of Lake Okeechobee, the largest freshwater lake in the state. 
 "This decision was made due to our sole focus on life safety as Hurricane Irma approaches Florida," Scott said. 
 With long lines at gas stations and highways clogged, emergency officials advised people not to travel far, but locate local shelters that are safely away from storm surges. 
 Related: Zoos Hunker Down as Hurricane Irma Takes Aim 
 "I can guarantee you that I don't know anybody in Florida that's ever experienced what's about to hit South Florida," Brock Long, administrator of the Federal Emergency Management Agency, said at a news conference Friday morning. "They need to get out and listen and heed the warnings." 
 Ahead of Irma, Miami Beach was virtually deserted Friday morning, Mayor Philip Levine told TODAY. He said the city used buses and trolleys to help move people, including senior and homeless populations. 
 Hurricane warnings were issued Thursday night from Jupiter Inlet south around the Florida peninsula to Bonita Beach, as well as for the Florida Keys, Lake Okeechobee and Florida Bay. Storm surges in those areas could be anywhere from 3 feet to 6 feet or higher, according to the hurricane center. 
 The National Weather Service said that "locations may be uninhabitable for weeks or months" because of extreme winds, adding that "structural damage to buildings, with many washing away" was possible in some coastal areas. 
 PHOTOS: Hurricane Irma Carves Path of Destruction in Caribbean 
 The hurricane center said Thursday that a slight westward shift in Irma's projected path indicated that it could make landfall just east of Key Largo, bringing its eye wall, which carries the most destructive winds, over Miami with major hurricane winds. 
 Hurricane conditions were expected in portions of southern Florida and the Florida Keys on Saturday night or early Sunday. 
Tropical storm winds are likely to arrive in the FL Keys and south FL Saturday. Preparations should be rushed to completion.  #Irma pic.twitter.com/eto2KVWtgP
 "During this weekend, all significant hurricane impact threats are possible, including especially life-threatening storm surge flooding and damaging winds," the hurricane center added late Thursday. 
 The good news is that Irma "has peaked in intensity." But even a slightly weaker Irma could do serious damage to Miami and lash the low-lying city with 129-mph winds and gusts of up to 159 mph. 
 Hurricane-force winds extended 70 miles from the center, so even if Irma somehow doesn't make direct landfall on Florida at all, its very large wind field promises dangerous surf and coastal flooding throughout the Southeast coast well away from the center into early next week, forecasters said. 
 "We could see a little bit of weakening as it interacts with land and moves farther north into some high wind shear, but Irma's going to remain dangerous for the next several days," Michael Brennan, a senior hurricane specialist for the National Hurricane Center, told NBC News. 
 "This could easily be the most costly storm in U.S. history, which is saying a lot considering what just happened two weeks ago," Brian McNoldy, a hurricane researcher at the University of Miami, told NBC Miami, referring to Hurricane Harvey. 
 Scott warned Floridians on Thursday: "If you're told to evacuate, get out quickly. Based on what we now know, Miami-Dade will have major hurricane impacts with deadly storm surge ... and life-threatening winds." 
 About 31,000 people have already fled the Florida Keys, Scott said, who invoked the memory of Hurricane Andrew, a Category 5 storm that struck 25 years ago, killing 44 people and doing more than an adjusted $47.8 billion in damage just in the Sunshine State alone. It nearly wiped the city of Homestead off the map. 
 "This is much worse and more devastating on its current path," he added. 
 The governors of Georgia, South Carolina and North Carolina have all declared states of emergency. 
 On its way to Florida, Irma passed the Turks & Caicos Islands, a low-lying British territory, and parts of the Bahamas. 
 It hit Puerto Rico with a glancing blow late Wednesday, killing at least three people, after battering the islands of Barbuda, St. Bart and St. Martin. 
 Thousands of people were left homeless in French-controlled St. Martin, where four people were reported dead and Irma demolished 95 percent of buildings on the island. One death was reported on the Dutch side of the island. Another death was reported on the island of Barbuda. 
 Video obtained by NASA had the eye of Irma, the most powerful Atlantic hurricane on record, passing just to the north of the Dominican Republic on Thursday morning. 
 "Extremely dangerous Hurricane Irma heading for the Turks and Caicos islands," the National Hurricane Center said. "Hurricane and storm surge watch are in effect for portions of South Florida and the Florida Keys." 
 Florida Power & Light, the nation's third-largest electric utility, warned that damage to the state's energy grid could be devastating. 
 "Our service area will likely see widespread and substantial destruction that will require crews to literally rebuild parts of our electric system," Eric Silagy, the utility's president and chief executive, said Thursday. "Restoring power through repairs is measured in days, while rebuilding our electric system could be measured in weeks." 
 Silagy said FPL, which has mutual-assistance agreements with utilities in other states, was positioning 11,000 employees and contractors in areas that are expected to be hardest hit. "But no utility is hurricane-proof, especially when facing a storm such as Irma," he said. 
 The state was also working to get more fuel to drivers as many areas began running low on gas. 
Hurricane watches & warnings remain in effect for most of Southern & Central Florida! #Irma #NBC4NY pic.twitter.com/ZFzLXEM44J
 About 39 percent of gas stations in Miami-Fort Lauderdale were without fuel late Wednesday afternoon, according to GasBuddy, a Boston company that tracks fuel prices and availability. As far north as Gainesville, near the Georgia border, 44 percent of stations had run dry, it said. 
 "We know fuel is very important. And we're absolutely devoting every state resource to addressing this and we're talking to the federal government about their support," Scott said. 
 In Washington, President Donald Trump said, "Florida is as well prepared as you can be for something like this, and we'll see what happens." 
 "We are with the people of Florida," added the president, whose Mar-a-Lago mansion is in Palm Beach. 
