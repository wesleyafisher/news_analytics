__HTTP_STATUS_CODE
200
__TITLE
Mississippi School Board Pulls To Kill a Mockingbird From Reading List
__AUTHORS
Chandelis R. Duster 
__TIMESTAMP
Oct 16 2017, 4:11 pm ET
__ARTICLE
 The school board in Biloxi, Mississippi, has pulled "To Kill a Mockingbird" from an eighth-grade reading list after receiving complaints about wording in the book. 
 Last week, Kenny Holloway, the board's vice president, said there was language in the book that "makes people uncomfortable." 
 "We can teach the same lesson with other books," Holloway said, according to the The Sun Herald newspaper. "It's still in our library. But they're going to use another book in the eighth-grade course." 
 The Biloxi School District didn't immediately respond to a request for comment. 
 The book will still be available to check out from the library and is still listed on the school website as part of the eighth-grade English Learning Arts program. 
 The Sun Herald reported that a reader said the decision stems around the book's use of the n-word. A syllabus posted on the school system's website says questions to focus on include "What does it mean to be racist?" and "What is the difference between tradition and ignorance?" 
 The Pulitzer Prize-winning novel, written by the late Harper Lee and published in 1960, centers on racial inequality and injustices in the Deep South. In 2009 and 2011, the American Library Association listed the novel as one of the "Top Ten Most Challenged Books." 
 The move has sparked national outrage on social media and a debate on discussing racism in schools. 
 "When school districts remove 'To Kill A Mockingbird' from the reading list, we know we have real problems," Arne Duncan, who was education secretary during most of President Barack Obama's administration, said on Twitter. 
When school districts remove 'To Kill A Mockingbird' from the reading list, we know we have real problems. https://t.co/TF3fGZmvXp
 And "Black-ish" actor and comedian Deon Cole said the move was another example of people changing history and "taking everything away from black people." 
if to kill a mockingbird makes you uncomfortable you should probably be reading to kill a mockingbird.
 Follow NBCBLK on Facebook, Twitter and Instagram 
