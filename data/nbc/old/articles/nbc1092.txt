__HTTP_STATUS_CODE
200
__TITLE
Teamsters Seek Ouster of Big Pharma Boss
__AUTHORS
Corky Siemaszko
__TIMESTAMP
Oct 13 2017, 5:00 pm ET
__ARTICLE
 The Teamsters are seeking to punish a Big Pharma boss for flooding the country with prescription painkillers. 
 The powerful labor union is calling on Cardinal Health Inc. shareholders to strip the company's CEO, George Barrett, of his title and replace him with an independent chairman at their Nov. 8 meeting. 
 While the opioid epidemic engulfs our communities and exposes shareholders to growing risks, Cardinal Health has continued to evade responsibility and reward executives who have turned a blind eye to the tragedy we face, the union's general secretary-treasurer, Ken Hall, wrote in a letter to Cardinal shareholders. 
 Our nation deserves better from Cardinal, and that starts with reforms that cement the kind of independent and accountable leadership required to address the opioid crisis. 
 Cardinal, along with other big pharmaceutical distributors like AmerisourceBergen Corp. and McKesson Corp., has been accused of fueling the deadly opioid epidemic that has ravaged Rust Belt states where many union members live, like Michigan, Ohio, Pennsylvania and West Virginia. 
 In the letter, Hall reminded shareholders that Cardinal has paid out more than $100 million to settle litigation with several states and the federal Drug Enforcement Agency for failing "to report suspicious orders of controlled substances." 
 Related: Teamsters Tell Big Pharma Shareholders: Vote No on Boss Bonus 
 Hall also noted that Cardinal, under Barrett, responded to an opioid lawsuit filed by Huntington, West Virginia, with a strategy that pointed fingers at everyone from first responders to respected local organizations like Lilys Place, which nurses babies born with addiction. 
 That tactic was widely condemned by lawmakers. Meanwhile, first lady Melania Trump toured Lilys Place on Tuesday. 
 Barrett, who has been Cardinals chairman and CEO since 2009, was paid $13,661,087 in salary and compensation last year, records show. 
 Ellen Barry, a Cardinal spokeswoman, declined to comment on the Teamsters letter. She directed NBC News to the companys Sept. 21 proxy statement in which the board recommends the shareholders vote against the Teamster proposal. 
 The Board believes that Mr. Barrett is best suited to serve as Chairman because of his unique knowledge of our business, the healthcare industry, and our shareholders, the statement reads. 
 The Teamsters used their clout in July to punish the CEO of McKesson, John Hammergren, by convincing shareholders to turns their thumbs down on the $1.1 million bonus that he was awarded earlier this year. 
 The union argued that Hammergren didnt deserve the money after McKesson  the nations largest pharmaceutical wholesaler  paid a record $150 million fine to the federal government for violating the Controlled Substances Act by failing to report suspicious orders of oxycodone and hydrocodone pills. 
 While the vote was not binding, it was big black eye for Hammergren because it is rare for shareholders to oppose a bonus for the boss, according to data compiled by Bloomberg. 
