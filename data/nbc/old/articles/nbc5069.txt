__HTTP_STATUS_CODE
200
__TITLE
U.S. Airstrikes in Iraq, Syria Killed Twice the Civilians Previously Announced
__AUTHORS
Alexander Smith
__TIMESTAMP
Nov 10 2016, 11:07 am ET
__ARTICLE
 More than twice as many civilians have been killed by U.S. airstrikes in Iraq and Syria than was previously believed, officials have revealed. 
 The military said Wednesday that after months of reviewing reports and databases it found its airstrikes have killed estimated 64 civilians in in the past 12 months. 
 This brought estimated civilian deaths to 119 since the U.S. started bombing ISIS in 2014, CENTCOM spokesman Maj. Shane Huff confirmed to NBC News. The previous estimate published last month showed 55 civilians were believed killed by American airstrikes during the campaign, he said in an email. 
 The figures are still far lower than those alleged by Amnesty International, which says around 300 civilians have been killed by U.S.-directed coalition airstrikes in Syria alone. 
 The U.S. is involved in an international effort to squeeze ISIS out of their strongholds in Iraq and Syria. 
 In Iraq, the United Nations says the extremists are using tens of thousands of people as human shields, essentially gathering civilians near their positions so the coalition is faced with the prospect of collateral damage. 
 CENTOM revealed the figures in a statement issued Tuesday. It said an estimated 64 people were killed and eight others injured in 24 airstrikes since last November. Some 37 people have been injured since 2014, Maj. Huff said in a later email. 
 The military said it "thoroughly reviewed the facts and circumstances" surrounding each death. 
 Spokesperson Col. John J. Thomas said in Tuesday's statement that it was a "key tenant of the ... air campaign that we do not want to add to the tragedy of the situation by inflicting addition suffering." 
 He said that the military's assessments showed that "in each of these strikes the right processes were followed" and that they "complied with law of armed conflict" because "significant precautions were taken, despite the unfortunate outcome." 
