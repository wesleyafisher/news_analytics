__HTTP_STATUS_CODE
200
__TITLE
NYPD, Prosecutors Point Fingers Over Harvey Weinstein Probe
__AUTHORS
Tracy Connor
__TIMESTAMP
Oct 11 2017, 11:31 am ET
__ARTICLE
 The Harvey Weinstein scandal has spawned a finger-pointing fight between two law enforcement agencies. 
 In dueling statements, the NYPD and the Manhattan district attorney's office each blamed the other for the fact that Weinstein was not charged with a crime in 2015 after an Italian model accused him of groping her and captured him on tape during a sting operation. 
 "I won't do it again," the movie mogul says on the recording, made public by The New Yorker magazine in a story detailing harassment and assault allegations against Weinstein by 13 women. 
 The damning audio raised the question of why Weinstein never faced criminal charges in the only known case where an accuser went to authorities. 
 The NYPD said they passed the case over to the DA's office. 
 The DA's office then issued a statement saying prosecutors were not consulted before the attempted sting and that what Weinstein said on the tape was "insufficient to prove a crime." The NYPD shot back that the recording corroborated the 22-year-old woman's story and "was just one aspect of the case against the subject." 
 The model, Ambra Battilana Gutierrez, brought allegations against Weinstein more than two years before last week's report in The New York Times unleashed a wave of allegations  some from A-list movie stars  spanning decades. 
 Related: Reporter Says Weinstein Ordered Her to 'Be Quiet' While He Exposed Himself 
 Gutierrez said she met the Hollywood powerhouse at a reception and was then invited to a business meeting at his Manhattan office. Once there, she told police, he groped her breasts and tried to put his hand up her skirt over her protests. 
 Afterward, he invited her to see a show, but she instead went to police, who arranged for her to meet him again and wear a wire, the New Yorker reported. 
 At the Tribeca Grand Hotel, Weinstein cajoled Gutierrez to come into his room while he took a shower, the recording reveals. 
 "Why yesterday you touch my breast?" Gutierrez asked him. 
 "Oh, please," Weinstein replied. "I'm sorry, just come on in. I'm used to that. Come on. Please." 
 "No, but I'm not used to that," the model said. 
 "I won't do it again," Weinstein responded. "Come on, sit here. Sit here for a minute, please?" 
 Related: Democrats Rushing to Give Weinstein Donations to Charity 
 Weinstein was questioned by police, but less than two weeks later, prosecutors said they would not charge him. 
 "If we could have prosecuted Harvey Weinstein for the conduct that occurred in 2015, we would have. Mr. Weinstein's pattern of mistreating women, as recounted in recent reports, is disgraceful and shocks the conscience," the DA's office said in a statement on Tuesday as the scandal intensified. 
 "After the complaint was made in 2015, the NYPD  without our knowledge or input  arranged a controlled call and meeting between the complainant and Mr. Weinstein. The seasoned prosecutors in our Sex Crimes Unit were not afforded the opportunity before the meeting to counsel investigators on what was necessary to capture in order to prove a misdemeanor sex crime. 
 "While the recording is horrifying to listen to, what emerged from the audio was insufficient to prove a crime under New York law, which requires prosecutors to establish criminal intent. Subsequent investigative steps undertaken in order to establish intent were not successful. This, coupled with other proof issues, meant that there was no choice but to conclude the investigation without criminal charges." 
 The NYPD, in its next statement, suggested police had turned over solid evidence to prosecutors. 
 "The case was carried out by experienced detectives and supervisors from NYPD's Special Victims Unit. The detectives used well established investigative techniques," it said. 
 "The recorded conversation with the subject corroborates the acts that were the basis for the victim's complaint to the police a day earlier. This follow-up recorded conversation was just one aspect of the case against the subject. This evidence, along with other statements and timeline information was presented to the office of the Manhattan District Attorney." 
 The New Yorker reported that after prosecutors declined to prosecute Weinstein, Gutierrez reached an out-of-court settlement with him. She signed a nondisclosure agreement and an affidavit in which she said there was no misconduct. 
 While Weinstein has recently apologized for his treatment of women over the years, allegations against him now include accusations of rape. His spokesperson has since said any allegations of nonconsensual sex are "unequivocally denied." 
