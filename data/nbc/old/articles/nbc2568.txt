__HTTP_STATUS_CODE
200
__TITLE
Rohingya Exodus: Fleeing Violence in Myanmar
__AUTHORS
NBC News
__TIMESTAMP
Oct 15 2017, 11:30 am ET
__ARTICLE
Rohingya refugees carry their belongings as they walk on the Bangladesh side of the Naf River after fleeing Myanmar, on Oct. 2, 2017,in Cox's Bazar, Bangladesh.
More than half a million Rohingya refugees have fled an offensive by Myanmar's military that the United Nations has called 'a textbook example of ethnic cleansing'. The refugee population is expected to swell further, with thousands more Rohingya Muslims said to be making the perilous journey on foot toward the border, or paying smugglers to take them across by water in wooden boats.
Hundreds are known to have died trying to escape, and survivors arrive with horrifying accounts of villages burned, women raped, and scores killed in the 'clearance operations' by Myanmar's army and Buddhist mobs that were sparked by militant attacks on security posts in Rakhine state on Aug. 25.
A Rohingya refugee boy is carried in a basket by a relative after crossing the border on the Bangladesh side of the Naf River on Sept. 24.
What the Rohingya refugees flee to is a different kind of suffering in sprawling makeshift camps rife with fears of malnutrition, cholera, and other diseases. Aid organizations are struggling to keep pace with the scale of need and the staggering number of them - an estimated 60 percent - who are children arriving alone.
Bangladesh, whose acceptance of the refugees has been praised by humanitarian officials for saving lives, has urged the creation of an internationally-recognized 'safe zone' where refugees can return, though Rohingya Muslims have long been persecuted in predominantly Buddhist Myanmar.
World leaders are still debating how to confront the country and its de facto leader, Aung San Suu Kyi, a Nobel Peace Prize laureate who championed democracy, but now appears unable or unwilling to stop the army's brutal crackdown.
A Rohingya refugee woman is helped from a boat as she arrives exhausted on the Bangladesh side of the Naf River at Shah Porir Dwip on Oct. 1.
Rohingya refugees carry their belongings as they walk through water on the Bangladesh side of the Naf River on Sept. 28.
A Rohingya refugee girl wears a plastic bag as she walks in the monsoon rains at the Palongkali refugee camp on Sept. 19.
A Rohingya refugee woman uses a candle to light her tent at the Palongkali refugee camp, on Oct. 1.
A Rohingya refugee boy desperate for aid cries as he climbs on a truck distributing aid for a local NGO near the Balukali refugee camp on Sept. 20.
A Rohingya refugee man who was shot in the back by the Myanmar army is helped by a relative after crossing the border to the Bangladesh side of the Naf River on Sept. 24.
Rohingya refugee boys study the Quran, Islam's holy book, at a madrassa or religious school on Oct. 2.
Rohingya refugees build a new mosque at the Balukali Refugee Camp on Oct. 2.
A Rohingya refugee woman sits exhausted after collapsing on a beach on the Bangladesh side of the Naf River at Shah Porir Dwip on Oct. 1.
A Rohingya refugee woman is carried by relatives near the border on the Bangladesh side of the Naf River on Oct. 2.
Rohingya refugees desperate for aid crowd as food is distributed by a local NGO near the Balukali refugee camp on Sept. 20.
A woman carries the body of a Rohingya refugee boy as others are seen wrapped in white sheets prior to burial after they died when their boat capsized while fleeing Myanmar on Sept. 29.
An elderly Rohingya refugees woman sits outside her shelter in the sprawling Balukali refugee camp on Sept. 22.
A Rohingya refugee washes at a well at the Palongkali refugee camp on Sept. 26.
A Rohingya refugee woman holds her child as she stands outside her shelter at the sprawling Balukali refugee camp on Sept. 27.
A Rohingya refugee family reacts as they disembark from a boat after arriving on the Bangladesh side of the Naf River at night on Sept. 26.
