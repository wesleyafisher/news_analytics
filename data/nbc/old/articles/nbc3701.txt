__HTTP_STATUS_CODE
200
__TITLE
Las Vegas Shooting: Photos Capture Chaos of Concert Massacre
__AUTHORS
NBC News
__TIMESTAMP
Oct 2 2017, 9:45 am ET
__ARTICLE
People scramble for cover after gunfire was heard atthe Route 91 Harvest country music festival grounds on Oct. 1 in Las Vegas.
A lone gunman opened firefrom the 32nd floor of theMandalay Bayhotelin the worst mass shooting in modern American history.
A man lays on top of a woman as others flee the festival grounds.

A pair of cowboy boots lies in the street outside the concert venue.
People run from the sceneafter gunfire was heard.
A police officer takes cover behind a police vehicle during the shooting.
Las Vegas Metropolitan Police and medical workers stand in the intersection of Tropicana Avenue and Las Vegas Boulevard South after the shooting.
Concertogers run for cover.
A woman sits on a curb at the scene of the shooting.
A woman pushes a man in a wheelchair away from the scene.
Las Vegas police stand guard outside the festival grounds.
A police helicopter circles the Mandalay Bay and Luxor hotels.

The shooter,Stephen Craig Paddock, fired shot from two rooms at the Mandalay Bay Resort and Casino.
More on Las Vegas shooting
