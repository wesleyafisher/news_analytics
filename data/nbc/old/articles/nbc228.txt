__HTTP_STATUS_CODE
200
__TITLE
Carrie Fisher Once Sent a Cows Tongue to a Producer Who Allegedly Assaulted Her Friend
__AUTHORS
Chelsea Bailey
__TIMESTAMP
Oct 17 2017, 4:13 pm ET
__ARTICLE
 As the scandal involving Harvey Weinstein continues to rock Hollywood, a screenwriter said she finally feels able to share her own story of survival  and the bold way actress Carrie Fisher came to her defense after she was allegedly sexually assaulted by a producer. 
 Screenwriter Heather Robinson said she and Fisher became friends after they met online in an AOL chat room where they would talk about the industry and workshop her screenplays. It was in that chat room that Robinson said she revealed an encounter she had with an award-winning producer, whom she declined to name. 
 Robinson said she never expected the actress' response would come in the form of an animal part. 
 In an interview with Arizona radio program "The Morning Mix" that has gone viral, Robinson recalled a night in 2000 during which a producer allegedly sexually assaulted her after inviting her to dinner to discuss an upcoming project. 
 "When it happened, it happened so quickly that I felt ashamed of myself," Robinson said in the radio interview. She described how the producer picked her up in his car and, within minutes, pulled to the side of the road. 
 "He reaches over and grabs the handle of the chair of the passenger seat and pulls me backwards," she said. "All of a sudden he's on top of me and he has his right hand that was busy, and his left hand was on my chest, by my neck, holding me down." 
 Robinson said she fought back, managed to escape the car, and ran. When she got home, she told her mom about the incident and debated whether or not she needed to go to the police. Ultimately, fearing for her safety, Robinson decided to remain silent. 
 Related: Will Flood of Weinstein Accusers Bring Sweeping Change to Hollywood? 
 Later that night, she said she saw that Fisher was online and told the "Star Wars" heroine what had happened in a series of messages. 
 "She stayed quiet and listened to me and then she said, 'I promise you he will never touch you again,'" Robinson told NBC News in an email. "She was exactly what I needed. Someone to just hold space for me  allowing me to be scared, hurt, confused, angry." 
 Weeks later, Robinson said Fisher messaged her online and told her that she'd hand-delivered the producer a gift: a cow tongue wrapped in a blue Tiffany's box and topped with a bow. 
 "It was a cow's tongue from Jerry's Deli in Westwood with a note that said If you ever touch my darling Heather or any other woman again, the next delivery will be something of yours in a much smaller box,' Robinson said, adding that the "gift" made her laugh after the traumatizing incident. 
 Photos: Carrie Fisher: Actress, Author and Princess 
 "She had an amazing heart," Robinson said in an email. "If you were one of the lucky people that she loved, she loved you with everything she had. In her world, it was okay to feel vulnerable because she made you feel safe in doing so. That alone is a huge gift." 
 Robinson said she has never seen or heard from the producer again and that it took her years to realize that the assault was not her fault. But the Weinstein scandal has brought the conversation around sexual assault and harassment in Hollywood back into the spotlight, and Robinson said she's happy to see women garnering support. 
 Related: Carrie Fisher Mourned by Fellow Stars: Our Princess Has Passed Away 
 Robinson said that, in part because of social media, she thinks women throughout the industry are feeling the same sense of safety and acceptance that Carrie gave her. 
 "It allows us the power to speak out and share our stories and to connect with other people who many not be at the point where I am," she said. "It's truly an individual journey and some may never shed light on what happened to them, and that is okay. As long as they see that they are not alone." 
