__HTTP_STATUS_CODE
200
__TITLE
Inside the Secret Rescue of Yazidi Sex Slaves From ISIS Captors 
__AUTHORS
Richard Engel
__TIMESTAMP
Dec 6 2016, 7:41 pm ET
__ARTICLE
 DUHOK, Iraq  It was dark as the car sped along a small road on the outskirts of the embattled Iraqi city of Mosul. The car was driving fast, but not so fast as to draw attention. That was essential. The lives of the two men in the front seat depended on their ability to keep a low profile and pass through undetected. 
 In the passenger seat, Khaleel Al-Dhaki was focused on the secret mission he was leading to rescue a Yazidi woman and her child, both of whom were taken by ISIS and dragged into Mosul. 
 This kind of operation cant be done during daytime, he later told NBC News. We are basically going in there to kidnap them back from ISIS. 
 Al-Dhaki, a lawyer by training, runs a small team of activists who regularly make dangerous trips into ISIS territory to rescue Yazidi women, members of a non-Muslim minority that live mostly in northern Iraq. 
 Saving a soul is the best thing a man can do, Al-Dhaki said. You get more motivated when you watch them meeting their families. I cant describe the moment of the reunion. We devote our whole lives to rescuing these women. 
 Related: Yazidi Women Tell of Rape and Enslavement at Hands of ISIS 
 Al-Dhaki estimates ISIS kidnapped around 7,000 Yazidis and that roughly 3,000 of them managed to escape on their own or were ransomed out of bondage. The fate of 1,000 Yazidi men remains unknown, he adds. 
 That leaves, by his calculation, some 3,000 Yazidi women and children like Leila and Ahmed in ISIS hands. 
 In 2014, as ISIS conquered large parts of the region, it targeted Yazidis for extermination, labeling them devil worshipers, executing some of the men and kidnapping them for forced labor. Yazidi women, sometimes with their children, were taken as sex slaves. In 2015, ISIS' propaganda magazine, Dabiq, boasted about reviving the ancient practice of slavery. 
 But ISIS is now under intense pressure. Its last stronghold in Iraq, the city of Mosul, is surrounded by Iraqi and Kurdish forces who are elbowing their way in with close support and guidance from U.S. troops. Amid the offensive, Al-Dhaki was sneaking into Mosul to steal the hostages from ISIS. 
 Related: ISIS Mines Pose 'Unprecedented' Threat to Families in Mosul 
 He had arranged to pick up a 23-year-old woman and her 3-year-old son. Although he shared her real name with NBC News, al-Dhaki asked that she be called "Leila"  for her safety and the safety of other Yazidis held by ISIS. Leila had managed to leave her captors home and go with her son to a safe house that al-Dhakis team had secured for them. 
 Now, outside that safe house, Leila and her son were climbing into the car. She was wearing a headscarf that left her face uncovered. She appeared terrified and sat silently as the car passed other vehicles along the road, each one potentially driven by an ISIS fighter. 
 The boy, whom NBC News is calling "Ahmed," slept in his mothers arms in the backseat. 
 The extraction was a success. Relatives were waiting to receive Leila and Ahmed once they were safely out of ISIS territory. 
 They cried, hugged and kissed one another before driving into Kurdish-held territory near the Turkish border, far away from ISIS. Leila and her son had been gone for two-and-a-half years. 
 They took my husband away from me and gave me to an ISIS fighter. They married me to him, she told NBC News. Leila is now a refugee living with relatives in a tented camp. Her own village is still occupied by ISIS. 
 Related: Amal Clooney Takes on ISIS for Persecuted Yazidis 
 Leila spoke hesitantly, struggling to finish her sentences. She described how, after the trauma of being separated from her husband and being forced to marry another man, she was resold twice again to other ISIS radicals. ISIS fighters seemed to trade Yazidi women like baseball cards. 
 Leila's last rapist was a squat, thickly-built Iraqi fighter with an unkempt beard that fell to his chest. His wife was also an ISIS fanatic. 
 I hated her even more than him, Leila said. She would beat my son. 
 As Leila spoke, Ahmed was crying and throwing whatever he picked up from the carpet covering the tents floor. For most of his life, Ahmed had been surrounded by violent armed men. To prevent Leila from escaping, her ISIS captor would take Ahmed with him when he went to ISIS headquarters, where acts of savage brutality were carried out. 
 Once the the Mosul offensive began, Leila said her captor and his wife wore explosive suicide vests in their home, ready to kill and die if U.S.-backed Iraqi troops came knocking. 
 How had all of this shaped Ahmed during his formative years? Al-Dhaki said many of the Yazidi children he has rescued remain troubled. 
 They need help. They are showing signs of violence. They are not the same as before, he said. 
 Leila said that Ahmed had seen beheading videos and passed by the body of a crucified man on the street. She doesnt know what he saw during those long hours at ISIS headquarters. 
 As Leila spoke about the past from her new home, Ahmed walked back inside the tent after briefly going outside to play with other children. He came back carrying a pair of scissors. Leila went over and tried to pry the scissors from his hands. Ahmed screamed and cried. 
 Ill slaughter you, he told his mother. 
 Al-Dhaki said another Yadizi boy hed rescued had tried to slit his sisters throat, apparently mimicking something he had seen ISIS fighters do. 
