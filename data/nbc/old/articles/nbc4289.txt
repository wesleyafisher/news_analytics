__HTTP_STATUS_CODE
200
__TITLE
Fentanyl Crisis: DEA Issues Guideline for First Responders Dealing With Deadly Drug
__AUTHORS
Corky Siemaszko
__TIMESTAMP
Jun 6 2017, 6:01 pm ET
__ARTICLE
 The first responders on the front line in the battle against the national opioid epidemic got a new guideline Tuesday from the Drug Enforcement Agency on how to handle the incredibly deadly drug fentanyl. 
 It's called Fentanyl: A Brief Guide for First Responders. And Acting DEA chief Chuck Rosenberg said it should be required reading. 
 It was produced thanks to Patrolman Chris Green of the East Liverpool (Ohio) Police Department, as well as officers in New Jersey, Connecticut, Maryland and Georgia who were inadvertently exposed to the extremely dangerous drug and survived. 
 This is not a hypothetical problem, said Deputy Attorney General Rod Rosenstein, who described how Green suddenly passed out after brushing some powder of his shirt while not wearing gloves. The officer needed four doses of emergency overdose antidote Narcan to be revived. The spread of fentanyl means that any encounter a law enforcement officer has with an unidentified white powder could be fatal. 
 Related: Fentanyl Rises as Cause of Drug Overdose Deaths 
 Green, a five year veteran, was hurt in May. He is back on the job, a dispatcher there said. 
 We need everybody in the United States to understand how dangerous this is, Rosenberg said. Exposure to an amount equivalent to a few grains of sand can kill you. 
 There is also a video to go along with the guideline. 
 Assume the worst, Rosenberg says in the video. Dont touch this stuff or the wrappings that it comes in without the proper personal protective equipment. 
 Meanwhile, the DEA has moved to declare a designer drug called Acryl fentanyl a controlled substance and thus make it illegal to buy online. 
 This could happen in the first part of July, said DEA spokesman Russ Baer. They can still advertise online, but it would be against federal law. 
 Related: Fentanyl Crisis: Ohio Cop Accidentally Overdoses During Drug Call 
 Laced with heroin or cocaine, Acryl fentanyl is competing with bullets for lives in Chicago. 
 As of Monday, it had figured in the deaths of 51 people in Chicago and the citys suburbs this year, said Becky Schlikerman, spokeswoman for the Cook County Medical Examiner. 
 Related: Prince Died of Accidental Overdose of Painkiller Fentanyl 
 Thats a huge jump over the seven fatal Acryl fentanyl overdoses last year in the nations third largest city. And that number is expected to climb as autopsies are completed and toxicology results come in, said Schlikerman. 
 There have been 261 homicides in Chicago this year as of Tuesday, 243 the result of gunfire, a Chicago Police Department spokesman said. 
