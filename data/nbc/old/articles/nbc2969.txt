__HTTP_STATUS_CODE
200
__TITLE
New Test Detects Heart Disease Risk, Especially for Black Women
__AUTHORS
__TIMESTAMP
Dec 15 2014, 6:22 pm ET
__ARTICLE
 A new test makes it easier for doctors to measure a fairly new indicator of heart disease risk  one that is especially helpful for African-American women. 
 The U.S. Food and Drug Administration approved the test Monday. It measures the inflammation caused by the buildup of dangerous gunk in the arteries and can show a patient risks having a heart attack or a stroke even if she doesnt have high cholesterol. 
 A cardiac test that helps better predict future coronary heart disease risk in women, and especially black women, may help health care professionals identify these patients before they experience a serious CHD event, like a heart attack, said FDAs Alberto Gutierrez. 
 The test measures lipoprotein-associated phospholipase A2 (Lp-PLA2) in a patients blood. It can indicate whether artery-narrowing plaque is building up and irritating the lining of the blood vessels. 
 Doctors have been able to custom-order the test. But a commercial version should be easier to get and doctors will be more likely to use it, says American Heart Association expert Dr. Jenifer Mieres of the North Shore-LIJ Health System and co-author of the book "Heart Smart for Black Women and Latinas." 
 It makes it much more readily available and reimbursable by insurers, Mieres told NBC News. When you put it in context with the fact that black women are at the highest risk of coronary disease  this makes it really easier to target women at higher risk. 
 Doctors run lots of different tests to assess someones risk of heart disease, which is the No. 1 killer of Americans. They measure cholesterol, blood pressure and heart rate, they test the electrical signals that keep the heart beating and they may also run imaging tests to check for clogged arteries. 
 Studies show that people with Lp-PLA2 levels above 225 had a coronary heart disease rate of 7 percent over the next five years, compared to 3 percent for people with lower levels. But African-American women had a much higher heart disease risk if their readings were high. 
 Cholesterol tests looking for low density lipoprotein (LDL or bad cholesterol) dont do a very good job of predicting who will have a stroke. Half of cardiovascular events such as stroke are in people with healthy LDL levels. Drugs such as statins can bring levels of LP-Pla2 down, and companies are also working on drugs that specifically lower the compound. 
