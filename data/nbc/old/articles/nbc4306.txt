__HTTP_STATUS_CODE
200
__TITLE
Chronic Pain Sufferers Are Scared by Ohios New Opioid Rules
__AUTHORS
Corky Siemaszko
__TIMESTAMP
Apr 6 2017, 5:35 am ET
__ARTICLE
 Chronic pain sufferers fear they could become casualties in the war on Ohios opioid overdose epidemic. 
 They say recent moves by Gov. John Kasich to fight the plague by restricting how many painkillers can be prescribed will add to their anguish  and could force them to go underground to find the relief they need to make it through a day. 
 We are being punished for being in pain, said Amy Monahan-Curtis, 44, who has been living in agony since 1993 due to condition called cervical dystonia that causes her neck muscles to contract involuntarily. 
 Monahan-Curtis, who lives in Cincinnati, said limiting painkiller prescriptions for adults to just seven days at a time, as Ohio now does, means an additional financial burden is being placed on the pain patient if a primary doctor will write a prescription to pay for multiple scripts. 
 What these regulations are doing is forcing pain patients out of terror and extreme pain to the street, to find something to control their pain, she said. Legislators are making the drug problem much worse. 
 Monahan-Curtis said she already follows strict rules laid out in the narcotic contract she signed when she enrolled in a pain management clinic, including having to submit to random urine samples. 
 I can be called into the office at any time in between my monthly appointments, asked to bring in my narcotics bottle for a count to see if I have an appropriate amount left and am not selling them or taking too many, she said. 
 And if her meds are lost or stolen, Monahan-Curtis said, they will not be replaced. 
 Kasich, when he made his announcement last week, said the new rules dont apply to patients who already take painkillers for things like cancer or to treat dying patients receiving hospice care. 
 We have tried to make it as clear as possible that this is not aimed at chronic pain sufferers, said Cameron McNamee at Ohios state Board of Pharmacy. 
 A guide put out by Kasichs administration clearly states that the limits only apply to the treatment of acute pain. 
 I suspect what may have caused some confusion is that two pieces of legislation that do address chronic pain dropped a day before Gov. Kasich made his announcement, McNamee said. I can tell you with 100 percent certainty that the rules proposed by the governor are not aimed at persons who suffer from chronic pain. Those individuals will still be able to get their appropriate medications. 
 Monahan-Curtis said she doesnt believe it. She said the online message boards for chronic pain sufferers lit up after Kasich unveiled the restrictions. 
 
