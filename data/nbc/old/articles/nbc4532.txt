__HTTP_STATUS_CODE
200
__TITLE
Pepe the Frog Is Dead: Creator Kills Off Meme Absorbed by Far-Right
__AUTHORS
Daniella Silva
__TIMESTAMP
May 8 2017, 1:20 pm ET
__ARTICLE
 Pepe the Frog, the meme that was transformed from a once peaceful frog dude into a symbol commonly appropriated by racist or bigoted internet memes, has croaked. 
 Pepes inventor, artist and illustrator Matt Furie, drew the green frog in an open casket as part of a one-page strip for publisher Fantagraphics Books on Saturday marking Free Comic Book Day. 
 Furie also posted an image on his Tumblr showing the frog in the casket, linking to an article on Comic Book Resources announcing the character's demise. 
 "While its unlikely Pepes official death will stop extremists from co-opting his image, this was, perhaps, the most effective way for Furie to reclaim his character; Pepes soul has returned to his creator. Rest in Peace," CBR author Shaun Manning said in the post. 
 The frog began as a harmless cartoon in 2005, Furie has said, in the Fantagraphics' comic book Boys Club. Pepe gained popularity over the next few years online as a meme before his more recent association with white supremacists, neo-Nazi groups and the so-called alt-right. 
 The Anti-Defamation League launched a #SavePepe campaign in October, in an attempt to reclaim the positive message originally behind the meme. Pepe the Frog had previously been declared a hate symbol by the ADL. 
 Furie said in a post on TIMEs website the day before the campaign was launched that it was completely insane that Pepe has been labeled a symbol of hate, and that racists and anti-Semites are using a once peaceful frog-dude from my comic book as an icon of hate. 
 The ADL said that while cartoons origins were inoffensive, it had been increasingly appropriated for bigoted themes in "a tendency exacerbated by the controversial and contentious 2016 presidential election." 
 Pepe's image was controversially included in a post on Instagram during the campaign by President Donald Trump's eldest son. Back in October 2015, Trump's Twitter account had also shared an image of the frog resembling Trump. 
