__HTTP_STATUS_CODE
200
__TITLE
Pokemon Go Spurs Lifestyle Changes, Business Boom as it Rolls Out in Asia
__AUTHORS
Reuters
__TIMESTAMP
Aug 19 2016, 7:36 am ET
__ARTICLE
 Asian fans of smartphone game "Pokemon Go" are hunting out the best telecom providers and network gear to overcome the hurdle posed by patchy network signals in their race to capture virtual cartoon characters. 
 From Indonesia to Hong Kong and Cambodia, the wild popularity of Nintendo's augmented reality app is also driving lifestyle changes for many gamers, who must trudge through real-life locations in their quest. 
 The game launched in many Southeast Asian countries on August 5, a month after the United States, New Zealand and Australia, but enthusiasts are finding they must first vanquish shaky transmission signals. 
 In Indonesia, Muchamad Syaifudin, a 29-year-old bank employee, said he switched to a mobile carrier offering better data packages, while his friends snapped up modems, at a cost of $20 each, to lock on to the signals. 
 "We can bring the modems to play, especially at places where a signal is hard to find," Syaifudin told Reuters by telephone. 
 He and his friends living in Central Java, a province famed for its idyllic paddy fields and mountains, used to spend leisure time playing strategy games at home. 
 But now, armed with the new devices, they increasingly venture out to catch the Pokemons that appear at temples and other landmarks where people gather. 
 In Hong Kong, commuters are hopping on the trams known as "ding dings" in their forays, while in countries such as Cambodia, Laos and Vietnam, the U.S. State Department has sent tweets warning players to beware of unexploded wartime mines. 
 The game officially launched just two weeks ago in Indonesia, but tens of thousands of enthusiastic adopters among a population of 250 million had started playing earlier, using proxy sites to access app stores elsewhere. 
 Read More: Travel Destinations Are Using 'Pokemon Go' to Capture Tourists 
 Gamers are fueling a boom for modem makers, such as PT Smartfren Telecom Tbk, whose nationwide sales of 4G modems, priced around 300,000 rupiah ($23) each, have jumped fivefold in just two months. 
 The firm has launched new devices with bigger battery capacity, Derrick Surya, Smartfren's vice president for brand and marketing communication, told Reuters. 
 Retailers also benefit as more gamers seek devices to power up mobile connections and minimize the expense of data packages. 
 "More and more customers are looking for alternative sources of additional mobile network capacity," said Billy Cahya, a salesman at an electronics shop in the Indonesian capital, Jakarta. 
 Also surging, however, is the demand for game innovations, ranging from new characters to a higher maximum level players can reach, said Syaifudin. 
 "Looking for Pokemons, looking for items, battling at gyms...We have done that again and again," he said, referring to game sites where players stage contests between virtual characters. 
 "It probably needs a new concept to retain our interest." 
 Other players complain rural areas have few Pokestops, or stockpile sites to secure equipment needed to catch Pokemons. 
 "We have fewer Pokestops in Legazpi City compared to metropolitan areas," said Rey Anthony Ostria, a player in the Philippine city about 340 km (211 miles) from Manila, the capital. "In towns that I have visited, there's almost none." 
