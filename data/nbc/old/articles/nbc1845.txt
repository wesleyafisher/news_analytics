__HTTP_STATUS_CODE
200
__TITLE
Hurricane Irma Packing 110 MPH Winds and Headed West Over Atlantic
__AUTHORS
Corky Siemaszko
__TIMESTAMP
Sep 2 2017, 11:32 am ET
__ARTICLE
 While Texas is still struggling to recover from Harvey, more trouble was brewing Saturday out in the Atlantic  and its name is Irma. 
 Packing 110-mile-per-hour winds and expected to remain a hurricane in the coming days, Irma was about 1,220 miles from the Leeward Islands in the West Indies as of 11 a.m. ET, and marching west at a steady 15 mph clip, according to the latest National Hurricane Center bulletin. 
 The NHC said earlier that "Irma is expected to be a major hurricane when it moves closer to the Lesser Antilles early next week." An updated advisory Saturday morning referred to it as a "smaller hurricane" and that "some strengthening is forecast during the next 48 hours." 
 So does it pose any danger to the Harvey-battered U.S.? 
 "We don't know yet," Dennis Feltgen, a NHC spokesman and meteorologist, told NBC News. "It's just too far away." 
 Still, it's moving closer. When Irma was declared a dangerous Category 3 hurricane at 5 p.m. Thursday, it was about 3,000 miles southeast of Miami. It was downgraded slightly to a Category 2 on Saturday, but is expected to continue alternating in intensity. 
 "Fluctuations in strength, up or down, are possible during the next few days, but Irma is expected to remain a powerful hurricane through the weekend," an NHC bulletin states. 
 The "rapid intensification" of Irma's strength got everybody's attention at their headquarters in Miami, Feltgen said Thursday. 
 The last major storm named Irma appeared in the Atlantic on Oct. 2, 1978, and never made it to the U.S. coast. 
 Tropical Storm Irma came within 500 miles of the Azores before petering out on Oct. 5, according to the National Weather Service. 
