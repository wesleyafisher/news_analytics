__HTTP_STATUS_CODE
200
__TITLE
The Week in Pictures: Aug. 25 - Sept. 1
__AUTHORS
NBC News
__TIMESTAMP
Sep 1 2017, 9:45 pm ET
__ARTICLE
Families displaced by flooding from flooding by Hurricane Harvey shelter at Gallery Furniture in Richmond, Texas on Aug. 30, 2017.
Harvey initially came ashore as a Category 4 hurricane in Texas Aug. 25, then went back out to sea and lingered off the coast as a tropical storm for days, inundating flood-prone Houston. The storm brought five straight days of rain totaling close to 52 inches, the heaviest tropical downpour ever recorded in the continental U.S.
PHOTOS: Displaced Families Find Shelter at Local Furniture Stores
An evacuee is rescued by a Coast Guard helicopter from floodwaters brought by Tropical Storm Harvey in Beaumont, Texas on Aug. 30.
People wait to be rescued from their flooded homes after their neighborhood was inundated with rain from Hurricane Harvey on Aug. 28 in Houston.
People walk down a flooded street as they evacuate their homes following Hurricane Harvey on Aug. 28 in Houston.
PHOTOS:Hurricane Harvey: Life-Threatening Rain Hammers Houston
Muslim worshipers pray during the Hajj pilgrimage on the Mount Arafat, near Mecca, Saudi Arabia on Aug. 31.
The Islamic hajj pilgrimage draws over 2.3 million people from around the world to Saudi Arabia each year. The crowds, squeezed shoulder to shoulder in prayer five times a day, fill the city of Mecca and surrounding areas to perform a number of physically demanding and intricate rites.
Horse riders fire their gunpowder filled rifles as they take part in Tabourida, a traditional horsemanship show also known as Fantasia, in Mansouria, near Casablanca, Morocco on Aug. 17.
Thousands gathered recently in Mansouria, a small town south of the capital Rabat, to attend one of the oldest festivals in Morocco. Nineteen horse troupes came from different parts of the kingdom to celebrate a three-day event that blends courage, skill and tradition.
This image was released by The Associated Press this week.
Supporters of Kenya's opposition party joke as they pretend to cry for Kenyan President Uhuru Kenyatta on a street of the Kibera slum in Nairobi, on Sept. 1.
Kenya's Supreme Court nullified Kenyatta's election win last month as unconstitutional and called for new elections within 60 days, shocking a country that had been braced for further protests by opposition supporters.
No presidential election in the East African economic hub has ever been nullified. Opposition members danced in the streets, marveling at the setback for Kenyatta, the son of the country's first president, in the long rivalry between Kenya's leading political families.
Interstate highway 45 is submerged from the effects of Hurricane Harvey seen during widespread flooding in Houston on Aug. 27.
PHOTOS:Hurricane Harvey Causes Destruction After Making Landfall in Texas
Search and rescue crews wrestle a stray steer to try and put it in a trailer in a neighborhood flooded after the Addicks Reservoir overflowed in Houston on Aug. 29.
A home is surrounded by floodwater after torrential rains pounded southeast Texas following Tropical Storm Harvey near Orange, Texas on Aug. 31.
PHOTOS:Chilling Aerials Show Widespread Flooding Across Houston Area
Residents wade through floodwaters from Tropical Storm Harvey in Beaumont Place, Houston on Aug. 28.
PHOTOS:Houston Residents Weather Floodwater with their Pets
South Sudanese rebels carry an injured rebel after an assault on government forces on the road between Kaya and Yondu, South Sudan on Aug. 26.
Oil-rich South Sudan became the world's youngest nation when it won independence from neighboring Sudan in 2011 following decades of conflict. But civil war quickly followed in 2013 after President Salva Kiir, an ethnic Dinka, fired his deputy Riek Machar, a Nuer.
Since then the conflict has been punctuated by gruesome massacres of civilians and extreme sexual violence. Ethnic militias have divided the country into a patchwork of fiefdoms.
Men hang a sacrificial sheep up after it was slaughtered during Eid al-Adha celebrations on Sept. 1 in Gaziantep, Turkey.
Muslims around the world celebrated Eid al-Adha, or "Feast of Sacrifice," to commemorate the Prophet Ibrahim's test of faith. For the holiday, Muslims slaughter livestock and distribute the meat to the poor.
Residents of the La Vita Bella nursing home in Dickinson, Texas, sit in waist-deep floodwaters caused by Hurricane Harvey on Aug. 27.
The residents were saved later that same day after the image drew attention on social media.
See: Nursing Home Residents Saved from Harvey Floods After Photo Goes Viral
The submerged trunk of a Mercedes remains visible as it sits in water in a west Houston neighborhood on Aug. 29.
Samaritans help push a boat with evacuees to high ground during rain caused by Tropical Storm Harvey along Tidwell Road in east Houston on Aug. 28.
PHOTOS:Rescuers Evacuate Elderly From Flooded Assisted Living Home
Demonstrators clash during a free speech rally in Berkeley, California on Aug. 27. Black-clad anarchists stormed into what had been a largely peaceful protest against hate and attacked at least five people, including the leader of a politically conservative group who canceled an event a day earlier in San Francisco amid fears of violence.
African migrants stand on the deck of a rescue vessel in the Mediterranean Sea, southwest of Malta on Aug. 31. This week, over 200 people were rescued from the sea and then transferred to Italy.
Smoke from a wildfire west of Sisters, Ore., blankets the Deschutes National Forest on Aug. 27. Firefighters in southern Oregon gained a toehold Tuesday on a fire burning near the coastal town of Brookings but new evacuations were ordered after a flare-up on a different complex of lightning-caused fires in a remote area near the California border.
Government troops march towards the Mapandi bridge during a battle against insurgents who have taken over parts of Marawi City, Philippines on Aug. 30.
Philippine troops have fought one of their toughest clashes against militants loyal to ISIS in a Marawi, and three soldiers were killed and 52 wounded, many by rebel bombs as they pushed forward.
The Islamists shocked the country by seizing large parts of Marawi town in May. After more than 100 days of fighting, pockets of fighters remain dug in in the ruins.
People cover the bodies of Rohingya refugee women and children who died after their boat capsized while crossing the border through the Bay of Bengal, near Teknaf, Bangladesh on Aug. 31. Three boats carrying ethnic Rohingya fleeing violence in Myanmar capsized in Bangladesh and the bodies of 15 children and 11 women were recovered.
Last week, a group of ethnic minority Rohingya insurgents attacked at least two dozen police posts in Myanmar's Rakhine state, triggering fighting with security forces that left almost 400 people dead and forced at least 18,000 Rohingya to flee into neighboring Bangladesh.
Huan Huan holds her surviving panda cub inside her enclosure at the Beauval Zoo in Saint-Aignan-sur-Cher, France on Aug. 28.Chinese giant panda experts and French zookeepers are working to ensure the cub's survival after its twin died during the first-ever birth of the rare animal in France.
The pink, hairless male weighing five ounces is tiny compared to its 190-pound mother. She bore twins Friday, but the firstborn was too weak to survive. Panda births are closely watched because they remain rare there are only about 1,800 pandas in the wild in China and about 400 in captivity worldwide. Zookeepers frequently need to help raise the cubs to ensure their survival.
A road stretches along the Atacama desert in Chile's Huasco region on Aug. 26.
Commuters walk through water-logged roads after monsoon rains in Mumbai on Aug. 29. Torrential monsoon rains paralyzed India's financial capital Mumbai with water swamping offices, schools and roads and killing about 60 people.
In the last two months, more than 1,000 people have been killed in flooding events across India, southern Nepal and northern Bangladesh. Some 40 million more have seen their homes, businesses or crops destroyed.
A contestant in the 'fantasy' category prepares to be judged during a body-painting festival in Daegu, South Korea on Aug. 26.
A woman and her poodle wait to be rescued while floating on an air mattress in floodwaters from Tropical Storm Harvey in Houston on Aug. 27.
The Week in Pictures:August 11 - 18
