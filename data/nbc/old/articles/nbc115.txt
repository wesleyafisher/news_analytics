__HTTP_STATUS_CODE
200
__TITLE
McCain Condemns 'Half-Baked, Spurious Nationalism' in Speech
__AUTHORS
Rachel Elbaum 
Associated Press
__TIMESTAMP
Oct 17 2017, 10:52 am ET
__ARTICLE
 PHILADELPHIA  Sen. John McCain lashed out at unnamed proponents of isolationist politics, saying that abandoning America's role as an international leader was "unpatriotic." 
 The six-term Arizona Republican made the remarks at the National Constitution Center in Philadelphia where he received the center's Liberty Medal for a lifetime of service and sacrifice to the country. 
 The former presidential hopeful mentioned his more than two decades of Navy service and his imprisonment in a Vietnamese prisoner of war camp, and recalled a time when politicians on both sides of the aisle worked together. 
 McCain, who in July revealed that he's fighting brain cancer, said: 
 "To abandon the ideals we have advanced around the globe, to refuse the obligations of international leadership for the sake of some half-baked, spurious nationalism cooked up by people who would rather find scapegoats than solve problems is as unpatriotic as an attachment to any other tired dogma of the past that Americans consigned to the ash heap of history." 
 He did not mention President Donald Trump by name, although the two have clashed in the past. 
 McCain later tweeted that particular section of his remarks, and provided a link to his full speech. 
Tweet Id not present
 None of the other speakers, who included former Vice President Joe Biden, mentioned any current or former government officials during their remarks. But many referred to a time when bipartisanship  namely, the friendship between McCain and the Democrat Biden  wasn't out of the ordinary. 
 "We often argued  sometimes passionately," McCain said of himself and the former vice president. "But we believed in each other's patriotism and the sincerity of each other's convictions. We believed in the institution we were privileged to serve in." 
 McCain joined the Navy in 1958 and rose to the rank of captain during his 22 years of service. In 1967, his plane was shot down over Hanoi during a bombing mission, and he spent five and a half years as a Vietnamese prisoner of war. 
 Known as a maverick in the Senate, McCain has gone against the will of the majority of his party in the past. He was one of three senators to vote against the GOPs attempt at repealing the Affordable Care Act in July, famously signaling his opposition with a thumbs down during the vote. 
 McCain himself tried several times to rise to the office that Trump now holds. He ran for the Republican nomination in 2000, losing to George W. Bush. Eight years later, he secured his partys nomination but lost the general election to Barack Obama. 
 The senator has disagreed with Trump several times since the presidential campaign, when the Republican candidate mocked McCains service, saying he was only a hero because he had been captured. 
 In February, McCain took a veiled swipe at Trumps attacks on the media, vehemently defending freedom of the press. Several months later, he called Trump's praise of North Korea's leader Kim Jong Un and controversial Philippine President Rodrigo Duterte "very disturbing." 
 "The statements, and the comments, obviously fly in the face of everything that Ive stood for and believed in all my life," McCain said on MSNBC's "Morning Joe" in May. 
 More recently, Trump slammed McCain for opposing efforts to repeal the Affordable Care Act, calling his no vote terrible and tweeting that the senator "let Arizona down." 
