__HTTP_STATUS_CODE
200
__TITLE
Technology Companies Face Growing Questions in Russia Probe
__AUTHORS
Alyssa Newcomb
Jo Ling Kent
__TIMESTAMP
Sep 28 2017, 5:35 am ET
__ARTICLE
 The flood gates have opened in a Congressional probe into how suspected Russian entities used social media to spread misinformation designed to influence America's presidential election. 
 On Thursday, Twitter, which has kept relatively quiet about suspected use of the platform for election meddling, will appear before Senate and House Intelligence Committee members. 
 The move comes as Facebook begins to hand over 3,000 Russian-linked ads, including payment data and details on how Russian entities targeted voters through Facebook's system. 
 A Facebook representative told NBC News on Wednesday the company is in the process of providing information to Special Counsel Robert Mueller's office. The House and Senate intelligence committees are also set to receive the advertisements as part of their ongoing probe into Russian interference in the election. 
 Facebook and Twitter representatives also confirmed to NBC News they received invitations to testify at an open hearing before Congress next month regarding the election probe. 
 It was unclear which executives might testify at the hearing. 
 "The question is, weve contacted the most senior officials (from Twitter, Facebook and Google), but well be in conversations about whos going to have the most relevant information," Sen. Mark Warner told reporters. 
 Related: Facebook Says It Will Hand Over Russia-Linked Ads to Congress 
 Earlier this month, Facebook revealed the results of an investigation, which found a suspected Russian operation spent $100,000 on issues-related advertisements from June 2015 to May 2017 with the intent to sway votes in the U.S. election. 
 On Wednesday, Facebook CEO Mark Zuckerberg responded to a tweet from President Donald Trump, which claimed "Facebook was always anti-Trump. The Networks were always anti-Trump." He also mentioned The New York Times and The Washington Post and asked, "Collusion?" 
 "Trump says Facebook is against him. Liberals say we helped Trump. Both sides are upset about ideas and content they don't like. That's what running a platform for all ideas looks like," Zuckerberg said. 
 Rep. Mike Conaway (R-TX) and Rep. Adam Schiff (D-CA), the bipartisan team leading the House Permanent Select Committee on Intelligence's investigation into Russian interference, issued a joint statement on Wednesday outlining the next big public step in the investigation - hearing from Facebook and other technology companies in an open hearing. 
 "In the coming month, we will hold an open hearing with representatives from tech companies in order to better understand how Russia used online tools and platforms to sow discord in and influence our election," they said. 
 It was not yet known whether Zuckerberg might testify at that open hearing. However, his lengthy response on Wednesday aimed to put Facebook's role in the election into perspective. 
 He said the facts suggest "the greatest role Facebook played in the 2016 election was different from what most are saying." 
 "Campaigns spent hundreds of millions advertising online to get their messages out even further. That's 1000x more than any problematic ads we've found," he said. 
 With this being the first election where the internet truly played a starring role, candidates were able to directly communicate with tens of millions of followers through their Facebook pages. In addition, Zuckerberg said there were billions of interactions on Facebook discussing the issues, including well beyond what was covered in the media. 
 "After the election, I made a comment that I thought the idea misinformation on Facebook changed the outcome of the election was a crazy idea. Calling that crazy was dismissive and I regret it," he said. 
 "This is too important an issue to be dismissive. But the data we have has always shown that our broader impact -- from giving people a voice to enabling candidates to communicate directly to helping millions of people vote -- played a far bigger role in this election." 
 It's not the first time Trump directly took aim at Facebook and the Russian advertisements. In a tweet earlier this month, he appeared to dismiss the scrutiny of ads on Facebook. 
The Russia hoax continues, now it's ads on Facebook. What about the totally biased and dishonest Media coverage in favor of Crooked Hillary?
Last week, Zuckerberg shared the company's "next steps" during a Facebook Live video stream from the Facebook's headquarters in Menlo Park, California, where he addressed measures the social media giant is taking to protect the integrity of future elections.
Many of Facebook's five million advertisers are able to set up their advertising campaigns without ever needing to work with a human on the other end.
The suspected Russian advertisements running in the United States were no different.
"Most ads are bought programmatically through our apps and website without the advertiser ever speaking to anyone at Facebook. That's what happened here," Zuckerberg said. "But even without our employees involved in the sales, we can do better."
Facebook has pledged to implement additional measures to ensure the integrity and transparency of ads, including disclosing who paid for them.
When people go to their page, they'll also be able to see all of the ads the person or group is running, Zuckerberg said.
They'll also work with candidates and local election officials to help ensure the integrity of elections and to stop the spread of misinformation.
These efforts were most recently seen with the German election. Facebook tested related articles to help provide different perspectives and using machine learning to reduce clickbait and spam stories and videos.
"These actions did not eliminate misinformation entirely in this election  but they did make it harder to spread, and less likely to appear in peoples News Feeds," Richard Allan, vice president of public policy, said in a blog post. "Studies concluded that the level of false news was low. We learned a lot, and will continue to apply those lessons in other forthcoming elections."
