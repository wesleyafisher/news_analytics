__HTTP_STATUS_CODE
200
__TITLE
What Will It Take to Get Virtual Reality Right?
__AUTHORS
Alyssa Newcomb
__TIMESTAMP
Oct 17 2017, 3:09 pm ET
__ARTICLE
 The world now knows Mark Zuckerberg isn't the kind of guy who thinks in millions  he wants billions. 
 But his latest ambition, getting one billion people using virtual reality, a technology that many believe is still in its infancy, isn't going to happen any time soon, according to experts. 
 Facebook's Oculus, Google, HTC, and others have spent years trying to hit on a formula that works  but mass adoption still isnt happening. 
 "The most important technologies don't start off mainstream. A lot of them seem maybe even too crazy or complex to start," Zuckerberg said at the annual Oculus Connect conference last week. 
 Related: Facebooks Oculus Is Having a Rough Start to 2017 
 But Zuckerberg is already starting to chip away at two of the pain points that experts say have hindered VR: price and portability. Last week, Zuckerberg announced the $199 Oculus Go, an untethered VR headset that doesn't require a phone or a computer. 
 The company's flagship product, the Oculus Rift, requires a headset to be tethered to a gaming computer, with the duo running around $1,400 on the low end, or thousands of dollars depending on the type of gaming computer. 
 Not only are they expensive, but the cord can be a hindrance, said Tuong Nguyen, principal research analyst at Gartner. 
 "I can only imagine what that would be like for my retired parents," Nguyen said. "Someone is going to break a hip for sure. Since you don't have the cord, that's one last hazard for consumers." 
 While it's an attractive price point, virtual reality has also had to contend with a reputation that it's too "anti-social." Even Apple CEO Tim Cook  one of the titans in the tech world  has previously discussed the isolating factor of VR. 
 "Some people say that VR is isolating and anti-social. I actually think it's the opposite," Zuckerberg said. "Saying VR is isolating because it's immersive is a very narrow view of the world you're all building." 
 Sure, Oculus wants to help create those immersive experiences Zuckerberg loves. But there may also be a business driver beyond just selling headsets. 
 The future of advertising could potentially be with cameras inside headsets that can understand what people are looking at, down to the pixel level, said Patrick Moorhead, principal analyst at Moor Insights & Strategy. 
 "Facebooks primary business model is advertising and virtual reality is the next growth area for it," said Moorhead. "Virtual reality is also another place to serve up advertising and I am sure we will see Facebook do that soon." 
 But first Facebook and their competitors will have to get more headsets strapped onto the heads of consumers. One of the leaders, Sony, revealed in June it had sold one million of its PlayStation VR headsets. 
 Facebook hasn't released its Oculus sales numbers, but the company's "Summer of Rift" sale helped it jump to a 48.87 percent market share in September, according to a Steam Hardware Survey, edging it even closer to HTC Vibe's 50.16 percent share. 
 Meanwhile, virtual reality's cool cousin, augmented reality, is having a bit of a moment right now. Augmented reality takes digital elements and puts them in your physical world. 
 Decades after being invented, AR finally hit the mainstream this year with the release of thousands of new AR apps in Apple's iOS 11 software update. 
 Those apps range from useful to fun, letting users do everything from seeing how virtual furniture looks in their living room to placing fun GIFs in their real world with GIPHY. 
 The other big mobile player in the United States, Google, is also betting on AR for Android users with its ARCore developer's kit. When the company announced its plan in August, they also made a big wager  that they hope to have 100 million devices using it by the end of the trial. 
