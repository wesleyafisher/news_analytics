__HTTP_STATUS_CODE
200
__TITLE
Florida Gov. Declares States Opioid Epidemic Public Health Emergency
__AUTHORS
Corky Siemaszko
__TIMESTAMP
May 4 2017, 10:49 pm ET
__ARTICLE
 The governor of Florida has officially declared the opioid epidemic a public health emergency  some four years after it began cutting a deadly swath through the Sunshine State. 
 Gov. Rick Scotts declaration on Wednesday allows the state to tap more than $54 million in U.S. Department of Health and Human Services grant money to pay for prevention, treatment and recovery services. 
 Scott also gave the green light to state Surgeon General Celeste Philip to start distributing the anti-overdose treatment Naloxone to first responders. 
 "I know firsthand how heartbreaking substance abuse can be to a family because it impacted my own family growing up," Scott said in a statement. "The individuals struggling with drug use are sons, daughters, mothers, fathers, sisters, brothers and friends and each tragic case leaves loved ones searching for answers and praying for help." 
 In his press release, the Republican governor said he was making his announcement "following the Centers for Disease Control and Prevention (CDC) declaring a national opioid epidemic." But the CDC has been calling this plague an epidemic since February 2011. 
 Scott spokeswoman Kerry Wyland said in an email to NBC News the federal funds became available to Florida on April 21 and that the opioid problem has been on the governor's radar for some time. 
 "The order was issued now because we have traveled the state and received feedback and community input," Wyland wrote. "Additionally, the order is needed now so Florida can immediately draw down the federal grant funding." 
 Related: Deaths From Infections May Be Masking Opioid Deaths 
 Mark Fontaine, executive director of the non-profit Florida Alcohol and Drug Abuse Association, said Scott really began focusing on the problem this year after going on a listening tour "where he heard first-hand the pain of parents who lost children, the pain of first responders who watched people die of overdoses." 
 "I think it became absolutely clear we have an epidemic on our hands," said Fontaine, who added that he applauds the governor's move. "This is something we have been dealing with for at least four years. Entire communities have been asking the governor for help." 
 Scott had been under increasing pressure from lawmakers to do something about the epidemic. 
 "I'm a little perplexed as to why we have thousands of deaths in the state of Florida and we're not creating a state of emergency but we have some wildfires that have caused zero deaths and yet that's a bigger issue," state Sen. Jeff Clemens, a Democrat,said last month according to the Florida Sun Sentinel. 
 Scott allies like state Senator Jack Latvala, a Republican, said "the important thing is he's done it, and not what day or how many days did it take him to do," the Tampa Bay Times newspaper reported. 
 Florida has been hit especially hard by the deadly opioid overdose epidemic. 
 In 2015, heroin, fentanyl and oxycodone were directly responsible for the deaths of 3,896 Floridians, according to the most recent Florida Department of Law Enforcement statistics. 
 That's about 12 percent of all the 33,000 people nationwide who died that year of opioid overdoses. 
 Last year in South Florida, the morgues in Palm Beach County were strained to capacity by 525 fatal opioid overdoses, the Sun Sentinel newspaper reported in March. 
 The deadly cocktail of heroin mixed with fentanyl or carfentanil figured in 220 deaths in Miami-Dade County last year, the paper reported. And 90 percent of the fatal drug overdoses in Broward County involved heroin, fentanyl or other opioids. 
