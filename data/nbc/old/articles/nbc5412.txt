__HTTP_STATUS_CODE
200
__TITLE
FEMA Chief Brock Long Says Puerto Rico Relief Most Logistically Challenging Event U.S. Has Ever Seen
__AUTHORS
Daniella Silva
__TIMESTAMP
Oct 1 2017, 9:11 pm ET
__ARTICLE
 Federal Emergency Management Agency Administrator Brock Long said Sunday that hurricane relief efforts in Puerto Rico are the "most logistically challenging event that the United States has ever seen" and defended the federal response. 
 "We're not going to be satisfied until the situation is stabilized and the bottom line is that this is the most logistically challenging event that the United States has ever seen," Long said on "Fox News Sunday. "And we have been moving and pushing as fast as the situation allows." 
 "Every day we make progress. Every day we have setbacks," he added. "For example, you don't just bring the commodities in  you have to be able to pump them down the roadway systems that we have been working desperately to get open." 
 Long also expressed frustration over remarks from San Juan Mayor Carmen Yuln Cruz and others criticizing the federal response. 
 "You know, we can choose to look at what the mayor spouts off or what other people spout off, but we can also choose to see what's actually being done, and that's what I would ask," he said. 
 Cruz issued an emotional plea for more federal assistance on Friday, saying, "We are dying." 
 Related: FEMA Says Progress Being Made, but Supplies Short in Puerto Rico 
 "We are dying, and you are killing us with the inefficiency and the bureaucracy," Cruz said. 
 Criticism has been mounting over the Trump administration's response to what is being called an unfolding humanitarian crisis. 
 President Donald Trump struck back at the mayor on Saturday, tweeting that she showed "poor leadership ability" and criticized other local politicians. 
 "They want everything to be done for them when it should be a community effort," he said. 
 Trump's comments drew rebukes from many, including "Hamilton" creator Lin-Manuel Miranda, whose family is from Puerto Rico. 
 "Did you tweet this one from the first hole, 18th hole, or the club? Anyway, it's a lie. You're a congenital liar," Miranda tweeted about the president. 
 Overall, FEMA said on its website Sunday that "millions of meals and millions of liters of water" had been provided to Puerto Rico and the U.S. Virgin Islands. 
 The agency and its partners have distributed close to 2 million liters of water and 1 million meals throughout Puerto Rico, Alejandro De La Campa, FEMA's federal coordinating officer, said during a teleconference Saturday. 
 On Sunday, De La Campa said that while supplies had been distributed to all of Puerto Rico's 78 municipalities, a series of issues from downed trees on roads to no communication made reached "isolated areas" very difficult. 
 "We have reached the 78 municipalities, at the same time we recognize that we have not been able or the mayors themselves have not been able to reach some of the isolated areas," he said. 
 Lt. Gen. Jeffrey Buchanan, a three-star general serving as the Department of Defense's primary liaison to FEMA, said they were dependent on aircraft relief efforts to deliver supplies to some of the hardest to reach areas. 
 Officials also said they hoped to expand the number of regional distribution centers from 11 to 25 or 30 throughout the island. 
 Long defended FEMA's efforts in the wake of the damage caused by both Hurricane Maria a week ago and Irma before that. The agency has helped reopen 11 major highways and 700 out of about 1,100 gas stations in Puerto Rico, he said. 
 He added that compounding the woes of Puerto Rico and the U.S. Virgin Islands was that "in some cases the infrastructure was incredibly fragile." 
 Related: For Some, Delayed Response to Puerto Rico Has Echoes of Katrina 
 "Both of these territories were hit by two major hurricanes, two, not just one so a lot of the infrastructure was damaged by Irma and then Maria comes in and finishes it off completely," he said. 
 "I think we have to filter out the noise and we have to continue to push forward," he said. 
 But Long acknowledged there was long-term work ahead, with power only restored to five percent of the island and communication restored to only a third of the island's population. 
 "It's going to be multiple, multiple months before power is restored to many of these areas and that's just the reality," he said. 
