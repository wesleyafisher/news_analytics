__HTTP_STATUS_CODE
200
__TITLE
Theyre Out of Work and Taking Prescription Opioid Painkillers
__AUTHORS
Corky Siemaszko
__TIMESTAMP
Sep 9 2017, 11:08 am ET
__ARTICLE
 Legions of young American men who have dropped out of the labor force have this much in common  theyre on prescription painkillers. 
 So says a Princeton University economist who warns the opioid crisis is crippling communities across the country and poses long-term dangers to the U.S. economy. 
 The opioid crisis and depressed labor-force participation are now intertwined in many parts of the U.S., Alan Krueger wrote in a study that was released this week by the Brookings Institution. 
 Nearly half of these men take pain medication of a daily basis, and in nearly two-thirds of these cases they take prescription pain medication, Krueger wrote. Labor force participation has fallen more in areas where relatively more opioid pain medication is prescribed. 
 Asked whether painkillers are the reason they arent working  or whether these men turn to painkillers as a result of prolonged unemployment  Krueger had an answer at the ready. 
 It is very likely some of both, Krueger wrote Friday in an email to NBC News. 
 Krueger, who was the chief economist in the Treasury Department during the Obama Administration, reached his conclusions after doing a survey of 571 men, ages 25 to 54, who were not working. 
 The results of this survey underscore the role of pain in the lives on nonworking men, and the widespread use of prescription pain medication, he wrote. Fully 47 percent of NLF (not in labor force) prime age men responded that they took pain medication on the previous day. 
 And nearly two-thirds of those said they were taking prescription meds, he wrote. 
 Thus, on any given day, 31 percent of NLF prime age men take pain medication, most likely an opioid-based medication, he wrote. And these figures likely understate the actual proportion of men taking prescription pain medication given the stigma and legal risk associated with reporting taking narcotics. 
 The labor force participation rate, meaning people who are either working or actively seeking work, peaked in the U.S. at 67.3 percent in early 2000, according to the Bureau of Labor Statistics. 
 It reached a 40-year low of 62.4 percent in September 2015, even as the U.S. economy under President Barack Obama was rapidly recovering from a recession that lasted from the end of 2007 till mid-2009. 
 Obamas critics often pointed to the labor participation rate as proof that the recovery wasnt as strong as the statistics would suggest. 
 The White House and most economists countered the rate had been in decline for years, largely as a result of Baby Boomers retiring, the disappearance of low-skill jobs, and demographic shifts like working-age people opting for school instead of plunging directly into the job market. 
 Still, as Krueger reported, the decline in the participation rate was faster in the last decade than in the preceding one. 
 The share of non-college educated young men who did not work at all over the entire year rose from 10 percent in 1994 to more than 20 percent in 2015, he wrote. 
 That coincides roughly with the start of an opioid epidemic during which the number of unintentional overdoses from prescription pain relievers quadrupled since 1999, according to the National Institute on Drug Abuse. 
 Kruegers conclusions preceded a stunning NIDA survey which found that one in three Americans  a whopping 91.8 million people  used opioid-based painkillers like OyxContin and Vicodin in 2015. 
 President Donald Trump said he would declare a national emergency on Aug. 10 and vowed to fight the opioid crisis. 
 But so far, the president has not formally signed such a declaration and sent it to Congress, NBC News confirmed Friday. 
 That means the White House cannot turn on the money spigot and direct millions of dollars toward expanding treatment facilities or supplying police with the anti-overdose remedy naloxone. 
 The Trump administration has not yet said when Trump would get around to it. When asked by NBC two weeks ago about the delay, a spokesperson said it was undergoing an expedited legal review. 
