__HTTP_STATUS_CODE
200
__TITLE
Meet the Press Transcripts
__AUTHORS
NBC News
__TIMESTAMP
Jul 9 2017, 12:22 pm ET
__ARTICLE
 October 15: U.S. Ambassador to the United Nations Ambassador Nikki Haley, Gov. John Kasich (R-OH), Kimberly Strassel, Heather C. McGhee, Kasie Hunt, Dan Balz | Transcript 
 October 8: Director Mick Mulvaney, Rep. Steve Scalise (R-LA), Sen. Dianne Feinstein, Hugh Hewitt, Kristen Welker, Eugene Robinson, Carol Lee | Transcript  
 October 1: Ron Mott, Sec. Steve Mnuchin, Sen. Bob Corker (R-TN), Ta-Nehisi Coates, Joy-Ann Reid, David Brooks, Danielle Pletka, Charlie Sykes | Transcript  
 September 24: Sen. Rand Paul (R-KY), Marc Short, Rep. Nancy Pelosi (D-CA), Stephanie Cutter, Eliana Johnson, Stephen Henderson, Rich Lowry, Mike Tirico | Transcript  
 September 17: Sen. Tom Cotton (R-AR), Sen. Bernie Sanders (I-VT), Fmr. Mayor Michael Bloomberg, Doris Kearns Goodwin, Katy Tur, Al Cardenas, David Brody | Transcript 
 September 3: Andrea Mitchell, Ron Allen, Mayor Sylvester Turner of Houston, Sen. Roy Blunt (R-MO), Eddie Glaude Jr., Kristen Welker, Susan Page, Matthew Continetti | Transcript 
 August 27: Lester Holt, FEMA Administrator Brock Long, Gov. John Kasich (R-OH), Sen. Sherrod Brown (D-OH), Danielle Pletka, Yamiche Alcindor, Katy Tur, Michael Gerson | Transcript  
 August 20: Fmr. Rep. J.C. Watts (R-TN), Fmr. Amb. Andrew Young, Mark Bray, Richard Cohen, Julius Krein, Peggy Noonan, Eugene Robinson, Steve Hayes, Fmr. Rep. Donna Edwards | Transcript 
 August 13: Lt. Gen. H.R. McMaster, Tom Costello, Adm. Mike Mullen (Ret.), Mayor Michael Signer of Charlottesville, Joy Reid, Helene Cooper, Rich Lowry, Amy Walter | Transcript  
 August 6: Sen. Jeff Flake (R-AZ), Gov. Jerry Brown (D-CA), Andrea Mitchell, David French, Dan Balz, Heather McGhee | Transcript  
 July 30: Sec. Tom Price, Sen. Susan Collins (R-ME), Corey Lewandowski, Eliana Johnson, Cornell Belcher, Helene Cooper, Hugh Hewitt | Transcript 
 July 16: Jay Sekulow, Sen. Mark Warner (D-VA), Sen. John Cornyn (R-TX), Danielle Pletka, Doris Kearns Goodwin, Al Cardenas, Tom Brokaw | Transcript 
 July 9: Fmr. CIA Dir. John Brennan, Sen. Lindsey Graham (R-SC), Tom Perez, Ronna McDaniel, Kristen Welker, Robert Costa, Ruth Marcus, Rich Lowry | Transcript  
 July 2: Sec. Tom Price, Sen. Bill Cassidy (R-LA), Sen. Tom Carper (D-DE), Malcolm Gladwell, Kasie Hunt, Katty Kay, Eugene Robinson, Hugh Hewitt | Transcript 
 June 25: Sen. Ron Johnson (R-WI), Sen. Bernie Sanders (I-VT), Rep. Debbie Dingell (D-MI), Rep. Tim Ryan (D-OH), Helene Cooper, Hallie Jackson, Mark Leibovich, George Will | Transcript  
 June 18: Jay Sekulow, Sen. Marco Rubio (R-FL), Sen. Angus King (I-ME), David Brooks, Eugene Robinson, Danielle Pletka, Amy Walter | Transcript  
 June 4: Fmr. Sec. John Kerry, EPA Administrator Scott Pruitt, Fmr. Vice President Al Gore, Stephanie Cutter, Michael Gerson, Heather McGhee, Hugh Hewitt | Transcript  
 May 28: Sec. John Kelly, Sen. Bob Corker (R-TN), Fmr. DNI James Clapper, Amy Walter, Kimberley Strassel, Joy-Ann Reid, Charlie Sykes | Transcript 
 May 14: Sec. Rex Tillerson, Sen. Lindsey Graham (R-SC), Sen. Chuck Schumer (D-NY), Hallie Jackson, Katty Kay, Matthew Continetti, Eugene Robinson | Transcript 
 May 7: Sec. Tom Price, Sen. Roy Blunt (R-MO), Sen. Dianne Feinstein (D-CA), Eliana Johnson, Matt Bai, Rich Lowry, Kristen Welker | Transcript 
 April 30: Vice President Mike Pence, Sen. Susan Collins (R-ME), Sen. Angus King (I-ME), Danielle Pletka, Helene Cooper, Nicolle Wallace, Chris Matthews | Transcript 
 April 23: White House Chief of Staff Reince Priebus, Rep. Nancy Pelosi (D-CA), Sen. Marco Rubio (R-FL), Cornell Belcher, Peggy Noonan, Savannah Guthrie, Robert Costa | Transcript 
 April 16: Sen. John McCain (R-AZ), Sen. Jack Reed (D-RI), Sec. John Kelly, John Sununu, Andrea Mitchell, Heather McGhee, Mark Leibovich, Bishop TD Jakes, Pastor JoAnn Hummel, Rabbi David Saperstein | Transcript 
 April 9: Amb. Nikki Haley, Sen. Lindsey Graham (R-SC), Sen. Tim Kaine (D-VA), Sen. Bernie Sanders (I-VT), David Brooks, Danielle Pletka, Rich Lowry, Helene Cooper | Transcript 
 April 2: Sen. Mitch McConnell (R-KY), Sen. Chuck Schumer (D-NY), Clint Watts, Robert Draper, Amy Walter, Greta Van Susteren, Eugene Robinson | Transcript 
 March 26: OMB Dir.Mick Mulvaney, Sen. Mike Lee (R-UT), Rep. Charlie Dent (R-PA), Sen. Mark Warner (D-VA), Gov. Jerry Brown, Tom Brokaw, Eliana Johnson, Hugh Hewitt, Joy-Ann Reid | Transcript  
 March 19: OMB Dir. Mick Mulvaney, Sen. Susan Collins (R-ME), Rep. Adam Schiff (D-CA), George F. Will, Katty Kay, Yamiche Alcindor, Robert Costa | Transcript 
 March 12:  Kathleen Sebelius, Gov. John Kasich (R), Sec. Tom Price, Helene Cooper, Rick Santelli, David Brooks, Stephanie Cutter | Transcript 
 March 5: Sen. Marco Rubio (R-FL), Sen. Chuck Schumer (D-NY), Fmr. DNI James Clapper, Danielle Pletka, Thomas Friedman, Cornell Belcher, Kimberley Strassel | Transcript 
 February 26: Sen. Tom Cotton (R-AR), Tom Perez, Gov. John Hickenlooper (D), Ramesh Ponnuru, Helene Cooper, Jerry Seib, Eliana Johnson | Transcript 
 February 19: White House Chief of Staff Reince Priebus, Fmr. CIA Dir. Leon Panetta, Sen. John McCain (R-AZ), David Brooks, Fmr. Rep. Donna Edwards, Hugh Hewitt, Amy Walter | Transcript  
 February 12: Stephen Miller, Sen. Bernie Sanders (I-VT), Fmr. Sen. Jim Webb (D-VA), Greta Van Susteren, Fmr. Gov. Pat McCrory, Eugene Robinson, Katty Kay | Transcript 
 February 5: Vice President Mike Pence, House Speaker Paul Ryan (R-WI), Rep. Nancy Pelosi (D-CA), Andrea Mitchell, Alex Castellanos, Danielle Pletka, Tavis Smiley | Transcript  
 January 29: Sen. Tim Kaine (D-VA), White House Chief of Staff Reince Priebus, Doris Kearns Goodwin, Michael Steele, Kimberley Strassel, Tom Friedman, Tom Brokaw |Transcript 
 January 22: Kellyanne Conway, Sen. Chuck Schumer (D-NY), Tom Barrack, Hugh Hewitt, Eliana Johnson, Chris Matthews, Kristen Welker | Transcript 
 January 15: Rep. John Lewis (D-GA), Reince Priebus, Sen. Dianne Feinstein (D-CA), Rich Lowry, Helene Cooper, Jeffrey Goldberg, Danielle Pletka | Transcript  
 January 8: Sen. John McCain (R-AZ), Sen. Lindsey Graham (R-SC), Sec. Ash Carter, Kellyanne Conway, David Brooks, Andrea Mitchell, Rick Santelli, Donna Edwards | Transcript 
 January 1: Nicolle Wallace, Joe Lockhart, Ari Fleischer, David Folkenflik, Hal Boedeker, Gabe Sherman, Claire Atkinson, Dean Baquet, Gerard Baker | Transcript 
 December 25: Katy Tur, Hugh Hewitt, Joy-Ann Reid, Robert Costa | Transcript  
 December 18: John Podesta, Fmr. Sec. Robert Gates, Katty Kay, Yamiche Alcindor, Rick Santelli, Jeff Greenfield | Transcript 
 December 11: Reince Priebus, Fmr. Amb. Mike McFaul, Rep. Adam Schiff (D-CA), Mike Rowe, Michael Steele, Doris Kearns Goodwin, Kimberley Strassel, Rick Stengel |Transcript 
 December 4: Vice President-elect Mike Pence, Kellyanne Conway, Joel Benenson, Andrea Mitchell, Rich Lowry, Amy Walter, Heather McGhee | Transcript 
 November 27: Kellyanne Conway, Sen. Marco Rubio, Rep. Tim Ryan, Helene Cooper, Matt Bai, Danielle Pletka, Mark Murray | Transcript 
 November 20: Reince Priebus, Sen. Chuck Schumer, Sen. Bernie Sanders, Robert Costa, Kathleen Parker, Thomas Friedman, Neera Tanden | Transcript 
 November 13: Kellyanne Conway, Sen. Cory Booker, Rep. Keith Ellison, David Brooks, Hugh Hewitt, Nina Turner, Katty Kay | Transcript 
 November 6: John Podesta, Newt Gingrich, Bill McInturff, Fred Yang, Tom Brokaw, Savannah Guthrie, Chris Matthews, Jose Diaz-Balart, Nicolle Wallace | Transcript 
 October 30: Gov. Mike Pence (R-IN), Robby Mook, Evan McMullin, Andrea Mitchell, Audie Cornish, Mike Murphy, Larry Kudlow | Transcript 
 October 23: Sen. Tim Kaine (D-VA), Kellyanne Conway, Tom Friedman, Stuart Stevens, Yamiche Alcindor, Eliana Johnson | Transcript 
 October 16: Vice President Joe Biden, Gov. Mike Pence (R-IN), Chris Cillizza, Hugh Hewitt, Joy-Ann Reid, Kristen Welker | Transcript 
 October 9: Rep. Nancy Pelosi (D-CA), Rudy Giuliani, Sen. Mike Lee (R-UT), Sara Fagen, Ruth Marcus, Heather McGhee, Steve Schmidt | Transcript 
 October 2: Rudy Giuliani, Robby Mook, Michael Moore, Glenn Beck, Mark Halperin, Maria Teresa Kumar, Rich Lowry, Amy Walter | Transcript 
 September 25: John Podesta, Fmr. Lt. Gen. Mike Flynn, Steve Schmidt, Stephanie Cutter, Doris Kearns Goodwin, Hugh Hewitt, Gwen Ifill, Mike Murphy | Transcript 
 September 18: Richard Engel, Pete Williams, Sen. Tim Kaine (D-VA), Kellyanne Conway, Cornell Belcher, Alex Castellanos, Maureen Dowd, Katy Tur | Transcript 
 September 11: Sec.Jeh Johnson, Paul Wolfowitz, Tom Brokaw, David Brooks, Audie Cornish, Stephanie Cutter | Transcript 
 September 4: Gov.Mike Pence (R-IN), Sen. Bernie Sanders (I-VT), Alex Castellanos, Chris Cillizza, Maria Teresa Kumar, Kristen Welker | Transcript 
 August 28: Reince Priebus, David Plouffe, Robert Costa, Hugh Hewitt, Andrea Mitchell, Joy-Ann Reid | Transcript 
 August 7: Sen. Tim Kaine (D-VA), Fmr. Lt. Gen. Michael Flynn, Gov. Rick Scott, Yamiche Alcindor, Mark Halperin, Hugh Hewitt, Joy-Ann Reid | Transcript 
 July 31: Paul Manafort, Robby Mook, Khizr Khan, Julian Assange, David Brooks, Alex Castellanos, Doris Kearns Goodwin, Hallie Jackson | Transcript  
 July 24: Donald Trump, Sen. Bernie Sanders (I-VT), Rachel Maddow, Chris Matthews, Andrea Mitchell, Michael Steele | Transcript 
 July 17: . Sec. John Kerry, Reince Priebus, Glenn Beck, Tom Brokaw, Sara Fagen, Hugh Hewitt, Joy-Ann Reid | Transcript 
 July 10: Sec. Jeh Johnson, Bill Bratton, Charles Ramsey, Sen. Cory Booker (D-NJ), Sen. Bob Corker (R-TN), Michael Eric Dyson, Michael Gerson, Mary Matalin, Eugene Robinson | Transcript 
 July 3: Sen. Tom Cotton (R-AR), Tom Perez, Jonathan Rauch, Andrea Mitchell, Kelly ODonnell, Katy Tur, Kasie Hunt | Transcript 
 June 26: David Miliband, Paul Manafort, Sen. Tim Kaine (D-VA), Chris Cillizza, Helene Cooper, Doris Kearns Goodwin, Kimberley Strassel | Transcript 
 June 19: Donald Trump, Hallie Jackson, Speaker Paul Ryan, Loretta Lynch, Jose Diaz-Balart, Mark Halperin, Gwen Ifill, Katy Tur | Transcript 
 June 12: Gabe Gutierrez, Pete Williams, Sen. Jeff Flake (R-AZ), Sen. Bernie Sanders (I-VT), Steve Schmidt, David Plouffe, Tom Brokaw, Hugh Hewitt, Joy-Ann Reid, Amy Walter | Transcript  
 June 5: Bob Costas, Bryant Gumbel, Jim Brown, Mitch McConnell, Gary Johnson, Lanhee Chen, Donna Edwards, Ron Fournier, Andrea Mitchell | Transcript 
 May 29: Sen. Bernie Sanders (I-VT), Arnold Schwarzenegger, Kellyanne Conway, Robert Costa, Jerry Seib, Neera Tanden | Transcript 
 May 22: Fmr.Sec. Hillary Clinton, Mark Cuban, Alex Castellanos, Helene Cooper, Robert Draper, Joy-Ann Reid | Transcript 
 May 8: Donald Trump, Sen. Jeff Flake (R-AZ), Matt Bai, Kellyanne Conway, Andrea Mitchell, Eugene Robinson | Transcript 
 May 1: Sen. Ted Cruz (R-TX), Dir. John Brennan, Ron Fournier, Tom Friedman, Doris Kearns Goodwin, Kristen Welker | Transcript 
 April 24: Michael Steele, Katie Packer, Bernie Sanders, Bob Graham, Robert Costa, Jose Diaz-Balart, Joy-Ann Reid, Nicolle Wallace | Transcript 
 April 17: George Clooney, Rep. Debbie Wasserman Schultz (D-FL), Reince Priebus, Pat McCrory, Perry Bacon, Hugh Hewitt, Chris Matthews, Kathleen Parker | Transcript 
 April 10: Paul Manafort, Sen. Bernie Sanders (I-VT), Mayor Bill de Blasio (D-NY), Glenn Beck, Molly Ball, Matt Bai, Rich Lowry, Joy-Ann Reid | Transcript 
 April 3: Fmr. Sec. Hillary Clinton, Reince Priebus, Ron Johnson, Charles Benson, David Brooks, Helene Cooper, Amy Walter | Transcript 
 March 27: Sen. Bernie Sanders (I-VT), Gov. John Kasich (R-OH), Michael Leiter, Richard Engel, Keir Simmons, Ben Ginsberg, Andrea Mitchell, Hallie Jackson, Katy Tur, Kristen Welker | Transcript 
 March 20: Steve Schmidt, Stuart Stevens, John Kasich, Harry Reid, Mitch McConnell, Jose Diaz-Balart, Joy-Ann Reid, Robert Costa, Molly Ball | Transcript  
 March 13: Donald Trump, Sen. Ted Cruz (R-TX), Gov. John Kasich (R-OH), Alex Castellanos, Anne Gearan, Doris Kearns Goodwin, Hugh Hewitt | Transcript 
 March 6: Gov. Mitt Romney, Lindsey Graham, Kevin Spacey, David Brooks, Stephen Henderson, Mary Matalin, Kelly O'Donnell | Transcript 
 February 28: Donald Trump, Sen. Ted Cruz (R-TX), Sen. Bernie Sanders (I-VT), Gov. John Kasich (R-OH), Tulsi Gabbard, Charlie Cook, Erick Erickson, Heather McGhee, Andrea Mitchell | Transcript 
 February 21: Donald Trump, Sen. Ted Cruz (R-TX), Sen. Marco Rubio (R-FL), Sen. Bernie Sanders (I-VT), Hugh Hewitt, Joy-Ann Reid, Jon Ralston, Amy Walter | Transcript 
 February 14: Donald Trump, Sen. Ted Cruz (R-TX), Sen. Marco Rubio (R-FL), Gov. John Kasich (R-OH), Rev. Al Sharpton, Pete Williams, Chris Cillizza, Ron Fournier, Gwen Ifill, Kathleen Parker | Transcript 
 February 7: Fmr. Sec. Hillary Clinton, Sen. Bernie Sanders (I-VT), Donald Trump, Gov. Jeb Bush, Hugh Hewitt, Hallie Jackson, Chris Matthews, Andrea Mitchell | Transcript 
 January 31: Sen. Ted Cruz (R-TX), Sen. Marco Rubio (R-FL), Sen. Bernie Sanders (I-VT), Sen. Rand Paul (R-KY), David Brody, Tom Brokaw, Jennifer Jacobs, Joy-Ann Reid | Transcript 
 January 24: Fmr. Sec. Hillary Clinton, Sen. Bernie Sanders, Donald Trump, Fmr. Sec. Bob Gates, David Brooks, Chris Cillizza, Kasie Hunt, Kristen Welker | Transcript 
 January 17: Sec. Hillary Clinton, Sen. Bernie Sanders, Sen. Marco Rubio, Sen. Jeb Bush, Amal Clooney, Richard Engel, Stephanie Cutter, Hugh Hewitt, Joy-Ann Reid, Steve Schmidt | Transcript 
 January 10: Donald Trump, Denis McDonough, Alex Castellanos, Ben Ginsberg, Helene Cooper, Robert Costa, Jeff Greenfield, Jennifer Jacobs | Transcript 
 January 3: Gov. John Kasich (R-OH), Sen. Rand Paul (R-KY), Hallie Jackson, Kristen Welker, Capt. Mark Kelly, Lt. Gov. Dan Patrick, Chris Matthews, Jennifer Rubin, Sara Fagen, Eugene Robinson, Richard Dorment | Transcript 
 December 27: Sen. Bernie Sanders, Rep. Debbie Wasserman Schultz, Mike McFaul, Wendy Sherman, Spike Lee, Matt Bai, Helene Cooper, Michael Gerson, Amy Walter | Transcript 
 December 20: Donald Trump, Sen. Bernie Sanders, John Podesta, Speaker Paul Ryan, Jose Diaz-Balart, Doris Kearns Goodwin, Maggie Haberman, Hugh Hewitt | Transcript 
 December 13: Sen. Marco Rubio, Sec. John Kerry, Molly Ball, Helene Cooper, Ted Koppel, Jerry Seib | Transcript 
 December 6: Attorney General Loretta Lynch, Pete Williams, Michael Leiter, Sen. Rand Paul, Sen. Lindsey Graham, Dalia Mogahed, Asra Nomani, Elisabeth Bumiller, Rich Lowry, Charles Ogletree, Amy Walter | Transcript 
 November 29: Donald Trump, Ben Carson, Fmr. Sec. Robert Gates, Joe McQuaid, Molly Ball, Hugh Hewitt, Andrea Mitchell, Eugene Robinson | Transcript 
 November 22: Sec. Jeh Johnson, Bill Bratton, Richard Engel, Arsalan Iftikhar, Leon Panetta, Gov. John Kasich, Tom Brokaw, Kathleen Parker, Helene Cooper, Ron Fournier | Transcript 
 November 15: Fmr. Gov. Jeb Bush, Richard Engel, Christiane Taubira, Ben Rhodes, Rep. Michael McCaul, Michael Leiter, Jeff Greenfield, Andrea Mitchell, Eugene Robinson, Jennifer Rubin | Transcript 
 November 8: Ben Carson, Donald Trump, Sen. Bernie Sanders, Carly Fiorina, Sen. Dianne Feinstein, Marc Caputo, Hugh Hewitt, Gwen Ifill, Rachel Maddow | Transcript 
 November 1: Fmr. Gov. Jeb Bush, Rep. Paul Ryan , Matt Bai, David Brooks, Helene Cooper, Anne Gearan | Transcript 
 October 25: Dr. Ben Carson, Rep. Trey Gowdy, Rep. Elijah Cummings, Doris Kearns Goodwin, John Harwood, Stephen Henderson, Jennifer Rubin | Transcript 
 October 18: Sen. Ted Cruz, Rep. Mike Pompeo, Rep. Adam Schiff, Alex Castellanos, Ron Fournier, Andrea Mitchell, Amy Walter | Transcript 
 October 11: Rep. Dave Brat, Rep. Charlie Dent, Sen. Bernie Sanders, Eugene Robinson, Kathleen Parker, Hugh Hewitt, Nathan Gonzales  | Transcript 
 October 4: Donald Trump, Miguel Almaguer, Stephen Hadley, Michael McFaul, Mark Leibovich, Rich Lowry, Amy Holmes, Ruth Marcus| Transcript 
 September 27: Hillary Clinton, Carly Fiorina, George Weigel, Andrea Mitchell, David Brooks, Andrew Ross Sorkin, Eugene Robinson | Transcript 
 September 20: Donald Trump, Ben Carson, John Kasich, Jamie Dimon, Hugh Hewitt, David Maraniss, Maria Shriver, Molly Ball | Transcript 
 September 13:Chris Christie, Bernie Sanders, Tom Cole, Jim DeMint, Amb. Robert Ford, Maria Hinojosa, Sara Fagen, Ron Fournier, David Brooks | Transcript 
 September 6: Colin Powell, Richard Trumka, Tom Brokaw, Doris Kearns Goodwin, Joy-Ann Reid, Hugh Hewitt, David Miliband | Transcript 
 August 30: Scott Walker, Amb. Brett McGurk, Malcolm Gladwell, Matt Bai, Melissa Harris-Perry, Helene Cooper, Steve Schmidt | Transcript 
 August 23: Alex Castellanos, Charlie Black, Howard Dean, Peter Hart, Jerry Brown, Carly Fiorina, Alfonso Aguilar, Susan Page, Amy Walter, Jon Ralston| Transcript 
  August 16: Donald Trump, Bernie Sanders, Eugene Robinson, Molly Ball, Kimberley Strassel, Jeff Greenfield | Transcript 
 August 9: Donald Trump, Marco Rubio, Gov. John Kasich, Claire McCaskill, Andrea Mitchell, Heather McGhee, Hugh Hewitt, David Brooks | Transcript 
 August 2: Donald Trump, Reince Priebus, Debbie Wasserman Schultz, Ben Carson, Charles Ramsey, Jerry Seib, Kathleen Parker, Helene Cooper, Chris Matthews | Transcript 
 July 26: Sen. Bernie Sanders, Gov. John Kasich, Pat Buchanan, John Nichols, Jose Diaz-Balart, Sara Fagen, Amy Walter, Ron Fournier | Transcript 
 July 19: Gov. Bill Haslam, Michael Leiter, David Cameron, John Kerry, Tom Cotton, Rick Perry, Joaquin Castro, Raul Labrador, Thomas Friedman, Andrea Mitchell, Danielle Pletka, Bill Richardson | Transcript 
 July 12: Gov. Nikki Haley, Brett Williams, Doris Kearns Goodwin, Arthur Brooks, Maria Hinojosa, Matt Bai | Transcript 
 July 5: Ted Cruz, Chris Cillizza, Kathleen Parker, Carolyn Ryan, Michael Steele | Transcript 
 June 28: Sen. Lindsey Graham, Bobby Jindal, Michael Eric Dyson, Kathleen Parker, Charles Ogletree, Newt Gingrich | Transcript 
 June 21: Alana & Daniel Simmons, James Clyburn, Mike Huckabee, Eugene Robinson, Helene Cooper, Gerald Seib, David Brooks | Transcript 
 June 14: John Podesta, Mitt Romney, Brett McGurk, Bill Daley, Hugh Hewitt, Stephanie Cutter, Andrea Mitchell, Evan Thomas | Transcript 
 May 31: Tom Davis, Sen. Bernie Sanders, Rick Santorum, Gov. John Kasich, Nuala O'Connor, Sara Fagen, Chris Matthews, Manu Raju, Amy Walter | Transcript 
 May 17: Sen. Cory Booker, Sen. Rand Paul, Tom Friedman, Sara Fagen, Helene Cooper, David Axelrod | Transcript 
 May 10: Sen. Dianne Feinstein, Michael Leiter, Carly Fiorina, Erna Solberg, Maria Shriver, Cathy Engelbert, Kishanna Brown, Jon Ralston, Kathleen Parker, Matt Bai, Michael Steele, Ruth Marcus | Transcript 
 May 3: Stephanie Rawlings-Blake, Martin O'Malley, John Boehner, Wes Moore, Tom Brokaw, Kimberley Strassel, April Ryan | Transcript 
 April 26: Tom Donilon, Micah Zenko, David Boies, Ted Olson, Garry Trudeau, Matt Bai, Doris Kearns Goodwin, Helene Cooper, Gov. Asa Hutchinson | Transcript 
 April 19: Gov. Terry McAuliffe, Gov. John Kasich, Sen. Mike Lee, David Axelrod, Kathleen Parker, Helene Cooper, Steve Schmidt | Transcript 
 April 12: Bill de Blasio, Sec. John Kerry, Sen. Rand Paul, Hugh Hewitt, Maria Hinojosa, Stephanie Rawlings-Blake, David Brooks | Transcript 
 April 5: Benjamin Netanyahu, Sen. Chris Murphy, Gov. Bobby Jindal, Cardinal Timothy Dolan, Rob Manfred, Matt Bai, Helene Cooper, Amy Walter, Perry Bacon | Transcript 
 March 29: Tom Costello, Dr. Erin Bowen, Andrea Mitchell, Amb. Adel al-Jubeir, Sam Stein, Neera Tanden, Kathleen Parker, Joe Scarborough | Transcript 
 March 22: Amb. Ron Dermer, Dr. Riyad Mansour, Gov. Jerry Brown, John Stanton, Rich Lowry, Helene Cooper, Jane Harman | Transcript 
 March 15: Adm. Mike Mullen, Sen. Tim Kaine, Sen. Roger Wicker, Rep. Trey Gowdy, Rep. Barney Frank, Matt Bai, Karen Finney, Kevin Madden, Andrea Mitchell | Transcript 
 March 8: Sen. Dianne Feinstein, Sen. Lindsey Graham, Rep. John Lewis, Curt Schilling, Sen. Claire McCaskill, Jonathan Martin, Kathleen Parker, Manu Raju, Amy Walter| Transcript 
 March 1: Rep. Kevin McCarthy, Garry Kasparov, Sen. Joe Lieberman, Rep. Jan Schakowsky, Dr. Ben Carson, Tony Messenger, Hugh Hewitt, Helene Cooper, Maria Hinojosa, Chris Cillizza | Transcript 
 February 22: Haley Barbour, Richard Engel, Sec. Jeh Johnson, Sen. Bob Corker, Sherrilyn Ifill, Rep. Charlie Dent, Michael Gerson, Robert Gibbs, Nia-Malika Henderson, Amy Walter | Transcript  
 February 15: Sen. John McCain, Sen. Jack Reed, Richard Engel, Robert McDonald, Dana Carvey, David Axelrod, Kathleen Parker, April Ryan, Joe Scarborough | Transcript 
 February 8: Sec. John Kerry, Michael McFaul, Jon Meacham, David Brooks, Stephen Henderson, Katty Kay, Andrea Mitchell | Transcript 
 February 1: Rep. Paul Ryan, Robert Gates, Leonard Marshall, Demaurice Smith, Jeff Pash, Savannah Guthrie, Kathleen Parker, Mark Halperin, Jim Cramer | Transcript 
 January 25: Denis McDonough, Fmr. Gov. Mike Huckabee, Kareem Abdul-Jabbar, Tom Brokaw, Hugh Hewitt, Helene Cooper, Mayor Stephanie Rawlings-Blake | Transcript 
 January 18: Gerard Birard, Robert Gibbs, Kelly O'Donnell, Carol Lee, Michael Steele, Dan Pfeiffer, Sen. Lindsey Graham | Transcript 
 January 11: Attorney General Eric Holder, Mohammed Al-Kibsi, Reza Aslan, Arsalan Iftikhar, Danielle Pletka, Clifford Sloan, Carol Rosenberg, Rich Lowry, David Brooks, Andrea Mitchell, Helene Cooper | Transcript 
 January 4: Sen. Amy Klobuchar, Sen. John Barrasso, Lt. Gen. Dan Bolger, Sarah Chayes, Muriel Bowser, Kaya Henderson, Cathy Lanier, Andrea Mitchell, John Stanton, Matt Bai, Helene Cooper | Transcript 
 December 28: William Bratton, Lewis Black, W. Kamau Bell, Laura Krafft Amy Walter, Gene Robinson, Luke Russert, and Ken Blackwell | Transcript 
 December 21: Sen. Marco Rubio, Christopher Hill, Michael Chertoff, Kal Penn, David Boies, Eric Adams, Michael Leiter, John Nolte, Bill Richardson, Sara Fagen, Chris Matthews | Transcript 
 December 14: Fmr. Vice President Dick Cheney, Sen. Ron Wyden, David Axelrod, Dan Senor, Andrea Mitchell, Helene Cooper, Richard Engel | Transcript 
 December 7: Rev. Al Sharpton, Esaw Garner, Mayor Kasim Reed, John Stanton, Amy Walter, Rick Santelli, Cyrus Vance Jr., Michael Nutter, Chuck Canterbury, Charles Ramsey | Transcript 
 November 30: Gov. Deval Patrick, Sherrilyn Ifill, David Brooks, Charles Ogletree, Dr. Ben Carson, Rep. Tom Cotton, Rich Lowry, Andrea Mitchell, Helene Cooper, Eugene Robinson | Transcript 
 November 23: Anthony Gray, Rudy Giuliani, Michael Eric Dyson, Jose Diaz-Balart, Amy Walter, Bill Richardson, Joe Scarborough, Sen. Jeff Flake, Sen. Robert Menendez, Daniel Yergin, John Hofmeister | Transcript 
 November 16: Sec. Sylvia Mathews Burwell, Gov. Bobby Jindal, Dr. Toby Cosgrove, Avik Roy, Neera Tanden, Carly Fiorina, Reid Wilson, Chris Matthews, Helene Cooper | Transcript 
 November 9: Gov. Scott Walker, Howard Dean, Howard Schultz, Mike Rounds, Gwen Graham, Jose Diaz-Balart, Amy Walter, Stephanie Cutter, Rep. Eric Cantor | Transcript 
 November 2: Sen. Rand Paul, Michael Steele, Andrea Mitchell, Robert Gibbs, Joe Scarborough, Kaci Hickox, Norman Siegel, Charlie Cook, Bill McInturff, Amy Walter, Fred Yang | Transcript 
 October 26: Dr. Anthony Fauci, Sophie Delaunay, Michael Leiter, Arsalan Iftikhar, Sen. Chuck Schumer, Sen. Rob Portman, Luke Russert, Nia-Malika Henderson, Carolyn Ryan, Dan Balz | Transcript 
 October 19: Dr. Anthony Fauci, Anthony Banbury, Dr. Gabe Kelen, Laurie Garrett, Sen. Bob Casey, Sen. Roy Blunt, Mike Murphy, Andrea Mitchell, Stephanie Cutter, Manu Raju | Transcript 
 October 12: Susan Rice, Dr. Anthony Fauci, Kathleen Parker, David Brody, Tom Brokaw, Helene Cooper, Sara Fagen, Robert Gibbs | Transcript 
 October 5: Dr. Tom Frieden, Dan Pfeiffer, Reince Priebus, Jim Webb, David Axelrod, Andrea Mitchell, Gwen Ifill, Joe Scarborough | Transcript 
 September 21: Sen. Ron Johnson, Sen. Chris Murphy, Adm. Mike Mullen, Neera Tanden, Ramesh Ponnuru, John Stanton, Amy Walter | Transcript 
 September 14: Denis McDonough, Mike Murphy, Nia-Malika Henderson, Jim Vandehei, Helene Cooper, James Baker, Sen. Bernie Sanders, Bryant Gumbel | Transcript 
 September 7: President Barack Obama, Andrea Mitchell, Michael Leiter, Joe Scarborough, Nia-Malika Henderson, John Stanton, Amy Walter, Mayor Marilyn Strickland, Mayor Bill Peduto, Mayor Mick Cornett | Transcript 
 August 31: Sen. Dianne Feinstein, Michele Flournoy, Michael Leiter, Anthony Zinni, Michael McFaul, Dan Henninger, Doris Kearns Goodwin, Wes Moore, Ruth Marcus | Transcript 
 August 24: Rep. Mike Rogers, Amb. Peter Westmacott, Richard Engel, Helene Cooper, Gwen Ifill, David Ignatius, Mayor Kasim Reed, Michael Gerson, Gov. Jay Nixon, Rev. Al Sharpton, Sen. Rand Paul | Transcript 
 August 17: Gov. Jay Nixon, Wesley Lowery, Gilbert Bailon, Stephanie Rawlings-Blake, Charles Ogletree, Bernard Parks, Rep. John Lewis, Anne Gearan, Jason Riley, Jane Harman, Rep. Michael Turner, Barry Levinson | Transcript 
 August 10: Sen. Dick Durbin, Rep. Peter King, Michael Leiter, Robin Wright, Jeffrey Goldberg, Chuck Todd, Rep. Donna Edwards, Rich Lowry, Andrea Mitchell, Greg Craig, Newt Gingrich | Transcript 
 August 3: Dr. Tom Frieden, Dr. Toby Cosgrove, Riyad Mansour, Amb. Ron Dermer, Sen. John Thune, Sen. Menendez, Rep. Keith Ellison, Mike Murphy, Kristen Soltis Anderson, Carolyn Ryan | Transcript 
 July 27: Prime Minister Benjamin Netanyahu, Chris Gunness, Sen. Chuck Schumer, Rep. Paul Ryan, Nia-Malika Henderson, Judy Woodruff, Ruth Marcus, David Brooks | Transcript 
 July 20: Se of State John Kerry, Sec. Lindsey Graham, Ron Fournier, Andrea Mitchell, Jason Riley, Amy Walter, Dr. Anthony Fauci | Transcript 
 July 13: Martin Indyk, Rep. Mike Rogers, Rep. Joaquin Castro, Iranian Foreign Minister Mohammad Javad Zarif, Fmr. Sen. Rick Santorum, Fmr. Gov Jennifer Granholm, Kim Strassel, Stephen Henderson | Transcript 
 July 6: Secretary Jeh Johnson, Rep. Raul Labrador, Chuck Todd, Carolyn Ryan, Michael Gerson, Lori Montenegro | Transcript 
 June 29: Former President Bill Clinton, Reince Priebus, Kathy Ruemmler, Rep. Sean Duffy, Andrea Mitchell, Nia-Malika Henderson, Valerie Jarrett | Transcript 
 June 22: Prime Minister Benjamin Netanyahu, Sen. Rand Paul, Rep. Michael McCaul, Michele Flournoy, E.J. Dionne, David Brooks, Katty Kay, Erika Harold | Transcript 
 June 15: Fmr.Gov. Mitt Romney, Sen. Joe Manchin, Rep. Peter King, Paul Wolfowitz, David Ignatius, Dexter Filkins, Chuck Todd, Fmr. Rep. Harold Ford, Ken Cuccinelli, Steve Schmidt, Ruth Marcus, Luke Russert | Transcript 
 June 1: Secretary Chuck Hagel, Michael Leiter, Paul Rieckhoff, Rep. Adam Kinzinger, Fmr. Mayor Michael Bloomberg, Fmr. Rep. Jane Harman, Fmr. Speaker Newt Gingrich, Rana Foroohar, Chuck Todd | Transcript 
 May 18: Andrea Mitchell, Reince Priebus, Sen. Claire McCaskill, Maria Shriver, Carolyn Ryan, Carly Fiorina, Chuck Todd, Dr. Ben Carson, Fmr. Sen. Blanch Lincoln, Jim Miklaszewski, Wes Moore, Rep. Adam Kinzinger | Transcript 
 May 4: Mayor Kevin Johnson, Gov. Rick Perry, Rep. Jason Chaffetz, Anita Dunn, Chuck Todd, Kathleen Parker, Will.I.Am | Transcript 
 April 27: Fmr. British Prime Minister Tony Blair, Tony Blinken, Lorraine Miller, Rev. Al Sharpton, Bryant Gumbel, Rich Lowry, Neera Tanden, Jeffrey Goldberg, Mallory Factor | Transcript 
 April 20: Ukraine Prime Minister Arseny Yatseniuk,Sen. Bob Corker, Sen. Chris Murphy, Rep. Debbie Wasserman-Schultz, Jo Becker, Chuck Todd, David Brooks, Radhika Jones, David Shribman | Transcript 
 April 13: Secretary Kathleen Sebelius, Sen. Ed Markey, Ed Davis, Doris Kearns Goodwin, John Tlumacki, Joe Andruzzi, Paul Gigot, Mike Murphy, Kara Swisher, Rep. Donna Edwards, Ken Burns | Transcript 
 April 6: Adm. Mike Mullen, Shaun McCutcheon, Robert Weissman, Kathleen Parker, Frm. Sen. John Sununu, Fmr. Rep. Harold Ford, Steve Case, Michael Lewis | Transcript 
 March 30: Fmr. Amb. Michael McFaul, Peter Baker, Rudy Giuliani, Loretta Weinberg, Jonathan Cohn, Avik Roy, Amy Walter, Svante Myrick, Rick Santorum, Sen. Ron Wyden | Transcript 
 March 23: Bill Neely, Michael Chertoff, Bob Hager, Rep. Mike Rogers, David Brooks, Andrea Mitchell, Mayor Michael Nutter, Rich Lowry, Secretary Arne Duncan, Mark Emmert, Reggie Love, and President Jimmy Carter | Transcript 
 March 16: Dan Pfeiffer, Greg Feith, Sen. Dick Durbin, Sen. Jeff Flake, Jon Ralston, Carolyn Ryan, Robert Gibbs, Israel Ortega, John Yang, Bill Maher, Harry Smith | Transcript 
 March 9: Tony Blinken, Cardinal Timothy Dolan, Rep. Peter King, Tom Costello, Ralph Reed, Andrea Mitchell, Karen Bass, Ron Fournier | Transcript 
 March 2: Secretary of State John Kerry, Sen. Marco Rubio, Gov. Jerry Brown, Stephanie Rawlings-Blake, Kathleen Parker, Chuck Todd, Tina Brown, Jeffrey Goldberg | Transcript 
 February 23: Susan Rice, Richard Engel, Judy Woodruff, David Brooks, Chris Matthews, Helene Cooper, Mark Potter, Harry Smith, Mark Wells | Transcript 
 February 16: Bill Nye, Rep. Marsha Blackburn, Mitt Romney, Chuck Todd, Julie Pace, David Axelrod, Nicolle Wallace, Richard Engel, Andy Cohen, Brian Boitano, Harry Smith | Transcript 
 February 9: Amb. Michael McFaul, Richard Engel, Andrea Mitchell, E.J. Dionne, David Brooks, Mike Needham, Mona Sutphen, Amie Parnes, Jonathan Allen, Sen. Chuck Schumer, Sen. Rob Portman | Transcript  
 February 2: Denis McDonough, Tim Scott, Julian Assange, Rich Lowry, Robert Gibbs, Gwen Ifill, Doris Kearns Goodwin, Chuck Todd, Alan Schwarz, Tony Dungy |Transcript  
 January 26: Rand Paul, Dick Durbin, Michael Chertoff, Jesselyn Radack, Carolyn Ryan, Michael Powell, Mike Murphy, Loretta Sanchez, Chuck Todd |Transcript  
 January 19: Dianne Feinstein, Mike Rogers, Alexis Ohanian, John Wisniewski, Rudy Giuliani, Robert Gates, Newt Gingrich, Andrea Mitchell, Harold Ford Jr., Nia-Malika Henderson | Transcript  
 January 12: Reince Priebus, Mark Halperin, Chuck Todd, Stephanie Rawlings-Blake, Kim Strassel, Maria Shriver, Rick Santorum, Jeffrey Goldberg, Jane Harman, Chris Matthews, Harry Smith | Transcript  
 January 5: Janet Napolitano, Gene Sperling, Jim Cramer, Delos Cosgrove, John Noseworthy, Steve Schmidt, Donna Edwards, Judy Woodruff, Chuck Todd | Transcript  
 For Transcripts before 2014 - Click Here 
 October 4 
