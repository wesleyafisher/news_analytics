__HTTP_STATUS_CODE
200
__TITLE
Good Fats as Helpful as Statins Against Heart Disease, Group Says
__AUTHORS
Maggie Fox
__TIMESTAMP
Jun 19 2017, 8:32 am ET
__ARTICLE
 The American Heart Association wants you to remember that there are such things as good fats and bad fats. 
 It issued a reminder this week that swapping out artery-clogging saturated fats such as butter with healthy vegetable fats can do as much good for some people as taking a statin drug. 
 Scientific studies that lowered intake of saturated fat and replaced it with polyunsaturated vegetable oil reduced cardiovascular disease by approximately 30 percent; similar to cholesterol-lowering drugs, known as statins, the organization said in a "presidential advisory." 
 These experts stress the word replace. That means not just cutting the bad fats  it also means adding the good fats, such as corn, soybean and peanut oil. 
 One reminder that may surprise some people  coconut oil may come from a tree, but its not a particularly healthful fat. 
 Several studies found that coconut oil  which is predominantly saturated fat and widely touted as healthy  raised LDL cholesterol the same way as other saturated fats found in butter, beef fat and palm oil, said the American Heart Association advisory, which published in the journal Circulation. 
 Related: These 10 Foods Affect Your Risk of Heart Disease the Most 
 Another possible surprise: All fats are a mix of saturated, polyunsaturated and monounsaturated fats. The Heart Association said polyunsaturated fats appear to lower the risk of heart disease the most, followed by monounsaturated fats. Saturated fats are the least desirable. 
 The advisory also lists which fats are healthier: 
 Heart disease is the No. 1 killer in the U.S. and in most industrialized countries. Nearly 808,000 people in the United States died of heart disease, stroke, and other cardiovascular diseases in 2014, translating to about one of every three deaths, the Heart Association said. 
 The Heart Association and American College of Cardiology advise anyone with questionable cholesterol readings to get saturated fat down to just 5 percent of total calories. 
 Related: Heart attacks, strokes fell after trans-fat ban 
 We want to set the record straight on why well-conducted scientific research overwhelmingly supports limiting saturated fat in the diet to prevent diseases of the heart and blood vessels, said Dr. Frank Sacks, lead author of the advisory and professor of Cardiovascular Disease Prevention at the Harvard T.H. Chan School of Public Health. 
 Saturated fat increases LDL  bad cholesterol  which is a major cause of artery-clogging plaque and cardiovascular disease. 
 Saturated fats are found in meat, full-fat dairy products and tropical oils such as coconut, palm and others. 
 A healthy diet doesnt just limit certain unfavorable nutrients, such as saturated fats, that can increase the risk of heart attacks, strokes and other blood vessel diseases. It should also focus on healthy foods rich in nutrients that can help reduce disease risk, like poly- and mono-unsaturated vegetable oils, nuts, fruits, vegetables, whole grains, fish and others, Sacks said. 
