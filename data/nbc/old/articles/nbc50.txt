__HTTP_STATUS_CODE
200
__TITLE
Opposition Cries Fraud After Socialists Win Venezuela Regional Vote 
__AUTHORS
Associated Press
__TIMESTAMP
Oct 16 2017, 12:45 am ET
__ARTICLE
 CARACAS, Venezuela - President Nicolas Maduro's government won a majority of governorships in Venezuela's regional election on Sunday, drawing fraud suspicions from the opposition, whom polls had shown poised for a big win. 
 Electoral board President Tibisay Lucena said the ruling Socialist Party took 17 governorships to five for the opposition Democratic Unity coalition, with results irreversible in all but one of the OPEC member's 23 states. 
 "'Chavismo' is alive, in the street and triumphant," a beaming Maduro, 54, said in a speech to the nation, referring to the ruling movement's name for former President Hugo Chavez. 
 Opposition leaders had warned that the pro-government election board was about to announce results that contradicted their certainty of a "gigantic" victory. 
 "We have serious suspicions and doubts," election campaign chief Gerardo Blyde told reporters. 
 Although opposition leaders stopped short of presenting detailed accusations or evidence of fraud, rank-and-file supporters expressed disgust at what they said was obvious vote tampering. 
 The election board used a different company for Sunday's vote machines after its former partner, London-based Smartmatic, accused it of manipulating a July election for a new legislative superbody by at least 1 million votes. 
 A survey before Sunday's vote had given the opposition 44.7 percent of voter intentions, compared to 21.1 percent for the government  close to Maduro's own approval rating of 23 percent. 
 The opposition's five victories included the restive Andean states of Merida and Tachira, as well as the oil-producing region of Zulia. 
 The government won back populous Miranda state, which includes part of Caracas, for an up-and-coming star of the Socialist Party, Hector Rodriguez, the election board said. 
 And Chavez's younger brother, Argenis Chavez, held the rural state of Barinas, where the family comes from. 
 The ruling Socialist Party previously controlled 20 of the 23 state governorships. But opinion polls had shown the opposition set to upend that, given voter anger at hunger and shortages stemming from an economic meltdown. 
 The opposition's five wins were two more than they took in the 2012 gubernatorial race, but far below the 18-19 they had targeted. "These results are unbelievable and inexplicable," opposition spokesman Ramon Aveledo said. 
 The surprise results raised the prospect of more unrest in Venezuela, where four months of opposition-led protests earlier this year led to 125 deaths, thousands of arrests and injuries, and widespread destruction of property. 
