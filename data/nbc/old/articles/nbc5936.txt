__HTTP_STATUS_CODE
200
__TITLE
U.S. Service Member Killed in Northern Iraq
__AUTHORS
Courtney Kube
Chelsea Bailey
Reuters
__TIMESTAMP
Oct 20 2016, 7:28 pm ET
__ARTICLE
 A U.S. service member died Thursday from wounds sustained in the explosion of an improvised explosive device in northern Iraq, the U.S.-led military coalition said. 
 A U.S. defense official told NBC News on the condition of anonymity that the vehicle hit the IED at 6 p.m. (11 a.m. ET) near Mosul, where more than 100 U.S. personnel are embedded with Iraqi and Kurdish Peshmerga forces in an offensive to retake the crucial city from ISIS. 
 The service member, who wasn't identified, died after being evacuated by medical helicopter to a hospital in Irbil, said the defense official, who said no other casualties were known to have occurred in the incident. 
 This is the fourth U.S. service member to have been killed since the start of Operation Inherent Resolve in 2014, focused on fighting ISIS in Iraq and Syria. 
 The Pentagon this week played down any new role for U.S. forces in Iraq's battle to retake Mosul and said they would be behind the forward line of troops. But as the United States has increased its presence in Iraq this year to help in the Mosul fight, officials have acknowledged that Americans will be "closer to the action." 
