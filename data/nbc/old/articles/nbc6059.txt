__HTTP_STATUS_CODE
200
__TITLE
How to Keep Your Belongings Safe in the Gig Economy
__AUTHORS
Alyssa Newcomb
__TIMESTAMP
Feb 8 2017, 7:33 am ET
__ARTICLE
 With a slew of apps and services, it's now easier than ever to hire a stranger to come to your home, at your convenience, for cleaning, repairs and any odd jobs. 
 Most of the time, the gig economy works perfectly for the customer and the worker. Many of the large services, like Handy and TaskRabbit, require people on the employee side to pass a background check. 
 But, while it's far more likely your favorite pair of shoes may be moved in your bedroom, rather than stolen, it still doesn't hurt to take a few extra precautions when you're trusting a stranger to come into your home. 
 Related: How Apps Can Help You Be Your Own Boss 
 Locking your most expensive items in a drawer or a safe is of course the most obvious option. However, there are also a number of tech tools you can use for home safety year-round, even when you don't have a stranger in your home. 
 If you want to make sure a certain area of your home stays off-limits, consider investing in smart sensors. 
 Samsung SmartThings makes a multipurpose sensor that works with a smart hub to notify you any time that private drawer or door is opened. As an added bonus, when you're not using it for security, place it on your washer and dryer and you'll get notified on your smartphone when your laundry is done. A smart hub will set you back around $150. 
 Another option is the Philips Hue Motion Sensor, which connects to a Phillips Hue Bridge. In total, the duo runs about $100 and can even turn on your smart lights when you get home. 
 Canaryis an all-in-one home security system trained to send intelligent alerts to a user's smartphone when it detects activity. 
 You can also open your Canary app to see live video and audio of what's happening in your home at that moment and replay video clips. The all-in-one security system can even let you sound a 90+ decibel alarm if you see shady activity unfolding live on your app, hopefully scaring the thief away and alerting neighbors. One device starts at $169, with deals for multi-packs. 
 Another option is the Nest Cam Indoor, which can be placed in an inconspicuous place in your home and record video that can be stored in the cloud for 30 days. If there's a suspicious sound in your home, Nest will also send an alert to your phone, so you're immediately in the know. It can also send "person alerts," letting you see who's in your home. The indoor camera starts at $199, but there are deals if you buy multiple devices to cover more areas of your home. 
 If losing your favorite bag or coat is a worry, consider putting a Tile tracker in it. The tiny device pairs to your smartphone and will let you know where your item is, as long as the Tile is still inside of it. 
 More often than not, it's likely your housekeeper moved that bag to vacuum and you'll find it somewhere in your house. If you're having trouble locating an item, you can also reach out to the customer service representatives from whichever platform you used. They can ask the service provider if they saw a specific item  and more often than not, quickly tell you where it is in your home. 
