__HTTP_STATUS_CODE
200
__TITLE
Trudeau and Obama Share Intimate Montreal Meal
__AUTHORS
Associated Press
__TIMESTAMP
Jun 7 2017, 4:49 pm ET
__ARTICLE
 Canadian Prime Minister Justin Trudeau tweeted his thanks to former U.S. President Barack Obama after the two shared a private dinner at a Montreal restaurant. 
 Trudeau posted a picture of the pair talking in the eatery, with the caption How do we get young leaders to take action in their communities? Thanks @BarackObama for your visit & insights tonight in my hometown. 
 Last year, Obama hosted Trudeau for a state dinner at the White House, the first for Canada since 1997. 
 PHOTOS: Canada's New Leader Receives Warm White House Welcome 
