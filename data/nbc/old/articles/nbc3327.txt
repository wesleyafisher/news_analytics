__HTTP_STATUS_CODE
200
__TITLE
The Frog Slime Cure for Flu
__AUTHORS
Maggie Fox
__TIMESTAMP
Apr 18 2017, 11:27 pm ET
__ARTICLE
 A compound found in the protective slime of an Indian frog blows apart flu viruses and might become a powerful new drug to treat influenza, researchers reported Tuesday. 
 The compound, a small structure called a peptide, cured mice of killer doses of human flu, the research team reported in the journal Immunity. 
 They hope to develop it and other compounds like it into antiviral drugs to treat people. 
 This peptide kills the viruses. It kind of blows them up, Joshy Jacob of Emory University, who led the study team, told NBC News. 
 And it seems harmless to healthy tissue. Theres no collateral damage, he said. 
 The team found the peptide in mucus taken from the skin of a frog species called Hydrophylax bahuvistara, found only recently in India. 
 Related: Flu Experts Line up to Defend Tamiflu 
 Like most frogs, it secretes protective compounds in its mucus to protect it from bacteria and fungi that can infect frogs. 
 Its not clear why it makes a compound that kills human flu, which does not infect frogs. Jacob thinks its a coincidence. They have it to fight some other bug that is detrimental to their survival, he said. 
 Jacobs named it urumin, after an Indian sword. 
 Jacob works with a team at the Rajiv Gandhi Center for Biotechnology in Poojapura thats been cataloging substances in frog slime. They have been testing them one by one against bacteria, looking for new antibiotics. 
 He decided to try them out against viruses, too. I tested them one at a time against influenza viruses, he said. Peptides are easy to make synthetically in the lab. 
 Related: Flu Vaccine Protects About Half the Time 
 This particular one works against H1 influenza viruses  the ones now circulating in the H1N1 flu that caused a pandemic in 2009 and that now is part of the seasonal flu mix. It does not affect other flu strains, such as the H3N2 flu and influenza B viruses that also circulate. 
 But Jacob believes other frog compounds almost certainly will, and hes testing them against other viruses too, included HIV, hepatitis viruses, Zika and Ebola. 
 Its just a matter of searching and finding them, he said. There are 3,000 of these that are published. 
 The hard part will be to find frog slime secretions that dont hurt human cells, he said. 
 The next challenge is systemic delivery, Jacob added. For the mice, they dripped the peptide into their noses but people may prefer to take pills. Most peptide-based drugs are delivered intravenously, and thats not practical for an everyday drug, which is what Jacob hopes to develop. 
 The peptide specifically attacks a part of the flu virus called hemagglutinin. This is also the structure that gives flu viruses the H in their names, and its the part that many flu vaccines aim for. 
 Related: What's in a Flu Name? 
 Whats good, he said, is that it also kills flu viruses that have mutated to resist the effects of antiviral drugs such as Tamiflu, which attack flu viruses at another point, called neuraminidase. 
 Urumin therefore has the potential to contribute to first-line anti-viral treatments during influenza outbreaks, the team wrote in their report, published in the journal Immunity. 
 Researchers are working to design drugs from the ground up to attack flu, but its a slow process and hit and miss. 
 The ones in nature have evolved over millions of years and perfected themselves by trial and error, Jacob said. These work really, really well. 
 Influenza viruses are notoriously mutation prone but Jacob says he was unable to get one to develop that resisted the effects of urumin. I tried to make mutant viruses that cannot be killed by this peptide, he said. We were unable to do it. 
 Related: Trump Would Cut Budget for Medical Research 
 Now Jacobs looking for money to continue the work. The National Institutes of Health rejected his request to pay for the research, he said. 
 Most drugs that look hopeful early in develop fail at some point in experimental stages. Such an early stage discovery is almost certainly years away from ever being made into a drug, if it ever makes it that far. 
