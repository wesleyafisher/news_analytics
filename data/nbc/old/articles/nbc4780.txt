__HTTP_STATUS_CODE
200
__TITLE
Investigation Launched Into Serious Airbus A380 Engine Failure
__AUTHORS
Alastair Jamieson
Erin Calabrese
__TIMESTAMP
Oct 1 2017, 9:28 pm ET
__ARTICLE
 LONDON  One of 500 passengers aboard an Airbus A380 bound for Los Angeles described Sunday the moment the superjumbo suffered a mid-flight engine explosion and was forced to land in a remote part of eastern Canada. 
 Enrique Guillen said Air France flight AF66 from Paris was passing over Greenland bound for California on Saturday when it was rocked by an uncontained engine failure  a rare emergency in which machinery and other parts break away from the plane at high velocity. 
 Aircraft engines are designed to contain most problems, such as snapped fan blades or bird strikes. 
 Pictures taken from inside the Air France plane showed the front cowling and fan disc of the No. 4 engine, outermost on the right side, had completely sheared off. 
 "We looked out the window and saw half of the engine was missing," said Guillen, an NBC executive who added that hundreds of passengers were stuck aboard the stricken plane at Goose Bay, Labarador, awaiting a replacement. 
 The U.S. manufacturer of the engine, Connecticut-based Engine Alliance, said it was investigating the failure, which the airline said caused "serious damage." 
 "The regularly trained pilots and cabin crew handled this serious incident perfectly," Air France said in a statement, adding that none of the 497 passengers was injured. "The passengers are currently being assisted by teams dispatched to the location." 
 The emergency has echoes of the November 2010 failure of a Rolls Royce engine on a Qantas A380 after take-off from Singapore. That incident led to the grounding of all six A380s in the Qantas fleet for three weeks. 
 Guillen said his flight has been airborne for two hours when passengers heard an unusual noise. 
 "The plane started shaking, it was a noise like once of the engines was failing ... a noise I had never heard before," he recalled. "We obviously knew it wasnt turbulence, it lasted about 30 seconds to one minute and we noticed the place was losing altitude very quickly. 
 "After 1, two minutes ... we looked out the window and saw half of the engine was missing." 
 He said the pilot confirmed there had been a failure in one engine and announced the diversion to Goose Bay. "Everybody had concerned faces, no panic but a lot of concerned faces," he said, adding that the pilots announcement was "oddly reassuring" since the aircraft had three other engines. 
 Speaking from the stricken plane early Sunday, he said passengers had already been waiting nine hours on the ground. "Were in a small, private airport with no facilities for the 500 people in this plane," he said. 
 The aircraft involved, an A380-800 series registered as F-HPJE, is seven years old. 
