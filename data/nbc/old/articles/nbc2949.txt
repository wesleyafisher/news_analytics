__HTTP_STATUS_CODE
200
__TITLE
Researchers Looking for Ways to Predict Alzheimers Before It Starts
__AUTHORS
Maggie Fox
__TIMESTAMP
Jul 19 2015, 4:49 pm ET
__ARTICLE
 Researchers are working on new tests to help predict Alzheimers disease years before people ever get symptoms, both to help them plan for the inevitable but also in the hope that experimental treatments might work better the earlier they are used. 
 Its not easy to diagnose Alzheimers disease even after it develops. Usually, doctors base a diagnosis on symptoms, but its not an exact science and other brain injuries, including stroke, can cause similar symptoms such as memory loss. Many patients are never given a specific diagnosis, the Alzheimers Association says. 
 An early test could make the diagnosis easier and it might eventually lead to ways to prevent Alzheimers altogether, researchers told a conference that started in Washington on Sunday. 
The disease doesnt start when the memory problems become apparent."
 The disease doesnt start when the memory problems become apparent, said Dr. William Klunk, a neurology professor at the University of Pittsburgh and an adviser to the Alzheimers Association. 
 Marilyn Albert, who directs the Alzheimers Research Center at Johns Hopkins University , tracked 189 middle-aged people who were normal to start with. A combination of six tests predicted who would develop mild cognitive impairment within five years. They include genetics, a spinal tap to measure two proteins associated with Alzheimers: amyloid and tau, MRI scans to watch for brain shrinkage, and standard tests of memory. 
 Some people have been followed for almost 20 years, she told a news conference. 
 The combination of tests could be useful for drug companies trying to find people who are likely to develop Alzheimers but who dont have symptoms yet, to see if medications might prevent them, she said. 
 Other tests in the works include blood tests. 
 Earlier diagnosis or, better still, the ability to predict the onset of Alzheimers, would significantly increase the window of opportunity a person with Alzheimers has to formulate an informed response to the news and empower them to be an active participant in decision-making while they still have the ability, Carrillo added. 
 Maartje Kester and colleagues at VU University Medical Center in Amsterdam found levels of another protein, called neurogranin were higher in spinal fluid from people who developed Alzheimers. It may signal damage to nerve connections called synapses are dying. 
That could be a test in the mall someday, if it was developed enough."
 Shraddha Sapkota and colleagues at the University of Alberta in Canada got hints that a saliva test might even be possible. They used a technique called liquid chromatography mass spectrometry to find compounds that might be more common in the saliva of people who later develop Alzheimers. Its only a very small study, with fewer than 100 people, but they found higher levels of six compounds were higher in people who later developed Alzheimers disease. 
 That could be a test in the mall someday, if it was developed enough, Klunk said. 
 The Alzheimer's Association says 5.3 million Americans have the disease, including 200,000 people under the age of 65. 
 "Barring the development of medical breakthroughs, the number will rise to 13.8 million by 2050," the association says in its annual report  with two-thirds being women. 
