__HTTP_STATUS_CODE
200
__TITLE
Federal Judge Blocks Latest Trump Travel Ban
__AUTHORS
Pete Williams
__TIMESTAMP
Oct 17 2017, 4:49 pm ET
__ARTICLE
 WASHINGTON  A federal judge in Hawaii blocked enforcement of President Donald Trump's latest restriction on travel to the U.S., ruling less than 10 hours before it was to take effect. 
 Judge Derrick Watson said the ban, announced by the president on Sept. 24, "suffers from the same maladies" as the earlier travel executive order that the judge also tried to block. 
 The new presidential order maintains restrictions on five of the six countries from the original travel ban  Iran, Libya, Somalia, Syria, and Yemen. It lifts restrictions on visitors from the Sudan, and it adds new restrictions on visitors and immigrants from Chad and North Korea, and on visits by some government officials from Venezuela. 
 The Trump administration said the latest version was the result of setting higher standards for the type of information provided by foreign countries, to assure that visa applicants are who they say are and pose no threat to the United States. Travel was restricted from the countries that failed to meet the new standards. 
 Related: Travel Ban: Why Has Trump Restricted Visas From Chad? Experts Are Puzzled 
 But in a ruling issued Tuesday, Watson said the restrictions lack specific findings that entry of more than 150 million nationals from the eight countries would be harmful to the U.S. and improperly discriminate on the basis of nationality. 
 "The categorical restrictions on entire populations of men, women and children, based upon nationality, are a poor fit for the issues regarding the sharing of 'public-safety and terrorism-related information" that the President identifies," Watson wrote. 
 He ruled in favor of the state of Hawaii, a Muslim group and three individuals who sought a temporary hold to block enforcement of the new travel measure. 
 The judges order did not apply to the latest restrictions on travel from North Korea and Venezuela. But few visas are ever granted to citizens from North Korea, and the restriction on Venezuela applied only to certain government officials and their family members. 
 The White House immediately condemned the ruling, saying that it "undercuts the president's efforts to keep the American people safe and enforce minimum security standards for entry into the United States." 
 The Justice Department said it will immediately appeal. A department spokesman, Ian Prior, said the ruling is "incorrect, fails to properly respect the separation of powers and has the potential to cause serious negative consequences for our national security." 
 A similar challenge to the latest travel restriction is pending in federal court in Maryland. 
