__HTTP_STATUS_CODE
200
__TITLE
Saudi Kings Visit to Russia Marks Shift in Relations on Syria, Oil Prices
__AUTHORS
Yuliya Talmazan
Reuters
__TIMESTAMP
Oct 6 2017, 11:30 am ET
__ARTICLE
 MOSCOW  Russian President Vladimir Putin hosted U.S. ally King Salman of Saudi Arabia for talks at the Kremlin this week, cementing a relationship that is pivotal for world oil prices and possibly signaling a shift in Middle East politics. 
 Salman, the first sitting Saudi monarch ever to visit Russia, led a delegation to Moscow bringing with him investment deals worth several billion dollars, providing much-needed investment for a Russian economy battered by low oil prices and Western sanctions. 
 Saudi Arabia said it signed a memorandum of understanding on the purchase from Russia of S-400 air defense systems. That marked a shift for the kingdom, which buys most of its military supplies from the United States and Britain. 
 On the political front, there was no sign of any substantial breakthrough on the issues that have long divided Moscow and Riyadh, including the fact that they back rival sides in Syria's war. 
 However, any discord was eclipsed by mutual expressions of respect, and the pomp and ceremony Russian officials put on to greet the Saudi king. 
 On his journey into central Moscow from Vnukovo airport late on Wednesday, King Salman's limousine passed billboards bearing his photograph and messages in Russian and Arabic welcoming him. 
 On Thursday, Putin received the monarch in the gold-decorated St. Andrew Hall, one of the grandest spaces in the Kremlin, attended by soldiers in ceremonial dress and with an orchestra playing the two countries' national anthems. 
 "I am sure that your visit will provide a good impulse for the development of relations between our two states," Putin told Salman later as they sat alongside each other in the Kremlin's lavishly-decorated Green Parlour. 
 The Saudi king invited Putin to visit his country  an offer the Russian leader accepted  and said they planned to keep cooperating to keep world oil prices stable. 
 Moscow and Riyadh worked together to secure a deal between OPEC and other oil producers to cut output until the end of March 2018, helping support prices. 
 Energy Minister Khalid al-Falih said Saudi Arabia was "flexible" regarding Moscow's suggestion to extend the pact until the end of next year. 
 The message of further joint Saudi-Russian action on output helped push up oil prices on Thursday. 
 Despite the chumminess, the two powers seemed to agree to disagree on one geopolitical issue: Syria. 
 Riyadh supports rebels fighting President Bashar al-Assads army, while Russian and Iranian forces have sided with Assad. This leaves Moscow aligned with Saudi Arabias arch-rival Iran, whose influence Riyadh fears is growing in the region. 
 Briefing the media after the talks, Russian Foreign Minister Sergei Lavrov focused on the common ground, saying the two leaders had agreed on the importance of fighting terrorism, finding peaceful solutions to conflicts in the Middle East, and on the principle of territorial integrity. 
 His Saudi counterpart, Adel al-Jubeir, said new horizons had opened for Russia-Saudi ties that he could not previously have imagined. 
 "Relations between Russia and Saudi Arabia have reached a historical moment," said Jubeir, speaking through an interpreter. 
 "We are certain that the further strengthening of Russian-Saudi relations will have a positive impact on strengthening stability and security in the region and the world." 
 A package of investments announced during the visit will help cement the new relationship and go some way toward plugging the vacuum left by sluggish Western investment in Russia that is, in part, a result of international sanctions imposed after Moscow annexed Crimea in 2014. 
 Among the deals was an agreement between Russian sovereign wealth fund RDIF and Saudi Arabia's Public Investment Fund to invest up to $100 million in transport projects in Russia. 
 The two countries also signed a deal to set up a $1 billion joint investment fund. 
 There has been no immediate U.S. reaction to the growing ties between Riyadh and Moscow. 
 But the United States, for which Saudi Arabia is an important strategic ally and trading partner in the Middle East, is likely to notice Russia's newfound friendship with Saudi Arabia. 
 Analysts see Salman's trip to Moscow, which follows other visits by Gulf royals, as the clearest sign yet that Russia's high-risk gamble in Syria has paid off. 
 "A number of Gulf leaders have been going with greater regularity to Moscow and I think for a simple reason: Russia has made itself much more of a factor in key parts of the Middle East as the U.S. has taken a step back in some ways, particularly in Syria," said Brian Katulis, a senior fellow at the Center for American Progress. 
 Saudi-U.S. ties grew strained under the Obama administration over its backing of a nuclear agreement with Iran and its cautious stance on the Syrian conflict. 
 Relations have improved  President Donald Trump began his first foreign trip with a stop in Riyadh and signed a nearly $110 billion arms deal with the country. But on Syria, the two countries remain divided with Washington's focus on fighting ISIS the Islamic State group, not on ousting Assad. 
 Anna Borshchevskaya, a fellow at The Washington Institute for Near East Policy, says that even though Moscow has no capacity to replace Washington, its is definitely exerting its diplomatic muscles on the world stage. 
 "It's clear that Russia has been able to play a weak hand very well and step into vacuums everywhere where the U.S. has retreated," she said. 
