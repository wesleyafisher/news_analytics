__HTTP_STATUS_CODE
200
__TITLE
President Trump Subpoenaed Over Sexual Misconduct Allegations
__AUTHORS
Phil McCausland
__TIMESTAMP
Oct 15 2017, 4:04 pm ET
__ARTICLE
 A former contestant on "The Apprentice" who accuses President Donald Trump of past sexual misconduct has filed a subpoena for "all documents concerning any woman who asserted that Donald J. Trump touched her inappropriately," it was revealed on Sunday. 
 Buzzfeed News first reported the existence of the court document, which names Trump's campaign organization and any applicable "directors, officers, partners, shareholders, managers, attorneys, employees, agents and representatives" as subjects. 
 Only a few weeks before the 2016 election, former Apprentice contestant Summer Zervos alleged that Trump had tried to kiss and touch her inappropriately without her consent at the Beverly Hills Hotel in 2007. 
 Her claim came shortly after the October 2016 release of the now-infamous Access Hollywood tape from 2005 in which Trump said: And when youre a star, they let you do it. You can do anything. Grab them by the p----. You can do anything. 
 Related: Trump Accuser Summer Zervos Files Defamation Suit Against President-Elect 
 More than a dozen women publicly accused Trump of sexual assault or misconduct during the 2016 presidential campaign. 
 Zervos's subpoena calls for "all documents concerning any accusations that were made during Donald J. Trumps election campaign for president, that he subjected any woman to unwanted sexual touching and/or sexually inappropriate behavior." 
 The subpoena also specifically names at least 10 women and requests documentation associated with them "or any woman alleging that Donald J. Trump touched her inappropriately." 
 Trump has flatly denied the allegations, calling them lies, lies, lies, which led Zervos to sue him for defamation. His lawyer tried to have Zervos defamation lawsuit dismissed in July, claiming that presidents cannot face civil lawsuits while in office. 
 That motion to dismiss could decide the merit of this subpoena, according to Gloria Allred, the famed women's rights attorney representing Zervos. 
 "The subpoena was served, but we agreed with the campaign to adjourn their response date until after the motion to dismiss is decided, as long as they gave us assurances that the documents be preserved (which they did)," Allred told NBC News via email. 
 Allred said Trump must reply to their filed opposition of his motion to dismiss by Oct 31. The court will schedule a hearing once he files his reply, which means there is no hearing date yet. 
 Representatives for the White House, the Trump Organization and the presidents personal lawyer did not immediately respond to requests for comment. 
