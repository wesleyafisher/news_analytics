__HTTP_STATUS_CODE
200
__TITLE
Demi Lovatos Latest Hit Song Is Dedicated to Her Childhood Bullies
__AUTHORS
Yelena Dzhanova
__TIMESTAMP
Sep 24 2017, 6:39 pm ET
__ARTICLE
 When Demi Lovato dropped her hit single Sorry Not Sorry last month, her fans devoured it immediately, with its upbeat tone and unapologetic vibe, and sent it up the charts. 
 When asked if the song was dedicated to anyone in particular, Lovato was quick to answer yes. 
 "So that song was actually directed to toward the bullies that bullied me in school," she told Weekend Nightly News anchor Kate Snow at the Global Citizen headquarters in New York. "When I was 12, I was bullied so bad that I had to home school. There were, there was a petition for me to take my own life, and people signed it." 
 That could come as a shock to her millions of listeners, who may have initially believed that the song paid sprang from the idea of getting revenge on an ex-lover. 
 A lot of people think its about an-boyfriend or past relationships, but its actually [directed] towards them [the bullies], Lovato clarified in the interview. Im in a really good place in my life today, and Im not sorry about it. 
 Sorry Not Sorry is the lead single on Lovatos upcoming album, Tell Me You Love Me, which will come out on Sept. 29. This will be Lovatos sixth studio album. 
 Over the summer, the song peaked at No. 13 on the U.S. Billboard Hot 100 list. 
 The song also reached the top 10 in Australia, Hungary, Ireland, Latvia, New Zealand and the United Kingdom, as well as the top 20 in Canada, the Czech Republic, Malaysia, Norway, Portugal, Scotland and Slovakia. 
