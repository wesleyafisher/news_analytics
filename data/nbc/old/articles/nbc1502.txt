__HTTP_STATUS_CODE
200
__TITLE
Reunited? Gov. Christie and President Trump to Work Together on Curbing Opioid Abuse
__AUTHORS
Ali Vitali
__TIMESTAMP
Mar 29 2017, 6:25 pm ET
__ARTICLE
 President Donald Trump and New Jersey Gov. Chris Christie were reunited on Wednesday as the friends-turned-political frenemies-turned allies pledged to join forces to fight the nation's opioid epidemic during a listening session at the White House. 
 "I am so honored president would ask me to take on this task," said Christie who was named chairman of a White House commission to combat America's opioid problem. "I think the president and I both agree addiction is a disease, a disease that can be treated." 
 Christies new role comes after the two shared a Valentines Day lunch where the governor's past work on the opioid crisis was a topic of discussion. 
 It also was announced on the same day his former aides were sentenced in the Bridgegate scandal. 
 Related: Democrats Ask HHS Chief: Do You Plan to Sabotage Obamacare? 
 Christie and Trump have known each other for years, but after Trump won the November presidential election Christie was ousted from his post leading the transition. Multiple sources told NBC News at the time that his demotion was because he wasn't loyal enough during the campaign. 
 Other staffers that Christie had brought into the transition operation were dismissed in his wake in what one staffer called a Stalinesque purge of people close to the New Jersey governor. 
 A source pushed back on the idea of a rift on Wednesday. 
 Who says he ever left, the source with knowledge of the relationship told NBC News. 
 During Wednesdays listening session Trump called Christie a friend of mine, great friend of mine  a very, very early endorser. In fact an immediate endorser once he got out of the race." 
 "He liked himself more than he liked me, but other than that," Trump joked. 
 "Still do sir, but that's alright," Christie quipped as those around them laughed. 
 The two men then shook hands. 
 The work on the shared goal of working to curb opioid addiction also give Christie an opportunity to work on a deeply personal issue for Christie who spoke passionately about the problem during his bid for the Republican nomination in 2016. 
 When he campaigned in New Hampshire he spoke as someone who understood "the cost of the community but also the human cost" of the crisis, former New Hampshire GOP Chairwoman Jennifer Horn told NBC News on Wednesday. "It's an extraordinarily painful thing for the loved ones" of those impacted," Horn said. 
 The new commissions goal will be to focus on treatment and prevention of the crisis, while maintaining the law enforcement angle, White House press secretary Sean Spicer said on Wednesday. 
 The commission, which will include other administration officials such as Attorney General Jeff Sessions, Education Secretary Betsy DeVos, and Defense Secretary James Mattis, will be tasked with outlining recommendations and federal resources to tackle the drug addiction that kills an estimated 78 people a day. 
 However, the Trump administration budget blueprint calls for a cut of about 20 percent of the National Institutes of Health's $30 billion budget. That $5.8 billion cut to the medical research center casts doubt about how much fiscal support the White House will throw to one of the nation's main centers for research on diseases and treatments of such illnesses as drug addiction. 
 President Trump and his budget team have promised increased defense spending with cuts across other agencies, including the State Department and the Environmental Protection Agency, to offset the increase. 
