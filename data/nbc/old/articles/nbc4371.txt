__HTTP_STATUS_CODE
200
__TITLE
Stars Unite for Hurricane Relief Telethon, Raise More Than $14 Million
__AUTHORS
Reuters
__TIMESTAMP
Sep 12 2017, 7:00 pm ET
__ARTICLE
 LOS ANGELES  A cadre of Hollywood celebrities gave passionate speeches, moving performances and hit the phones on Tuesday at a live televised telethon to raise funds for the survivors and victims of Hurricanes Harvey and Irma. 
 Celebrities including George Clooney, Julia Roberts, Leonardo DiCaprio, Matthew McConaughey, Tom Hanks, Beyonce, Justin Bieber and Demi Lovato came together at the "Hand in Hand" telethon with inspirational songs and stories of survival during the devastating disasters. 
 The hour-long telethon, which aired across broadcast, cable and digital platforms, raised more than $14 million. During the show, Stephen Colbert said Apple Inc had donated $5 million, which the comedian quipped, "is also the price of the new iPhone." 
 Beyonce, appearing in a taped message, spoke of the urgent need for supplies in her hometown of Houston after Harvey became the most powerful hurricane to hit Texas in more than 50 years, killing more than 50 people, displacing more than 1 million people and damaging some 203,000 homes. 
 "During the time where it's impossible to watch the news without seeing violence or racism in this country, just when you think it couldn't possibly get worse, natural disasters take precious life, do massive damage and forever change lives," Beyonce said. 
 "Natural disasters don't discriminate. They don't see if you're an immigrant, black or white, Hispanic or Asian, Jewish or Muslim, wealthy or poor ... we're all in this together," she said. 
 Irma, one of the most powerful Atlantic storms on record, ravaged several islands in the northern Caribbean. It barreled into the Florida Keys on Sunday, where 90 percent of homes are believed to be demolished or heavily damaged. 
 The telethon aired from New York, Nashville and Los Angeles, where Stevie Wonder kicked off the event singing "Lean On Me" with a gospel choir as stars such as Hanks, Barbra Streisand and Cher clapped along while sitting at their phone stations. At one point, Clooney and Roberts, sitting next to each other, jovially swapped phones to speak to each other's callers. 
 From Nashville's Grand Ole Opry, Reese Witherspoon and Nicole Kidman told the story of a young girl who survived by clinging to her mother's lifeless body. R&B singer Usher and country music star Blake Shelton sang "Stand By Me." 
 Tori Kelly and Luis Fonsi performed Leonard Cohen's "Hallelujah," Dave Matthews performed his song "Mercy" and George Strait sang his song "I Believe," joined by friends including Miranda Lambert and Chris Stapleton. 
