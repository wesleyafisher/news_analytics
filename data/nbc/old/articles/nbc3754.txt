__HTTP_STATUS_CODE
200
__TITLE
Experts: North Korea Targeted U.S. Electric Power Companies
__AUTHORS
Andrea Mitchell
Ken Dilanian
__TIMESTAMP
Oct 10 2017, 6:05 pm ET
__ARTICLE
 WASHINGTON  The cybersecurity company FireEye says in a new report to private clients, obtained exclusively by NBC News, that hackers linked to North Korea recently targeted U.S. electric power companies with spearphishing emails. 
 The emails used fake invitations to a fundraiser to target victims, FireEye said. A victim who downloaded the invitation attached to the email would also be downloading malware into his or her computer network, according to the FireEye report. The company did not dispute NBC's characterization of the report, but declined to comment. 
 There is no evidence that the hacking attempts were successful, but FireEye assessed that the targeting of electric utilities could be related to increasing tensions between the U.S. and North Korea, potentially foreshadowing a disruptive cyberattack. 
 "This is a signal that North Korea is a player in the cyber-intrusion field and it is growing in its ability to hurt us," said C. Frank Figliuzzi, a former chief of counterintelligence at the FBI. 
 The FireEye report comes on the heels of an NBCNews.com report in August that U.S. intelligence officials are increasingly worried that North Korea will lash out against enhanced U.S. pressure by using its fearsome cyber capabilities to attack U.S. infrastructure. 
 Click Here to Read the Original Report 
 "We've been worried for some time that one of the ways that North Korea can retaliate against further escalation of tensions is via cyber, and particularly attacks against our financial sector," Dmitri Alperovitch, co-founder of Crowdstrike, a cybersecurity firm, told NBC News in August. "This is something they have really perfected as an art against South Korea." 
 Scott Aaronson, a top security official at the Edison Electric Institute, an industry trade group, said in a statement: "Phishing attacks are something that electric companies prepare for and deal with on a regular basis, often in coordination with security experts and industry stakeholders. In this case, the delivery of safe and reliable energy has not been affected, and there has been no operational impact to facilities or to the systems controlling the North American energy grid." 
 EEI referred questions to Robert Lee, a cybersecurity expert who consults with the industry. 
 Lee told NBC News that "any targeting of infrastructure by a foreign power is a concerning thing," but that North Korea and other adversaries "are far from being able to disrupt the electric grid." 
 "This activity represents initial targeting, and if disruptions are even possible they would be very minor," he said. 
 Related: Has It Ever Been This Bad With North Korea? Yes, Actually 
 Lee added: "The North American power grid is considerably better off than people give it credit for. Our threats are growing, and this activity comes at a concerning geopolitically tense time, but if the North Koreans tried to disrupt the electric grid, they would be disappointed with what they were able to achieve." 
 But not every part of the electric grid is equally well defended. Some under-funded public utilities rely on antiquated equipment and outdated cybersecurity strategies, experts have long said, and Figliuzzi said there's also great disparity among private utility companies. 
 "North Korea and any other hacking state will start looking for the weakest link, where's the weakest part of that defense," he said. "And when they find it, they'll exploit it. So there's a need to step up security in that regard." 
 American intelligence officials rank North Korea behind Russia, China and Iran among U.S. adversaries in ability to inflict damage via cyberattacks. 
 Related: U.S. Spy Agencies Agree North Korea Can Fit Nuke on Missile 
 In 2014, U.S. intelligence officials say, North Korean hackers attacked Sony Pictures, destroying corporate computers and disclosing sensitive company data. The U.S. accused North Korea of carrying it out in response to a film lampooning North Korean leader Kim Jong Un. 
 Experts worry that North Korea could deploy the same techniques to inflict harm not just on one company, but on the American economy. 
