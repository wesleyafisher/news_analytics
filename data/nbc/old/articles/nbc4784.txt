__HTTP_STATUS_CODE
200
__TITLE
African-American History Museum to be Honored With Forever Stamp
__AUTHORS
Foluke  Tuakli
__TIMESTAMP
Oct 10 2017, 6:39 pm ET
__ARTICLE
 In honor of its first anniversary, the National Museum of African American History and Culture will be commemorated with a Forever Stamp by the U.S. Postal Service. 
 Deputy Postmaster General and Chief Government Relations Officer Ronald A. Stroman praised the D.C.-based museum for acknowledging African-American history in the U.S. and called the recognition of a stamp an honor. 
 "The National Museum of African American History and Culture is an American treasure that serves as a repository for the history of suffering, struggle and triumph of African Americans," Stroman said in a statement to NBC News. 
 The stamp features a photo of the northwest corner of the museum, displaying the bronze-colored architecture designed by David Adjaye. 
 The museum, a Smithsonian institution, celebrated it's one year anniversary on September 24 and has hosted close to three million visitors since it opened its doors. It is the first national museum "devoted to exploring and displaying the African-American story." It was established by Congress in 2003. 
 The stamp will be dedicated at the museum and available for purchase on Friday, Oct. 13. 
 Follow NBCBLK on Facebook, Twitter and Instagram 
