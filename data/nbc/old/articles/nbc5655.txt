__HTTP_STATUS_CODE
200
__TITLE
Its Never Too Early: Three Holiday Marketing Tips to Live By
__AUTHORS
Lindsay Anvik, SeeEndless.com
__TIMESTAMP
Oct 4 2017, 1:25 pm ET
__ARTICLE
 If youre a business owner or manager, holiday marketing is something that you probably should have thought of months ago. Marketing plans should technically be in place 6-12 months in advance. This allows for planning, budgeting, and the ability to add a new idea or event without panicking. 
 The reality though is that a lot of businesses only develop their marketing plans few weeks in advance. So with Q4 looming, here are a few tricks to boost your holiday marketing efforts. 
 The old adage team work makes the dream work rings true even with marketing. Find another business to collaborate with for an event or joint marketing effort. 
 Pick a business with a bigger social media presence or email mailing list than you, so that you can leverage the businesss broader reach. 
 Then offer a complete marketing plan and offer to do most of the heavy lifting like event descriptions, clean up, and hosting to make it easy for them to say yes. 
 Get customers through your doors before the holiday craze begins, and incentivize them to purchase early. 
 Offer discounts, deals, preview nights, VIP customer access and more as a way to bring them into your business. Invite customers onto your site with a value proposition that is meaningful for them. For example, if youre a bakery, host a free class on how to decorate cookies. If youre a brewery, maybe its a beer tasting. 
 Once customers are through your doors find ways to get them to spend money right away or soon after the event. Use the time that you have with them to promote whatever holiday specials, products or services you offer. Dont wait for the customer to get into the holiday mood. Its your job to get them there. 
 Your staff should have a specific daily sales goal for Q4. If youre a retailer, try to add on one item to every sale they ring up. If youre a restaurant, make it a goal to have every table order at least 1 dessert. If youre a gym, try to get every member to buy a one-month package for their loved one. 
 Keep the goal simple and specific. Make sure your staff knows what the goal is and give tips on how to execute. 
 Management can even make it into a game by giving prizes to the staff member or members who perform the best by the end of the day, week, or month. 
 Lindsay Anvik is a business coach and international keynote speaker. Her seminars and consulting have been praised by major museums, Fortune 500 companies, and small mom and pop business owners. Her company, See Endless, consults with businesses by playing the role of both coach and cheerleader. 
