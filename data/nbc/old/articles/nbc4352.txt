__HTTP_STATUS_CODE
200
__TITLE
Ben Affleck Accused of Once Groping Hilarie Burton as Criticism Against Weinstein Mounts
__AUTHORS
Daniella Silva
__TIMESTAMP
Oct 18 2017, 1:00 pm ET
__ARTICLE
 Hours after Ben Affleck released a statement condemning the alleged sexual misconduct of Hollywood mogul Harvey Weinstein, the actor was accused of having groped actress Hilarie Burton in the early 2000s. 
 Affleck went on social media Tuesday to address the controversy surrounding Weinstein. That prodded one Twitter user to comment that Affleck "also grabbed Hilarie Burton's breasts on TRL once. Everyone forgot though." 
 "I didn't forget," Burton, a former host on MTV's "Total Request Live" from 2000 to 2003 and actress on "One Tree Hill," responded. 
I didn't forget.
 "I was a kid," Burton, now 35, said in another tweet. 
 The actress included a video showing the uncensored cold open of "TRL" in which she says, in apparent reference to Affleck, "He comes over and tweaks my left boob." 
 Video of the incident posted online also appears to show Affleck when he interacted with Burton over a decade ago. While no physical touching is seen on screen, Burton describes what happened in a candid interview segment during the clip. 
 The video was edited, and NBC News cannot determine what may have been left out. 
 "He wraps his arm around me and comes over and tweaks my left boob," she says in the video, later adding, "I'm just like, 'What are you doing?'" 
 "Some girls like good tweakage here and there," she continues. "I'd rather have a high-five." 
 In her tweet Tuesday, she clarified why she appeared to have been joking in that video. 
 "Girls. I'm so impressed with you brave ones," she said, linking to the "TRL" uncensored cold open. 
 "I had to laugh back then so I wouldn't cry. Sending love," she tweeted. 
 Burton couldn't immediately be reached Wednesday by NBC News. 
 Representatives for Affleck didn't respond to a request for comment, but the actor issued a tweet Wednesday addressed to Burton without specifically acknowledging that he groped her. 
I acted inappropriately toward Ms. Burton and I sincerely apologize
 Later Wednesday, makeup artist Annamarie Tendler accused Affleck of inappropriately grabbing her during a party in 2014. 
 "I would also love to get an apology from Ben Affleck who grabbed my ass at a Golden Globes party in 2014," she tweeted, describing the incident in detail in follow up posts.  
I would also love to get an apology from Ben Affleck who grabbed my ass at a Golden Globes party in 2014.
 "Like most women in these situations I didn't say anything but I have thought a lot about what I'd say if I ever saw him again," she added.  
 Affleck, who first rose to fame with Weinstein's help in the 1997 film "Good Will Hunting," was criticized for not denouncing the famous producer's alleged behavior sooner. 
 Weinstein stands accused of sexual harassment or sexually inappropriate behavior by more than two dozen women in a string of allegations stemming over decades. 
 His representatives said in a statement that "any allegations of non-consensual sex are unequivocally denied" by him and that he did not retaliate against women who rejected his advances. 
 Affleck decided to release his statement Tuesday, following two additional reports where more women, including actresses Gwyneth Paltrow and Angelina Jolie, came forward to accuse Weinstein of sexual harassment. 
 Three women in a New Yorker magazine article accused Weinstein of rape. 
 Related: Cara Delevingne Comes Forward With Claims Against Weinstein 
 "I am saddened and angry that a man who I worked with used his position of power to intimidate, sexually harass and manipulate many women over decades," Affleck said in a Facebook post. "The additional allegations of assault that I read this morning made me sick. This is completely unacceptable, and I find myself asking what I can do to make sure this doesn't happen to others." 
 But Affleck was quickly slammed by actress Rose McGowan for saying he had been unaware of Weinsteins alleged behavior. The Times reported that Weinstein reached a $100,000 settlement with McGowan in 1997 after an episode in a hotel room during the Sundance Film Festival. 
 Other male celebrities who have commented on the allegations against Weinstein include actors George Clooney, Colin Firth and Mark Ruffalo, director Kevin Smith, and Broadway star Lin-Manuel Miranda. 
 Tuesday's reports came five days after The Times first published a report detailing allegations of sexual harassment against the producer. Weinstein was subsequently fired by the company he co-founded. 
