__HTTP_STATUS_CODE
200
__TITLE
Jessica Chambers Case: Mistrial Declared in Death of Teenager Burned Alive
__AUTHORS
Associated Press
__TIMESTAMP
Oct 17 2017, 10:53 am ET
__ARTICLE
 BATESVILLE, Miss.  The jury in the tense murder trial of a Mississippi man charged with setting a 19-year-old friend on fire and leaving her to die handed a bailiff a note: They have reached a verdict. 
 What followed was confusion. 
 Before the decision was read, Judge Gerald Chatham asked if the 12-person panel had unanimously agreed on a verdict. A male juror spoke the shocking words: We didnt all agree. 
 That started a chain of events that led to a hung jury and a mistrial in the murder trial of Quinton Tellis and sparked a wave of emotions for his family and the relatives of Jessica Chambers. For the Chambers family, it ended up being pain and frustration. For relatives of Tellis, it was relief and joy. 
 Prosecutors said Tellis had sex with Chambers and set her and her car on fire in a rural back road in Courtland, Mississippi, on the night of Dec. 6, 2014. 
 Chambers was found walking on the road looking like a zombie, according to trial testimony. She had third-degree burns on most of her body when she died at a hospital in Memphis, Tennessee, about 60 miles north of Courtland. 
 Related: Jessica Chambers, Teen Burned to Death, Mourned at Funeral 
 The horrific circumstances surrounding Chambers death garnered national attention. A jury was chosen from another county 225 miles away and brought to Panola County for the trial. 
 After the verdict disagreement, the judge asked the panel to continue deliberating. Shortly afterward, a court clerk read what was believed to be the final verdict: Not guilty. 
 Tellis relatives smiled. Chambers family cried. 
 Then, the judge polled the jury. Seven for guilty, five for not guilty. 
 Spectators looked at each other in dismay. How could the verdict be not guilty, if seven people said guilty? 
 The jury had been instructed by the judge that, under the capital murder charge, all 12 of them must agree on a guilty verdict. But the instruction did not say that the entire panel of seven blacks and five whites also must unanimously agree on a not guilty verdict. 
 With corrected instructions in hand, it didnt take long for the jury to tell the judge that it was hopelessly deadlocked. A mistrial was declared. 
 Defense lawyer Alton Peterson called the events very unusual. 
 Ive never seen that happen before, said Peterson, who has 19 years of experience. 
 Chambers stepmother, Debbie Chambers, questioned how the jury could have deadlocked in such a manner. She said the victims father, Ben, was very emotional. 
 Hes not good, Debbie Chambers said. 
 District Attorney John Champion said he will retry Tellis. 
 After the verdict was read, Tellis smiled slightly while speaking with his other attorney, Darla Palmer. 
 I characterize it as a victory, Palmer said. 
 Tellis mother, Becky Tellis, hugged friends and family who cheered outside the courthouse. Some relatives wore T-shirts saying, Its a family thang and #JusticeForQuinton. 
 Prosecutors didnt present a clear motivation for Chambers killing. They used cellphone location data and video surveillance footage from a store across the street from Tellis home in an attempt to prove he was with Chambers the night she was burned. 
 Firefighters testified that Chambers told them someone named Eric or Derek set her on fire. Palmer said Tellis was falsely accused because Chambers said someone elses name. 
 Champion said Chambers throat and mouth were severely damaged. 
 Maybe she was trying to say Tellis, he said in closing arguments. 
 Investigators said about 10 to 15 people named Eric or Derek were questioned and cleared. A doctor testified Chambers had so much damage to her mouth, throat and chest that she would have been unable to properly say and pronounce words. 
 Related: Firefighters Say Burned Woman Said Eric Set Her on Fire 
 Early in the investigation, Tellis told investigators he had only seen Chambers during the morning on the day she died. In a January 2016 interrogation, he acknowledged he was with her that night. 
 Investigators showed the jury a video recording of a vehicle appearing to be Tellis sisters, stopping at Tellis house at 7:50 p.m. and staying for about two minutes before heading toward the crime scene. 
 Tellis told investigators he kept a container of gasoline in a shed at his house. Prosecutors said they believe he was driving his sisters vehicle when he picked up the gas from his shed before setting Chambers on fire. 
 A smoldering Chambers was found on the back road by a passing motorist shortly after 8 p.m. 
 Tellis faces another murder indictment in Louisiana, where hes accused in the torture death of a 34-year-old Taiwanese graduate student at the University of Louisiana at Monroe. No trial date has been set in that case. 
