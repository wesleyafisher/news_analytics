__HTTP_STATUS_CODE
200
__TITLE
Iraqis Mourn Destruction of Ancient City of Nimrud: ISIS Tried to Destroy the Identity of Iraq
__AUTHORS
Lucy Kafanov
__TIMESTAMP
Dec 11 2016, 7:11 pm ET
__ARTICLE
 NIMRUD, Iraq  When ISIS swept into Mosul two years ago, Leila Salih begged the militants not to destroy the Mosul Museum, where she worked, or the archaeological site at Nimrud, which she helped oversee, just south of the city. 
 "I told them we would destroy the graves ourselves if they just left the buildings standing, she told NBC News. "I begged them to save Iraqs history." 
 But the pleas fell on deaf ears. Several videos released by the militants last year show ISIS fighters using sledgehammers, power tools, and bulldozers to demolish priceless sculptures and stone carvings. What they didnt destroy with explosives they tore down by hand. 
 Built three thousand years ago  and forgotten for centuries  the ancient city of Nimrud was the second capital of the Assyrian empire, which at its height extended to modern-day Egypt, Turkey and Iran. 
 Related: National Museum of Iraq Director Discusses ISIS Destruction of Relics 
 Archaeologists first began excavating Nimrud in the 1840s, finding the remnants of ancient palaces, sculptures, and cuneiform tablets  some of the earliest examples of writing known to man. The UNESCO heritage site was considered one of the most important archaeological finds in the world. Most famous for its colossal Lamassu sculptures  hulking winged mythical beasts with a human face, the body of a bull and the wings of an eagle. 
 The site was leveled by ISIS last year but retaken by Iraqi forces last month. Today, the ancient city lies in ruins. 
 Salih tears up thinking about Nimruds destruction  60 percent of whats been excavated now gone, according to her estimate. Seeing Nimrud as a schoolgirl inspired her to become an archaeologist; many of her colleagues are still trapped inside the city. 
 "It is a tragic thing  our culture, our history, our memories," she said, wiping her eyes. "They tried to destroy the identity of Iraq. The only thing we can do now is document the damage so we can start thinking of how to rebuild again." 
 A recent visit to Nimrud revealed the devastating scale of the destruction. Just the archway entrance to famed Northwest Palace, built by the Assyrian King Ashurnasirpal II, remains, surrounded by piles of rubble. The fragments of its former winged guardians lie in a heap nearby. 
 Related: ISIS Is Erasing Iraqi History by Destroying Antiquities, Officials Warn 
 Although ISIS fighters have been pushed out, the site lies unprotected. Sheikh Khalid al-Jabbouri, a local tribal commander from Nimrud, said he wept when he first saw how little remained of the ancient city that generations of his family used to take care of. 
 "I wasn't as devastated when they destroyed my house or when they killed some of my relatives because this is life  all of us die," he said, surveying the damage. "But Nimrud was like a part of our family. This heritage was part of our lives, part of all of Iraq." 
 Iraq has lost thousands of precious objects from its national museum and other archaeological sites following the U.S.-led invasion of the country in 2003. And Salih said that Nimrud needs immediate protection before archaeologists can fully assess the damage. She also hopes that the militants featured in the ISIS video would be found and prosecuted for war crimes, similar to what happened in Mali. 
 While the ancient palaces and pyramid are gone for good, Salih said there is hope yet for Nimrud. Only 30 percent of the site has been excavated. 
 Related: UNESCO's Irina Bokova Laments ISIS' 'Cultural Cleansing' of Antiquities 
 There are still many undiscovered priceless objects buried under the ground, she said. "Lets see what we can discover over the next few years. 
 ISIS tried to erase Iraqs history, but in his tiny makeshift studio on the outskirts of Erbil, one teenager is fighting back  with art. 
 An Assyrian Christian whose family was driven from their home by ISIS two years ago, 17-year-old Ninous Thabit considers Nimrud to be the work of his ancestors. Over the past year, hes been sculpting intricate replicas of the priceless objects destroyed by ISIS. 
 Related: ISIS Damages Iraq's Hatra Archaeological Site in Purported Video 
 He said he made nearly 40 pieces over the past year  including replicas of the famed "lamassu" statues -- winged bulls with human heads. It's painstaking work, but Thabit said he feels a duty to protect his country's heritage. 
 "It is very hard to accept the destruction of a civilization that was built thousands of years ago so started sculpting Assyrian figures, especially those are connected to Nimrud to fight back," Thabit said. "I wanted to let ISIS knows that we are people who love life, we love our civilization, and we will protect it in the future." 
