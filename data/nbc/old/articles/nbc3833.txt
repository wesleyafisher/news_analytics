__HTTP_STATUS_CODE
200
__TITLE
Families Salvage Treasures in the Ashes of Their Homes
__AUTHORS
NBC News
__TIMESTAMP
Oct 17 2017, 6:00 pm ET
__ARTICLE
Heather Tiffee wipes her eyes as she looks through the remains of her parents' home after it was destroyed by the Atlas Fire on Oct. 13, 2017 in Napa, California.
The deadliest wildfires in California history have been burning for more than a week, killing at least 41 people and destroying nearly 6,000 homes. About 34,000 people remained under evacuation Tuesday, down from 40,000 on Monday.
Ben Pederson finds a school yearbook in the remains of his bedroom after his family's home was destroyed in Santa Rosa, on Oct. 11.
In the states wine-making region, tens of thousands of people began drifting back to their neighborhoods. Some returned to find their homes gone.
Charred vehicles are seen next to a wildfire-ravaged home on Oct. 14, in Santa Rosa.
Benjamin Lasker, 16, pauses while looking at what remains of his home on Oct. 15, 2017, in Santa Rosa.
With the winds dying down, fire officials said Sunday they have apparently "turned a corner" against the wildfires that have devastated California wine country and other parts of the state over the past week, and thousands of people got the all-clear to return home.
Lilly Ham, her husband Ted and their son Alex find pottery in the rubble of Lilly and Ted's home in Santa Rosa, on Oct. 15.
Flowers mark the spot where Mike Rippey's father died, as he walks through the ruins of the house near Napa on Oct. 12. Charles, 100, and Sara Rippey, 98, were killed in the Atlas Fire just after it began.
Howard Lasker, right, comforts his daughter, Gabrielle, who is visiting their home for the first time since the wildfire swept through, on Oct. 15 in Santa Rosa.
Chimneys are all that remain standing amidst a swath of burned out properties in Santa Rosa on Oct. 12.
A firefighter hands a safe to Julian and Lisa Corwin in the Fountaingrove neighborhood of Santa Rosa on Oct. 13. Corwin is part of a community email list and as a result of gaining entry to the area was taking requests from friends and neighbors looking for treasured items.
PHOTOS:From Above, California Wine Town an Ashy Wasteland
Firefighter Terry Sanders and his son Isaac, 11, who lost their home in a wildfire, comfort each other outside of their neighbor's home in Santa Rosa on Oct. 14.
Nicole Green takes pictures of the neighborhood in Santa Rosa on Oct. 14.
Ed Curzon sifts debris to salvage anything he can from the rubble of his home that was destroyed by a wildfire in the Coffey Park neighborhood of Santa Rosa on Oct. 15. "This is our home. This is where we grew up. This is where our kids grew up," said Curzon. "We will rebuild here."
Tammy Christiansen holds her wedding ring and her son's wrestling trophy that she found after searching the remains of her burned home in Santa Rosa's Coffey Park neighborhood on Oct. 11.
Charred property is seen before a home that was untouched by the fire in Santa Rosa on Oct. 15.
A set of salvaged china that Karen Curzon, background center, inherited from her grandmother lies on the foundation of her home in the Santa Rosa's Coffey Park neighborhood on Oct. 15. "We are going to rebound, rebuild and get this community back," said Curzon.
Firefighters walk throughthe Fountaingrove neighborhood of Santa Rosa on Oct. 13.
Residents embrace after viewing their destroyed home in Santa Rosa on Oct. 10.
An American flag hangs from a tree in a neighborhood destroyed by wildfire in Santa Rosa on Oct. 12.
PHOTOS:Massive Wildfires Consume Homes Across Northern California
