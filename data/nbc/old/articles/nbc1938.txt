__HTTP_STATUS_CODE
200
__TITLE
Secret Witness Testifies Robert Dursts Wife Feared The Jinx Subject
__AUTHORS
Andrew Blankstein
Robert Dean
Tracy Connor
__TIMESTAMP
Feb 15 2017, 8:58 pm ET
__ARTICLE
 The so-called secret witness in the Robert Durst case was revealed Wednesday to be an advertising executive who was introduced to him through a mutual friend  the woman Durst is accused of murdering. 
 Nick Chavin, 72, took the witness stand at a pretrial hearing and testified that he had met "Bobby" more than three decades ago through his pal, writer Susan Berman, and became such good friends that Durst was the co-best man at his wedding. 
 Durst, 73, a scion of a massive real estate empire, is charged with murdering Berman in Los Angeles in 2000. Prosecutors claim that he was worried that she would implicate him in the death of his first wife, Kathie, who vanished in 1982. Durst denies killing either woman. 
 Chavin  who told the New York Times in 2001 that "Bobby didn't kill Kathie"  testified Wednesday that Kathie told him that she was physically afraid of her husband. 
 Durst, meanwhile, described Kathie as "impossible," Chavin testified. 
 "It was always a strange relationship," he said, adding that Durst said he had an open marriage and would go to bars with Chavin hoping to meet women. 
 Related: Durst Hearing Focuses on Mystery of 1982 Phone Call 
 Chavin also testified about Durst's close relationship with Berman, saying there was nothing she would not have done for him. 
 Los Angeles County Deputy District Attorney John Lewin asked Chavin whether he ever discussed with Berman whether Durst might have been involved in his wife's disappearance, but the defense objected to the question. 
 Chavin, who arrived in court with two armed security guards, is back on the stand Thursday for what's expected to be the final day of the hearing. Prosecutors requested the hearing to get early testimony from several witnesses who they said were in danger of falling ill or dying or were scared that Durst could harm them  even though he is serving a seven-year sentence on a gun possession charge. 
 Chavin's identity was not revealed until he took the stand. 
 Earlier, the judge heard from a woman who worked for Chavin, met Durst through him in 2002 and became what she called his "closest friend." 
 Susan Giordano testified that she visited Durst in Texas while he was jailed for the 2001 death and dismemberment of his elderly neighbor, Morris Black. 
 She also said that, over time, Durst gave her $350,000 in gifts and loans  of which she repaid $2,000  and that at one point she discussed with Durst and his lawyers the possibility of getting money from him for the rest of her life. 
 She said that although she and Durst had a platonic relationship, they talked about having a "love nest," and she said she wanted to spend the rest of her life with him. 
 Related: The Millionaire Drifter Who Can't Run From His Past 
 Giordano was called to the stand because she had stored boxes full of Durst's personal papers  and allowed producers of the HBO program "The Jinx" to go through them before they were seized by police in 2015. 
 The defense claims that the contents of the boxes were privileged and should not be used as evidence at Durst's trial, but prosecutors contend that Durst waived that privilege when he gave "The Jinx" producers access. 
 "The Jinx" examined Durst's ties to his wife's disappearance and Berman's death, as well as the 2001 death and dismemberment of Black. The series ended with Durst blurting out on a hot microphone that he "killed them all." 
