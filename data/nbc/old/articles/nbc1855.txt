__HTTP_STATUS_CODE
200
__TITLE
#SOSHouston: How Apps and Social Media Assist Harvey Rescue Efforts
__AUTHORS
Chelsea Bailey
__TIMESTAMP
Aug 31 2017, 7:07 pm ET
__ARTICLE
 As daylight broke in Houston Sunday, the Twitter hashtags #SOSHouston and #SOSHarvey spiked, filling social media with rescue requests from people trapped by floodwaters. 
 As FEMA and the National Guard mobilized the government's rescue response, local first responders and social media mavens swung into action, launching a mobile rescue command center using the apps on their mobile phones. 
 Remote volunteers created a Google spreadsheet to log rescue requests tagged with the trending hashtags. While on the app Zello, first responders on the ground radioed in their locations and dispatchers coordinated rescues. 
 Soon the app, which essentially turns a cell phone into a mobile CB radio, had separate channels targeting specific areas of the city. 
 Two quick beeps alerted dispatchers to an incoming rescue request, which is logged by location and incident number into an online database. Dispatchers then coordinate boats, trucks and supplies to the location and later verify that the evacuees have been safely rescued. 
 Its instant. Its thorough  and its saving lives. 
 Volunteer rescuer Charles Garfinkel, 51, said he used Zello to help rescue a 92-year-old woman and her relatives from their flooded home in West Houston. 
 After flood waters destroyed his ranch house along the Brazos River, Garfinkel said he packed his small boat  which he usually uses to go fossil hunting with his daughter  into a friends truck and headed out into the waters to help neighbors. 
 \ 
 They initially planned to help out his friends parents who were stranded in their home. But Garfinkel said they wound up staying in the area and shuttling victims to higher ground for nearly two days.  
 The thing is, once youre out there and youre helping someone  theres always someone else to help, he said, adding that apps like Zello and Waze were essential to helping him locate and rescue families. 
 I was amazed at the fact that we were able to do this and didnt have to go through a FEMA coordinator or anything. 
 Related: How Forecasters Nailed Harvey's Massive Rain Dump 
 Zello CEO Bill Moore said the company is proud to be a part of the rescue efforts. The Austin, Texas-based company has been around since 2012, but Moore said the app has more frequently been used for emergency situations in countries like Venezuela and Turkey. 
 Last August, the "Cajun Navy" began using the app to coordinate their response to the massive floods in Baton Rouge and parts of Louisiana, that saw 20,000 people rescued from their flooded homes. 
 Since then, Moore said the community search and rescue team has perfected their use of the app to respond to disasters. 
 "We know that it's really helpful when the stakes are high," he said. 
 Garfinkel said he thinks the community rescue efforts exemplify Texas brazen lone star spirit. 
 We will take care of our own, he said. I think theres still a bit of a streak that the federal government is necessary, but its not a requirement. 
 When the rubber meets the road, theyll get out there and do whatever is necessary. 
