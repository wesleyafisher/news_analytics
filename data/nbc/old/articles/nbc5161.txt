__HTTP_STATUS_CODE
200
__TITLE
What Is Gray Death? The Killer Drug Cocktail Is Latest Battle in War Against Opioids
__AUTHORS
Corky Siemaszko
__TIMESTAMP
May 5 2017, 4:35 pm ET
__ARTICLE
 It looks like a chunk of concrete, can kill with one dose, and its got an ominous name  Gray Death. 
 And its the latest killer drug cocktail making headlines in the ongoing war against the national opioid crisis. 
 So far, its been limited to the Gulf Coast and states like Georgia and Ohio and we are monitoring the potential spread of this deadly combination of drugs, Russ Baer of the federal Drug Enforcement Agency told NBC News. 
 Weve not yet seen a national proliferation of the gray death substance, the DEA spokesman wrote. 
 But even as law enforcement is focusing on Gray Death, drug dealers are hard at work on even more lethal drug cocktails made from opioids that are smuggled into the country from Mexico or shipped in by mail from China. 
 Its mad science and the guinea pigs are the American public, Baer said. The ingredients come from abroad but this is made in America. 
 What is Gray Death? Start with heroin. Mix in the powerful painkiller fentanyl, which has 50 times more punch. Add a dash of carfentanil, which is an animal tranquilizer 100 times more powerful than fentanyl and made to be used on tigers and elephants. 
 Top it all off with a synthetic opioid called U-47700, better known on the street as Pink or U4. 
 "Pink by itself has been blamed for at least 46 deaths in 2015 and 2016 in New York, New Hampshire, Ohio, Texas, and Wisconsin, North Carolina, according to the DEA. 
 When added to Gray Death, it creates as lethal a combo as any out there, according to Baer. 
 We are more routinely seeing deadly cocktails of heroin, fentanyl, various fentanyl-class substances, along with combinations of other controlled substances of varying potencies including cocaine, methamphetamine, and THC, Baer said. 
 THC is Tetrahydrocannabinol, which is the chemical compound in marijuana that produces the high. 
 No one should underestimate the deadly nature associated with these cocktails, Baer added. You can buy one of these cocktails for $10-$20 on the street and lose your life in a few seconds.  
 A Pink-free precursor to Gray Death was first spotted back in 2012 in the Atlanta area, Baer said. 
 Later, this gravel-like heroin was spotted in places like Chicago, Cincinnati, San Diego, Merrillville, Indiana and Lexington, Kentucky, he said. 
