__HTTP_STATUS_CODE
200
__TITLE
How to Survive When You're Stuck in a Toxic Work Environment
__AUTHORS
Kim Bainbridge
__TIMESTAMP
Oct 13 2017, 10:35 am ET
__ARTICLE
 Toxic work environments sap our productivity and hurt our businesses  yet we've all found ourselves in that situation at some time or other. We spend so much of our lives at work that it's incredibly important to be comfortable there. So, here are five ways to clean up. 
 We know its not always possible for every manager in your company to keep an open door policy. However, you should really try to encourage open dialogue between your companys leadership and its staff. 
 Your employees are more likely to report problems if their managers are approachable and regularly speak with them one-on-one. 
 The workplace is no place for bullies. If you discover that one of your employees is degrading the others or causing their work to suffer, take immediate action. 
 Fire them, says Cindy Whitehead, CEO of The Pink Ceiling. I did it. It was my number one salesperson in the company. And youre never going to live on one persons contribution alone. And it created paralysis of everybody around him. And he was so toxic to the culture that nobody else was performing. 
 Related: These Leaders Get Real About Dealing with Toxic Employees 
 Stress can bring out the worst in people, but it is important to remember that your employees are human. In fact, Whitehead says that a little competition might be just the thing you need in order to humanize a particularly toxic employee. Why, you ask? 
 [By] knocking them  not knocking them down in a malicious way, but sort of humanizing them for everybody else, says Whitehead. My feeling is that if theres toxicity, theres a lack of humanity particularly if theyre a star contributor. 
 Related: How This Company Built a Billion-Dollar Brand by Treating Employees Like Family 
 Keep in mind, however, that while you want to challenge your employees so that they learn and grow, you must also give them enough room to make mistakes, and reward them publicly for good work. 
 Samantha Skey of SheKnows Media has been leading and developing diverse teams for years. She knows a thing or two about weeding out toxic employees and getting people up to snuff. 
 We go through a wave of thinking we know everything before we learn we dont know anything, says Skey. She suggests that age, experience, and maturity may cause an employee to be seemingly, though unwittingly, toxic. 
 To fix this, managers should work with their HR teams to develop solid workplace policies and to implement rock solid training programs. 
 Burnout is very real. Even the best worker needs to recharge. Provide your employees with ample time to relax and take care of themselves and their families. 
