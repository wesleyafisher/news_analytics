__HTTP_STATUS_CODE
200
__TITLE
Filmmakers Behind Marshall Talk Movie Renaissance
__AUTHORS
__TIMESTAMP
Oct 11 2017, 4:59 pm ET
__ARTICLE
 If you ask the filmmakers behind the new Thurgood Marshall biopic, Marshall, there was a time when it felt like African American cinema was still a fragile idea, and that it might not evolve into a dependable part of the cultural landscape. Not anymore. 
 Chadwick Boseman, who plays a young Thurgood Marshall in the film, said that, three or four year ago, he feared the industry would realize Okay, they had their little shot and now its over. Now he refers to the genres renaissance. 
 Appetite for serious films focused on the black experience is growing. In 2013 there was 12 Years a Slave. 2014 brought and Selma. Last year, Moonlight, Hidden Figures, Fences, and Loving hit theatres  each of them Oscar contenders. This year, Jordan Peeles race/thriller Get Out is generating Oscar buzz. 
 In the latest 1947: The Meet the Press Podcast, Director Reggie Hudlin and Boseman joined Chuck to talk about Marshall, a legal thriller set in the 1940s that Hudlin calls tragically relevant. 
 The film focuses on a lesser-known chapter in the career of the future Supreme Court Justice. Instead of focusing on the landmark Brown v. Board of Education decision, Marshall tells the story of a rape case from the lawyers early career. 
 People go, Oh yeah, Brown v. Board of Education, I learned about that in fifth grade. Ill catch it on cable, he said. Instead, the movie they made presents a dramatic, lesser-known trial, Connecticut v. Joseph Spell. 
 Another way the film tries to avoid audience expectations: focusing on a case in New England. 
 There is a layer of gentility over Northern racism, Hudlin said. Tragically, it feels more like how contemporary racism and prejudice works. 
 Before his career as a director, Hudlin served as the president of BET, and produced the 2016 Oscars  better known as the year of the #OscarsSoWhite controversy. Boseman played Jackie Robinson in 42 and James Brown in Get On Up. 
 The duo sees their work as part of a trend in cinema in which audience of all races embrace African American stories. 
 Black film or black TV is no different from eating Chinese food, Hudlin said. He added that the mainstreaming of black culture started with hip-hop and is now accelerating on screen. People just want the cool thing. 
