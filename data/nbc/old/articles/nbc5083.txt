__HTTP_STATUS_CODE
200
__TITLE
Sec. Carter: ISIS Could Seek Revenge for Offensives in Raqqa, Mosul
__AUTHORS
Hans Nichols
__TIMESTAMP
Oct 25 2016, 1:40 pm ET
__ARTICLE
 Paris  U.S. Defense Secretary Ash Carter said Tuesday that the planned offensive against the Islamic State's capital of Raqqa, in Syria, would overlap with the ongoing assault in Mosul, while warning that the terrorist group would feel cornered and seek revenge against targets in Europe or America. 
 "In fact we have already begun laying the ground work to commence the isolation in Raqqa," Carter said Tuesday during a joint press conference in Paris with his French counterpart Jean-Yves Le Drian. "They are working to generate the local forces." 
 "Protecting our homelands in another critical campaign objective," Carter said. 
 Related: Defense Secretary Ash Carter Heads Closer to Front Line in Iraq 
 Carter is in Paris, along with twelve other defense ministers, as they coordinate with their domestic security agencies to prepare for a potential attacks in Europe, or elsewhere in the west, from the Islamic State . 
 As ISIS loses territory in the Middle East and its leaders become more concentrated in urban strongholds, U.S. officials insist that they will be easier to kill, while warning that the terrorist group may seek to stage counter attacks outside of the region. 
 Related: Mosul: Who Are Key Players Trying to Recapture ISIS Stronghold? 
 "The threat is there," Le Drian said. "It will be there after the taking of Mosul but in different conditions." 
 "We have put our joint special operations command in the lead of counter ISILs external operations," Carter said. "We shared best practices and ideas on how each of our countries can improve." 
 The Paris gathering occurred while Iraqi and Kurdish forces continued their march on the center of Mosul, as the campaign to liberate Iraq's second largest city from ISIS continues in its second week. 
 Related: Defense Secretary Ash Carter Makes Unannounced Trip to Iraq 
 After spending two separate days in Iraq meeting with Iraqi and Kurdish officials as well as U.S. generals and troops, Carter has praised the coordination between NATO members and Iraqi and Kurdish forces, insisting that the battle plan was unfolding on schedule. But both he and Lt. Gen. Stephen Townsend, the commander of NATO forces in Iraq have cautioned that more difficult fighting lies ahead. 
 Earlier today, Carter claimed that the coalition had killed 35 ISIS commanders in Mosul in the last 90 days. 
 "The most dangerous job in Iraq right now is to be the military emir of Mosul," he said. "The taking of Mosul will have a major effect on (ISIS) in terms of resources....management and leadership." 
 PHOTOS: Iraqi Families Flee Mosul as Army Battles ISIS 
