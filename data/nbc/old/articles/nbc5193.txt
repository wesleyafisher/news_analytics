__HTTP_STATUS_CODE
200
__TITLE
Infographic: The Startling Rise in Heroin Use
__AUTHORS
Janet Klein
Ronnie Polidoro
__TIMESTAMP
Apr 7 2014, 8:22 am ET
__ARTICLE
 Attorney General Eric Holder recently called heroin use in this country an "urgent public health crisis," citing that between 2006 and 2010, heroin overdose deaths have increased by 45-percent. 
 Vermont is one of the worst hit states in the country, where hundreds of heroin users are on waiting lists for treatment. Dr. Dean McKenzie, an addiction specialist in Rutland, Vt., explains that heroin is a very difficult drug to quit, "after a while, you're not doing it to get high. You're doing it to survive. You're doing it so you don't get sick. I mean, the withdrawal is incredibly painful." 
 Experts believe the increase in heroin use, in part, is simple economics. As the nation cracked down on prescription drug abuse, pain pills became harder to come by - driving up price. "Heroin was out there and was cheaper, "says McKenzie, "and so people made the switch to heroin." 
 NBC's National Correspondent Kate Snow will have more on the startling climb of heroin use tonight on Nightly News with Brian Williams. 
