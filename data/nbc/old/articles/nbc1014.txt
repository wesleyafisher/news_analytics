__HTTP_STATUS_CODE
200
__TITLE
One Big Reason Millennials Are Buying Homes? For Their Dogs
__AUTHORS
Nicole Spector
__TIMESTAMP
Aug 9 2017, 11:28 am ET
__ARTICLE
 Millennials are now leading the pack of home buyers, and whats one incentive driving them to take the mortgage plunge? Their dogs. 
 A recent survey conducted by Harris Poll on behalf of SunTrust Mortgage found that 33 percent of millennial home buyers decision to buy a home was driven chiefly by their dog. Furry friends outranked wedding bells (25 percent cited marriage as their top motivator for buying a home) and kids, too (only 19 percent said birth of a child was their prime incentive). 
 And even those millennials who dont yet own a home but are planning to are prioritizing pups, with 42 percent of those surveyed by SunTrust saying a dog  present or future  is a key factor in their home purchasing decisions. 
 Certainly, this was true for 25-year-old Gwen Werner and her husband. They just bought a house so they could get a dog and not feel guilty about the pup being cooped up in an apartment all day. 
 "It felt inhumane having a dog live in a third-floor apartment without any space to run around," said Werner. "I'm glad that was our route, as we have a dog who has way too much energy for an apartment setting." 
 Werner adds that she and her husband bought their house in May. They rescued their pup, a German Shepherd mix in June. 
 The desire to give one's dog the best life possible is one that real estate brokers see frequently among their millennial clientele. 
 "All the time they mention their dogs," said Lee Fowler, a real estate agent with Coldwell Banker Triad in Winston-Salem, North Carolina, adding that just under half of his clients are millennials. "A lot of times they'll go into the house, through the kitchen, and then walk right into backyard and say 'This will work perfectly for my dog.' Or they'll look to see whether there's a fence, or if they can make one for their dog." 
 Sure, yards are great, but what about a dog-washing station? Millennials may also be on the hunt for a home that has deluxe amenities for their dog. 
 Our sales center has found that millennials are particularly attracted to dog-related amenities, said Marilyn Osborn Patterson, marketing director and legal counsel for Norton Commons in Louisville, Kentucky, which she says has about 1200 residences. "Demand has been really strong, so we just completed our third dog park. All of them include entry vestibules for safety, fresh running water, and seating for pet owners. Millennials favor parks and walking trails and an active lifestyle alongside their dogs, so we added pet waste stations to keep things looking good. In new home builds, we see a lot of people putting in dog-washing stations. 
 It could sound a bit over the top. But it's actually not that ridiculous when you consider that millennials tend to regard their pets as family members  arguably more so than any generation prior. 
 Related: Social Media Users Post About Their Dogs Six Times a Week 
 "Millennials have grown up in a different world than boomers and Gen-Xers, and it has impacted the way they see dogs," said Laura Schenone, author of The Dogs of Avalon: The Race to Save Animals in Peril. "For one thing, this generation is more educated than any before: 27 percent of millennial women have a bachelor's degree, compared with 14 percent of boomers and 20 percent of Gen-Xers. There is research to show that the college educated are more aware of the environment and the natural world, which includes animals." 
 Perhaps it's the popularity of animal shows like The Dog Whisperer, or just being able to access so much information on the web  whatever it is, millennials have evolved past boomer thinking about what makes Fido a good boy. 
 "Back in the '70s, my parents thought they were being good and responsible [dog owners by using] punishment and choke collars," said Schenone. "Now, as behavioral science has advanced, we know that dogs respond better toward positive training and rewards-based punishment." 
 And while more and more millennials are becoming parents, there are still quite a few who are waiting to have kids, or who have decided not to have children. And not even the fanciest digs for your dog come close to the cost of bringing up baby: A dog is flat out cheaper than a kid. 
 "Some millennials say they are having dogs [instead] of children," said Schenone. "That's a leap, but not hard to believe; after all, they are less well off than boomers and Gen-Xers were at their age, and more burdened by student loans and debt. Everybody needs love and a family: dogs are cheaper, easier, and provide love." 
 Everybody needs love and also, everybody needs a place to live. Anyone with a dog who has rented knows that the odds are stacked against them. Property managers tend to lump on fee after fee, along with tons of restrictions, if theyre even so generous as to allow a dog. The millennial who is ready to buy a home has possibly become sick and tired by the pricey rigmarole that goes along with renting with a dog. They want a place to hang their hat  and their pup's leash  without a big fuss. 
 But buyer beware: If you're purchasing a condo or an apartment, Home Ownership Associations can present similar obstacles for your pooch. 
 "Most HOAs will allow dogs but have weight limits (around 25 pounds), which excludes many breeds," said Jeffrey A. Hensel, a real estate agent and the sales and marketing director at North Coast Financial. 
 A millennial and a devout dog owner, Hensel says it took him a year to find his current condo in San Diego  all because of the dog requirement. 
 Related: Five Millennial Jobs That Parents Will Never Understand 
 Prospective home buyers with pups should certainly check to see if an HOA is involved, and what it requires. Beyond that, buyers must also disclose to their home insurance agent information about their dog. 
 Lovable as your mutt is, it could be considered high-risk based solely on its breed. 
 "I recommend being open and honest with your insurance company," said Ava Lynch, a licensed insurance agent who works at The Zebra, and a volunteer for Austin Pets Alive. "A lot of times, your insurance company won't deny you coverage because of the dog you own; they might just charge you a little extra for what they see as an increased risk (i.e., your "dangerous" dog), or they will simply not cover your dog in your liability portion. But if you lie and you need to file a dog bite claim, you risk being denied coverage and forced to cover any damages your dog causes out-of-pocket." 
 Looks like not even owning a home can eradicate all the annoying fine print and fees of having a dog. It's a dog eat dog world, indeed. 
