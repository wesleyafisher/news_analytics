__HTTP_STATUS_CODE
200
__TITLE
Body of Missing Illinois Woman Emily Dull Found in Submerged Car
__AUTHORS
Bianca Hillier 
__TIMESTAMP
Oct 12 2017, 9:32 am ET
__ARTICLE
 The search for missing Illinois woman Emily Dull has ended. The Winnebago County coroner announced Wednesday that a body found inside a car in a nearby river is that of the missing 25-year-old. 
 According to a press release from the Loves Park Police Department, the Winnebago County Sheriffs Department received a 911 call Sunday afternoon from a resident reporting a car antennae protruding from Rock River. Crews were then dispatched to the scene. 
 During the search, [Harlem Roscoe Fire Department] divers found a vehicle submerged in the river, the press release states. After discovering that the vehicle was the same vehicle that has been driven by a missing Loves Park resident, Emily [Dull], at the time of her disappearance, Loves Park Police Department was notified and Detectives responded to the scene. 
 While the cause of death has yet to be determined, Winnebago County Coroner Bill Hintz told NBC affiliate WMTV that dental records positively identified the body as Emily. He believed the body had been in the river for "some time." 
 Hintz said he believes Emily was in the driver seat at the time of the accident, but floated to the passenger seat over time, adding that there was no sign of struggle. 
 Emily went missing in the early hours of June 19, 2017 after getting off work and going to bars with friends. 
 At the time of Emilys disappearance, Loves Park Police Department Sgt. Michael Landman told Dateline they were using every tool available to locate Emily and her vehicle. During the course of the investigation, according to Mondays press release following the discovery, those tools included aerial and sonar searches of 11 nearby waterways, including Rock River, where Emily was eventually found. 
 Emilys sister, Cait Dull, has acted as the family spokesperson since Emily first went missing. In July 2017, Cait told Dateline everyone who knows our family knows this isn't typical. Emily wouldn't just leave." 
 Now, via a Facebook post, Cait says the family is devastated and broken; we will not let this break us. We will remember the good times to get us through these rough times. We still do not have many details and there are a lot of unanswered questions we have for the coroner and the police. 
 Despite the unimaginable pain, Cait says the family is keeping one thing in mind. 
 Emily has always said she was jealous of us older family members because we got more time with our deceased mother, Cait wrote on Facebook. We are taking comfort that they are reunited somewhere together and watching over us. 
 A memorial now rests near the area where Emily was found. 
 Emily was featured in Dateline's Missing in America series shortly after she disappeared. 
