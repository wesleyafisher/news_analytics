__HTTP_STATUS_CODE
200
__TITLE
Obamacare Architect: Healthcare Laws Mistakes are Still Fixable
__AUTHORS
Matt Rivera
__TIMESTAMP
Jun 20 2017, 5:14 pm ET
__ARTICLE
 As Senate Republicans craft their healthcare bill, the legacy of what did and didnt work in the Affordable Care Act casts a long shadow over healthcare policy discussions. 
 Dr. Ezekiel Emanuel helped craft the Affordable Care Act or Obamacare, and beginning with a meeting at Trump Tower in December, Emanuel has met with President Trump three times to discuss Obamacares effects on healthcare delivery. He's also the author of a new book which tackles healthcare reform, Prescription for the Future. 
 In the latest episode of 1947: The Meet the Press Podcast, Emanuel joined Chuck Todd to talk about how Obamacare might have been structured differently had anyone known that the Obama administration would be followed by the Trump administration. 
 On the consumer cost side, he points to a mistake that was made in crafting Obamacare. 
 In addition to the subsidies paid to people to buy insurance for themselves, Emanuel says that cost-sharing subsidies  money that goes to pay for deductibles and copays  werent given the same kinds of protections as the subsidies that pay for the insurance coverage. As a result, the cost-sharing subsidies are open to annual appropriations rather than built-in as a kind of entitlement. 
 In the years since the landmark law was passed, Republican opponents of the legislation have reduced the available funding for those subsidies, and the effect is that costs have risen for some families. 
 Emanuel believes that complaints about rising insurance premiums and decreasing coverage options could be fixed within the current legal framework. Some of the fixes are simple: enforce the mandate to buy insurance, target ads at young, healthy consumers, and bring back risk-corridors a kind of short-term insurance for insurance companies. 
 Taken together, Emanuel says, those things will stabilize the market and actually bring the premiums down. 
 Emanuel would also like to see the exchanges become a national market, instead of one thats organized state-by-state. 
 Partly we had it state by state because the Senate wanted it that way and we thought we might be able to get some Republicans to come aboard with state-by-state exchanges. In the end, no Republican Senators voted in favor of Obamacare. 
 In meetings held during the transition period, and later at the White House, Emanuel says that he has tried to explain Obamacare to Trump, and to convince him to make changes to the system instead of scrapping it and starting fresh. After all, healthcare is complicated. 
 Of his meetings with the president, Emanuel is both cheerful and pessimistic. Its quite clear [Trump] does not get the nuts and bolts. But lets be honest, very few Americans get the nuts and bolts. 
