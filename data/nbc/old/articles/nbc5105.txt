__HTTP_STATUS_CODE
200
__TITLE
ISIS Information Minister Dr. Wail, Killed in Airstrike in Syria: Pentagon
__AUTHORS
Jim Miklaszewski
Elisha Fieldstadt
__TIMESTAMP
Sep 16 2016, 3:50 pm ET
__ARTICLE
 The ISIS leader responsible for producing the militant group's often gruesome propaganda videos was killed in an airstrike earlier this month, the Pentagon announced Friday. 
 Wa'il Adil Hasan Salman al-Fayad, also known as "Dr. Wa'il," was targeted and killed by coalition forces near Raqqah, Syria, on Sept. 7, Pentagon spokesman Peter Cook said in a statement. 
 Cook said Wa'il was one of ISIS's "most senior leaders" as a "prominent member" of the terror group's leadership council. 
 Related: ISIS Says No. 2 Leader Abu Muhammad al-Adnani Is Dead in Syria 
 Wa'il was ISIS' official minister of information and "oversaw (ISIS's) production of terrorist propaganda videos showing torture and executions," the statement said. 
 The Pentagon also said Wa'il had been a close associate of Abu Muhammad al-Adnani, ISIS' second-in-command, who was killed in an airstrike on Aug. 30. 
 ISIS has not confirmed the death of Wa'il, according to global security firm and NBC News analyst Flashpoint Intelligence, which added that he was the mastermind behind featuring beheadings and executions in the group's propaganda films. 
 Wa'il's death is the latest in a series of successful attacks on senior ISIS leadership, specifically the groups original leadership," a senior U.S. intelligence official told NBC News. Along with Al-Adnani, two military officials were killed by U.S. drone strikes  the ISIS deputy minister of war, Basim Muhammad Ahmad Sultan al-Bajari, and a senior commander in Mosul, Iraq, Hatim Talib al-Hamduni. 
