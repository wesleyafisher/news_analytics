__HTTP_STATUS_CODE
200
__TITLE
Russia Probe: Senate Asks Mike Flynns Son for Documents, Testimony
__AUTHORS
Ken Dilanian
Carol E. Lee
Mike Memoli
__TIMESTAMP
Oct 17 2017, 6:38 am ET
__ARTICLE
 WASHINGTON  The Senate Intelligence Committee has requested documents and testimony from Michael G. Flynn, the son of former national security adviser Michael Flynn, but has not received a response, three sources familiar with the matter told NBC News. 
 The committee, which is investigating possible collusion between the Trump campaign and Russia, is interested in Flynns work as his fathers aide and travel companion with Flynn Intel Group, the consulting firm retired Lt. Gen. Michael Flynn formed after he left government service, the sources said. 
 Sen. Richard Burr of North Carolina, the intelligence committee chairman, and Sen. Mark Warner, of Virginia the ranking Democrat, declined to comment when asked about the matter Monday by NBC News. 
 Michael G. Flynns lawyer, Barry Coburn, declined to comment. 
 The younger Flynn, 34, accompanied his father on a 2015 trip to Moscow, where the elder Flynn sat next to Vladimir Putin at a dinner to celebrate Russias state-funded media network, RT. The younger Flynn can be seen in video from an associated event. 
 Ultimately, the committee could issue a subpoena to Flynn if he doesnt comply, but he could assert his right against self-incrimination under the Fifth Amendment to the U.S. Constitution. 
 NBC News reported last month that the younger Flynn is a subject of the criminal and counterintelligence investigation being conducted by Special Counsel Robert Mueller, who is also interested in Flynns work with his fathers consulting business. 
 Flynn responded on Twitter to the NBC News report, tweeting on Sept. 14: Im not the sub of any federal investigation. 
 The elder Flynn was fired as Trump's national security adviser in February after it became public that he had misled Vice President Mike Pence about his conversations with the Russian ambassador in Washington. 
You're still #fakenews and a wimp for blocking me...I'm not the sub of any federal investigation...idiots like u believe everything u read.. https://t.co/Urlrq6UHoA
 A former business associate of Michael Flynn's said the younger Flynn played an important role in the day-to-day operations of Flynn Intel Group and served as his father's chief of staff. 
 Those responsibilities included attending meetings with his father and communicating with prospective clients, the former business associate said. 
