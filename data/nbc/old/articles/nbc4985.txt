__HTTP_STATUS_CODE
200
__TITLE
Seouls K-Pop Dancers Party On, Despite North Korean Threat
__AUTHORS
Alexander Smith
__TIMESTAMP
Oct 14 2017, 10:21 am ET
__ARTICLE
 SEOUL, South Korea  Listening to the drums of war beating in Washington and Pyongyang, you might imagine that those people closest to the standoff gripping the Korean Peninsula would be hunkering down and fearing the worst. 
 Not in Seoul's pulsating Hongdae district, renowned as a hotbed for youth culture, arts and entertainment. Here, the only thing teens and young adults seem to care about is making sure their dance moves are on point. 
 At night, Hongdae's neon-painted streets are commandeered by street groups dancing to K-pop  South Korea's hyperactive, infectious brand of music. 
 Like many people across this high-tech city, the country's capital, the last thing on young minds is the heavily militarized border that's just 30 miles away. 
 "I don't think about Kim Jong Un and North Korea," says Yang Seung Ho, 22, the leader of a dance group that attracted a sizable crowd one night this week. 
 "We just like dancing. We are not worried," chimes in one of his fellow performers, Yu Seung Ho, 20. 
 K-pop can trace its modern roots to the 1990s but gained worldwide notoriety in 2012 with Psy's "Gangnam Style," the viral mega-hit whose nearly 3 billion views made it one of the most popular YouTube videos of all time. 
 This music industry is central to the increasing global popularity of South Korean culture, a phenomenon known as "hallyu"  or "Korean wave." 
 The government has invested billions in these industries, using hallyu as a tool of soft power, and boosting exports and foreign investments in everything from K-pop and K-drama to Korean food, cosmetics and fashion. 
 Often touted as one of the major economic success stories of the 20th century, South Korea is the world's 12th-largest economy despite its modest population of just over 50 million. 
 Hongdae has been popular with street musicians since the 1980s, but in recent years it has become a haunt for K-pop dance groups. 
 Despite some noise complaints from residents and businesses, the local government supports the street performers as long as they stop by 10 p.m. 
 Some groups that perform away from the main streets appear to ignore this curfew and carry on much later. 
 Yang and his colleagues carry many of the stylistic hallmarks of the genre  all asymmetrical haircuts, long crucifix earrings, tight jeans and brightly colored jackets. 
 They are meticulously well drilled. Most make their living by street performing almost every night. When NBC News met up with Yang this week, he made around $140 in just a few hours. 
 Competition is fierce. Another dance group set up its loudspeaker around 100 yards from Yang's troupe, and even closer were two guitar groups playing Radiohead and Coldplay covers. The performers were so close to each other that their music sometimes created a jumbled din.The scene is an all-out assault on the senses, the music accompanied by the the canopy of neon that overhangs the streets and narrow alleyways. Garish signs in Korean and English offer everything from sunglasses and beauty products to Korean barbecue and beer. 
 At the center of it all are the K-pop dancers. Crowds of mostly young girls, some of whom appear to have come straight from school, gather around and swoon at the young men, many of the onlookers copying the dance moves they've clearly practiced at home. 
 Others capture the scene on smartphones adorned with elaborate cases depicting cats and other cartoon creatures. 
 One of them is 19-year-old Byeon Hee Yeon, who also identifies herself by a Westernized name, Clara. She looks a little bemused when asked about the supposed threat from Kim and his thousands of artillery pieces at the border. 
 "Every day North Korea says, 'We will capture you,' but they don't," she says with a laugh. 
 Byeon says she feels protected by the U.S. military, whose nuclear umbrella, missile defense system and 28,500 troops stationed here are designed to ward off Pyongyang. 
 If North Korea attacks, she said, jabbing her fingers together to signify an imaginary conflict, "then the U.S.A. and China will come," 
 Rather than protecting South Korea, President Donald Trump has been accused by many experts and politicians of bringing the region closer to war by matching Kim's rhetorical threats. 
 The latest of these critics was Sen. Bob Corker, the Tennessee Republican who suggested in an interview with The New York Times on Sunday that Trump was putting the U.S. on the path to "World War III." 
 That worry doesn't seem to be shared by many in Seoul. 
 "We are just feeling safe in Korea," Byeon says, shortly before the dancers fire up their sound system for another barrage of high-energy K-pop. 
