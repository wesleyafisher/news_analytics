__HTTP_STATUS_CODE
200
__TITLE
Heroin Use Spikes Fivefold in U.S.
__AUTHORS
Maggie Fox
__TIMESTAMP
Mar 29 2017, 1:36 pm ET
__ARTICLE
 The number of people trying and abusing heroin has gone up by nearly five times in the U.S. since 2002, and white men are especially affected, researchers reported Wednesday. 
 It might be because it's more socially acceptable, they said. 
 Its no secret that America is going through a crisis of both heroin and opioid use. 
 Whats different about this study is it finds heroin use was not always linked to previous use of opioids  unlike previous studies that suggest that people get hooked on opioids prescribed for pain and then move to heroin because its cheaper. 
 Heroin use appears to have become more socially acceptable among suburban and rural white individuals, perhaps because its effects seem so similar to those of widely available prescription opioids, Dr. Silvia Martins of the Mailman School of Public Health at Columbia University and colleagues wrote in the Journal of the American Medical Associations JAMA Psychiatry. 
 Deaths from opioid drug overdoses have hit an all-time record in the U.S., rising 14 percent in just one year, and heroin deaths quadrupled between 2002 and 2013. 
 States hit hardest by the last recession have the most cases of abuse. 
 Related: America's Heroin Epidemic 
 President Donald Trump has set up a commission to investigate the crisis, with New Jersey Gov. Chris Christie serving as its chair. Former President Barack Obama asked for $1 billion last year to fight it. 
 Martins took a hard look at surveys of 79,000 people asked about heroin use between 2001 and 2013. 
 Overall, the percentage of people admitting to having used heroin rose from one-third of 1 percent (0.33 percent) in 2001-2002 to 1.61 percent in 2012-2013, they report. 
 Related: Spirit Airlines Pilot, Wife Found Dead by Children After Possible Overdose  
 This suggests that more than 3.8 million U.S. adults have tried heroin at least once, and 1.6 million have abused it, they said. 
 Increased availability and lower heroin prices in recent years may have contributed to increased heroin use, they wrote. 
 Dr. Bertha Madras, a psychiatrist at Harvard Medical School, said overuse of opioids is almost certainly to blame, also. 
 Related: Nearly 12,000 Kids Poisoned by Opioids 
 Physicians prescriptions for chronic noncancer pain rose three-fold and became the major source of opioids over the past two decades, she wrote in a commentary. 
 This shift in practice norms was fueled by an acceptance of low-quality evidence that opioids are a relatively benign remedy for managing chronic pain. 
 Nashs team also found that more men than women were using both heroin and drugs such as marijuana. That may be because of the economy, they speculated. 
 Related: Overdoses Spike in This Pennsylvania County 
 For example, men may have been more affected by economic stressors than women (eg, low manufacturing employment rates leading to greater increases in male heroin use), they wrote. 
 There are ways to fight heroin abuse. 
 Promising examples include expansion of access to medication-assisted treatment (including methadone, buprenorphine, or injectable naltrexone), educational programs in schools and community settings,overdose prevention training in concert with comprehensive naloxone distribution programs, and consistent use of prescription drug monitoring programs that implement best practices by prescribers, they added. 
 Efforts may be most efficient if concentrated in states acutely affected by the opioid epidemic, as noted in President Obamas Comprehensive Addiction Recovery Act signed in July 2016. 
