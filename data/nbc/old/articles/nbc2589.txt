__HTTP_STATUS_CODE
200
__TITLE
Obamacare Is Suddenly in Grave Danger. Heres Why.
__AUTHORS
Benjy Sarlin
__TIMESTAMP
Sep 19 2017, 6:18 am ET
__ARTICLE
 WASHINGTON  A last-ditch Republican effort to repeal Obamacare picked up steam on Monday as a key senator opened the door to supporting the bill, which is popularly known as Graham-Cassidy. 
 The GOP got a boost when Sen. John McCain, R-Ariz., who was one of three Republican "no" votes in July that derailed the last GOP health care effort, said he might "reluctantly" vote for the bill if his governor supported it. 
 Arizona Gov. Doug Ducey, a Republican, backed the legislation later that day. McCain has yet to take a solid position on the measure and has said he prefers a longer bipartisan approach. The Senate Finance Committee announced it would hold a hearing next week on the bill, which could help address his complaints about the rushed process. 
 Democrats and health care advocacy groups opposed to the legislation, which include AARP and the American Heart Association, are taking the latest Republican push very seriously. 
 Republicans lawmakers face a tight deadline to get it done: They can only pass the bill using budget reconciliation, which lets them bypass a Democratic filibuster, if they vote before September 30. 
 Heres what you need to know about what the new GOP health care bill does, where senators stand, and what would have to happen for it to pass. 
 In many ways, the bill is the most sweeping proposal yet. It would do away with Obamacares Medicaid expansion, subsidies for private insurance, eliminate the requirement that Americans have insurance under the Affordable Care Act, and payments to insurers to reduce out-of-pocket costs. 
 In their place, it would offer states a new block grant they could use to spend on health care mostly as they saw fit. But the block grant would include less total federal spending, meaning states would struggle to cover the same number of people. 
 How much states would get would depend on a new formula that would cut funding for some states and potentially raise it for others. It would slash the most from large blue states that spend more on health care, like New York and California, and redistribute it to poorer red states that spend less, like Texas and Alabama. Smaller states that rely on Obamacare's Medicaid funding, like Kentucky and Alaska, could be hit hard as well. 
 In effect, the bill would open up all 50 states to major health care changes depending on their approach. Some might loosen protections on pre-existing conditions by allowing insurers to charge sick patients more or drop requirements that insurers cover certain essential health benefits. States also would not be required to focus their spending on low-income residents, who were the largest beneficiaries of Obamacare. 
 Other features are common to Republican repeal-and-replace bills: Traditional Medicaid would face new per-person caps on spending and would likely grow at a slower rate than under current law. The bill would also repeal Obamacares requirements that individuals buy insurance and employers offer it. It would also cut off funding to Planned Parenthood. 
 There isn't a detailed estimate yet and the senators, if the vote on it, won't see one either. That's because the Congressional Budget Office, a nonpartisan agency that reviews legislation, announced on Monday that it would not be able to complete a full study by September 30. 
 When the CBO looked at previous GOP bills that scaled back Medicaid funding and imposed caps on future spending, however, it estimated major losses from those policies alone. The CBO also predicted that loosening protections on pre-existing conditions would cause some insurance markets to become unstable and significantly raise costs for certain medical treatments while lowering premiums. 
 Thats hard to answer. The Senate Republicans can only lose two members. 
 McCain's position is unclear and Sens. Susan Collins, R-Maine and Lisa Murkowski, R-Alaska, the other two Republican who voted with McCain against GOP "skinny repeal" health care bill, have been highly critical of Medicaid cuts. Collins and Murkowski also opposed defunding Planned Parenthood, which Graham-Cassidy would do as well. 
 Neither Murkowski nor Collins have taken a decisive position on Graham-Cassidy, but they are likely to be tough gets based on their many concerns with prior repeal bills. Collins said she had a "number of concerns" with the bill, including its Medicaid changes, in a statement on Monday. 
 "Medicaid was probably the policy area that was the most sensitive in the Senate," said Joe Antos, a scholar at the American Enterprise Institute. 
 Graham-Cassidy also has at least one strong "no" vote from the right from a lawmaker who had supported "skinny repeal." Sen. Rand Paul, R-Ky., has gone on a public rampage against Graham-Cassidy in recent days, which he says doesnt go far enough in repealing Obamacare. 
 Meanwhile, Sen. Lamar Alexander, R-Tenn., who chairs the Senate HELP committee, is currently making progress on a bipartisan health care bill with modest Obamacare tweaks. If Graham-Cassidy fails, it could be a fallback option. 
 Many Republicans are still undecided on Graham-Cassidy and prior repeal attempts have attracted some surprising holdouts in crunch time. Already, some conservatives have said theyre worried the bill would open the door to liberal states enacting single-payer health care programs, which could provide another reason for them to oppose it. 
 On the other hand, Republicans have been hammered  including sharply by President Donald Trump  for failing to produce a health care bill after years of promises to repeal and replace Obamacare. That pressure could prompt holdouts to take a chance on the bill. 
 If the measure does pass the Senate, it would have to then clear the GOP-controlled House without any changes  and there's a deadline. After September 30, any new bill would be subject to a 60-vote threshold in the Senate to get past a Democratic filibuster. 
