__HTTP_STATUS_CODE
200
__TITLE
FDA Seizes Smallpox Vaccine from Stem Cell Cancer Clinic
__AUTHORS
Maggie Fox
__TIMESTAMP
Aug 28 2017, 6:00 pm ET
__ARTICLE
 The Food and Drug Administration said Monday it had seized five vials of smallpox vaccine from a San Diego clinic that used it, without approval or proof it worked, to treat cancer patients. 
 The vaccine, which uses a live virus, is supposed to be tightly controlled and the company did not have permission to use it. Injecting live vaccines into cancer patients can be dangerous, the FDA said. 
 The company, StemImmune Inc., had given the vaccine to patients at the California Stem Cell Treatment Centers in Rancho Mirage and Beverly Hills, the FDA said. 
 The U.S. Marshals Service seized five vials of Vaccinia Virus Vaccine (Live)  a vaccine that is reserved only for people at high risk for smallpox, such as some members of the military. Each of the vials originally contained 100 doses of the vaccine, and although one vial was partially used, four of the vials were intact, the agency said. 
 The FDA did not say if anyone had been hurt by the unapproved treatments, but added that because the clinic was operating without approval, it was unlikely people would report trouble to the FDA. 
 Related: Three Women Blinded by Bogus Stem Cell Treatment in Florida 
 StemImmune said it was cooperating with the FDA. 
 "We look forward to continuing our dialogue with the FDA as we seek to bring this important cancer therapy to cancer patients," the company said in a brief statement. 
 The Centers for Disease Control and Prevention said the company got the vaccine through a legitimate program. CDC spokesman Tom Skinner said researchers working with vaccinia can get supplies of the vaccine to protect themselves. 
 "Over the past several years, CDC has shipped approximately 150 vials of vaccinia to private or public researchers," Skinner said. 
 The FDA has been trying to crack down on stem cell clinics that cash in on news coverage about stem cells, providing bogus treatments that are not only often useless, but also harmful. 
 Smallpox was declared eradicated in 1979 and no one is routinely vaccinated any more. Its the only human virus to ever have been eradicated. The vaccine that prevents smallpox uses a related virus called vaccinia. 
 Vaccinia is used as the basis of some other vaccines and some experimental therapies, including some experimental cancer treatments. But the FDA said StemImmune did not have permission to test vaccinia and did not have permission to inject the vaccine into anyone. 
 The company and its associated clinics were mixing the vaccines with patients own fat cells, or proposing to do so. 
 The unproven and potentially dangerous treatment was being injected intravenously and directly into patients tumors, the FDA said. 
 Speaking as a cancer survivor, I know all too well the fear and anxiety the diagnosis of cancer can have on a patient and their loved ones and how tempting it can be to believe the audacious but ultimately hollow claims made by these kinds of unscrupulous clinics or others selling so-called cures, the FDA commissioner, Dr. Scott Gottlieb, said in a statement. 
 The FDA will not allow deceitful actors to take advantage of vulnerable patients by purporting to have treatments or cures for serious diseases without any proof that they actually work, added Gottlieb, who was treated for Hodgkins lymphoma. 
 I especially wont allow cases such as this one to go unchallenged, where we have good medical reasons to believe these purported treatments can actually harm patients and make their conditions worse. 
 Related: Questions About Costly, Unproven 'Stem Cell' Therapies 
 StemImmune's website says it offers treatments using stem cells armed with potent anti-cancer payloads. 
 StemImmune has pioneered a potent new class of immunotherapies to wage a targeted, stealth attack on cancer," it says on its website. 
 There are companies that have legitimate FDA licenses to test such therapies. The idea of using viruses to attack cancer is not new, but groups have tried the approach without much success. 
 Amgen makes one FDA-approved treatment that uses a live herpes virus to treat melanoma. A company called Western Oncolytics is testing a vaccinia treatment for cancer but its still in whats called pre-clinical development  not ready to test in people, let alone actually treat paying customers. 
 To get new treatments approved, companies must first test them in the lab, and later prove that they are safe among a small group of people. They then must perform more tests in larger groups of people to prove an experimental treatment works as intended. Then they can apply for FDA approval to see and use them. 
 The company implied its treatment was safer than standard, proved cancer treatments. 
 Todays cancer therapies (chemo, radiation, even targeted therapies) expose patients to significant toxicities and side effects, it says on its website. 
 Related: Stem Cells Treat Blinding Disease in Some Patients 
 StemImmunes treatment was dangerous and not just to those being treated, according to the FDA. The agency noted that serious health problems, including those that are life-threatening, could occur in unvaccinated people if they come into close contact with someone who has recently received the vaccinnia virus. 
 In particular, unvaccinated people who are pregnant, or have problems with their heart or immune system, or have skin problems like eczema, dermatitis, psoriasis and have close contact with a vaccine recipient are at an increased risk for inflammation and swelling of the heart and surrounding tissues if they become infected with the vaccine virus, either by being vaccinated or by being in close contact with a person who was vaccinated, the FDA said. 
 Gottlieb said the FDA would continue to go after phony stem cell clinics. 
 Ive directed the agency to vigorously investigate these kinds of unscrupulous clinics using the full range of our tools, be it regulatory enforcement or criminal investigations. Our actions today should also be a warning to others who may be doing similar harm, we will take action to ensure Americans are not put at unnecessary risk, Gottlieb said. 
 The International Society for Stem Cell Research (ISSCR) welcomed the FDA's action. 
 We have been very concerned about reports of patients using unproven medical therapies whose safety and efficacy have not been tested in systematic clinical trials, the group's president, Hans Clevers, said in a statement. 
 Many of these patients have suffered great harm, and even death as a result of using unproven stem cell therapies. We are hopeful that increased regulatory enforcement against clinics offering unproven treatments will deter this practice and help protect patients." 
 The group says clinics are preying on people who have heard about experiments involving stem cells. 
 "Currently, stem cell therapies are known to be effective for only a small subset of diseases, mostly diseases that involve the blood-forming system," said the ISSCR, which has been complaining about unregulated "clinics" for years. 
 "In most cases, years of research will be required to identify the diseases that can benefit from stem cell treatments and to figure out how stem cells can be used safely and effectively in those contexts. Nonetheless, scores of clinics have sprung up throughout the United States selling stem cell therapies whose safety and efficacy have not been proven in clinical trials," it added. 
 "These clinics are preying on the hopes of desperate patients, selling what amounts to snake oil." 
