__HTTP_STATUS_CODE
200
__TITLE
Homeless Claim They Were Segregated and Shamed During Irma
__AUTHORS
Associated Press
__TIMESTAMP
Sep 29 2017, 2:29 pm ET
__ARTICLE
 ST. AUGUSTINE, Fla.  Shelby Hoogendyk says that when she, her husband and her 17-month-old son arrived at an emergency shelter as Hurricane Irma closed in, they were separated from others by yellow wristbands and told to stay in an area with other people like them  the homeless. 
 Sheriff's deputies, she says, told them the wristbands were prompted by problems that arose among homeless people at the shelter during Hurricane Matthew a year earlier. 
 "We were treated like we were guilty criminals," Hoogendyk says. 
 In the storm's wake, homeless people and their advocates are complaining that some of them were turned away, segregated from the others, denied cots and food, deprived of medication refills and doctors' visits, or otherwise ill-treated during the evacuation. 
 Many of the complaints have been blamed on misunderstandings, the sheer magnitude of the disaster, the crush of people needing shelter immediately, or inadequate state and local emergency planning. 
 All told, a record 72,000 Floridians sought refuge from the hurricane in early September at nearly 400 shelters. The response varied widely by county. 
 In Miami, over 700 homeless were picked up and taken to shelters. In Collier County, the sheriff sent officers into homeless encampments in the woods to bring people to a shelter. But in Polk County, Sheriff Grady Judd warned that any evacuees with warrants against them and all sex offenders seeking shelter would be taken to jail. And in Volusia County, some officials were accused of turning homeless evacuees away from shelters without explanation. 
 Related: Florida Sheriff Sued Over Tough-on-Crime Approach to Hurricane Irma 
 "Communities were all dealing with the fallout of not having very comprehensive planning in place to deal with this population," said Kirsten Anderson, litigation director at Southern Legal Counsel, a nonprofit public interest law firm in Florida. 
 She said if a shelter discriminated against people based on their economic status, it could be a violation of federal law that protects people in federal disaster zones. 
 In Hoogendyk's case, St. Johns County Sheriff David Shoar and school officials who ran the shelter at Pedro Menendez High vigorously denied segregating the homeless, saying the yellow wristbands were simply used to identify people with "special needs"  substance abuse problems, mental illness or other "frailties"  who needed to be closer to the bathrooms. 
 But Hoogendyk said neither she nor her husband claimed any special needs when they checked in. Other homeless people said they, too, were automatically issued the yellow wristbands, while others around them got blue or other colors denoting them as part of the "general population." 
 Gary Usry, a 57-year-old homeless man who arrived at the same St. Augustine shelter, said the first night was rough. 
 "We were left on concrete floor overnight. No blanket, no nothing," he said. Usry said a few cots were provided to people with wristbands of other colors, but not to any of the homeless in his yellow-band section. Usry said he felt "insulted, demeaned." 
 While insisting homeless people were not singled out, the sheriff also said that the homeless population has "a disproportionate representation of those with mental illness, substance abuse problems and, quite frankly, those with criminal backgrounds." 
 Sheriff's spokesman Cmdr. Chuck Mulligan said that last year, during Hurricane Matthew, there were numerous arguments, fights and instances of drunkenness among homeless people at the shelter. 
 Elsewhere around Florida, Robin Williams said she and about 60 others from the homeless-assistance group where she works, the Florida Keys Outreach Coalition, spent their first night as evacuees sleeping on a cold, hard gymnasium floor with no cots, blankets or food. The glaring lights stayed on all night, she said. 
 Over the next few days, the 30 or so special-needs evacuees among them were shuffled to various locations. 
 Just down the road, hundreds of other evacuees from the Keys rested comfortably with cots, hot meals, free toiletries and showers, Williams said. 
 "What these people have been through borders on criminal," she said. 
 The group's interim executive director, Stephanie Kaple, said three of her medically fragile clients ended up in the hospital after bouncing from place to place, wondering where they would sleep or if they would be fed. One case was a direct result of the stress, she said. 
 Kaple said that when she asked why some of her special-needs evacuees were sleeping on the floor, she was told that many of the cots were still being used in Houston, which was ravaged by Hurricane Harvey. 
 "I think there were places that the ball just got dropped," she said. 
 In the county's defense, Sheryl Graham, a senior director with Monroe County Social Services, said officials got barraged with last-minute requests from hundreds of people asking to be added to the special-needs registry, and it took precious manpower to contact and screen each one to make sure they were assigned to the correct shelter. 
 Special-needs evacuees are those who require assistance beyond what is provided at an ordinary shelter. Some might use an oxygen tank or wheelchair, for example. Medical assistance, which can include doctors' visits and medication, must be made available at such shelters. That's why special-needs evacuees must register beforehand. 
 But execution seemed to break down during Irma. Kaple said it was not until four days after the storm that her medically needy clients started getting doctors' visits, medications, showers and regular meals. 
 Lawanda Tobler, a bus driver for Volusia County who took part in the evacuation efforts, said a shelter at New Smyrna Beach High School refused to take a homeless person when they arrived, offering no other explanation than that he was homeless. 
 Tobler was then sent to a Salvation Army shelter where they "wouldn't even open the door and there were over a dozen homeless people at the site looking for shelter," she said. 
 Related: Hurricane Irma Survivors in Caribbean Fear They Will Be Forgotten 
 Emails and a call to the Salvation Army were not immediately returned. 
 The Rev. Jeffrey Dove said that after the storm, he headed to New Smyrna Beach's community center with about 30 homeless evacuees, only to be told by the city manager "we were not welcome." 
 When one of the homeless evacuees asked the city manager why they couldn't eat and shower there, "she looked at him in a very condescending way and stated that he did not pay taxes," Dove said. 
 New Smyrna Beach City Manager Pam Brangaccio said Dove's people were turned away because they included three "unknown homeless men" and because children were there and city maintenance employees were being fed at the time. 
 She said she and Dove have since apologized to each over after their heated conversation and are now working together to hold a summit on homelessness. 
 Volusia County spokeswoman Joanne Magley said all those who needed a place were provided with shelter. She said everyone had to produce identification to get in, and those who had no ID or were homeless were sent to separate shelters for the homeless. 
 "If you don't have an ID and we can't do a background check, how do you know if someone is a sex offender?" she said. "You can't just let anyone into a general population shelter." 
