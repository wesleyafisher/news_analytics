__HTTP_STATUS_CODE
200
__TITLE
Seouls Bomb Shelters Largely Ignored by Locals, Despite North Korea Threat
__AUTHORS
Alexander Smith
__TIMESTAMP
Oct 9 2017, 6:35 am ET
__ARTICLE
 SEOUL, South Korea  With thousands of North Korean artillery pieces little more than 30 miles away, Seoul has established a vast underground network of air-raid shelters across this sprawling city of 10 million people. 
 But many residents in the South Korean capital admit they don't even know where their nearest shelter is. And few seem concerned about that. 
 Most of the 3,253 civil defense shelters dotted across the city are hiding in plain sight. 
 They double up as underground shopping malls, subway stations and hotel parking lots  all dug extra deep by the South Korean government over recent decades with this dual purpose in mind. 
 To the newcomer, their red "shelter" signs, written in Korean, English and Chinese, are hard to miss. 
 But jaded by 60 years of tension and threats from Pyongyang, many locals say they rarely even consider that many of the places where they shop, take the train or park their vehicles also serve as a subterranean defensive infrastructure. 
 "I don't even think about these shelters," Oh Sun Jin, a 36-year-old banker, told NBC News. "I don't have a plan about what to do in the event of a North Korean attack because I think many young people, Korean people, don't think about this seriously." 
 North and South Korea are technically still in a state of conflict since the Korean War ended in an armistice, rather than a truce, in 1953. 
 In the decades since, the North has trotted out a regular stream of apocalyptic warnings, saying it will engulf Seoul in "a sea of fire." The pair have even exchanged some low-level shelling across the heavily militarized border known as the Demilitarized Zone, or DMZ. 
 The situation heated up this year as North Korean dictator Kim Jong Un appeared to achieve new advances in his nuclear and missile program, and President Donald Trump matched Kim's fiery rhetoric with his own outspoken statements. 
 Any preemptive military action by the U.S. would likely result in a catastrophic counter-strike by North Korea  with Seoul among the most vulnerable targets. 
 These high stakes make all-out war unlikely, but not impossible. 
 In the decades since the Koreas were partitioned by war, the North has become an impoverished, militarized pariah, while the South has embraced technology, capitalism and deep ties with the Western world. 
 Walking around this rapidly-evolving city, it is clear few are in an open state of panic. 
 Commuters bustle between the modern, asymmetric high-rises draped in neon lights and giant electronic advertising screens. Tourists still visit the city's historic temples and palaces, and hike the surrounding mountains. 
 Many people who spoke with NBC News said they feel much of the international reaction to the current standoff has been overblown. 
 "Foreigners do think about it, but we don't," said Oh, who was standing outside the Myeongdong Underground Shopping Center, one of several subterranean malls connected by subway stations across Seoul. 
 These local skeptics roll their eyes and say North Korea's threats are nothing new. 
 "I don't worry about an attack because I have been living in Korea for over 50 years," said IT worker Park Young Bae. "The possibility is almost at zero percent." 
 Nevertheless the 54-year-old still knows where he would go if Pyongyang launched an assault: the nearest subway station. He just doesn't think it will happen. 
 It's not just the older generation that is seemingly desensitized by decades of post-war rivalry. The country's young people seem to give as little thought to their emergency preparations. 
 "We don't usually think about it that much and we don't care that much," said Yoo Ji Sang, a 25-year-old waitress who was sightseeing near Seoul's 14th century Gyeongbokgung Palace. 
 She was astonished to learn that her home city had more than 3,200 shelters. 
 "If they attack us, I guess we have to run away," her friend, 23-year-old Cha Keon Ha, said with a shrug. 
 The likelihood of conflict is open to debate, but the blas attitude of many South Koreans worries officials here. In what appears to be the most recent available analysis, a government survey from 2002 found that 74 percent of Seoul residents did not know the location of their nearest shelter. 
 Although they are extensive, the shelters, which number around 18,000 across South Korea, have received their share of criticism. They may have some effectiveness against North Korea's conventional weapons, but they are not designed to withstand a nuclear or chemical attack. 
 Some include gas masks and other supplies, but most do not. One official at the Seoul Metropolitan Government told Reuters in July that this was because of a lack of public funding. 
 Not all South Korean residents are so relaxed in the face of the current geopolitical impasse. 
 The rising tensions have coincided with some South Koreans stocking up on more gold and ready-to-eat meals in recent months, according to another Reuters report in August. 
 The government is also trying to modernize its emergency plans, releasing an app in both English and Korean that points people toward their nearest shelter and gives them instructions on what to do if they hear an air-raid siren. There is also an interactive online map. 
 Residents are also encouraged to participate in public emergency drills, the last of which came on August 23, although many saw it as an inconvenience and did not even go down to their nearest shelter. 
 One man who isn't taking anything for granted is Noh, a 60-year-old retired consultant in the construction industry who only gave his first name. 
 "I'm concerned about the political situation, so I'm always thinking about the shelters for emergencies," he said after emerging from a shopping trip inside one of these would-be underground refuges. "In an emergency situation, I will go into the underground subway station. I think it's the safest place." 
 He said that unlike most of his compatriots, he felt threatened by Kim, who he called "the most cruel dictator" in the world. 
 "They have an atomic bomb, the ultimate lethal weapon against South Korea, and we don't have any atomic bombs," he said. 
