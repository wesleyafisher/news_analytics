__HTTP_STATUS_CODE
200
__TITLE
Turkey and ISIS: Istanbul Attack Signals Descent Into Open War
__AUTHORS
Don Melvin
__TIMESTAMP
Jan 3 2017, 2:04 pm ET
__ARTICLE
 The massacre targeting New Year's Eve revelers at an Istanbul nightclub illustrates that the relationship between ISIS and Turkey has shifted from an uneasy peace to an all-out war, according to experts. 
 ISIS has claimed responsibility for the shooting, identifying the attacker who killed 39 people as "a heroic soldier of the caliphate." The group said the rampage had been carried out "in response to a call" from its leader, Abu Bakr al-Baghdadi, although it didn't say whether it directed or merely inspired the perpetrator. They did not identify the attacker by name or provide proof he was acting on their behalf. 
 Related: American Stayed Silent After Being Shot During Nightclub Massacre 
 Michael Horowitz, director of intelligence at Middle East-based political risk consultancy Prime Source, suggested that Sunday's attack signaled that ISIS had initiated "open war" with Turkey, which boasts NATO's second-largest military. 
This confirms that we are in a new phase of "open war", where #ISIS feels it can operate more "openly" on Turkish soil
 Throughout 2014 and part of 2015, Turkey and ISIS had "an implicit understanding," according to Fawaz Gerges, the author "ISIS: A History." Their mutual approach was live and let live, he added. 
 During that period, Ankara prioritized unseating Syrian President Bashar al-Assad over direct engagement in the war against the Sunni militants. 
 In Sept. 2014, Turkey managed to negotiate the release by ISIS of 49 hostages who had been captured at the country's consulate in the northern Iraqi city of Mosul. ISIS is not the type of group to give up hostages in exchange for nothing. No details were made public, but some observers suspect a quid pro quo of some type  perhaps a tacit non-aggression pact. 
 If ISIS had any political vision or imagination, it would have maintained that non-aggression pact with Turkey, Gerges said. 
#ISIS's escalating "war" against #Turkey pic.twitter.com/WYYDZCDoBH
 In the following months, Turkey did little to impede the flow of foreign fighters passing through its borders on the way join ISIS in Iraq and Syria. 
 And despite international pressure, Turkey didn't join the fight against the terrorist group, even as the horrors it perpetrated became ever more brutal. 
 So what changed? 
 For one thing, both Turkey and ISIS dislike Assad. 
 Weakening the opposition to Assad by attacking ISIS was not something Turkey wanted to do. 
 Furthermore, many Turks joined ISIS. According to Gerges, intelligence experts estimate that between 800 and 1,000 Turks currently belong to ISIS. 
 Turkish President Recep Tayyip Erdogan may have been reluctant to bomb people whose families and friends were voters. 
 As ISIS' atrocities mounted, Turkey, as a major player in the region, faced huge pressure to take action. 
 On July 20, 2015, a suicide bomber killed more than 30 people in the Turkish town of Suruc, near the Syrian border. 
 Two days later, Erdogan received a phone call from President Barack Obama. Within days, American warplane began roaring off the runway the the Incirlik Air Base in southern Turkey to strike ISIS targets in Syria. 
 Turkish planes, too, bombed ISIS targets in Syria  along with Kurdish camps in northern Iraq. 
 Alex Kassirer, of Flashpoint and an NBC News terrorism analyst, said allowing the Americans to attack from inside Turkey was the turning point for ISIS. 
 After Erdogan started cooperating with the U.S. military  at the point, you stop being a Muslim country and you become the enemy, she said. 
 Gerges suggested that Erdogan used opposition to ISIS to cover his primary goal, which was to avoid having an independent Kurdish state forming on Turkey's southern border. 
 That, Erdogan felt, would only encourage Turkish Kurds in their effort to create an independent homeland. 
 According to Kassirer, ISIS militants are now taking aim at Turkeys pocketbook  as other terrorists have done elsewhere in the past. In this case, that involved the Istanbul nightclub that attracted tourists and the wealthy. 
 Tourism revenues in Turkey dropped 32.7 percent year-on-year to $8.28 billion in the third quarter of 2016, from $12.3 billion in the same period the previous year, according to TradingEconomics.com. 
 The number of visitors tumbled by 30.7 percent in the third quarter to 12.6 million, due to security concerns following a series of terrorist attacks and failed military coup attempt, the economics website reported. 
 Kassirer added: These groups know that if you target a countrys economy, thats a great way to weaken it." 
 
