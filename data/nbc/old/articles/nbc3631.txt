__HTTP_STATUS_CODE
200
__TITLE
Akron, Ohio, Schools to Get Anti-OD Med Narcan, but Not Everybody Agrees
__AUTHORS
Corky Siemaszko
__TIMESTAMP
Jul 12 2017, 9:21 pm ET
__ARTICLE
 There was one dissenter when the school board in Akron, Ohio, voted this week to start stocking medicine cabinets with what all members agree is a sad sign of the times  the anti-overdose drug Narcan. 
 And that school board member, Debbie Walsh, said she was bracing for blowback for casting that no vote. 
 "But blowback is not what I've received," Walsh told NBC News on Wednesday. "The people who talked to me said they agreed with me. They, too, are worried that having it on hand might be creating an even bigger problem by sending the message to kids: 'Don't worry, take drugs. We've got Narcan to save you.'" 
 While public policy experts and organizations like the National Association of School Nurses beg to differ, Walsh is not the first public official in Ohio to publicly oppose a policy that appears to be gaining widespread acceptance elsewhere in the state. 
 In recent weeks, the sheriff of Butler County announced that he would not equip his deputies with Narcan (a brand name for naloxone), and a City Council member in Middletown asked his legal department to look into whether authorities are legally required to dispatch ambulances to rescue drug addicts who repeatedly overdose. 
 Both cited the rising cost to taxpayers as part of their reasoning  and both are Republicans. 
 Walsh, who is also a Republican, said, "I got positive responses from both sides of the aisle." 
 "Politics wasn't my driving force, and neither was the cost," she said. "Every life is valuable, and there's no price on life. I am for protecting our students, and I agree that Narcan is a lifesaver. But I think it can also become a crutch and stop some people from taking personal responsibility." 
 Walsh's reservations echo those of past opponents of setting up needle exchanges and distributing condoms to stop the spread of AIDS, who argued that such moves were just encouraging drug use and sex. 
 But Walsh's logic did not fly with Dr. Matthew Davis, a professor of pediatrics at Northwestern University's Feinberg School of Medicine and head of general pediatrics at Ann & Robert H. Lurie Children's Hospital of Chicago. 
 "Health care workers in hospitals and first responders in communities have had naloxone on hand for decades, but there is no evidence that having naloxone as an antidote has encouraged Americans to try street drugs and abuse prescription opioids," Davis wrote in an email to NBC News. "Similarly, we would not expect teens to abuse opioids because naloxone is available in their schools." 
 Naloxone, he wrote, must be "part of comprehensive drug use prevention programs in schools and communities, to try to reduce drug use among teens." 
 "Making naloxone available in junior high and high schools is smart public health policy, given what is known about teens' misuse of prescription opioid medicines and teens' use of heroin in the U.S. today," he added. 
 Having naloxone on hand "is just like putting a defibrillator on the gym wall for a heart attack, or having injections of epinephrine available for someone who can't breathe because of a severe allergic reaction," he wrote. "They are tools made available to save lives." 
 Ohio has been hit especially hard by the national opioid overdose epidemic. In 2015, it recorded 3,050 drug overdose deaths. And preliminary data for 2016 from the state Health Department show the death toll rising to 3,986. 
 The young have not been spared. In the 15-19 age group, there were 23 fatal overdoses in 2014, 31 in 2015 and 29 last year. 
 Summit County, which is where Akron is located, saw a spike in fatal overdoses for that age group last year, with nine  up from three in 2015 and one in 2014. But no opiate overdose deaths were reported there or anywhere else in Ohio for youths ages 10 through 14, which includes middle school students. 
 To prevent more teenagers from dying, Gov. John Kasich has taken a number of steps that include "expanding access to naloxone to save lives," said Russ Kennedy, a Health Department spokesman. 
 "It is a local decision by schools regarding whether they want to carry naloxone," Kennedy said. 
 Margaret Cellucci of the National Association of Schools Nurses said it is a position the organization endorses. 
 "The school nurse is often the first health professional who responds to an emergency in the school setting," NASN said in its position statement. 
 "When administered quickly and effectively, naloxone has the potential to immediately restore breathing to a victim experiencing an opioid overdose," it said. 
 Akron, the fifth-largest city in Ohio, with a population of near 200,000, has six high schools and nine middle schools. 
