__HTTP_STATUS_CODE
200
__TITLE
Judge Stuns Court by Rejecting Defendants Plea Deal in Drug-Ravaged State
__AUTHORS
Corky Siemaszko
__TIMESTAMP
Jul 6 2017, 9:54 am ET
__ARTICLE
 This was the deal: A West Virginia drug dealer was going to plead guilty to a single count of distributing heroin and in exchange prosecutors would drop the five other counts against him, including a couple counts of peddling highly addictive fentanyl. 
 But when the plea agreement was presented to U.S. District Judge Joseph Goodwin, he did something neither the prosecutor nor Charles York Walker Jr.s public defender was counting on  he said no. 
 And in a 28-page order, the judge said one of the chief reasons Walker should face the bright light of the jury trial is because West Virginia is ground zero for the opioid epidemic that has been ravaging the nation. 
 A court should consider the cultural context surrounding the subjects criminal conduct, he wrote. Here, that cultural context is a rural state deeply wounded by and suffering from a plague of heroin and opioid addiction. 
 Goodwins ruling, which was released Monday in Charleston federal court, stunned Walkers lawyer. 
 This is the first time anything like this has happened in 25 years of practicing law, Lex Coleman told The Washington Post. 
 Related: DEA Issues Guidelines on Fentanyl to First Responders  
 Clint Carte, a prosecutor in the U.S. Attorneys office, said this was a shocker for them too. 
 We believe it's the first time in recent memory that a plea deal like this has been rejected by a judge, he told NBC News. 
 Its very rare that federal criminal cases  of any kind  go to trial. Last year, more than 97 percent were resolved by a plea agreement in which a defendant agreed to plead guilty to a lesser offense, thus avoiding a jury trial and guaranteeing the government a conviction, according to the U.S. Sentencing Commission. 
 Walker was indicted by a federal grand jury last September on three counts of distributing heroin, two counts of distributing fentanyl, and one count of being a felon in possession of a firearm. 
 Goodwin, who was appointed to the court in 1995 by President Bill Clinton, did not return a call for comment. But in his ruling, dated June 26, he made an impassioned argument for why Walker should face trial. 
 Related: Meth: 'It's Everywhere Again' 
 The jury trial reveals the dark details of drug distribution, an abuse to the community, in a way that a plea-bargained guilty plea cannot, he wrote. A jury trial tells a story. 
 In addition, the secrecy surrounding plea bargains in heroin and opioid cases frequently undermines respect for the law and deterrence of crime. 
 The judge also went to great lengths to describe the damage this epidemic had done to his home state. 
 The heroin and opioid crisis is a cancer that has grown and metastasized in the body politic of the United States, he wrote. West Virginia has the highest rate of fatal drug overdoses in the nation  and that rate continues to rise. 
 The judges claim is backed up by the most recent available statistics from the federal Centers for Disease Control and Prevention which reported that in 2015 West Virginia had the highest rate in the nation of deaths due to drug overdoses. It was 41.5 per 100,000 people. 
 Then the judge zeroed-in on Walker, noting that he has a voluminous criminal history. 
 For most of his life, Mr. Walker has been involved with illicit drugs, Goodwin wrote. 
 Wrapping up, Goodwin wrote that a jury trial is fundamental to the American scheme of justice and the plea deal offered Walker has little to do with justice. 
 The principal motivation appears to be convenience, the judge wrote. For the reasons stated, I REJECT the plea agreement. 
