__HTTP_STATUS_CODE
200
__TITLE
Tesla Wirelessly Upgraded Owners Batteries to Help Flee Irma
__AUTHORS
Paul A. Eisenstein
__TIMESTAMP
Sep 11 2017, 7:22 am ET
__ARTICLE
 Tesla wirelessly unlocked extra power in some owners' cars so they could flee more quickly from the path of Irma. 
 Drivers of several versions of its Model S sedan and Model X noted on social media that they were getting as much as 30 to 40 miles more range than expected. A Tesla spokesperson confirmed to NBC News that the automaker used its ability to reprogram its vehicles remotely to increase the amount of battery capacity a motorist in the evacuation area could access. 
 The upgrade was specifically targeted to affect select base versions of Tesla's SUV and sedan models. Known as the Models S and X 60, 60D, 70 and 70D, they had 60 kilowatt-hours of battery capacity. But, in a bid to reduce factory complexity  and costs  those versions actually shared the same, 75 kWh battery used in the more expensive, but longer-range Models S and X 75 and 75D models. Coming out of the factory, Tesla used software to allow an owner of the less expensive models to only access part of the battery pack. 
 In fact, it already offered owners the ability to upgrade their vehicle  for a price ranging from $4,500 to $9,000  to get access to the full 75 kWh capacity. That could be done with a simple, over-the-air update. 
 In light of the risks posed to owners struggling to leave Florida as Irma approached, Tesla decided to let all owners get the maximum range out of vehicles with the 75 kWh battery pack, using a wireless upgrade to boost range to about 230 miles at highway speeds. For someone heading from Miami to, say, Atlanta, that cuts by one the number of stops needed to charge along the way. For a drive to Detroit or New York City, as many as two charge stops could be eliminated. 
 Until Irma actually barreled into Florida Sunday morning, Tesla said its network of high-speed superchargers in the normally Sunshine State continued to operate. Those high-speed chargers can give an owner an 80 percent top-off in about a half hour. But, as with those depending on gas stations across Florida, Tesla owners had to contend with Supercharger stations that could face lines of frantic motorists fleeing the storm. 
 Owners with 60- and 70-series versions of the Models S and X will be able to tell if theyve gotten the upgrade by checking the digital display that operates almost all vehicle functions. If the full power of the battery has been unlocked it will show that the vehicle has effectively been upgraded to a 75-series model. 
 That's the good news. The bad news is that this is only a "temporary" upgrade. Tesla tells us owners of 60- and 70-series models will only have access to the full battery pack until September 16, presumably time enough for the storm to subside and owners to head back home. Of course, they could choose to retain the added capacity and pay the upgrade fee. 
 The automaker no longer sells the base versions of its Models S and X, incidentally, finding that demand was relatively light. Most Model S and X buyers have been willing to cough up the extra cash to get all the range possible from their battery vehicles. 
