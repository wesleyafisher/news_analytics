__HTTP_STATUS_CODE
200
__TITLE
Caitlan Coleman and Family Leave Pakistan After Hostage Ordeal
__AUTHORS
Courtney Kube
Wajahat S. Khan
F. Brinley Bruton 
Hans Nichols
__TIMESTAMP
Oct 13 2017, 7:09 am ET
__ARTICLE
 An American woman and her family freed from the custody of a Taliban-linked group left Pakistan on a flight bound for Britain Friday after her husband earlier declined to board a plane to the U.S., officials said. 
 Caitlan Coleman, who is originally from Pennsylvania, and Canadian Joshua Boyle were kidnapped by militants while hiking in Afghanistan in late 2012. Coleman was pregnant when she was captured, and the couple had three children while being held hostage. 
 The Pakistan military said that after it was alerted by U.S. intelligence that the family was being moved across the border from Afghanistan, a team that included infantry and intelligence personnel raced to surround the vehicle. 
 "The vehicle was immobilized with sharp shooting. We destroyed their tires. The hostages remained inside the vehicle. The driver, and an accomplice, managed to escape to a nearby refugee camp," Maj. Gen. Asif Ghafoor, a spokesman for the Pakistani military, told NBC News. "We moved the hostages via helicopter to Islamabad. They were then handed over to U.S. authorities." 
 U.S. military officials pushed back on the idea that the release was the result of a hostile, armed confrontation, with one official describing it as more of a diplomatic handover. 
 The U.S. had a C-130 ready to fly the family out of Pakistan but the husband did not want the U.S.-supplied transportation, three American officials told NBC News. It was not clear why they rejected the opportunity to immediately leave. 
 The family left for the U.K. on Friday but their final destination was not immediately clear, according to a senior Pakistani military official. 
 "It's a blessing that those children have survived and they're young enough that they can live a normal life," Boyle's aunt, Kelli O'Brien, told Canadian broadcaster Global TV. "It was my sister who called me and let me know that theyve been saved and theyre coming home after five years  that it was really happening this time, that they had been saved." 
 FBI Director Chris Wray said "we could not be happier" about the outcome. 
 "It's a great day. They've been held a long time," he said. 
 Coleman's family posted a note on their door referring to the "joyful news" and asking for privacy "as we make plans for the future." 
 The family were held by the Haqqani network, an insurgent group that supports the Taliban, when the Pakistani military mounted what it called "an intelligence-based operation." 
 The Haqqani network, whose leader is the deputy head of the Afghan Taliban, also held Sgt. Bowe Bergdahl for five years. The Afghan Taliban obtained five top commanders in exchange for the U.S. soldier in 2014 in a deal with the U.S. that was brokered by Qatar. 
 The couple had pleaded for their release in propaganda videos released by their captors. In one released in December, Coleman referred to "the Kafkaesque nightmare in which we find ourselves" and urged "governments on both sides" to reach a deal for their freedom. She then adds: "My children have seen their mother defiled." 
 Ghafoor said "no prisoner exchange or ransom money" was involved in freeing Coleman, Boyle and their children. 
 Before he married Coleman, Boyle was briefly married to the sister of Omar Khadr, a Canadian who spent 10 years at Guantanamo Bay after being captured as a teenager during a firefight at an al Qaeda compound in Afghanistan. After Coleman and Boyle were taken, U.S. officials said they did not think his connection to the Khadr family was a factor. 
 After the couple was freed, President Donald Trump called the operation "a positive moment" for U.S.-Pakistan relations. Pakistan's cooperation was a "sign that it is honoring America's wishes for it to do more to provide security in the region," Trump added. 
 The U.S. has long criticized Pakistan for not aggressively going after the Haqqani network, which is considered part of the Taliban. 
 In August, Trump warned Pakistan "has much to lose by continuing to harbor criminals and terrorists," a reference to the country's alleged support for militant groups like the Haqqani network. Pakistan rejects accusations that it shelters the militants. 
 The Haqqanis waged war on NATO forces in Afghanistan and have been blamed for many of the more than 2,000 U.S. military deaths there. 
 Due to their wealth and deep links to local tribes, one Western diplomat dubbed the Haqqanis "the Kennedys of the Taliban movement." 
 Courtney Kube and Hans Nichols reported from Washington. Wajahat S. Khan reported from Islamabad, Pakistan. F. Brinley Bruton reported from London. Mushtaq Yusufzai reported from Peshawar, Pakistan. 
