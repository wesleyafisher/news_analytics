__HTTP_STATUS_CODE
200
__TITLE
Painkillers to Heroin, New Film Shows It Can Happen to Anybody
__AUTHORS
Corky Siemaszko
__TIMESTAMP
Apr 27 2017, 7:39 pm ET
__ARTICLE
 The struggling heroin addicts whose tragic stories drive a harrowing new documentary about America's opioid crisis turned to the needle after the damage was done by prescription painkillers. 
 Only one of them is still alive when the credits close on Warning: This Drug May Kill You, a searing study of the plague directed by longtime New York newswoman Perri Peltz. 
 But in the final frames, the fate of a young survivor named Stephany Gay is anything but certain as she quits a program designed to wean her off the drugs that killed her sister and wrecked her life  after just six days. 
 She is doing much better, Peltz told NBC News, where she worked for many years before she began making documentaries. She just completed a 30-day rehab program with medication and she will be starting another. Our fingers are crossed. 
 That would be the kind of happy ending that was denied to Gail Cole, whose story about losing her 22-year-old son Brendan to a heroin overdose is part of the movie. 
 What were trying to do with this documentary is honor his memory and make sure he did not die in vain, said Cole, who lives in Allendale, N.J. We are basically trying to get people more educated and aware that we have a heroin epidemic because we have prescription painkiller epidemic. And that it can happen to anybody. 
 Peltz spoke to NBC shortly before the film made its debut Thursday at the Tribeca Film Festival. The new film comes as the governors of Ohio and New Jersey have taken steps to combat the crisis by limiting the amount of painkillers doctors can prescribe. 
 Three out of four new heroin users abused prescription opioids before they started using heroin, the federal Centers for Disease Control and Prevention reported. 
 Peltz she said the seeds for the piece were planted during a discussion with one of the head honchos of HBO who wanted to know why there were so many stories of people overdosing and dying. 
 Right now we have close to 100 people a day dying and she wanted to know why the narrative was those somehow these were bad kids abusing good drugs, said Peltz. By and large these arent bad people abusing good drugs. In fact, its good people who become addicted and most often it starts with a legitimate prescription. Case in point: Wynne Doyle, a doomed mother of three in tony Mill Valley, California, whose road to ruin was paved with painkillers she was prescribed after her third C-section. 
 Doctors were just throwing pills at her, her still grieving husband says, his eyes welliing-up with tears. 
 Then theres Georgia Cayce, who was 26 and living with her parents in New Jersey when she died of a heroin overdose. She got hooked not long after she was prescribed monster painkillers for a back injury she suffered during a fall. 
 By the time the prescription ran out, she needed more, her weeping father David Cayce says on camera. 
 And theres young Brendan, who died of a heroin overdose four years after he was prescribed opioids after an operation to remove a cyst. 
 You completely underestimate how hard the battle with addiction is, his father, Brian, says in the film. As a father you have this guilt that it happened on your watch. Being reassured you did everything you could does not lessen the pain, he adds. 
 That will always lingering the back of your mind, Brian Cole says. 
 But its Gays story that looms largest over a documentary that begins with jarring footage of people overdosing, including one especially wrenching shot of a weeping child trying to pull her unconscious mother off the floor of a store. 
 As the stories unfold in the film, Peltz does not intrude. 
 In a story that is as powerful as this, I felt a reporters voice imposing itself was not necessary, she said. The victims of this horrible epidemic speak more eloquently than I ever could. 
 Gay, who lives north of Chicago, recounts how her heroin addiction began shortly after she was was sent home from the hospital with Dilaudid, Vicodin, and Oxycontin after she was treated for kidney stones at age 16. 
 In the beginning I would take my Vicodin as prescribed, she says. 
 Soon, she began taking an extra one here and there. And when she realized she had a problem and went to the doctor, she came home with yet another highly-addictive opioid --- Percocet. 
 Gay describes how she wound up sharing pills with her older sister Ashley and how they graduated from that to snorting heroin. 
 I felt it loved me, Gay says. I felt like it loved me. And Gays love affair with heroin doesnt wane even after her sister dies of an overdose in 2013. 
 I do blame prescription opioids for my daughters addiction, Gays mother, Kathy, says. I trusted the doctors. 
 Warning: This Drug May Kill You debuts on HBO on May 1st at 10 p.m. 
