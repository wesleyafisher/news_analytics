__HTTP_STATUS_CODE
200
__TITLE
Medicare Starts Replacing Social Security Numbers on ID Cards
__AUTHORS
Maggie Fox
__TIMESTAMP
May 30 2017, 1:21 pm ET
__ARTICLE
 Medicare beneficiaries are getting new ID cards soon that won't carry their Social Security numbers, the Health and Human Services Department said Tuesday. 
 Congress had told HHS to get the cards replaced by 2019 to fight identity theft. The Center for Medicare and Medicaid Services (CMS) said people will start getting new cards next April. 
 Related: New Law Aims to Protect Medicare Patients from Surprise Hospital Bill 
 "Personal identity theft affects a large and growing number of seniors," CMS said in a statement. 
 "People age 65 or older are increasingly the victims of this type of crime. Incidents among seniors increased to 2.6 million from 2.1 million between 2012 and 2014, according to the most current statistics from the Department of Justice." 
 Medicare, the federal health insurance plan for people over 65, covers about 57 million Americans. 
 "The Medicare Beneficiary Identifier (MBI) is confidential like the SSN and should be protected as personally identifiable information," CMS said. The new identifier will be 11 characters long and will include letters and numbers. 
