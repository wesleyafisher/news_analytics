__HTTP_STATUS_CODE
200
__TITLE
Ex-Chemistry Teacher Gets 4 Years for Cooking Meth. Sound Familiar?
__AUTHORS
Daniel Arkin
__TIMESTAMP
Jul 19 2017, 6:54 pm ET
__ARTICLE
 Life apparently imitates art  or at least cable television. 
 A former high school chemistry teacher convicted of cooking meth in New Mexico  not unlike Walter White, the fictional antihero from "Breaking Bad"  was sentenced to four years in prison Wednesday, prosecutors said. 
 New Mexico District Judge Fernando Macias handed the defendant, John W. Gose, a nine-year sentence, all but four years of it suspended. After he is released, Gose must serve five years of supervised probation. 
 "We asked the court for the sentence we felt was appropriate for Mr. Gose," Doa Ana County District Attorney Mark DAntonio said in a statement. "We are very pleased that the defendant will have to face the consequences of his illegal and irresponsible decisions." 
 Gose, 56, pleaded guilty in May to trafficking by manufacturing a controlled substance. 
 He was arrested in October after Las Cruces police officers found a white Styrofoam ice chest containing glassware, rubber tubing and chemicals used to cook methamphetamine during a routine traffic stop, according to prosecutors. 
 Related: Twin Plagues: Meth Rises in Shadow of Opioids 
 Authorities later found more chemicals and supplies at his home in southern New Mexico. Investigators said he possessed ingredients to make at least a pound of meth  at an estimated street value of $44,800. 
 Gose taught high school science in El Paso, Texas, and middle school science in Las Cruces, prosecutors said. He told a roommate that he used to teach chemistry, according to police reports cited by prosecutors. 
