__HTTP_STATUS_CODE
200
__TITLE
Amid Twitter Boycott, Advocates Aim to Amplify Women of Color With #WOCAffirmation
__AUTHORS
Foluke  Tuakli
__TIMESTAMP
Oct 13 2017, 6:04 pm ET
__ARTICLE
 As people take a day off of Twitter to show their support for Actress Rose McGowan, advocates are also reminding others not to forget communities of color. 
 Twitter users boycotted the site on Friday in response to Rose McGowans suspension using the hashtag #WomenBoycottTwitter, meanwhile other users are using hashtag #WOCAffirmation to amplify the voices of women of color. 
 On Thursday, influencer and activist April Reign tweeted to her more than one hundred thousand followers to use the hashtag to magnify women of color on the platform. 
So that's what I'm going to do. You on your sh*t? You doing something you're proud of? You selling stuff? Hit me. I'll retweet. Let's go.
The hashtag I'll be using is #WOCAffirmation. Shout out sistas that are doing the damn thing. Promote them. Hit me so I can RT. Lift us up.
 The former lawyer is not new to starting social media movements. As the creator of the viral hashtag #OscarsSoWhite, she often advocates for marginalized communities and instigates conversations about representation. 
 Related: Civil Rights Activists Legacy Raises Breast Cancer Awareness Among Black Women 
 Reign was not the only one who found flaws with the #WomenBoycottTwitter movement. 
 Author Roxane Gay was intrigued by how conveniently inclusive the boycott was considering how often women are bullied on the platform. 
Now people want to boycott twitter? Always interesting where and for whom people draw the line.
 Ava Duvernay called out the often one way ask for support by white women while musician Questlove reminded his followers that reporter Jemele Hill who was suspended at ESPN for her tweets still needs our support. 
Calling white women allies to recognize conflict of #WomenBoycottTwitter for women of color who haven't received support on similar issues.
aight....in addition to supporting the #WomenBoycottTwitter movement i ask you all remember that @JemeleHill is catching hell as well. out.
 Many users are taking the opportunity to shine light on themselves and others with hashtag #WOCAffirmation. Writer and director Matthew A. Cherry gave a shout out to women he sees not getting the shine they deserve. 
For @jemelehill @Lesdoggg & other black women on here who deal w/ harassment daily & never get the same support #WocAffirmation
 Women of color and their allies are taking recognition into their own hands using the hashtag to promote themselves and their projects. 
I'm a WOC journalist writing "Uppity" a book about WOC in the US, and hoping to shine light on the obstacles we face. #WOCAffirmation
I'm the only Indian woman in the entire philosophy department at my school, & I'm applying to graduate school this semester. #wocaffirmation
I am a loud #transgender woman of color who is also a Marine Corps veteran and I fight for the rights of our community!#WOCAffirmation pic.twitter.com/FoGtm3Czy6
LDF, the nation's premier civil rights law firm and racial justice organization, is led by women of color. Enough said. #WOCAffirmation
While following, liking, and RTing with #WOCAffirmation, keep going. Buy their goods & services. Give them $. Promote & engage their work. pic.twitter.com/m4ZJiJoOJo
I'm horrendous at self-promotion but 2017 is teaching me there is no shame in being my biggest hype woman.#WOCAffirmation
I am affirming YOU, @ReignOfApril, 4 starting #WOCAffirmation hashtag. I'm literally sitting here w/gr8ful tears threatening to spill over. https://t.co/TUDKw6YNlf
 Follow NBCBLK on Facebook, Twitter and Instagram  
