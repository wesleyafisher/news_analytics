__HTTP_STATUS_CODE
200
__TITLE
Top U.S. Commander Sees Tough Fight in Surprise Visit to Iraqi Base
__AUTHORS
Courtney Kube
__TIMESTAMP
Oct 25 2016, 7:41 pm ET
__ARTICLE
 QAYYARAH AIRFIELD WEST, Iraq  U.S. commanders expect ISIS resistance to strengthen as Iraqi forces move closer to the strategic city of Mosul, the head of U.S. Central Command said during a surprise trip to this U.S. logistics base Tuesday night. 
 Army Gen. Joseph Votel, commander of the Pentagon's unified command for the Middle East, North Africa and Central Asia  including Iraq  flew in to "Q-West" on a C-130 cargo plane under the cover of darkness, making him the most senior U.S. military official to visit so far. 
 ISIS fighters destroyed the runway over the summer as Iraqi security forces advanced to the area to reclaim it. The plane carrying Votel was only the third to land here since the U.S. military repaired and reopened the runway several days ago. 
 Votel told NBC News that Q West was already playing "an extraordinarily important role in supporting operations" for Mosul  which will be vital as Iraqi forces advance and ISIS puts up and more resistance. 
 "I would expect we are going to run into more obstacles [and] more VBIEDs," or vehicle-borne IEDs, Votel said. "The population will be there, so that will add complexity into the fight. 
 "They've had a long time to prepare for that fight, so all of that we're going to contend with here very, very soon," he said. 
 Army Lt. Gen. Stephen Townsend, commander of the U.S.-led Combined Joint Task Force fighting ISIS, said the terrorist group had deployed used an "extraordinary" number of rockets and vehicle-born improvised explosive devices in the fight for Mosul. Still, he said he was confident that the U.S.-backed Iraqi security forces would retake Mosul. 
 "I think [ISIS] is living on borrowed time, and their clock's running out," Townsend said, adding: "The Iraqi security forces are going to take Mosul back. Period." 
 Related: Defense Secretary Carter Heads Closer to Front Line in Iraq 
 NBC News is the only broadcast news outlet traveling with Votel, who has already made stops in Afghanistan and Saudi Arabia. 
