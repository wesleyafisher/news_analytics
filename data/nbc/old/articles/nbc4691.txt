__HTTP_STATUS_CODE
200
__TITLE
Wildfires in Californias Wine Country Hit Vulnerable Immigrant Farmworkers
__AUTHORS
Carmen Sesin
__TIMESTAMP
Oct 13 2017, 12:50 pm ET
__ARTICLE
 Amelia Cejas vineyard in California's Sonoma Valley has been spared by the infernos raging through the region. But recently, in a nearby vineyard, she was taken aback by the sight of six crews picking grapes at night, under the cover of heavy smoke. 
 People shouldnt be out there working, she said, referring to the health risks associated with inhaling the smoke. 
 With her own beginnings as an immigrant farmworker, she knows all too well the deep impact the fires will have on workers in the fields. 
 With the intense fires still racing through Napa and Sonoma, the full scope of the damage to Californias wine industry is still not known. But what is certain is that thousands of farmworkers, many undocumented immigrants, will be left without jobs. 
 The fires, which have already destroyed some vineyards, struck during harvest season. Although harvest is nearing its end, thousands of workers who flock to the area have been affected. 
 Armando Elenes, a vice president of United Farm Workers, calculates 80 percent of this years grapes have been harvested. But many farmworkers, who live paycheck to paycheck, have been counting on the remaining weeks of harvest. He said most have not been able to work since last Sunday. 
 A lot of the farmworkers are undocumented and dont have access to our financial system like unemployment, Elenes said. 
 Related: Northern California Wildfires: Firefighters Continue to Battle Blazes, Hope for Reprieve 
 Normally, most of the grapes are picked by November. 
 "There are still quite a few grapes that have not been picked. Its not over yet. A lot of the grapes are going to be damaged by the smoke and the heat," according to Marco Lizagarra from La Cooperativa Campesina de California, a statewide association of agencies that implement farmworker service programs. 
 Right now, Lizagarra is trying to get an estimate of how many farmworkers the organization will be assisting. It normally helps farmworkers with temporary jobs when they are displaced because of a natural disaster. He estimated there are around 5,000 farmworkers in Napa alone and at least half will be displaced. 
 Its going to be large, he said. 
 There are also many who work in wineries that caught fire and are now out of business. Napa and Sonoma are known for making much of the countrys premier wines, which is why the area is a wine and food destination that draws 23.6 million tourists each year, according to the Wine Institute. 
 One of the obstacles facing organizations that normally help farmworkers in times of disaster is that federal funds cannot be used to serve undocumented workers. 
 Lizagarra said he is trying to secure funds from the state for direct services that can be used to assist with housing or other needs. 
 With those kinds of funds, if we get them, we can assist anyone, he said. 
 Related: Drought and Heat, Worsened by Humans, Help Fuel California Fires 
 But in general, undocumented farmworkers are less likely to seek the assistance of organizations, like the American Red Cross  which provides assistance regardless of immigration status  for fear of being deported. 
 Bruce Goldstein, who is president of Farmworker Justice, said the services undocumented immigrant farmworkers have access to are limited. 
 While some emergency services are available to undocumented immigrants, some are not. Even when theyre eligible for services, many undocumented immigrants are too fearful of deportation to take advantage of services, he said. 
 Sonoma County posted on its Twitter account that immigration status will not be asked at our shelters. They have also tweeted information Spanish, to spread the word among non-English speakers. Despite the effort, its not known to what extent undocumented immigrants will take advantage of these services. 
Immigration status will not be asked at our shelters. Keep your families safe. Our shelters have room for you. https://t.co/f4a3bPmgxs
 "In these emergencies, Goldstein said, "undocumented immigrant farmworkers are especially vulnerable and are being terribly affected by these disasters." 
 Follow NBC Latino on Facebook, Twitter and Instagram. 
