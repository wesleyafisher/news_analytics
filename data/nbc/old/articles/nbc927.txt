__HTTP_STATUS_CODE
200
__TITLE
War in Afghanistan: U.S.-Led Coalition Bombings Hit Seven-Year High
__AUTHORS
Yuliya Talmazan
__TIMESTAMP
Oct 10 2017, 9:59 am ET
__ARTICLE
 The number of airstrikes carried out by the U.S.-led coalition in Afghanistan hit a seven-year high in September, according to the U.S. Air Forces Central Command. 
 The U.S. and its NATO partners used 751 bombs against ISIS and Taliban targets during the period, according to a recently released report. This was a 50 percent jump from August. The highest one-month total for airstrikes was in October 2010, at 1,043. 
 The command's report attributed the increase to President Donald Trump's strategy to "more proactively target extremist groups that threaten the stability and security of the Afghan people." 
 The report did not detail the number of those believed to have been killed in the strikes. Afghan and American military officials in the country did not respond to requests for comment. 
 In August, Trump announced a new approach for the U.S. war in Afghanistan. Acknowledging that his "original instinct was to pull out" of Afghanistan, Trump said that after becoming president he realized a hasty withdrawal would cede ground to terror groups. 
 Related: The War In Afghanistan: By the Numbers 
 The president didn't reveal any specifics of his plan, saying conditions on the ground, not arbitrary timetables, would guide U.S. strategy from now on. As a candidate and private citizen, Trump repeatedly urged the U.S. to get out of Afghanistan. 
 In April, U.S. forces dropped the largest non-nuclear bomb ever used to target ISIS in eastern Afghanistan. The GBU-43 bomb, nicknamed the "mother of all bombs," reportedly killed 36 suspected ISIS militants. 
 American forces have been fighting in Afghanistan since the end of 2001, when they helped topple the Taliban, which was sheltering Osama bin Laden, author of the Sept. 11, 2001, attacks on the U.S. In recent years, the militants fighting to oust the U.S.-backed government in Kabul have expanded the territory under their control. 
 Smaller groups of ISIS-linked fighters have also gained a foothold in parts of Afghanistan. 
 In July, the United Nations reported that civilian deaths had reached a 16-year high in Afghanistan, with children and women bearing the brunt of the increase. 
