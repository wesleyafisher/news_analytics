__HTTP_STATUS_CODE
200
__TITLE
ISIS Singles Out Macys Thanksgiving Day Parade as Excellent Attack Target
__AUTHORS
Alex Johnson
Richard Engel
__TIMESTAMP
Nov 17 2016, 3:44 pm ET
__ARTICLE
 Police are reinforcing security for the Macy's Thanksgiving Day Parade through New York City after ISIS urged its supporters in the West to use rented trucks in attacks similar to the operation that killed 86 people this summer in France. 
 Police plan to station sand-filled trucks and concrete barriers as blockades all around the parade route to stop trucks, authorities told NBC News, after the appeared last weekend in the terrorist organization's English-language magazine, which called the parade "an excellent target." 
 About 3.5 million people are expected to line the parade's 2-mile route on Nov. 24. 
 Terrorism experts say the election of Donald Trump may have encouraged ISIS, which could believe he could be easier to provoke than President Barack Obama. 
 While ISIS hasn't put out any official propaganda centered on Trump's election, it did denounce both him and Hillary Clinton in a recent article on the U.S. election. Meanwhile, ISIS supporters and allied jihadist groups have discussed the advantages of a Trump presidency online, said Alex Kassirer, a senior counterterrorism analyst at NBC analysts Flashpoint Security, a worldwide security firm. 
 The new ISIS article offers a guide to inflicting maximum casualties using trucks, accompanied by a picture of the Macy's parade with a caption identifying it as an especially rich target for terrorists. 
 The guide suggests an operation closely similar to the tactic used July 14  Bastille Day  in the French resort city of Nice, where Mohamed Lahouaiej Bouhlel barreled a 19-ton truck into a crowd of revelers, killing 86 people and injuring more than 200 others. Police shot and killed Bouhlel. 
 Related: Terror by Truck: How the Attack in Nice Unfolded 
 Experts said that attack mimicked a strategy often used by al-Qaeda in the Arabian Peninsula and by attackers in Israel  zigzagging through the crowd and then speeding up when authorities approached. 
 So-called vehicle ramming is attractive to potential attackers because it offers them an opportunity to conduct strikes without firearms or explosives and with "minimal prior training or experience," the U.S. Department of Homeland Security has warned since as early as December 2010. 
 The tactic is considered to have pioneered in April 1995, when U.S. Army veteran Shawn Nelson, 35, stole a tank and went on a rampage in San Diego, crushing cars and any other object in his way before a police officer shot and killed him. 
 Authorities told NBC News that they've heard of no credible, specific threat to the Macy's parade. John Miller, the New York Police Department's deputy commissioner for intelligence and counterterrorism, said that while officials are taking the possibility seriously, people shouldn't let it dampen their holiday enthusiasm. 
 "This is not something that just occurred to us over the weekend," Miller said at a news conference. "Bottom line: Come to the Thanksgiving Day Parade. Have a good time. Bring the family." 
 (Editor's note: This article was revised Nov. 17 to reflect updated analysis from NBC News security analyst Flashpoint Security.) 
