__HTTP_STATUS_CODE
200
__TITLE
Trump Administration Hits Womens Health Fund
__AUTHORS
Maggie Fox
__TIMESTAMP
Apr 4 2017, 5:43 pm ET
__ARTICLE
 The Trump administrations decision to pull support from a United Nations family planning agency hurts some of the world's most desperate women and girls, advocates said Tuesday. 
 The State Department announced late Monday that it was yanking money from the UN Population Fund (UNFPA), which focuses on family planning, help for pregnant women and new babies. 
 Advocates had expected the move  conservatives have long criticized the UNFPA  but were nonetheless distraught. 
 It will directly impact the poorest girls and women on the planet, Seema Jalan, executive director of the Universal Access Project at the UN Foundation, told reporters. They have no recourse. 
 The U.S. paid $69 million into the UNFPA last year and was its fourth-biggest donor. 
 UNFPA is the largest international provider of contraception, family planning, and other reproductive health services, the International Womens Health Coalition said in a statement. 
 In 2016, U.S. support for UNFPA prevented an estimated 320,000 pregnancies and averted 100,000 unsafe abortions, while ensuring 800,000 people had access to contraception. 
 Related: Trump Pulls Back Protections for Women Workers 
 Republican presidents, backed by conservatives, have routinely stopped funding the UNFPA, while Democratic presidents, most recently the Obama administration, provide the cash. At issue is whether the UNFPA supports some of the actions taken by China in the past as part of its one-child-per-family policy, including forced abortions and sterilizations. 
 Jalan said there is no evidence the UNFPA has supported the policy and added that the agency has in fact worked with the Chinese government to help reverse it. 
 We were really pushing for a factual review based on evidence and we didnt see that, she said. We were not surprised that the Trump administration would move to defund UNFPA. However, we were incredibly surprised that the administration would do so without any evidence. 
 Related: What a Trump Administration Will Mean for Women's Health 
 Just days after he was inaugurated, Trump also reinstated the Mexico City policy, also known as the global gag rule, stopping funding for any group involved in any way with abortions. This is also a policy thats gone back and forth depending on the party holding the White House. 
 Jalan said conservatives and liberals alike should support groups that help womens health. UNFPA supports the only clinic in Iraq, for example, that cares for women and girl attacked by ISIS, the Islamist extremist group also known as ISIL. 
 I cant think of a better use of U.S. taxpayer dollars than ensuring that a woman fleeing violence can have a safe birth and that her child can survive delivery or that a woman who been raped by an ISIL terrorist can actually have some care and support, and thats what this funding provides, Jalan said. 
 The UNFPA also helps female Syrian refugees in the Zataari camp in Jordan, according to the humanitarian organization CARE, which also denounced the U.S. move. 
 Through the work of UNFPA, more than 7,000 babies have been delivered at this camp without a single maternal death, CARE said. 
 This loss of funding is a matter of life or death for families in the camp: nearly 50,000 people will lose safe delivery services in Jordan's refugee camps, a devastating consequence of this deeply flawed decision, it added. 
 CARE calls on the Trump Administration to stand by our American values and restore full funding to UNFPA to ensure they can continue providing access to health services for the worlds most vulnerable populations. 
 The Guttmacher Institute, which studies reproductive health issues, says the U.S. spent $607 million last year on family planning assistance in other countries. 
 Foreign policy should be rooted in evidence and results, not ideology and the politics of punishment, said Shannon Kowalski of the International Womens Health Coalition. 
