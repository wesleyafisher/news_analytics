__HTTP_STATUS_CODE
200
__TITLE
Trump Meets With Top Airline Executives to Discuss Jobs, Infrastructure
__AUTHORS
Lucy Bayly
Ali Vitali
__TIMESTAMP
Feb 9 2017, 10:53 am ET
__ARTICLE
 President Donald Trump met with aviation executives Thursday morning to discuss what he termed as the nation's "obsolete" travel infrastructure and a regulation framework he called a disaster. 
 Executives from Americas biggest cargo and passenger airlines and airports met with Trump at the White House, including Ed Bastian, CEO of Delta Air Lines; United Airlines CEO Oscar Munoz; and Myron Gray, president of U.S. Operations at UPS. Doug Parker, the head of Americas largest carrier, American Airlines, skipped the meeting due to an annual company retreat. 
 White House Press Secretary Sean Spicer said Wednesday that the meeting would focus on "U.S. jobs both in terms of the people who are serving those planes and the person who is building those planes, as well as how Trump is enacting orders to make sure the country's safe." 
 Youre going to be so happy with Trump. I think you already are, the president told attendees and reporters before the meeting began. "Travel is very important to me." 
 Related: Trump and the Travel Industry Are off to a Rocky Start 
 Citing the opinions of his own pilot, whom Trump noted was "a smart guy," the president said America's aviation infrastructure lagged way behind that of nations such as Japan and China. His administration would improve America's ailing infrastructure and "modernize our systems," Trump pledged. 
 Trump also announced an upcoming tax strategy that would "lower the overall tax burden on American businesses big league," with firmer details coming in the next three weeks, he said. 
 Following Trump's election, the travel industry was generally optimistic about the new administration's pledges to revitalize infrastructure and reduce taxes. But Thursday's meeting also comes amid rising tension about how airlines and airports should implement President Trump's controversial immigration order, which is currently stayed. 
 Related: What Are the Next Steps in the Fight Over Trump's Travel Ban? 
 However, after their meeting with the president, airline executives praised the gathering as wonderful and delightful but ignored repeatedly shouted questions from press on whether the travel ban came up. While exiting the White House, President and CEO of Airlines for America Nick Calio told a small group of reporters No, it did not come up. 
 When asked how that was possible, Calio exclaimed, It didnt come up! It didnt come up. Instead, he says, the focus was on aviation policy. 
