__HTTP_STATUS_CODE
200
__TITLE
NRA Backs New Regulations on Rapid-Fire Gun Bump Stocks
__AUTHORS
Alex Seitz-Wald
__TIMESTAMP
Oct 5 2017, 7:43 pm ET
__ARTICLE
 WASHINGTON  In its first public statement since the deadliest shooting in modern American history, the National Rifle Association on Thursday called for new regulations on bump stocks that rapidly accelerate a weapons' rate of fire. 
 "The National Rifle Association is calling on the Bureau of Alcohol, Tobacco, Firearms and Explosives (BATFE) to immediately review whether these devices comply with federal law," the NRA's chief executive, Wayne LaPierre, and its chief lobbyist, Chris Cox, said in a statement. 
 "The NRA believes that devices designed to allow semi-automatic rifles to function like fully-automatic rifles should be subject to additional regulations," the statement continued. 
 It was an unusual and potentially game-changing move for an organization that has made a habit of opposing any and all new restrictions on gun rights, and one likely to increase momentum on Capitol Hill for legislative action to crack down on bump stocks, especially among Republicans. 
 At the White House shortly after the NRA issued its statement, Press Secretary Sarah Huckabee Sanders said that President Donald Trump is aware that Congress wants to take a look at bump stocks. 
 "Wed like to be a part of that conversation," she said. "Were open to that moving forward." 
 A dozen bump stocks were found in the shooter's hotel room after Sunday's massacre in Las Vegas, leading some top Republicans who are generally hostile to gun restrictions to call for congressional action on the devices. 
 Moderate Republican Rep. Carlos Curbelo of Florida announced Thursday that he's working on a bipartisan bill to ban bump stocks with Democratic Rep. Seth Moulton of Massachussets, a rising star in his party. 
 For the first time in decades, there is growing bipartisan consensus for firearm reform, a polarizing issue that has deeply divided Republicans and Democrats, Curbelo said in a statement. 
 And a number of other key GOP lawmakers, including the House Judiciary Committee chairman, Bob Goodlatte of Virginia, are also looking into the issue. 
 Still, the carefully worded NRA statement stops short of calling for legislation, instead endorsing new federal regulations through the ATF and blaming the Obama administration for approving the sale of bump stocks in the first place. 
 The ATF has already concluded bump stocks do not violate current law, Special Agent in Charge Jill Snyder told reporters in Las Vegas on Tuesday. And Sen. Dianne Feinstein, D-Calif., a leading gun control advocate, said Thursday that only new legislation could close what she called a "loop hole." 
 In 2013, the ATF told Congress its hands were tied on the devices. "Stocks of this type are not subject to the provisions of Federal firearms statutes. Therefore, ATF does not have the authority to restrict their lawful possession, use, or transfer," the agency's assistant director wrote in a letter to Rep. Ed Perlmutter, D-Colo. 
 The NRA statement also criticized some lawmakers for pushing for gun control in the immediate aftermath of the shooting. 
 "Unfortunately, the first response from some politicians has been to call for more gun control. Banning guns from law-abiding Americans based on the criminal act of a madman will do nothing to prevent future attacks," LaPierre and Cox said. 
 And they used the statement to reiterate their desire that Congress pass a bill to make concealed carry weapons permits valid across state lines. 
 Meanwhile, Americans for Responsible Solutions, the gun safety group run by former Democratic Rep. Gabby Giffords, is working on a separate framework with the goal of attracting bipartisan support. 
 Unlike bills already introduced by Democrats in the House and Senate that would criminalize the possession of bump stocks, the proposal would regulate existing devices by making them subject to the same strict law that covers machine guns and would ban their future manufacture. 
 "This really balances what were trying to do in the name of public safety while recognizing the political reality that were in," said Rob Lloyd, the director of government affairs at Americans for Responsible Solutions. 
