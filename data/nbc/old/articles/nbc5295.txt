__HTTP_STATUS_CODE
200
__TITLE
China Grows Weary of Its Unruly Neighbor North Korea
__AUTHORS
Janis Mackey Frayer
__TIMESTAMP
Sep 6 2017, 12:41 pm ET
__ARTICLE
 BEIJING  The timing of North Koreas recent nuclear test managed, once again, to embarrass its only ally in the diplomatic world: China. 
 It was particularly unfortunate for Chinas president, Xi Jinping. The test took place hours before Xi took the stage as host of the BRICS summit of major emerging economies  a meeting meant to showcase Beijing as a global player. 
 This was not the first time that North Koreas leader, Kim Jong Un, has rattled the Chinese leader: Missile tests upstaged at least two other major events for Xi this year. 
 Obviously, it is an insult to China, said Cheng Xiaohe, an international politics professor at Beijings Renmin University. Every time  they put China in an awkward situation that turns the conflict between North Korea and the rest of the world into a conflict between China and the U.S. 
 As North Koreas primary patron, China finds itself at the center of the problem. President Donald Trump has repeatedly pressured China to use its leverage to rein in the North. After Sundays test, he tweeted that North Korea had become a great threat and embarrassment to China, then followed it up with a threat to cut trade with any country doing business with the North. 
The United States is considering, in addition to other options, stopping all trade with any country doing business with North Korea.
 A spokesperson for Chinas Foreign Ministry called Trumps trade tweet unacceptable. 
 Yet Pyongyangs tests and escalating rhetoric have also infuriated Beijing, by calling into question its credibility to control Kim. 
 Related: Trump Threatens to Stop U.S. Trade With China. Could He? 
 North Korea has said in public statements that it wants an official end to the Korean War. The conflict was halted by a 1953 armistice but no peace treaty has been signed. It also wants nothing short of full normalization of relations with the U.S. and to be treated with respect and as an equal in the global arena. 
 Officially, Chinas position remains the same: The only solution is for both sides to back down, and for dialogue. But editorials in government-controlled newspapers and academic forums, which typically reflect what the leadership here is thinking, there is a noticeable shift in tone and more hawkish viewpoints are emerging. 
 Its time to get rid of all the emotions and be a thorough realist on the North Korea issue, wrote current affairs commentator Li Fang on WeChat, the popular social media platform. Sanctions are not working, and wars are more terrifying than before.  The key is to figure out what to do next. 
 The U.S. is already working on drafting tougher sanctions against North Korea. South Korea and Japan are also expected to push the United Nations to impose an oil embargo on the North. Cutting off supplies would effectively cripple the economy and presumably the development of its weapons programs. 
 China holds the key: It supplies about 80 percent of the North's oil and fuel oil, much of it through the so-called Friendship Pipeline that runs under the Yalu River separating Dandong, China, from Sinuiju, North Korea. 
 China has resisted the move, even as Kim Jong Un has become arguably the worlds most undesirable neighbor. Beijing has talked of red lines before, but fears that outright collapse of the Kim government could trigger a wave of refugees across its border. Longer term, the reunification of the Koreas would place an American ally right at Chinas doorstep. 
 If Washington and Seoul cannot solve the crisis and instead place China at the forefront of this situation, they will only mess up the peninsula issue, said an editorial in Mondays Global Times, a state-run newspaper and website that frequently publishes nationalistic columns on international affairs. China is the main external victim of the North Korean nuclear crisis, it added. 
 Sanctions to this point have been ineffective. North Korea made a point of announcing that all its components for Sundays test were homemade, implying that it can survive without imports. 
 [North Koreas] nuclear technology has improved a lot, said Zhang Liangui, a professor at the Central Party School of the Communist Party of China. But thats not the most important.  It is that North Korea is determined to go on a nuclear path to oppose global society. 
 Some Chinese academics and scholars say more powerful sanctions could lure North Korea to the negotiating table. Beijing has long supported measures toward ensuring a denuclearized peninsula, though it is expected to use its veto power at the United Nations to prevent isolating the government even further. 
 Putin said the sanctions are useless, said Cheng of Renmin University, referring to the Russian president. China said that some people stab others in the back under the name of sanctions. 
 As for Xi, he did not even mention North Korea in his opening speech at the BRICS summit or at a closing news conference. A condemnation by BRICS leaders was added to a joint communique as point number 44. 
 Getting a read on Chinas opaque leadership is never easy. Public discussion is censored or outright discouraged. 
 Asked if he was optimistic the crisis could be resolved without conflict, Zhang, the Central Party School professor, paused, then said, Lets see how Trump will decide. 
