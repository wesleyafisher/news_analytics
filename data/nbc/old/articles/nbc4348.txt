__HTTP_STATUS_CODE
200
__TITLE
New Photo Shows the Moon May Be Farther Away Than You Think
__AUTHORS
Matthew Nighswander
__TIMESTAMP
Sep 29 2017, 6:24 pm ET
__ARTICLE
 When it's looming over the horizon, the moon can seem enormous and close. But looks can be deceptive. In a 2011 video posted by Veritasium, an interviewer found that people's guesses were wildly off when they tried to estimate the relative distance of the moon from the Earth. 
 If the Earth were the size of a basketball, the moon would be a tennis ball some 24 feet away. Because of space considerations, illustrations almost always put them much closer together than they actually are. 
 A new photo from NASA may help to dispel these misconceptions. Captured by NASAs OSIRIS-REx spacecraft when the Earth and moon were 249,000 miles apart, the image dramatically illustrates the distance. The Earth is a tiny mottled ball at upper left and the moon is little more than a grey speck in the lower right corner. 
 The spacecraft was 804,000 miles from Earth when the image was captured. 
 Click on the image below to see it at a larger scale. 
 OSIRIS-REx launched in 2016 will travel to a near-Earth asteroid called Bennu and bring a small sample back to Earth in 2023. 
 Month in Space Pictures: Cassini's Finale and an Astronaut Returns 
