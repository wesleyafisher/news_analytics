__HTTP_STATUS_CODE
200
__TITLE
Disconnect From Reality  and Modern Conveniences  on Alaskas Dalton Highway
__AUTHORS
NBC News
__TIMESTAMP
Oct 7 2017, 9:15 pm ET
__ARTICLE
A truck rolls north along the Dalton Highway through the Brooks Range near Atigun Pass, Alaska, on Sept. 6, 2017.Stretching 414 miles north from central Alaska to Prudhoe Bay, the Dalton Highway is one of America's northernmost roads and arguably its most remote.
Built as a supply road for the Trans-Alaska Pipeline, the Dalton was opened to public use in 1981. Largely gravel and littered with potholes, a round-trip drive takes four days. Though it still offers few facilities and no radio, cell service, or internet, the Haul Road, as it is often called, rewards its rare visitors with spectacular Arctic scenery.
Motorcyclists navigate a mountain pass through the Brooks Range as they drive along the Dalton Highway in Atigun Pass on Sept. 4.
The Dalton Highway winds through strands of birch trees near Yukon River Camp on Sept. 3.
A truck rumbles down "the roller coaster", a series of steep hills along the Dalton Highway near Yukon River Camp on Sept. 3.
A sign along the Dalton Highway marks the Arctic Circle near Prospect Creek on Sept. 7.
A car rolls south along the Dalton Highway near Coldfoot on Sept. 7.
A message is scrawled on the Trans-Alaska Pipeline along the Dalton Highway near Wiseman on Sept. 4.
Built by the Alyeska Pipeline Company in 1974 as the haul road for the Alaskan pipeline, which runs alongside it, the Dalton is mostly gravel, and not for the faint of heart. Basketball-sized potholes litter the road, and giant supply trucks kick up great clouds of dust and rock as they barrel between Fairbanks and the oil fields on the North Slope.
Coldfoot Camp is one of the few places to stay overnight along the Dalton Highway in Coldfoot on Sept. 4.
A sign marks a trucker's-only dining area at Coldfoot Camp on Sept. 6.
The house phone at Coldfoot Camp on Sept. 3.
A guest room at Coldfoot Camp on Sept. 6. The Camp resides at mile marker 175, not even half way along the 414 mile route.
The inside of an abandoned school bus has found new use along the Dalton Highway in Coldfoot on Sept. 3.
Along the desolate route are the treeless mountains of the Gates of the Arctic National Park stretching to the west, and the Arctic National Wildlife Refuge to the east.
Daniel Johnson-Utsogn pushes a children's bike trailer full of his belongings along the Dalton Highway in the final days of a four-year-long walk from New York City to Prudhoe Bay near Wiseman on Sept. 6.The 23-year-old adventurer said he walked more than 25,000 miles through 49 states.
The Dalton Highway cuts through the Brooks Range near Atigun Pass on Sept. 6.
Industrial traffic navigates the Dalton Highway through the Brooks Range near Atigun Pass on Sept. 4.
A caribou crosses the Arctic tundra along the Dalton Highway near Sagwon on Sept. 4.
A truck travels on the Dalton Highway through the Brooks Range south of Sagwon on Sept. 4.
A truck navigates heavy mist as it rolls north to Prudhoe Bay along the Dalton Highway near Deadhorse on Sept. 5.
A truck sits idle along the banks of the Sag River next to the Dalton Highway near Deadhorse on Sept. 6.
Oil rigs sit in the Prudhoe Bay Oil Fields off the Dalton Highway in Deadhorse on Sept. 4. Low oil prices have idled many of the North Slope's rigs.
The trucks of North Slope oil field workers fill the parking lot outside the Aurora Hotel near the end of the Dalton Highway in Deadhorse on Sept. 4.
Signs at an intersection in front of Colleen Lake mark the unceremonious end of the Dalton Highway, 414 miles later, in Deadhorse on Sept. 4.
A fog bow floats above the Arctic Ocean near the end of the Dalton Highway in Prudhoe Bay on Sept. 5. Though the Dalton ends 10 miles south of the Arctic Ocean in nearby Deadhorse, visitors can take a shuttle through the Prudhoe Bay Oil Fields, where public access is restricted, to reach the ocean.
