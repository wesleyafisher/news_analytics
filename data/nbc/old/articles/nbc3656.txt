__HTTP_STATUS_CODE
200
__TITLE
Fentanyl Crisis: Chicago Sees Surge in Deaths From New Designer Drug
__AUTHORS
Corky Siemaszko
__TIMESTAMP
May 8 2017, 4:21 pm ET
__ARTICLE
 When it comes to killing people in Chicago, bullets have some competition from a deadly designer drug called Acryl fentanyl. 
 Laced with heroin or cocaine, it has already killed 44 people in the nations third largest city and its suburbs through April 8  a huge jump from the seven fatal overdoses last year, the Cook County Medical Examiner reported Monday. 
 And the number of Acryl fentanyl deaths this year could actually be higher because toxicology results in some fatal overdose deaths have not been completed, the ME's office said. 
 What makes this fentanyl derivative even more dangerous is that it is more resistant to the overdose antidote naloxone, better known by the trade name Narcan. 
 We are seeing people in our emergency department who need increased doses of naloxone  in some cases as many as four doses  for the patient to be stabilized, Dr. Steve Aks of the Cook County Health & Hospitals Systems Stroger Hospital said in the MEs statement. 
 And heres the kicker: this particular variation of fentanyl has not yet been declared a controlled substance by the federal Drug Enforcement Agency. So buying it online is a cinch  and not illegal, DEA spokesman Melvin Patterson told NBC News. 
 When an NBC reporter did an online search for Acryl fentanyl, a site offering 100 grams of the powder for $797 and 1000 grams for $3,497 popped up within seconds. 
 Effects similar to: Heroin/Fentanyl, it says. 
 Im sure theyre doing evaluations of Acryl fentanyl right now, said Patterson, adding that the agency is constantly trying to keep up with the illegal drug makers who regularly dump new variants of fentanyl on the black market. 
 Acryl fentanyl comes in a powder and to the naked eye it looks like heroin or regular fentanyl. It doesnt have a street name, said Patterson, "but it can be more potent than regular fentanyl. 
 That is saying a lot. Fentanyl packs 50 times more punch than heroin. 
 Fentanyl and fentanyl analogues are very powerful drugs that are likely to be lethal, said Cook County Medical Examiner Dr. Ponni Arunkumar. Just one dose can easily stop a person from breathing, causing immediate death. 
 President Donald Trump regularly rails about Chicagos homicide rate, which hit 812 last year. 
 But the MEs office reported that in 2016, a total of 1,091 people in Cook County died, at least in part, because of an opiate-related overdose. 
 And fentanyl  or a variation of the powerful painkiller  figured in 562 of those deaths, the MEs office reported. 
 Cook County has a population of about 5,238,216, more than half of whom live in Chicago proper. This year, 30 of fatal overdoses involving Acryl fentanyl were city residents and the remaining 14 were from the suburbs, according to stats provided by the ME's office. 
 So far, more than 200 people have been killed in Chicago. 
 Patterson said Acryl fentanyl is clandestinely manufactured, most likely in Mexico or China and smuggled into the U.S. Its just a different type of fentanyl, modified in some way, he said. 
 The type of fentanyl most often seen in Chicago, according to the MEs office, is Furanylfentanyl, which the DEA labeled a controlled substance last year, and an older variant called Despropionyl fentanyl. 
