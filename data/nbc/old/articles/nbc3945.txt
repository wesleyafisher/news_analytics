__HTTP_STATUS_CODE
200
__TITLE
These Five Tips Will Make You a Master at Delegation 
__AUTHORS
Kim Bainbridge
__TIMESTAMP
Sep 7 2017, 11:58 am ET
__ARTICLE
 When you grow a business from the ground up it can be so hard to let things go. It's normal to want to be involved in every aspect of every deal, but at some point you will have to learn how to delegate in order to scale your business and improve operations efficiency. 
 These five tips will help you master the art of delegation. 
 If you don't take the time to fully develop your ideas you risk setting up your employees for failure. Think about your budget, timelines and end goals before reaching out to your workers for help. 
 Even the best laid plans will fall apart if you choose the wrong people to do them. Use your knowledge of your employees' strengths and weaknesses to pick the right people for each assignment. 
 You've already planned your project effectively and delegated your tasks to competent people. So you must trust them to get the work done. Still, schedule time to check in with your teams and track their progress so that you can pivot if necessary. 
 Even if your employees do a great job on the tasks you give them, you have to give them feedback. Empower them and help them hone their decision-making skills by letting them know what they did well and what they might need to work on. 
 Your employees work hard for you every day, so celebrate them as often as you can. You don't need to throw a party for every effort, but a little recognition can boost morale and show other employees how you like to see work done. 
