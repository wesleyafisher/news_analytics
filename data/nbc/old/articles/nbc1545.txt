__HTTP_STATUS_CODE
200
__TITLE
Did Video Games and iPads Kill Toys R Us?
__AUTHORS
Ben Popken
__TIMESTAMP
Sep 20 2017, 4:17 am ET
__ARTICLE
 Toys R Us declared bankruptcy Monday, but the batteries powering this bunny went dead a long time ago. 
 While there are a lot of factors at "play," a big part of the issue is that the Batmobiles and Barbie dolls have been gathering dust in the rec room as children's faces are lit by glowing apps and bleeping video games. 
 Now when you type into Google, "My kid is addicted to..." the top suggested word to fill in the blank is "Minecraft," followed by sugar, then Xbox, then "Roblox," another block-building game. That's right, Minecraft is more addictive than sugar, according to what people are anxiously typing into Google. 
 In 1999, Toys R Us was the #2 retailer for video games and software. 
 Today it accounts for less than 1 percent of the market share, according to research by Niko Partners, a firm specializing in games market analysis. 
 While you can certainly buy video games at Toys R Us, why would you? Customers complain the stores feel dirty, disorganized, overwhelming, and overpriced, especially compared to what you can get online and with less friction. 
Toysrus is not closing because of tablets. They are closing because they are dirty and we don't want to take our kids in there.
For what it's worth, I have yet to step in a Toys R Us that wasn't dirty, disorganized, and overpriced.
 Toys R Us rose to prominence through the 90s as a "category killer," providing for even the most demanding kid what specialist toy stores and department stores could not. But it began to lose traction as big box stores expanded, and with the rise of online shopping it ceded ground to Walmart, Target, and Amazon. 
 "They got stuck in 1985," Robyn O'Bryan, an author based in Boulder, Colorado, and the mother of four children, told NBC News. "I'm kind of surprised they managed to hang on." 
 While O'Bryan and other parents say physical toys are still a big deal for younger kids, the iPad and other electronic devices are an irresistible draw. By age seven, most kids these days have largely set aside their physical toys in favor of virtual attractions. 
 "There's a thing about being a dad where you can all of a sudden buy these toys, sometimes ones you never had, and play with your kid and revisit childhood," said Kipp Jarecke-Cheng, a communications chief and father of two from Maplewood, New Jersey. "It's somewhat bittersweet when they put those things down." 
 While Jarecke-Cheng did find himself in a Toys R Us recently to look for toys for his soon-to-be three-year-old daughter, purchases for his 6th-grade son primarily come in the form of gift cards to be redeemed inside the Xbox or Steam game systems. Those and App Store dollars aren't flowing toward Toys R Us any more. 
 According to a study by the Kaiser Family Foundation, kids aged 8-18 are spending nearly eight hours a day using entertainment media. 
 Concerned parents have sounded the alarm, setting limits on screen and internet time. The American Pediatrics Association recommends no screen time for children under the age of 18 months and limited screen time for 18-24 months, solely consisting of high-quality programming in the presence of an adult who can talk to the child about what's going on. 
 But what if you threw all the rules out? 
 Megan Zander, a 33-year-old staff writer for CafeMom, tried an experiment with her then three-year-old twin boys. For three days, she let them have as much screen time as they wanted. Frosting cakes in Daniel Tiger, feeding people in Toca Kitchen, making their own hamburger in Lego Duplex (food is favorite topic), they could gorge as much as they wanted. 
 Part of her wanted to see if they would tire of it if the forbidden fruit aspect was taken away. Another part just wanted a break as a then-freelancing mom working at home. 
 "I thought they were going to turn into little zombies. They're going to stop speaking and never want to play with their toys," Zander said. Instead, "I was able to strike a balance." 
 She knows that they're not equivalent and playing with virtual worlds can't substitute for the real thing. 
 "You can build the most beautiful 3-D world, but nothing is going to replace the fine motor or social skills by picking up two blocks and stacking them on top of each other," said Zander. 
 Toys R Us CEO David Brandon said in a statement the bankruptcy protection period will give the company flexibility with its creditors to redesign the in-store shopping experience and pivot towards creating "interactive spaces" that will host birthday parties, product demonstrations, and let employees take toys out of the boxes before purchase. In addition, he said the company had reached out to app developers to start creating augmented reality video games. 
 "We are confident that these are the right steps to ensure that the iconic Toys R Us and Babies R Us brands live on for many generations," said Brandon. 
 The company confirmed its 1,600 stores will remain open through the holiday season. 
