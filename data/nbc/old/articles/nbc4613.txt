__HTTP_STATUS_CODE
200
__TITLE
Catalonias Leader Demands Dialogue With Spain on Independence
__AUTHORS
Associated Press
__TIMESTAMP
Oct 16 2017, 7:31 am ET
__ARTICLE
 BARCELONA, Spain  Catalonia's leader called for dialogue with Spain and a meeting with the country's prime minister, complying with a Monday deadline to respond to a request from the central government to state explicitly whether the regional president had declared independence. 
 But Carles Puigdemont's letter, released about two hours before the deadline was set to expire, didn't clarify whether he indeed had proclaimed that Catalonia had broken away from Spain. The central government had explicitly asked him to respond with a simple "yes" or "no" to that question. 
 Instead, Puigdemont replied with a four-page letter seeking two months of negotiations and mediation. 
 "The priority of my government is to intensively seek a path to dialogue," Puigdemont said in his letter. "We want to talk ... Our proposal for dialogue is sincere and honest." 
 Spain has repeatedly said that it's not willing to sit down with Puigdemont if calls for independence are on the table, or accept any form of international mediation. Prime Minister Mariano Rajoy's government has threatened to activate Article 155 of Spain's Constitution, which could see Madrid take temporary control of some parts of Catalonia's self-government. 
 Deputy Prime Minister Soraya Saenz de Santamaria said Spain's government will take control of Catalonia and rule it directly if Puigdemont does not drop a bid to split the region from Spain by Thursday morning. 
 "Mr Puigdemont still has the opportunity to start resolving this situation, he must answer 'yes' or 'no' to the declaration (of independence)," Saenz de Santamaria said. 
 Article 155 has not been activated since the constitution was adopted in 1978 after the death of dictator Francisco Franco. 
 Puigdemont held a banned independence referendum on Oct. 1, and made an ambiguous declaration of independence last week. He then immediately suspended the declaration to allow time for talks and mediation. 
 The Catalan government says 90 percent of Catalans voted for a breakaway in the independence referendum that central authorities in Madrid declared illegal and that has resulted in widespread violence. 
 In Monday's letter, Puigdemont also called on Spanish authorities to halt "all repression" in Catalonia, referring to a police crackdown during the referendum that left hundreds injured. 
