__HTTP_STATUS_CODE
200
__TITLE
Mexicans Shed Tears for Hundreds Killed in Earthquake
__AUTHORS
NBC News
__TIMESTAMP
Sep 25 2017, 11:00 am ET
__ARTICLE
A woman prays during the funeral of Maria Ortiz Ramirez, who died in one of the flattened buildings in the Roma Norte neighborhood of Mexico City on Sept. 24, 2017.
The 7.1 magnitude earthquake rocked the heart of the mega-city, toppling dozens of buildings and killing more than 300 people.
People accompany caskets holding the bodies of victims who died in an earthquake through the streets in Atzala, on the outskirts of Puebla, on Sept. 20.
Mariachis pray during a wedding ceremony in an empty lot in front of a church that was collapsed by the recent earthquake in Atzala, on Sept. 24
Parishioners pray during a Mass remembering the victims of the recent 7.1-magnitude earthquake, at the Basilica of Guadalupe in Mexico City on Sept. 24.
As the search continued Sunday for survivors and the bodies of people who died in quake-collapsed buildings, specialists have fanned out to inspect buildings and determine which are unsafe after Tuesday's powerful earthquake that killed more than 300 people.
Family members and friends attend the funeral of a woman who was killed in the earthquake in Jojutla de Juarez on Sept. 21.
A family attends the wake of an earthquake victim, under a tarpaulin serving as a makeshift shelter, in Jojutla, Morelos state on Sept. 20.
A man with a helmet reading "Let's go, Mexico" sits in front a street altar near a collapsed building in the Condesa neighborhood of Mexico City on Sept. 24.
As rescue operations stretched into day 6, residents throughout the capital have held out hope that dozens still missing might be found alive.
PHOTOS:Rescuers Tirelessly Search For Earthquake Victims in Mexico City
People cover caskets holding the bodies of victims who died in the earthquake with dirt in Atzala, on the outskirts of Puebla, on Sept. 20.
Volunteers stand next to a cross erected in memory of people killed when a factory building collapsed in Mexico City on Sept. 23.
A crucifix, recovered from a collapsed church, is held up by ropes inside an auditorium during a Mass, in Tepeojuma, on Sept. 24.
On the first Sunday after the earthquake, priests no longer able to say Mass inside collapsing churches instead held services paying homage to victims and survivors outside or in other buildings.
A child prays in front a street altar near a collapsed building in Amsterdam Street in the Condesa neighborhood of Mexico City on Sept. 24.
PHOTOS:Desperate Rescuers Dig Through Rubble After Powerful Mexico Quake
