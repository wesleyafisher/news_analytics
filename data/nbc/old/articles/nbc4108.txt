__HTTP_STATUS_CODE
200
__TITLE
Dove Apologizes for Racially Insensitive Facebook Advertisement 
__AUTHORS
Kalhan Rosenblatt
__TIMESTAMP
Oct 8 2017, 11:38 am ET
__ARTICLE
 Soap company Dove has apologized for a racially insensitive Facebook ad it said "missed the mark in representing women of color thoughtfully." 
 The advertisement, apparently for some sort of soap but which has since been deleted, showed a black woman wearing a brown shirt removing her top to reveal a white woman in a lighter top. A third image shows the white woman removing her shirt to show a woman of apparently Asian descent. 
 On Saturday, Dove, which is owned by Dutch-British transnational consumer goods company Unilever, issued an apology on its Twitter page for the advertisement. 
 "An image we recently posted on Facebook missed the mark in representing women of color thoughtfully," the apology read. "We deeply regret the offense it caused." 
 On Facebook, Dove posted a similar statement, saying the feedback the company received from the image would help guide their decisions in the future. 
 Dove is committed to representing the beauty of diversity. In an image we posted this week, we missed the mark in thoughtfully representing women of color and we deeply regret the offense that it has caused. The feedback that has been shared is important to us and well use it to guide us in the future, the statement read. 
 Unilever and Dove did not immediately respond to NBC News' request for comment. 
 By Sunday morning, Dove's Facebook and Twitter pages were filled with a litany of backlash from consumers. 
 "This is gross. You think people of color can just wash away their melanin and become white? What were you going for, exactly? Your creative director should be fired," one Angela Reinders wrote on Facebook. 
 Mary Braden, of Baltimore, Maryland, wrote, "That's not missing the mark, that's ensuring that I will never buy another Dove product and I will encourage every one I know to join me." 
 "This is the most non-apology apology I've seen all week. Are you joining the Trump administration now? WTF is that ad even supposed to mean?" Sonia Gupta tweeted at the company. 
 A screenshots of the advertisement was first shared by American makeup artist Naomi Leann Blake, which went viral and was shared thousands of times on Facebook and Twitter. 
 Blake did not immediately respond to a request for comment. 
 The now-deleted advertisement is not the first time Dove has come under fire for being perceived as racially insensitive. In 2011, a controversial ad showed three women standing in front of a wall designated in before and after. 
 The woman standing in front of the before image had dark skin, a woman in between had medium-toned skin and the woman in front of the after image was white. 
 Dove at the time clarified the ad's intent in a statement from its PR team. 
 "All three women are intended to demonstrate the 'after' product benefit. We do not condone any activity or imagery that intentionally insults any audience," the statement read. 
 At the time, Dove said it was committed to featuring realistic and attainable images of beauty in its advertising. 
