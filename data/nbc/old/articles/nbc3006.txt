__HTTP_STATUS_CODE
200
__TITLE
Obama: Climate Change May Fuel Spread of Diseases
__AUTHORS
Bill Briggs
__TIMESTAMP
Apr 8 2015, 6:00 am ET
__ARTICLE
 Meaner allergy seasons and nastier asthma attacks in the U.S. are already symptoms of climate change but looming next is the advance of once-distant diseases to American soil, President Barack Obama warned. 
 In an interview with NBC News medical contributor Dr. Natalie Azar, the president was asked if he shares the concern of some who believe climate change has hastened the spread of insect-borne diseases like West Nile Virus  the first domestic cases of which emerged in New York City in 1999. 
 "It is inevitable that if temperatures rise ... certain diseases that traditionally have been localized in warmer climates are going to start creeping up into more temperate climates," Obama said. "That's just adaptation by insects and other critters that spread disease." 
 He added: "It's a significant concern. We haven't had to deal with things like dengue fever in the past, but if you go south, into Central America, that's a problem. That traditionally is a problem in higher elevations or in jungles. If, suddenly, you start seeing those in cities, that means that they're more likely to spread here." 
 In fact, dengue fever, a mosquito-borne illness, already has spread to parts of the southern U.S. and has killed some Americans. 
 On Tuesday, the president convened a roundtable of leading doctors and environmental experts to discuss the impacts of climate change on public health. Obama also announced a series of actions to prod governments and businesses to better monitor and address the issue. 
 "Temperature's rising. We know that there's going to be more severe wildfires, which mean particulates in the air, so that the allergy season gets extended," Obama told NBC News. 
 "We know that longer summers, hotter summers mean potential heatstroke and impacts on communities, whether it's asthma or insect-borne diseases. 
 "What's exciting is that you're seeing doctors, nurses  medical schools  really trying to raise awareness so that people recognize climate's not just some abstract thing that is off in the distance. This is something that's having a potential impact on our kids right now," Obama said. 
 The administration's new initiative to bring a fresh wave of professionals into the climate flight includes a White House visit later this week for educators who infuse their teaching time with lessons on climate change. 
 In fact, the president is largely positioning this fight as a battle to protect America's children from hazards he asserts are related to climate change. 
 "The good news is there's something you can do about it," Obama said. 
 "If we are able to move forward on the power plant rule so that the big power plants that produce electricity are not emitting as much carbon, as well as taking out things like mercury ... that means our kids are going to be healthier," the president added. "And we save money in the long-term because if you have one child who has three, four, five visits to the emergency room for asthma, somebody's paying for that." 
