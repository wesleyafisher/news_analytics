__HTTP_STATUS_CODE
200
__TITLE
Are You Saving Enough for Retirement?
__AUTHORS
Kelli B. Grant, CNBC
__TIMESTAMP
Apr 17 2016, 12:12 am ET
__ARTICLE
 Are you on track to actually retire? 
 Savings benchmarks based on age and income can offer a quick assessment  but you shouldn't entirely base your plan on them. 
 To pull out a few examples, JPMorgan Asset Management's 2016 Guide to Retirement reports that someone age 40 with an annual household income of $100,000 should have 2.6 times that amount put away for retirement. By age 60, the bank estimates, that multiple should be 7.3. At Fidelity, the latest "savings factors" released in fall 2015 call for a 40-year-old worker to have saved an amount equivalent to three times his salary, and by age 60, eight times salary. (See charts below for others.) 
 Hitting those multiples can seem like a tall order. Despite swelling ranks of 401(k) millionaires, many people have saved far less. A quarter of workers say their family has less than $1,000 in savings and investments, according to the 2016 Retirement Confidence Survey from the Employee Benefit Research Institute and Greenwald & Associates. Only 14 percent said their family has set aside $250,000 or more. 
 That's exactly the point of benchmarks, the banks say. Many consumers don't take advantage of more in-depth retirement calculators and other tools, so they don't know just how close or far off they are to meeting their savings goals, said Katherine Roy, chief retirement strategist for JPMorgan Asset Management. Benchmarks offer a quick litmus test. 
 "It's a great way to create an immediate recognition that you may not be where you need to be, and that you need to sit down and create a more comprehensive plan," she said. 
 That might mean escalating your contributions, or socking away more of your next raise, said John Sweeney, executive vice president of retirement and investing strategies for Fidelity. 
 "Think about not expanding your lifestyle to accommodate the new income," he said. 
 Consumers who are hitting or exceeding benchmarks could take that as an indication it's time to revisit their plan with an eye to funding other life goals, he said. 
 But advisors caution that benchmarks  like blanket estimates that you might need $1 million, $2.5 million or more for a solid retirement  easily miss the mark. The underlying assumptions about investment returns, income needs and years in retirement aren't one-size-fits-all, said certified financial planner Cathy Seeber, a partner at Wescott Financial Advisory Group in Philadelphia." 
 The only time they would ever work is if everything else in your life is status quo," she said. "It rarely is. 
 "Someone's "magic number" is highly personalized, said certified financial planner Clark Randall, founder of Financial Enlightenment in Dallas. 
 You might need more or less than a benchmark says you do depending on things including your health, anticipated retirement age and lifestyle in retirement. "Your pre-retirement income might have no bearing whatsoever on your post-retirement needs," he said. 
 If you do consult benchmarks, don't be surprised to see the goal numbers shift. 
 "The objective is not to try to change the goal posts on folks quarterly," said Sweeney, but to incorporate new trends and data. 
 RELATED: Is a Mandatory Retirement Plan in Your Future? 
 Fidelity tweaked its equation last fall to factor in consumers' longer lifespan and the rising full retirement age of 67 for Social Security. The latest version of JPMorgan's assessments lowered pre-retirement return assumptions from 7 percent to 6.5 percent, with the net effect of higher benchmarks. 
 "If the market won't work as much for you, the one variable you can control is how much you save," Roy said. 
 Check your retirement savings progress at least on an annual basis and during times of major transition (say, getting married or divorced or switching jobs). "You would drive people crazy if you try to [check in] too often," Seeber said.For example, market volatility might mean you're hitting a benchmark one day, and short the next week. 
 "It's a trend line you're looking for," she said. 
