__HTTP_STATUS_CODE
200
__TITLE
ISIS Fighters Claim Attack on Kirkuk, Iraq
__AUTHORS
__TIMESTAMP
Oct 21 2016, 6:34 am ET
__ARTICLE
 ERBIL, Iraq  ISIS attacked targets in and around the Iraqi city of Kirkuk early Friday, a likely attempt to divert attention from the the U.S.-supported battle to retake Mosul from the extremists. 
 ISIS suicide bombers stormed a power plant around 30 miles from the city, killing three Iranian engineers and eight Iraqi technicians before being shot dead, a senior Iraqi security official told NBC News. 
 Inside Kirkuk, militants armed with guns and grenades attacked multiple government sites, and security forces surrounded an unknown number of fighters inside one downtown building, according to Iraq's military. 
 Multiple explosions rocked the city and gunbattles were underway, witnesses in Kirkuk told The Associated Press, speaking on condition of anonymity as they were concerned for their safety. 
 Both the government and the ISIS-run Amaq news agency said the extremist group were behind the attack. 
 Kurdish television channel Rudaw broadcast footage of smoke rising over the city and gunfire could be heard as of 11:30 a.m. local time (4:30 a.m. ET). 
 There were no immediate reports of casualties from inside Kirkuk. 
 The attack comes as the Iraqi government and Kurdish forces are making a major push to drive ISIS militants from Iraq's second-largest city of Mosul. 
 The Iraqi military said that some of its elite counterterrorism units, as well as Kurdish peshmerga forces, were being drafted into Kirkuk to maintain security. Both have been heavily involved in the Mosul offensive. 
 Kirkuk is an oil-rich city some 180 miles north of Baghdad that is claimed by both Iraq's central government and the country's Kurdish region. It has long been a flashpoint for tension and has been the scene of multiple attacks by ISIS. 
 Iraqi and Kurdish forces backed by U.S.-led coalition support launched a multi-pronged assault this week to retake Mosul and surrounding areas from ISIS. The operation is the largest undertaken by the Iraqi military since the 2003 U.S.-led invasion. 
 Iraqi officials said they had advanced as far as the town of Bartella, nine miles from Mosul's outskirts, by Thursday. 
 
