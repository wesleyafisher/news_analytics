__HTTP_STATUS_CODE
200
__TITLE
Puerto Rico Crisis: San Juan Mayor Pleads for Federal Aid, Trump Hits Back
__AUTHORS
Phil Helsel
Saphora Smith
__TIMESTAMP
Sep 30 2017, 4:12 pm ET
__ARTICLE
 The mayor of Puerto Rico's largest city was rebuked by President Donald Trump on Saturday after pleading for more federal assistance in the wake of Hurricane Maria. 
 "We are dying, and you are killing us with the inefficiency and the bureaucracy," San Juan Mayor Carmen Yuln Cruz said Friday at a news conference. She highlighted donations from companies and others, including 200,000 pounds of food donated by Goya, as a contrast to federal help. 
 "This is what we got last night: four pallets of water, three pallets of meals and 12 pallets of infant food  which I gave them to the people of Comerio, where people are drinking off a creek," she said. "So I am done being polite. I am done being politically correct. I am mad as hell." 
 Criticism has been mounting over the Trump administrations response to what is being called an unfolding humanitarian crisis, with some likening the situation to the aftermath of Hurricane Katrina. 
 However, Trump hit back early Saturday, accusing Cruz of "poor leadership ability" and criticizing Puerto Rican officials. 
 "They want everything to be done for them when it should be a community effort," he tweeted. "10,000 federal workers now on island doing a good job." 
The Mayor of San Juan, who was very complimentary only a few days ago, has now been told by the Democrats that you must be nasty to Trump.
...Such poor leadership ability by the Mayor of San Juan, and others in Puerto Rico, who are not able to get their workers to help. They....
...want everything to be done for them when it should be a community effort. 10,000 Federal workers now on Island doing a fantastic job.
 Hurricane Maria knocked out power to most of Puerto Rico when it struck as a Category 4 storm on Sept. 20. The storm came on the heels of Hurricane Irma, which skirted past the island but still knocked out electricity to more than 1 million. 
 Some 18 people have been confirmed dead in Puerto Rico in the wake of the storm, while 16 died on the island of Dominica and one on the French territory of Guadeloupe. 
 Related: Delayed Response to Puerto Rico Has Echoes of Katrina for Some 
 Initially, the Trump administration named a one-star general to run U.S. military operations, then upgraded the command to three-star Lt. Gen. Jeffrey Buchanan, who headed to Puerto Rico on Thursday. 
 Cruz told MSNBC on Saturday that she wasn't saying anything "nasty" about Trump. 
 Gen. Buchanan, a three-star general, has said that he doesnt have enough troops and he doesn't have enough equipment," the mayor said, adding, "So who am I? I'm just a little mayor from the capital of San Juan. This is a three-star general saying he doesn't have the appropriate amount of tools." 
 An Army spokesperson said that as of 4 p.m. ET Friday the Army had more than 4,900 soldiers and Army Corps of Engineers civilian personnel in Puerto Rico and the U.S. Virgin Islands, which were also hit by the hurricane. Thirty aircraft and more than 500 trucks are committed to relief efforts, the spokesperson said. The National Guard said it projects 1,400 guard forces will be sent to Puerto Rico over the next four days. 
 The Federal Emergency Management Agency and its partners have distributed close to 2 million liters of water and 1 million meals throughout the island, Alejandro De La Campa, FEMA's federal coordinating officer, said during a teleconference Saturday. 
 De La Campa said that relief had to be air dropped or sent in via helicopter for some of the municipalities on the island, such as Utuado, Maricao and Las Maras. 
 "It is very difficult to get to some isolated areas," he said. 
 Related: Puerto Ricans Document Shattered Lives 
 Only 5 percent of electricity had been restored to the island, with around 33 percent of telecommunication and close to 50 percent of running water services restored as of Saturday morning, he added, citing the governor's office. 
 Cruz said help has not been reaching residents quickly enough. She said FEMA "has collapsed" in Puerto Rico. 
 "I cannot fathom the thought that the greatest nation in the world cannot figure out logistics for a small island of 100 miles by 35 miles," she said. "If we don't get the food and the water into people's hands, we are going to see something close to a genocide," she said later. 
 "I am asking the president of the United States to make sure somebody is in charge that is up to the task of saving lives," Cruz said. 
 Trump announced that he plans to visit Puerto Rico on Tuesday with first lady Melania Trump. 
 Cruz told MSNBC that she would be "very glad" to meet with him if he wants. 
 "But again, this is about saving lives," she said. "One can visit as a photo-op or one can visit to make sure things get done in the right way." 
 FEMA Administrator Brock Long said on MSNBC Friday that progress is being made, and that airport and port capacity are increasing. 
 "We are making progress, every day capacity is coming open," he said. 
 "We've worked to clear 11 highways, were continuing to push forward and open up those arteries to be able to pump more in," Long added. "The bottom line is that the capacity will continue to increase." 
 Trump continued to praise the federal government's response after the hurricane  the third major storm in a row to require an all-hands response after Hurricane Harvey hit Texas and Louisiana in August and Irma raked Florida earlier this month. 
 We have done an incredible job, considering theres absolutely nothing to work with, Trump said, adding that the power grid is gone." 
 Vice President Mike Pence tweeted Friday that he had called the governor of Puerto Rico, Ricardo Rossell, "to ensure we're doing all we can to back his recovery efforts." 
 The vice president also announced that he would travel to Puerto Rico and the U.S. Virgin Islands next week. 
Trump on Puerto Rico relief: "We have done an incredible job, considering there's absolutely nothing to work with" https://t.co/fRuw7TZ0PR
 
