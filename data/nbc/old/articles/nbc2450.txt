__HTTP_STATUS_CODE
200
__TITLE
Harrowing Scenes from the Front Line as Kurds Battle ISIS
__AUTHORS
NBC News
__TIMESTAMP
Oct 30 2016, 9:30 am ET
__ARTICLE
Peshmerga soldiers gather the night before an offensive starts to retake Mosul, near the Ba'ashiqah front line in Iraq on Oct. 19.
The Mosul campaign aims to crush the Iraqi half of the caliphate declared by ISIS in Iraq and Syria.
Photographer Harry Chun traveled across the front line near Mosul with fighters from the Kurdistan Freedom Party (PAK), a Kurdish militant group affiliated with the Peshmerga, as they engaged with ISIS.
A soldier named Awara sings with fellow PAK fighters the night before the offensive.
Awara was killed the next day when the unit was ambushed by ISIS.
He was known to have a fine voice and often led other PAK soldiers in song to boost morale.

Kurdish Peshmerga and Iraqi army soldiers wait to cross the front line on Oct. 20.
Members of the PAK unit advance toward the first village after crossing the front line.
The photographer writes, "the day started out smoothly [with other Peshmerga units] easily taking 4-5 villages without much resistance."
Helo Remshti, a junior officer for the PAK unit, returns from a meeting with Peshmerga leaders.
Remshti lost his arm a few months ago while trying to defuse an IED around Hawija, in the Kirkuk province, where he was based before moving the unit to the Mosul front line.
A PAK soldier rests between bursts of fighting against ISIS.
Awara lays on the ground after being shot in the head. He died on the spot.
"It was madness with a mixture of crying, more bullets and screaming on the radio asking for enforcement and help."
Ardalan, another PAK soldier, is treated by fellow soldiers after being shot in the head.
"Ardalan asked me to step aside so he could position himself to shoot back from where I stood. Still lots of bullets. I looked back to see where the tanks and more armored cars are and the moment I turned around Ardalan got hit and fell. The bullet hit through the top of his head. 
Three other soldiers dropped their guns to come and aid him resulting in less defense and more intense shooting from ISIS. We all thought Ardalan was also dead as he was was bleeding uncontrollably, but he survived."
A PAK soldier is overcome with emotion as they evacuate the two soldiers.
The sister of Awara, the slain PAK soldier, is held by other mourners during her brother's funeral in the mountains near Irbil, Iraq, on Oct. 26.
"Many soldiers took a break from the front line to attend [the funeral] but there were still 200 others who remained and continued fighting. 
The unit and Awara's fellow soldiers asked me to honor his death by sharing the story."
PHOTOS:Iraqi Families Flee Mosul as Army Battles ISIS

