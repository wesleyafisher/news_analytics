__HTTP_STATUS_CODE
200
__TITLE
The Century Old Heart Test That May Predict Sudden Cardiac Death
__AUTHORS
Parminder Deo
Lauren Dunn
John Torres, M.D.
__TIMESTAMP
Aug 22 2017, 5:43 pm ET
__ARTICLE
 It was a typical day for 43-year-old traveling nurse, Lynn Howard. She was visiting a patient at their home when she suddenly felt dizzy and lost consciousness. The Buffalo native woke up a few days later in a hospital bed with a breathing tube. 
 Earlier this month, the mother of three suffered a sudden cardiac arrest  a condition so deadly that more than 90 percent of people dont survive. Clinically, she died. But luckily for Howard, the patient she was caring and her husband were paramedics. They started CPR and saved her life. 
 I can't wrap my brain around it still, I had no health problems before this, said Howard. I was at the right place at the right time with two paramedics right in front of me. It's the only way I survived. 
 Unlike a heart attack which occurs when one or more coronary arteries are blocked, a sudden cardiac arrest occurs following a short circuit in the heart. According to the American Heart Association, this electrical problem strikes more than 350,000 people each yearabout 1,000 every day. 
 The only way to prevent a sudden cardiac arrest is with the use of an implantable defibrillator which shocks the heart back to a normal rhythm. Until recently, no test can predict when a sudden cardiac arrest will happen and whos at high risk for needing a defibrillator. 
 Related: Why Heart Attacks Are Striking Healthy Young Women 
 By the time cardiac arrest happens, its way too late, said Dr. Sumeet Chugh, associate director of Genomic Cardiologyat Cedars-Sinai Heart Institute. Every minute that passes there's a ten percent chance of dying. Ten minutes and you're gone. 
 For decades, Chugh has been working to solve the problem of predicting the unpredictable. Now, after studying thousands of patients, he has discovered a solution that involves an inexpensive century old test called an electrocardiogram, or EKG, which displays the hearts electrical activity. 
 Chugh looks at six data points on an EKG tracing that reveals important information ranging from how fast the heart is pumping to its electrical activity. This key information is then used to assign a risk score ranging from zero (normal) to six (severe). A score above four indicates a 20-fold increase in the risk of suffering a cardiac arrest requiring a defibrillator. 
 Related: Too Many People Stop Their Lifesaving Statins, Doctors Say 
 Half the patients who are going to have a sudden cardiac arrest, will have some kind of warning while the other half won't, said Chugh. Imagine half of them are going have a cardiac arrest as the first unexpected sign of heart disease. 
 The hope is that in the future, anytime a patient receives an EKG at a doctors office, it will show their risk of having a sudden cardiac arrest, according to NBC News Medical Correspondent Dr. John Torres. If the score is high, then a person will be referred to a cardiologist for further evaluation which may require an implantable defibrillator, even if they are currently healthy. 
 Howard didnt know she was at risk. She had a clean bill of health her entire life and never imagined that a defibrillator would be implanted in her chest. Shes back home now, getting stronger every day, and thankful for the quick thinking of those two paramedics. 
 I feel so grateful for a second chance at life, said Howard. Having your risk profile assessed is important; anything to prevent this from happening to at least one person would be great. 
