__HTTP_STATUS_CODE
200
__TITLE
Ohio Couple Calls Out Heroin Overdose in Teen Daughters Obituary
__AUTHORS
Associated Press
__TIMESTAMP
Sep 6 2015, 5:18 pm ET
__ARTICLE
 MIDDLETOWN, Ohio  Confronted with the sudden death of their 18-year-old daughter, Fred and Dorothy McIntosh Shuemake made a defiant decision: they would not worry about any finger-pointing, whispers or family stigma. 
 They directed the funeral home to begin Alison Shuemake's obituary by stating flatly that she died "of a heroin overdose." They aren't the first grieving American parents to cite heroin in an obituary as such deaths nearly quadrupled nationally over a decade, but it's rare, even in a southwest Ohio community headed toward another record year in heroin-related deaths. 
 Related: As Heroin Epidemic Grows, So Does Rehab Wait  
 "There was no hesitation," Dorothy said. "We've seen other deaths when it's heroin, and the families don't talk about it because they're ashamed or they feel guilty. Shame doesn't matter right now." 
 Her voice cracked as she sat at a table covered with photos of Alison: the high school diploma earned this year, awards certificates, and favorite things such as her stuffed bunny named Ashley that says "I love you" in a voice recording Alison made as a small child. 
 "What really matters is keeping some other person, especially a child, from trying this ... We didn't want anybody else to feel the same agony and wretchedness that we're left with," she said. 
 A search of "heroin" on the Legacy.com site with obituaries from more than 1,500 newspapers found only a handful, including Alison's, in the last month. 
 Related: White House Announces Program to Combat Rise in Heroin Deaths 
 Alison's obituary calls her a "funny, smart, gregarious, tenacious and strong-willed teenager with gusto." 
 Alison had recently joined a salon staff after being recruited by a manager who admired the way she did her hair and makeup. 
 She and her boyfriend Luther both had two jobs and moved into an apartment together a few weeks ago. 
 The Shuemakes were expecting the couple over to do laundry the night of Aug. 25. When they didn't show up, Dorothy phoned and texted without answer. At about 3:30 a.m., their roommate called: "Something's wrong." 
 She rushed over to the apartment and saw immediately both were "definitely gone." She spotted a needle on the floor. 
 Before Alison's obituary was published, her mother called her boyfriend's family to let them know of the plan to name heroin in her obituary. They had no objection. 
 A few days later, his was published. 
 It began: "Luther David Combs, 31, of Middletown, passed away Wednesday, Aug. 26, 2015, of a heroin overdose." 
