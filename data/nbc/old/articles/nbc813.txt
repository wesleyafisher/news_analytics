__HTTP_STATUS_CODE
200
__TITLE
Candidates in Georgia Special Election Brace for Final Weekend
__AUTHORS
Beth Fouhy
__TIMESTAMP
Jun 17 2017, 12:12 pm ET
__ARTICLE
 ALPHARETTA, Ga.  To hear him tell it, Jon Ossoff  the Democrat vying to fill a House seat here in next Tuesday's special election  isn't even thinking about the seismic implications of what many consider the first major referendum on Donald Trump's presidency. 
 "I think this race is about who can deliver for this community more than its about national politics," Ossoff told NBC News heading into the campaign's final weekend. "There are many in the community who do have serious concerns about the direction the administration is taking us in and Im one of them and those concerns have only grown over time. But fundamentally what people want from their representative is the kind of results that improve quality of life." 
 The race to succeed Republican Rep. Tom Price, who gave up the seat to become Trump's Health and Human Services secretary, pits Ossoff, 30, a former congressional aide turned documentary film maker, against Karen Handel, 55, the former Georgia secretary of state and longtime fixture in state Republican politics. 
 The final weekend promised to be a hot and steamy sprint for both candidates, thanks both to Georgia's June temperatures and to the high stakes surrounding the contest. 
 Handel headlined a rally Saturday with Price and former Georgia Gov. Sonny Perdue, who now serves as Trump's agriculture secretary. Speaking to some 200 attendees, Handel promised she'd hang onto the seat Tuesday and "rock Nancy Pelosi's world." 
 Perdue, a feistier campaign presence than Handel, urged Republicans to turn out for her in big numbers while acknowledging the elephant in the room keeping the election close. 
 "Some Republicans," Perdue said, "may be turned off by our president." 
 Handel has staked out delicate balancing act with Trump, whose rocky tenure thus far has threatened his hold on the kind of educated, well off Republicans who populate the sixth district. She's embraced Trump's help with fundraising and rallying base supporters, while pledging to remain independent. 
 RELATED: Ossoff Leads Handel in Latest Poll in Georgia After Announcing $15 Million Haul 
 The race has dragged on for months  Ossoff narrowly missed a chance to win outright in April, running against a multi-candidate GOP field and coming a few thousand votes short of the necessary 50 percent threshold  and it's become by far the most expensive House race ever waged, with Ossoff raising an astonishing $23.8 million, most of it from outside the district. 
 Handel has raised far less than Ossoff but has been buoyed by significant support from conservative super PACs and other outside groups. She's seized on Ossoff's massive national fundraising haul to cast her rival as a Nancy Pelosi acolyte who doesn't live in the district. (Ossoff grew up here but currently lives outside the district while his fiancee completes medical school.) 
 Handel's message has been effective in motivating Republicans, many of whom have grown impatient with the political paralysis in Washington since Trump's election. 
 Ossoff "doesnt live here, he's supported financially by people out of the Georgia area, he doesnt have the conservative views that I think are important for our country at the present time," Republican voter Dana Hawkins told NBC News outside an early-voting station. "I think Karen Handel will at least try to engender the policies Trump is trying to get through the nation  bills and reform that Trump is trying to get accomplished." 
 Trump barely won the district in November, and his popularity has faltered here as voters have expressed concerns about the health care plan crafted by the GOP House that is now making its way through the Senate. 
 Ossoff, for his part, has resisted being cast as the lead Democratic player in an anti-Trump proxy battle. His campaign message focuses largely on delivering for the district and ridding the nation's capital of its intense polarization. 
 "Im focused on what people here at home are concerned with  the state of our local economy, reaching our economic potential, access to health care especially for women and folks with pre-existing conditions and the kind of accountability thats lacking in Washington," he said. 
