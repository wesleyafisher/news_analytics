__HTTP_STATUS_CODE
200
__TITLE
North Korea Also Has Nerve Agent VX, Chemical Weapons Expert Warns
__AUTHORS
Nick Bailey
Michele Neubert
__TIMESTAMP
Sep 24 2017, 4:40 am ET
__ARTICLE
 LONDON  Amid a flurry of missile tests and inflammatory rhetoric, the worlds attention is focused on North Korea's nuclear program. 
 But one expert believes the rogue state's stockpile of chemical weapons could also bring catastrophic consequences. 
 The Center for Nonproliferation Studies estimates North Korea has between 2,500 and 5,000 metric tons of chemical weapons. 
 In particular, it has a large supply of VX, the deadliest nerve agent ever created; last year it was used to assassinate Kim Jong Uns half-brother, Kim Jong Nam, at Kuala Lumpur airport. 
 The chemical stockpile could harm thousands of people if it were attached to a missile or if it ended up in the hands of Islamist extremists, according to Hamish de Bretton-Gordon, former commanding officer of the U.K. Chemical, Biological, Radiological and Nuclear Regiment (CBRN) and NATOs Rapid Reaction CBRN Battalion. 
 The chance that North Korea might provide jihadis with some of their chemical or nuclear capability is a huge concern at the moment, he said. What some people forget ... is that in 2006 North Korea helped [Syrian President Bashar al-] Assad and his regime set up their own nuclear program which was destroyed by the Israelis. But only as recently as a few weeks ago, the Organization for the Prohibition of Chemical Weapons intercepted two North Korean ships heading towards northern Syria with equipment to make chemical weapons. 
 De Bretton-Gordon has described VX as "the most toxic chemical weapon ever produced," highlighting that even a "microscopic amount" can prove deadly. VX also featured in the 1996 action thriller "The Rock." 
 It's banned under several international conventions and was designated a weapon of mass destruction by a U.N. resolution in April 1991. Its origins date back to the early 1950s, when a British scientist named Ranajit Ghosh was researching pesticides and developed the "V-series" of nerve agents  the V stood for "venom." 
 De Bretton-Gordon, who now works for military supplier Avon, fears impoverished Pyongyang could be more tempted to sell its chemical stockpile as it grapples with toughening global sanctions. 
 "We know that the jihadis have a lot of money and only last year tried to buy a highly enriched uranium from Russian criminals for $40 million a kilogram," he said. "So, would Kim Jong Un sell deadly VX for $40 million a kilogram? I think absolutely they would the more that they get pushed." 
 However, Professor Hazel Smith at London's School of Oriental and African Studies (SOAS) says that would be a major change in policy for the North Korean regime. 
 "Historically North Korea values state sovereignty and doesn't value interactions with non-state entities such as ISIS and al Qaeda," she said. "Given the level of surveillance over their shipping activities it's also unlikely they would be able to, or try to transport weapons." 
 She says the regime would be more concerned right now with protecting its oil imports, which are still flowing despite economic sanctions. 
 There also fears that North Korea could put VX to use itself. Japanese Prime Minister Shinzo Abe has warned of that possibility, and Pyongyang's recent successful missile launch tests and nuclear tests have heightened the likelihood of chemical warfare in any conflict on the Korean peninsula. 
 "I think we now know that they have 5,000 tons of VX," de Bretton-Gordon said, speaking to NBC News at the Defence and Security Equipment International conference in London. "We know they have missiles capable of firing 4,000 to 6,000 miles, probably with a payload of half a ton, so half a ton of VX in those missiles could kill tens of thousands of people, and they could do that now, so that is a genuine concern." 
 He added: "We are focusing on the nuclear ... but whatever military option there is [for dealing with] North Korea, it must include mitigating and destroying that very large stock of VX that we know of. 
 But Smith says chemical weaponry doesn't form part of the regime's strategic plans. 
 "Were there to be an escalation of the current crisis, there would next be the use of conventional weapons. [North Korea] would not need chemical weapons for an attack on Seoul [and] if it did ... it would invite a wholesale global response to any military conflict between North and South Korea." 
 North Korea has said in public statements that it wants an official end to the Korean War, which was halted by a 1953 armistice but not ended by peace treaty. It also wants nothing short of full normalization of relations with the U.S. and to be treated with respect and as an equal in the global arena. 
 
