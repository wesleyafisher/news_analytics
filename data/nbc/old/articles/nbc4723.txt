__HTTP_STATUS_CODE
200
__TITLE
Amb. Nikki Haley: Youre Going to See Us Stay in Iran Nuclear Deal
__AUTHORS
Kailani Koenig
__TIMESTAMP
Oct 15 2017, 12:37 pm ET
__ARTICLE
 WASHINGTON  The U.S. ambassador to the United Nations, Nikki Haley, on Sunday said it's the administration's hope that America stays with the Iran nuclear deal if Congress takes action to keep it together. 
 I think right now you are going to see us stay in the deal, she said during an interview on NBC's Meet The Press." 
 "What we hope is that we can improve the situation," she added. "And that's the goal. So I think right now, we're in the deal to see how we can make it better. And that's the goal. It's not that we're getting out of the deal. We're just trying to make the situation better so that the American people feel safer." 
 On Friday, President Trump declined to certify that Iran was in compliance of the 2015 agreement, and threatened to terminate it if Congress does not strengthen it. 
 Haley has been a long-time harsh critic of the agreement, and was one of the few voices in the Trump administration to encourage the president to declare Iran in violation. 
 "What we're trying to say is, 'Look, the agreement was an incentive. The agreement was for you to stop doing certain things,'" Haley added. "You haven't stopped doing certain things. So what do we do to make Iran more accountable so that they do?" 
 President Trump will be working "very closely with Congress," she said, "to try and come up with something that is more proportionate." 
 Haley also cited North Korea as an example of the kind of situation theyre trying to avoid with Iran, saying the whole reason we are looking at this Iran agreement is because of North Korea." 
 Secretary of State Rex Tillerson on Sunday also said the U.S. plans to "stay in" the agreement. 
 "The issue with the Iran agreement is, it does not achieve the objective," he said on CNN. "It simply postpones the achievement of that objective. And we feel that that is one of weaknesses under the agreement, so we're going to stay in. We're going to work with our European partners and allies to see if we can't address these concerns, which are concerns of all of us." 
 Haley on Sunday also didnt specifically deny tensions with Secretary of State Rex Tillerson during an interview on Sundays Meet The Press," but did try to distance herself from it. 
 Last week, a White House official told Politico that escalating friction between the two appeared to be reaching "World War III proportions"  following NBC News exclusive reporting that Tillerson had called Trump "moron." 
 "That is just so much drama," she said of the report. "I mean, it's really, it's all this palace intrigue." 
 Haley added that she feels "every member of the NSC [National Security Council]" works hard to put options on the table for the president, and that they share the common goal of keeping Americans safe. 
 But, she added, "I am glad to be living in New York just for that reason, is that I don't want to be near the drama and I don't want to be near the gossip." 
