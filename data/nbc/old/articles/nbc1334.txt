__HTTP_STATUS_CODE
200
__TITLE
#NBCLatino20: Exporter of Equality, Cesar Francia 
__AUTHORS
Carmen Sesin
__TIMESTAMP
Oct 11 2017, 12:10 pm ET
__ARTICLE
 When Cesar Francia left his native Venezuela as a teen, he never imagined that fifteen years later he would be involved in the fight for marriage equality in neighboring countries. 
 Weve been developing a legal strategy along with legal practitioners in Latin America to get those countries to move the needle on those issues, Francia, a 29-year-old attorney, told NBC Latino. 
 At the end of August, Chilean President Michelle Bachelet signed a proposal for same-sex marriage legislation that also allows couples to adopt children. The proposal is now in their Congress for debate. 
 None of this would have likely happened without Francia and a coalition of volunteer attorneys working pro bono with non-governmental organizations and LGBT rights organizations. Together they have been working to push for same-sex marriage rights in Latin America. 
 Francia and the other attorneys have also submitted amicus briefs to local courts in Colombia and Panama in support of petitioners seeking same-sex marriage rights there. Meanwhile, a lawsuit against the government of Mexico in support of marriage equality has been submitted to the Inter-American Commission. 
 Francia is in corporate law; yet he and his boss take the time to lead the American component of these volunteer efforts with the support of the Vance Center for International Justice of the New York City Bar Association. So we write them [amicus briefs], I translate them into Spanish and then we submit them on behalf of organizations who support same sex marriage, he said. 
 RELATED: Hispanic Heritage Month 2017: #NBCLatino 20 
 Francia was born in Caracas and lived there until he immigrated to Miami at 14. He learned English in one year, making it one of 4 four languages he now masters. While attending college at New York University, he wanted to join the Peace Corps but couldnt because he was not yet a citizen. So he came up with his own plan and spent time in Rwanda studying the Gacaca court system, which are community courts set up to speed up the trials of suspects implicated in the Rwandan genocide. 
 After college, he spent two years working at the U.S. Supreme Court as the aide to Sonia Sotomayor. 
 That changed my life  she is such an inspiration, the first Latina U.S. Supreme Court Justice, he said with a smile. 
 Sotomayor encouraged Francia to go to law school, so he took her advice and ended up at New York University School of Law. 
 Francia credits his mom, who he calls his role model, for his accomplishments. At 18, she was homeless, divorced, with two children, and had recently lost her own mother. But she quickly turned her life around. She got a technical degree and was hired at an accounting firm. Eventually, she bought the company and employed her family. 
 RELATED: #NBCLatino20: Broadway's Professor, John Leguizamo 
 My mother is an incredibly driven, entrepreneurial woman. What she has gone through and how she has been able to be so resilient is a constant source of inspiration, Francia said. 
 What is the best advice anyone has given you? Just ask. The question could be anything ask for help, ask for support, ask for input. And its incredible when you just ask how people respond and how people are willing to help. 
 What is your guilty Latino pleasure? I love dancing very much  So dancing to Marc Anthony on my own at home is something I do often. 
 The #NBCLatino20 honors achievers who are making our communities and our nation better. Follow their fascinating stories throughout Hispanic Heritage Month. 
 Follow NBC Latino on Facebook, Twitter and Instagram.  
