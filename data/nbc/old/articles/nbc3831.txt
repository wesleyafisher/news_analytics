__HTTP_STATUS_CODE
200
__TITLE
Halloween Retailer Pulls Anne Frank Costume After Backlash
__AUTHORS
Dan Corey
__TIMESTAMP
Oct 17 2017, 7:08 pm ET
__ARTICLE
 A children's Halloween costume representing Holocaust victim Anne Frank has been pulled from online retailers after receiving widespread condemnation from Jewish groups and others. 
 The "WW2 Anne Frank Girls Costume" featured a blue button-up dress, a green beret and a brown shoulder bag "reminiscent of the kind of clothing that might be worn by a young girl during WWII," the product description said below the image of a smiling child model with her hand on her waist. 
 The costume was priced at $24.99, not including a $4.99 shipping charge. 
 Online retailer HalloweenCostumes.com removed the costume from its website after it was called offensive and exploitive by major advocacy groups and individuals alike. 
 "We would like to apologize for any offense this has caused. Due to the feedback from our customers and the public, which we take very seriously, we have elected to stop selling this costume immediately," the retailer said in statement. 
 Ross Walker Smith, a spokesman for the company, tweeted that the retailer also sells costumes for non-Halloween purposes, such as school projects and plays. 
 Related: Political Home Decorations Reach a Whole New Level This Halloween 
 But even though HalloweenCostumes.com removed the costume from its website, the bundle was still available online as of Tuesday through third-party vendors under monikers like "1940's Wartime Fancy Dress" and "World War II Evacuee Costume." 
 Many Jewish advocacy groups swiftly condemned the costume portraying Frank, the Jewish teenager whose diary depicted her experience hiding from the Nazi regime with her family. 
 "Hard to see how this offensive idea made it this far, but thankfully this costume has been removed from the market," the Anti-Defamation League tweeted. 
 Related: A Look Into Anne Frank's Amsterdam Home Where She Wrote Her Diary 
 The Anne Frank Center for Mutual Respect said in a Facebook post: "Honoring Anne's legacy is at the heart of what we do. There are better ways to commemorate her memory than a Halloween costume. We are pleased that the company acted swiftly." 
 Frank died of typhus at age 15 in 1945 at a concentration camp in Germany. After two years of hiding, she and her family were captured by the Gestapo in 1944 after they were found behind a movable bookcase in a secret annex in Amsterdam. 
 Her father, Otto Frank, survived the war and spent the rest of his life searching to find who notified the Nazis of their whereabouts. "The Diary of Anne Frank" has been translated into 67 languages. 
