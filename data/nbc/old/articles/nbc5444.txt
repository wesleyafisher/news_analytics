__HTTP_STATUS_CODE
200
__TITLE
Firm Floats Plan to Hang Colossal Skyscraper From an Asteroid
__AUTHORS
David Freeman
__TIMESTAMP
Mar 28 2017, 3:02 pm ET
__ARTICLE
 Dont expect it to go up anytime soon, but a New York City-based design firm has floated a mind-bending plan for the erection of a skyscraper it bills as the worlds tallest building ever. 
 Dubbed Analemma, the fanciful tower wouldnt be built on the ground, but would be suspended in air by cables from an asteroid repositioned into geosynchronous Earth orbit just for the purpose. 
 Related: NASA's Bold Plan to Save Earth From Killer Asteroids 
 Over the course of each day, the floating skyscraper would trace a figure-eight path over our planets surface, according to plans posted online by Clouds Architecture Office. It would swing between the northern and southern hemispheres, returning to the same point once every 24 hours. 
 The speed of the tower relative to the ground would vary depending upon which part of the figure eight it was tracing, with the slowest speeds at the top and bottom of each loop, the plans say. The asteroids orbit would be calibrated so that the slowest part of the towers path would occur over New York City. 
 Clouds Architecture Office, which recently partnered with NASA to design a Martian habitat dubbed Mars Ice Home, envisions that the tower would be constructed in Dubai, site of the worlds tallest building, Burj Khalifa. 
 Analemma would be powered by solar panels and use recycled water. Lower floors would be set aside for business use, while sleeping quarters would be sited about two-thirds of the way up. The plans dont say exactly how people would get on and off the building, though one illustration seem to show people parachuting from the tower to the ground. 
 Anyone venturing to Analemmas highest reaches would be treated to extra daylight  about 45 minutes more at an elevation of 32,000 meters, according to the plans. Of course, tenants who ventured that high would need a spacesuit to go outside because of the near-vacuum and temperatures estimated to be about minus 40 Celsius. 
 Why on Earth would anyone want to build an off-the-Earth skyscraper? The plans spell that out: 
 "Harnessing the power of planetary design thinking, it taps into the desire for extreme height, seclusion and constant mobility. If the recent boom in residential towers proves that sales price per square foot rises with floor elevation, then Analemma Tower will command record prices, justifying its high cost of construction." 
 Presumably, much of the anticipated cost would come from repositioning the asteroid to which Analemma would be moored. No one has ever tried such a feat, although NASA has plans to do so with its Asteroid Redirect Mission. And as the plans point out, the European Space Agencys Rosetta mission showed in 2014 that its possible to rendezvous with a distant asteroid. 
 But not everyone is ready to jump aboard the Analemma bandwagon. 
 In an email to NBC News MACH, Carlo Ratti, director of MITs Senseable City Lab and an architect noted for futuristic thinking, called the tower a fun utopian mega-project but said a cable suspended from space would break under its own weight. 
 And beyond feasibility, Ratti expressed philosophical doubts about the very idea a suspended skyscraper. 
 I am not sure I would like to live in such a self-referential building, literally detached from the rest of the world, he said in the email. Cities are built to bring humans together, while a building such as the Analemma tower would do the opposite. 
 But to Ostap Rudakevych, a partner at Clouds Architecture Office, buildings like Analemma are all but inevitable. 
 "If you look at the history of architecture, buildings have been getting taller and lighter over the past thousands of years," Rudakevych told NBC News MACH in an email. "In the past two centuries we've had the aerial age and space age begin. So our belief is that it is only a matter of time before buildings become ungrounded." 
 Stay tuned. 
 Follow NBC MACH on Twitter, Facebook, and Instagram. 
 
