__HTTP_STATUS_CODE
200
__TITLE
Sexually Transmitted Disease Cases Hit New High in U.S.
__AUTHORS
Maggie Fox
__TIMESTAMP
Oct 19 2016, 3:18 pm ET
__ARTICLE
 More cases of sexually transmitted diseases were reported last year than ever before, federal officials said Wednesday  just as state and local health departments that could help fight them lose funding. 
 More than 1.5 million people were reported with chlamydia, the most common sexually transmitted disease (STD), the Centers for Disease Control and Prevention reported. 
 The CDC recorded nearly 400,000 cases of gonorrhea and nearly 24,000 cases of syphilis. 
 "The STD epidemic is getting worse in the United States and, in fact, is at its highest levels yet," said Dr. Jonathan Mermin, director of CDCs National Center for HIV/AIDS, Viral Hepatitis, STD, and TB Prevention. 
 The CDC reported a record year for STDs in 2014, also, but the trend is worsening, Mermin told NBC News. 
 "Last year was the first year that we saw increases but those increases are actually continuing and at a higher rate," Mermin said. 
 Related: 3 Common STDs Are Becoming Untreatable 
 The new numbers translate to a 19 percent increase in syphilis cases, a 13 percent rise in gonorrhea and a 6 percent increase in chlamydia, Mermin said. 
 While some of the new numbers may be due to better reporting of cases, most of the rise appears to be a real increase in new infections, he said. 
 Gay and bisexual men account for many of the new cases, and the biggest numbers are among young adults, especially those in their late teens and early 20s. "Half of all STDs occur in youth under age 20," Mermin said. 
 Part of the increase may be due to better treatment for HIV, which may make people believe  usually wrongly  they do not need to use condoms. CDC officials say. 
 At the same time, state and local health departments are losing funding. 
 "That is correlated with an eroding infrastructure for sexually transmitted disease clinics," Mermin said. "In 2012 alone, half of state public health programs had to close some of those clinics." 
 That means young people most vulnerable to new infection have fewer places to go for help, advice, testing and treatment, he said. 
 All three infections can be cured with antibiotics, but people often dont even know they are infected. In the early stages none of them cause obvious symptoms. 
 Mermin said talking openly about STDs can help. "Parents and providers and teachers can provide young people with safe and effective ways to prevent STDs," he said. "Good sex education prevents STDs." 
 Related: CDC Finds Ongoing, Severe Epidemic of STDs 
 And Mermin has a ready answer for people who fear talking about sex might create an expectation among youths that they should be having sex. 
 "There is no evidence that talking about sexually transmitted disease prevention increases sexual activity," he said. 
 Some pregnant women are falling through the gaps and having babies infected with syphilis at birth, the CDC statistics show. Mermin said theres no excuse for this. 
 "Clinicians can make STD screening a standard part of clinical care," he said. 
 "The way to prevent infants from dying of syphilis is to help women get diagnosed and treated when they are pregnant," Mermin said. 
 Congenital syphilis cases hit a record high in 2015, CDC said. More than 430 babies were born with syphilis. "Up to 40 percent of babies born to women with untreated syphilis may be stillborn, or die from the infection as a newborn," the CDC said. 
 Related: Survey Suggests 2 Million People Have Chlamydia 
 While some women get no prenatal care at all  about 22 percent of women whose babies were born infected had no prenatal care in 2014  others may be tested early in pregnancy but not later on, he said. 
 An earlier CDC report showed that 43 percent of pregnant women with syphilis who did see a doctor or other health professional while pregnant nonetheless got no treatment for the infection. 
 All pregnant women should be tested again in the third trimester of pregnancy, he said. 
 While some reports have linked dating and hook-up apps with an increase in STDs in some communities, Mermin said theres no hard evidence to show cause and effect. And he said such apps can be tools for good. 
 "The dating apps can also be vectors of prevention by increasing the amount of prevention information that people can see and also by linking people to STD and HIV testing," he said. 
