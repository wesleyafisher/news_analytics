__HTTP_STATUS_CODE
200
__TITLE
After Being Shot, Rep. Steve Scalise Still Opposes More Gun Control
__AUTHORS
Kailani Koenig
__TIMESTAMP
Oct 8 2017, 9:00 am ET
__ARTICLE
 WASHINGTON  House Majority Whip Steve Scalise, who is still recovering from his wounds after a gunman opened fire on Republican members of Congress in June, told Chuck Todd in an exclusive interview for Meet the Press that he still stands behind the unlimited right to bear arms. 
 Our Founding Fathers believed strongly in gun rights for citizens," said Scalise, R-La. 
 Instead of passing new regulations in the wake of the Las Vegas mass shooting, which thrust the countrys lawmakers into the center of the debate over gun control, existing ones should be enforced, he said. 
 Don't try to put new laws in place that don't fix these problems," said Scalise, who was gravely wounded on June 14 when a shooter opened fire on the GOP's congressional baseball team as members practiced in Virginia. "They only make it harder for law-abiding citizens to own a gun. 
 When asked if he believed the right to bear arms was "unlimited," Scalise said: "It is, it is." 
 During the shooting in June, Scalise was hit in the left hip but managed to drag himself into the outfield and was then rushed to a Washington hospital. The gunman was himself shot dead, while Scalise sustained grave injuries and required several operations. He returned to the House on Sept. 28 for the first time since the shooting, earning an emotional standing ovation from his colleagues. 
 Since the Las Vegas massacre, some top Republicans  including President Donald Trump, House Speaker Paul Ryan, and Majority Leader Kevin McCarthy  have indicated that they are open to looking at banning bump stocks  devices that enable semi-automatic weapons to fire like machine guns. The National Rifle Association backed new regulations on bump stocks after a dozen of the devices were found in the Las Vegas shooter's hotel room. 
 Sen. Dianne Feinstein, D-Calif., the sponsor of a bill aimed at banning bump stocks, called on Americans to consider the fate of those killed in Las Vegas. 
 Every American can look at, should look at those pictures and say, Do we want more of this? she said on "Meet the Press." This is one simple thing that stops the making of a semi-automatic weapon into a machine gun. 
 She admitted that she was not sure which specific laws would have prevented the shooting, however. 
 For his part, Scalise said he did not want Congress to examine the issue of bump stocks. 
 Minority Leader Nancy Pelosi already said she wants it to be a slippery slope, he said. She doesn't want to stop at bump stocks. They want to go out and limit the rights of gun owners. 
 He said the Bureau of Alcohol, Tobacco, Firearms and Explosives should revisit their 2010 decision that legalized bump stocks  a request also made by the NRA. 
 A week ago most people didn't know what a bump stock was, he said. So to think that we're now all experts and know how to write some ... panacea law  it's fallacy. Let's focus on the facts. Let's get the facts and let's go focus on some of the problems. 
 Scalise also said he was opposed to limits on the number of firearms a person could purchase during a given time. 
 Its dangerous for the concept that the federal government would have some kind of list of who has guns and what they have, he said. 
 When asked whether he agreed with laws banning automatic weapons, the congressman said that existing regulations hadn't resulted in a reduction of violence. 
 Some of the weapons that [current laws] included in there aren't really automatic weapons, said Scalise. You haven't seen a decrease in gun violence with those laws on the books. So go and enforce the laws that are there. 
 Scalise also said people across the country used firearms to protect themselves from criminals  an argument consistently used by gun rights advocates. 
 A member of the U.S. Capitol Police detail shot the gunman who opened fire on Scalise and his colleagues. 
 During his Meet The Press interview, Scalise said much good had come from the shooting, which he called "a bad act." 
 "The love, the support, the prayers from people all around the country  it touched me and my wife Jennifer in a way that, you know, maybe we never would've imagined, because it was so overwhelming," he said. "And such a good sign of America that you don't see a lot, but we felt it directly. 
