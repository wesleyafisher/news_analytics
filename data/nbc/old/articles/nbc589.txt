__HTTP_STATUS_CODE
200
__TITLE
Iraqis Fleeing ISIS Could Be Tortured By Their Own Armed Forces: Amnesty
__AUTHORS
Alexander Smith
__TIMESTAMP
Oct 18 2016, 11:04 am ET
__ARTICLE
 Innocent civilians trying to flee fighting to oust ISIS from the Iraqi city of Mosul risk being tortured, disappeared or executed by the country's own military, a leading human rights group warned Tuesday. 
 Amnesty International said Iraqi government forces and affiliated militias had committed "war crimes" and other "gross human rights violations" over the past two years, and this could well be repeated during the struggle to retake Mosul from the extremists. 
 NBC News was not able to independently verify the research, which Amnesty said was gathered over three weeks in July and August. 
 Past alleged violations were usually revenge attacks, according to the human rights group, and mostly perpetrated by the country's state-backed Shiite militias against Sunni Arab civilians they suspected of being members of ISIS. 
 Amnesty International said it had "serious fears" that Iraq's military and militias would commit similar crimes against people fleeing Mosul, the city currently being stormed by an Iraqi-led coalition involving U.S. forces. 
 Aided by U.S. airstrikes, Iraqi and Kurdish forces launched an offensive on Mosul early Monday, a three-sided assault involving some 25,000 troops that's expected to last weeks if not months. 
 On Tuesday, the coalition continued to recapture villages on its way into the city, including two communities less than two miles away from the Shura District south of Mosul, according to local police. 
 Amnesty said those fleeing the jihadis were now at risk from their own government. 
 "After escaping the horrors of war and tyranny of [ISIS], Sunni Arabs in Iraq are facing brutal revenge attacks at the hands of militias and government forces, and are being punished for crimes committed by the group, said Philip Luther, Amnesty's research and advocacy director for the Middle East and North Africa. 
 "As the battle to retake Mosul gets underway, it is crucial that the Iraqi authorities take steps to ensure these appalling abuses do not happen again," he said, adding that countries such as the U.S. that are supporting the Iraqis must do their bit to prevent war crimes. 
 "States supporting military efforts to combat IS in Iraq must demonstrate they will not continue to turn a blind eye to violations," he added. 
 Amnesty made the allegations in a 69-page report published Tuesday that it said was based on interviews with more than 470 former detainees, witnesses and relatives of those killed. It also said it spoke with officials, activists and humanitarian workers. 
 Amnesty said the Iraqi government has not responded to its allegations. Kurdish authorities  which Amnesty also said were also guilty of subjecting detainees to "torture and other ill-treatment"  denied the claims in the report. 
 "All detainees have gone through the established legal procedures," Kurdish officials told the human rights group. 
 Among the incidents uncovered by Amnesty were the 1,300 men and boys from the Mehemda tribe who were seized near Falluja as Iraqi forces were attempting to recapture the city. 
 Some were tortured and killed, according to the report, while around 600 were transferred to local officials three days later bearing the marks of torture of their bodies. 
 There was blood on the walls," one survivor told Amnesty on condition of anonymity. "They hit me and the others with anything they could lay their hands on, metal rods, shovels, pipes, cables." 
