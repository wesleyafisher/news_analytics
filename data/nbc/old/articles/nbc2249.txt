__HTTP_STATUS_CODE
200
__TITLE
Napalm Girl Photographer Nick Ut Retires After 51 Years in Photojournalism
__AUTHORS
Lakshmi Gandhi
__TIMESTAMP
Mar 28 2017, 5:49 pm ET
__ARTICLE
 Pulitzer Prize winning photojournalist Nick Ut, best known for his 1972 Vietnam War photograph known as "Napalm Girl," is expected to retire Wednesday after working for the Associated Press for 51 years. 
 Ut was just 15 years old when he jumped into the world of photojournalism at the height of the Vietnam War. He was following in the footsteps of his older brother, who was working on assignment when he was killed by the Viet Cong in 1965. His brothers memory would impact the rest of his career. 
 Im very careful when I get my assignment, Ut told NBC News earlier this year. I see I can die anytime, any day. We all die. That's why I don't worry about [it]. I keep going and do my assignment. 
 It was while Ut was on assignment on June 8, 1972, that he captured the now-famous image of 9-year-old Phan Th Kim Phc screaming and running down the road naked after what the Associated Press called a "misdirected napalm attack" by South Vietnamese military in then Trang Bang, South Vietnam. 
 I looked in my cameras viewfinder [and] I saw the girl running from black smoke, Ut recalled. I say Why isnt she wearing clothes? And I run and take a lot of pictures of her. 
 RELATED: Nick Ut, Photographer Behind 'Napalm Girl' Picture, Announces Retirement 
 After seeing the burns on Phcs back and arms, Ut says he poured water on her wounds and carried her to his car. It was only when he showed hospital officials his press pass that he was able to get Phc and the other children injured that day treated. 
 "I say Im media...the pictures will be everywhere, he said. They worry and bring her in and help her right away. Then after that, I go back to Saigon to AP to work on my film with the picture that day. 
 After the Vietnam War ended, Ut relocated to Los Angeles, where we continued to work for the Associated Press and covered events including the O.J. Simpson trial and Paris Hiltons 2007 arrest. 
 But it is the photo of a young Kim Phc, Ut is primarily remembered for. The photographer said he visited her family a week after the attack and that Phc, who now lives in Canada with her family, affectionately calls him Uncle Nick. 
 We become like family. Her mom and daddy always say Oh, thank you, he said. You helped my daughter. 
 Follow NBC Asian America on Facebook, Twitter, Instagram and Tumblr.  
