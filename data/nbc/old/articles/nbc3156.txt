__HTTP_STATUS_CODE
200
__TITLE
Pilot in Deadly Balloon Crash Was as Impaired as Drunk Driver, NTSB Says
__AUTHORS
Associated Press
__TIMESTAMP
Oct 17 2017, 4:38 pm ET
__ARTICLE
 AUSTIN, Texas  The pilot in the deadliest hot air balloon crash in U.S. history was probably impaired by opioids and sedatives when he ignored weather warnings and flew the ride into a power line, investigators said Tuesday. 
 Besides Valium and oxycodone, there was a high enough dosage of the over-the-counter antihistamine Benadryl in Alfred "Skip" Nichols' system to mimic "the impairing effect of a blood-alcohol level" of a drunken driver, said Dr. Nicholas Webster, a National Transportation Safety Board medical officer. 
 During a meeting in Washington, NTSB revealed its findings about the July 2016 crash near Austin that killed all 16 people aboard. Investigators scolded the Federal Aviation Administration for lax enforcement of the ballooning industry and recommended that balloon pilots submit to the same medical checks as airplane pilots. 
 Related: Friends, Families Remember Victims of Texas Balloon Crash 
 Nichols, 49, had at least four prior convictions for drunken driving, though no alcohol was found in his system after the crash. Investigators said Nichols was told during a weather briefing before the flight that clouds may be a problem. He brushed off the warning. 
 "We just fly in between them," Nichols allegedly answered back, according to NTSB investigators. "We find a hole and we go." 
 Visibility was 10 miles about two hours before the balloon took off from a Walmart parking lot near the rural town of Lockhart but had diminished to just 2 miles before the ride began. 
 Investigators said Nichols told his psychiatrist three months before the crash that he was not using his antidepressant medication and that his psychiatrist documented his mood as "not good." Nichols was prescribed 13 medications and was also being treated for attention deficit hyperactivity disorder, known as ADHD, which investigators say also was a contributing factor. 
 The final public hearing by the NTSB into the crash wasn't the first time the federal government's crash-site investigators have urged the FAA to more closely regulate the balloon industry. NTSB Chairman Robert Sumwalt ripped the FAA and questioned why the agency was endorsing voluntary pilot requirements written by the Balloon Federation of America instead of tightening regulations. 
 "Why is the FAA promoting it? It is not an FAA program," Sumwalt said. "The FAA is treating this as the be-all, end-all. They are abdicating their responsibility to provide oversight. They are saying, 'The BFA will take care of this so we do not have to do anything.' That is what is sad." 
 The FAA said in a statement that it will carefully consider the NTSB recommendations but did not address Sumwalt's criticism. 
 Related: FAA and NTSB Clash Over Balloon Pilots Disclosure Rules 
 Before the Texas crash, Nichols' balloon-ride companies in Missouri and Illinois were the targets of various customer complaints dating back to 1997. Customers reported to the Better Business Bureau that their rides would get canceled at the last minute and their fees never refunded. 
 Aviation experts say the FAA might allow a recovering alcoholic to fly commercial jets if the pilot could show that he or she was being successfully treated but that the agency is unlikely to accept pilots with drunken driving convictions. 
 Scott Appelman, owner of the New Mexico-based balloon operator Rainbow Ryders Inc., said his pilots already meet the higher standard and that federal requirements won't have an impact. He called Nichols a renegade who operated outside the rules anyway. He said the Texas crash has taken a toll on customers. 
 "It was a significant effect on the balloon industry. The industry has not recovered," Appelman said. 
