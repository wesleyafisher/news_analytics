__HTTP_STATUS_CODE
200
__TITLE
Zika Damages Male Fertility, in Mice Anyway
__AUTHORS
Maggie Fox
__TIMESTAMP
Nov 1 2016, 12:01 pm ET
__ARTICLE
 The Zika virus can get into the testes of mice, shrinking them and damaging them so badly that sperm production drops, researchers report this week. 
 Theres no evidence it can do the same thing in human men, but the virus has thrown many surprises at scientists over the past year. As researchers so often say, more study is needed. 
 "This is the only virus I know of that causes such severe symptoms of infertility, said Dr. Kelle Moley, a fertility specialist at Washington University in St. Louis who helped lead the study. 
 There are very few microbes that can cross the barrier that separates the testes from the bloodstream to infect the testes directly. 
 Mumps is one of the most notorious causes of male infertility. The virus can cause pain and swelling in the testicles and can damage fertility in a small percentage of men. Ebola virus can also get into semen and stay in the testes for months. 
 Related: Can I Ever Get Pregnant? And Other Zika Questions 
 Zika has already been shown to cross the placenta and infect developing human fetuses. It causes severe brain damage and other birth defects when it does. 
 The virus also can, rarely, damage the central nervous system of human adults  causing brain inflammation and an unusual paralyzing side effect of many infections called Guillain Barre syndrome. 
 It can also get into semen  it is transmitted sexually. And its been shown to stay in men's semen for months. 
 So the team at Washington University decided to see if it was damaging the testes as it hung out there for months. 
 It does, at least in the mice they tested. 
 Related: Sex Spreads Zika - Any Kind of Sex 
 What we found was that by day seven you could already detect the virus there and it was cleared by day 21, Moley said. 
 But you could see a progressive destruction of the cells in the testes. So by day 21 the testes was about a tenth of the size of what it originally was. As a result there was no sperm. 
 And when they mated the mice, those with Zika-damaged testes had fewer offspring, the report in the journal Nature. 
 The mice are altered in the lab so their immune systems are suppressed, so the experiment doesnt precisely reflect what happens in natural circumstances. 
 Now its time to check out some Zika-infected men, says Michael Diamond, who also worked on the study. 
 What we dont know is does it also cause the same level of injury? he asked. And if a man doesnt have symptoms, can his testicles be damaged? Most people infected with Zika virus have very mild symptoms or none at all. Does the degree of severity of infection correlate with injury? Diamond asked. 
 Zika has swept across swatches of South America, Central America and the Caribbean. Its causing small outbreaks in Miami. Florida has more than 200 home-grown cases of Zika and around 800 cases carried into the state by travelers. 
 The Centers for Disease Control and Prevention reports 4,000 Zika cases in the 50 U.S. states and more than 28,000 in territories  mostly Puerto Rico. More than 950 pregnant women have been infected in the states and 2,000 in territories. 
