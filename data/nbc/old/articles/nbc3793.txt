__HTTP_STATUS_CODE
200
__TITLE
Strangers Form 70-Person Human Chain to Save Family From Rip Current
__AUTHORS
Amanda Proena Santos 
__TIMESTAMP
Jul 11 2017, 7:14 pm ET
__ARTICLE
 A potential tragedy in a north Florida beach got a happy ending when strangers formed a human chain in the water to rescue a family from a rip current. 
 Roberta Ursery and her family were spending Saturday at Panama City Beach when she realized she'd lost sight of her 8- and 11-year-old sons, NBC affiliate WJHG reported. She spotted them far out in the water, where they were caught in a rip current and unable to swim back to shore. 
 "They were screaming and crying that they were stuck and they couldn't go nowhere," Ursrey said. 
 The sight of the children in distress prompted most of the Ursery family to swim out to save them, but they were all caught in the current themselves when it proved to be too strong. 
 That's when fellow beachgoers came to the rescue, heading into the water and linking arms to form a human chain that could withstand the current. 
 "It went from, like, five people to about 70 people at the end. It lasted well over an hour," Jessica Simmons, one of the people who helped save the family, told WJHG. 
 The rescue mission was successful, and the whole family made it safely back to shore to the sounds of cheering and clapping, Simmons told the station. 
 "As a mama, I'm supposed to be able to protect [my children] and do everything, and I couldn't do it that day," Ursery told WJHG. "I had to have help, which I was eternally grateful for." 
 And her son Stephen, 8, said that all he hopes for is to be able one day to rescue those who rescued him on Saturday. 
