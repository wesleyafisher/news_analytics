__HTTP_STATUS_CODE
200
__TITLE
Most Millennials Are Finding It Hard to Transition Into Adulthood: Report
__AUTHORS
Safia Samee Ali
__TIMESTAMP
Apr 20 2017, 5:14 am ET
__ARTICLE
 By his twenties, Kyle Kaylor imagined he would be living on his own, nearing a college degree, and on his way to a job that fulfilled him. 
 Instead, at 21, he found himself out of school, living with his parents, and "stuck" working as a manager at a fast food restaurant scraping to make hand-to-mouth. 
 Launching into adulthood has been tricky, he said. 
 "It became too difficult financially to be in school and not working," says Kaylor, who dropped out of Lincoln Christian University, in Illinois, after one semester because of a money crunch. "And without schooling, you cant get a job that you can survive on, so I had to move back home," he said. 
 It's a scenario that has become far too common, according to a new census report out Wednesday that reveals staggering statistics on millennials and their journey to independence. 
 For one, the report shows young men like Kaylor, who makes less than $22,000, have fallen by the wayside when it comes to income. 
 "In 1975, only 25 percent of men aged 25 to 34 had incomes of less than $30,000 per year. By 2016, that share rose to 41 percent of young men," according to the report.  
 "That is a product of a shrinking blue-collar economy," said Anthony Carnevale, director of the Center on Education and the Workforce, a non-profit institute at Georgetown University. 
 Traditionally, men occupied most positions in industries such as manual labor and construction work. With those mostly gone, male wages have been hit harder than "women who started off behind" but excelled in school and college, Carnevale said. 
 Related: Tight job market pits old against young 
 Men are "more easily drawn away from schooling by blue-collar jobs because they pay $20,000 to $25,000 out of high school," he said. Even though that job may not be there in ten years and will never pay more than that, he said. 
 But the issue goes beyond men when it comes to what many may perceive as a failure to launch. 
 In 2015, one-third or about 24 million young adults, ranging from 18 to 34, lived with their parents, according to the report. 
 "Living in an independent household is expensive and the ability to do so hinges, in part, on young adults' economic resources as well as the costs of rent and home-ownership," the report stated. 
 While 81 percent of those who live at home are either working or going to school, one in four between 25 to 34 are "idle, meaning they are not in school and do not work" the report stated. 
 Related: Jobs Growth Slowed in March, 98,000 vs 180,000 Expected 
 "These individuals may be temporarily not working or not in school, but that doesn't mean they are permanently out of the workforce," said Jessica McManus Warnell, a professor at the University of Notre Dame Mendoza College of Business. 
 After getting a bachelor's and masters degree, 27-year-old Yvonne Juris also didnt expect to be unemployed and living with her parents. 
 Two years out of school, she hasnt been able to find a job that would cover her basic bills, which includes a hefty student loan payment, she said. 
 "Recently I was offered $500 a month, but I negotiated to $700 a month," she said, adding that she thinks employers may be unwilling to offer better wages because they feel they can "exploit desperation." 
 "I thought my life would be very different," she said. "Now it just feels like it is out of my control." 
 The perception that millennials are not doing enough to help themselves or are lazy is inaccurate, Warnell said. 
 They do want to work but are trying to figure out how to go about it in a job market with much different demands and expectations, she said. 
 "The onramp to middle class takes a lot longer than it did before," Carnevale said. 
 In the past, you could get by with a high school degree and survive on your own, but thats just not the case anymore, he said. The price of adulthood involve high education costs and housing costs that were previously not a barrier. 
 "These individuals are the first to go through new demands in a drastically different job force than from one generation prior," he said. So it's no surprise the transition has been bumpy for many. 
 But one thing that can be done, according to Warnell, is to change the education structure. 
 "Schools should do more, by not just teaching principles and concepts but the application of these principles and concepts," she said. 
 Related: Trump Promised Job Creation  but His Budget Axes Many Job Creation Programs 
 This concept should be applied not just to advanced academic institutions but also high schools, she said. 
 Young people today have not only witnessed a global economic crash overseen by previous generations, but they grew up observing a world of work that is no longer applicable, according to Carnevale. 
 For 21-year-old Kaylor, the path is still unclear as he figures out how to propel himself into independence  something he's still figuring out. 
 "I dont know what the solution is, Im no expert," he said. "But Im trying to do whatever I can to get myself in a better situation." 
