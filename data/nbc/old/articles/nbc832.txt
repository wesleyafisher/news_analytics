__HTTP_STATUS_CODE
200
__TITLE
Trumps Expected Iran Move Would Defy Allies Abroad  and at Home
__AUTHORS
Chuck Todd
Mark Murray 
Carrie Dann
__TIMESTAMP
Oct 11 2017, 8:49 am ET
__ARTICLE
 First Read is your briefing from Meet the Press and the NBC Political Unit on the day's most important political stories and why they matter. 
 Later this week, President Trump will announce his administrations position on the Iran nuclear deal, and the expectation is that hell decertify the deal, saying that its not in the countrys national interest. But decertification would put Trump at odds with much of the rest of the world  as well with members of his own national security team  at a time when the United States is trying to curb North Koreas nuclear ambitions. 
 Defense Secretary James Mattis 
ANGUS KING: Secretary Mattis, very quick, short-answer question  do you believe it's in our national security interest at the present time to remain in the JCPOA [Iran nuclear deal]? That's a yes or no question.
MATTIS: Yes, senator, I do.
(October 3, 2017 Senate Armed Services Committee hearing)
 Joint Chiefs Chairman Joseph Dunford 
Iran is not in material breach of the agreement, and I do believe the agreement to date has delayed the development of a nuclear capability by Iran.
(October 3, 2017 Senate Armed Services Committee hearing)
 British Prime Minister Theresa May 
The [prime minister] reaffirmed the UK's strong commitment to the deal alongside our European partners, saying it was vitally important for regional security.
(A May spokespersons readout of her call yesterday with Trump)
 German Foreign Minister Sigmar Gabriel 
[W]e do not want to see this agreement damaged We urge the White House not to call into question such an important achievement that has improved our security."
(Reuters)
 In addition to those comments, Trumps administration has already certified the agreement twice (on April 18 and July 17), and the International Atomic Energy Agency says Iran is abiding by the agreement. 
 It all raises the question: What are the consequences of the United States decertifying something that the rest of the world says is working when it comes to Irans nuclear program? How do Americas European allies react? What does Iran do? And what does it mean for the United States push to restrain North Koreas nuke program? 
 Decertification wouldnt mean that the United States is formally ending the Iran nuclear deal. But withholding certification, as NBC News reported last week, would trigger a sanctions review process in Congress. And so it would be up to Congress to decide whether to slap sanctions on Iran  which would effectively tear up the deal  or not to. 
 Still, decertification is a precarious step. How do Congress, Europe and Iran all react to it? 
 He told his advisers back in July he wanted a nearly tenfold increase in Americas nuclear arsenal: What was the context behind Secretary of State Rex Tillerson calling President Trump a moron? It appears to be this meeting and discussion, per NBCs Courtney Kube, Kristen Welker, Carol Lee, and Savannah Guthrie. 
 President Donald Trump said he wanted what amounted to a nearly tenfold increase in the U.S. nuclear arsenal during a gathering this past summer of the nations highest ranking national security leaders, according to three officials who were in the room. 
 Trumps comments, the officials said, came in response to a briefing slide he was shown that charted the steady reduction of U.S. nuclear weapons since the late 1960s. Trump indicated he wanted a bigger stockpile, not the bottom position on that downward-sloping curve. 
 According to the officials present, Trumps advisers, among them the Joint Chiefs of Staff and Secretary of State Rex Tillerson, were surprised. Officials briefly explained the legal and practical impediments to a nuclear buildup and how the current military posture is stronger than it was at the height of the build-up. In interviews, they told NBC News that no such expansion is planned. 
 The July 20 meeting was described as a lengthy and sometimes tense review of worldwide U.S. forces and operations. It was soon after the meeting broke up that officials who remained behind heard Tillerson say that Trump is a moron. 
 Trump and congressional Republicans havent passed a health care bill. And tax reform still seems to be a gigantic lift. But Trump and his supporters can crow about this  the president has won back-to-back battles in the Culture War. 
 Yesterday, NFL Commissioner Roger Goodell wrote a letter to NFL owners and executives, saying: [W]e believe that everyone should stand for the National Anthem, and said that the NFL has developed a plan that it will address at next weeks meeting. 
 Trump tweeted his reaction this morning. It is about time that Roger Goodell of the NFL is finally demanding that all players STAND for our great National Anthem-RESPECT OUR COUNTRY. 
 Also, ESPN suspended host Jemele Hill for violating the companys social media policy after she tweeted about boycotts against Dallas Cowboys owner Jerry Jones. And here was Trump yesterday: With Jemele Hill at the mike, it is no wonder ESPN ratings have tanked, in fact, tanked so badly it is the talk of the industry! 
 But the Culture Wars are still ongoing, and heres rapper Eminem firing back at Trump. 
 In reaction to Hillary Clinton denouncing Hollywood mogul Harvey Weinstein after the sexual-assault allegations against him, Trump White House counselor Kellyanne Conway tweeted, It took Hillary abt 5 minutes to blame NRA for madman's rampage, but 5 days to sorta-kinda blame Harvey Weinstein 4 his sexually assaults. 
 But this will only draw attention to the allegations of sexual assault and harassment against President Trump, Trumps remarks in that infamous Access Hollywood video, and Trump calling Roger Ailes a very, very good person. 
 At 5:45 pm ET from Harrisburg, Pa., President Trump will deliver remarks selling the GOP tax-reform proposal. 
 The AP previews Trumps event. Trump will be speaking in front of an audience of roughly 1,000 people, including lots of truckers, against a backdrop of big rigs at a local air plane hangar, according to the White House. Trump is pitching a plan that would dramatically cut corporate tax rates from 35 percent to 20 percent, reduce the number of personal income tax brackets and boost the standard deduction. 
 Finally, in Virginias gubernatorial race, former Vice President Joe Biden will campaign for Democrat Ralph Northam on Saturday. Biden will participate with Northam in a roundtable discussion on economic development in Reston, Va. 
