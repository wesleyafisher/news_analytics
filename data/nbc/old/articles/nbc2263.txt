__HTTP_STATUS_CODE
200
__TITLE
Eating Pure Oats May be Okay for Celiac Sufferers: Study
__AUTHORS
Reuters
__TIMESTAMP
May 4 2017, 6:49 pm ET
__ARTICLE
 People with celiac disease have to avoid most grains, but oats may be an exception thats safe, according to a recent research review, so long as the oats are uncontaminated by traces of other grains. 
 More studies are needed to see whether so-called pure oats available in the real world dont provoke celiac symptoms. If proven safe, oats could provide celiac sufferers some of the benefits of eating grains that they miss out on following a gluten-free diet, researchers say. 
 Oats, compared to other cereals, are a source of good quality proteins, vitamins and minerals and they improve palatability and the texture of gluten-free food, said study coauthor Dr. Elena Verdu. 
 For a person diagnosed with celiac disease, adding oats to a gluten-free diet could not only increase food options but also help them follow a better gluten-free diet and have a higher quality of life, said Verdu, a gastroenterology researcher at the Farncombe Family Digestive Health Research Institute at McMaster University in Hamilton, Canada. 
 Celiac disease is an autoimmune disorder that affects roughly one of every 100 people in the U.S. For sufferers, consuming even trace amounts of the gluten protein in wheat, barley and rye can trigger an immune response that damages the intestines. Over time, this immune attack can lead to malnutrition, osteoporosis, chronic inflammation and a variety of other problems. 
 People with celiac disease are also at heightened risk of heart disease and some recent research suggests that might be in part because avoiding gluten causes them to miss out on the heart-protective benefits of eating whole grains. 
 Related: More People Go Gluten-Free Than Need To, Study Finds 
 Oats dont contain the same celiac-provoking protein found in other grains, the study team writes in the journal Gastroenterology. However, Verdu told Reuters Health, issues have been raised regarding potential adverse reactions to oats by celiac patients, and this has reduced the enthusiasm of adding oats to the gluten-free diet in many cases. 
 The first study suggesting that oats may be harmful for patients with celiac disease was published more than 50 years ago. Since then, the addition of oats to a gluten-free diet has remained clouded in controversy, she said in an email. 
 For this reason, the review team decided to evaluate the existing evidence. They re-analyzed data from 28 previous studies that included oats in gluten-free diets for people with celiac disease. Eight of the studies were controlled clinical trials; the rest were observational. 
 Related: Is a gluten-free diet for you? The hidden downsides of the food craze 
 The researchers looked at any negative effects on symptoms or blood tests for up to one year of oat consumption. 
 In our study, we found no evidence that addition of oats to a gluten-free diet affects symptoms or activates celiac disease. However, it is very important to stress that there were few studies in some of the analyses, the quality of the studies was low and most of them were conducted outside of North America, Verdu said. 
 Although the consensus is that pure oats are safe for most patients with celiac disease, contamination with other cereal sources that may contain gluten needs to be avoided, Verdu added. 
 The purity of oats will depend on the country of origin and local regulations, and this is why we were surprised to see that most recommendations in North America are still based on studies performed in Europe, she said. 
 Related: FDA Approves 23andMes At-Home DNA Tests for 10 Diseases 
 Patients who follow a gluten-free diet are sometimes able to consume small quantities of gluten-free oats without adverse reaction, said Hannah Swartz, a clinical dietitian at Montefiore Medical Center in New York who wasnt involved in the study. 
 Patients who have the most success with including oats in their diet ensure the oats are certified gluten free, and wait one or more years after following a gluten free diet to ensure that gut inflammation has subsided, she said. 
 Patients with celiac disease must first ensure that the oats they are adding are certified gluten free oats. Regular oats used in products that are labeled gluten free such as some mainstream cereals are not recommended for patients with celiac disease as there remains the possibility of cross contamination with gluten containing grains during the processing of the oats, she said. 
