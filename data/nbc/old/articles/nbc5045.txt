__HTTP_STATUS_CODE
200
__TITLE
ISIS, U.S.-Backed Forces Battle Over Syrias Tabqa Dam
__AUTHORS
__TIMESTAMP
Mar 27 2017, 7:27 am ET
__ARTICLE
 U.S.-led coalition forces in Syria dismissed ISIS claims that the country's largest dam was at imminent risk of collapse. 
 The dam is located on the Euphrates about 25 miles upstream from ISIS' stronghold of Raqqa. 
 The Syrian Democratic Forces, an alliance of Kurdish and Arab militias supported by a U.S.-led international coalition, has been battling to capture the facility since Friday. 
 ISIS said in messages carried on its social media channels that the dam's operations had been put out of service and that all flood gates were closed. It said the dam was at risk of collapse as a result of airstrikes and increased water levels. 
 The activist group Raqqa is Being Slaughtered Silently also reported that ISIS had ordered Raqqa residents to evacuate. NBC News could not independently confirm that report. 
 But U.S.-led coalition forces said the 2.5-mile long dam was structurally sound and denied that any of its strikes had hit it. 
 U.S.-backed Syrian Kurdish forces were in control of a spillway north of the dam "which can be used to alleviate pressure on the dam if need be," the coalition said in a letter to The Associated Press. 
 The push toward the dam comes three days after U.S. aircraft ferried Syrian Kurdish fighters and allies behind ISIS lines to spearhead a major ground assault on Tabqa. 
 Backed by U.S. special forces in a campaign that has driven ISIS from large swathes of northern Syria, the SDF fights separately from other rebel groups that seek to topple President Bashar al-Assad's rule. 
 The U.S. has deployed more than 700 advisers, marines and Rangers to Syria to support fighters battling ISIS militants. 
 Meanwhile, the SDF said in statement Sunday that it had seized a military airport from ISIS. 
 Tabqa air base was captured by IS militants from the Syrian government in August 2014. Shortly afterward, the group announced it had killed about 200 government soldiers at the base, in a mass killing recorded and distributed on video over social media. 
