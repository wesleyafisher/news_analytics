__HTTP_STATUS_CODE
200
__TITLE
Automakers Search for Easier, Friendlier Ways to Sell Cars
__AUTHORS
Paul A. Eisenstein
__TIMESTAMP
Oct 15 2017, 10:09 pm ET
__ARTICLE
 The internet has transformed how Americans shop, and now automakers are looking to update the car selling process, from cutting out the haggling to allowing motorists to shop from home to even letting customers return cars that don't satisfy. 
 Hyundai just launched its Shopper Assurance program, which, among other things, offers potential buyers a three-day money-back guarantee. 
 Maybe your taste and budget run more toward Porsche, but you can't decide among the classic 911 sports car, the Panamera sedan or the big Cayenne SUV. No problem. Porsche just introduced the Porsche Passport, which lets customers swap vehicles as often as they like. 
 For $2,000 a month, choose among the classic 911, the 718 Boxster, the Cayman S or the Cayenne. For $3,000 a month, customers have access to 22 Porsche models, including the 911 Carrera S, the Panamera 4S sports sedan, the Macan GTS, and the Cayenne S E-Hybrid. There's an additional $500 activation fee, but registration, insurance and maintenance costs are included. 
 Swapping vehicles requires a subscriber simply to use a smartphone app developed by Clutch Technologies, a software development company focused on automotive subscription services. 
 For now, the Porsche Passport pilot program will be offered to only 50 customers through the two dealers in Atlanta, where Porsche is based. But the program could be expanded if it proves popular  and manageable. 
 Audi is offering a concierge rental service, Audi on Demand, in San Francisco. General Motors, meanwhile, launched its own pick-and-choose pilot in New York early this year, with Book by Cadillac letting buyers switch vehicles as often as 18 times a year for a flat $1,500 monthly fee. 
 Volvo just launched what it also calls a subscription service, Care by Volvo, that is, for now, limited to the new XC40 sport-utility vehicle. Customers pay a flat monthly fee, with no down payment, and will be able to upgrade to a new Volvo in as little as 12 months. 
 Also included is free pickup and delivery for service, with buyers getting loaners while their vehicles are in the shop. It's a feature a number of automakers have begun to offer, including Lincoln and Hyundai's recent luxury spinoff, Genesis. Makers hope that by enhancing the ownership experience they can entice new buyers to give them a try. 
 "We have to make sure we're retailing on the customer's terms," said Adam Chamberlain, vice president of sales for Mercedes-Benz USA, which is experimenting with online used car sales in Portland, Oregon. "We have to make sure we give them the experience they want." 
 The push for new retail models can be challenging, as automakers have to work around state franchise laws. Chamberlain said the company's dealers would be central to any new approach it tried. 
 Tesla has been steadfast in opening only factory-owned stores, a position that has forced it to battle in court with dealer groups in a number of states. In several others, including New Jersey, it has struck deals with lawmakers and regulators. 
 The push to find new ways of retailing is especially fierce among brands in the competitive luxury market. When customers are spending $60,000, $100,000, even $200,000 or more, they expect to have things done more on their terms, experts say. 
 But even mainstream brands are exploring new retail strategies that could radically change how customers buy, lease  or subscribe  in the future. 
 Hyundai's new Shopper Assurance program targets a number of issues consumers typically complain about. In addition to the three-day money-back guarantee, it will offer "transparent" pricing, flexible test drives and a simpler purchase process. 
 "We expect this to be a differentiator," said Dean Evans, chief marketing officer of Hyundai Motor America, adding: "Our research showed that 84 percent of people would visit a dealership that offered all four features over one that did not. It is the future of car buying." 
 Related: This Hyundai Gets Better Ratings Than BMW and Porsche 
 Even for manufacturers still wedded to conventional retailing, things have changed dramatically, said Fran O'Hagan, head of the automotive retail consultancy Pied Piper. 
 "The combination of the internet and wide use of smartphones has had a huge impact on the retail auto industry," said O'Hagan, who is based in California. 
 "The role of the dealership and salespeople is still critical," but where they once were the gatekeepers, he said, buyers usually know what they want before they walk into the showroom, and "the role of the salesperson is to now to help a customer fine-tune their choice of vehicle." 
 Yet some speculate that millions of motorists may simply stop buying cars and switch to ride-sharing companies like Uber and Lyft as they make the transition to driverless vehicles and sharply lower the cost of their services. 
 That, said O'Hagan, will force manufacturers and their dealers to get even more creative to keep customers in the retail market. 
