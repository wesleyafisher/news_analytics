__HTTP_STATUS_CODE
200
__TITLE
North Koreas Nuclear Test Site Could Be Unstable, Experts Say
__AUTHORS
Reuters
__TIMESTAMP
Oct 13 2017, 5:24 am ET
__ARTICLE
 SEOUL, South Korea  Tremors and landslides near North Korea's nuclear test base likely mean the country's sixth and largest blast has destabilized the area, meaning the site may not be used for much longer to test nuclear weapons, experts say. 
 A small quake was detected early on Friday near the Punggye-ri nuclear site, South Korea's weather agency said. However, unlike quakes associated with nuclear tests, it did not appear to be man-made. The tremor was the latest in a string of at least three shocks to be observed since Pyongyang's Sept. 3 nuclear test, which caused a 6.3-magnitude earthquake. 
 Friday's quake was a magnitude 2.7 with a depth of 1.9 miles in North Hamgyong Province in North Korea, the Korea Meteorological Administration said. The United States Geological Survey (USGS) measured the quake at 2.9 magnitude at a depth of 3.1 miles. 
 The series of quakes has prompted experts and observers to suspect the last test, which the North claimed to be of a hydrogen bomb, may have damaged the mountainous location in the northwest tip of the country, where all of North Korea's six nuclear tests were conducted. 
 "The explosion from the Sept. 3 test had such power that the existing tunnels within the underground testing site might have caved in," said Kim So-gu, head researcher at the Korea Seismological Institute. "I think the Punggye-ri region is now pretty saturated. If it goes ahead with another test in this area, it could risk radioactive pollution." 
 According to 38 North, a Washington-based project which monitors North Korea, numerous landslides throughout the nuclear test site have been detected via satellite images after the sixth test. These disturbances are more numerous and widespread than seen after any of the North's previous tests, 38 North said. 
 The explosion from the sixth test was large enough for residents of the Chinese border city of Yanji, 125 miles north of North Korea's nuclear test site, to feel the ground shake beneath their feet. 
 "The reason why Punggye-ri has become North Korea's nuclear testing field is because this area was considered stable and rarely saw tremors in the past," said Hong Tae-kyung, a professor of earth system science at Yonsei University in Seoul. "The recent small quakes suggest that the test might have triggered crust deformation." 
 Another issue that could keep North Korea from using Punggye-ri for nuclear tests is the nearby active volcano of Mt. Paektu, according to Hong. 
 The 9,000-ft mountain, straddling the northwestern border between China and North Korea, last erupted in 1903. Since North Korea began testing its nuclear capabilities, experts have debated whether explosions at Punggye-ri could trigger another volcanic eruption. 
