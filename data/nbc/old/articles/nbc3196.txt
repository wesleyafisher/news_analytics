__HTTP_STATUS_CODE
200
__TITLE
Fall of ISIS Stronghold Raqqa Will Change War on Extremists, Not End It
__AUTHORS
Saphora Smith
__TIMESTAMP
Oct 11 2017, 12:40 pm ET
__ARTICLE
 While U.S.-backed forces are inching toward the final showdown in Raqqa, Syria, the Islamic State's de facto capital, the war against the terror group is set to grind on even after the extremists are driven from their stronghold. 
 "Its too early to plan ISISs obituary," said Fawaz Gerges, professor of Middle Eastern politics at the London School of Economics. "You still have a year of serious battles in Iraq and Syria to pick apart and dismantle the territorial caliphate, and even if you do this it will mutate into ... a terrorist organization." 
 At the height of its power ISIS ruled over millions of people and attracted thousands of fighters from overseas. Now it clings to a fraction of its former territory. 
 Experts said the group would revert to operating as an insurgency after the eventual fall of Raqqa  from which ISIS oversaw the management of much of eastern, central and northern Syria during the civil war and planned attacks overseas. 
 The real challenge will be [ISIS] fighters fleeing to other areas, said Karin Von Hippel, director general of RUSI, a defense think tank in London. 
 After Raqqa's fall, the group will most likely revert to operating as an insurgency and focus on carrying out attacks abroad instead of capturing and holding territory, experts said. 
 Von Hippel called this new phase ISIS as it moves into the cloud," and warned it would be especially challenging because it would involve fighting the group's ideology  something coalition partners aren't very good at. 
 Regardless of what comes next, the current battle to retake Raqqa has been painstaking. 
 Related: In Battle Against ISIS in Syria and Iraq Civilians Suffer Most 
 In the past week the Syrian Democratic Forces (SDF)  an alliance of Kurdish and Arab militias backed by U.S. air raids  have captured 40 blocks in Raqqa that were once held by ISIS, according to the U.S. military. 
 "Progress comes block by block and, quite frankly, building by building," said Col. Ryan Dillon, spokesman for the U.S.-led coalition against ISIS, because of what he described as a "difficult and dense, complex urban environment." 
 The SDF estimate that some 85 percent of Raqqa has been cleared since the drive to take the city started in June. There are 300 to 400 ISIS fighters still embedded in a 2.5-mile area around what was once the National Hospital and the sports stadium, down from some 3,500 at the beginning of the campaign, according to the U.S. military. 
 While Dillon said he wouldn't speculate how long it would take to drive the remaining fighters from the city, Shiraz Maher, deputy director of the International Centre for the Study of Radicalisation and Political Violence at Kings College London, was more definitive. 
 The fall is imminent, he said, adding the militants are hanging on by the skin of their teeth. 
 The militants' behavior indicated its leadership know they are on their back foot, Maher added. 
 Theyve realized they can no longer fight off these campaigns the way they used to, he said, pointing to ISISs recent recruitment drive for female fighters as indicative of their weakened state. 
 Theyve never done that before, Maher added. For them it indicates that theyre quite desperate. 
 But four months into the grueling battle for Raqqa the human toll of the fighting is clear. 
 Gerges called the toll on the city's residents "disproportionate," and added that coalition air airstrikes had been punishing to civilians because ISIS combatants have embedded themselves among the city's residents. 
We are working at this hour with the #Raqqa Civilian Council to protect & evacuate civilians being used as human shields by #ISIS terrorists pic.twitter.com/DSBhaM16Wg
 The precise toll the intensifying fighting is having on civilians is hard to pin down. According to an Amnesty International report issued in August, hundreds of civilians had already been killed and injured since the offensive began. 
 Humanitarian organizations on the ground have estimated that some 8,000 civilians are still trapped in Raqqa, said Sara Kayyali, who researches Syria for Human Rights Watch. Many if not most of those in the city were believed to have been held as human shields, rights groups say. 
 Maher said Raqqa had essentially become a waste town, comparing the offensive to the Word War II blitz bombing campaign. 
 Not only has the city been left without infrastructure, basic sanitation or commodities but its destruction also fulfills ISISs preferred narrative  that coalition forces have obliterated innocent peoples lives, he added. Thats a message that resonates well for ISIS, he said. 
 Gerges agreed: It plays into ISISs campaign for hearts and minds. 
