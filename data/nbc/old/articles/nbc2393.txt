__HTTP_STATUS_CODE
200
__TITLE
U.S. Soldier Accused of Pledging Support to ISIS Believed Conspiracy Theories
__AUTHORS
Associated Press
__TIMESTAMP
Jul 14 2017, 11:57 am ET
__ARTICLE
 A U.S. soldier accused of wanting to commit a mass shooting after pledging loyalty to the Islamic State group believed the moon landing was faked, questioned the assassination of President John F. Kennedy and thought the Sept. 11 terrorist attacks were an inside job coordinated by the U.S. government, according to a former Army bunkmate. 
 Army Sgt. 1st Class Ikaika Kang was ordered held without bail in federal court in Honolulu Thursday after a brief detention hearing. Kang's court-appointed attorney Birney Bervar did not contest his client's detention but said after the hearing that he will ask for a mental health evaluation. 
 Related: U.S. Soldier Arrested in Hawaii, Accused of Trying to Support ISIS 
 A "turning point" for Kang's mental state seems to be a 2011 deployment, Bervar said: "He's a decorated American soldier for 10 years, goes to Afghanistan and comes back and things start going off the rails." 
 Bervar said his client may suffer from service-related mental health issues of which the government was aware but neglected to treat. 
 "It looks like rather than helping him, the government exploited and took advantage of him," Bervar said. 
 "Kang's military training, weapons abilities and personal combat skills, coupled with his strong stated desire to kill people in the name of Islamic State, makes him one of the more dangerous criminal defendants to have been charged in this judicial district," prosecutors wrote in a motion asking that be held without bail. 
 According to court documents, Kang met with undercover agents he thought were from the Islamic State group at a home in Honolulu, where he pledged allegiance to the group and kissed an Islamic State flag. 
 Kang was arrested immediately "to remove the possibility that he would act on his impulse to kill people in the name of Islamic State," prosecutors wrote. 
 Bervar noted that since Kang never had any contact with real members of the group, the government was "pouring gasoline on the fire of his mental illness." 
 Kang and Dustin Lyles, a medically retired soldier, bunked together for a month in 2013 during military training. The two were friends for several years before Lyle left the Army and the two lost touch. 
 Lyles told The Associated Press that Kang's arrest came as a shock and that he never heard Kang express support for the enemy. They shared sleeping quarters, ate together and practiced mixed martial arts. 
 "If I had known that then ... I wouldn't even have talked to him after that," Lyles said. 
 Lyles said he and Kang debated about conspiracies, including that 9/11 was staged by the U.S. to spark wars in the Middle East. 
 Kang aspired to become a professional MMA fighter, Lyles said. Kang completed a course to become a tactical combat instructor to soldiers, according to an FBI complaint filed in court. 
 With help from a Veterans Administration loan, Kang purchased a condo in May 2016 in a tidy suburban complex about a 20-minute drive to Schofield Barracks, according to property records. 
 Dee Asuncion, the real estate agent who helped him with his condo purchase, told The Associated Press that there was one conversation that seemed strange to her. He talked about having respect for the ideology of Islamic terrorist groups. 
 She also said the two were driving around together looking at properties and began talking about road rage. "(Kang) goes, 'One time I followed this guy from the east side down to the west side,'" she recalled. "I told him, 'Don't do stuff like that. Just let it go.'" 
 Kang's father, Clifford Kang, told the Honolulu Star-Advertiser his son's promotion to sergeant first class came in the last six months, and told the newspaper he was proud of his son for serving and his position as an air traffic controller. 
 But his father was worried about the stress of the job. 
 "I kept on telling him, 'Being an air traffic controller (is) too stressful. You can always change ... and they will understand.' And he said, 'I can handle.'" 
