__HTTP_STATUS_CODE
200
__TITLE
The Week in Pictures: Oct. 6 - 13
__AUTHORS
NBC News
__TIMESTAMP
Oct 13 2017, 6:59 pm ET
__ARTICLE
Hundreds of sheep fill a central square in Lyon, France on Oct. 9, 2017, in protest against the government's protection of wolves, which they blame for livestock deaths.
As the wolf population has rebounded, after being hunted to the brink of extinction in the 1930s, they have encroached increasingly on farmland.A new five-year government plan allows a small number of wolves to be culled each year, but farmers are demanding the right to shoot dead any wolf that attacks their herds.
Firefighters watch as thick smoke from a wildfire fills the air near Calistoga, California on Oct. 12.
An onslaught of wildfires across a wide swath of Northern California broke out almost simultaneously Sunday night then grew exponentially, swallowing up properties from wineries to trailer parks and tearing through both tiny rural towns and urban subdivisions.
At least 9,000 firefighters from across the state and the country are attacking the flames.
Coffey Park homes burn early in Santa Rosa, California on Oct. 9. The wildfires across the state have chased about 90,000 people from their homes.
Photos: Massive Wildfires Consume Homes Across Northern California
Opposition supporters hold up bricks as they block streets and burn tires during a protest in Kisumu, Kenya, on Oct. 11. Supporters of Kenya's opposition leader Raila Odinga took to the streets as poll officials mull their next move after he withdrew from the new presidential election vote ordered by the Supreme Court.
Los Angeles Dodgers first baseman Cody Bellinger falls over the railing as he catches a fly ball hit in the fifth inning during a playoff baseball series against the Arizona Diamondbacks in Phoenix on Oct 9.
People watch as the Hoa Binh hydroelectric power plant opens the flood gates after heavy rainfall in Hoa Binh province, Vietnam on Oct. 12.
A couple dances in a park in Huai'an, China on Oct. 10.
Canadian Prime Minister Justin Trudeau, President Donald Trump, first lady Melania Trump and Sophie Gregoire Trudeau stand outside the White House in Washington on Oct. 11.
Trudeau joined Trump in the Oval Office at the start of a new round of talks over NAFTA, which the president has threatened to withdraw from if he can't negotiate a better agreement with Canada and Mexico.
A Michibiki satellite lifts off aboard an H-2A rocket from Japan's southern Tanegashima space port on Oct. 10. Japan on Tuesday launched a fourth satellite for a new high-precision global positioning system it hopes will encourage new businesses and help spur economic growth.
The Coffey Park neighborhood was destroyed as wildfires swept through Santa Rosa, California, on Oct. 11.Santa Rosa, a city of 175,000 people, was one of the hardest-hit communities during the wildfires that ignited Sunday night.
Photos:From Above, California Wine Town an Ashy Wasteland
Kristine Pond searches the remains of her family's home destroyed by fires in Santa Rosa, California on Oct. 9.Since igniting Sunday in spots across eight counties, the blazes have killed 32 people and destroyed at least 3,500 homes and businesses, making itthe deadliest week of wildfires in California history.
The body of a cow that died in the Atlas Fire lays along the side of a road in Soda Canyon, near Napa, California on Oct. 11.
Rohingya refugees walk after crossing the Naf river from Myanmar into Bangladesh in Whaikhyang on Oct. 9.
More than 500,000 Rohingya Muslims have fled from Myanmar to neighboring Bangladesh since Aug. 25, when security forces responded to attacks by a militant Rohingya group with a broad crackdown on the long-persecuted Muslim minority. Many houses were burned down.
A Rohingya refugee cries as she nurses a baby after crossing the Naf river from Myanmar into Bangladesh in Whaikhyang on Oct. 9.
Photos:Heartbroken Rohingya refugees bury dead after boat capsizes
Fifty-eight white doves are released in honor of the victims of the mass shooting in Las Vegas, at the culmination of a faith unity walk on Oct. 7 in Las Vegas.
Photos:Nation Mourns Las Vegas Route 91 Mass Shooting
Puerto Rican National Guard deliver food and water, brought via helicopter, to residents of Morovis, Puerto Rico on Oct. 7, in the aftermath of Hurricane Maria.
The storm is blamed for at least 45 deaths and it damaged or destroyed tens of thousands of homes. About 90 percent of the island remains without power and 40 percent without water service.
Photos:Relief Team in Puerto Rico Brings Aid and Comfort to Elderly
Residents signal that they need water as UH-60 Blackhawk helicopters from the First Armored Division's Combat Aviation Brigade fly past during recovery efforts following Hurricane Maria, near Ciales, Puerto Rico on Oct. 7.
Photos:Dark Days and Long Nights Descend on Puerto Rico
A man dives into the Espiritu Santo river, in a hurricane-damaged section of forest, in Palmer, Puerto Rico on Oct. 8.
A huge flock of common starlings fly above a field at Tolcsva, Hungary on Oct. 6.
The Week in Pictures:Sept. 29 - Oct. 6
