__HTTP_STATUS_CODE
200
__TITLE
Federal Grand Jury Charges Hawaii Army Sergeant With Supporting ISIS
__AUTHORS
Phil McCausland
__TIMESTAMP
Jul 22 2017, 1:09 pm ET
__ARTICLE
 A 34-year-old Army sergeant was indicted in Hawaii on Friday and faces four counts of attempting to provide material support to ISIS. 
 Sgt. Ikaika Erik Kang, an air traffic control operator with the 25th Infantry Division at U.S. Army Pacific Command in Hawaii, was taken into custody by an FBI SWAT team earlier this month after he was surveilled for nearly a year, according to court documents. The indictment said he attempted to provide the militant group secret information, a drone and other gear, as well as military training. 
 He faces up to 20 years in prison and $250,000 for each of the four counts hes charged with, according to the Justice Department. He is set to appear in a preliminary hearing on Monday. 
 Kang allegedly sought to provide ISIS with classified military documents and training and declared his allegiance to the jihadist militant group, according to an FBI affidavit. 
 A search of Kangs computer found 18 documents marked SECRET  16 of those remain classified today, according to the FBI. The Army sergeant also said he wanted to join the group and considered traveling to Turkey, where he believed ISIS had a consulate. 
 According to the indictment, he is also accused of providing "a GoPro Karma drone, a chest rig (which is a piece of military-style equipment worn over the shoulders that has chest pouches and is typically used to hold tactical equipment, ammunition, and other military gear), and other military-style clothing and gear." 
 Related: US Soldier Arrested in Hawaii, Accused of Trying to Support ISIS 
 Authorities arrested Kang on July 8 after he pledged loyalty to Abu Bakr al-Baghdadi, the leader of ISIS, and said he wanted to kill a bunch of people, the FBI said. 
 "People still say it's illegal to join them, but the way I look at it is they're just fighting people who are committing genocide there," Kang said, according to the affidavit. "I'm just going to go there ... and fight these guys who are committing genocide." 
 Kangs attorney, Birney Bervar, previously told NBC News that Kang, who served in Iraq in 2010 and Afghanistan in 2014, may have some service-related mental health issues which the government was aware of but neglected to treat." 
 The Army referred Kang to the FBI in August 2016, the affidavit said, and reported that he had made threatening remarks and statements about supporting ISIS since 2011. 
 Kang's father, Clifford, told NBC News-affiliate KHNL that his son enlisted in the Army after the Sept. 11, 2001, terrorist attack and later converted to Islam. 
