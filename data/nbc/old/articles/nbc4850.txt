__HTTP_STATUS_CODE
200
__TITLE
A New Way to Fight Heart Disease May Also Tackle Cancer
__AUTHORS
Maggie Fox
Felix Gussone, MD
__TIMESTAMP
Aug 28 2017, 10:11 am ET
__ARTICLE
 Researchers say theyve proven a long-held theory about heart disease: that lowering inflammation may be nearly as important as cutting cholesterol levels. 
 They showed that using a targeted drug to reduce inflammation cut the risk of heart attacks, strokes and other events in patients who had already suffered one heart attack  independent of any other treatment they got. 
 A bonus side-effect  the treatment also appeared to have reduced rates of lung cancer diagnosis and death. 
 The studies, being presented at a meeting in Barcelona this weekend, are just a first step and do not yet open a door to a new way of treating heart patients. 
 And they dont necessarily apply to everybody. But Dr. Paul Ridker of Brigham and Womens Hospital and Harvard Medical School, who led the research team, thinks the findings will lead to ways to help people most at risk of dying of heart disease and stroke. 
 This plays beautifully into the whole idea of personalized medicine and trying to get the right drug to the right patient, Ridker said. 
 Novartis, which makes the drug, said it would ask the Food and Drug Administration for permission to market the drug as a way to prevent heart attacks and would start further tests on its effect in lung cancer. 
 Ridkers team tested 10,000 patients who had suffered one heart attack already and thus were at very high risk of having a second one. The patients all had high levels of high sensitivity C-reactive protein or CRP, a measure of inflammation in the body. 
 Related: FDA Approves Pricey New Cholesterol Drug 
 They were already taking a basket of medications for their heart disease, from cholesterol-lowering statins to blood pressure drugs. 
 On top of that, the team added a drug called canakinumab, a monoclonal antibody or magic bullet agent that targets a specific cause of inflammation called interleukin 1 beta. 
 Volunteers got either a placebo, or injections every three months of low, medium or high doses of canakinumab. 
 After three to four years, people who got the highest dose of the drug were the least likely to have had another heart attack, stroke or to have died of heart disease. 
 Those who got the two highest doses of canakinumab had a 15 percent lower chance of having a heart attack, stroke or other major cardiovascular event, the team found. Patients were also less likely to need a heart bypass or angioplasty to clear out clogged arteries. 
 For the first time, weve been able to definitively show that lowering inflammation independent of cholesterol reduces cardiovascular risk, Ridker said. 
 Dr. Steven Nissen, chairman of the Department of Cardiovascular Medicine at the Cleveland Clinic, who was not involved in the study, said the results were impressive. It shows us that people with high levels of inflammation - if you target the inflammation - you can reduce the risk of heart attack stroke and death, Nissen said. 
 Related: Here's How Stress Might Cause Heart Attacks, Strokes 
 The results are being presented at the European Society of Cardiology meeting in Barcelona, and also published in the New England Journal of Medicine and the Lancet medical journal. 
 Its been long known that both inflammation and cholesterol buildup are involved in heart and artery disease. 
 Inflammation is part of the bodys immune process, and the patients in the trial were more likely to suffer serious infections, including pneumonia. The same thing happens to people taking immune-suppressing drugs to fight rheumatoid arthritis. 
 Physicians would have to be cautious, Ridker said. 
 But the researchers found some other side-effects. People taking the higher doses of canakinumab had lower rates of cancer, especially lung cancer, as well as lower rates of arthritis and gout. 
 This makes sense to Ridker. 
 If you smoke a pack of cigarettes, you chronically inflame the lung. If you are a long-haul truck driver breathing in diesel, you are chronically inflaming the lung, he said. Inflammation can drive cancer as well as heart disease, he said. 
 These are fascinating, human findings that open a potential new class of therapies for cancer, said Dr. Laurie Glimcher, president and CEO of the Dana-Farber Cancer Institute. 
 Ridker does not believe the drug prevents cancer. He thinks inflammation may fuel the growth of some tumors. 
 The tumors were obviously already there. They were just small and undiagnosed, he said. 
 Related: Heart Attacks, Strokes, Fell After Trans-Fat Ban 
 The findings will not immediately mean new treatments for heart disease patients. For one thing, like any medical finding, theyll have to be replicated by other researchers. Ridkers testing another drug, methotrexate, thats also used to treat rheumatoid arthritis. 
 While canakinumab has already been approved by the FDA, it is a so-called orphan drug used to treat a very rare genetic condition. Sold under the brand name Ilaris, it costs about $200,000 a year. 
 We look forward to submitting the ... data to regulatory authorities for approval in cardiovascular and initiating additional phase III studies in lung cancer," said Vas Narasimhan, who heads drug development for Novartis. 
 Ridker says he is pressing Novartis to try something different, perhaps offering the first dose of the drug free. People whose CRP levels fell more after their first dose also tended to be those who had lower rates of heart attacks and strokes years later. 
 It might be worth taking one dose and see if you respond. If you dont, well, there is no reason to be on the drug, he said. 
 This is the way to really focus these treatments on the patients on whom it really works. I think thats just good medicine. 
 In the end, Ridker believes, some extreme heart disease patients will be helped more by the newest cholesterol-lowering drugs, called PCSK9 inhibitors, while others may be better helped by targeted anti-inflammatory drugs. 
 Half of heart attacks occur in people who do not have high cholesterol, he said. For the first time, weve been able to definitively show that lowering inflammation independent of cholesterol reduces cardiovascular risk. 
 Nissen agrees. 
 I think its a game changer. The only good therapies weve had so far were statins. But now it seems like we have something new in the future, he said. 
 It opens up pathways to new research and new treatments in the future. There are many other anti-inflammatory activities going on in our body, not just the one thats tackled by canakinumab. There will be so many more studies now to see if other therapies that tackle other pathways will also reduce the risk. 
 And the findings may offer some common-sense advice to everyone about lowering inflammation, Ridker said. 
 Theres a lot you can do about it right now, he said. 
 If your high sensitivity C-reactive protein is elevated, you are a high-risk patient. This is overwhelming evidence that you should go to the gym, throw out the cigarettes, eat a healthier diet, he said. 
 Because all three of those well-known interventions lower your inflammatory burden. 
