__HTTP_STATUS_CODE
200
__TITLE
Iraqi Forces Fight to Recapture Mosul Block by Block
__AUTHORS
NBC News
__TIMESTAMP
Jan 18 2017, 4:45 am ET
__ARTICLE
An Iraqi special forces Counter Terrorism Service (CTS) member screams during a battle against ISIS in Mosul's al-Rifaq neighborhood, as an ongoing military operation against the militants continues, on Jan. 8, 2017.
ISIS has been driven out of most eastern districts of its Iraqi stronghold in the three months since the U.S.-backed campaign began. Iraqi troops have seized large areas along the river, which bisects Mosul from north to south and have begun "moving" in western Mosul.
A man crosses a street through the bullet-riddled windshield of a CTS Humvee, on Jan. 10.
Capture of the entire east bank, which military officials say is imminent, will allow the army, special forces and elite police units to begin attacks on the city's west, still fully held by the militants.
An Iraqi army T-72 tank heads to the front line during a battle against ISIS near the Fourth Bridge over the Tigris River connecting eastern and western Mosul, on Jan. 10.
CTS forces pushed into the Eastern Nineveh and Souq al-Ghanam districts, which are flanked by areas held by Iraqi troops, spokesman Sabah al-Numan said on Nov. 16.
An Iraqi family flees the fighting in eastern Mosul, on Jan.15.
The special forces have now taken control of the Andalus and Shurta neighborhoods.
An elderly Iraqi man comforts a wounded boy as he receives treatment from military doctors in Mosul's al-Sahiroun neighborhood, on Jan. 12.
Several thousand civilians have been killed or wounded in the street-to-street fighting since the U.S.-backed offensive began in October.
Iraqis walk down a street while smoke rises in the background following a car bomb explosion in eastern Mosul, on Jan.15.

A member of the Iraqi special forces' CTS, fires his machine gun as he holds a position inside Mosul's university, on Jan.15.
Iraqi forces on January 14 retook Mosul's university from the ISIS.
The body of a suspected ISIS fighter lies on the ground in Mosul's al-Sahiroun neighborhood, on Jan. 12.
An Iraqi man shields his daughter from seeing the body of an ISIS fighter as they flee Mosul's al-Mithaq neighborhood, on Jan. 8.
An Iraqi special forces CTS member shoots at a drone flown by ISIS in Mosul's al-Rifaq neighborhood, on Jan. 8.
In the battle for Mosul, drones are being used by both sides to get details on the topography, adjust their targets and to find places to plant car bombs.
An Iraqi army soldier takes position during a battle against ISIS near the Fourth Bridge over the Tigris River connecting eastern and western Mosul, on Jan. 10.
Iraqis push a cart carrying an elderly woman in Mosul's al-Sahiroun neighbourhood, on Jan. 12.
A member of the Iraqi special forces' CTS holds a sword while guarding a position at Mosul's university, on Jan. 15.
A gunner from the Iraqi Rapid Response Division mans a machine gun in a Humvee in Mosul's Yarimjah neighbourhood, on Jan. 12.
A member of the Iraqi special forces' CTS patrols alongside a burnt-out building in Mosul's university, on Jan. 15.
A member of the Iraqi special forces' CTS, stands guard next to a damaged building at Mosul's university, on Jan. 15.
