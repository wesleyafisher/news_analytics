__HTTP_STATUS_CODE
200
__TITLE
Woman Gets $417 Million Verdict From Johnson & Johnson in Baby Powder Cancer Suit
__AUTHORS
Associated Press
__TIMESTAMP
Aug 22 2017, 6:24 am ET
__ARTICLE
 LOS ANGELES  A Los Angeles jury on Monday ordered Johnson & Johnson to pay a record $417 million to a hospitalized woman who claimed in a lawsuit that the talc in the company's iconic baby powder causes ovarian cancer when applied regularly for feminine hygiene. 
 The verdict in the lawsuit brought by the California woman, Eva Echeverria, marks the largest sum awarded in a series of talcum powder lawsuit verdicts against Johnson & Johnson in courts around the U.S. 
 Echeverria alleged Johnson & Johnson failed to adequately warn consumers about talcum powder's potential cancer risks. She used the company's baby powder on a daily basis beginning in the 1950s until 2016 and was diagnosed with ovarian cancer in 2007, according to court papers. 
 Echeverria developed ovarian cancer as a "proximate result of the unreasonably dangerous and defective nature of talcum powder," she said in her lawsuit. 
 Echeverria's attorney, Mark Robinson, said his client is undergoing cancer treatment while hospitalized and told him she hoped the verdict would lead Johnson & Johnson to put additional warnings on its products. 
 "Mrs. Echeverria is dying from this ovarian cancer and she said to me all she wanted to do was to help the other women throughout the whole country who have ovarian cancer for using Johnson & Johnson for 20 and 30 years," Robinson said. 
 "She really didn't want sympathy," he added. "She just wanted to get a message out to help these other women." 
 Related: Can Talcum Powder Really Cause Cancer? 
 The jury's award included $68 million in compensatory damages and $340 million in punitive damages, Robinson said. The evidence in the case included internal documents from several decades that "showed the jury that Johnson & Johnson knew about the risks of talc and ovarian cancer," Robinson said. 
 "Johnson & Johnson had many warning bells over a 30 year period but failed to warn the women who were buying its product," he said. 
 Johnson & Johnson spokeswoman Carol Goodrich said in a statement that the company will appeal the jury's decision. She says while the company sympathizes with women suffering from ovarian cancer that scientific evidence supports the safety of Johnson's baby powder. 
 The verdict came after a St. Louis, Missouri jury in May awarded $110.5 million to a Virginia woman who was diagnosed with ovarian cancer in 2012. 
 She had blamed her illness on her use of the company's talcum powder-containing products for more than 40 years. 
 Besides that case, three other trials in St. Louis had similar outcomes last year  with juries awarding damages of $72 million, $70.1 million and $55 million, for a combined total of $307.6 million. 
 Another St. Louis jury in March rejected the claims of a Tennessee woman with ovarian and uterine cancer who blamed talcum powder for her cancers. 
 Two similar cases in New Jersey were thrown out by a judge who said the plaintiffs' lawyers did not presented reliable evidence linking talc to ovarian cancer. 
 More than 1,000 other people have filed similar lawsuits. Some who won their lawsuits won much lower amounts, illustrating how juries have wide latitude in awarding monetary damages. 
 Johnson & Johnson is preparing to defend itself and its baby powder at upcoming trials in the U.S., Goodrich said. 
