__HTTP_STATUS_CODE
200
__TITLE
Equifax CEO Richard Smith Steps Down After Epic Breach
__AUTHORS
Ben Popken
__TIMESTAMP
Sep 26 2017, 9:53 am ET
__ARTICLE
 The CEO of Equifax, Richard Smith, is stepping down in the wake of a data breach that affected millions of Americans, the credit bureau's board of directors announced Tuesday. 
 "The cybersecurity incident has affected millions of consumers, and I have been completely dedicated to making this right," Smith said in a statement. "At this critical juncture, I believe it is in the best interests of the company to have new leadership to move the company forward." 
 Smith had served at Equifax's helm for 12 years. Paulino do Rego Barros Jr, president of the company's Asia Pacific division, was tapped to serve as interim CEO, and board member Mark Feidler will serve as nonexecutive chairman. The board has launched a search for a new permanent CEO. 
 Meanwhile, Smith will serve "as an unpaid adviser to Equifax to assist in the transition," the board said in the statement. 
 Related: The One Move to Make After Equifax Breach 
 Smith will depart with no severance and will forfeit his estimated $3 million performance bonus for 2017, but will retain $18.3 million in pension benefits, Equifax spokesperson Wyatt Jefferies wrote NBC News in an email. He received a base salary of $1.45 million, according to SEC filings. 
 More than 143 million Americans' personal credit records, including name, address, Social Security number, date of birth and credit history, were exposed in the data breach, the company has said. In addition, more than 200,000 credit card numbers were stolen, Visa and Mastercard said in a private notice to banks. 
 In March, vulnerability on a popular server software system used by Equifax and others was exposed by researchers and quickly exploited by hackers to gain control of entire systems that hadnt been patched. Equifax said the breach on their systems occurred from mid-May to July. 
 Related: Equifax Fallout: FTC Launches Probe; Websites and Phones Jammed With Angry Consumers 
 Consumer outrage boiled over after the disclosure, with Americans rushing to freeze their credit reports, only to find Equifax's website and systems overwhelmed. The company's chief security officer and chief information systems officer abruptly stepped down in the aftermath. 
 The FBI has opened an investigation into the incident, which affected 75 percent of the adult population of the United States. Cybersecurity experts say a state actor couldn't be ruled out as a culprit. 
 Attorney generals from more than 30 states have opened investigations into Equifax. The company is also fending off multiple consumer class action and small-claims lawsuits. 
 Consumer advocates and cybersecurity experts said that while Smith's departure was a step in the right direction, it didn't solve key underlying problems. 
 "Equifax is trying to change the optics by a rapid move to sacrifice king as well as a few knights," Ed Mierzwinski, senior fellow for U.S. PIRG, a Washington-based consumer advocacy group, said in an email. Only action by Congress and the Consumer Financial Protection Bureau will suffice to keep up pressure on the credit reporting marketplace, he said. 
 One cybersecurity expert told NBC News that a chief executive has little to do with cybersecurity. 
 "Ultimately he just approves a budget," the expert said. "Fresh faces will roll in, but they will find themselves constrained by the budgetary restraints." 
