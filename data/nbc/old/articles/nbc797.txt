__HTTP_STATUS_CODE
200
__TITLE
Under Sessions Plan, Government Will Seize More Peoples Property
__AUTHORS
Jon Schuppe
__TIMESTAMP
Jul 18 2017, 4:47 pm ET
__ARTICLE
 Should the government be able to seize people's property in the name of crime fighting, even without evidence to prove someone has broken the law? 
 A growing number of politicians and policy makers across the political spectrum say that's an abuse of Americans' civil rights. 
 But the nation's top law enforcement officer disagrees, putting him at odds with an effort  led by conservative members of his own party  to curb the practice. 
 This week, Attorney General Jeff Sessions plans to release a directive ordering an expansion of the federal government's use of civil asset forfeiture. He also said he'd undo an Obama administration order that prohibited local authorities from using the federal system to sidestep restrictive state laws. 
 Sessions has linked unfettered forfeitures with a broader crackdown on drug trafficking, citing an uptick in violence and deadly overdoses as justification. But critics say that approach, including orders to seek longer prison sentences, recalls the early years of the war on drugs, when the government embraced zero-tolerance policies that helped reduce crime but also drove dramatic increases in prison populations  with little impact on drug use. 
 A March report by the Justice Department's Office of Inspector General pointed out that from 2007 to 2016, the Drug Enforcement Administration obtained forfeitures for $3.2 billion in cash seizures without charging the people they took it from. 
 Related: GOP Civil War to Fill Jeff Sessions Senate Seat 
 "When seizure and administrative forfeitures do not ultimately advance an investigation or prosecution, law enforcement creates the appearance, and risks the reality, that it is more interested in seizing and forfeiting cash than advancing an investigation or prosecution," the report said. 
 Speaking to a gathering of local prosecutors in Minneapolis on Monday, Sessions scoffed at the backlash against his proposed crackdown, saying the government needed to release police and prosecutors from onerous policies. 
 "With care  we gotta be careful  and professionalism, we plan to develop policies to increase forfeitures," Sessions told members of the National District Attorneys Association. "No criminal should be allowed to keep the proceeds of their illegal activity." 
 The prosecutors applauded. But the move is expected to meet stiff opposition from a bipartisan coalition of lawmakers. 
 That includes some of the most conservative members of Congress. 
 "This is a step in the wrong direction and I urge the Department of Justice to reconsider," Rep. Jim Sensenbrenner, Republican of Wisconsin, said in a statement. "Expanding forfeiture without increasing protections is, in my view, unconstitutional and wrong." 
 Related: Activist Faces New Trial for Laughing During Jeff Sessions Hearing 
 Sensenbrenner is the sponsor of a House bill that would make it easier for innocent victims of asset forfeiture to get their property returned. A related bill, called the FAIR Act, would make similar changes, and would also direct the proceeds into a fund controlled by Congress. Sessions, an Alabama Republican and former federal prosecutor, opposed such reforms when he was a member of the Senate. 
 The FAIR Act's main sponsor, Republican Sen. Rand Paul of Kentucky, responded to Sessions' announcement with a defense of the Fifth Amendment, which prohibits the taking of private property for public use, without just compensation. "I oppose the government overstepping its boundaries by assuming a suspect's guilt and seizing their property before they even have their day in court," Paul said in a statement. 
 Sen. Mike Lee of Utah, another conservative sponsor of the FAIR Act, said in a statement that the Justice Department "has an obligation to consider due process constraints in crafting its civil asset forfeiture policies." 
 Jason Snead, a policy analyst at the conservative Heritage Foundation, said Sessions' move was not surprising given his past opposition to reform and his strong identification with law enforcement, which is basically the only bloc that supports greater use of civil asset forfeiture. 
 "That is absolutely not what we have come to expect as Americans of the criminal justice system," Snead said. 
 Sessions predecessor, Loretta Lynch, also a former federal prosecutor, supported civil asset forfeiture. 
 Civil forfeiture laws, rooted in American maritime law and expanded during the drug scares of the mid-1980s, were designed to eviscerate large drug trafficking operations by allowing authorities to take ill-gotten cash, cars, homes and other properties. 
 Those seizures have since become a booming government enterprise, used in busts small and large, and resulting in billions in forfeited assets. 
 Related: Jeff Sessions Tells Hate Group DOJ Will Issue Religious Freedom Guidance 
 With the expansion of forfeitures came allegations that police and federal agents are driven more by profit than by crime fighting. Many suspects have lost property without being charged with a crime, and many innocent people have found it nearly impossible to get their property back. 
 Sometimes, critics say, police doing roadside searches simply take cash in exchange for not locking someone up. 
 "The fact they're seizing property and not connecting it to a crime down the road shows how ineffective it is," said Kanya Bennett, legislative counsel for the American Civil Liberties Union. 
 Several such cases were collected in a 2015 report by the Institute for Justice, a Virginia non-profit that fights to limit government power. Among the most egregious took place in Tenaha, Texas, where a lawsuit exposed a local law enforcement program in which officers targeted out-of-state drivers, searched their cars on flimsy evidence, seized cash and threatened the subjects with bogus charges if they refused to waive their rights to the property. 
 The report also documented cases in which innocent people lost their cars because relatives or loved ones had used them in alleged crimes. 
 As stories of abuses mounted, many states moved to restrict the use of civil asset forfeiture. But authorities in some of those states found a way around it: by asking the federal government to step in, take the bounty and share it with them. 
 Related: Attorney General Jeff Sessions Criticized for Speaking to 'Hate Group' 
 In 2015, then-Attorney General Eric Holder prohibited the federal government from entering such arrangements, known as "adopted" forfeitures. 
 The number of seizures dropped significantly after that order. Seizures by the DEA, which performs the vast majority of federal seizures, decreased from $8.6 billion in 2014 to $6.1 billion the following year, and to $4.6 billion in 2016, according to the March inspector general's report. 
 The report sharply criticized the federal government's use of civil asset forfeitures, saying it didn't keep good enough records to determine if seizures actually helped criminal investigations or whether they violated people's civil liberties. 
 Sessions seems undeterred. 
 He indicated Monday that he'd eliminate Holder's 2015 mandate. 
 "Adopted forfeitures are appropriate," Sessions said. "Sharing of assets with our state and local law enforcement colleagues is appropriate. We want forfeitures to increase, the sharing to increase." 
 Sessions' coming directive will put more pressure on Congress, and state lawmakers, to push for reforms, advocates said. 
 Darpana Sheth, a senior attorney at the Institute for Justice, called Sessions' announcement "a disheartening setback in the fight to protect Americans private property rights." 
 Nearly half the states have taken some steps to roll back civil forfeiture laws, Sheth said in a statement. "The Attorney Generals plan to increase forfeitures is jarringly out of step with those positive developments." 
