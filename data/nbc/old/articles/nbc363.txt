__HTTP_STATUS_CODE
200
__TITLE
Trump Budget Cuts to Scientific, Medical Research Would Have Devastating Effect: Experts
__AUTHORS
Maggie Fox
__TIMESTAMP
Mar 16 2017, 1:18 pm ET
__ARTICLE
 President Donald Trump's plan to cut billions of dollars in funding to medical and scientific research agencies would cost the country countless jobs, stall medical advances and threaten America's status as the world leader in science and medicine, advocates said Thursday. 
 Cutting the funding in this way will have devastating and generation-long effects, said Dr. Clifford Hudis, CEO of the American Society of Clinical Oncology, which represents cancer specialists. 
 [Medical research] is a fundamental driver of American economic strength and it is being compromised here, Hudis told NBC News. Its a jobs program. 
 Multiple organizations expressed shock and disappointment at Trumps budget proposal, which adds $54 billion in defense spending but would slash nearly $6 billion from the National Institutes of Health, which funds most basic medical research in the country, as well as eliminate entirely dozens of other agencies and programs. 
 It would cut the overall Health and Human Services department budget by 18 percent, including the 20 percent budget reduction at NIH, and reassign money from the Centers for Disease Control and Prevention to states. 
 Most cancer drugs get their start in the basic research funded by the NIH and often done in NIH labs. 
 The targeted therapies, the immunotherapies, the conventional chemotherapy drugs  all of these things have roots in the NIH, Hudis said. 
 One example of the discoveries the NIH and NIH-funded researchers make: Finding the cancer-killing properties of trees, or sea sponges, and developing them into compounds that are then licensed to pharmaceutical companies to develop. Companies rarely do such basic, risky research. 
 Related: Budget Threatens Cancer Progress 
 The American Lung Association urged Congress to ignore Trump's budget blueprint and increase funding. 
 Congress must reject this budget and start anew with a balanced approach that protects vital health programs in HHS and at EPA (the Environmental Protection Agency), Lung Association CEO Harold Wimmer said. 
 American Heart Association President Steven Houser said he was shocked by the budget proposal. 
 I thought we were, all of us, interested in improving the health of all Americans, he said. We need to give more, not less. 
 The AHA projects that by 2035, up to half of all Americans will have heart disease. 
 The predicted cost is $1.1 trillion, Houser told NBC News. 
 If we dont figure out how to slow stop or reverse these trends, we are going to pay way more, he added. You can save $6 billion today and spend $1 trillion down the road. 
 There are a few bright spots for health in the Trump budget proposal, however. It appears to create a rapid response fund that CDC, NIH and other infectious disease experts have been begging for. 
 Related: GOP Health Care Bill Cuts Vital Disease Fund 
 The Budget also creates a new Federal Emergency Response Fund to rapidly respond to public health outbreaks, such as Zika Virus Disease, the budget proposal reads. It doesnt give details, but CDC and NIH renewed their requests for such a fund after a nine month-long fight with Congress last year over funding Zika response efforts. 
 It also increases medical spending for the Department of Veterans Affairs, increasing the overall VA budget by 6 percent to nearly $79 billion. That includes $4.6 billion for VA health care to improve patient access and timeliness of medical care services for over nine million enrolled veterans, the budget plan reads. 
 This funding would enable the Department to provide a broad range of primary care, specialized care, and related medical and social support services to enrolled veterans, including services that are uniquely related to veterans health and special needs. 
 Heres how the budget would affect key medical and scientific agencies: 
 CDC: The budget reforms CDC with a $500 million block grant to states. The White House says the idea is to allow states flexibility. Wimmer, the American Lung Association CEO, calls it a radical change." 
 This approach could have devastating impacts on state asthma programs, tobacco prevention and cessation and tuberculosis control, Wimmer said. 
 FDA: The budget doesnt say much about the FDA but specifically doubles user fees  money paid by pharmaceutical companies to speed approvals, from $1 billion to $2 billion. In a constrained budget environment, industries that benefit from FDAs approval can and should pay for their share, it says. 
 NIH: NIH had just gotten a big boost in the budget resolution Congress passed in December  a 6.6 percent funding increase to $32 billion. Trump's budget takes $6 billion of that back out, and eliminates the Fogarty International Center, which coordinates international medical research. Its kind of like pretending that Ebola doesnt exist and that it doesnt come into our country, said Ellie Dehoney of the advocacy group Research America. 
 The NIH budget had been flat for years, and after inflation actually fell to 20 percent below what it was in 2003. 
 Agency for Healthcare Research and Quality (AHRQ): Trumps budget plan moves the little-known Agency for Healthcare Research and Quality into NIH, which Dehoney says is the equivalent of eliminating it. Thats just shooting yourself in the foot if the goal is to be fiscally responsible, she said. What AHRQ does is cut waste out of a system that is very costly, she added. AHRQ has done work that is proven to lower medical errors and save billions of dollars in medical spending. 
 Dehoney and others say theyre hopeful Congress wont go along with all the cuts. 
 Congress has a long bipartisan history of protecting research investments. We encourage Congress to act in the nations best interest, said Rush Holt, CEO of the American Association for the Advancement of Science (AAAS). 
 "We are grateful and encouraged that members of Congress have already spoken out about the importance of keeping NIH funding at healthy levels. It would be a tremendous disappointment if we backed away now from all the gains that have been made and all those that are within reach," added David Arons, CEO of the National Brain Tumor Society. 
