__HTTP_STATUS_CODE
200
__TITLE
How Much Does It Cost to Have a Kid? Try $13,000 a Year
__AUTHORS
Maggie Fox
__TIMESTAMP
Jan 11 2017, 8:33 am ET
__ARTICLE
 Couples who had a child in 2015 will end up spending an average of $13,000 a year raising that child, which adds up to $233,000 by the time the kid is 17, according to new government calculations. 
 Thats down from past years  the estimate was $245,000 in 2014. 
 The estimates vary a lot depending on where people live and how much money they make. Daycare alone for an infant in New York City, for example, can run $18,000 a year or more. 
 But the annual U.S. Department of Agriculture projections can give parents an idea of what they are up against as they build a family. 
 And, since they only go to age 17, they don't include college costs. 
 Related: The High Cost of Child Care in 2014 
 "Understanding the costs of raising children and planning for anticipated and unexpected life events is an important part of securing financial health, said Louisa Quittman, who directs the Office of Financial Security at the Treasury Department. 
 Fewer Americans are having children. The Centers for Disease Control and Preventions National Center for Health Statistics reported last week that 3.9 million babies were born in the U.S. in 2015, down about 1 percent from 2014. In general, birth rates for younger women are falling, and theyre increasing for older women. 
 Related: Child Care Costs Can Cripple 
 Its not the strollers, baby clothes and toys that cost the most. The biggest cost by far is housing, which accounts for 29 percent of the extra costs of having a child. Food costs make up 18 percent of the calculation, while child care and education account for 16 percent. 
 Related: Comparing Trump's and Clinton's Child Care Plans 
 And each child adds proportionately less, because kids can double up on rooms and in the back seats of cars. 
 There are significant economies of scale, with regards to children, sometimes referred to as the 'cheaper by the dozen effect, said USDA economist Mark Lino, who wrote the report. 
 Related: What Will That Kid Cost You? 
 As families increase in size, children may share a bedroom, clothing and toys can be reused and food can be purchased in larger, more economical packages. 
 Want to know how much to budget for your family? 
 The USDA has a calculator here to help people estimate the cost. 
