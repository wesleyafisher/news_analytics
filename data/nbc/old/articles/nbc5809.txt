__HTTP_STATUS_CODE
200
__TITLE
In Little Fires Everywhere, Author Celeste Ng Explores Asianness and Family
__AUTHORS
Taylor Weik
__TIMESTAMP
Sep 7 2017, 8:42 am ET
__ARTICLE
 In the three years since the release of her New York Times best-selling debut Everything I Never Told You, Celeste Ng has been thinking a lot about growing up. 
 Her next novel, Little Fires Everywhere, scheduled for release Sept. 12, touches on the subject, following multiple families in Shaker Heights, Ohio, a master-planned suburb of Cleveland and Ngs hometown. 
 The book, which takes place in the 90s, focuses on a clash between families  one thats been in Shaker Heights for generations, the other new to the town  after a custody battle over an adopted Chinese-American baby has the neighborhood on opposing sides. 
 Shaker Heights was a really great place to grow up in, and it shaped me into the kind of person I am in a lot of ways, I think, Ng told NBC News. But now I also now see some of the blind spots it has. It does struggle with issues of race and class, like everywhere else does. I was interested in that tension, and I wanted to try to write about it. 
 Born to Chinese immigrants from Hong Kong, Ng was an early reader and remembers being raised with classic British literary works like The Secret Garden, Peter Pan, and The Chronicles of Narnia series. 
 Her interests shifted once she left Shaker Heights to attend Harvard University, where she majored in English. Diaspora and culture became recurring themes in the books she consumed, and she enrolled in classes on Indian-American literature and studied Mandarin for a year. 
 When I got to college, I developed an interest in where I had come from, Ng said. I feel like that happens to a lot of people when they go to college. You move away, and suddenly you realize that you actually really love your mothers cooking, or you love the family rituals that youre no longer with. 
A photo posted by NBC News (@nbcnews)
 After graduating, Ng landed a job in publishing at Houghton Mifflin in Boston, where she worked in the textbooks division. Though shed written short stories as a child and took several creative writing classes in college, literature was never something she thought she could turn into a career. 
 The child of a NASA physicist and a chemist who taught and researched at Cleveland State University, Ng saw writing as something she would do in her spare time. 
 I always thought writing was more of a hobby because I come from this family of immigrants who are very practical, and come from the sciences, Ng said. When I was 5, I wanted to be a paleontologist and then write on the side. As I got older, I wanted to be an astronaut and then write on the side, and then I thought Id work in publishing or teach, and write on the side. 
 It wasnt until a favorite professor encouraged her to apply for graduate school that she would pursue writing full-time. 
 Ng was accepted to the University of Michigans creative writing graduate program, where she wrote short stories and essays that would later appear in publications including the New York Times Magazine and the Alaska Quarterly Review. It was at the University of Michigan that Ng penned the first few chapters of what would become Everything I Never Told You. She spent the next six years writing, producing four different drafts before selling the book to Penguin Press in 2012. 
 Much of Ngs writing addresses family dynamics and parent-child relationships, whether shes sifting through the secrets the Lee family keeps from one another in Everything I Never Told You or focusing on a black sheep daughter in the seemingly-perfect Richardson family in Little Fires Everywhere. 
A photo posted by NBC News (@nbcnews)
 Im really interested in the ways we try to become the exact opposite of our parents, and how we inevitably end up being like them anyway, Ng said. Its something I find to be a really rich vein to mine. 
 Another topic Ng explores is race; especially what it means to be Asian, and how one defines their Asianness. 
 In Everything I Never Told You the Lee children are multiracial and their life experiences vary greatly from those of their Chinese father and white mother. Ngs family is also multiracial  she is ethnically Chinese and her husband is white. Together, they have a son. 
 I think a lot about what my sons experience is going to be like, and I think about what my own experience was like in that Im fully Asian, but I was growing up in areas where there werent other Asians around, Ng said. I wanted to talk about what its like to feel like you have a foot in two different places and yet neither of those feet is firmly grounded. 
 In Little Fires Everywhere, a court battle erupts when a white family attempts to adopt a Chinese-American child. 
 Though the novel is set more than 20 years in the past, Ng hopes the characters and their decisions will push her readers to reflect on their beliefs. 
 I hope it will get people thinking about the difference between intentions and action, and that a lot of times having good intentions isnt quite good enough, Ng said. I want people to think about the principles they have, and how they put them into action or how they dont, and why. 
 Follow NBC Asian America on Facebook, Twitter, Instagram and Tumblr. 
