__HTTP_STATUS_CODE
200
__TITLE
Indonesia AirAsia Jet Plunges 24,000 Feet in Just 9 Minutes
__AUTHORS
The Associated Press
__TIMESTAMP
Oct 16 2017, 7:57 pm ET
__ARTICLE
 CANBERRA, Australia  A jetliner rapidly descended by 24,000 feet after the aircraft lost cabin air pressure while traveling from Australia to the holiday island of Bali on Sunday. 
 People aboard Indonesia AirAsia Flight QZ535 described a panicked flight crew announcing an emergency and oxygen masks dropping from the ceiling. 
 Passenger Clare Askew told reporters that airline staff "were screaming, looked tearful and shocked." 
 She added: Now, I get it, but we looked to them for reassurance and we didnt get any, we were more worried because of how panicked they were." 
 Chris Jeanes told NBC News that AirAsia workers sprinted down aisle shouting "emergency, brace, crash positions" about 25 minutes after the plane took off from Perth, Australia. 
 Jeanes, the passenger, had planned to propose to his girlfriend Casey Kinchella in Bali, but pulled down his oxygen mask and did it on the plane. 
 "Luckily she said, 'yes,'" he added. "We both reconfirmed with each other when we were on the ground." 
 The budget airline said in a statement the pilot turned back following a technical issue to ensure the safety of passengers. 
 We commend our pilots for landing the aircraft safely and complying with standard operating procedure, AirAsia Group head of safety Captain Ling Liong Tien said. We are fully committed to the safety of our guests and crew and we will continue to ensure that we adhere to the highest safety standards." 
 The Australian Transport Safety Bureau, an accident investigator, said it was investigating the airliners depressurization at 34,000 feet. 
 The plane rapidly descended to around 10,000 feet, an altitude to which cabins are pressurized and at which oxygen masks are no longer needed. 
 Data from FlightRadar, a website which tracks flights globally using GPS, shows the plane descended 23,800 feet in the space of nine minutes. 
 Perth Airport said in a statement that emergency services were on hand when the plane landed 78 minutes after it took off. 
 Peter Gibson, spokesman for Civil Aviation Safety Authority, the Australian industry regulator, said the airline had been asked for information on what occurred on board. 
 Indonesia AirAsia said passengers on the Sundays aborted flight had been transferred to the next available flight and provided with all necessary assistance. 
 The safety of passengers and crew is our priority, the airline said. AirAsia apologizes to passengers for any inconvenience caused. 
