__HTTP_STATUS_CODE
200
__TITLE
The Week in Pictures: Sept. 8 - 15
__AUTHORS
NBC News
__TIMESTAMP
Sep 15 2017, 8:45 pm ET
__ARTICLE
Tim Scheu plays with his dogs in an emptied Hillsborough Bay ahead of Hurricane Irma's arrival in Tampa, Florida on Sept. 10.Irma's winds and low tide pushed the water unusually far from its normal position. The water is normally about 4 to 5 feet deep and reaches a seawall.
The waters retracted because the leading wind bands of Irma whipped the coastal water more out to sea.
An injured woman leaves the scene after an explosion at a tube station in London on Sept. 15. A homemade bomb exploded on a packed subway train during rush hour, leaving 22 people injured. None of the injured were reported to be seriously hurt.
Police say the explosion was a terrorist attack, the fifth in Britain this year. Britain's domestic spy agency is helping out in the investigation.
A sunken boat lays on its side in Marathon in the Florida Keys on Sept. 13. The Keys were struck hard by Hurricane Irma and officials aren't sure when residents of the middle and lower Keys will be allowed back. FEMA estimates that 25 percent of the homes in the Florida Keys have been destroyed.
PHOTOS:Hurricane Irma Faces Off With Florida
A Cuban man wades through a flooded street in Havana, on Sept. 10. Deadly Hurricane Irma battered central Cuba on Saturday, knocking down power lines, uprooting trees and ripping the roofs off homes as it headed towards Florida.
Seawater penetrated as much as 1,600 feet inland in parts of the city. Authorities said they had evacuated more than a million people as a precaution, including about 4,000 in the capital.
PHOTOS:Hurricane Irma Carves Path of Destruction in Caribbean
The Soyuz space capsule blasts off taking two Americans and a Russian to the International Space Station from Russia's manned launch facility in Kazakhstan on Sept. 13.
Joe Acaba of NASA is making his third trip into space and Russian Alexander Misurkin his second. It's the first voyage for American Mark Vande Hei. All three are to stay on the ISS space station for about 5 1/2 months.
Pedestrians walk by a car on a flooded street as Tropical Storm Irma hits Charleston, South Carolina on Sept. 11. The former hurricane remained an immense, 415-mile wide storm as its center moved on from Florida Monday afternoon, giving its still-formidable gusts and drenching rains a far reach.
Irma sent 4 feet of ocean water into downtown Charleston, as the storm's center passed 250 miles away.
Cleveland Indians' Jay Bruce, left, celebrates with teammates after Bruce hit a double off Kansas City Royals pitcher Brandon Maurer to drive in the winning run during the 10th inning of a baseball game on Sept. 14 in Cleveland.
The Indians rallied in the bottom of the 10th inning for their 22nd straight win to extend their AL record, beating Kansas City 3-2.Cleveland moved within four wins of matching the 1916 New York Giants for the longest streak in major league history.
First lady Melania Trump watches as President Donald Trump speaks to members of the media upon arrival on the South Lawn of the White House on Sept. 10 in Washington.
Sloane Stephens holds up the U.S. Open championship trophy as the lid falls off after she won the women's singles final on Sept. 9 in New York.
Stephens never looked shaken by the setting or the stakes in her first Grand Slam final. She easily beat her close friend Madison Keys 6-3, 6-0 to win the tournament, capping a remarkably rapid rise after sitting out 11 months because of foot surgery.
The 83rd-ranked Stephens, who beat Venus Williams in the semifinals, is only the second unseeded woman to win the tournament in the Open era, which began in 1968.
George Clooney poses with fans at the premiere of "Suburbicon" at the Toronto International Film Festival in Toronto on Sept. 9.
A man walks by a destroyed home following Hurricane Irma on Vilano Beach outside St. Augustine, Florida on Sept. 11.
A rough surf surrounds Boynton Beach inlet as Hurricane Irma hits Boynton Beach, Florida on Sept. 10.
Mary Della Ratta, 94, sits by a battery powered lantern in her home three days after Hurricane Irma knocked out power in Naples, Florida on Sept. 13."I don't know what to do. How am I going to last here?" said Della Ratta.
About 6.8 million people remain without electricity on Wednesday in the steamy late-summer heat as a result of Irma. Utility officials warned it could take over a week for power to be fully restored.
PHOTOS - After Irma: Florida Confronts Damage and Darkness
Relatives of 38-year-old earthquake victim, Juan Jimenez Regalado, a policeman,weep during his funeral in Juchitan, Oaxaca state, Mexico on Sept. 10.
One of the most powerful earthquakes ever recorded in Mexico struck off the country's southern coast, toppling hundreds of buildings and sending panicked people fleeing into the streets in the middle of the night. At least98 people were killed.
A Rohingya man carries an elderly woman, after the wooden boat they were travelling on from Myanmar, crashed into the shore and tipped everyone out on Sept. 12, in Dakhinpara, Bangladesh.
Thousands of Rohingya are continuing to stream across the border, with U.N. officials and others demanding that Myanmar halt what they describe as a campaign of ethnic cleansing that has driven nearly 400,000 Rohingya to flee in the past three weeks.
PHOTOS:Over 123,000 Rohingya Refugees Flee Violence in Myanmar
Rohingya refugees Nasir Ahmed and his mother, cry above Nasir's 40-day-old son who died as a boat capsized in the shore of Shah Porir Dwip while crossing Bangladesh-Myanmar border, in Teknaf, Bangladesh on Sept. 14.
A U.S. flag is unfurled at sunrise at the Pentagon on the 16th anniversary of the Sept. 11th attacks.
Frank Giaccio, 11, mows the grass in the Rose Garden at the White House on Sept. 15, in Washington. Giaccio, from Falls Church, Virginia, wrote a letter to Trump expressing admiration for Trump's business background and offered to mow the White House grass.
Frank was so focused on pushing the lawn mower, he didn't notice Trump had emerged to greet him until the president was next to him in the Rose Garden.
PHOTO: Grassroots Appeal: Kid Mows White House Lawn, Gets Presidential Visit
