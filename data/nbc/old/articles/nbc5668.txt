__HTTP_STATUS_CODE
200
__TITLE
The Week in Pictures: Sept. 22 - 29
__AUTHORS
NBC News
__TIMESTAMP
Sep 29 2017, 9:00 pm ET
__ARTICLE
A man rides his bicycle through a damaged road in Toa Alta, Puerto Rico, on Sept. 24, 2017. Hurricane Maria tore up Puerto Rico a week ago, killing at least 16 people and leaving nearly all 3.4 million people on the island without power and most without water.
Photos: Powerless Puerto Rico Struggles to Recover Post Maria
House Republican Whip Steve Scalise reacts to cheers as he returned to Capitol Hill on Sept. 27 for the first time after being shot in June at a congressional baseball team practice in Alexandria, Virginia.
Flames rise after a powerful explosion at an ammunition depot at a military base in Kalynivka, Ukraine, early on Sept. 27. A huge fire in central Ukraine set off a series of explosions and prompted an evacuation of thousands of people. The prime minister hinted it was possible sabotage by Ukraine's enemies.
A schnauzer who survived the earthquake is pulled out of the rubble of a collapsed building by rescuers in Mexico City on Sept. 24. The dog's rescue gave hope to residents and neighbors of the building who successfully got an injunction from a judge Saturday night requiring the rescue operation continue for at least five more days.
Photos:Desperate Rescuers Dig Through Rubble After Powerful Mexico Quake
Aremy Sanchez Flores walks with her husband Jose Padilla after getting married in an empty lot outside a church that collapsed after an earthquake in Atzala, Mexico on Sept. 23. Twelve people died during a baptism at the church on Sept. 19 where the couple was scheduled to get married. Flores said she was very sad, but that it is time to move forward.
A Balinese man watches the Mount Agung volcano almost covered with clouds as he stands at a temple in Karangasem, Bali, Indonesia on Sept. 26. A week after authorities put Bali's volcano on high alert, tremors that indicate an eruption is coming show no sign of abating, swelling the exodus from the region to at least 140,000 people.
Photos: Residents Flee as Bali Volcano Rumbles
A worker removes paper rolls after filling them with gunpowder mixture to make firecrackers at a factory on the outskirts of Ahmedabad, India on Sept. 27.
Skyscrapers pierce clouds during heavy fog in Dubai, United Arab Emirates on Sept. 27.
Hugh Hefner fans gather at the gate of the Playboy Mansion in Los Angeles on Sept. 27. Hefner, who built Playboy into a multimillion-dollar adult magazine and entertainment empire tied to a Lothario lifestyle of lavish parties and beautiful women, died Wednesday at his home, the iconic Playboy Mansion in Hollywood. He was 91.
Photos: Playboy Magazine Founder, Hugh Hefner, Dies at 91
Ugandan opposition lawmakers fight with plain-clothes security in parliament in Kampala, Uganda on Sept. 27 as they protest a proposed change to the constitution to let long-ruling President Yoweri Museveni run for re-election after age 75.
Surfer dog Derby wipes out in his heat during the annual Surf City Surf Dog event at Huntington Beach, California on Sept. 23.
A woman walks on a road covered in debris from Hurricane Maria in Frederiksted, St. Croix, U.S. Virgin Islands on Sept. 26.
Photos: Hurricane Maria Lashes Puerto Rico, Storm-Battered Caribbean
Chicago Cubs shortstop Addison Russell dives into the crowd but is unable to catch a foul ball hit by St. Louis Cardinals' Jedd Gyorko during the second inning of a baseball game on Sept. 25, in St. Louis.
After diving into the stands chasing a foul ball down the third-base line and spilling a man's tray of chips, Russell emerged from the dugout a few innings later with a plate of nachos and delivered it to the fan. Russell stopped to take a selfie before heading back to play shortstop.
Brooke Fiddes, 5, and her brother Carter Fiddes, 9, both from Charlotte, N.C., laugh as Marine One departs the White House with President Donald Trump as he heads to New York on Sept. 26.
A man stands in a flooded street in the aftermath of Hurricane Maria in San Juan, Puerto Rico on Sept. 25.
Photos: Dark Days and Long Nights Descend on Puerto Rico
An albino Rohingya refugee poses for a picture in Cox's Bazar, Bangladesh on Sept. 27.
Veronica Aguilar Naranjo embraces her 11-year-old daughter Veronica Villanueva as they watch rescuers search for survivors in a collapsed building on Sept. 22, after an earthquake struck Mexico City.
Aguilar was in a supermarket when the quake struck and rushed home to her daughter. At first, she stayed home, but then concluded she had to do something. She took her daughter to a collapsed office building to help, and show the girl the importance of helping however possible in the face of tragedy.
"The first days (after the quake) I didn't leave my house because of fear. But I decided to leave so that my daughter could see what is happening, to make her aware. So that she sees when you can help, you should," Aguilar said. "Among Mexicans, there is a lot of love. When something bad happens, we know that everyone chips in."
Photos:Mexicans Shed Tears for Hundreds Killed in Earthquake
Former Congressman Anthony Weiner enters the federal court for his sentencing hearing in a sexting scandal on Sept. 25 in New York. Weiner was sentenced Monday to 21 months in prison for sexting with a 15-year-old girl in a case that rocked Hillary Clinton's campaign for the White House in the closing days of the race and may have cost her the presidency.
People gather under heavy rain around the bodies of Rohingya refugees after their boat with passengers fleeing from Myanmar capsized off Inani beach near Cox's Bazar, Bangladesh on Sept. 28.
The U.N. migration agency said that more than 60 people are either confirmed dead or missing and presumed dead following the shipwreck.
Photos: Heartbroken Rohingya Refugees Bury Dead After Boat Capsizes
A woman carries a sick Rohingya refugee child through a camp in Cox's Bazar, Bangladesh on Sept. 28.
Photos: Desperate Rohingya Refugees Face Squalor at Crowded Bangladeshi Camp
Birds rest on power lines at dusk on Sept. 24, in Kansas City, Kansas.
Irma Maldanado stands with her parrot Sussury and her dog in what is left of her home on Sept. 27 after it was destroyed by Hurricane Maria in Corozal, Puerto Rico.
Photo:Satellite Photos Show Puerto Rico Left in the Dark
Northern lights (or Aurora borealis) are visible on Norway's Unstad's bay in the Arctic Circle on Sept. 22.
Month in Space Pictures: Cassini's Finale And An Astronaut Returns
An ostrich reacts inside an enclosure at a farm near the town of Chekhov, Russia on Sept. 23.
The Week in Pictures: Sept. 15 - 22
