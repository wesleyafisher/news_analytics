__HTTP_STATUS_CODE
200
__TITLE
ISIS Terror in Syria: American Volunteer Dead Fighting Alongside Anti-ISIS Militia
__AUTHORS
Alexander Smith
__TIMESTAMP
Jul 12 2017, 2:12 pm ET
__ARTICLE
 LONDON  An American fighting as a volunteer alongside a Kurdish militia has been killed in Syria, his partner and mother of his child confirmed to NBC News on Wednesday. 
 Robert Grodt, 28, had traveled to Syria to volunteer alongside a Kurdish militia called the People's Protection Units, or YPG, which is backed by American bombs and weapons. 
 The YPG forms part of the Syrian Defense Forces, a U.S.-backed umbrella group that forms the main fighting force against ISIS in Syria. 
 The exact number of Americans volunteering for local anti-ISIS militias isn't known, but doing so is "strongly discouraged" by the State Department. 
 Grodt, a former Occupy Wall Street activist originally from Santa Cruz, California, said he had gone out to Syria after a friend of his joined the Kurdish militia. 
 "It was last year, a buddy of mine I was really close with came out here, and when I saw him out year I was like, 'pffft! You know, that's it, I gotta go. I gotta go,'" he said in a video released by the YPG after his death. "I talked with my partner and my family and I'm like, 'I'm gonna go out to Syria. I'm gonna go. This is something I care about.'" 
 In a separate video, he said he wanted to "help the Kurdish people in their struggle for autonomy within Syria and elsewhere," and to "do my best to help fight Daesh and create a more secure world." 
 He told his family: "Just know that I love you all, and there's a lot that goes unsaid," and to his daughter: "I'm sorry that I'm not there." 
 Grodt gained some notoriety when he was a volunteer medic in the Occupy Wall Street protests in New York during 2011. 
 He came to the aid of Kaylee Dedrick, a fellow demonstrator who was pepper-sprayed by police in a video that was shared widely on the internet. The pair ended up in a relationship and later had a baby together. 
 "He was always someone who, when he set his heart to it, there was no changing his mind," Dedrick told NBC News. "It was sad, but I was, and still am proud of him." 
 Grodt and another American, Nicholas Warden, were killed over two days last week, the YPG said, although NBC News has not independently confirmed Warden's death. 
 Warden also traveled to Syria to fight ISIS after seeing the attacks the group was inspiring across the United States and the West. 
 The 29-year-old father of one was killed last week while fighting ISIS in its de facto Syrian capital of Raqqa, YPG announced Monday. His father, Mark Warden, told the Buffalo Times that he had received confirmation of his son's death after getting conflicting reports. 
 "I'm not in a good way right now," he told the newspaper. 
 Although the Buffalo, New York, native was a veteran of the U.S. Army, he went out in February to volunteer alongside a Kurdish militia called the People's Protection Units, or YPG, which is backed by American bombs and weapons. 
 Last year, three Americans  Levi Shirley, Jordan MacTaggart, and William Savage  were killed volunteering for the YPG. 
 Warden joined the YPG in February and took the nom de guerrer "Rody Deysie." The group said he was a "real soldier," as well as being "disciplined," "modest" and "always the first to volunteer for duties." 
 He said in a video released by the YPG after his death that he joined "to fight ISIS because of the terrorist attacks they were doing in Orlando, in San Bernardino, in Nice, [France], in Paris." 
 He added that he "would like to say 'hi' to my parents in New York." 
 Warden served two tours of Afghanistan and reached the rank of sergeant during nearly five years with the 101st Airborne Division between 2007 2011, according to the Military Times. 
