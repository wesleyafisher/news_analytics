__HTTP_STATUS_CODE
200
__TITLE
Will Flood of Weinstein Accusers Bring Sweeping Change to Hollywood?
__AUTHORS
Claire Atkinson
__TIMESTAMP
Oct 15 2017, 6:14 am ET
__ARTICLE
 Rose Marie, the actress best known for playing Sally Rogers on The Dick Van Dyke Show, learned how Hollywood really works back in her first major film role as an adult in the 1950s. She was cast in the Phil Silvers comedy Top Banana when a producer made sexual advances against her. 
 I rejected him with great flair, and he cut all of my musical numbers out of the film, she told NBC News. 
 Now, learning the details of the Harvey Weinstein sexual harassment scandal, she feels the same anger again. Its sickening, its disgusting, said Rose Marie, 94. But its also different, she said: In contrast to the silence of her day, women are now speaking up against sexual coercion and taking action. 
 I think things are changing, she said. Just look at the news stories right now . We can't change what men are or are not going to do, but when women start believing that we are just as powerful as men, and they act that way, theres nothing we can't do. 
 The flood of accusations by actresses of assault and harassment against Weinstein, one of the most powerful producers in Hollywood, is causing a tectonic shift in attitudes as more women band together to call out powerful, badly-behaved men. The movement appears to be gaining an unstoppable momentum of its own, and no one knows where it will stop. 
 Its barely been a week and the volume of it happened to me, too statements is breathtaking, as is the speed at which this is all happening. Gwyneth Paltrow, Angelina Jolie, La Seydoux, Tara Subkoff, Kate Beckinsale, and Cara Delevingne are among more than 30 women who have revealed their own harrowing experiences with Weinstein, after actresses Ashley Judd and Rose McGowan made their initial allegations in a New York Times article just a week ago. 
 Weinstein was expelled late Saturday from the ranks of the Academy of Motion Picture Arts and Sciences in a sow of solidarity by Hollywood against a onetime titan of the industry. 
 On Thursday evening, in a case unrelated to Weinstein, the chief of Amazon Studios, Roy Price, was suspended after a female producer of an Amazon show, Isa Hackett, revealed verbally abusive comments he made to her, according to The Hollywood Reporter. 
 We all know its been going on forever but it seems like things are changing really quickly, said Jessica Neuwirth, the president of the Equal Rights Amendment Coalition, a group lobbying for civil rights protections for women. Somewhere we are breaking this glass ceiling of justice. 
 Neuwirths description of a long-standing film industry problem is borne out by the personal accounts of actresses like Rose Marie and Tippi Hedren, who accused Alfred Hitchcock of sexual harassment in her 2016 memoir, Tippi. 
 Hedren, 87, who appeared in the Hitchcock movies Marni and The Birds, told NBC News that she always said no to the director, and refused to give in no matter the consequences. 
 Related: After Weinstein Allegations, Rose McGowan Emerges as Major Voice 
 Hitchcock said, Ill ruin your career, and I said, do what you have to do and he walked out of the door speechless, she said. Hedren said if more attention is brought to harassment and people of note were taken to task, maybe it might make a difference. She added: It doesnt just happen in the movies. 
 Other figures in the entertainment and media world who have been seriously impacted by allegations of harassing or assaulting women include the actor Bill Cosby, Bill OReilly and the late Roger Ailes of Fox News, and Antonio L.A. Reid of Epic Records. 
 The condemnations, too, are growing louder, whether from politicians, including Hillary Clinton, to business leaders like Facebook chief operating officer Sheryl Sandberg, to womens rights crusader and actress Meryl Streep. 
 And this time, many men are speaking up: former Vice President Joe Biden, TV chef Anthony Bourdain; Sen. Cory Booker, D-N.J., and the actor Mark Ruffalo. Corporate executives who have spoken up include Disneys chief Bob Iger, its former chief, Michael Eisner, and the movie mogul Jeffrey Katzenberg to name a few. Even Weinsteins brother, Bob, called him a world class liar. 
 Jeremy Zimmer, the chief executive of the talent agency UTA sent a memo to his staff on Thursday telling them how to report harassment. You will be heard, he assured them. 
 There is a widespread awakening all over the country, said Nita Chaudhary, the co-founder of UltraViolet, an advocacy group that fights sexism and attacks on women. I hope this is a reckoning moment that were not going to stand for it anymore. Chaudhary says the election of President Donald Trump, who boasted about groping women, was also a turning point, leading hundreds of thousands of women to protest in a march after Trumps inauguration. 
 Even the film director Oliver Stone seems to have changed his mind on the subject. After the initial article came out about Weinstein, Stone told the Hollywood Reporter that Weinstein shouldnt be condemned by a vigilante system, and urged public opinion to wait for a trial. 
 Its not easy what hes going through either, Stone said, drawing widespread ridicule. 
 Model and actress Carrie Stevens told The New York Daily News Thursday that Stone had grabbed her breast at a party more than two days ago. 
 On the same day, Stone backtracked on his comments, saying he had finally seen the widespread allegations against Weinstein. He said he would drop out of a Showtime television project he was working on with the Weinstein Co. 
 Im appalled and commend the courage of the women whove stepped forward to report sexual abuse or rape, Stone said. 
