__HTTP_STATUS_CODE
200
__TITLE
Will Human Beings Ever Reach Earth 2.0? 
__AUTHORS
Keith Wagstaff
__TIMESTAMP
Nov 15 2016, 4:15 pm ET
__ARTICLE
 Kepler-452b is the most Earth-like planet ever discovered, a place with just enough sunlight to possibly support the crops and house plants of life forms like ourselves. 
 But don't pack your bags just yet. While the planet might seem like a tantalizing target for NASA's next mission, it's extremely unlikely that human beings will ever set foot on Kepler-452b, thanks to the 1,400 light-years they would have to travel to get there. 
 The separate discovery of the closest confirmed rocky exoplanet to Earth, HD 219134b, was announced by NASA on Thursday  that one's only 21 light-years away. 
 So what makes Kepler-452b so great? NASA called it "Earth 2.0" for a reason. It's a "Goldilocks planet," meaning it sits in the habitable zone of its star, where the temperatures are not too hot or cold for liquid water to form. 
 HD 219134b, on the other hand, is a broiler of a planet, way too close to its sun for water or life to ever develop. 
 Scientists still need more data, but there is a strong possibility that Kepler-452b has a rocky surface and a thick atmosphere. It might not be Risa, the tropical vacation planet from "Star Trek: The Next Generation," but it's the best candidate NASA has found so far for another planet that might support life. 
 Even in science fiction, that is not a quick journey. If Captain Jean-Luc Picard wanted to travel from Earth to Kepler-452b, it would take the USS Enterprise more than 16 months traveling at warp 8 to reach its destination. 
 Related: Kepler-452b: NASA Mission Discovers 'Older, Bigger Cousin' to Earth 
 That is for a ship that can go faster than the speed of light  which, as far as we know, is impossible. Sticking to existing technology, a trip to Kepler-452b might take so long human beings could evolve into a different species before the spacecraft completed its mission. 
 NASA's New Horizons probe  which recently sent back amazing photos of Pluto  would take around 20,000 years to travel one light-year, according to Jeffrey Bennett, astronomer and author of "What Is Relativity?: An Intuitive Introduction to Einstein's Ideas, and Why They Matter." 
 At that pace, it would take 28 million years to reach Kepler-452b, Bennett told NBC News. Lucy, also known as Australopithecus afarensis, lived approximately 3.2 million years ago. It's not hard to imagine human beings would look completely different by the time a craft launched today reached its remote destination. 
 More advanced technology could shorten the trip, but even with advanced engines, reaching Kepler-452b seems like an impossible goal. 
 The electrically-powered ion engine on NASA's Dawn orbiter is way more efficient than the chemical thrusters used by past spacecraft. 
 The agency is looking to improve on that ion engine design with its NASA Evolutionary Xenon Thruster (NEXT), which could propel future spacecraft as fast as 90,000 miles per hour. (Dawn is currently capable of going 9,600 miles per hour.) 
 That might sound fast. But by Bennett's calculations, it would still take 10.5 million years to make the trip to Kepler-452b. 
 What if we developed something even more advanced? In 1958, physicist Freeman Dyson came up with the idea for Project Orion, which eventually became the inspiration for the SyFy show "Ascension." It would use pulsed nuclear explosions to move a giant ship forward at around 5 percent the speed of light. 
 According to Bennett, a spaceship going that fast would take 28,000 years to reach Kepler-452b from Earth. An improvement, sure, but it would be hard to convince people to go on a journey that would last longer than the history of human civilization. 
 To make the journey even a remote possibility, we would need something like an antimatter engine. 
 Antimatter sounds like science fiction  in fact, the ships in "Star Trek" rely on antimatter engines but the stuff does exist. It's matter with the electrical charge reversed. An antiproton, for example, is a proton that has a negative charge instead of a positive one. 
 When antimatter meets matter, it creates a massive amount of energy, something that theoretically could power a rocket engine (or in a Dan Brown novel, a bomb). 
 According to NASA, it would take $100 billion to create one milligram of antimatter in a particle accelerator. 
 Creating a spacecraft engine with the stuff would require finding a way to make tons of it, then harnessing the intense energy from a matter/antimatter reaction. If human beings figured that out, some scientists believe an antimatter engine could propel a spacecraft forward at 70 percent the speed of light. 
 In that case, it would take a 2,000 years to reach Kepler-452b. 
 "That is a long time," Charles Liu, an associate in astrophysics at the American Museum of Natural History, told NBC News. "Two thousand years ago, the Roman Empire was around." 
 To reach "Earth 2.0," humans would probably have to rely on a multi-generational ship loaded with more than 100 people to maintain genetic diversity, and enough power to take care of them and future generations, Liu said. 
 The people on the ship would have to be shielded from intense solar radiation and stay clear of supernovas, which can emit the equivalent of 10 billion years of sunlight in a single blast, according to Liu. 
 The spacecraft would also have to endure the impact of interstellar dust, which would wear its hull down over the course of 2,000 years. 
 "It's only a matter of time before that would waste any kind of shielding," Liu said. 
 One bonus of traveling really, really fast: time moves much slower for the person moving than for those who are left behind. 
 At 70 percent the speed of light, only 1,428 years would go by on the spaceship, compared to 2,000 years on Earth. 
 Going at the much more impossible speed of 99.99999999 percent the speed of light, a spaceship would get to Kepler-452b in a little more than 1,400 years as experienced on Earth, but its passengers would barely have aged a month. That would make space travel pretty convenient ... if it were possible. 
 But what about the "Interstellar" solution? Can't we hurl Matthew McConaughey through a wormhole and hope for the best? 
 Yeah ... but that is almost certainly not going to happen. As astronomer Sten Odenwald told NBC News, wormholes, if they exist, could probably only be formed through something like the Big Bang or the implosion of a star. 
 Ultimately, while traveling to Kepler-452b would be an amazing accomplishment, human beings might be better off colonizing a nearby planet. Besides, who wants to spend their life on a spaceship when they could be eating home-cooked meals and watching Netflix on Mars? 
