__HTTP_STATUS_CODE
200
__TITLE
Apple Stock Soars to Record High After Earnings, iPhone Excitement
__AUTHORS
Alyssa Newcomb
__TIMESTAMP
Aug 1 2017, 7:20 pm ET
__ARTICLE
 The world's most valuable company just keeps getting richer. 
 Apple posted quarterly revenue of $45.4 billion on Tuesday, beating a Thomson Reuters consensus estimate of $44.89 billion. The company's stock jumped more than 6 percent after hours and was trading at a record high of $159.20. 
 The tech giant sold 41 million iPhones in the last quarter  a healthy showing considering the Cupertino-based company is expected to release a new iPhone in September. Earlier this year, CEO Tim Cook attributed the slowdown in iPhone sales to frequent rumors about what Apple loyalists can expect from the next device. 
 Related: Could Apples Next Move Finally Kill the Need to Carry Cash? 
 Sales of iPads also exceeded expectations, marking the first period of growth for the tablet in more than three years. Last quarter, Apple sold 11.42 million iPads. 
 The all-encompassing services  including Apple Music, iTunes, AppleCare, and the App Store, among others  remained the star of the show with a 22 percent increase in revenue from this time last year. 
 As for those three "big" and "beautiful" plants President Donald Trump told the Wall Street Journal Apple allegedly said they would build, Cook dodged an analyst's question during a call on Tuesday. But he reiterated Apple's commitment to job creation. 
 The earnings report comes ahead of what will be a pivotal quarter for Apple as the company reportedly gears up to launch a new iPhone. Apple has been following a September launch cycle, but this one will be particularly special, marking the 10th anniversary of the flagship device. 
 Apple, of course isn't, commenting. But revenue projections of $49 billion to $52 billion for the next quarter indicate Apple may have something big in store. 
 While Apple is remaining characteristically tight-lipped about what the next iPhone will look like, it's expected the new phones will have edge-to-edge displays, competing with the sleek recent releases from Samsung. 
 Developers have been busy sifting through code  particularly in the recently released firmware for the Home Pod, Apple's smart speaker that goes on sale at the end of this year. 
 In the code, developers they say they've found hints about what we can expect from the iPhone 8. Steve Troughton-Smith reported on Twitter that the new iPhone will have facial recognition technology. The finding was also confirmed by iOS developer Guilherme Rambo on Twitter. 
I can confirm reports that HomePods firmware reveals the existence of upcoming iPhones infra-red face unlock in BiometricKit and elsewhere pic.twitter.com/yLsgCx7OTZ
 While Apple may have made a rare misstep in releasing the clues in the Home Pod firmware, it's also worth noting that nothing has yet been confirmed by the company. For that, we're likely going to have to wait until September. 
