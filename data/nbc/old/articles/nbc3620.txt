__HTTP_STATUS_CODE
200
__TITLE
Nashville Mayor Reveals Son Overdosed on Opioids and Other Drugs
__AUTHORS
Gabe Gutierrez
__TIMESTAMP
Aug 9 2017, 6:09 pm ET
__ARTICLE
 NASHVILLE, Tenn.  Mayor Megan Barry heard a knock at her door at 3 a.m. on Sunday, July 30. 
 Your first thought is that you've had a police officer who's been injured, she said, and you need to get dressed, you need to go to the hospital, and you need to comfort the family." 
 Instead, she was the one who would need comforting. The officers at the door told her it was her son who had just died of a suspected overdose in Denver hours earlier. 
 Barry told NBC News Wednesday her son had a combination of drugs in his system when he died of an overdose 10 days ago. 
 She said the drugs included Xanax, cocaine, and the opioids methadone and hydromorphone. The autopsy report showed the cause of death as combined drug intoxication. 
 "I didn't see it coming," Barry said  even though Max Barry, 22, had been to rehab once before in Florida for abusing Xanax. Hed recently graduated from the University of Puget Sound and moved to Colorado. 
 "He was our only child, she said. "I want to just shake him and say, 'What were you thinking?'" 
 At an emotional memorial service last week, the mayor's husband, Bruce, remembered his son. 
 "We've all made incredible mistakes that we could almost always walk away from, he said. He made one he couldn't walk away from." 
 Barry returned to work for the first time on Monday, and she has decided to take her story public. 
 It's definitely part of my new normal, she said. To bring awareness to this because I don't want this to happen to anybody else. 
 In Nashville alone last year, there were 245 deadly overdoses involving opioids, the mayors office said. Thats a 120 percent jump from the year before. 
 On Tuesday, President Trump stopped short of declaring a national emergency  but pledged to ramp up law enforcement to combat the crisis. 
 "We're not going to arrest our way out of this problem, Barry said. You need having access to beds and treatment. Treating this like a disease by declaring a federal emergency  we [would] get those resources that we need." 
 Barry said her sons death still hadnt sunk in. 
 This hole that I have in my heart will never be filled, she said. And I reach for my phone because like most 22-year-olds, we texted a lot." 
 She shared their final texts just hours before her sons death. 
 "I'm so grateful that the last words we said were: I love you and I love you too, Mom." 
