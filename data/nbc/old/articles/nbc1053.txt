__HTTP_STATUS_CODE
200
__TITLE
Philando Castile Fund Pays Off Student Lunch Debts in Minnesota
__AUTHORS
Foluke  Tuakli
__TIMESTAMP
Oct 17 2017, 6:07 pm ET
__ARTICLE
 A memorial fund set up in honor of Philando Castile has raised enough money to pay off lunch debts at schools across St. Paul, Minnesota for one year. 
 Philando Feeds the Children, an online crowdfunding venture started by Metropolitan State University psychology professor Pam Fergus, had raised more than $75,000 on Tuesday afternoon  $70,000 more than the original goal of $5,000. Because of the momentum of the campaign, organizers have increased their goal to $100,000 in order to maximize their lunch debt relief efforts for students across Minnesota. 
 "This fund really speaks to exactly who Philando Castile was as a passionate school nutrition leader," Stacy Koppen, director of the district's nutrition services told NBC News.  
 Castile, whose fatal shooting by police officer Jeronimo Yanez in 2016 ignited national discussion after videos of the shooting and its aftermath went viral, worked as a nutrition services supervisor at J.J. Hill Montessori School. 
 Affectionately known as Mr. Phil to the students, Castile would often financially assist students who were not able to purchase lunches with his own money. This action inspired Fergus to create The Philando Feeds The Children Fund in memory of his service. 
 A former school employee and a former student of Saint Paul Public Schools, Castile was known for maintaining great relationships with staff and students alike according to a statement from Saint Paul Public Schools. 
 On average, school meals cost $400 a year for one students lunch according to Koppen. She shared thatfamilies in the St. Paul Public School system who fall outside the qualification level for free and reduced lunch most definitely need the kind of intervention support this fund provides. 
 Related: Mississippi School Board Pulls To Kill a Mockingbird From Reading List 
 Children require that nutrition to focus their attention on their academics. We know that students who receive these meals are able to pay attention and perform better on exams. Koppen said. 
 St. Pauls School provides breakfast, lunch, snacks and summer programs. Some students eat all meals with them each day. She added that, For the academic setting, nutrition and school meals are just as important as textbooks. 
 This project means the world to me. Valerie Castile, Philando's mother, told Star Tribune. She presented the first check from the venture to the school where Philando last worked, J.J. Montessori, on Friday afternoon. 
 St. Paul Public Schools and Fergus will continue to work together to identify students and get money in their accounts in the hopes of supporting their most optimal life outcomes. 
 Follow NBCBLK on Facebook, Twitter and Instagram 
