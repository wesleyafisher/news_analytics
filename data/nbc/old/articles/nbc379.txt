__HTTP_STATUS_CODE
200
__TITLE
The Experience of Moving Often Helped Rory Crofton Choose Brandeis
__AUTHORS
Amy DiLuna
__TIMESTAMP
May 12 2017, 10:47 am ET
__ARTICLE
 As part of an as-told-to essay for College Game Plan's How I Got In series, Brandeis University sophomore Rory Crofton, 19 , reflects on his college journey. 
 Victoria, Canada 
 History-loving world traveler who finds joy in volunteering. 
 My parents are international school teachers, so weve moved around a fair amount. I was living in Bosnia when I applied for university. 
 It was a bit of a different application process for me, just because I didnt get to go see any of the schools, or talk to anybody. So there was that added layer of uncertainty about where I was applying. 
 Im a double major in history and American Studies. I was definitely focused on schools that had good departments in terms my interests, but also had an emphasis on extracurricular activities. Being super close to Boston, arguably one of the most historically significant cities in the U.S. was a fantastic bonus, but finding schools that emphasized volunteerism as well was a big priority for me. 
 I applied using the common app, and my personal statement was about growing up and hopping around from place to place, with my changing perceptions of what home is to me. My hometown is on the west coast of Canada, but when I go back home, I feel almost as much like a tourist as I do a native, because I havent spent as much time there. So I wrote about having a sense of home as something that changes depending on where you are. 
 One of my high schools offered AP courses, so I made sure to take those, and at another school I took an extra year to do the International Baccalaureate program. I challenged myself academically, but I also spent my time doing community service. 
 I did a lot of volunteer work with the national parks service, giving tours, working at the visitors center, and also at lots of other museums and historic sites. I really enjoy history, so I was able to combine my academic interest with volunteer work. 
 Having had this experience of moving around so much, I was used to being told, this is where were living; this is where were going; you're going to go school here. 
 So when it came to college, I was at a loss at what to do with all this freedom of choice. My parents sat down with me and helped me go through some places, trying to find ones that stood out, and [Brandeis] seemed like a good fit. 
 On applications, try to present your true self. Dont think that admissions people reading your application are necessarily looking for a particular answer. Theyre looking at the bigger picture. Sell yourself, of course, but dont write things just to try to impress them. 
 When youre picking out schools, dont be afraid to go out of your comfort zone in terms of finding a school that might be a little bit further away from home. Look for a school that might challenge you in ways that you might not have known. 
