__HTTP_STATUS_CODE
200
__TITLE
Samsungs Galaxy Note 8 Ready to Battle Apple, Google
__AUTHORS
Alyssa Newcomb
__TIMESTAMP
Aug 23 2017, 8:07 pm ET
__ARTICLE
 Samsung debuted its long-awaited Galaxy Note 8 smartphone on Wednesday, showing off a 6.3-inch screen with an edge-to-edge display, packing more real estate into a smaller form factor. While the screen may be big, the phone is designed to make it still easy to hold in your hands. 
 It's the first salvo in this summer's smartphone war between Samsung, Apple, and Google as the three technology giants get ready to go after the estimated 50 million Americans currently eligible for an upgrade to the latest and greatest devices. 
 The smartphone comes with its famous S-pen, a stylus that has twice the pressure sensors of the Galaxy Note 5. Samsung executives compared the S-pen to writing with a 0.7 mm ballpoint pen. The tool also comes with a new translate feature: Hover the pen over text and it can translate 71 different languages and quickly convert foreign units and currencies. 
 The release comes one year after the Galaxy Note 7 was blamed for dozens of fire-released incidents. After a botched recall, Samsung ultimately pulled the ill-fated phone from the market and created new safety guidelines. 
 Related: Samsung Finally Explains the Galaxy Note 7 Exploding Battery Mess 
 While Samsung released a new Galaxy S8 and S8+ earlier this year, the latest release of the Note line of smartphones marks a turning point for the company. During a briefing with NBC News earlier this month, Samsung executives said there was a discussion of "Do we want to go down the Note path after everything that happened?" 
 However, executives said an extremely loyal Note user base  and confidence in their eight-point battery safety check process  made it clear that they should continue with the Note line. 
 But Apple and Google are determined to capture that interest for themselves. 
 "This is going to be one of the biggest years of the battle that we have ever seen," Patrick Moorhead, principal analyst at Moor Insights & Strategy, told NBC News earlier this month. 
 Apple has been following a September launch cycle, but this one will be particularly special, marking the 10th anniversary of the iPhone. Apple, of course, isn't commenting. But revenue projections of $49 billion to $52 billion for the next quarter indicate Apple may have something big in store. 
 It's expected Apple's newest generation of iPhones will have edge-to-edge displays worthy of rivaling Samsung. 
 Developers have been busy sifting through code  particularly in the recently released firmware for the Home Pod, Apple's smart speaker that goes on sale at the end of this year. 
 In the code, developers found hints about what we can expect from the iPhone 8. Steve Troughton-Smith reported on Twitter that the new iPhone will have facial recognition technology. The finding was also confirmed by iOS developer Guilherme Rambo on Twitter. 
I can confirm reports that HomePods firmware reveals the existence of upcoming iPhones infra-red face unlock in BiometricKit and elsewhere pic.twitter.com/yLsgCx7OTZ
 But it could extend beyond just using biometrics to unlock your phone. 
 There's also been some speculation the new iPhone could potentially be able to read your emotions. Last year, Apple acquired Emotient, a company that uses artificial intelligence to read facial expressions. 
 It's unclear what purpose the emotion tech could have  but it could tap in to an entire new functionality for the pocket-sized devices so many of us rely on in our day-to-day routines. 
 Also expect Google to be back with a second generation of its Pixel smartphones later this year. The first generation Pixel phones won rave reviews for their cameras, but demand far outpaced supply, leaving plenty of potential eager customers without a chance to buy one of the premium phones. 
 Google hasn't confirmed whether  or when  new Pixel phones could be arriving; however, earlier this year, Rick Osterloh, senior vice president of hardware at Google, told AndroidPIT that a Pixel 2 is definitely in the works. 
 "There is an annual rhythm in the industry. So, you can count on us to follow it," he said. "You can count on a successor this year, even if you don't hear a date from me now." 
 If Google sticks with that "annual rhythm," the world should be meeting the next generation of Pixel phones sometime in October  one year after the devices were first released. 
 The bottom line: Whether you're an iPhone loyalist or looking to try a new Android phone, it's going to be a busy few months for new releases. The question is  which one will you choose? 
 
