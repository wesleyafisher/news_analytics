__HTTP_STATUS_CODE
200
__TITLE
Health Disparities Narrowed for Blacks, Latinos Under Obamacare, Study Shows
__AUTHORS
Chandelis R. Duster 
__TIMESTAMP
Aug 24 2017, 7:41 pm ET
__ARTICLE
 Health care disparities among blacks and Latinos compared to whites have narrowed because of the Affordable Care Act, also known as Obamacare, according to a study published by the The Commonwealth Fund Thursday. 
 The report found that the number of blacks and Latinos without health care coverage dropped during the first two years of the ACA's coverage expansion. 
 From 2013 and 2015, the uninsured rate among blacks between ages 19-64 dropped 9 percent, and dropped 12 percent among uninsured Latinos ages 19-64, the study showed. The rate of uninsured whites dropped 5 percent. The disparity among uninsured blacks and whites also narrowed by 4 percent and among Latinos and whites narrowed 7 percent. 
 Dr. Pamela Riley, vice president of The Commonwealth Funds Delivery System Reform and a coauthor of the report, said although the study shows progress in health coverage for everyone, blacks and Latinos are still more likely than whites to not get the medical care they need. 
 If we are going to reduce these disparities, we must continue to focus on policies like expanding eligibility for Medicaid that will address our health care system's historic inequities, Riley said in a statement. 
 Related: With Obamacare, More California Latinos Sought Care for Hypertension 
 The analysis also found the number of uninsured Latino adults dropped 14 percent in states that expanded Medicaid coverage compared to 11 percent in states that did not. The number of uninsured black adults meanwhile fell 9 percent in states both with and without Medicaid expansion. 
 And because of the decline in the number of uninsured, the number of adults ages 18 and older who reported skipping health care when they needed it because of high costs also declined. 
 The number of black adults who reported not going to the doctor because of costs dropped from 21 percent to 17 percent and for Latino adults it dropped from 27 percent to 22 percent. 
 The number of adults without a usual source of health care also dropped. By 2015, the disparity between black adults and white adults without a usual source of health care narrowed from 8 percent to 5 percents. It narrowed even more for Latinos compared to whites  24 percent to 21 percent. 
 After Senate Republicans failed to "repeal and replace" the current health care law, uncertainty looms around Obamacare's future once Congress returns to Washington from recess. The Commonwealth Fund's President Dr. David Blumenthal said improving the ACA will continue to help minorities get access to health care. 
 Improving upon the Affordable Care Act, and expanding Medicaid in all states, will be critical if we are going to see disparities continue to shrink and ensure that everyone can get affordable, high-quality health care," he said. 
 Follow NBCBLK on Facebook, Twitter & Instagram  
