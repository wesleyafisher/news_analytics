__HTTP_STATUS_CODE
200
__TITLE
Trump Adviser, Son Post Image of Trumps Deplorables Featuring White Nationalist Symbol
__AUTHORS
Ali Vitali
__TIMESTAMP
Sep 11 2016, 7:37 pm ET
__ARTICLE
 A white nationalist symbol has made its way into the latest back and forth in the 2016 presidential campaign. 
 Amid the flurry of statements about Hillary Clinton calling "half" of Donald Trump supporters a basket of deplorables,  a reference to some of the Republican nominee's supporters who ascribe to views popular among the white nationalist-linked alt-right movement  informal Trump adviser and confidante Roger Stone tweeted a picture of the poster from the movie The Expendables altered as "The Deplorables." Donald Trump, Jr., one of Trump's sons, posted the same image on Instagram. The origin of the image is unclear. 
 The Photoshopped faces in the picture include Trump, running mate Gov. Mike Pence, Gov. Chris Christie, former New York Mayor Rudy Giuliani, Dr. Ben Carson, both of Trumps eldest sons, conspiracy theorist Alex Jones, alt-right icon Milo Yiannopoulos, and Stone himself. 
 Prominently featured over Trumps right shoulder: popular white nationalist symbol, Pepe the Frog. 
 "Pepe the Frog is a huge favorite white supremacist meme, Heidi Beirich of the Southern Poverty Law Center told NBC News of the meme. 
 While Pepe the Frog may not be a household name, the meme is known to members of the alt-right on the internet. 
 Its constantly used in those circles, Beirich said. "The white nationalists are gonna love this because theyre gonna feel like 'yeah were in there with Trump, theres Pepe the Frog.'" 
 Pepe the Frog, a cartoon amphibian, was popularized on the website 4chan, and became associated with the neo-Nazi movement. 
 The Trump campaign has been repeatedly accused of dog whistles to white supremacists and the alt-right, though his original position on support from these groups was ambiguous. When confronted with the support of prominent white nationalist and former KKK Grand Wizard David Duke in February, Trump stumbled in his initial disavowal of the man  telling CNN at the time, "I don't know David Duke. I don't believe I have ever met him. I'm pretty sure I didn't meet him. And I just don't know anything about him." 
 He later clarified that he disavowed Dukes support, though the former Klansman  now running for Congress in Louisiana  has continued to tweet messages of support for the Republican nominee. 
 Over the course of this campaign, Trump has retweeted Twitter accounts with names such as WhiteNationalistTM and blasted out anti-Semitic images to his over 11 million followers on the social media site. Some members of his campaign have been tied to the alt-right, including Breitbart's Steve Bannon, who is now CEO of the Trump campaign. Democratic nominee Hillary Clinton gave a speech shortly after Bannon's appointment linking Trump's campaign to the nationalistic movement and calling on the rest of the GOP to reject extremist views. Clinton has continued to argue that Trump has "given voice" those who engage in "offensive, hateful, mean-spirited rhetoric." 
 Related: Presidential Campaign Brings the Alt-Right Out of the Shadows 
 Stone, for his part, is known for his controversial tweets that usually defend Trump, warn of a rigged election, and lashing out at Clinton. For months he has repeatedly advertised Clinton Rape t-shirts on his account and pushed hard on the Trump-proposed narrative that the election could be rigged against the Republican nominee. 
 Stone is no longer with the campaign in an official capacity, after parting ways with Trump in August of last year. Despite that, he remains a self-described FOT: Friend of Trump who was most recently invited to attend the campaigns event announcing Gov. Mike Pence as Trumps running mate. 
 Trump spokeswoman Hope Hicks tells NBC News that "Don Jr., like Mr. Trump, disavows any groups or symbols associated with a message of hate." 
 Stone could not be reached for comment on this article or the images origination. In his tweet, Stone said that he was "proud" to be among "The Deplorables" in the image, while Trump, Jr. wrote that he was "honored to be grouped with the hard working men and women of this great nation that have supported" his father. 
