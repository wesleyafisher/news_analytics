__HTTP_STATUS_CODE
200
__TITLE
Zika Took Her Baby. She Doesnt Want It to Happen to You
__AUTHORS
Maggie Fox
__TIMESTAMP
Aug 21 2016, 11:06 am ET
__ARTICLE
 A long Thanksgiving weekend seemed to Satu and her husband like a good time to go on vacation to Latin America. She was 11 weeks pregnant and feeling well, and the beach seemed like just the ticket. 
 She never saw the mosquito that bit her and gave her Zika. And she had no way of knowing it would cost her a longed-for first pregnancy. 
 I had never heard of Zika. There was no reason to suspect Zika, says Satu, a 33-year-old Finn living in Washington, D.C. 
 While grateful she got the best care possible, Satu worries that people are not taking the threat seriously enough. 
 Zika has now caused at least two outbreaks in Florida, bringing the virus to a new and often unsuspecting population of women. 
 There are people who are not aware of the risks related to Zika, says Satu, who doesnt want to use her real name to protect her privacy. Shes sharing her story in the hope of sparing other women the trauma she went through. 
 If even one person avoids getting infected with Zika while pregnant, thats good for me. 
 Satu was infected in November, probably in Guatemala  months before anyone suspected Zika was even circulating in that country and weeks before world health officials had fully accepted that Zika could cause birth defects. 
 If I had known then, I would have protected myself better, says Satu, who is cheerful, physically fit, and clearly determined to learn as much as possible about Zika and its effects on the rest of the world, as well as on her personally. 
 Related: Zika Virus Birth Defects May be Tip of the Iceberg 
 She became a case study for doctors, donating her own medical tests and her fetus to science and helping doctors learn and understand how the virus can infect and damage an unborn child. Hers was one of the cases that persuaded the World Health Organization and the Centers for Disease Control and Prevention that Zika was indeed causing horrendous birth defects  something never before seen in a mosquito-borne virus. 
 Zika first hit Brazil hard early in 2015 but it took several months for authorities to understand and accept that the seemingly harmless virus was exploding through the population and causing a smaller, matching explosion of babies born with a rare birth defect called microcephaly. The first CDC warning didnt go out until January. 
 That was too late for Satu, who felt ill almost as soon as she and her husband got back from their dreamy beach and jungle vacation in Mexico, Guatemala and Belize. 
 I thought I had caught the flu or a cold on the airplane. I was just a bit more tired than usual, she recounted. Then a rash erupted, first on her chest, then her face and arms. It didnt itch or hurt. 
 It just looked weird. What the heck is this? Satu said. 
 Being pregnant, she went straight to her doctor. Then the doctor told me it was nothing to worry about, she said. 
 Related: Here's What Zika Virus Infection Looks Like 
 Her husband developed symptoms the next day, and tested negative for malaria. 
 They both got better, and if she hadnt been so curious and insistent, Satu may never have suspected anything was wrong until well into the pregnancy. The severe birth defects caused by Zika usually cannot be seen by ultrasound until the end of the second trimester. 
 On a visit home to Finland for Christmas, Satu tested positive for Zika infection. 
 You always remain hopeful, Satu said. Even when I found out my symptoms matched the Zika disease, I wanted to find out the worse-case scenario. Of course it was dreadful, but even in the midst of the horror you think there is a small chance it will be OK. 
 Related: New Map Finds 2 Billion at Risk From Zika 
 At first it did seem OK. Ultrasounds showed the fetus was developing normally. 
 Yet blood tests showed Satu continued to have evidence of Zika in her blood. Doctors have now found thats common when pregnant women get Zika, and think perhaps the infected fetus is sending the virus back into the mothers blood, or that some people have a kind of predisposition to prolonged infection during pregnancy. 
 It was late January before the first ultrasound showed some evidence of brain damage. The corpus callosum in the fetus  the structure that connects the two sides of the brain  had not developed. 
 But ultrasounds can be fuzzy and hard to read. 
 Satu was referred to Childrens National Health System in Washington, which has a magnetic resonance imaging (MRI) machine that can image a developing fetus. 
 In the MRI, they really saw clearly all the structures of the brain, Satu said. That was helpful. They could show us the picture and it somehow made it very concrete. 
 It looked bad, says Dr. Roberta DeBiasi, who helped treat Satu at Childrens. 
 As the brain develops, there are different layers that develop while the baby is in the womb, DeBiasi told NBC News. These were basically absent. There were some zones that were just completely undetectable. 
 That suggested the virus had destroyed the developing tissue, and once damaged in this way, it doesnt grow back. 
 This particular MRI was so deficient in brain tissue that most people felt this child was extremely unlikely to make it through the pregnancy and unlikely to make it after delivery, DeBiasi said. 
 These were not possible or slight findings. They were severe. 
 If the fetus survived to be born, there would be profound damage. The child would never walk or talk and would need constant, 24/7 care through its life, which would be a short one, Satu said. There was a risk of constant epileptic seizures. That was a dire prognosis. 
 There was little question that Zika was the cause. 
 Related: Zika Damages Pregnancies At All Stages 
 The Finnish folks had done serial testing of her blood up until the point she got to us and, lo and behold, she had positive tests not only when she first came to them but four weeks later and six weeks later and when she came to see us she was still positive, DeBiasi said. 
 It wasnt until her pregnancy ended that her own bloodstream level went negative. 
 Satu and her husband had discussed the possibilities. I wouldnt say we struggled, but it wasnt like snapping our fingers, she said. The earlier an abortion is done, the easier it is for everyone. 
 It did help that when I got the bad news, that I had worked to get the information as soon as possible. I knew what the worst-case scenario was, Satu said. 
 The pregnancy was terminated at 21 weeks. 
 Satu allowed the doctors to examine her and the fetus. What they found was even worse than what the scans had indicated. They found the virus in many different parts of the fetus: the brain, the placenta, in muscles, the liver, the lungs and spleen. They described their findings in the New England Journal of Medicine. 
 It seemed like a science fiction movie, that there was this virus that prefers brain cells, Satu said. 
 Doctors have since demonstrated that Zika goes into developing brain and nerve cells, destroying them. It also damages other tissues, and health workers are only just beginning to list the birth defects the virus can cause, from the devastating brain destruction to deformed joints and eye damage. 
 Satu has no doubt she made the right decision. I was probably the best-treated Zika patient in the whole world. I would say I am a privileged person, she said. 
 I had the option of terminating the pregnancy. Here in the U.S. people can mostly decide when they want to be pregnant and when they dont want to be pregnant. In Latin America, they mostly dont have that choice. 
 Zikas caused thousands of birth defects in Brazil, Colombia and elsewhere. It has infected hundreds of thousands or even millions of people across central and South America and the Caribbean. Many of the affected women are poor, with little access to health care. 
 I would say its saddest for those families that end up having a baby with microcephaly and little support for the family and child, Satu said. 
 She worries that people may not take the risk seriously. It is very easy to say 'it wont happen to me', Satu said.'' 
 More than 2,200 cases of Zika have been reported in the United States, most carried by travelers from more affected zones. But each infected traveler has the potential to infect someone else and if its a pregnant woman, the effects could be catastrophic. 
 Because Zika doesnt cause symptoms in most people, Satu worries whether people can transmit it without knowing. So does Scott Weaver of the University of Texas Medical Branch, who chairs the Global Virus Network Zika Task Force. 
 Related: Pregnant and Worried About Zika 
 It is striking that a lot of women still understand so little about the risk, Weaver said. 
 The one message that doesnt get out very often is even if you travel to an affected area and you are not at high risk like a pregnant woman and her partner, you can really have a significant impact on public health by protecting yourself from mosquito bites after you get back, he added. 
 Thats because it takes a human being to carry the virus to a new area and to infect mosquitoes there. Mosquitoes dont infect one another  they get infected by biting people. 
 Its actually everybody who could spread the virus, Satu said. Even here in D.C. you can get bitten and not know it. You wouldnt know you were spreading the virus. 
 Plus, its hard to persuade even a pregnant woman to cover up and use mosquito repellent every single day in the summer heat. 
 Related: 'Invisible' Zika Epidemic Frustrates Health Officials 
 It is very human to think its just one mosquito or to say, right now I dont see any mosquitoes, Satu said. 
 And shes infuriated by rumors that mosquitoes or Zika are not causing the birth defects, but insecticides or perhaps something else. One friend, unaware of what happened to Satu, posted one of the rumors on social media. That makes me angry, Satu said. 
 It seems very unlikely that I myself would have been in touch with a pesticide somewhere and that the Zika virus would at the same time infect me and be in the tissues of the fetus. 
 Now Satu is free of any evidence of Zika, as is her husband. Traces of the virus can stay in semen for six months or longer, but his tests are clear. 
 And so is the future. 
 Me and my husband would want to have a child, so I think its in the planning, Satu says, smiling. 
 
