__HTTP_STATUS_CODE
200
__TITLE
Harvey Weinstein Says Im Not Doing OK as Brother Calls Him Very Sick Man
__AUTHORS
Erik Ortiz
__TIMESTAMP
Oct 13 2017, 1:58 am ET
__ARTICLE
 The brother of Harvey Weinstein ripped into him as a "very sick man" whose remorse rings "hollow"  a day after the ousted studio executive said he needs help following a string of sexual misconduct allegations. 
 "I've urged him to seek immediate professional help because he is in dire need of it," Bob Weinstein, who co-founded the film studio The Weinstein Co. with his embattled brother in 2005, said in a scathing statement Thursday. 
 "He said he would go away for help and has yet to do so," Bob Weinstein added. "He has proven himself to be a world class liar and now rather than seeking help he is looking to blame others. His assertion is categorically untrue from A to Z." 
 As more celebrities came forward Wednesday to share alleged encounters with the movie mogul, Harvey Weinstein acknowledged outside of his daughter's Los Angeles home that he's "not doing OK, but I'm trying." 
 "I gotta get help," the 65-year-old said in a video licensed by NBC News, before getting into a car that was reportedly taking him to the airport. "You know, what? We all make mistakes. Second chance, I hope." 
 Weinstein, a longtime Hollywood power player who helped to launch countless careers, now stands accused of sexual harassment or sexually inappropriate behavior by more than two dozen women over decades. 
 Earlier in the day, Los Angeles police were called to his daughter's home when, according to TMZ, she was concerned about her father's well-being. But when police arrived, the LAPD told NBC News, the situation was relayed as a family dispute. 
 Related: Whatever Harvey Weinstein Is, He Is No Sex Addict, Experts Say 
 Weinstein's representatives did not return a request for further comment Wednesday, but have said in a statement that "any allegations of non-consensual sex are unequivocally denied" by him and that he did not retaliate against women who rejected his advances. 
 The NYPD said its detectives were reaching out to women who publicly spoke out about Weinstein's alleged behavior to determine if they may have been victimized in New York. 
 Following an investigation last week in The New York Times detailing some of the accusations, the board of The Weinstein Co. fired him Sunday. Another damaging report in The New Yorker this week included audio of Weinstein appearing to confess to the sexual assault of Filipina-Italian model Ambra Battilana Gutierrez in 2015. 
 In addition, Ashley Judd, Mira Sorvino, Rose McGowan, Gwyneth Paltrow and Angelina Jolie have been among the more high-profile celebrities to go on the record about their alleged encounters. 
 On Wednesday, model and actress Cara Delevingne posted on Instagram that Weinstein, during a meeting, asked her to kiss another woman and then attempted to kiss her as she tried to leave. 
 She ended up getting cast in The Weinstein Co.'s 2017 film, "Tulip Fever." 
 "Since then I felt awful that I did the movie. I felt like I didn't deserve the part," Delevingne wrote. 
 The statement by Bob Weinstein, who remains the head of The Weinstein Co., follows one by the wife of Harvey Weinstein, fashion designer Georgina Chapman, who said this week that she wouldn't stand by his "unforgiveable actions." 
 Related: Could Harvey Weinstein Face Criminal Charges? 
 The unfolding scandal has also put a spotlight on the sexism and sexual harassment that has run rampant in Hollywood in general. Male actors, including James Van Der Beek and Terry Crews, have also tweeted about their experiences and why victims often feel powerless to come forward. 
 The fallout from the revelations continued with Richard Koenigsberg deciding to step down from The Weinstein Co. board, a source in a position to know told NBC News. 
A source confirms, Richard Koenigsberg, is the latest Weinstein Co. board member to step down.
 The Academy of Motion Picture Arts and Sciences' board said it would hold a special meeting Saturday to discuss the allegations against Weinstein and what, if any, course of action it should take. 
 The organization said in a statement Wednesday that Weinstein's alleged conduct is "repugnant, abhorrent, and antithetical to the high standards of the Academy and the creative community it represents." 
