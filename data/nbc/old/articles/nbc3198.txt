__HTTP_STATUS_CODE
200
__TITLE
ISIS Releases Recording Billed as Abu Bakr Al-Baghdadi
__AUTHORS
Tracy Connor
__TIMESTAMP
Sep 28 2017, 3:39 pm ET
__ARTICLE
 ISIS released an audio recording Thursday that it billed as a message from leader Abu Bakr al-Baghdadi, rumored to be dead since the summer. 
 NBC News security analysts Flashpoint said the voice in the propaganda message appears to be the terror boss but they could not pinpoint when it was made. 
 The recording, which was released by the official ISIS media wing, makes reference to North Korea "threatening America and Japan with nuclear power," but it wasn't clear if that was based on the most recent round of saber-rattling, Flashpoint said. There's another reference to an international summit in Kazakhstan, but that took place in June. 
 In mid-June, Russia's Foreign Ministry said it was "highly likely" al-Baghdadi had been killed in an air strike south of Raqqa, Syria, and weeks later the Syrian Observatory for Human Rights said his death was confirmed. U.S. officials, however, said they believed al-Baghdadi was still alive. 
 Al-Baghdadi was last heard from in November 2016, before the Islamic State lost Mosul, Iraq. ISIS has lost more than half of the territory it once controlled in Iraq and Syria. 
