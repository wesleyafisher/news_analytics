__HTTP_STATUS_CODE
200
__TITLE
After 31 Years, Ruth Parker Still Hopes to See Sons CJ and Billy Vosseler Again
__AUTHORS
Logan Johnson
__TIMESTAMP
Oct 8 2017, 1:51 pm ET
__ARTICLE
 Charles Vosseler was a businessman who dreamed of living a simple life  at least that's who he portrayed himself to be in the classified ad Ruth Parker found him in in 1980. Coming from a farming background herself, Ruth found the idea of simple and sustainable living to be right up her alley. 
 After less than a year of letter writing and phone conversations, Ruth packed up her things and moved from Wisconsin to New Hampshire to marry Charles. 
 Ruth and Charles, who had a real estate business, had two children together, both boys: Charles "CJ" Vosseler Jr. and William "Billy" Vosseler. 
 But after six years of marriage, Ruth and Charles divorced. 
 And that's when Ruth's life became anything but simple. 
 On October 9, 1986, Charles picked up the boys  CJ, then 3 years old and Billy, then 2  for the weekend, as he did regularly. He agreed to bring them home to their mother in a couple of days. 
 But he didn't bring them home. 
 Instead, he called Ruth and told her he and the boys were in Connecticut visiting his aunt, and he would be extending his time with them until the following day. 
 The following day came and went. No boys. 
 Ruth says she went to Charles's office to confront him. But when she arrived, she saw some of the employees leaving the office with boxes in their arms. 
 "Charlie came in on Friday and told his employees that he was closing the business and that was it," Ruth told Dateline. 
 Charles had closed his business the same day he picked up his sons for the weekend, Ruth said. She instantly knew she had to act fast. 
 Before Ruth went to the police, she was stunned by another awful realization: Every picture she'd ever taken of CJ and Billy was gone from her apartment. Charles must have removed the photos of the boys so she would have nothing to present to authorities to use for missing posters. 
 All she had left was an old baptism certificate. 
 Charles, Ruth says, had even wiped out their joint bank account and stopped making payments on her car. 
 "What I was afraid of is that I would start to scream and I wouldn't be able to stop," said Ruth. 
 When Ruth went to the authorities in Rochester, New Hampshire to tell them her children were kidnapped by their own father, she says detectives were hesitant to act. Parental kidnapping wasn't considered a serious crime in the 1980s. 
 "They told me they didn't do that," Ruth told Dateline. "They didn't do child kidnapping, especially parental kidnapping." 
 One thing authorities did seem to care about was the car Charles was driving that weekend. 
 "There's a law against taking someone's car but not their own children," Ruth said. 
 Six weeks after the boys went missing, a family friend remembered they had once taken home videos of CJ and Billy. The authorities were able to capture images from that video to use for posters and age-progression images. 
 In hindsight, Ruth tells Dateline that prior to her children's disappearance Charles was behaving strangely, but she didn't notice at the time. 
 For instance, Charles owned a horse that he loved, but Ruth says one time she went by his house, the horse was gone. She was told he had sold it to a neighbor. 
 "Charlie didn't live without his horse," said Ruth. "So something was up. I figured something was up." 
 Ruth also remembers Charles asking her to quit her job in social work, because he said it would be best for the family and their efforts to live a simpler lifestyle. At the time it made sense, so Ruth went along with her husband's request. 
 "What he told me at the time was that would take too much energy from the family," said Ruth. "It is something that requires you to put a lot of emotional energy into cases, as well as physical." 
 Ruth tells Dateline that she believes Charles was trying to eliminate both her support system and her income, before he took off with the boys. Ruth also says that when friends called to talk to her, Charles would tell them she was unavailable and then hang up the phone. 
 "He systematically separated me from family and friends," said Ruth. She continues to battle the guilt that she feels for not being able to bring her children home after all this time. 
 "It's still pretty strong," said Ruth. But, she added, "I don't have the nightmares anymore  I used to have nightly nightmares that my kids are calling for me and I can't help them." 
 After more than 30 years, Ruth says she continues to hope that this will be the last year without her 'boys.' They would, of course, be grown men now. 
 "Some folks get to the point when they give up. But not me," said Ruth. "I'll never get to that point." 
 Thirty years is a long time for a mother to hold on to the hope that her children will come walking through the door again, but for Ruth hope is all she has. 
 "People have talked to me about being brave, being strong. And that's not really the case," said Ruth. "What's really the case is: You have no choice." 
 Over the course of three decades, Ruth has watched other parents be reunited with their missing children. Those are bittersweet moments for her. 
 "Part of you asks the good Lord, 'Why wasn't it my turn this time?' But you can't stay there. You still have to go on," said Ruth. 
 If authorities catch Charles, Ruth tells Dateline she won't force a relationship with her sons if they aren't ready. 
 "My children are now in their mid-30s," said Ruth. "I can't take them to ball games any more. They're not moving back in with me, I can't get that childhood back. So I need to meet them where they're at." 
 The closest authorities have come to catching Charles was in 1988 in Oklahoma. Authorities arrived at his home to find he had burned it down and fled. At that time he was going by the name of an old college friend, Charles Wilson. 
 "It appears Vosseler wanted to ensure that he left no trace evidence behind that could help law enforcement locate him," said FBI Special Agent Gary Cacace. "It made the FBI realize that he had enough interpersonal skills and charisma to enlist help from someone who was willing to put themselves in jeopardy of prosecution." 
 Charles was charged with unlawful flight to avoid prosecution, and there is still a warrant out for his arrest. 
 Special Agent Cacace tells Dateline that Charles, CJ and Billy Vosseler could all be using different names. 
 The search is ongoing and the FBI is offering a reward of up to $25,000 to anyone who can provide information leading to the capture of Charles Vosseler or his sons. 
 "The FBI continues to actively pursue all logical leads and has conducted numerous interviews in its efforts to find Vosseler and hopefully allow CJ and Billy to learn their true origins, and see their mother for the first time in 31 years," said Special Agent Cacace. 
 Anyone with information regarding the whereabouts of Charles, CJ or Billy Vosseler should contact the FBI Boston Division at 857-386-2000 or their local FBI Office. 
