__HTTP_STATUS_CODE
200
__TITLE
Was Jordin Sparks Sending Trump a Message at Cowboys-Cardinals Game?
__AUTHORS
Alex Johnson
__TIMESTAMP
Sep 25 2017, 11:21 pm ET
__ARTICLE
 When Jordin Sparks stepped forward to sing the National Anthem before Monday night's NFL game between the Dallas Cowboys and the Arizona Cardinals in Glendale, Arizona, she didn't take a knee  unlike many of the players. 
 She was more subtle. 
 Sparks, 28, the platinum-selling singer who won the sixth season of "American Idol," belted out "The Star-Spangled Banner" with gusto  and with a small note written on her hand: "Prov 31:8-9." 
 The verses at Proverbs 38:8-9 are part of an Old Testament admonishment to rule fairly, taught to King Lemuel by his mother: 
 8 Speak up for those who cannot speak for themselves,for the rights of all who are destitute. 
 9 Speak up and judge fairly;defend the rights of the poor and needy. 
 Sparks  the daughter of nine-year NFL veteran Phillippi Sparks  dipped her toe in the political waters two weeks ago, when, as a judge at the Miss America pageant, she asked Miss Missouri this question: "There are multiple investigations into whether Trump's campaign colluded with Russia on the election. Well, did they? You're the jury: guilty or innocent, and please explain your verdict." (Miss Missouri acquitted the president.) 
 On Inauguration Day, Sparks tweeted out prayers for Trump, saying she had been "raised to show respect for everyone." 
https://t.co/HXul9UuUPl pic.twitter.com/DOBz5j1TKE
 
