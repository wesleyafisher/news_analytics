__HTTP_STATUS_CODE
200
__TITLE
Suicide Truck Bomb Kills Dozens in Iraq, Mostly Iranian Pilgrims
__AUTHORS
Reuters
__TIMESTAMP
Nov 25 2016, 3:49 am ET
__ARTICLE
 BAGHDAD  A suicide truck bomb killed about 100 people, most of them Iranian Shiite pilgrims, at a gas station Thursday south of Baghdad on Thursday, police and medical sources said. 
 An online statement claimed claimed responsibility for the attack in the city of Hilla in the name of ISIS, which is fighting off a U.S.-backed offensive in Mosul in northern Iraq. 
 The pilgrims were en route back to Iran from the Iraqi Shiite holy city of Kerbala, where they had commemorated Arbaeen. The rite marks the 40th day of mourning for the killing of Imam Hussein, a grandson of the Prophet Mohammad, in the 7th century A.D., the medical sources said. 
 A restaurant on the premises of the gas station is popular with travelers. Five pilgrim buses were set afire by the blast from the explosives-laden truck, a police official said. 
 In recent months, ISIS has intensified attacks in areas out of its control in efforts to weaken the offensive that was launched Oct. 17 to retake Mosul, the last major Iraqi city under its control. 
 Iran's Foreign Ministry condemned the attack. Tehran will continue to support Iraq's "relentless fight against terrorism," Bahram Qasemi, a spokesman for the ministry, was quoted as saying by the Tasnim news agency. 
 The White House said the bombing "was clearly intended to stoke sectarian tensions." 
