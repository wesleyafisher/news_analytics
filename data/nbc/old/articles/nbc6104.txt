__HTTP_STATUS_CODE
200
__TITLE
Uber Board Votes to Change the Companys Power Structure
__AUTHORS
Alyssa Newcomb
__TIMESTAMP
Oct 3 2017, 8:00 pm ET
__ARTICLE
 SAN FRANCISCO  Uber's board of directors voted on Tuesday for new measures that will reshape the company's power structure, stripping early investors, including ex-Chief Executive Travis Kalanick, of some of their influence. 
 In addition to the corporate governance changes, which will eventually see all shareholders receiving one vote per share of stock they own, regardless of the class, Uber also chose to move forward with an investment from the Japanese telecommunications firm SoftBank. 
 The decisions were made relatively quickly for a board that has at times been divided. Two new members, former Xerox Chief Executive Ursula Burns and former Merrill Lynch Chief Executive John Thain, joined the meeting Tuesday to round out the 11-member group. 
 Related: Can Companies Like Uber, Equifax Find Redemption After Scandals? 
 "Today, after welcoming its new directors Ursula Burns and John Thain, the Board voted unanimously to move forward with the proposed investment by SoftBank and with governance changes that would strengthen its independence and ensure equality among all shareholders," Uber's board said in a statement. "SoftBank's interest is an incredible vote of confidence in Uber's business and long-term potential, and we look forward to finalizing the investment in the coming weeks." 
 In a statement issued by his representative, Kalanick said the board's unanimous vote marked "a major step forward in Uber's journey to becoming a world class public company." 
 "We approved moving forward with the Softbank transaction and reached unanimous agreement on a new governance framework that will serve Uber well," he said. 
 Last week, in an unexpected move, Kalanick used his right to fill two vacant board seats with Burns and Thain. 
 The two new appointments came as a "complete surprise" to Uber and the board of directors, and were a "highly unusual" move, according to an internal letter from new Chief Executive Dara Khosrowshahi to employees obtained by Recode. 
 "That is precisely why we are working to put in place world-class governance to ensure that we are building a company every employee and shareholder can be proud of," Uber said in a statement. 
 Kalanick's right to fill those seats is being challenged in a lawsuit filed by Benchmark, an early investor that owns 13 percent of Uber. Benchmark, which wants Kalanick off the board, alleges that he "fraudulently" obtained the power in 2016 to name three additional members to the company's board of directors. After Kalanick resigned in June, he appointed himself to one of those seats. 
 A source familiar with Kalanick's thinking said he didn't intend to fill the seats until the legal matter, which is in arbitration, was completed. However, after corporate governance proposals were presented last week, he decided to exercise his right to appoint Burns and Thain. 
 Benchmark said it never would have granted Kalanick control over the additional seats had it been aware of the "gross mismanagement of Uber and several other significant matters," according to the complaint. 
 The board meeting comes as Uber's new chief executive met with a London transport official, trying to persuade the city to reverse a decision to not renew Uber's license. 
 Transport for London said in a statement last month that "Uber's approach and conduct demonstrate a lack of corporate responsibility in relation to a number of issues which have potential public safety and security implications." 
 In London, Khosrowshahi, who apologized last month for the company's mistakes, struck a hopeful tone that Uber and London could strike a deal. 
Great meetings in London, including w some of the drivers who rely on our app. Determined to make things right in this great city! pic.twitter.com/QLgqon30yT
 After meeting with the transport commissioner, Khosrowshahi also spent time speaking with drivers who rely on Uber to make their livings. As Uber goes through the appeals process, the company's 40,000 drivers will be able to continue operating in the city. 
 The meeting also came as Jo Bertram, head of Uber's Northern Europe markets, announced that she is leaving the company. Her departure wasn't related to the London license issue, according to reports. 
