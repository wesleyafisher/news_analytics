__HTTP_STATUS_CODE
200
__TITLE
Barack Obama to Make First Campaign Appearance Since Leaving Office
__AUTHORS
Mark Murray 
__TIMESTAMP
Oct 11 2017, 2:52 pm ET
__ARTICLE
 Former President Barack Obama will stump in Virginia next week for Democratic gubernatorial candidate Ralph Northam, the Northam campaign announced Wednesday. 
 The rally  Obama's first public campaign event since leaving office  will take place in Richmond on October 19. 
 The event is one of only two public campaign appearances on the books for Obama this year. The former president is also expected to campaign for New Jersey gubernatorial candidate Phil Murphy, although a date has not yet been set. 
 And the announcement comes a day after the Northam camp said that former Vice President Joe Biden will appear at a workforce development roundtable for the Democratic candidate this Saturday. 
 Virginia's gubernatorial race, will takes place on November 7, is the marquee general election this year. Polls show Northam will a slight lead over Republican Ed Gillespie. 
 On the Republican side, former President George W. Bush will headline fundraisers for Gillespie later this month, and Vice President Mike Pence is set to appear at a rally for the GOP candidate this Saturday. 
