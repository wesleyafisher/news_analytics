__HTTP_STATUS_CODE
200
__TITLE
After Equifax, Why Are Cat Photos Safe But Not Our SSN?
__AUTHORS
Ben Popken
__TIMESTAMP
Sep 22 2017, 4:44 am ET
__ARTICLE
 Why are your cat pictures on Facebook better protected than your entire personal financial history on Equifax? Or, apparently, private data on the SEC's servers that could have been used for illegal trades? 
 The last time Facebook had a breach was in 2013  and that wasn't even a hack; the company accidentally published 6 million users' phone numbers and email addresses. But some of the biggest organizations entrusted with the most sensitive data are getting hacked and nobody can stop them. 
 Or can they? 
 Security experts say the smartest companies out there are proactively budgeting for security and employing multi-layered approaches to prevent, detect, and respond to network intrusions. Others... aren't. Or they may only invest after they've already been hacked. 
 But there are some basic best practices businesses should follow. And security-minded consumers should demand they do or take their business elsewhere, experts say. (Of course, with credit bureaus, individual consumers are not actually customers of the bureaus, they and their data is the commodity, and cannot opt out or "punish with their wallet," though credit report freezes are a step in the direction) 
 For starters, to catch hackers, try hiring some. 
 D.J. Vogel, a partner in the security and compliance practice at Sikich, a professional services firm, said that when they're hired by a hospital to conduct "penetration testing" they might pose as a patient and attempt to get in that way. Or ask for the hospital to give them one of the doctor's laptops to simulate what would happen if that computer got lost or stolen. 
 It's a wake-up call for executives when they realize, "Hey, we gave these guys no access  and they got the keys to the kingdom," Vogel told NBC News. 
 Related: The One Move to Make After Equifax Breach 
 Hackers are always looking for weak links. Hackers breached an energy company in Ukraine using fake Word documents that were designed to look like resumes. Cybercriminals use fake Twitter accounts to impersonate customer service reps and trick customers into divulging their banking information. And just a few months ago, digital thieves broke into a casino through its internet-connected fish tank. 
 When the Panama Papers breach surfaced, it turned out the law firm Mossack Fonseca they came from hadn't patched its blogging and server software. Afterward, Mossack Fonseca blamed the intrusion on "an international campaign against privacy." 
 And when the WannaCry ransomware virus spread around the world through older versions of Windows in March  at around the same time as the vulnerability that lead to the Equifax breach was first discovered  Microsoft President and Chief Legal Officer Brad Smith blamed government entitites for hoarding system flaws to use for their own purposes, which then fell into the wrong hands. 
 "We have seen vulnerabilities stored by the CIA show up on WikiLeaks, and now this vulnerability stolen from the NSA has affected customers around the world," wrote Smith at the time. 
 The intrusions are ramping up: 2016 was a record year for data breaches, with over 1,000 reported data breaches at U.S. companies and government agencies, a 40 percent increase since the year before. 
 "How many of the Fortune 500 are hacked right now? The answer: 500," Mikko Hypponen, chief research officer at F-Secure Oyj, told Bloomberg in 2015. "They all have security breaches, big or small. If you have a big enough infrastructure, you wont be able to secure all of it." 
 The Equifax breach shows how companies need to speed up their update cycles. Most consumers know to keep their own iPhone and iTunes up to date, so why can't corporate America keep the systems protecting valuable data up to date? 
 Related: Equifax Hackers Exploited Months-Old Flaw 
 Less sophisticated companies likely "don't have proper change practices to apply patches on a weekly basis. They're probably lucky if it's on a biannual basis," said Jonathan Jaffe, a cybersecurity and compliance consultant at PriceWaterhouseCoopers. 
 For what it's worth, Equifax does face a data security paradox. 
 "Equifax has the challenge of protecting confidentiality and integrity of records," wrote Chris Hoofnagle, a professor at the University of California, Berkeley, School of Information and the School of Law. "In confidentiality, it is difficult because it has to provide instantaneous access to a very large, diverse group of report users and to the consumers themselves." 
 Facebook and other social media platforms don't have the same challenges. Also, the value of a personal cat photo sold on the dark web is a bit lower than a social security number. Were that math to be changed, we might see more attempts on the social media giants. Facebook, Google and Snapchat didn't provide comments in response to emailed inquiries by NBC News. 
 That's why other experts recommend following a protocol known as Privileged Access Management. Instead of users getting passwords on the system itself, they actually log in to a separate system. Then that system itself logs into the main computer and controls the password, changing the real access password every few minutes. 
 In the event of a breach, the stolen password would only be good for a few minutes. 
 "If I want to drive your car to a 7-Eleven, first you look at my ID and make sure I am who it says I am," explained Jaffe. "Then you give me a set of keys to drive, you track those keys via GPS, and once you come back and the next person wants to drive, I give them an entirely new set of keys." 
 Otherwise, typically, "Companies hand out privileges to tech folks, or non techies, and never take it away," independent cybersecurity expert Bob Sullivan told NBC News. "Down the road they have no idea who can do what." 
 Another strategy that companies can use to lower the possibility of a breach is Data Loss Prevention, or DLP, which alerts you when unusual activity is detected on the network. For instance, if the normal course of business is one record being accessed at a time, and all of a sudden 100,000 records are being accessed... then klaxons need to go off. 
 Of course an alarm only works if someone pays attention to it. The problem with "DLP" is that companies tend to underinvest in it, experts say, putting a low-level person in charge of it who just clicks through the alerts while sipping coffee and moving on to the next task. 
 Related: Equifax Fallout: FTC Launches Probe, Websites and Phones Jammed With Angry Consumers 
 If DLP was installed and being properly watched on Equifax's systems, it could have set off warnings earlier on that gigabytes of data were being slurped by an outside agent. 
 Lastly, there are third-party intrusion detection teams that companies can contract with, sort of like a Sloman's Shield for your system. They can shut down suspicious activity on the incoming ports and send the internal team a wake-up call when any kind of sustained drain is detected. 
 But it really comes down to the bottom line. Security costs money. And until it becomes a "job-threatening event," said Jaffe, most companies will instead try to keep their costs down. 
 There's no one holding these companies' feet to the fire. Though Visa and Mastercard contractually requires Equifax's vendors to adhere to a strict, prescriptive set of data security protocols known as PCI DSS, which includes frequent patching, there is no one making Equifax follow the same rules, experts say. 
 "Compliance typically drives security budgets," said Vogel. For institutions like Equifax and the SEC, "The market is not mandating specific security initiatives," because they aren't subject to regulatory or market forces mandating it. 
