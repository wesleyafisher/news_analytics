__HTTP_STATUS_CODE
200
__TITLE
DOJ Unveils New Opioid Fraud And Abuse Unit To Combat Crisis
__AUTHORS
Corky Siemaszko
__TIMESTAMP
Aug 2 2017, 2:57 pm ET
__ARTICLE
 The Justice Department unveiled a new unit Wednesday to tackle the national opioid epidemic and announced that it is dispatching a dozen federal prosecutors to hard hit states like West Virginia, Pennsylvania, Florida and Ohio to combat the crisis. 
 If you are a doctor illegally prescribing opioids for profit or a pharmacist letting these pills walk out the door and onto our streets based on prescriptions you know were obtained under false pretenses, we are coming after you, Attorney General Jeff Sessions said. We will reverse these devastating trends with every tool we have. 
 Sessions announced the pilot program Wednesday at the headquarters of the police department in Columbus, Ohio, which is located in a county where 173 people have died this year alone as a result of drug overdoses. 
 I wanted to be here with you all today because Ohio is at the center of this drug crisis that is gripping our entire nation, Sessions said. The crisis affects all of us, but it is especially taking its toll on this community. 
 Calling it an opioid fraud and abuse detection unit, Sessions said it will focus specifically on opioid-related health care fraud using data to identify and prosecute individuals that are contributing to this opioid epidemic. 
 Related: Sessions Heads to West Virginia, Epicenter of U.S. Opioid Epidemic 
 "With this data in hand, I am also assigning 12 experienced prosecutors to focus solely on investigating and prosecuting opioid-related health care fraud cases in a dozen locations around the country where we know enforcement will make a difference in turning the tide on this epidemic." 
 Working in tandem with the FBI, DEA and local law enforcement, the prosecutors will help us target and prosecute these doctors, pharmacies, and medical providers who are furthering this epidemic to line their pockets, Sessions said. 
 Sessions also praised police working on the front lines of the epidemic and mentioned the East Liverpool, Ohio, officer who accidentally overdosed after being exposed to fentanyl. 
 Luckily, he was in his squad room and they got to him immediately, Sessions said. As his police chief said, If he would have been alone, he would have been dead. Or imagine if hed gone straight home that day to give his kids a hug? These are terrifying thoughts for our law enforcement. 
 Sessions in recent weeks has been the target of repeated attacks from President Trump over his decision to recuse himself from the investigation into Russian meddling in the presidential election. 
 But in his remarks at the Columbus Police Department, Sessions steered clear of that controversy and instead credited his boss with focusing on the Mexican border, even plugging the infamous wall that Trump has vowed to build. 
 We also have to recognize that most of the heroin, cocaine, methamphetamine, and fentanyl in this country got here across our southern border, Sessions said. Under President Trumps strong leadership, the federal government is finally getting serious about securing our borders. Illegal entries are down 50 percent already and the wall has not even gone up. 
 Sessions announcement came two weeks after the DOJ announced it had charged more than 400 people with medical fraud, including dozens of doctors who had been prescribing unnecessary opioids and medical facilities that had been supplying addicts with pills and illegally billing Medicare and Medicaid to the tune of $1 billion. 
 On Monday, the presidential opioid commission urged Trump to declare a national emergency made several recommendations for fighting the epidemic like expanding treatment facilities across the country, educating doctors about the proper way to prescribe pain medication and equipping all police officers with the anti-overdose remedy naloxone. 
