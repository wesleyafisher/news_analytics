__HTTP_STATUS_CODE
200
__TITLE
North Korea Says Sanctions Cause Colossal Damage But Wont Work
__AUTHORS
Phil Helsel
Stella Kim
__TIMESTAMP
Sep 29 2017, 11:21 pm ET
__ARTICLE
 North Korea on Friday said that international and U.S. sanctions are causing a "colossal amount of damage" but insisted they would not work, and that sanctions have failed to curb its nuclear ambitions. 
 A spokesman for North Korea's Sanctions Damage Investigation Committee made the comments in a statement distributed by the country's state-run media, KNCA. In the statement, the spokesman accused the United States of "a brutal criminal act." 
 The U.N. Security Council this month approved new sanctions on North Korea over its ballistic missile tests and nuclear weapons program, and on Sept. 21 President Donald Trump announced additional sanctions that target individuals, companies and financial institutions that do business with the regime of Kim Jong Un. 
 China, North Koreas ally and main trading partner, last Saturday said it would limit energy supplies to North Korea and would stop buying its textiles, and on Thursday ordered North Korean-owned businesses to close, the Associated Press reported. 
 "The colossal amount of damage caused by these sanctions to the development of our state and the people's livelihood is beyond anyone's calculation," The North Korean spokesman said. 
 But the statement added, "it is a foolish dream to hope that the sanctions could work on the DPRK," referring to an acronym of the countrys official name the Democratic People's Republic of Korea, and "the sanctions have failed to stop it from becoming a full-fledged nuclear weapons state and making rapid progress in the building of an economic power for more than half a century." 
 North Korea conducted its sixth nuclear test on Sept. 3, and it has twice conducted tests of intercontinental ballistic missiles in defiance of U.N. resolutions. On Sept. 15, North Korea for a second time fired a ballistic missile that flew over Japan before crashing into the Pacific Ocean. 
 Tensions have increased between North Korea and the U.S. in recent weeks, with Trump in August promising that North Korea would be met with "fire" and "fury" if it threatens the U.S. Trump took to calling Kim "Rocket Man" and in a speech at the U.N. declared that the United States would "totally destroy" North Korea if forced to defend itself. 
 North Korean dictator Kim Jong Un in a rare statement addressed to a world leader called Trump a "dotard." And after Trump last weekend reacted on Twitter to a speech by North Koreas foreign minister by suggesting if the minster "echoes thoughts of Little Rocket Man, they won't be around much longer!," North Korea's foreign minister said Trump "declared war" on his country. The White House rejected the notion that the U.S. declared war. 
 The North Korean official said in Fridays statement that "the U.S. should be fully aware that the more frantic it gets in the unprecedented anti-DPRK frenzy, the earlier it will meet its own miserable extinction. 
 Kim and his officials are no stranger to fiery rhetoric, often threatening to immolate the U.S. in "a sea of fire" and to reduce "the whole of the U.S. mainland to ruins." 
