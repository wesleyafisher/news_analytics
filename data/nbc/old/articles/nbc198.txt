__HTTP_STATUS_CODE
200
__TITLE
Missing Army Veteran Julia Jacobson Now Believed Dead, Ex-Husband Arrested
__AUTHORS
Bianca Hillier 
__TIMESTAMP
Oct 17 2017, 9:35 am ET
__ARTICLE
 A San Diego Army veteran who disappeared last month is now believed to be dead, according to local officials. 
 Julia Jacobson, 37, has been missing since September 2, 2017. She was last seen on security video at a store in Ontario, California with her Wheaten Terrier, Boogie. According to a press release from the Ontario Police Department on October 16, detectives now have reason to believe Julia and Boogie are both deceased. 
 While Julia and Boogies remains have not yet been located, on Friday October 13, police arrested Julias ex-husband on suspicion of her murder. 
 According to the press release, through a coordinated effort with the Phoenix Police Department and the FBIs Arizona Violent Crimes Task Force unit, Dalen Larry Ware was arrested at his home in Laveen, Arizona. He was then transferred to the San Bernardino County West Valley Detention Center and is awaiting booking. 
 Ontario police believe Julia was murdered based on the forensic analysis of her car. Shortly after Julias disappearance, Dateline reported that authorities were able to track her company car, equipped with OnStar, to a spot near her apartment just five days after she was last seen. The vehicle had been left unlocked, with the windows partially rolled down. 
 Dateline spoke with Julias family around the time of her disappearance. Her sister, Casey Jacobson, described Julia as someone who sticks to her routines and was the rock of the family since their mother died in February. 
 She came home safe after serving her country, and then now she disappears," Casey told Dateline in September. "We were reeling from the loss of our mother, and now Julia is gone." 
 Last night, on the Help Find Julia Jacobson  San Diego Missing Person Facebook page, the family wrote, in part: The loss of Julia is beyond words. There will be no more birthday parties, backyard gatherings, holiday celebrations or other family activities to share. The laughter, hugs, guidance and our sense of security are forever gone and our familys hearts will be forever broken. 
 When Dateline spoke with Lt. Mike Holden of the San Diego Police Department at the time of Julias disappearance, he confirmed they had spoken with Julias ex-husband but that he was not a person of interest to [them at the] time. 
 The Ontario Police Department is asking anyone with information on the case to call them at 909-986-6711, Detective Ruben Espinoza at 909-395-2894 or Detective Brant Devey at 909-395-2715. Information can also be reported anonymously by calling WE-TIP at (800) 78-CRIME or online at www.wetip.com. 
 Julia was originally featured in Datelines Missing in America series shortly after she disappeared. 
