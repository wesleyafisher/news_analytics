__HTTP_STATUS_CODE
200
__TITLE
States Work to Keep Opioid Epidemic From Splitting Up Families
__AUTHORS
Shefali Luthra, Kaiser Health News
__TIMESTAMP
Aug 17 2017, 6:42 pm ET
__ARTICLE
 After Raven Mosser gave birth six years ago, she woke up to a social worker in her hospital room. Her newborn son had been born exposed to opioids  drugs she had been abusing for years. If she didnt get clean, she was at risk of losing him. 
 Mosser, 26, is one face of the nations opioid crisis. But the epidemic is also touching a much younger set of victims: children whose parents have substance abuse problems, who are increasingly being shuttled through the foster care system, unable to stay at home even as their parents struggle to get clean. 
 Data from the U.S. Department of Health and Human Services show that from October 2012 to September 2015, as addiction surged, the number of kids entering the foster system rose 8 percent. In recent years, experts suggest, the number has continued to climb, though data arent yet available to track that increase. 
 Even before this epidemic, social systems struggled to meet demand. Now, the swell of kids needing care is putting new pressure on a network already stretched to capacity  both financially and in its ability to find homes and families that can take in displaced children. 
 Related: Trump Declares Opioid Crisis A National Emergency 
 This a replay of what we saw with the meth crisis.  Were seeing huge rates of children going into a system with limited capacity, said Christine Grella, a professor of psychiatry and biobehavioral sciences at the University of California-Los Angeles, who has researched whether and how drug treatment can be used to reunite mothers and children 
 The opioid crisis has taken a hefty toll across age and class groups, but researchers note it has fueled a particularly steep heroin surge among people ages 18 to 25 and among women, the precise demographic of young parents like Mosser. Their addictions can designate them as unfit parents. 
 In Mossers home state of Kentucky, the number of children in foster care rose from 6,000 in 2012 to 8,000 in 2015, with about a third of them entering the system because of their parents substance abuse, according to data kept by KVC Kentucky, a behavioral health and child welfare organization in Lexington. 
 The problem has led states to invest in new approaches, linking drug treatment with efforts to keep families together  a bid to tackle both substance abuse problems and reduce the burden on other parts of the safety net. 
 Kentucky was a pioneer, starting in 2007, when opioid addiction first emerged as a public health concern. The program, called Sobriety Treatment and Recovery Teams  or START  emphasizes a wraparound approach for at-risk parents that includes frequent home visits, vouchers for child care and transportation and mentorship from people in recovery. 
 Its hardly cheap, but the strategy is believed to save money by reducing spending on, for instance, emergency medical treatment for parents with addiction, or on moving kids through the system. Other states are trying similar approaches: Ohio is launching a program this fall explicitly modeled after Kentucky, armed with $3.5 million from the state attorney general. 
 Initiatives are under way in Indiana and North Carolina, too. But even as demand increases, funding is precarious, since these efforts are heavily financed by Medicaid, the federal-state insurance plan for low-income people whose budget congressional Republicans hope to trim. 
 For our first five years, for every one case we took we had to turn away two, said Tina Willauer, who directed Kentuckys program until this past July. 
 Under the Kentucky model, when child protection specialists learn a child is at risk, authorities specifically assess whether substance abuse could be a factor. If so, the parent is fast-tracked into treatment and assigned a recovery team, which coordinates among agencies such as childrens aid, mental health, social services and recovery mentors. 
 Related: NIH in New Push to Fight Opioid Epidemic 
 This sort of interagency collaboration is heralded by public health experts as a natural, effective response, and a way states can achieve the most bang for their buck. 
 We all know thats the way to go, Grella said. 
 But its a heavy lift for states to tackle these issues. 
 For one thing, keeping parents clean means ensuring they see an addiction specialist, or go to rehab or a recovery group. Those are all relative rarities in the small, rural areas where opioid addiction rates are relatively high. 
 Finding recovery coaches, those relatable faces whom patients like Mosser found invaluable, is also tricky, since it means recruiting and training people with histories of addiction but who are stable in their recovery. 
 For Mosser, that intervention  timed at a moment when she was ready to get help  has been transformative. 
 They never gave up. A lot of times in the past  people would throw in the towel, and say this person isnt going to get clean, recalled Mosser, who enrolled in the program after her sons birth. 
 Not to say it was easy. Even after entering START, Mosser relapsed, and cycled in and out of treatment. She ended up temporarily losing custody of her children, who were sent to live with her mother. She was completely broken, she said. 
 Today, the mother of four is sober and has been parenting her children at home the past two years. 
 Its hardly easy, she said. Theres the challenge, financially, of making ends meet. And she still experiences days when her patience with her children wears thin. 
 But she can see progress. Mosser now lives in nearby Pedro, Ohio, and is training to be a START mentor in Ashland. Her son just had a birthday. 
 He was born substance-exposed, she said. Now hes 6  and hes an amazing kid. 
 This story was contributed by Kaiser Health News, a national health policy news service that is an editorially independent part of the Henry J. Kaiser Family Foundation, a nonprofit, nonpartisan health policy research and communication organization not affiliated with Kaiser Permanente. 
