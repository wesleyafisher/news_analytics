__HTTP_STATUS_CODE
200
__TITLE
You No Longer Need a Plane Ticket to Wait at the Gate at This Airport
__AUTHORS
Harriet Baskas
__TIMESTAMP
Sep 2 2017, 6:04 am ET
__ARTICLE
 One much-missed aspect of the good-old days of air travel was the ability to walk a friend or family member right out to the gate and to be right there with a hug, a kiss or a balloon when someone you loved exited the plane after a long flight. 
 That all ended after 9/11, when only ticketed passengers were permitted past the security checkpoints. 
 Now Pittsburgh International Airport has figured out how to bring that airport amenity back. 
 With the approval of the Transportation Security Administration, starting on September 5 non-ticketed members of the public will be able to access gates, shops, restaurants and artwork located beyond the security checkpoint year-round. 
 Non-flyers wont be able to just sashay into the secure side of the airport, however. Before they are issued a myPITpass, an airport guest will have to check in at a special desk, show a valid photo ID (Drivers License or Passport), have their name vetted against existing No Fly lists and then line up and go through screening at the security checkpoint just as if they were heading out to a flight. 
 For now, passes will only be issued Monday through Friday from 9 a.m. to 5 p.m. and will only be valid the day they are issued. 
 PIT airport officials say they picked the 9 to 5 program hours to avoid the busiest times at the airport and at the checkpoints. But if security lines were to become long, people seeking a myPITpass will have to wait until the line dies down, said airport spokesman Bob Kerlik. 
 For its part, the Transportation Security Administration (TSA) is pleased to support the myPITpass program, but is quick to note that the agency did not hire any additional staff to accommodate the program. 
 And while the agency doesnt expect the myPITpass program will have a negative impact on checkpoint wait times, There are no plans to expand the program at this time, said TSA national spokesman Mike England via email. 
 Beyond seeing someone off or welcoming them home, there are plenty of reasons someone not flying away might want go through the trouble to get a pass to spend time on the secure, or air, side of PIT airport. 
 In addition to shops and restaurants not available anywhere else in the city, such as Hugo Boss, Armani, Tumi, Bar Symon and others, the airport also has a top notch art collection that includes a giant Alexander Calder mobile, works by Andy Warhol, and a new center core terrazzo floor that depicts iconic Pittsburgh neighborhoods. 
 News of the myPITpass program brings up pleasant memories for Dallas-based Southwest Airlines pilot Michael Weisser, who met his wife, Terri, in Pittsburgh back in 1998 when she worked for a contractor on the landside of the airport. 
 Back then Weisser flew for Chautauqua Airlines and, until 9/11, for US Airways, and whenever he passed through PIT around lunchtime the couple would meet on the airside to have lunch together. 
 The couple was married a month after 9/11 and by then airside was off limits unless you had a ticket. Six months later I was laid off by US Airways, said Weisser, But we will always remember the early days of our relationship at PIT airport fondly. 
