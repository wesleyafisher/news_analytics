__HTTP_STATUS_CODE
200
__TITLE
USA and Chile Are Out, But These Latin American Countries Qualified for the World Cup
__AUTHORS
Darek Michael Wajda
__TIMESTAMP
Oct 12 2017, 4:52 pm ET
__ARTICLE
 Its been a nail-biting ride for soccer fans around the world this week. Coming down to the final stages of World Cup Qualifying for Russia 2018, many teams needed a win to make it to the most prestigious soccer tournament in the world. The United States and Chile were sent home early after failing to obtain the points needed to attend the competition. 
 In the Americas, teams in the CONMEBOL and CONCACAF groups battled it out to send their countries to Russia for a chance to win the highest honor in the soccer world. Brazil, Uruguay, Mexico, and Costa Rica had already qualified, leaving the remaining qualifying positions in the groups open for grabs. 
 The United States kicked off on Tuesday night against Trinidad and Tobago, needing a tie to qualify for the World Cup. Unable to produce the results needed, the U.S. went down 2-0 early in the game and got one back early in the second half after young star Christian Pulisic sent his shot to the top of Trinidads net. 
Devastated. But we are forever grateful to our supporters. We will persevere. pic.twitter.com/9ATsjtE7o7
 While time started to run out for the U.S., Panama and Honduras netted goals, crushing any chance of qualification for the United States. Both Panama and Honduras won their games to pass the U.S. in the CONCACAF standings, taking the last two remaining spots for World Cup qualification. 
 We failed on the day. No excuses, said U.S. Coach Bruce Arena, who according to ESPN blamed the failure to qualify on himself. Arenas team is the first American group to not qualify for the World Cup since 1986. 
 Related: Soccer Fans Eagerly Await Start of FIFA Confederations Cup 2017 
 Panama broke out in a celebratory fashion after qualifying for its first ever World Cup, defeating Costa Rica 2-1 in Panama City. The Panamanian president declared the day after the game a national holiday after Roman Torres equalized just three minutes before the game finished, celebrating by taking his shirt off and running to the crowd. With open arms, he was welcomed as a new national hero. 
Panama player, pitch invader and police officer have a big embrace.This is what football's all about  pic.twitter.com/y6DkdNq9zp
 The president tweeted, La voz del Pueblo ha sido escuchada; celebra este da histrico para Panam. Maana ser Da de Fiesta Nacional. This means, "The voice of the people has been heard... Tomorrow will be a national holiday." 
On the other side of the #USMNTs story: Panamas first World Cup berth ever. The emotional home call is everything pic.twitter.com/njW8OJc0MU
 Honduras took on Mexico for the third match of the CONCACAF group. Winning 3-2, the Hondurans fought until the last minute against the already-qualified Mexican team. After the final whistle blew, the home crowd was sent into a frenzy knowing that Honduras would qualify as fourth in the group. 
 In the CONMEBOL group, Brazil was in top shape and had already qualified for the World Cup. Brazil stars Neymar and Gabriel Jesus took the field as they were ready to win another important game for their country. Argentina, on the other hand, was looking for its national hero Lionel Messi to save their World Cup dreams after starting the night in sixth place. 
Hora de dormir imaginando esse time na Rssia em 2018!  #PartiuRussia #GigantesPorNatureza pic.twitter.com/ct6DXKsWBq
 In So Paulo, Brazil took on 2016 Copa America Winners Chile. Needing a win to qualify, Chile came out with guns blazing but was unable to crack the Brazilian defense. Brazil netted all three of its goals in the second half, breaking the hearts of the Chilean faithful and destroying the land of poets dreams of attending the 2018 World Cup. The game finished 3-0 in favor of the Brazilians. 
 Over in Quito, Messi and Argentina needed a win to qualify. After a horrific start to the game, Argentina was down 1-0 after a minute into the game. Feeling the pressure, the Argentinians looked to their hero and captain Lionel Messi for a response. Just over ten minutes later, Messi responded with his first goal of the night. A man on a mission. 
 Less than 10 minutes, later Messi scored again, sending the ball to the top left corner of the net and putting Argentina in the lead, 2-1. But Messi was not finished yet. In the 62nd minute of play, Messi took on the Ecuadorian defense with another stunner. 3-1 Argentina, and Messi would finish the game with a hat-trick knowing he sent his country to the 2018 World Cup. 
A photo posted by NBC News (@nbcnews)
 In the next big headliner, Colombia took on Peru. Both would advance with a win or a draw. Colombia went up 1-0 after James Rodriguez sliding the ball past the Peruvian goalie into the back of the net. In the 74th minute, the Peruvians were awarded an indirect free-kick 20 yards from the Colombian goal. As the rules read, an indirect free-kick must be touched by any field player before entering the net. 
 Related: U.S., Mexico and Canada to Make Historic Joint Bid for 2026 World Cup 
 Gambling with his luck, Perus leading goal scorer Paolo Guerrero hit the ball without anyone touching it. Heading for the back of the net, Colombian goalkeeper David Ospina needed to just watch it go in and the goal would have been disallowed. Forgetting the rules, or simply not knowing, Ospina reacted to the free-kick, attempting to save the ball and touching it before it hit the net.  
A photo posted by NBC News (@nbcnews)
 With all said and done, joining next years 2018 FIFA World Cup in Russia are Brazil, Uruguay, Argentina and Colombia from CONMEBOL. From the CONCACAF group, Mexico, Panama and Costa Rica will be making the trip to Russia. Honduras and Peru will be facing off against Australia and New Zealand, respectively, in the playoffs qualifying game. 
 Follow NBC Latino on Facebook, Twitter and Instagram. 
