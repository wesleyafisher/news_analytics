__HTTP_STATUS_CODE
200
__TITLE
Australia Christmas Terror Plot Foiled, Police Say
__AUTHORS
Phil Helsel
__TIMESTAMP
Dec 23 2016, 8:15 am ET
__ARTICLE
 Australian police say theyve foiled a plot to bomb parts of Melbourne on Christmas Day in a planned attack inspired by the terror group ISIS. 
 "As a result of this investigation we believe we have neutralized that threat," Victoria Police Chief Commissioner Graham Ashton said at a news conference Friday morning local time. 
 Seven people were arrested in overnight raids in northwest Melbourne, but police said two of those were later released without charges. 
 The plot involved explosives and possibly knives or guns, against possible targets in the heart of the city of 4 million, including the area near Federation Square, Flinders Street Station and St. Pauls Cathedral, Ashton said. 
 Police said they found "the makings of an improvised explosive device" in raids. The plot also involved a "surveillance mission" of the area that was to be attacked, Ashton said. 
 Australian Prime Minister Malcolm Turnbull called it "one of the most substantial terrorist plots that have been disrupted over the last several years" and vowed that the nation would not be cowed by the threat of terrorism. 
 Cities around the world are on alert after a driver plowed a truck into a Christmas market in Berlin Monday in an attack that left 12 people dead and nearly 50 injured. The suspect was killed in an early morning shootout in Milan, Italy.  
 The five suspects in the alleged Australian plot are men between the ages of 21 and 26. All are Australian citizens, and four of the five were born in the country and the fifth was born in Egypt, Ashton said. He said four of the five were of Lebanese descent. 
 Investigators believe the suspects were "self-radicalized but certainly inspired by ISIS and ISIS propaganda," Ashton said. Four of the five men are accused of undertaking preparations for planning a terrorist act, police said. 
 The suspects "have been persons of interest for Victoria police and intelligence agencies now for some period of time but interest accelerated over the last two weeks and an investigation was launched, Ashton said. 
 If the alleged plot had been carried out as planned, "this would have been a significant attack, we believe," Ashton said. "Theres no doubt about that." 
 Victoria Premier Daniel Andrews denounced the plot as a crime rather than an expression of religion, and hailed the state's multiculturalism. 
 "What was planned here, what it will be alleged was going to occur if not for the professionalism and hard work of our police, were not acts of faith," he said. "They were, in their planning, acts of evil." 
 Australia raised its terrorism threat level to "probable" in 2014 in response to threats posed by ISIS. Since then 57 people have been charged in 27 counter-terror operations in the country, Turnbull said. 
