__HTTP_STATUS_CODE
200
__TITLE
Viral Photos of Drug Overdose Victims Expose Pitfalls in Fighting Addiction
__AUTHORS
Jon Schuppe
__TIMESTAMP
Nov 12 2016, 2:58 pm ET
__ARTICLE
 The images are both repellent and captivating: men and women, often with children in their care, passed out from drug overdoses in near-death moments captured just after cops arrive at the scene. 
 In another era, they might have only appeared in tabloid newspapers, like the work of street photographer Weegee. Instead, they catch fire first on social media, rocketing across the internet and turning what would have been an anonymous, small-town tragedy into a public spectacle. 
 Related: What Happens When a Drug Overdose Photo Goes Viral 
 Police chiefs who've distributed the photos in recent months  in Ohio, Massachusetts and Indiana  say they had no intention of making the images go viral. Their sole goal, they say, was to get residents of their own communities to recognize that they were not immune from America's opioid epidemic. 
 But the photos have opened a new front in the drug battle, with addiction experts and some criminal justice researchers saying the photos violate people's privacy and fuel the shame and stigma that keeps addicts from seeking help  and push the problem into the shadows. 
 "That person is not volunteering to be the spokesperson," said Levele Pointer, a recovering heroin addict from New York who counsels addicts and policymakers on ways to curb drug use by treating it as a medical problem, rather than one that requires punishment. "You're throwing that person out there and may cause more harm than good." 
 The distribution of the photos, Pointer said, reflects and perpetuates misunderstandings of how addiction works. 
 Related: Education or Humiliation? What Happens When a Drug Overdose Photo Goes Viral 
 "I'm not aware of anyone who says, 'I want to be a drug addict,'" Pointer said. "This is not a conscious choice that anyone has made. It's something that has happened and they're trying to get over it. The addiction sometimes creeps up on us, and we're not in control in that moment. It's like having an out-of-body experience. The whole time you're feeling total regret." 
 That, he said, helps to explain what to many seems unforgivable: people getting high around children. 
 "Sometimes, there's nowhere else to take the kid, so they try to do it in the safest place," Pointer said. 
 Lindsey LaSalle, a senior staff attorney for the Drug Policy Alliance, said she understands the desire among police to spread word about the crisis. But she said there are more effective ways to do it: collecting and sharing data on overdose deaths, and finding addicts and families willing to talk publicly about their ordeals. 
 "It facilitates a conversation that can lead to opportunities to create policies that would positively impact the issue," LaSalle said. "That includes interventions that have evidence behind them and can save lives." 
 Jim Bueermann, president of the Police Foundation, said the departments that distribute overdose photos "are coming from the right place, and have good intentions, and are trying to get people to understand how serious this is." 
 Such a tactic can be "a good way to reach people who don't believe that's something to care about in their community," he added. 
 But police must also anticipate how the community might react to the photos, Bueermann said. If there is a relative lack of sensitivity among locals, then police risk fueling backlashes against the overdose victims, in which people call for them to go to jail and for their children to be taken away from them. 
 "You can show people a photo and make them believe it's a problem in their community, but it needs to be paired with a message of what you're trying to achieve," Bueermann said. 
 Jim O'Neill, the police chief of Reynoldsburg, Ohio, said his message in releasing such photos is clear: "We want to raise awareness and make sure people understand that this kind of thing is around." 
 Last week, his department posted a picture on Facebook of a man passed out at the wheel of a car. Some praised the move, while others accused the police of using shaming tactics. O'Neill said he stands by the move, and believes it served its purpose. 
 Police gave a similar explanation in Hope, Indiana, after they distributed a picture in late October of a young woman passed out in a car with her 10-month-old son in the back. Her mother said she received hate messages. The police chief said he was surprised by the backlash, but did not regret the photo. 
 A month earlier, police in East Liverpool, Ohio, posted on Facebook photos of a woman and her friend unconscious in a car with her 4-year-old grandson in the back. The story went viral, and the woman's sister accused police of humiliating her and the boy. A city official was unapologetic, saying the public needed to see the damage heroin does to families. 
 Also in September, police in Lawrence, Massachusetts, gave the media copies of video showing a mother passed out in a Family Dollar store while her young daughter tried to wake her. 
 The police chief told a local NBC affiliate that the footage was "evidence that shows what addiction can do to someone." The woman later came forward to explain her battle with addiction, her struggle to get treatment, and the unwanted publicity and shame the video had caused. 
 In each of the cases, the overdose victim was revived. In some, they were also charged with crimes. Some were directed into drug treatment as well. 
 But all now live with their darkest moment recirculating in perpetuity online. 
 "It's an image of people at the absolute worst moment of their lives, and in an era of social media, it will potentially haunt them forever," said Daniel Raymond, policy director at the Harm Reduction Coalition in New York. 
 "If they get on their feet and complete treatment and try to seek new employment, find a job or find housing, they're always going to be wondering, 'Does this person already recognize me and have this image of me in their mind?'" Raymond said. "It means the moment is frozen in time  without their consent or permission." 
 That is why the Police Executive Research Forum, which advises law enforcement agencies on emerging criminal justice issues, recommends against distributing overdose images  for the same reasons it urges caution on the distribution of body cam footage. 
 "Generally speaking, most police departments have a policy of not showing victims of car accidents, or of crime victims. So why would we want to show pictures of those persons who, as the result of an addiction, are at their most vulnerable?" said Chuck Wexler, the organization's executive director. 
 He added: "It just doesn't seem the right thing to do." 
