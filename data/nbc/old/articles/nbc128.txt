__HTTP_STATUS_CODE
200
__TITLE
Trump Will Look Into His Drug Czar Nominee Following Report
__AUTHORS
Ali Vitali
Corky Siemaszko
__TIMESTAMP
Oct 17 2017, 7:15 pm ET
__ARTICLE
 WASHINGTON  President Donald Trump said Monday that he would "look into" a report that his pick for drug czar championed a bill that effectively handcuffed federal agents from going after the Big Pharma firms that flooded the country with addictive opioids. 
 Trump weighed in on Rep. Tom Marino, R-Pa., his nominee to head the Office of National Drug Control Policy, following an expose by The Washington Post and CBS' 60 Minutes that revealed Marinos role in pushing through the drug industry-backed Ensuring Patient Access and Effective Drug Enforcement Act. 
 Marino was a "very early supporter of mine," Trump said during a wide-ranging question and answer session with reporters in the Rose Garden. 
 Trump also said that he would formalize his Aug. 10 national emergency declaration by signing it and sending it to Congress this week. 
 Related: Opioid Addiction is a Mental Health Crisis, Not a Crime Wave 
 That would enable the executive branch to direct millions of federal dollars toward things like expanding drug treatment facilities and supplying police officers with the anti-overdose remedy naloxone. 
 "I want to get that absolutely right," Trump said. "This country, and frankly the world, has a drug problem. The whole world has a drug problem." 
 Earlier, West Virginia Sen. Joe Manchin, whose state has been among the hardest hit by a deadly plague of overdoses that has killed tens of thousands of Americans, demanded that Trump shelve Marino's nomination. He said the legislation that Marino helped push through Congress "has tied the hands" of the federal Drug Enforcement Administration. 
 "The head of this office, often called Americas Drug Czar, is a key voice in helping to push and implement strategies to prevent drug abuse, stop drug trafficking, and promote access to substance use disorder treatment," Manchin, a Democrat in a pro-Trump state, wrote. 
 Related: A Badge Was No Protection From Addiction 
 Marino's support of this legislation calls into question his "ability to fill this critical role in a manner that will serve the American people and end the epidemic," Manchin wrote. "Congressman Marino no longer has my trust or that of the public that he will aggressively pursue the fight against opioid abuse." 
 An early Trump supporter, Marino has not yet responded to the findings in the report. The legislation he went to bat for was spearheaded by the drug industry-funded Healthcare Distribution Management Association, which spent more than $106 million to lobby for the bill, according to the report. 
 Big Pharma pitched the bill as a way to prevent painkillers from falling into the wrong hands while protecting reputable pharmacists and drug distributors. But what it actually did, according to the report, was defang the DEA by curbing their power to stop drug distributors from sending millions of opioids to doctors and pharmacies suspected of supplying addicts. 
 Marino's bill gained steam when the Department of Justice named Chuck Rosenberg to head the DEA, an agency that has long-opposed loosening restrictions on the drug companies. 
 "Rosenberg wanted to paint a new face on the DEA for the Hill," Regina LaBelle, the drug control office's chief of staff at the time, said in the report. "He wanted to show them the softer side of the DEA, and he wanted to work with industry." 
 The bill was passed by Congress through unanimous consent in 2016 after Sen. Orrin G. Hatch, R-Utah, negotiated a final version with the DEA. It was later signed by President Barack Obama. 
 "We deferred to the DEA, as is common practice," Michael Botticelli, who was the White House drug czar under Obama, said in the report. 
 Rep. Marsha Blackburn, R-Tenn., who got $120,000 in campaign contributions from the pharmaceutical industry, was a co-sponsor of the bill. Her state has also been hit hard by the opioid crisis. 
 In a statement, a Blackburn spokesperson told The Tennessean that "if there are any unintended consequences" from the legislation "they should be addressed immediately." 
 Rep. Judy Chu, a California Democrat who also co-sponsored the bill, said Rosenberg assured her "the bill would not negatively impact their work." 
 Related: Death Came to This City in a Yellow Pill 
 Meanwhile, Sen. Claire McCaskill, D-Mo., announced she would introduce legislation that would repeal the law Marino championed, saying it has "significantly affected the government's ability to crack down on opioid distributors that are failing to meet their obligations and endangering our communities." 
