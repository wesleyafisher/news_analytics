__HTTP_STATUS_CODE
200
__TITLE
How Heroin Flows Over the Border and Into Suburbia
__AUTHORS
__TIMESTAMP
Sep 6 2014, 12:08 pm ET
__ARTICLE
 NOGALES, Ariz.  Agents with U.S. Customs and Border Protection (CBP) here are increasingly on the hunt for a nefarious drug wreaking havoc across the U.S., inspecting cars top to bottom as smugglers try to bring heroin across the border. 
 DEA figures show a four-fold increase in heroin seizures along the southwest border since 2008, with 4,653 pounds being confiscated last year. 
 Most of it was hidden in cars and trucks driven across the nearly 2,000-mile-long border in California, Arizona, New Mexico and Texas. 
 You can hide the heroin anywhere from bumper to bumper, roof to floor and anywhere in between, said CBP Port Director Lupe Ramirez. 
 Increasingly, agents are also seeing heroin being hand-carried from Mexico in backpacks across rugged deserts and remote American ranchland. 
 We have had heroin strapped to a child, heroin strapped to a grandmother, heroin in a stroller, heroin in a car seat, Ramirez said. 
 Officials say almost all the heroin in the United States is made from opium poppies grown and processed in labs either in Mexico or Colombia, South America, and smuggled across the U.S. land border. 
 A 2013 government report called the National Drug Threat Assessment Summary said: The increase in Southwest Border seizures appears to correspond with increasing levels of production of Mexican heroin and expansion of Mexican heroin traffickers into new U.S. markets. 
 In Tucson, Arizona, just an hours drive from the border, treatment specialists and recovering addicts say heroin is more easily found on the streets now than in past years. 
 The availability of heroin is increasing all the time. No one comes in here saying they cannot find heroin, said Dr. Lenn Ditmanson, of Cope Community Services, which treats opiate dependency. Its a very difficult clinical medical problem to be cured of. You need treatment, he said. 
 The Substance Abuse and Mental Health Services Administration, known as SAMHSA, reports a steady increase in heroin abuse nationwide, with an estimated 669,000 American users in 2012. 
 For years, Chicago has been one of the cartel's top heroin distribution centers, but officials say its gotten even worse. 
 Ive never seen it this bad, said Jack Riley, a veteran Special Agent in Charge of the DEA Chicago Field Division. Ive seen what it does to families, communities, educational settings, healthcare. Its enormous. 
 Almost all the heroin in Chicago is of the form produced in Mexico, not Colombia, he said. 
 Todays heroin is being trafficked primarily by Mexican organized crime, probably the most vicious, well-financed, criminal entities weve ever known, he said, referring to the powerful Sinaloa cartel. 
 Most of the heroin bound for Chicago crosses the border in the El Paso area, Riley said, and is then placed into the hands of local gang members for distribution. In Chicago, weve got 100,000 documented street gang members who largely make their living putting Mexican cartel heroin on the street. 
 Riley and area health-care providers insist theres a direct link between the rise in heroin use and the widespread abuse of prescription painkillers in the United States. As authorities crack down on opiate pills, heroin  which is much cheaper and more powerful  fills the void, they say. 
 What scares me the most is the Mexican organized crime, the cartels, are geniuses at marketing, Riley said. They understand the addiction problem in this country is caused by prescription drugs, and that is exactly why the number one drug of choice for the street gangs and the cartels is heroin, because theyre linked. 
 Much of the heroin available now makes its way into the hands of teenagers and young adults living in affluent suburbs, authorities say. 
 Weve seen a number of deaths and overdoses among young people  people who are naive about heroin, said Gil Kerlikowske, the Commissioner of U.S. Customs and Border Protection. 
 Addicts from outlying areas will often drive on Highway 290, known locally as the Heroin Highway, to buy heroin on Chicagos West Side. 
 But a 24-year-old recovering addict, who chose to be identified only by his first name, Wes, said heroin is so readily available that there is no need to risk the long trip. 
 I got it from the suburbs. I never had to travel very far, he said. It was within a 10- to 15-minute drive, and I have my fix. 
 Dr. Thomas Wright, the chief medical officer at the Rosecrance Health Network, which offers substance abuse treatment, worries about the growing number of young American heroin addicts. 
 Were losing a part of a generation, an important part of a generation that can contribute a lot to society, Wright said. 
 Its also a major concern for authorities who established the Chicago Strike Force to coordinate the efforts of federal, state and local law enforcement. Their goal of disrupting the connections between the Mexican heroin suppliers and local street distributors has already led to the arrests of numerous alleged gang leaders in the region. 
 This is an American problem, and its not going away, said Riley. And for people to think that its not in their community, its not in their town, theyre immune to it? Youre living in a fantasy world. 
 
