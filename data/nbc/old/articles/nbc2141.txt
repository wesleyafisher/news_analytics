__HTTP_STATUS_CODE
200
__TITLE
MIT Sophomore Designs Winter Sleeping Bags for Syrian Refugees
__AUTHORS
Associated Press
__TIMESTAMP
Oct 15 2017, 2:53 pm ET
__ARTICLE
 CAMBRIDGE, Mass.  It wasn't enough to send warm wishes to refugees in Syria. Vick Liu wanted to send them actual warmth. 
 The sophomore at the Massachusetts Institute of Technology is creating a new line of sleeping bags designed for refugees who have few other options to keep warm during harsh winters in the Middle East. An avid backpacker in his youth, Liu came up with the idea last year after reading about Syrian families who were struggling to survive freezing temperatures after fleeing the country's civil war. 
 "The only way for them to create heat is through fire and through blankets," said Liu, a 19-year-old finance and political science student. "It's tough to stay warm at 15 degrees Fahrenheit with a couple blankets." 
 Freezing temperatures in Syria and surrounding countries have been blamed for causing hypothermia and some refugee deaths in recent years. The United Nations says up to 4 million refugees in the Middle East face "extreme risk" this winter, but that only a quarter are expected to get assistance preparing for the cold. 
 To help, Liu and a team of five classmates recently raised $17,000 to manufacture 250 bedrolls and send them to resettlement areas in northwest Syria. They'll be distributed in December by Nu Day Syria, a nonprofit group based in New Hampshire that provides medical supplies and everyday items to refugees in Syria. 
 The group partnered with Liu after hearing from families who feared a repeat of last year's winter, one of the worst in recent history. Workers say even a sleeping bag can make a major difference for refugees who had to flee home without warm clothing and who can't afford fuel for gas heaters. 
 "We have 8-year-old children saying, 'I don't want my brother to die,'" said Huda Alawa, grants and logistics coordinator for the group. "It's a very tangible fear because it's something they've seen happen already." 
 The project joins other efforts to help refugees through the winter, including programs by the U.N. and other nonprofits that distribute blankets and warm clothing. 
 Liu's work began last year in his dorm room, where the Los Angeles native crafted a prototype using a sewing machine and materials stashed under his bed. 
 His final product is called the TravlerPack, a lightweight sleeping bag that's filled with duck down insulation and can handle temperatures as low as 15 degrees. Each one costs about $50 to make and distribute. 
 Some of the design is based on Liu's experience as an Eagle Scout who grew up backpacking in cold conditions. But other features were suggested by Syrian refugees Liu met through a friend, including a waterproof pocket for travel documents and a shoulder strap for portability. Multiple bags can be zipped together to create a larger blanket for families. 
 "I made it a point not to assume what their needs were, but to go out and find out," said Liu, who tested one of his early models by zipping into it overnight during a Boston snowstorm. 
 After the first sleeping bags are delivered to Syria, Liu's team aims to send another 1,000 to refugee camps in Lebanon and Jordan, where fires for warmth can be a deadly hazard. The students are now trying to raise $50,000 for that effort. Once that's finished, Liu plans to turn the project into a nonprofit group and look for other refugees in need. 
 "At the end of the day, we didn't start this to make money. We didn't start this to get a ton of prestige," he said. "We just wanted to help people." 
