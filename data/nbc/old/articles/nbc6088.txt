__HTTP_STATUS_CODE
200
__TITLE
Samsung Unveils New Tablets Before the Start of Mobile World Congress
__AUTHORS
Alyssa Newcomb
__TIMESTAMP
Feb 26 2017, 1:46 pm ET
__ARTICLE
 Samsung unveiled two new tablets on Sunday before the start of the Mobile World Congress in Barcelona, where the company, as expected, broke from tradition and did not release a flagship smartphone. 
 The Sunday press conference is an annual event for Samsung, with the time slot usually reserved for introducing the latest Galaxy smartphones. 
 Related: Samsung Finally Explains the Galaxy Note 7 Exploding Battery Mess 
 However, after two Samsung Galaxy Note7 recalls, resulting in the smartphone being pulled from the market, the company appears to be taking their time before releasing the next generation of smartphones. 
 Patrick Moorhead, principal analyst at Moor Insights & Strategy, told NBC News this decision could be based on two big reasons. 
 "First, Samsung expended many resources to find the root cause of the Note7 issues and change their processes to assure it wont happen again. Secondly, the expectation is that Apple will really extend themselves to make the next iPhone dramatically different, so Samsung benefits from the extra time," Moorhead said. 
 While they might not seem as exciting as new smartphones, Samsung's two new tablets are spruced up with a number of standout features for people looking to invest in a 2-in-1 device. The new offerings all boast fast charging, allowing users to optimize the amount of power they get in a short amount of time. 
 First up is the Galaxy Book, which comes in 10-inch and 12-inch options. 
 The tablet feels similar to Microsoft's Surface Book in that it can easily go from being a tablet to snapping into a separately sold keyboard and becoming a laptop computer. 
 Samsung is also bringing back its rubber-tipped S-Pen, which has 4,096 levels of pressure. 
 NBC News got an advance look at the new tablets ahead of Mobile World Congress, and using the S-pen on the screen was a standout feature and felt as though you were writing on paper. 
 The other new tablet, the Galaxy Tab S3, has a 9.7-inch display and packs quad speakers. Battery consumption is optimized for gamers, making it an ideal tablet to take on a long flight without having to worry about charging. 
 The Galaxy Book runs Windows 10 and the Galaxy Tab S3 runs Android 7.0. Both are able to easily communicate with the Android ecosystem thanks to Samsung Flow, a new feature allowing your devices to talk to each other. 
 You'll be able to receive text message notifications on your tablet and easily reply, without having to pick up your phone. Samsung is also planning to integrate third party apps, including WhatsApp, to make the experience a huge value add for users. 
 While the secret is now out, Samsung hasn't yet revealed when the new tablets will be available or the pricing. However, a source said it's expected the pricing will be competitive with other tablets. 
