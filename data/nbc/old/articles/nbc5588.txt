__HTTP_STATUS_CODE
200
__TITLE
Veteran Chef Roshara Sanders Mixes Food With Service
__AUTHORS
Mashaun D. Simon
__TIMESTAMP
Oct 15 2017, 9:57 pm ET
__ARTICLE
 Roshara Sanders is pairing her passion for food with her commitment for service through a new relationship with Habitat for Humanity. 
 For Sanders, food is life. 
 It was a declaration she made in 2016 as part of the inaugural class of NBCBLK28  in which NBCBLK recognized 28 trailblazers under the age of 28. 
 She cooks, she told NBC News, because food is essential  "You can't live without food." 
 Now, Sanders has signed on to be a celebrity ambassador for Habitat for Humanity. Through much of the summer and early fall, she has raised money for Habitat's Global Village initiative. The initiative helps volunteers travel internationally to build homes and to advocate for or work alongside residents in disaster recovery or care. 
 "Giving back has always been important to me," Sanders told NBC News. "I'm an Army veteran, and just because I'm out of the military doesn't mean I shouldn't give back to my country. So I think Habitat for Humanity is the best way to give back." 
 Sanders hosted an exclusive dinner in Greenville, South Carolina, in July to launch her partnership with Habitat. The event featured a custom menu African continent inspired dishes by Sanders. 
 The relationship with Habitat is a personal one for Sanders. She grew up in a Habitat home. 
 "I know firsthand the difference Habitat makes in people's lives," she said, adding that it's now her turn to give back. "A powerful way to be selfless is to give your time doing something you know will change someone's life forever. I want to service the world through food and let food be the outlet to inspire the mind, heart and soul." 
 NBCBLK28: Roshara Sanders: The View From Chef's Table 
 Habitat International is sending Sanders to Malawi and Madagascar in November. She''ll be building orphanages for children suffering from HIV/AIDS while sharing in communal meals. 
 Sanders joined the military in 2007. Six years later, she embarked on her dream of becoming a chef. A few years later, she would become a "Chopped" champion, winning the Food Network competition "Chopped: Military Vets." She was named the 2017 Faces of Diversity Award winner by the National Restaurant Association Educational Foundation. 
A photo posted by NBC News (@nbcnews)
 She hopes, she said, to be able to bring much of the knowledge she acquires while on her mission trip back to the United States, especially some of the cuisines. 
 "I can't wait to see Malawi and get my hands dirty and do good work on behalf of such a prestigious organization," she added. "Habitat International has a tremendous impact on communities across the globe that need the most basic things we take for granted here. I am just humbled to be able to travel to Africa for them and do my part." 
 Follow NBCBLK on Facebook, Twitter and Instagram  
