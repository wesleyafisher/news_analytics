__HTTP_STATUS_CODE
200
__TITLE
Computer Chip Maker Asks Government to Block All U.S. iPhone Sales
__AUTHORS
Phil McCausland
__TIMESTAMP
Jul 6 2017, 7:01 pm ET
__ARTICLE
 A computer chip maker asked the U.S. government to ban new iPhone imports and all U.S. sales of the popular cellphone, according to a lawsuit filed Thursday. 
 Qualcomm claimed in a 177-page complaint filed in U.S. District Court in San Diego, where Qualcomm is based, and with the U.S. International Trade Commission that Apple had infringed on six of its patents that improve the iPhone's battery and performance. 
 It's now asking the giant California tech company to pay up. 
 "Apple continues to use Qualcomm's technology while refusing to pay for it," Don Rosenberg, executive vice president and general counsel of Qualcomm, said in a statement. "These lawsuits seek to stop Apple's infringement of six of our patented technologies." 
 Qualcomm alleges that its patents, which it claims have been issued in the past four years, lie "at the heart of every iPhone." 
 In the complaint, Qualcomm said it asked for the ITC to investigate and block Apple's imports of iPhones. The company is also "seeking a Cease and Desist Order" that would keep Apple from selling and advertising of products it has already imported. 
 It is unclear how many iPhones that would keep from the market. 
 According to CNBC, Apple sued Qualcomm for about $1 billion in January, alleging that the chip maker had charged royalties for technology that they had not had a hand in. 
 When NBC News asked for a comment, Apple reiterated its previous statements. 
 "Qualcomm's illegal business practices are harming Apple and the entire industry," Apple said in a June 20 statement. "They supply us with a single connectivity component, but for years have been demanding a percentage of the total cost of our products  effectively taxing Apple's innovation." 
 Apple Chief Execuitve Tim Cook alleged during a quarterly earnings call soon after the suit was filed that Qualcomm charges Apple higher licensing fees for wireless chips than it does other companies. 
 "And so we don't think that's right, and so we're taking a principled stand on it," Cook said in the second quarterly earnings sales call. 
 Qualcomm, meanwhile, said in its statement that it believes an investigation into Apple's use of its patent will begin next month and that a case will be tried in 2018. 
