__HTTP_STATUS_CODE
200
__TITLE
Donald Trump Has History of Contradictory Statements on Nuclear Weapons
__AUTHORS
Andrew Rafferty
__TIMESTAMP
Oct 11 2017, 12:47 pm ET
__ARTICLE
 WASHINGTON  President Donald Trump's suggestion that the U.S. drastically increase its nuclear arsenal follows a presidential campaign in which he made a number of contradictory statements about weapons of mass destruction. 
 As a candidate, he called nuclear proliferation the single biggest threat facing the world while also suggesting Japan and South Korea should obtain nuclear weapons as a defense. During one debate he ruled out a first strike but in the same breath said he would not take anything off the table. 
 Related: Trump Wanted Tenfold Increase in Nuclear Arsenal, Surprising Military 
 His desire to increase the country's nuclear capabilities nearly tenfold, voiced during a meeting with top national security leaders in July, came as North Korea continued to escalate nuclear tensions with more weapons tests. 
 As a candidate and as president, Trump has been fairly consistent in calling for the modernization of the country's nuclear weapons. 
 Heres how Trump has talked about nuclear weapons since launching his presidential run and entering the White House. 
 Trump Claims to Have Ordered the Modernization of the Countrys Nuclear Weapons 
Tweet Id not present
Tweet Id not present
 As a Candidate, He Criticized the Countrys Nuclear Arsenal as Outdated 
Tweet Id not present
 Trump Has Given A Variety of Answers on Using Nuclear Weapons 
"I dont want to rule out anything. I will be the last to use nuclear weapons. Its a horror to use nuclear weapons. The power of weaponry today is the single greatest problem that our world has. -- TODAY, April 28, 2016
"Well, it is an absolute last stance. And, you know, I use the word unpredictable. You want to be unpredictable." -- Interview on CBS' "Face The Nation," Jan. 3, 2016
 He Has Called Nuclear Proliferation the "Greatest Threat" Facing the U.S. 
 But He Has Also Suggested Japan, South Korea and Even Saudi Arabia Should Have Them 
CNN's WOLF BLITZER: But  but youre ready to let Japan and South Korea become nuclear powers?
TRUMP: I am prepared to  if theyre not going to take care of us properly, we cannot afford to be the military and the police for the world. We are, right now, the police for the entire world. We are policing the entire world.
You know, when people look at our military and they say, Oh, wow, thats fantastic, they have many, many times  you know, we spend many times what any other country spends on the military. But its not really for us. Were defending other countries.
So all Im saying is this: They have to pay.
And you know what? Im prepared to walk, and if they have to defend themselves against North Korea, where you have a maniac over there, in my opinion, if they dont  if they dont take care of us properly, if they dont respect us enough to take care of us properly, then you know whats going to have to happen, Wolf?
Its very simple. Theyre going to have to defend themselves.
 -- Interview on CNN, May, 4, 2016 
CNN's ANDERSON COOPER: So you have no problem with Japan and South Korea having nuclear weapons?
TRUMP: At some point we have to say, you know what, we're better off if Japan protects itself against this maniac in North Korea, we're better off, frankly, if South Korea is going to start to protect itself, we have ...
COOPER: Saudi Arabia, nuclear weapons?
TRUMP: Saudi Arabia, absolutely.
COOPER: You would be fine with them having nuclear weapons?
TRUMP: No, not nuclear weapons, but they have to protect themselves or they have to pay us.
Here's the thing, with Japan, they have to pay us or we have to let them protect themselves.
COOPER: So if you said, Japan, yes, it's fine, you get nuclear weapons, South Korea, you as well, and Saudi Arabia says we want them, too?
TRUMP: Can I be honest with you? It's going to happen anyway. It's going to happen anyway. It's only a question of time. They're going to start having them or we have to get rid of them entirely. But you have so many countries already, China, Pakistan, you have so many countries, Russia, you have so many countries right now that have them.
Now, wouldn't you rather in a certain sense have Japan have nuclear weapons when North Korea has nuclear weapons? And they do have them. They absolutely have them. They can't  they have no carrier system yet but they will very soon.
Wouldn't you rather have Japan, perhaps, they're over there, they're very close, they're very fearful of North Korea, and we're supposed to protect.
 -- CNN Town Hall, March 29, 2016 
 Trump Even Said He Would Not Take Using a Nuclear Bomb in Europe Off the Table 
