__HTTP_STATUS_CODE
200
__TITLE
Theres an App for That: How Tech Became a Lifeline for Harvey and Irma
__AUTHORS
Alyssa Newcomb
__TIMESTAMP
Sep 8 2017, 1:53 pm ET
__ARTICLE
 The last time Florida had a major hurricane, apps weren't even invented. 
 But as Floridians brace for Hurricane Irma to make landfall this weekend, they're able to use a variety of apps as lifelines to stay up to date on conditions and connected to friends and family. 
 We've already seen the importance of Twitter and Facebook in the aftermath of Hurricane Harvey: With jammed phone lines, people needing to be rescued turned to social media to ask for help. 
 Related: Social Media Becomes a Savior in Hurricane Harvey Relief 
 But there is also a number of other apps that are at the ready to help people stay informed about everything from road closures and where to find gas to how your friends and family are doing. 
 As of Friday afternoon, the walkie talkie app was the most popular free app on Google Play and in Apple's App Store. 
 "We have seen a large number of people signing up for Zello in preparation for Hurricane Irma. Over 1 million people have joined in the last day, with most coming from Puerto Rico and Florida," Alexey Gavrilov, founder and chief technology officer, wrote in a blog post on Wednesday. 
 Zello works like an old-school walkie talkie, letting you press a button to speak to your contacts in real-time. It's also set up to handle group communication for as many as 2,000 people. 
 However, if there's absolutely no Wi-Fi or cellular service, Zello won't work. Gavrilov's post also warned that when the app is open, it will use a lot of battery power. This can be minimized by turning the screen off unless you're connected to a power source. If your battery dips below 30 percent, he recommends turning off Zello. If a contact messages, you'll still get a push notification. 
 The gas price comparison app has been a godsend for people desperately needing fuel before the hurricane makes landfall. 
 The free app has been keeping users updated on which gas stations still have gas and power as Floridians head north to escape the path of Irma. 
 As of Friday morning, the app showed gas shortages were continuing to get worse in South Florida. In Gainesville, more than 60 percent of stations were out of gas. In West Palm Beach and Miami/Fort Lauderdale, nearly half of city gas stations had no fuel. 
 "We estimate that GasBuddy has been downloaded approximately half a million times since Hurricane Harvey made landfall on August 25," Oliver Yeh, CEO of Sensor Tower, an app intelligence firm, told Mashable. This marked a 420 percent increase in downloads over the previous two weeks, he said. 
 Snap Maps had their first big test during Hurricane Harvey, and they turned out to be helpful for people needing to locate friends or stay up to date on local conditions. 
 Even if you're not in Irma's path, you can check out photos and videos from around town by opening Snapchat's camera screen. Pinch the screen with your thumb and index finger to be taken to the Snap Map. Zoom out until you see South Florida, where you can then click and explore the content people are sharing in the area. 
 For those trying to get out of the storm's path, Google Maps may provide some of the most up-to-date information on the best route to take. Florida Governor Rick Scott said the state is working with Google Maps to mark road closures in real-time. 
 The road closures will also show up on Google's Irma Crisis Map, which is embedded in an SOS alert accessible map anytime someone searches the keywords "Hurricane Irma." 
 Google Maps also has a feature that allows you to download the maps for use when you are offline. This can be done by clicking the three lines in the upper left corner, choosing "offline maps" and then signing in to your Google account. 
 Look for a "Stay Safe During the Hurricane" bundle of apps being featured in Apple's App store. Apple has handpicked a dozen apps people can use to track the storm and stay connected. Some old favorites are on the list, including Twitter and Facebook; Apple is also recommending Venmo and Squarecash for sending and receiving payments, Periscope for live streaming, and NOAA's weather radar app to stay updated on the storm. 
