__HTTP_STATUS_CODE
200
__TITLE
Battle for Raqqa: U.S.-Backed Forces Reclaim Syrian City From ISIS
__AUTHORS
NBC News
__TIMESTAMP
Oct 17 2017, 3:30 pm ET
__ARTICLE
A member of the Syrian Democratic Forces (SDF), an alliance of U.S. backed militias, holds up their flag in Raqqa on Oct. 17.
The SDF said Tuesday they had taken full control of Raqqa from ISIS, defeating the last jihadist holdouts in the de facto Syrian capital of their now-shattered "caliphate."
Kurdish fighters run across a street on July 3.
The SDF, a multi-ethnic alliance that includes Kurdish and Arab militias, has been fighting since June to take the city.
Raqqa was the first big city ISIS captured in early 2014, before its rapid series of victories in Iraq and Syria brought millions of people under the rule of its self-declared caliphate, which passed laws and issued passports and money.
A Syrian fighter takes his position inside a destroyed apartment on the front line in Raqqa on July 27.
ISIS has lost much of its territory in Syria and Iraq this year, including its most prized possession, Mosul. In Syria, it has been forced back into a strip of the Euphrates valley and surrounding desert.
Children flash victory signs as a military convoy carrying U.S.-made vehicles and arms headed for Raqqa passes through the northeastern city of Qamishli on Sept. 19.
Female members of the SDF stand on a street in Raqqa on Sept. 22.
Syrian forces carry their wounded comrade Ibrahim towards an armored vehicle after he was shot by a sniper in Raqqa on Sept. 24.
An SDF fighter comforts his comrade Ibrahim as they drive towards a medical center on Sept. 24.
Members of the Kurdish Red Crescent examine a girl displaced by fighting in Raqqa on Sept. 26.
Syrian forces battle to retake the National Hospital in Raqqa from ISIS on Sept. 28.
A Syrian fighter checks a building near the ISIS-held hospital on Oct. 1.
A Syrian fighter takes cover from sniper shots near the ISIS-held hospital on Oct. 1.
Young Syrian fighters joke with each other in Raqqa on Oct. 1.
A wounded Syrian fighter smokes a cigarette on Oct. 1.
A Syrian fighter takes a position in a house in Raqqa on Oct. 1.
Smoke rises after a coalition air strike against ISIS positions near the stadium in Raqqa on Oct. 4.
Syrian fighters hold an ISIS flag discovered in a building next to the stadium on Oct. 4.
A Syrian fighter listens to the radio inside a vehicle on Raqqa's eastern front line on Oct. 5.
A Syrian fighter fixes his hair using a broken mirror on Oct. 6.
Civilians gather after fleeing the city center on Oct. 12.
A bandaged woman looks back as civilians gather after fleeing the city center on Oct. 12.
Syrian fighters celebrate after declaring victory over ISIS in Raqqa on Oct. 17.
Related: Raqqa Recaptured From ISIS by U.S.-Backed Militias
