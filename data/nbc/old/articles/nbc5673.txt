__HTTP_STATUS_CODE
200
__TITLE
Harvey and Irma Walloped What Would Have Been a Strong Month for Jobs
__AUTHORS
Martha C. White
__TIMESTAMP
Oct 6 2017, 1:05 pm ET
__ARTICLE
 Had hurricanes Harvey and Irma not hit, September would likely have produced a jobs gain of around 190,000, experts said Friday after a dismal employment report that saw figures dip into negative territory for the first time in seven years. 
 The 33,000 jobs lost in September  in contrast to a gain of 90,000 expected by economists  are the direct result of Hurricane Harvey sweeping through the heart of Texass Gulf Coast oil industry and flooding Houston, and Hurricane Irma barreling north through Florida on a hard-to-predict course that prompted large-scale evacuations. 
 The Department of Labor says more than 11 million workers live in the affected areas, representing nearly 8 percent of the countrys jobs. Restaurants, which had been adding an average of 24,000 jobs per month, lost 105,000 in September. 
 We knew that the hurricanes were going to affect nonfarm payrolls to a certain extent, said Dan North, chief economist at Euler Hermes North America. Well expect to see a bump back for the next couple of months, he predicted, a function of people going back to work in affected areas, an expected increase in demand in some fields like construction and later revisions by the Bureau of Labor Statistics of its data. 
 Related: Hurricanes Harvey and Irma Wiped Out 33,000 Jobs Last Month 
 This months revision of July and August employment numbers trimmed the total number of jobs gained by 38,000 over the two months, with an upward revision in August unable to cancel out a larger downward revision for July. 
 Even a surprisingly high wage growth figure could have been affected by the storms, labor market experts said. 
 To see average hourly earnings up 2.9 percent certainly is welcome news indeed, said Mark Hamrick, Bankrate.coms senior economic analyst, but he added that the storms might be responsible for distorting the data. 
 We want to be cautious to make sure this is sustained, he said, given the high number of low-wage restaurant jobs that were shed last month. That could've had the temporary impact of skewing pay higher because lower-paid jobs werent present. 
 Other figures in the monthly report paint a more consistently positive picture. Economists say some of the disconnect stems from the fact that the Department of Labors monthly snapshot on jobs is gathered from two different surveys, with different methodologies and timelines for collecting data. 
 Sectors including health care and transportation actually saw job gains for the month. The overall unemployment rate fell from 4.4 percent to 4.2 percent, the labor force participation rate ticked up from 62.9 percent to 63.1 percent, and the employment-population ratio hit a post-recession high of 60.4 percent. 
 It looks like something about the tighter labor market is finally luring them back in, said Harry Holzer, a professor of public policy at Georgetown University. Whether it is the high chance of finding something, or the wages  it's hard to know from these data alone. 
 Im on Main Street and I see the work that our clients are doing  theyre all still hiring, said Tom Gimbel, founder and CEO of the staffing and recruiting firm LaSalle Network. My open job orders with clients is up in October and was up in September." 
 LinkedIn chief economist Guy Berger suggested that the improvements in labor force participation could be coming at the expense of more rapidly accelerating wages. Were discovering there still is this pool of people willing to work and keeping wage pressure down, he said. Job growth is moderate, but its fast enough the soak up the shadow supply of workers, he said, although he added it might be another year or more before this impact trickles down to workers paychecks. 
 Data on private payroll job growth from ADP and Moody's Analytics also supplied mixed messages. Although it showed a steep drop from the previous month, the 135,000 jobs added exceeded economists expectations. 
 This month is difficult to judge because of the area impacted, said Ahu Yildirmaz, vice president and head of the ADP Research Institute. Had Harvey and Irma simply not occurred, she estimated that September would have produced a gain of around 190,000. The storm impact may have cut 50,000 to 60,000 jobs, she said. 
 Taken together, experts say the overall picture is of an economy much the same as it was pre-Harvey and Irma. I think the report overall just confirms that the underlying trends in the U.S. economy remain intact, Hamrick said. 
 They suggest a lot of the trends weve been seeing for some time, which is the labor market recovery continues to broaden more, people are finding an incentive to come back into the labor force, said Sameer Samana, a global quantitative and technical strategist for the Wells Fargo Investment Institute. 
 The increased labor force participation rate in particular, he said, was a particularly good sign. Thats a huge positive in terms of labor market strength. 
