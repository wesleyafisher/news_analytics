__HTTP_STATUS_CODE
200
__TITLE
Drinking diet soda just makes you eat more
__AUTHORS
__TIMESTAMP
Jan 16 2014, 5:02 pm ET
__ARTICLE
 Don't depend on diet soda to help you lose weight. A new study shows that overweight and obese people who drink diet beverages consume more calories from food than heavy people who consume sugary drinks, according to a new Johns Hopkins study. 
 When you make that switch from a sugary beverage for a diet beverage, youre often not changing other things in your diet, says lead researcher Sara Bleich, associate professor in the department of health policy and management at Johns Hopkins Bloomberg School of Public Health. Bleich and other Johns Hopkins researchers used data from the 1999-2010 National Health and Nutrition Examination Survey. For this study, published Thursday in the American Journal of Public Health, they analyzed participants recollection of what theyd had to eat and drink over the past 24 hours. 
 They found that about one in five overweight or obese American adults regularly drinks diet beverages -- that includes soda and low-calorie juices, teas and the like -- which is about twice the amount that healthy-weight adults are drinking. On the one hand, thats encouraging. People are being told if you need to cut calories from your diet, discretionary beverages are a great place to start, Bleich says.  
 More From Today: How to Lose 10 Pounds in a Month 
 Diet soda consumption has increased steadily since 1965, when just 3 percent of Americans were regularly drinking the stuff, the study authors write. Sales of diet soda actually declined7 percent last year, but Bleich thinks that just means habitual diet soda drinkers are switching to the many flavored teas, juices and vitamin-enhanced waters currently on store shelves. 
 Our bodies fight to try to keep our weight stable, which is one of the reasons weight loss is so hard and it could help explain why overweight diet soda drinkers may be consuming more calories from solid food. 
 For slimmer people, the study suggests that diet drinks can contribute to weight maintenance, explains NBC News diet and health editor Madelyn Fernstrom. 
