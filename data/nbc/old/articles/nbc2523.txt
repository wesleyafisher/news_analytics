__HTTP_STATUS_CODE
200
__TITLE
Dangerously Addictive Painkiller Prescribed for Patients Who Shouldnt Have Received It, Says Whistleblower
__AUTHORS
Corky Siemaszko
__TIMESTAMP
Jun 5 2017, 2:35 am ET
__ARTICLE
 It's called Subsys and it's a painkiller 100 times more powerful than morphine that was approved by the FDA for cancer patients whose agony can't be relieved by other narcotics alone. 
 But despite the fact that what's known as "breakthrough cancer pain" is uncommon, Insys Therapeutics  the Arizona-based company that sells what can be a highly addictive drug, and nothing else  has sold almost a billion dollars worth of this medication in five years. 
 NBC's Senior Investigative Correspondent Cynthia McFadden spoke with a former employee who said she was part of a scheme to get the drug Subsys to patients who never should have had it. 
 "It was absolutely genius," Patty Nixon said of the alleged scheme. "It was wrong, but it was genius." 
 And Nixon would know. She's a former Insys sales rep turned whistleblower. 
 "What I did, I was instructed to do, I was trained to do," Nixon, who was fired by Insys after she says she felt guilty about lying on the job and stopped showing up for work. Shetold McFadden. "If I didn't do it, I was going to be in trouble." 
 The ingredient that gives Subsys so much kick is fentanyl, according to the company's website. 
 In addition to its strength, the other element that sets Subsys apart from other painkillers is the way it's used  it's a spray that is absorbed underneath the tongue, the website states. 
 Nixon said her job was to make sure Subsys got into the hands of as many patients as possible. 
 "My job responsibilities were to contact insurance companies on behalf of the patients and the doctors to get the medication approved and paid for by their insurance company," she told NBC. 
 Subsys is not cheap. A 30-day supply costs anywhere from $3,000 to $30,000. 
 Nixon told NBC that her supervisor told her ways to trick the insurers into believing it was "medically necessary." 
 "I would say, 'Hi, this is Patty. I'm calling from Dr. Smith's office. I'm calling to request prior authorization for a medication called Subsys,'" she told McFadden. Nixon says she would also mention oncology records that didn't exist and provide insurance companies with specific diagnosis codes, whether or not the patients had those conditions. 
 Was that a lie? "Absolutely," Nixon replied. "It was a complete bold-faced lie." 
 Sarah Fuller was one of the patients who was prescribed Subsys even though she didn't have cancer. 
 In her case, it was chronic neck and back pain from two car accidents. And when her doctor prescribed Subsys, an Insys sales rep was sitting in the room with them, her father Dave Fuller told NBC News. 
 Within a month, Fuller's prescription was tripled. And 14 months after she started using the drug, she was found dead on a bathroom floor. 
 What killed her? 
 "Well, technically fentanyl," Fuller's still-grieving mother said. "But a drug company who couldn't care less about a human life. And, apparently, a doctor who didn't either." Fuller's doctor has had her license temporarily suspended but denies responsibility for her death. 
 Sadly, Fuller is not alone. 
 FDA reports of adverse events, possible related complications, includes hundreds of deaths. 
 An attorney for Fuller's family who is suing, Richard Hollawell, said, "this is serious stuff that we're dealing with ... People need to finally be held accountable." 
 Nixon later testified before a federal grand jury that indicted the company's former CEO Michael Babich for fraud, conspiracy and racketeering. Five other former Insys executives have also been indicted for racketeering. All have pleaded not guilty. 
 Insys founder Dr. John Kapoor is not among the indicted. He is a billionaire who Forbes lists among the wealthiest Americans. 
 Prosecutors say the company paid hundreds of thousands of dollars to doctors in exchange for prescribing Subsys. Three top prescribers have already been convicted of taking bribes from Insys. 
 For its part, Insys has denied any responsibility and insists it shouldn't be blamed for how doctors prescribe their products. The corporation is not facing criminal charges and is still selling Subsys  some $240 million worth of Subsys just last year. 
 Meanwhile, Nixon says blowing the whistle on Insys has made her unable to find another job in her field. She said she continues to be racked with guilt over what she did as an Insys employee. 
 "I just wanna tell everybody out there who's been hurt, I am so sorry for any suffering or any pain  and for those families that visit their family members at the graveyard and for those family members that see their loved ones going through the pain of addiction," a weeping Nixon said, "I am sorry." 
 In a statement, Insys said: "The charges against individuals, including our former employees, discussed in your news story relate to previously disclosed investigations and litigation. Insys continues to cooperate with all relevant authorities in its ongoing investigations, including our federal investigation which began in and around December 2013. We are committed to complying with laws and regulations that govern the promotion of our products and all other business practices. We continue to emphasize ethical behavior within our organization and pursue opportunities to illustrate that our company's mission is to put patients first." 
