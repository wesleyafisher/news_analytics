__HTTP_STATUS_CODE
200
__TITLE
The Week in Pictures: Aug. 11 - 18
__AUTHORS
NBC News
__TIMESTAMP
Aug 18 2017, 8:00 pm ET
__ARTICLE
Colors of Spain
A soldier walks up the ramp of the Planalto Presidential Palace, illuminated in the colors of the Spanish flag to honor of the victims of the Barcelona attack, at dawn in Brasilia, Brazil, on Aug. 18.
Dred Scott Legacy
Workers use a crane to lift the monument dedicated to U.S. Supreme Court Chief Justice Roger Brooke Taney after it was was removed from outside the Maryland State House in Annapolis early on Aug. 18.
Taney wrote the 1857 Dred Scott decision that upheld slavery and denied citizenship to African Americans.
PHOTOS:A Look at the Confederate Statues Sparking Heated Debate
Bearing Torches
White nationalists rally around a statue of Thomas Jefferson on the University of Virginia campus in Charlottesville on Aug. 11.
The white nationalists were holding the rally to protest plans by the city of Charlottesville to remove a statue of Confederate Gen. Robert E. Lee.
Charlottesville Attack
People fly into the air as a vehicle drives into a group of counter-protesters demonstrating against a white nationalist rally in Charlottesville, Virginia, on Aug. 12. Several hundred counter-protesters were marching in a long line when the car drove into a group of them.
PHOTOS:State of Emergency in Charlottesville
Returning to the Scene
Marcus Martin, who was injured in the Charlottesville attack, visits the memorial on Aug. 13 at the site where he was injured and where 32-year-old Heather Heyer was killed.
PHOTOS:Mourners Honor Heather Heyer in Charlottesville
Disruption in Oakland
Protesters block both directions of Interstate 580 in Oakland, California, on Aug. 12, during a rally against racism in response to the violent clashes in Charlottesville.
Back in New York
John Kelly, White House chief of staff, and President Donald Trump speak aboard Air Force One as they arrive at John F. Kennedy International Airport in New York on Aug. 14. Trump was spending the night at Trump Tower, his first visit to his Manhattan home since he was sworn in.
Guam Sunset
A group of children stand behind a motorcycle on a beach at Tumon Bay in Guam on Aug. 12.
Fears about North Korea's missile and nuclear weapons programs have grown in recent weeks. Pyongyang has said it was considering plans to fire missiles over Japan toward the U.S. Pacific territory of Guam, although North Korean leader Kim Jong Un appears to have delayed the decision.
Fear in Barcelona
A woman carries a child as she flees the scene after a van plowed into pedestrians in Barcelona, Spain, on Aug. 17.
PHOTOS:Barcelona Attack
Defying Violence
A woman holds a sign that reads "I sing today for those voices that you have dared to shut up. We are not afraid" while observing a minute of silence at Placa de Catalunya, on Aug. 18, a day after a driver plowed into pedestrians, killing at least 13 people and injuring more than 100.
ISIS quickly claimed responsibility for what appeared to be the latest in a spate of low-tech terror attacks across Europe.
Young Shepherd
Nine-year-old Rory Scott from Bonar Bridge herds sheep as farmers gather for the great sale of lambs on Aug. 15 in Lairg, Scotland. At the annual lamb sale, the biggest one-day livestock market in Europe, some twenty thousand sheep from all over the north of Scotland are bought and sold.
Tragic Landslide
A mother who lost her son in the mudslide is consoled near Connaught Hospital in Freetown, Sierra Leone, on Aug. 16.
Authorities in Sierra Leone now say up to 450 bodies have been recovered following this week's torrential mudslides.Aid officials have estimated that at least 600 others are still missing.
Ice Raft
A polar bear stands on the ice in the Franklin Strait in the Canadian Arctic Archipelago, on July 22. While some polar bears are expected to follow the retreating ice northward, others will head south, where they will come into greater contact with humans, encounters that are unlikely to end well for the bears.
Photo made available Aug. 13.
Disputed Election
A man who was badly beaten is carried in the Kibera slum on Aug. 12 in Nairobi, Kenya. Police clashed with opposition supporters overnight as Uhuru Kenyatta was announced as president for his second term.
At least 28 people have been killed in election-related violence since polling day, many of them shot by police.
Opposition Rally
Kenyan opposition leader Raila Odinga speaks to thousands of supporters gathered in the Mathare area of Nairobi, Kenya on Aug. 13.Odinga condemned police killings of rioters during protests after the country's disputed election.
Election authorities have said that President Uhuru Kenyatta won the Aug. 8 election by 1.4 million votes, but Odinga said the results were rigged.He has not yet presented evidence of fraud.
Javelin Motion
Adam Sebastian Helcelet of the Czech Republic competes in the Men's Decathlon javelin during day nine of the 16th IAAF World Athletics Championships in London on Aug. 12.
The Week in Pictures: Aug. 4 - 11
