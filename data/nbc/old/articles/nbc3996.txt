__HTTP_STATUS_CODE
200
__TITLE
Earthquake Victims Face Race Against the Clock as Rescuers Scramble
__AUTHORS
Shamard Charles, M.D.
__TIMESTAMP
Sep 21 2017, 6:32 pm ET
__ARTICLE
 The search for survivors continues after Tuesdays 7.1 magnitude earthquake left behind a wake of destruction across Mexicos capital. The death toll from the quake has climbed to at least 250 people. 
 First responders  from policemen and firefighters to emergency medical technicians, construction workers, and untrained volunteers  continue to search frantically for survivors as thousands more may be trapped under the rubble. Helmeted heroes are pulling survivors from their would-be tombs while knowing that time is of the essence. 
 Assuming that survivors have access to clean air and avoided potentially fatal injury, many victims can survive these unimaginable conditions, at least for a little while. 
 But the question remains: Just how long can you survive without food and water? 
 Related: Pulled from rubble after 16 days: Water secret to survival 
 Its a race against the clock, says Dr. John Torres, emergency medicine physician and NBC News medical correspondent. In situations like these we need to ensure that these victims get fluids as soon as possible. Fluids give them time. Without water these people can suffer muscle breakdown and organ damage from dehydration. The longer they are dehydrated the greater the risk of these complications. 
 How long a person can survive on water alone depends on a variety of factors including the outside temperature and individual metabolism but, under extreme conditions, organ failure can develop relatively quickly. A healthy person, in ideal conditions, can go between three and eight days without water, and can last even longer without food, as long as eight weeks. Children, who have not developed the capacity that adults have to most efficiently regulate their body temperature, often deteriorate more quickly than others. 
 Related: Nepal Earthquake Victim Drank Own Urine to Survive 82 Hours in Rubble 
 The quick removal of children from the rubble is especially important, says Torres. Reports say that the rescue workers in Mexico are giving the children Pedialyte which is a nutritionally rich fluid, as well as chocolate, which is likely used to regulate their blood sugar and raise their spirits. Its a tough time, but its important to remind them that youre there with them and want them to stay positive. 
 In disaster response, phase I consists of arrival and assessment of damage and phase II involves developing a rescue plan. Teams providing immediate fluid resuscitation to all survivors are now in phase III of rescue  using thermal devices and highly sensitive microphones to help them identify potential survivors wedged deeply beneath the rubble. 
