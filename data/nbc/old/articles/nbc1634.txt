__HTTP_STATUS_CODE
200
__TITLE
United Fiasco: How Do Airlines Select Who to Remove From Overbooked Flights?
__AUTHORS
Nikita Biryukov
__TIMESTAMP
Apr 13 2017, 7:53 pm ET
__ARTICLE
 It's arguably the most notorious airline story of the moment: Dr. David Dao was randomly selected to be removed from an overbooked flight, refused, and was subsequently dragged off the flight, triggering a wave of public outrage and a flurry of apologies from United Airlines. 
 But how exactly was he picked? That much is less clear. 
 Passengers on the flight told NBC News the airline had picked passengers at random using an algorithm, but industry insiders and the airlines contract with its flyers indicate its unlikely the algorithm left much up to chance. 
 Airlines overbook flights using an algorithm to offset losses from seats that would otherwise go empty should a passenger not show up. That's standard practice, said Joseph Lamonaca, an aviation attorney and airline transport pilot. 
 The original algorithm was to make money  its a money maker, Lamonaca said. 
 When the algorithm guesses wrong and passengers are left without seats, the airline must first search out flyers who will voluntarily de-board. In most cases, volunteers are compensated for their troubles. If volunteers are in short supply, as they were on flight 3411, the airline must pick which flyers get the boot. 
 According to Uniteds contract of carriage, the airline will give priority to unaccompanied minors and passengers with disabilities, de-boarding them last. But outside those groups, things become a little more complicated. 
 NBC News reached out to United for comment but did not receive a response. 
 The airline will look to a passengers enrollment in their frequent flier programs and to the fare class of their ticket, said Brett Snyder, author and founder of the Cranky Flier travel blog. If it comes down to it, the airlines may compare check-in times. 
 Related: Doctor Dragged off United Plane Has Broken Nose and Significant Concussion: Lawyer 
 It's almost like a musical chairs sort of thing, Snyder said. You have a certain number of seats on the plane. And so they start filling up those seats, and then whoever's left standing at the end, in most cases, is the one who's left out. Different story when people are already on the airplane. 
 Passengers who are involuntarily bumped are entitled to compensation, too. Uniteds contract of carriage says passengers may receive up to $675 if the flight arrives between one and four hours after the planned arrival time of the original flight. If the rescheduled flight arrives more than four hours later, the passenger is entitled to up to $1350 from United. 
 Longer flights with many stops might decrease ones chance of getting bumped, said Ross Aimer, CEO of Aero Consulting Experts, an aviation consulting group, and a retired United pilot. He couldnt say the same of booking flights in advance. Since tickets bought earlier tend to cost less, being an early bird could up chances of getting booted. 
 Location plays a part, too. Bumping is more common at large hubs like Chicagos OHare International Airport, where Dao was violently removed from flight 3411 by Chicago aviation officers, Lamonaca said. Bumps are less frequent to destinations with limited flight paths. 
 They've made it public how they make the determination, and they have a right to do that, I suppose, Snyder said. It's up to them, if they think this is really the right way to do it. But I don't think there's anything random about it if they're following their guidelines. 
 And though some lawmakers have been incensed by Daos treatment, bumping may get worse before it gets better, Lamonaca said. Airlines may have to ferry more and more crews around the country to keep up with demand. The industry is currently experiencing a pilot shortage caused by a lack of experienced military pilots entering the private sector, as they did after World War II and the Korean War. 
 That glut, Lamonaca said, is all gone. 
