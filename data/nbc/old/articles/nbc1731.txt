__HTTP_STATUS_CODE
200
__TITLE
The Week in Pictures: May 5 - 12
__AUTHORS
NBC News
__TIMESTAMP
May 12 2017, 6:00 pm ET
__ARTICLE
Storm chaser Tim Marshall monitors a supercell system during a tornado research mission Monday in Elbert County, Colorado. With funding from the National Science Foundation and other government grants, scientists and meteorologists from the Center for Severe Weather Research try to get close to supercells and tornadoes to better understand their structure and strength, how low-level winds affect and damage buildings, and to learn more about tornado formation and prediction.
Marshall began stormchasing 40 years ago after his hometown in Oak Lawn, Illinois was struck by an F-4 tornado.
PHOTOS: Stormchasers Hunt Tornadoes for Science
A Venezuelan opposition activist wearing a homemade gas mask takes cover behind a makeshift shield as clashes erupt with riot police during a protest against President Nicolas Maduro in Caracas, Venezuela on May 8.
At least 38 people have been killed and hundreds injured in protests that erupted after the Supreme Court issued a ruling March 29 nullifying the opposition-controlled National Assembly, a decision it later reversed amid a storm of international criticism and outrage among Venezuelans.
Hundreds of thousands have taken to the streets to castigate Maduro's administration, which they claim has become a dictatorship responsible for triple-digit inflation, skyrocketing crime and crippling food shortages.
PHOTOS:In Venezuela, Upheaval Shows No Signs of Slowing Down

Lightning strikes over the Fatih Mosque and the Istanbul skyline during a thunderstorm on May 7 in Turkey.
A young guest tries to remove the seal from the lectern as Vice President Mike Pence delivers remarks during and event with military families to celebrate National Military Appreciation Month and National Military Spouse Appreciation Day in the Eisenhower Executive Office Building May 9 in Washington. The vice president hosted about 160 spouses and children of active duty U.S. military members.
The statue of Confederate President Jefferson Davis is removed in the early morning hours in New Orleans on May 11, as protesters both for and against the monument carried out tense protests nearby.
Following demonstrations and lengthy legal wrangles, the statue is one of four monuments relating to the Confederacy that's in the process of being removed by the Louisiana city.
Supporters of French independent centrist presidential candidate Emmanuel Macron react outside the Louvre museum in Paris on May 7.
French voters elected centrist Macron as the countrys youngest president ever, at 39, delivering a resounding victory to the unabashedly pro-European former investment banker and strengthening Frances place as a central pillar of the European Union.
Marine Le Pen, his far-right opponent in the runoff, quickly conceded defeat after voters rejected her French-first nationalism by a large margin.
The result wasnt even close: Pollsters projected that Macron won 65% of the votes.
A house is surrounded by floodwaters in Pointe-Calumet, Quebec, on May 8. Flooding caused by unusually persistent rainfall has driven nearly 1,900 people from their homes in 126 municipalities in the Canadian province of Quebec.
PHOTOS - Quebec in Deep Water: Historic Flooding Forces Nearly 1,900 to Evacuate
Opposition activists march along Francisco Fajardo highway, during a protest against President Nicolas Maduro in Caracas, Venezuela on May 10.
PHOTOS:Venezuela Opposition Protestd in Caracas as Unrest Enters Fourth Week
Jockey John Velazquez sprays champagne in the winner's circle after guiding Always Dreaming to win the 143rd running of the Kentucky Derby at Churchill Downs on May 6 in Louisville, Kentucky.
A woman enjoys the weather at the Forty Foot diving area and beach in Sandycove, Ireland May 10.
A young boy shies away after he handed a bouquet of flowers to Britain's Catherine, the Duchess of Cambridge in Luxembourg on May 11.
An Angolan woman prays while walking on her knees at the Fatima Sanctuary in Fatima, Portugal on May 11.
Three shepherd children in Portugal were jailed and threatened with being boiled alive in olive oil when they claimed the Virgin Mary appeared to them 100 years ago. The Catholic Church doubted their story too.
From that unpromising start, the children's reported visions would go on to strengthen the faith of Portugal's persecuted Catholics and make the small farming town of Fatima one of the world's foremost pilgrimage sites, with around 6 million visitors a year.
Now, the Portuguese youngsters' long-ago testimony is giving the Catholic Church its youngest saints who did not die as martyrs. Pope Francis arrived Friday in Fatima to observe the centenary of their visions by canonizing Francisco and Jacinta Marto this weekend.
Ultra-Orthodox Jews pray as others harvest wheat in a field near the central Israeli town of Modi'in on May 11. The harvested wheat will be stored for almost a year and then use it to grind flour to make unleavened bread for the week-long Passover festival.
Mount Sinabung volcano spews thick volcanic ash as seen from Tiga Kicat village in Karo, North Sumatra province, on early May 6. Sinabung roared back to life in 2010 for the first time in 400 years. After another period of inactivity it erupted once more in 2013, and has remained highly active since.
A young Buddhist monk takes part in prayers marking the Buddha's birthday at the Boudha Stupa on May 10 in Kathmandu, Nepal. Vesak is observed during the full moon in May or June, which celebrates the stages of the life of Buddhism's founder, Gautama Buddha, from the birth, the enlightenment to nirvana, and his passing (Parinirvana).
Local residents carry portraits of their ancestors, participants in World War II as they celebrate the 72nd anniversary of the defeat of the Nazis in World War II in St. Petersburg, Russia, on May 9. About 400,000 people walked in central streets of St. Petersburg in a march named 'Immortal Regiment' while carrying portraits of their relatives who fought in World War II.
The Week in Pictures: April 28 - May 5
