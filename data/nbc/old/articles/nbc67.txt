__HTTP_STATUS_CODE
200
__TITLE
Author Tia Williams on the Incredibly Fashionable World of Romantic Escapism
__AUTHORS
Lesley-Ann Brown
__TIMESTAMP
Oct 13 2017, 1:02 pm ET
__ARTICLE
 Best-selling novelist Tia Williams is a writers writer with a fashionista twist. 
 An author of four books, including the coffee table book "The Beauty of Color" (2006) which she co-authored with legendary Iman, Williams was awarded the Best Fiction prize at the African American Literary Awards for her book "The Perfect Find" last October. 
 NBCBLK recently caught up with Williams to discuss marrying her love for fashion with her love for writing, using life as creative inspiration and conjuring up Michael B. Jordan to base her love-interest on. 
 [The following interview has been edited and condensed for clarity] 
 NBCBLK: Your book takes your reader into the world of fashion. Can you tell us a little bit about your background and how you seem to have such a grasp on this world? 
 I think I was born a style junkie! Ive been a journalist in the beauty and fashion world for twenty years. I started off my career as a beauty editor at magazines like Elle, YM, Lucky, Glamour, Teen People, and Essence  and, in 2004, I started one of the first beauty blogs, Shake Your Beauty. These days, Im head of copy at Bumble and bumble, the global hair care brand. 
 When "The Perfect Find" becomes a film, who do you see playing the main characters? 
 Michael B. Jordan was in my head the whole time I was writing Eric. My crush on him knows no bounds. And for Jenna, I see Gabrielle Union or Tracee Ellis Ross. Or Regina Hall, who I love. Shes equal parts gorgeous and hilarious. 
 New York and Brooklyn are both characters in this book. Tell us about your own, personal relationship to each. 
 Im originally from Virginia, but I moved to Brooklyn two weeks after I graduated from UVA, in the summer of 1997. I lived on the same block where Biggie grew up, and for a sheltered suburban girl, this was the most glamorous thing, ever. I instantly fell in love with the high/low culture, the diversity, the accents, the grit. The Brownstones. Brooklyn is style. And Manhattan is where my career has played out. So, I love infusing both in my books. 
 Whats your favorite thing to do in Brooklyn? 
 Brooklyn in the spring is magical. My daughter and I love spending a Saturday walking around Fort Greene. Flowers are blooming, couples are canoodling, Brooklyn Flea is open for business (its delightful outdoor flea market with the best food trucks). Wed visit Greenlight Bookstore, or the fabulous playground by the Clinton/Washington A train, catch a street festival, or a matinee at BAM. Theres always something happening. 
 Related: Author Bernice McFadden Gets to the Heart of a Good Story 
 Whats your favorite thing to do in New York? 
 Whatever I do in the city, it must involve getting mini-donuts at the Doughnuttery in Chelsea Market and a stop at The Strand for out-of-print books. For fancy brunch, Im obsessed with Upland on Park Avenue South. And my favorite mommy/daughter spot is definitely Alices Teacup on the Upper East Side. 
 Do you think you will always be a New Yorker? 
 Definitely. Its expensive, abusive, and completely freezing from November to March, but its home. New Yorks unlike anywhere else. 
 You have a daughter who is probably too young to read your books! How do you pull that off? 
 Its definitely tough having a full-time job while also juggling being a single mother and an author! I do manage to pull it off, but I havent mastered work/life balance. I barely sleep or date, and I havent worked out since June. Really, I should look into a life coach. 
 Whats your favorite way to spend Sunday? 
 This is embarrassing, but watching old episodes of "Moonlighting" while eating waffles with strawberries sounds heavenly. Im easy to please. 
 Related: 50 Years of Couture: New Exhibit Celebrates Fashion Empire 
 If you could throw a dinner party with anyone alive or dead, who would be your guests? What would you serve/where would you go out to dine? You can only invite three guests. 
 For entirely different reasons, Id invite Prince, Dorothy Parker, and Riz Ahmed. At dinner, Prince would have to sing "If I Was Your Girlfriend". Dorothy Parker would have to compose an original poem. And Riz would have to speak in his Queens accent from "The Night Of". The menu is tricky, because I cant cook. I guess Id serve Trader Joes hors d'oeuvres. Theyre delectable! Who doesnt love a mini crabcake? 
 Whats your next project? 
 Im working on my fifth novel right now, but Im too superstitious to talk about it! 
 Follow NBCBLK on Facebook, Twitter and Instagram 
