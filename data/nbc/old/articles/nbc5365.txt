__HTTP_STATUS_CODE
200
__TITLE
California Wildfires Threaten Thousands More Homes
__AUTHORS
Henry Austin
Associated Press
__TIMESTAMP
Sep 21 2015, 7:46 am ET
__ARTICLE
 One of the worst wildfires in Californias history tore through another 162 homes and was threatening thousands more along with another monster blaze sweeping through the state, officials warned Monday. 
 Cal Fire said Monday that the Valley Fire was almost 69 per cent contained, but that another 6,563 residential buildings were at risk from the wildfire north of San Francisco. 
 Damage-assessment teams have counted 1,050 homes in total burned in Lake County, many of them in the town of Middletown, according to Department of Forestry and Fire Protection spokesman Daniel Berlant. 
 He said teams have completed about 80 per cent of damage assessment, focusing largely on homes and have not yet determined how many additional structures  such as sheds, barns and other outbuildings  were destroyed. 
#ValleyFire [update] in southern Lake County is now 75,711 acres & 69% contained. http://t.co/ggH21VbYzf
 The fire  which has killed at least three people and charred 118 square miles  is considered the fourth worst fire in California history based on total structures burned. 
 Some of those who fled the fire have been allowed to return home, according to NBC station KCRA. 
 But "some of these areas were very remote and to get all the crews and equipment in that's needed to clean it up, it's difficult," Lake County Sheriff's Lt. Norm Taylor, told the station. "Beyond that, there are huge tasks for clean-up of toxic substances and health safety issues." 
 He added that they were gradually reopening small areas on purpose because they couldnt have tens of thousands of people racing to one highway to get in all at once." 
 A second large blaze  dubbed the Butte Fire  is threatening another 6,400 structures about 170 miles southeast in the Sierra Nevada foothills. The Butte Fire already has destroyed almost 550 homes and 356 outbuildings. 
#ButteFire [update] east of Jackson (Amador & Calaveras Counties) is now 70,760 acres and 72% contained. http://t.co/EF4lnPaeKR
 That blaze  which has left at least two people dead  was 72 per cent contained by early Monday. While all evacuation orders had been lifted Saturday, some residents returned to find nothing. 
 "Everything was destroyed," said Annie Curtis, 16, an evacuee from Mountain Ranch, told KCRA. 
 "My house, the barn, the woodshed, three cars, some tractors, a whole backhoe, the tires melted off," she said. 
 Meanwhile, authorities announced Monday that another body was found in the ashes of the fast moving Tassajara fire in Monterey County. The Tassajara fire was 30 per cent contained by Monday. 
#TassajaraFire [update] 2.5 miles north of Jamesburg (Monterey County) is now 1,086 acres and 30% contained. http://t.co/0j7N95Tw1W
 Firefighters found the man's corpse inside a charred vehicle after the fire began Saturday near the community of Jamesburg. His death was being investigated as a possible suicide, Monterey County Sheriff's spokesman John Thornburg said. 
 
