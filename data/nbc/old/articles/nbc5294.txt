__HTTP_STATUS_CODE
200
__TITLE
Chinas Diplomatic Ties With Ally North Korea Are Fraying
__AUTHORS
Reuters
__TIMESTAMP
Sep 8 2017, 7:11 am ET
__ARTICLE
 BEIJING  When Kim Jong Un inherited power in North Korea in late 2011, then-Chinese president Hu Jintao was outwardly supportive of the untested young leader, predicting that "traditional friendly cooperation" between the countries would strengthen. 
 Two years later, Kim ordered the execution of his uncle Jang Song Thaek, the country's chief interlocutor with China and a relatively reform-minded official in the hermetic state. 
 Since then, ties between the allies have deteriorated so sharply that some diplomats and experts fear Beijing may become, like Washington, a target of its neighbor's ire. 
 While the United States and its allies  and many people in China  believe Beijing should do more to rein in Pyongyang, the acceleration of North Korea's nuclear and missile capabilities has coincided with a near-total breakdown of high-level diplomacy between the two. 
 Before retiring this summer, China's long-time point man on North Korea, Wu Dawei, had not visited the country for over a year. His replacement, Kong Xuanyou, has yet to visit and is still carrying out duties from his previous Asian role, traveling to Pakistan in mid-August, diplomats say. 
 Related: China Grows Weary of Its Unruly Nuclear Neighbor 
 The notion that mighty China wields diplomatic control over impoverished North Korea is mistaken, said Jin Canrong, an international relations professor at Beijing's Renmin University. 
 "There has never existed a subordinate relationship between the two sides. Never. Especially after the end of the Cold War, the North Koreans fell into a difficult situation and could not get enough help from China, so they determined to help themselves." 
 A famine in the mid-1990s that claimed anywhere from 200,000 to three million North Koreans was a turning point for the economy, forcing private trade on the collectivized state. That allowed the North a degree of independence from outside aid and gave credence to the official "Juche" ideology of self-reliance. 
 China fought alongside North Korea during the 1950-53 Korean War, in which Chinese leader Mao Zedong lost his eldest son, and Beijing has long been Pyongyang's chief ally and primary trade partner. 
 While their relationship has always been clouded by suspicion and mistrust, China grudgingly tolerated North Korea's provocations as preferable to the alternatives: chaotic collapse that spills across their border, and a Korean peninsula under the domain of a U.S.-backed Seoul government. 
 That is also the reason China is reluctant to exert its considerable economic clout, worried that measures as drastic as the energy embargo proposed this week by Washington could lead to the North's collapse. 
 Instead, China repeatedly calls for calm, restraint and a negotiated solution. 
 The North Korean government does not provide foreign media with a contact point in Pyongyang for comment by email, fax or phone. The North Korean embassy in Beijing was not immediately available for comment. 
 China's foreign ministry did not respond to a faxed request for comment. It has repeatedly spoken out against what it calls the "China responsibility theory" and insists the direct parties  North Korea, South Korea and the United States  hold the key to resolving tensions. 
 Related: Kim Jong Un May Mark an Anniversary With a Missile 
 Until his death in 2011, North Korean leader Kim Jong Il made numerous entreaties to ensure China would back his preferred son as successor. 
 While then-President Hu reciprocated, the younger Kim, in his late 20s at the time, began to distance himself from his country's most powerful ally. 
 "There's a lot of domestic politics in North Korea where this young leader who isn't well-known, he's not proven yet, especially has to show that he's not in the pocket of Beijing," said John Delury of Seoul's Yonsei University. "I think he made the decision first to keep Hu Jintao and then (current President) Xi Jinping really at bay." 
 Within months of coming to power, Kim telegraphed North Korea's intentions by amending its constitution to proclaim itself a nuclear state. The execution of Jang's uncle in 2013 sealed Beijing's distrust of the young leader. 
 "Of course the Chinese were not happy," said a foreign diplomat in Beijing focused on North Korea. "Executing your uncle, that's from the feudal ages." 
 In an attempt to warm ties, Xi sent high-ranking Communist Party official Liu Yunshan to attend the North's October 2015 military parade marking the 70th anniversary of the founding of the Workers' Party of Korea. 
 Liu hand-delivered a letter from Xi praising Kim's leadership and including congratulations not just from the Chinese Communist Party but Xi's personal "cordial wishes" in a powerful show of respect. 
 Xi's overture has been repaid with increasingly brazen actions by Pyongyang, which many observers believe are timed for maximum embarrassment to Beijing. Sunday's nuclear test, for example, took place as China hosted a BRICS summit, while in May, the North launched a long-range missile just hours before the Belt and Road Forum, dedicated to Xi's signature foreign policy initiative. 
 Related: Putin Warns 'Hysteria' Over N. Korea Threatens 'Catastrophe' 
 Mao Zedong's description of North Korea's relationship with China is typically mischaracterized as being as close as "lips and teeth". 
 His words are better translated as: "If the lips are gone, the teeth will be cold," a reference to the strategic importance of the North as a geographical security buffer. 
 Despite its resentment at the pressure North Korea's actions have put it under, Beijing refrains from taking too hard a line. 
 It said little when Kim Jong Un's half-brother was assassinated in February at Kuala Lumpur's airport. The half-brother, Kim Jong Nam, had been seen as a potential rival for power in Pyongyang and had lived for years in Beijing, then Macau. 
 An editorial in China's influential Global Times warned after Pyongyang's latest nuclear test that cutting off North Korea's oil would redirect the conflict to one between North Korea and China. 
 Zhao Tong, a North Korea expert at the Carnegie-Tsinghua Center in Beijing, said North Korea was deeply unhappy with China's backing of earlier UN sanctions. 
 "If China supports more radical economic sanctions that directly threaten the stability of the regime, then it is possible that North Korea becomes as hostile to China as to the United States." 
