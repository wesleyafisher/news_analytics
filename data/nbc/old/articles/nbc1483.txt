__HTTP_STATUS_CODE
200
__TITLE
Tragedy in Ohio: 18-Month-Old Boy Dies of Suspected Opioid Overdose After 9-Year-Old Calls 911
__AUTHORS
Corky Siemaszko
__TIMESTAMP
Jun 5 2017, 4:21 pm ET
__ARTICLE
 Inside an Ohio morgue Monday lay the tiny body of one of the youngest victims of the opioid epidemic ravaging the state: an 18-month-old boy from Akron. 
 The infant overdosed on Thursday evening and emergency workers  summoned by a 911 call from the victims 9-year-old sibling to their Gale Street home  were able to revive him with a dose of Narcan and get him to the hospital, Lt. Rick Edwards of the Akron Police Department told a local news organization. 
 But on Sunday, the boy died at Akron Childrens Hospital and his body was taken to the Summit County Medical Examiners office for an autopsy, the MEs chief investigator Gary Guenther told NBC News. 
 We havent done the exam yet, Guenther said. Right now his death is being classified as a suspicious death of an 18-month-old. When he was brought it, it was as a suspected opioid overdose. Thats how it was reported. 
 Asked if he was the youngest suspected overdose victim hes seen, Guenther simply replied, Yes. 
 Police did not release the boys name. They said the drugs belonged to his mother, who fled the home after police and paramedics arrived. 
 The surviving child has been turned over to the care of the Summit County Children Services. 
