__HTTP_STATUS_CODE
200
__TITLE
Rocket With Mustard Agent Lands Near Qayara Base in Iraq: U.S. Official
__AUTHORS
Courtney Kube
F. Brinley Bruton 
__TIMESTAMP
Sep 22 2016, 6:48 am ET
__ARTICLE
 A rocket containing mustard agent landed near a base housing American troops involved in the fight against ISIS militants in Iraq, a U.S. official told NBC News on Wednesday. 
 No one was hurt in the Tuesday afternoon incident near Qayara air base, where U.S. troops are advising and assisting Iraqi troops, the official added. 
 The official spoke on condition of anonymity and said a three or four-man explosives ordnance team investigating the shell that landed found a small amount of black, oily substance on a fragment. 
 A field test was positive for mustard agent while a second test was negative, the official added. As a precaution the team went through decontamination and the sample was sent for further testing. 
 None of the team members have shown any symptoms of mustard-agent exposure, according to the official. 
 ISIS has tried to put chemicals in artillery shells in the past, although these ordinances were poorly weaponized, crude and militarily ineffective, the official added. 
 In large enough quantities and concentrations, mustard gas can maim or kill by damaging skin, eyes and airways. 
 The official told NBC News that hundreds of American troops were at the base south of ISISs stronghold of Mosul when the shell landed. Iraqi forces are readying themselves for an offensive to retake Mosul. 
 On Sept. 13, Air Force Lt. Gen. Jeffrey Harrigian told journalists that American warplanes had eliminated a "significant chemical threat" to civilians by bombing a complex near Mosul that had been transformed from pharmaceutical manufacturing site to chemical weapons production facility. 
 He provided no details on the chemical production. 
