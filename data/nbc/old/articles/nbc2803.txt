__HTTP_STATUS_CODE
200
__TITLE
Zika Funding Delay in Congress Puts Americans at Risk, Obama Says
__AUTHORS
Maggie Fox
__TIMESTAMP
Aug 27 2016, 8:28 am ET
__ARTICLE
 President Barack Obama urged Congress to make Zika funding its first priority after members return from a seven-week summer break, saying the delay is putting Americans at risk. 
 [E]very day that Republican leaders in Congress wait to do their job, every day our experts have to wait to get the resources they need. That has real-life consequences, Obama said in his weekly radio address. Weaker mosquito-control efforts. Longer wait times to get accurate diagnostic results. Delayed vaccines. It puts more Americans at risk. 
 Zika virus has caused two outbreaks in Florida and infected 42 people bitten by local mosquitoes. Its been brought to the continental U.S. by more than 2,000 people  probably many more than that  and infected more than 580 pregnant women in U.S. states. 
 Its caused a full epidemic in Puerto Rico, killed at least two people there and threatens Gulf states where its carrier, the Aedes mosquito, thrives. 
 Related: Zika Funding Battle 'No Way to Fight an Epidemic' 
 And its caused terrible and often lethal birth defects in at least 21 cases on U.S. soil. 
 In February  more than six months ago  I asked Congress for the emergency resources that public health experts say we need to combat Zika, Obama said. 
 That includes things like mosquito control, tracking the spread of the virus, accelerating new diagnostic tests and vaccines, and monitoring women and babies with the virus. 
 Obama asked for $1.9 billion in funding to fight the virus. Republicans in Congress refused, offering counter-proposals with part of that funding, and finally offering a bill that would defund Planned Parenthood, a move calculated to torpedo the bill as Democrats refused to support it. 
 As Zikas spread worsened, Congress left for its summer break. Federal agencies  which cannot spend new money unless its appropriated by Congress  have had to shift cash around. 
 Instead, we were forced to use resources we need to keep fighting Ebola, cancer, and other diseases, Obama said. 
 Related: Woman Lost Her Baby to Zika Before CDC Could Even Warn Her 
 Republicans in Congress should treat Zika like the threat that it is and make this their first order of business when they come back to Washington after Labor Day. That means working in a bipartisan way to fully fund our Zika response," the president added. 
 Floridas Republican governor, Rick Scott, has criticized both Congress and the federal government. He says hell be in Washington when Congress comes back on Sept. 6. 
 During Congresss vacation, we have identified 43 cases of locally acquired Zika in four Florida counties, Scott said earlier this week. 
 The Zika virus demands immediate federal action and I will impress upon our congressional members the urgency to protect our residents and visitors. 
 But he also griped about the CDC, setting off an unusual back and forth with an agency that usually defers completely to the states. 
 Last week, I requested that the CDC provide Florida with 5,000 Zika antibody tests, but they have only sent less than 1,200, Scott said. 
 This is unacceptable. We also requested additional lab support personnel to help the state expedite Zika testing and an additional 10,000 Zika prevention kits. It is disappointing that these requests have not been fulfilled. 
 The agency snapped back that its been helping all along. 
 CDC has and will continue to provide support to Florida to address the Zika outbreak. CDC experts in epidemiology, surveillance, and vector control have been on the ground for weeks supporting the state of Floridas response, it said. 
 Related: Rare Zika Complication Hits 30 in Puerto Rico 
 CDC has provided $35 million in federal funds for Zika and emergency response, including public health and emergency preparedness funds for both FY 2015 and 2016 that can be used to purchase items for Zika prevention kits. CDC also has provided 10,000 bottles of DEET for the kits. 
 DEET is one of the most effective mosquito repellents, CDC says, and is safe for everyone, including pregnant women, to use. 
 Obama offered medical advice to all Americans. 
 If you live in or travel to an area where Zika has been found, protect yourself against the mosquitoes that carry this disease, he said. Use insect repellent  and keep using it for a few weeks, even after you come home. Wear long sleeves and long pants to make bites less likely. Stay in places with air conditioning and window screens. If you can, get rid of standing water where mosquitoes breed. And to learn more about how to keep your family safe, just visit CDC.gov. 
 Men who may have been infected should use condoms to prevent sexual transmission, he added. Researchers say sexual transmission may be a common route of transmitting Zika. 
 See full Zika coverage from NBC News here. 
