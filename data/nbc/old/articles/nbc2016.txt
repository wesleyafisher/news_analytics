__HTTP_STATUS_CODE
200
__TITLE
Nearly Untreatable Gonorrhea Spreading Globally
__AUTHORS
Maggie Fox
__TIMESTAMP
Jul 7 2017, 4:01 pm ET
__ARTICLE
 Drug-resistant gonorrhea is becoming more common, making a once easily treated infection into a nightmare disease, the World Health Organization said Friday. 
 Gonorrhea, known commonly as "the clap" or "the drip," is one of the most common sexually transmitted diseases there is. Anyone who is sexually active is at risk and any kind of sex, including anal and oral sex, can pass it along. 
 It was once easily treated with a quick dose of antibiotics. But, like all bacterial infections, strains have evolved that can evade the mechanisms used by antibiotics and now WHO says they are becoming increasingly common. 
 The bacteria that cause gonorrhea are particularly smart. Every time we use a new class of antibiotics to treat the infection, the bacteria evolve to resist them, said WHOs Dr. Teodora Wi. 
 Penicillin was the original, simple cure. Now, WHO reports, 97 percent of countries report gonorrhea that resists ciprofloxacin, 81 percent have found cases that resist azithromycin and two-thirds of countries have found strains that resist the last-resort drugs: extended-spectrum cephalosporins such as oral cefixime or injectable ceftriaxone. 
 Related: New Drug-resistant Gonorrhea Shows Up in Hawaii 
 These cases may just be the tip of the iceberg, since systems to diagnose and report untreatable infections are lacking in lower-income countries where gonorrhea is actually more common, Wi said. 
 And a few cases have been reported that were almost completely untreatable 
 To date, three extensively drug-resistant gonococcal strains with high-level resistance to ceftriaxone (superbugs) have also been reported  in France, Japan, and Spain, Wi and colleagues wrote in their report. 
 There are several reasons its spreading. Decreasing condom use, increased urbanization and travel, poor infection detection rates, and inadequate or failed treatment all contribute to this increase, WHO said in a statement. 
 Now the treatment guidelines call for a double-dose treatment for all gonorrhea: an injection of ceftriaxone and azithromycin pills. 
 Related: Three STDs Are Becoming Untreatable 
 The WHO report urges countries to encourage the development of new drugs and a vaccine against gonorrhea and urges better surveillance and treatment in the meantime. 
 The development of new antibiotics is not very attractive for commercial pharmaceutical companies. Treatments are taken only for short periods of time (unlike medicines for chronic diseases) and they become less effective as resistance develops, meaning that the supply of new drugs constantly needs to be replenished, WHO said. 
 Gonorrhea is not deadly, but it can have serious health effects. It also often spreads silently. 
 It rarely causes immediate symptoms in women, and often none in men, even as it does damage that can ruin fertility. 
 The CDC estimates 800,000 new gonorrhea infections occur every year in the U.S. and having it once doesnt protect you  people can catch it again and again. Globally, WHO estimates 78 million people are infected with gonorrhea every year. 
 Gonorrhea is the second most commonly reported infectious disease, after chlamydia. 
 According to the CDC, symptoms in men include: 
 Most women with gonorrhea do not have any symptoms. But they can include: 
 You can get gonorrhea by having vaginal, anal, or oral sex with someone who has gonorrhea. A pregnant woman with gonorrhea can give the infection to her baby during childbirth, CDC cautions. 
