__HTTP_STATUS_CODE
200
__TITLE
Michigan Man Turns Himself In After Police Complete His Facebook Challenge
__AUTHORS
Kara Taylor
__TIMESTAMP
Oct 17 2017, 1:45 pm ET
__ARTICLE
 A man wanted on two probation violations followed through Monday on his unusual promise to police in Redford Township, Michigan: Get 1,000 shares on their next Facebook post and he'd hand himself over, along with donuts and a promise to clean up litter around public schools. 
 "Yeah I'm not worried about it," Michael Zaydel, 21, wrote in what appeared to be a private Facebook message sent to the department. Redford police used a screenshot of his message as a Facebook post earlier this month. It had been shared more than 4,000 times by midday Wednesday. 
 In the Oct. 6 post accepting the challenge police wrote, Donuts!!!! He promised us donuts! You know how much we love Donuts!" 
 Police announced Zaydel arrived at approximately 6:30 p.m. Monday with donuts in hand. They thanked the public for supporting the search in a Facebook post. 
 "We would again like to express our gratitude for the support of all who followed this, shared it, and left us positive feedback," police wrote. 
 Zaydel was charged with misdemeanors for two probation violations, one for operating a vehicle impaired and another for attempted assault, police told NBC news. 
 In a hearing that took place on Tuesday morning, he was sentenced to 39 days in jail, police said, and an additional 30 days if he failed to pay court fees. 
 According to Click on Detroit, Zaydel is also known as "Champagne Torino" on social media. Previous to this incident, Redford police had threatened to block him from commenting on the department's Facebook page due to veiled threats and an inability to engage in constructive dialogue. 
 
