__HTTP_STATUS_CODE
200
__TITLE
Lots of Americans Prescribed Opioids, Insurance Survey Shows
__AUTHORS
Maggie Fox
__TIMESTAMP
Jun 29 2017, 5:53 pm ET
__ARTICLE
 More than one in five people insured by Blue Cross and Blue Shield were prescribed an opioid painkiller at least once in 2015, the insurance company reported Thursday. 
 And claims for opioid addiction and dependence surged nearly 500 percent between 2010 and 2016, the company said. 
 The report, which covers 30 million people with Blue Cross and Blue Shield insurance in 2015, supports what experts have been saying: much, if not most, of the opioid overdose epidemic is being driven by medical professionals who are prescribing the drugs too freely. 
 Twenty-one percent of Blue Cross and Blue Shield (BCBS) commercially insured members filled at least one opioid prescription in 2015, the report says. Data also show BCBS members with an opioid use disorder diagnosis spiked 493 percent over a seven year period. 
 The report excludes people with cancer or terminal illnesses. What it found fits in with similar surveys of people with Medicare, Medicaid or other government health insurance, said Dr. Trent Haywood, chief medical officer for the Blue Cross and Blue Shield Association (BCSBA). 
 Its consistent, Haywood said. 
 The United States is suffering an intense opioid overdose epidemic, the Centers for Disease Control and Prevention says.  
 Opioids (including prescription opioids and heroin) killed more than 33,000 people in 2015, more than any year on record. Nearly half of all opioid overdose deaths involve a prescription opioid, CDC says. 
 Related: Ohio Limits Opioid Prescriptions to Just Seven Days 
 The report also finds people in the areas hardest hit by the epidemic are least likely to get medication-assisted treatment, a regimen that includes counseling and drugs such as buprenorphine to help wean patients off the highly addictive opioids. 
 The CDC has been trying to get doctors to prescribe opioids only when absolutely necessary, and to prescribe as low a dose as possible for the shortest time possible. Other painkiller options include ibuprofen or acetaminophen, ice and even relaxation techniques. 
 Haywood said health insurance companies can help, both by keeping track of statistics but also by helping doctors control what they prescribe. 
 That includes keeping an eye on doctors who seem to prescribe far more opioids than other physicians, but also by using guidelines. We are not telling providers how to practice medicine, Haywood told NBC News. But what are called prior authorization processes act as a brake on unnecessary prescriptions, he said. 
 Some of the trends turned up in the BCBSA report: 
 The danger is worsened when patients with chronic conditions, such as arthritis, also get high-dose prescriptions. But these are the patients most likely to need a higher dose because sensitivity to the painkillers goes down over time. 
 We are still struggling as clinicians to do a better job with chronic pain management, Haywood said. 
 Related: FDA Asks Drug Company to Pull its Opioid 
 Separately, the Agency for Healthcare Research and Quality reported that hospital stays to treat opioid disorders rose between 2005 and 2014. Although the rate for males was higher in 2005, by 2014 the rate was the same for both sexes, AHRQ said in its report. 
 Opioid-related inpatient stays were lowest in Iowa, Nebraska, Texas, and Wyoming and highest in Massachusetts, the AHRQ report reads. Opioid-related ED (emergency department) visits were lowest in Arkansas and Iowa and highest in Maryland. 
