__HTTP_STATUS_CODE
200
__TITLE
Celebrating Latinidad: Google Creates Online Latino Culture Exhibit
__AUTHORS
Dorean K. Collins
__TIMESTAMP
Sep 8 2017, 7:51 am ET
__ARTICLE
 Google has unveiled a massive online collection of U.S. Latino art, culture and history. The exhibit created by the Google Cultural Institute is titled Google Arts & Culture: Latino Cultures in the US, and it showcases collections such as Celebrating our Latinidad, Freedom Tower, Tower of Hope and LGBTQ + Collections. It was unveiled Thursday. 
 Some of the collections explore Hispanic experiences in 20th Century America (La Experiencia Americana), the history of Franciscan missions that dominated the economic and spiritual fabric of Spanish and Mexican California from 1769 to 1835 (Californias Missions), and prominent people in Latino culture such as civil rights activist Dolores Huerta, baseball player and U.S Marine Roberto Clemente Walker, and Supreme Court Justice Sonia Sotomayor. 
 This digital collection of stories, narratives, and exhibits come from 50 partner institutions, including museums such as the Smithsonian Latino Center and the Ballet Hispnico in New York, as well as libraries and archives from across the country. 
 The collection is a compilation of over 4,300 archives and artworks related to the Latino experience in the U.S., with over 90 multimedia exhibits in English and Spanish. It also includes virtual tours of historic sites and culturally significant locations such as the Pilsen neighborhood in Chicago and the Calle Ocho Domino Park in Little Havana, Miami, Florida. 
 Googles platform allows users around the world to have an immersive way to experience art, history and culture. The Latino Cultures in the US exhibit debuted just in time for Hispanic Heritage Month, sharing with the world stories of Latino history, art and culture in the U.S, a community and culture that has often been underrepresented. Hispanic Heritage Month begins Sept. 15 and continues through Oct. 15. 
 The Google Arts & Culture: Latino Cultures in the U.S. exhibit will be available online all year. 
 Follow NBC Latino on Facebook, Twitter and Instagram. 
