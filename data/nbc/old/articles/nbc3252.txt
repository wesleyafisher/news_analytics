__HTTP_STATUS_CODE
200
__TITLE
Washington State Sues OxyContin Maker Purdue Pharma
__AUTHORS
Reuters
__TIMESTAMP
Sep 28 2017, 3:38 pm ET
__ARTICLE
 Washington state on Thursday sued Purdue Pharma LP, the maker of OxyContin, becoming the latest state or local government to seek to hold a pharmaceutical company accountable in court for a national opioid addiction epidemic. 
 The city of Seattle also filed a separate lawsuit against Purdue as well as units of Teva Pharmaceutical Industries Ltd , Johnson & Johnson, Endo International Plc and Allergan Plc. 
 The lawsuit by Washington Attorney General Bob Ferguson accused Purdue of deceptive marketing of OxyContin and of convincing doctors and the public that its drugs had a low risk of addiction and were effective for treating chronic pain. 
 He said he would seek to force Purdue to pay a "significant" sum for engaging in marketing practices that downplayed the addictiveness of its drugs, allowing it to earn billions of dollars while fueling the opioid crisis. 
 "I don't know how executives at Purdue sleep at night," Ferguson told reporters. 
 Purdue, based in Stamford, Connecticut, said in a statement that it was "deeply troubled" by the opioid crisis and that its U.S. Food and Drug Administration-approved products account for just 2 percent of all opioid prescriptions. 
 Related: CVS to Limit Opioid Prescriptions to 7-Day Supply 
 "We vigorously deny these allegations and look forward to the opportunity to present our defense," Purdue said. 
 According to the U.S. Centers for Disease Control and Prevention, opioids were involved in over 33,000 deaths in 2015, the latest year for which data is available. The death rate has continued rising, according to estimates. 
 The lawsuits followed a wave of lawsuits against opioid manufacturers and distributors by Louisiana, West Virginia, New Mexico, Oklahoma, Mississippi, Ohio, Missouri, New Hampshire and South Carolina, as well as several cities and counties. 
 Related: One in Three Americans Took Prescription Opioid Painkillers in 2015, Survey Says 
 Purdue and three executives pleaded guilty in 2007 to federal charges related to the misbranding of OxyContin, and agreed to pay $634.5 million to resolve a Justice Department probe. 
 That year, the privately held company also reached a $19.5 million settlement with 26 states and the District of Columbia. It had agreed in 2015 to pay $24 million to resolve a lawsuit by Kentucky. 
 In filing his lawsuit in King County Superior Court in Seattle on Thursday, Ferguson said he was breaking off from an ongoing multistate probe by various attorneys general into companies that manufacture and distribute opioids. 
 While Ferguson said he looked forward to seeing its results, "we felt we had a case ready to go." 
  (Reporting by Nate Raymond in Boston; Editing by Susan Thomas and Tom Brown) 
