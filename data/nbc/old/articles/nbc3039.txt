__HTTP_STATUS_CODE
200
__TITLE
Johnson & Johnson Hit With $110 Million Verdict in Baby Powder Cancer Suit
__AUTHORS
Associated Press
__TIMESTAMP
May 5 2017, 7:48 pm ET
__ARTICLE
 ST. LOUIS  A St. Louis jury has awarded a Virginia woman a record-setting $110.5 million in the latest lawsuit alleging that using Johnson & Johnson's baby powder caused cancer. 
 The jury ruling Thursday night for 62-year-old Louis Slemp, of Wise, Virginia, comes after three previous St. Louis juries awarded a total of $197 million to plaintiffs who made similar claims. Those cases, including the previous highest award of $72 million, are all under appeal. 
 Slemp, who was diagnosed with ovarian cancer in 2012, blames her illness on her use of the company's talcum-containing products for more than 40 years. Her cancer has spread to her liver, and she was too ill to attend the trial. About 2,000 state and federal lawsuits are in courts across the country over concerns about health problems caused by prolonged talcum powder use. 
 Johnson & Johnson, based in Brunswick, New Jersey, said in a statement that it would appeal and disputed the scientific evidence behind the plaintiffs' allegations. The company also noted that a St. Louis jury found in its favor in March and that two cases in New Jersey were thrown out by a judge who said there wasn't reliable evidence that talc leads to ovarian cancer. 
 "We are preparing for additional trials this year and we will continue to defend the safety of Johnson's Baby Powder," the statement said. 
 Talc is a mineral that is mined from deposits around the world, including the U.S. The softest of minerals, it's crushed into a white powder. It's been widely used in cosmetics and other personal care products to absorb moisture since at least 1894, when Johnson & Johnson's baby powder was launched. But it's mainly used in a variety of other products, including paint and plastics. 
 RELATED: Can Talcum Powder Really Cause Cancer? 
 Much research has found no link or a weak one between ovarian cancer and using baby powder for feminine hygiene, and most major health groups have declared talc harmless. Still, the International Agency for Research on Cancer classifies genital use of talc as "possibly carcinogenic." 
 Attorneys with Onder, Shelton, O'Leary & Peterson, the firm that handled the St. Louis cases, cited other research that began connecting talcum powder to ovarian cancer in the 1970s. They cite case studies showing that women who regularly use talc on their genital area face up to a 40 percent higher risk of developing ovarian cancer. 
