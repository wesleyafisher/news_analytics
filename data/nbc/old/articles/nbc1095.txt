__HTTP_STATUS_CODE
200
__TITLE
First Reads Morning Clips: Trump Takes Both Sides in GOP Civil War
__AUTHORS
__TIMESTAMP
Oct 17 2017, 8:39 am ET
__ARTICLE
 TRUMP AGENDA: Trump takes both sides in GOP civil war 
 NBC's Jonathan Allen writes that Donald Trump is wearing both uniforms in the GOP civil war -- at least for now. 
 The New York Times lede: "President Trump and Senator Mitch McConnell, the Republican leader, tried to convey a sense of harmony on Monday after months of bitter, private feuding that threatened to undermine their partys legislative push in the coming weeks to enact a sweeping tax cut." 
 "An emotional Sen. John McCain (R-Ariz.) launched a thinly veiled critique of President Trumps global stewardship Monday night, using a notable award ceremony to condemn people who would rather find scapegoats than solve problems," writes the Washington Post. "McCain said that some half-baked, spurious nationalism should be considered as unpatriotic as an attachment to any other tired dogma of the past that Americans consigned to the ash heap of history. 
 "[B]ehind the scenes, Trump, his administration and even some senators are increasingly worried that taxes will go the way of Obamacare repeal in the Senate: Months of bickering ending in extreme embarrassment," POLITICO notes. 
 The AP wraps Trump's press remarks yesterday -- and notes his new comfort speaking with journalists on the fly. 
 The Washington Post, on Trump's penchant for identifying a villain. 
 The Wall Street Journal: "The U.S. sought to stay on the sidelines as an all-out battle broke out between two of its closest ground partners in the campaign against Islamic State and raised concerns about a broader civil conflict erupting in Iraq." 
 Dartunorro Clark outlines Trump's false claim that former presidents did not call the families of fallen U.S. soldiers. 
 Carl Hulse in the New York Times: "Stymied legislatively, President Trump and Senator Mitch McConnell are turning their attention to one way they can skirt Democratic roadblocks and mollify unhappy Republicans  by filling scores of federal court vacancies." 
 More, from POLITICO: "President Donald Trump has nominated 50 candidates to lifetime appointments to the federal bench  including a man who asserted transgender children were evidence of Satans plan, one deemed unqualified by the American Bar Association and a handful of prolific bloggers.And the GOP has unanimously stuck by Trumps judges. Senate Republicans have cleared judicial nominees at a comparatively rapid clip this year  even as the conservative base has complained theyre not moving fast enough  and are planning to pick up the pace even more in the coming months." 
 "President Donald Trump said Monday that he would "look into" a report that his pick for drug czar championed a bill that effectively handcuffed federal agents from going after the Big Pharma firms that flooded the country with addictive opioids." 
 Thad Cochran's health issues are presenting a hurdle for Senate Republicans. 
 Don't miss: "Florida Gov. Rick Scott declared a state of emergency Monday in anticipation of a speech by white nationalist Richard Spencer at the University of Florida." 
 OFF TO THE RACES: Bredesen mulling Senate bid in Tennessee? 
 AL-SEN: From AL.com: "Roy Moore's Senate campaign said Monday it does not know why more than 1,000 fake Twitter accounts originating from Russia started following the former judge's account over the weekend, causing a spike in Moore's social media following." 
 NJ-SEN: "The federal corruption trial of Senator Robert Menendez resumed Monday after a judge rejected a motion by the defense to dismiss the case based on a major Supreme Court decision last year that overturned the corruption conviction against a former Virginia governor," writes the New York Times. 
 TN-GOV: "Former Gov. Phil Bredesen, the last Democrat to win a statewide race in Tennessee, is considering a bid to succeed retiring Republican Bob Corker in the U.S. Senate. Bredesen said in a statement to The Associated Press on Monday that he is mulling an entry into the race after several people urged him to reconsider his initial statements that he had no interest in running." 
 VA-GOV: New polling in the Virginia governors' race shows Northam maintaining his narrow lead over Ed Gillespie. 
