__HTTP_STATUS_CODE
200
__TITLE
Save Your Energy, Rex: Trump Tweet Undermines Sec. of State Tillerson on North Korea Talks
__AUTHORS
Kalhan Rosenblatt
__TIMESTAMP
Oct 1 2017, 9:22 pm ET
__ARTICLE
 Just one day after Secretary of State Rex Tillerson said the United States had "lines of communication" to North Korea, President Donald Trump sent out a tweet telling the secretary of state: "Save your energy." 
 "I told Rex Tillerson, our wonderful Secretary of State, that he is wasting his time trying to negotiate with Little Rocket Man...," Trump said in one tweet, adding in another: "...Save your energy Rex, we'll do what has to be done!" 
 Hours after the initial tweets, Trump tweeted again, saying that his predecessors had "failed" at solving the North Korea problem and that he would not do the same. 
 "Being nice to Rocket Man hasn't worked in 25 years, why would it work now? Clinton failed, Bush failed, and Obama failed. I won't fail," Trump wrote. 
 Trump, using his nickname "Rocket Man" for the North Korean leader Kim Jong-Un (who is just 33 years old and only took over power in 2011), sent the tweets out Sunday, undermining Tillersons Saturday admission that Washington has been probing Pyongyang for a dialogue. . 
 During a trip to China, Tillerson told reporters, "We ask: Would you like to talk? We have lines of communications to Pyongyang. Were not in a dark situation." 
 As the rhetoric between the two nations continues to spiral towards doom, Tillerson and the State Department have held high-level talks with China, a nation the United States believes is most critical in avoiding military action. 
 Related: North Korea Says Sanctions Cause Colossal Damage But Wont Work 
 U.S. officials say Beijing appears increasingly willing to cut ties to North Korea's economy by adopting U.N. sanctions, after long accounting for some 90 percent of its neighbor's foreign trade. 
 Heather Nauert, the spokeswoman for the State Department, also tweeted on Sunday, saying that while channels of communication to North Korea are open, Kim Jong-Un's window to use them is closing. 
 "#DPRK will not obtain a nuclear capability. Whether through diplomacy or force is up to the regime @StateDept," Nauert tweeted, adding, "Diplomatic channels are open for #KimJongUn for now. They won't be open forever @StateDept @potus." 
 But Trump has often talked of military options in North Korea, rather than opt for reconciliatory negotiations. 
...Save your energy Rex, we'll do what has to be done!
Being nice to Rocket Man hasn't worked in 25 years, why would it work now? Clinton failed, Bush failed, and Obama failed. I won't fail.
#DPRK will not obtain a nuclear capability. Whether through diplomacy or force is up to the regime @StateDept
Diplomatic channels are open for #KimJongUn for now. They won't be open forever @StateDept @potus
 In August, Trump said North Korea would be met with "fire and fury" if the secretive nation threatened the United States with nuclear weapons. While speaking at the United Nations in September, Trump also threatened that the United States would "totally destroy" North Korea if forced to defend itself. 
 Tensions continued between the two leaders when Kim Jong-Un called Trump a "dotard," and North Koreas foreign minister claimed the president had "declared war" against them. 
 The White House denied that the United States had declared war with North Korea. 
