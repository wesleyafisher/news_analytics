__HTTP_STATUS_CODE
200
__TITLE
Reese Witherspoon, Jennifer Lawrence Describe Abuse in Speeches
__AUTHORS
Associated Press
__TIMESTAMP
Oct 17 2017, 3:40 pm ET
__ARTICLE
 Oscar winners Reese Witherspoon and Jennifer Lawrence used their speeches at a Hollywood event honoring women to detail experiences of assault and harassment at the hands of directors and producers and pledged to do more to stop such situations. 
 Witherspoon told the audience at the Elle Women in Hollywood Awards on Monday night that the recent revelations of decades of sexual misconduct allegations against producer Harvey Weinstein had prompted her own experiences to come back "very vividly." 
 Witherspoon said she had "true disgust at the director who assaulted me when I was 16 years old and anger at the agents and the producers who made me feel that silence was a condition of my employment." 
 Witherspoon didn't name the director. Her publicist didn't immediately respond to a request Tuesday for further comment. 
 Lawrence detailed what she called a "degrading and humiliating" experience of being asked early on in her career to lose 15 pounds in two weeks for a role. She was then forced to pose nude alongside thinner women for photos that a female producer told her would serve as inspiration for her diet, she said. 
 When she tried to speak up about the demands, Lawrence said, she couldn't find a sympathetic ear from those in power. 
 Related: After Weinstein Allegations, Rose McGowan Emerges as Major Voice 
 "I was trapped, and I can see that now," Lawrence added. "I didn't want to be a whistle-blower. I didn't want these embarrassing stories talked about in a magazine. I just wanted a career." 
 Lawrence said it wasn't until she was an A-list star that she had the power to say no. 
 Witherspoon expressed similar regret at not having been more vocal about her experience, saying she has felt anxiety about being honest and guilt for not having spoken out earlier or having takeig action. 
 For her part, Lawrence pledged to be an advocate for "anyone who has ever felt threatened in this industry." 
 "We will stop this kind of behavior from happening," she said as she concluded her speech. "We will stop normalizing these horrific situations. We will change this narrative and make a difference for all of those individuals pursuing their dreams." 
