__HTTP_STATUS_CODE
200
__TITLE
Taliban Launch Wave of Attacks in Afghanistan, Killing 74
__AUTHORS
Associated Press
__TIMESTAMP
Oct 17 2017, 9:41 pm ET
__ARTICLE
 KABUL, Afghanistan  The Taliban unleashed a wave of attacks across Afghanistan on Tuesday, targeting police compounds and government facilities with suicide bombers in the country's south, east and west, and killing at least 74 people, officials said. 
 Among those killed in one of the attacks was a provincial police chief. Scores were also wounded, both policemen and civilians. 
 Afghanistan's deputy interior minister, Murad Ali Murad, called the onslaught the "biggest terrorist attack this year." 
 Murad told a press conference in Kabul that attacks in Ghazni and Paktia provinces killed 71 people. 
 In southern Paktia province, 41 people  21 policemen and 20 civilians  were killed when the Taliban targeted a police compound in the provincial capital of Gardez with two suicide car bombs. Among the wounded were 48 policemen and 110 civilians. 
 The provincial police chief, Toryalai Abdyani, was killed in the Paktia attack, Murad said. 
 Related: U.S.-Led Coalition Bombings in Afghanistan Hit Seven-Year High 
 The Interior Ministry said in a statement earlier Tuesday that after the two cars blew up in Gardez, five attackers with suicide belts tried to storm the compound but were killed by Afghan security forces. 
 Health Ministry spokesman Waheed Majroo said the Gardez city hospital reported receiving at least 130 wounded in the attack. 
 Hamza Aqmhal, a student at the Paktia University, told The Associated Press that he heard a very powerful blast that shattered glass and broke all the windows at the building he was in. The university is about 1.25 miles from the training academy, said Aqmhal, who was slightly injured by the glass. 
 A lawmaker from Paktia, Mujeeb Rahman Chamkani, said that along with the provincial police chief, several of his staff were killed. Most of the casualties were civilians who had come to the center, which also serves a government passport department, Chamkani said. 
 In southern Ghazni province, the insurgents stormed a security compound in Andar district, using a suicide car bomb and killing 25 police and five civilians, Murad said. At least 15 people were wounded, including 10 policemen, he added. 
 Arif Noori, spokesman for the provincial governor in Ghazni, said the Taliban attack there lasted nine hours. By the time the attackers were repelled, there were 13 bodies of Taliban fighters on the ground, Noori added. 
 And in western Farah province, police chief Abdul Maruf Fulad says the Taliban attacked a government compound in Shibkho district, killing three policemen. 
 The Taliban claimed responsibility for all three attacks. 
 Related: Bergdahl Pleads Guilty After Walking off Military Post in Afghanistan 
 Despite the staggering numbers, Murad said Afghan forces are confident in their "readiness to fight terrorists and eliminate them from Afghanistan." He said the Taliban have suffered heavy defeats over the past six months at the hands of Afghan forces and were seeking revenge. 
 Later on Tuesday, an Afghan official said drone strikes killed 35 Taliban fighters in the country's east, near the border with Pakistan. 
 Abdullah Asrat, spokesman for the governor of Paktia province, said drones fired missiles at four locations in Anzarki Kandaw, killing the insurgents and wounding 15 others. He said a commander of the Pakistani Taliban, Abu Bakr, and other senior insurgents were among the dead. He did not provide further details. 
 Chamkani, the lawmaker from Paktia, said the drones struck as the Taliban were collecting the bodies of 20 militants killed in a strike Monday on a militant base near the border. 
 Pakistani intelligence officials say Monday's drone strike hit a militant compound on the Pakistani side of the border, but Pakistan's army later denied any such strike on its territory. It was not immediately possible to reconcile the two accounts. 
