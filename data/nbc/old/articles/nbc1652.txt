__HTTP_STATUS_CODE
200
__TITLE
Who Knew? Hyundai Makes a Luxury Car That Gets Better Ratings Than BMW or Porsche
__AUTHORS
Paul A. Eisenstein
__TIMESTAMP
Jul 28 2017, 12:07 pm ET
__ARTICLE
 Genesis is turning out to be one of the best-kept secrets in the U.S. luxury car market. Industry data show that relatively few upscale buyers even know the spin-off from mainstream South Korean carmaker Hyundai even exists. But those who do seem positively ecstatic, with Genesis rising to the top in recent quality and customer satisfaction surveys, well ahead of more traditional luxury brands like Lexus, BMW, and Mercedes-Benz. 
 Were in a great position, Erwin Raphael, general manager of Genesis, told NBC News. 
 The G80 line  which includes two other variants  is already outselling the comparably sized Audi A6 and giving chase to the BMW 5-Series and Mercedes E-Class models. 
 Say the name, Hyundai, and youre likely to think of a compact economy car. But in recent years, the brand has moved steadily up-market, nudging into the luxury segment with its original Genesis sedan in 2009. The midsize model quickly shook up the established order, taking honors as the North American Car of the Year. Hyundai soon followed with the even bigger and more lavishly equipped Equus model. 
 Related: Hyundai Launches the Genesis Brand 
 Initially, the Korean carmaker was content to offer the two high-line models under its own badge. But that tended to confuse customers  and turn off upscale buyers who didnt want to see their cars share the same logo as a $15,000 Hyundai Accent. Two years ago, the Koreans decided it was time to spin off the Genesis brand. 
 To hold down initial investments, Hyundai decided not to require dealers to set up separate showrooms. The vast majority of Genesis retailers have simply carved out space in existing Hyundai showrooms. But thats about to change. Genesis plans to ask new retailers to set up standalone Genesis showrooms. 
 Thats not a cheap proposition. Its not unusual for luxury dealers to spend $5 million or more on a showroom these days. The question is whether it will be worth the return. When the first model to carry the winged Genesis badge debuted last year, barely 1 percent of American motorists knew what the brand was, according to industry research. That has since grown to around 4 percent - but top European models have brand awareness that runs into the mid-60 percent range. 
 That poses a serious challenge, said analyst Joe Phillippi, of AutoTrends Consulting, but one that is not insurmountable  especially considering the way Genesis has been scoring in recent owner surveys. 
 Last month, Genesis was named second-highest brand in the closely watched J.D. Power Initial Quality Survey  behind only sibling Korean carmaker Kia. It leapfrogged perennial luxury leaders like Porsche and BMW, with Mercedes-Benz, Acura, Audi and Cadillac not even coming up to the industry average in the new IQS. 
 Weeks later, Genesis landed among the leaders in the Vehicle Satisfaction Awards, an annual survey of owners by consulting firm AutoPacific. And the flagship G90 sedan not only outranked competitors like the BMW 7-Series, Lexus LS, and Mercedes-Benz S-Class, but scored higher than any product ever in the VSA. 
 AutoPacific President George Peterson called the results exceptional, and rare for a new brand and a new product to create a new benchmark for excellence. Perhaps the only other example of that happening in recent decades came in the early 1990s with the launch of the Lexus brand which quickly shook up an established luxury order then dominated by German and American brands. 
 Related: How Does a Luxury Brand Differentiate Itself? 
 That could happen again, said Peterson, pointing to the Genesis strategy of offering a high value for the dollar, while emphasizing customer satisfaction in both its products and its approach to sales and service. For one thing, buyers get free pick-up and delivery when a vehicle needs service or repairs, along with a free loaner. 
 Can Genesis repeat the success of Lexus? Not everyone is confident that can happen  at least not unless parent Hyundai is willing to invest billions of dollars to take on its well-funded luxury competitors. 
 If the new Korean luxury brand has taken any criticism its for the decision to launch with just two sedan models. That would seem to run out of sync with current market trends. Utility vehicles now make up about 40 percent of the American marketplace, and they are gaining traction in China and Korea, the two other big Genesis markets, as well. 
 I think we launched the way we should, Raphael told NBCNews. Sedans are where luxury brands make their name, he said, pointing to vehicles like the Mercedes S-Class and BMW 7-Series. 
 That said, the initial Genesis five-year plan calls for the brand to offer three sedans, two SUVs and a sporty coupe. The brand revealed a prototype version of its first ute, dubbed the GV80 Concept, at the New York Auto Show this past April. A second, smaller SUV will follow soon afterwards. 
 And Genesis is already looking to expand on that five-year plan, with Raphael revealing Genesis will look to fill the white space, other niches that German brands have been especially good at targeting. Mercedes, BMW, and Audi have launched a product offensive previously unseen in the luxury market, and they have plans to add dozens more new models before the end of the decade. 
 Its unlikely Genesis will even try to match the Germans, model-for-model. But it is betting that if it can fill high-volume segments with new products that win the same sort of kudos as the G80 and G90 have, it should be able to carve out a modest, but highly profitable, niche in the luxury market. 
 And, as happened with the Lexus, Genesis is already sending its more established competitors scrambling, trying to figure out how to respond. 
