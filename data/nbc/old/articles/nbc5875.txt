__HTTP_STATUS_CODE
200
__TITLE
Pope Francis Says Coptic Christians Killed in Egypt Bus Attack Were Martyrs
__AUTHORS
The Associated Press
__TIMESTAMP
May 28 2017, 7:00 am ET
__ARTICLE
 VATICAN CITY  Pope Francis expressed solidarity with Egypt's Coptic Christians Sunday following the attack on a bus carrying pilgrims to a remote desert monastery. 
 Francis led thousands of people in prayer Sunday for the victims, who Francis said were killed in "another act of ferocious violence" after having refused to renounce their Christian faith. 
 Speaking from his studio window over St. Peter's Square, Francis said: "May the Lord welcome these courageous witnesses, these martyrs, in his peace and convert the hearts of the violent ones." 
 ISIS claimed responsibility for Friday's attack, which killed 29 people. 
 On Saturday during a visit to Genoa, Francis prayed for the victims and lamented that there were more martyrs now than in early Christian times. 
