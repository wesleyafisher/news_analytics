__HTTP_STATUS_CODE
200
__TITLE
 Covfefe: Donald Trump Invents New Word That Conquers Twitter
__AUTHORS
Alexander Smith
__TIMESTAMP
May 31 2017, 7:16 am ET
__ARTICLE
 When you went to bed Tuesday night you hadn't heard of "covfefe." No one had. But by early Wednesday, Twitter was abuzz about little else. 
 Why? The word was accidentally invented by President Donald Trump in a tweet just after midnight ET. 
 Instantly broadcast around the world, the message confused, delighted and horrified many of Trump's 31 million followers. 
 People had questions. What does it mean? Is it a secret code? How do we pronounce it? And why did it take almost six hours for Trump or one of his advisers to delete it? 
 The message was finally removed at around 5:50 a.m. ET. But until then there had been no follow-up or explanation for the original message. 
 Just "covfefe." 
 The word quickly became a trending topic across the United States and Europe. 
 Inevitably, memes began to spring up lampooning what appeared to be an unfinished sentence and a typo for the word "coverage"  or was it? 
This is #covfefe translated from Russian. pic.twitter.com/1Im7ZzLO0Y
 Others wondered if "covfefe" had another meaning... 
"And just before you serve it, you hit it with a dash of #Covfefe" pic.twitter.com/fm9CAF4Iyz
 ...or was it some revolutionary form of communication that had gone over everyone's heads? 
Winner. #covfefe pic.twitter.com/FaCq4WjIiz
 The word sparked several parody accounts, which quickly invented their own "covfefe" folklore and backstory. 
This is everything #covfefe pic.twitter.com/wWwTImumWe
 ...or was there another explanation? 
I like to imagine that "covfefe" is the exact moment someone tackled Trump and wrestled the phone out of his hand. https://t.co/96MAwttvdL
 Others were already eyeing up potential retail opportunities. 
Knock on *45's door:"Mr. Resident, would you like the good news first or the bad?""Good news.""One of your tweets went viral..."#covfefe pic.twitter.com/dmuIs3DYVK
maybe he's born with itmaybe it's #covfefe pic.twitter.com/MK0f3DcDLz
 White House Press Secretary Sean Spicer said Wednesday afternoon that, "I think the president and a small group of people know exactly what he meant." 
 It was far from the first time Trump has been guilty of firing off late-night typo-strewn messages from his official account  although it may have been the most spectacular. 
 Away from the hilarity, many wondered why the message took hours to be deleted. Would he or his aides be as slow to remove a mistaken message that was inadvertently problematic in its content? 
 In the end, Trump finally posted a follow-up message that appeared to laugh at his own mistake. 
Who can figure out the true meaning of "covfefe" ???  Enjoy!
 But that hasn't stopped thousands of people from continuing to enjoy "covfefe." 
covfefe pic.twitter.com/2kIHlF0H1x
 
