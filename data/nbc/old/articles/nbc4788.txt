__HTTP_STATUS_CODE
200
__TITLE
Bowing to Market Pressure, Ferrari Is Making an SUV
__AUTHORS
Paul A. Eisenstein
__TIMESTAMP
Oct 10 2017, 4:36 pm ET
__ARTICLE
 Ferrari, a brand that has long dominated on track and street with its iconic sports car, cant seem to resist the rise of the sport-utility vehicle. 
 Sergio Marchionne, who heads both the Italian automaker and Fiat Chrysler Automobiles, appeared to confirm that an SUV is on the way for Ferrari as he spoke to reporters at the New York Stock Exchange on Monday. The sports car maker had been one of the few remaining holdouts as exotic competitors like Aston Martin and Lamborghini have begun work on SUVs of their own. 
 Were dead serious about this, Marchionne said during his comments, taking things a step further than he had gone during an August earnings call when he hinted a Ferrari SUV might probably happen. 
 Related: The New, Entry-Level Ferrari Portofino 
 The decision to add an SUV  or what Marchionne Monday referred to as an FUV  would be a significant move for Ferrari. The brand has long stayed true to its exotic roots, and doesnt even offer a four-door model, unlike such competitors as Porsche, where the coupe-like Panamera sedan is now one of its most popular offerings. The closest Ferrari has so far come to straying is with the wagon-like GTC4 Lusso  originally known as the FF. Even there it only offers two doors for passengers, as well as a hatch. 
 But Ferrari appears to have recognized the inevitably of the ute in a rapidly changing market. In the U.S., which has long been the worlds dominant outlet for utility vehicles, light trucks now account for nearly two-thirds of new vehicle sales. And traditional SUVs and more modern crossover-utility vehicles, or CUVs, make up the bulk of that. Other markets, notably China, are now picking up on the trend. 
 That has led to a number of surprise announcements. Bentley already offers the big Bentayga which is rapidly becoming its top model. Aston Martin unexpectedly unveiled the DBX concept vehicle at the 2015 Geneva Motor Show and soon after confirmed it would put a version of the ute into production before the end of this decade. Even Ferraris direct rival, Lamborghini, is working up a utility vehicle model  as is the very traditional Rolls-Royce. 
 Of course, Ferrari can look closer to home to understand the value of adding an SUV. Ferrari was spun off in 2015, but FCAs two remaining high-line Italian brands, Alfa Romeo and Maserati, have introduced their own utility vehicles over the last 18 months and both are generating strong  and growing  demand. 
 Related: Check out the Aperta, Ferraris Fastest Convertible 
 Even before spinning off from Chrysler, Ferrari seemed to be shifting gears. After a flap that saw the ouster of long-time brand chief Luca di Montezemolo, Marchionne took the reins at the company, which is based just outside the city of Modena. The two reportedly had argued about Ferraris long-held strategy of severely limiting volume, even at the expense of profits. Under Marchionne, Ferrari has begun ramping up production, and that would likely continue with the introduction of an SUV. Aston, for example, believes the DBX could virtually double its size. 
 We need to learn how to master this whole new relationship between exclusivity and scarcity of product, Marchionne explained during his appearance at the New York Stock Exchange. Then were going to balance this desire to grow with a widening of the product portfolio." 
