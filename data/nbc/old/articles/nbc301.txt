__HTTP_STATUS_CODE
200
__TITLE
Whatever Harvey Weinstein Is, He Is No Sex Addict, Experts Say
__AUTHORS
Maggie Fox
__TIMESTAMP
Oct 12 2017, 5:49 pm ET
__ARTICLE
 Its happened again: A powerful, famous man is accused of improper, even criminal, sexual behavior and says he's seeking therapy to deal with it. 
 "Guys, I'm not doing OK," Hollywood mogul Harvey Weinstein said Wednesday in a video licensed to NBC News. "I'm trying. I've got to get help." 
 While there's no official confirmation of where Weinstein is headed or what type of therapy he may be getting, experts contacted by NBC News said that whatever he may have done, hes not a sex addict. And most say theres no such thing as sex addiction. 
 Psychiatrists debated whether to include sex addiction as a diagnosis in the latest edition of the Diagnostic and Statistical Manual of Mental Disorders (DSM-V), and opted not to. 
 I am not sure when being a selfish, misogynistic jerk became a medical disorder, said David J. Ley, a clinical psychologist in Albuquerque, New Mexico, and the author of "The Myth of Sex Addiction." 
 This is a concept that has been used to explain selfish, powerful, wealthy men engaging in irresponsible impulsive sexual behavior for a long time, Ley said. 
 On Thursday, Weinstein's younger brother, Bob Weinstein, called Harvey "a world class liar" who has not yet actually sought therapy. 
 My brother Harvey is obviously a very sick man," Bob Weinstein said in a statement to NBC News. "Ive urged him to seek immediate professional help because he is in dire need of it. His remorse and apologies to the victims of his abuse are hollow." 
 Related: Sex addiction, or an excuse? 
 Several actors, including Angelina Jolie and Gwyneth Paltrow, as well as journalists, models and other women involved in the film industry have accused Weinstein of making unwanted sexual advances and some even have alleged rape. Weinstein was fired from the Weinstein Company, the independent film studio he founded with his brother, and his wife, Georgina Chapman, said she had left him. 
 "I support her decision," Weinstein said in a statement. "I am in counseling and when I am better, we can rebuild." 
 Related: Weinstein Says He's Not Doing OK 
 Weinstein's spokesperson said he had begun counseling  without saying where or what kind  and "has listened to the community and is pursuing a better path." 
 "Mr. Weinstein is hoping that, if he makes enough progress, he will be given a second chance. 
 Holly Richmond, a psychologist and licensed family therapist in Los Angeles, said that while therapy of some kind is called for  I support him getting help, she said  sex addiction therapy would not be a legitimate route. 
 Saying it is sex addiction is a misnomer," Richmond said. "There is no such thing as sex addiction." 
 Therapists are divided on whether famous men caught in sexual scandals have treatable conditions or if they even deserve sympathy. 
 Ley says no to both. 
 We see this parade of men getting caught engaging in this type of hypocrisy," he said. "Then they claim their behavior is the result of this alleged disorder when we all know these were men who were abusing their power and privilege." 
 Related: Reporter Says Weinstein Ordered Her to 'Be Quiet' While He Exposed Himself 
 And Ley doubts treatment centers offer anything useful to such men. 
 After 40 years of sex addiction treatment, there is still no published evidence that this treatment works, he said. This is an exploitive industry that loves to capitalize on these celebrity sex addiction scandals so they can get referrals. 
 Dr. Dawn Michael, a sex therapist in Thousand Oaks, California, agrees that many men abuse their power and that it has little to do with sex. 
 The problem that I see with the label of sex addiction is that it is taking away the responsibility for the person and putting it into the idea that it a disorder and that he didnt have control over it, Michael said. 
Harvey Weinstein seeking #SexAddiction treatment. Are you kidding? Excuses for misogyny and exploitation
 If he goes to a clinic, he is basically giving up responsibility for his bad behavior, she added. 
 To me, this man had power. He had money. He had clout. He got off on the idea that he could get these women to do what he wanted, more so than the sex itself. I think he got excited at the fact that they said no, that they resisted. 
 Related: Prosecutors Point Fingers Over Weinstein Probe 
 Michael Bader, a psychologist and psychoanalyst in San Francisco who has written on the issue, is a little more sympathetic. 
 Someone like Weinstein who uses his social/economic power to get women to have some sort of sex with him  the sexual experience might function as a reassurance to him that he is desirable, worthwhile, masculine, etc, Bader said via email. 
 It might offer an antidote to private feelings of loneliness or disconnectedness. It might even  wait for it  reassure him that he can make a woman happy. A man might hire a prostitute, for example, and know on one level that she is faking arousal for the money, but on the other level, the level that counts, he lets himself believe that she loves it. Who is to say that Weinstein wasn't in this same position? 
 Related: Is There Any Such Thing as Sex Addiction? 
 Some experts argue that sex addiction is real, but say that, based on media reports, that doesnt appear to be what was going on with Weinstein. John Giugliano, a psychologist in Bellmore, New York, and a spokesman for the Society for the Advancement of Sexual Health, prefers the term out-of-control sexuality. 
 Not all sex addicts are sex offenders, Giugliano said. But he could just simply be a sex offender. Both are legitimate and real disorders that many people suffer with. Sex addiction is very different and it is driven by different motives usually, which are complicated. Its not about morals. 
 Richmond, who also works with sexual abuse victims, says there is a second group who could benefit from therapy and from simply making their experiences public, and those are the women who have accused Weinstein. 
 I am really happy this is seeing the light of day, she said. Most of the time when survivors can find their voices, it is helpful to them in the healing process." 
 Richmond added, Especially in an industry like the entertainment industry, when your job is on the line, it's scary for these women to come out. 
