__HTTP_STATUS_CODE
200
__TITLE
Otto Warmbiers Parents Rip N. Korea as Terrorists Who Destroyed Son
__AUTHORS
Adam Edelman
__TIMESTAMP
Sep 26 2017, 9:06 am ET
__ARTICLE
 The parents of Otto Warmbier, the American college student who died after being released from a North Korean prison in June in an unconscious state, slammed the regime in Pyongyang on Tuesday as terrorists and not a victim. 
 They kidnapped Otto, they tortured him, they intentionally injured him, Fred Warmbier told Fox News Fox & Friends, with his wife, Cindy. "They are not victims. They are terrorists." 
 Now we see North Korea claiming to be a victim and that the world is picking on them, he added. Were here to tell you that theyre not a victim, theyre terrorists. 
 The Warmbiers recalled the harrowing details of seeing Otto aboard a military aircraft in Cincinnati, where he was transported after his release. 
 Photos: Thousands Mourn Otto Warmbier at Ohio Funeral 
 We got halfway up the steps, we heard this howling, involuntary, inhuman sound. We werent really certain what it was, Warmbier said. Otto was on the stretcher  and was jerking violently making these inhuman sounds. 
 He was blind, he was deaf, the grieving father continued. It looked like someone had taken a pair of pliers and rearranged his bottom teeth. 
 Cindy Warmbier revealed that the sight of her mangled son almost caused her to pass out. 
 They destroyed him, she said. 
 Warmbier was detained in Pyongyang in January 2016 while on a tourist trip to North Korea. He was charged with committing a hostile act against the government after officials said he had tried to steal a propaganda banner from a hotel and was convicted and sentenced to 15 years of hard labor. 
 The North Korean government released him in June, and when he returned to U.S. soil, doctors found him to be in a state of unresponsive wakefulness. He died days later; he was 22. Fred and Cindy Warmbier were told he had been in a coma since shortly after being sentenced. 
 Their account of Otto's treatment comes amid Pyongyang's continued nuclear provocations and as President Donald Trump and the North Korean dictator Kim Jong Un ratchet up their war of words. 
Great interview on @foxandfriends with the parents of Otto Warmbier: 1994 - 2017. Otto was tortured beyond belief by North Korea.
 Trump tweeted praise for Fox & Friends on Tuesday, saying that Otto was tortured beyond belief by North Korea. 
