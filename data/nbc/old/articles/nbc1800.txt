__HTTP_STATUS_CODE
200
__TITLE
Hurricane Irma Is Bigger, Stronger Than Hurricane Andrew, Florida Gov. Warns
__AUTHORS
Kalhan Rosenblatt
Elizabeth Chuck
__TIMESTAMP
Sep 6 2017, 5:37 pm ET
__ARTICLE
 Deadly hurricane Irma is "bigger, faster and stronger" than Hurricane Andrew, the devastating Category 5 hurricane that caused major damage in the Sunshine State a quarter-century ago, Florida Gov. Rick Scott warned on Wednesday. 
 Andrew, which made landfall in Homestead, Fla., in 1992, left a path of destruction, costing an adjusted $47.8 billion in damage and leaving 65 dead. Another 43 indirect deaths were attributed to Andrew, according to Weather.com. 
 It was one of only three Category 5 hurricanes on record to make landfall in the continental United States. 
 But Scott was adamant on Wednesday: Irma could be worse. 
 "Hurricane Andrew is one of the worst storms in the history of Florida," Scott said at a Wednesday evening press conference. "This is much worse and more devastating on its current path." 
 Irma first made landfall as a Category 5 storm in the Caribbean early Wednesday. It killed at least two people and seriously injured two others on the islands of St. Barts and St. Martin, a French official said. 
 At least one person also died in Barbuda, said Midcie Francis, a spokeswoman for Antigua & Barbudas National Office of Disaster Services. 
 Widespread flooding and power outages were reported on all three islands. 
 In Florida, mandatory evacuations began on Wednesday for visitors to Monroe County, which encompasses the southernmost point of the state, including the Florida Keys. A mandatory evacuation order was issued Wednesday night for some parts of Miami-Dade County, including the barrier island communities like Miami Beach. 
 Evacuations so far have been "very orderly," Monroe Sheriff Rick Ramsay said. Heavy traffic had thinned out, and the only issue was fuel shortages, he said. In Miami-Dade County, those with special needs and residents living in low-lying areas began leaving on Wednesday morning. 
 "Do not ignore evacuation orders, Scott said earlier Wednesday. Remember: We can rebuild your home, but we cannot rebuild your life. 
 "This is not a storm you can sit and wait through," the governor warned. 
 Storm surge and winds pose the greatest threat, Scott said. The exact path of the hurricane, which had 185 mph winds Wednesday, is not precisely known but whether it hits Florida on the west or east coast the Florida Keys will be affected and could begin feeling the effects of Irma Friday night or early Saturday, Scott said. 
 All hospitals in the Florida Keys will be evacuated Wednesday, Scott said. "If youre in the Keys, there is no high ground. There's only one way out," the governor said. 
 "So you shouldn't be taking risks like that. You need to listen to local officials and get out," he said. In the middle of the storm, first responders can't get to you. You've got to decide now to get out." 
 As South Floridians scramble to prepare for Irmas impact, many supermarkets have struggled to keep up with demand, leaving many store shelves barren and gas pumps empty. 
 On social media, Floridians posted pictures of vacant grocery store shelves, while others said they had to drive for over an hour to find gas. 
 Scott told reporters he was aware of the shortages. "One of our top priorities right now is fuel availability," Scott said. "We're moving as much fuel through the system as fast as possible." 
 On Wednesday morning, the Broward Sheriffs Office tweeted out a photo of a line of people waiting for a Publix Supermarket to restock with water. 
 "Ahead of #HurricaneIrma at the @Publix in Fort Lauderdale, FL, empty shelves where there should be water, paper towels and toilet paper," Justin Michaels, a Weather.com reporter, tweeted. 
 Schools in Miami-Dade, Broward, Collier, Lee, Indian River, Martin, Hernando and St. Lucie counties announced they would be closed starting on Thursday. Schools in Monroe County had closed as of Wednesday. 
 Florida Atlantic University in Boca Raton and Florida International University in Miami canceled classes from Wednesday until Sunday in preparation for the storm. 
 By Wednesday, the only major airport announcing a suspension in operations was Key West International Airport. Two others, Miami International and Fort Lauderdale-Hollywood International, both posted on Twitter that while they were monitoring Irma, operations had not been affected. 
 Scott said 1,000 members of the Florida National Guard have been activated to assist with preparation, and by Friday the remaining 6,000 members would be reporting for duty. 
 Thirteen helicopters and more than 1,000 high-water vehicles are on standby, and with resources from other states 30,000 troops, 4,000 trucks and 100 helicopters and air evacuation crews are ready to support Florida, Scott said. 
 Members of the North Carolina National Guard had begun assisting with evacuations from hospitals in the Florida Keys. Mercy Hospital in Miami said Wednesday that as a precaution it was moving 200 patients and closed the emergency room to new patients. 
 Scott said President Donald Trump had approved a pre-landfall emergency declaration for Florida in order to free up federal funding for the state as necessary. 
 "This is a life-threatening storm, and protecting life is absolutely our first priority," Scott said. 
 By Wednesday afternoon, South Carolina Governor Henry McMaster had also declared a state of emergency in preparation for Irma's path up the east coast. 
 Related: Hurricane Irma Passes Over Barbuda, Heads Toward Puerto Rico 
 In anticipation of the rising floodwaters, the U.S. Army Corps of Engineers Jacksonville District began releasing water from Lake Okeechobee, Florida's largest freshwater reserve, and the South Florida Water Management District began lowering water levels in canals to prevent overflowing. 
