__HTTP_STATUS_CODE
200
__TITLE
ISIS Fighters Returning From Iraq, Syria May Unleash Europe Attacks: Cops
__AUTHORS
Eoghan Macguire
__TIMESTAMP
Dec 2 2016, 1:03 pm ET
__ARTICLE
 ISIS may step up attacks in Europe as it loses ground in the Middle East and foreign fighters return to their home countries, a new report from Europol warned Friday. 
 The continental law enforcement agency said that car bombs, extortion and kidnappings could be employed by networked groups of fighters or lone wolf attackers who have pledged allegiance to the group or have been inspired by it. 
 However, attacks using guns, knives and vehicles remain more likely given the easier access to such weaponry, according to Europol. Attacks on major infrastructure such as power stations and nuclear facilities also remains unlikely given ISIS' preference for soft targets," it added. 
 It is possible ISIS will consider the use of chemical and/or biological weapons" in Europe while it was conceivable that homemade, commercial or military explosives placed in improvised devices could be used at some stage, according to the report. 
 "We have to be vigilant, since the threat posed by ... [ISIS] and returning foreign fighters is likely to persist in the coming years," said the E.U. Counter-terrorism Coordinator Gilles de Kerchove in a statement accompanying the report. "These people are trained to use explosives and firearms and they have been indoctrinated by the jihadist ideology." 
 ISIS is under increasing pressure in Syria and Iraq, where its last major stronghold, Mosul, is under assault by a coalition of U.S.-backed forces. 
 The Europol report also warns that some intelligence services indicate that several dozen people who are directed by ISIS and have the capability to commit terrorist acts may currently be in Europe and that other groups such as al Qaeda also pose a threat. 
 The report added that an increase in arrests and court proceedings related to terrorism "of a jihadist nature" was proof of the high priority that had been given to counter-terrorism across the continent. 
 European nations have been on alert in the wake a series of attacks attributed to ISIS or its followers across the continent in recent years. Terrorists have already carried out attacks in Belgium, France, Germany, and Turkey this year. 
 Related: German Military, Police to Team Up Amid Fears of ISIS Attack 
 Earlier this month, the State Department issued a warning to U.S. nationals to exert caution at festivals and holiday markets in Europe. 
 In October, meanwhile, U.S. officials told NBC News that the military campaign to push ISIS out of its stronghold in Mosul may lead to hundreds of battle-hardened fighters returning to Europe where they could launch terrorist attacks against allied and U.S. interests. 
 Europol did not specify how many ISIS fighters it estimated could make their way back to Europe. About one-fifth of ISIS's total number of fighters, or 3,700 people, are residents or nationals of Western Europe, including 1,200 from France alone, according to a study last year by King's College London. 
