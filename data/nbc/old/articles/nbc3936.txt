__HTTP_STATUS_CODE
200
__TITLE
For Facts Sake: U.S. Healthcare Lags Others
__AUTHORS
Ali Velshi
__TIMESTAMP
Jul 18 2017, 9:44 am ET
__ARTICLE
 America's health care system is the envy of the world. Yet, Americans pay more for health care than their peers in other developed countries, but end up with health outcomes that are less. 
 
