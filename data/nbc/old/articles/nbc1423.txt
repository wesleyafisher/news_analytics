__HTTP_STATUS_CODE
200
__TITLE
ISIS Attack in Kirkuk Is Reality Check Amid Mosul Offensive 
__AUTHORS
Alexander Smith
__TIMESTAMP
Oct 21 2016, 10:32 am ET
__ARTICLE
 After a week of successfully repelling ISIS near the Iraqi city of Mosul, Friday brought a reality check. 
 Backed by the U.S., Iraqi and Kurdish forces have made four days of significant gains in their attempt to oust the extremists from their regional stronghold. 
 On Friday, however, ISIS militants launched a surprise assault on the Iraqi city of Kirkuk, far away from the frontlines some 90 miles to the southeast. 
 According to experts, this was most significant evidence yet that ISIS' territorial losses around Mosul are forcing it to become more to focused on popup attacks rather than holding ground. 
 This means that even when ISIS has been vanquished from Mosul, it will be far from defeated. 
 "We need to manage our expectations," said Hayder al-Khoei, visiting fellow at the European Council on Foreign Relations, a London-based think tank. "When we talk about the defeat of ISIS ... they are not just going to vanish. They're going to go back to trying to wreaking havoc, not just in Iraq but across the world." 
 He added that "we're going to have to prepare for these type of attacks to happen [in Iraq] much more frequently." 
 Shiraz Maher, another London-based expert, agreed. 
 "As Mosul is being encircled and attacked, ISIS is going to do things like this to pull focus away from their defeats in the heartland and effect huge damage," said Maher, who is deputy director at the International Centre for the Study of Radicalisation and Political Violence, part of Kings College London. 
 "They also want to reverse the narrative," he added. "Their own propaganda has all been about Kirkuk, with very little about Mosul." 
 The attack on Kirkuk saw militants armed with guns and grenades fire on several government buildings in the city. Eyewitnesses and local Kurdish television reported gunfire and explosions although no casualties were immediately reported. 
 Meanwhile, some 30 miles outside the city, ISIS suicide bombers stormed a power plant and killed three Iranian engineers and eight Iraqi technicians, a senior Iraqi security official told NBC News. 
 Both the government and the ISIS-run Amaq news agency said the extremist group were behind the attack. 
 Kirkuk is an oil-rich city that near a pocket of Iraq that's still under ISIS control. 
 Al-Khoei said it was highly likely that the fighters involved in Friday's assault were members of ISIS sleeper cells who were living inside the city. 
 "It's likely that they have been activated now because of how successful the Mosul operation has been," he added. 
 Friday's attack on Kirkuk mirrors a devastating bombing in Baghdad in July that many saw as ISIS' revenge for losing control of the city of Fallujah to Iraqi security forces. 
 This bombing  along with attacks in Turkey, Bangladesh and elsewhere  provided evidence for experts as early as this summer that losses on the battlefield were forcing ISIS to act less like a state and more like a hit-and-run terror group. 
 These pattern is likely to increase after ISIS is squeezed out of Mosul and pushed underground. According to Maher this will mark the beginning of the most difficult stage of the fight. 
 "This idea that you can defeat ISIS by taking over Mosul, it's like 2003: You can put U.S. tanks in Baghdad, but then what?" said Maher, alluding to the troubled aftermath of the invasion that toppled Saddam Hussein. "That's when the really nasty insurgency will begin." 
 He added that he was "sure ISIS is going to have some spectaculars," referring to the group's penchant for high-profile attacks. 
