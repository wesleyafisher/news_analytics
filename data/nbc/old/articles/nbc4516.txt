__HTTP_STATUS_CODE
200
__TITLE
Caught in a GOP Civil War, Trump Picks Both Sides
__AUTHORS
Jonathan Allen
__TIMESTAMP
Oct 16 2017, 4:58 pm ET
__ARTICLE
 WASHINGTON  President Donald Trump is wearing both uniforms in the Republican civil war  at least for now. 
 At a Monday morning Cabinet meeting, Trump empathized with Steve Bannon, the former White House strategist who has called for the ouster of Senate Majority Leader Mitch McConnell and other Republican lawmakers who don't fall in line behind the Trump agenda. 
 "I can understand where Steve Bannon's coming from," the president said. "Steve is doing what Steve thinks is the right thing." 
 Just hours later, at a free-wheeling White House news conference, Trump insisted there's not much distance between him and McConnell, who underscored the message by standing at the president's side after the two had lunch together. 
 "We are closer than ever before, and the relationship is very good," Trump said. "We are fighting for the same thing." 
 Usually, it's Trump who loves to feud and hates to de-escalate. But he may have the most to lose in this fight. 
 If he hopes to salvage any elements of his legislative agenda  most immediately, a slow-moving push to rewrite the tax code  he'll need Republicans on Capitol Hill to be unified behind him, GOP strategists say. His spats with a string of Republican senators  and the threat that Bannon will marshal anti-establishment forces to defeat them in primaries  puts his priorities at risk. 
 "What's clear is that Trump knows if he wants to have any kind of legislative victory, much less a legislative legacy, he has to work with Mitch McConnell to make that happen," said Republican strategist Doug Heye. 
 "The question is how short term this will be," Heye added of Trump's newfound desire for detente. "It's hard to see, just how given Trump has been thus far, that this is going to be a long-term answer." 
 Reed Galen, who served as deputy campaign manager for John McCain's 2008 presidential bid, said Trump is torn between his heart, which is with Bannon and those who seek to disrupt the GOP establishment, and his head, which must be telling him that he needs McConnell for the time being. 
 "Even the president, as unique a style as he has, I think he does have divided loyalties here," Galen said. 
 On one hand, Trump is frustrated with the inability of GOP majorities in Congress to deliver on his agenda. 
 Trump's Monday love tap notwithstanding, McConnell, who couldn't get Senate Republicans to pass a repeal of Obamacare, has been the target of the president's ire and of Bannon's band of anti-establishment forces. 
 On the other hand, Trump doesn't have the luxury of waiting until the 2018 midterm elections are over to show that he can govern. He needs action now, before lawmakers turn their focus to electoral politics next year. That makes McConnell an important figure, even if Trump's base would like to sack him. 
 Trump and McConnell need each other, but that doesn't mean Trump will stand in the way of Bannon's efforts to put pressure on Republican senators by backing primary challengers. 
 "I wouldnt expect the president's going to do anything to get him to back off," Galen said. 
