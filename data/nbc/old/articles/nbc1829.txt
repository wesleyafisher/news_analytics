__HTTP_STATUS_CODE
200
__TITLE
Texas Woman Dies After Contracting Flesh-Eating Infection in Harvey Floodwaters
__AUTHORS
Elizabeth Chuck
__TIMESTAMP
Sep 28 2017, 12:58 pm ET
__ARTICLE
 A Texas woman has died after she contracted a flesh-eating infection when she fell into Hurricane Harvey's floodwaters, officials say. 
 Nancy Reed, 77, died on Sept. 15 after falling in a flooded home earlier in the month in Houston's Kingwood neighborhood, the Harris County medical examiner said this week. She contracted necrotizing fasciitis, a skin infection that kills the body's soft tissue, through a wound she got from the fall, Houston Health Department spokesman Porfirio Villarreal said Thursday. 
 Necrotizing fasciitis is a rare disease caused by various types of bacteria, most commonly group A strep, according to the Centers for Disease Control and Prevention. While infections from group A strep are generally mild, in the case of necrotizing fasciitis, the bacteria spread quickly and can become deadly in a short period of time without prompt treatment. 
 Those with weakened immune systems and other health problems, like diabetes and kidney disease, are more susceptible to getting the infection, the CDC says. The agency says there are anywhere from 700 to 1,100 cases each year in the U.S. 
 Villarreal was not aware of any other cases of the disease in Houston since the hurricane. However, in nearby Missouri City, Texas, first responder J.R. Atkins was also infected with necrotizing fasciitis earlier this month while he tried to rescue victims from Harvey's floodwaters, reported NBC affiliate KPRC. He recovered. 
 Reed's death is the county's 36th Harvey-related fatality, according to the Harris County Institute of Forensic Science. 
 Related: Mosquitoes, Medicine and Mold: Texas Battles Post-Harvey Health Issues 
 Harvey made landfall in August as a Category 4 storm. Health officials had feared its historic levels of rain would create a breeding ground for dangerous waterborne illnesses. 
 While the floodwaters have mostly receded, Villarreal urged Texans to still take precautions. 
 "A lot of people are working on their homes, and so it is important to clean up your home, but do it at a pace where you don't injure yourself," he said. "It's really important if anybody has a wound just to keep it clean and covered up with dressing and bandages, and make sure that those bandages and dressing are clean. That way, it doesn't become infected." 
