__HTTP_STATUS_CODE
200
__TITLE
ISIS-Linked Militants in Philippines Plotted to Set Marawi Ablaze
__AUTHORS
The Associated Press
__TIMESTAMP
May 30 2017, 4:32 am ET
__ARTICLE
 MARAWI, Philippines  ISIS-aligned militants plotted to burn a city of 200,000 people to the ground after occupying it, according to an official in the Philippines. 
 The country's military said Monday that it was close to retaking the southern city of Marawi, where Islamist gunmen have been fending off the army for a week. About 100 militants, troops and civilians have been killed. 
 Military chief of staff Gen. Eduardo Ano told The Associated Press on Tuesday that the extremists had planned to set besieged Marawi entirely ablaze in a bid to project ISIS' influence. 
 He said the fighters also sought to kill Christians in nearby Iligan city during Ramadan, the Muslim holy month of fasting. 
 Related: Duterte Declares Martial Law to Tackle Rampaging Rebels 
 "They wanted to show the world that there is an ISIS branch here which can inflict the kind of violence that has been seen in Syria and Iraq," Ano said. 
 The violence erupted last week after the army raided the hideout of Isnilon Hapilon, who has been designated by ISIS as its leader in the Philippines. 
 He is on Washington's list of most-wanted terrorists with a $5 million reward for information leading to his capture. 
 Hapilon escaped and militants called for reinforcements and around 100 gunmen loyal to him entered the mostly Muslim city, torching buildings and taking hostages. 
 Over the past week, the ISIS-linked fighters have shown their muscle, withstanding a sustained assault by the Philippine military. 
 President Rodrigo Duterte subsequently declared martial rule for 60 days in the entire Mindanao region  which is home to 22 million people and the size of South Korea  and vowed to be "harsh." 
 The army insists the drawn-out fight is not a true sign of the militants' strength, and that the military has held back to spare civilians' lives. 
 As of Tuesday morning, Ano said the military, working house-by-house, had cleared 70 percent of the city and the remaining fighters were isolated. 
 Still, the fighters have turned out to be remarkably well-armed and resilient. 
 Experts have warned that as ISIS is weakened in Syria and Iraq, battered by years of American-led attacks, Mindanao could become a focal point for regional fighters. 
 The bloodshed in Marawi has raised fears that extremism is growing as smaller militant groups unify and align themselves with ISIS. 
 Rohan Gunaratna, a terrorism expert at Singapore's S. Rarajatnam School of International Studies, said the fighting in Marawi, along with smaller battles elsewhere in the southern Philippines, may be precursors to declaring a province, which would be "a huge success for the terrorists." 
 Muslim rebels have been waging a separatist rebellion in the south of the predominantly Roman Catholic nation for decades. 
 In 1980, Marawi proclaimed itself an "Islamic City" and it is the only city in the country with that designation. 
 The largest armed group dropped its secessionist demands in 1996, when it signed a Muslim autonomy deal with the Philippine government. Amid continuing poverty and other social ills, restiveness among minority Muslims has continued. 
