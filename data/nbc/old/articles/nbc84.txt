__HTTP_STATUS_CODE
200
__TITLE
Harvey Weinstein Resigns From His Company Following Scandal
__AUTHORS
Claire Atkinson
__TIMESTAMP
Oct 17 2017, 7:51 pm ET
__ARTICLE
 Harvey Weinstein resigned from the board of his company on Tuesday, according to a statement from the Weinstein Co. 
 Weinstein, shunned by Hollywood after allegations of rape, assault and sex harassment, doesnt think he violated his contract but agreed to step down from the Weinstein Co., according to source knowledgeable of the meeting. He was fired October 8 by the firm as co-chairman, but remained on the board. The company's board on Tuesday ratified its decision to fire him. 
 Based in New Yorks Tribeca Film Center, the Weinstein Co. is known for Oscar-winning movies such as the Hateful Eight, and The Kings Speech. It is currently in discussions about a possible sale of the firm to private equity company, Colony Capital run by Thomas Barrack, a friend of President Donald Trump. The Weinstein Co. announced Monday in a statement that it "has entered a negotiating period with Colony Capital for a potential sale of all or a significant portion of the company's assets." 
 Related: NYPD Probing Two Allegations Against Harvey Weinstein 
 Actor Rose McGowan accused Weinstein of raping her. Other actresses including Gwyneth Paltrow, Angelina Jolie and a host of others have said Weinstein harassed them. A spokeswoman for Harvey Weinstein has previously denied all allegations of nonconsensual sexual encounters. 
 Weinstein Co., currently run by Harvey's brother, Bob Weinstein, and Chief Operating Officer David Glasser, was expecting to hear from the embattled mogul, who said he believed he could make a comeback after attending a sex addiction rehab clinic. 
 Barracks Colony Capital said Monday it is negotiating to acquire the company and has given it funds to keep it in business, though it has an exclusive negotiating window. 
 There are currently just three board directors, as six others quit the company subsequent to the initial Weinstein allegations becoming public. The three remaining directors are Bob Weinstein, Tarak Ben Ammar and WPP Groups Lance Maerov. 
 Related: Motion Picture Academy Ousts Weinstein 
 Just moments after the board meeting concluded, Variety reported on an accusation against Bob Weinstein. Amanda Segel, executive producer of The Mist, a Spike TV series said he wouldnt take no for an answer when she said she had no interest in dating him. 
 'No should be enough, Segel told Variety. After no, anybody who has asked you out should just move on. Bob kept referring to me that he wanted to have a friendship. He didnt want a friendship. He wanted more than that. My hope is that no is enough from now on. 
 Bert Fields, a lawyer for Bob Weinstein, denied the allegations. 
 "Varietys story about Bob Weinstein is riddled with false and misleading assertions by Ms. Segel and we have the emails to prove it. 
 "But even if you believe what she says it contains not a hint of any inappropriate touching or even any request for such touching. There is no way in the world that Bob Weinstein is guilty of sexual harassment, and even if you believed what this person asserts there is no way it would amount to that. 
