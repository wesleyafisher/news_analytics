__HTTP_STATUS_CODE
200
__TITLE
NYC Father and Son Busted for Alleged Opioid Dealing on the Darknet
__AUTHORS
Tom Winter
Corky Siemaszko
__TIMESTAMP
Aug 23 2017, 3:18 pm ET
__ARTICLE
 A father and son team from New York City has been busted for allegedly using the so-called dark web to sell potentially deadly fentanyl and oxycodone. 
 Using the vendor name Zane61, Michael Luciano and his son Phillip allegedly set up shop in a place where some criminals think they can hide by trying to conceal their identity and transactions, Acting Manhattan U.S. Attorney Joon Kim said in a statement. 
 Now Michael Luciano, 58, and his 29-year-old son are each charged with conspiring to distribute and possession with intent to distribute two controlled substances. 
 On the fentanyl charge, they each face anywhere from five to 40 years in prison, the feds said. If convicted on the oxycodone charge, they each face a maximum of 20 years in prison. 
 Fentanyl and other deadly opioids continue to plague far too many American communities because the unscrupulous dealers believe their surreptitious online activities escape the reach of law enforcement, said Angel Melendez, special agent-in-charge of the Homeland Security Investigations (HSI) in New York City. The arrests of these two defendants prove that notion false. 
 The Lucianos live together in the borough of Staten Island. And from February 2016 through July 2017, according to officials, they allegedly peddled opioids over an online black market called AlphaBay. 
 It was the younger Luciano who knew how to use the dark web, set up the account, and handled the technological aspects of their transactions, they said. 
 And apparently they had plenty of satisfied customers. 
 Great stealth, fast shipping, legit product, one wrote on June 24, according to the complaint. Perfect 10/10. 
 "A+ product," wrote another on June 26. 
 Unknown to them, federal investigators were already in the process of shutting the Lucianos and their alleged drug operation down. 
 In March, they intercepted a package from China bound for Staten Island to a "Mike Luciano" and inside they found 64 grams of fentanyl and 37 grams of an "unknown powdered substance," the complaint states. 
 The next month, federal investigators probing an accidental fatal overdose in South Carolina found records the allegedly showed Zane61 making at least seven drug purchases from an AlphaBay vendor with the user name PeterTheGreat. 
 In June, undercover HSI officers made a fentanyl purchase from Zane61 and had it shipped to an address in the Bronx, another New York City borough. 
 "HSI officers paid Zane61 in Bitcoin for this fentanyl purchase," the complaint states. 
 The next month, HSI agents raided the Lucianos home and found, according to the complaint, fentanyl patches in a downstairs safe. 
 Investigators said Philip Luciano, who has a degree in nursing, took orders and used the online currency bitcoin to make transactions and buy drugs that were shipped to their address. Then Michael Luciano would mail packages filled with drugs to customers from the local post office, using a fake return address, the feds said. 
 During the search, investigators found a cellphone and iPad that they believe Philip Luciano used to run the business on which they found text messages referencing their joint drug-dealing operation, photographs of fentanyl patches and oxycodone pills, and websites associated with bitcoins. 
 AlphaBay and another online black-market site called Hansa were shut down last month in a government crackdown. 
 Both, according to the Drug Enforcement Administration and Justice Department, were major sources of the opioids and heroin responsible for an epidemic of fatal overdoses that killed 35,000 people across America in 2015  and maybe more. 
