__HTTP_STATUS_CODE
200
__TITLE
Trump Warns Congress: You Better Get Tax Plan Passed
__AUTHORS
Adam Edelman
__TIMESTAMP
Oct 11 2017, 7:21 pm ET
__ARTICLE
 American truckers will be back on the road to prosperity if Congress passes the White Houses tax plan, President Donald Trump promised Wednesday. 
 And they better," he warned. 
 To all of our great congressmen, congresswomen ... all I can say, is, you better get it passed, Trump told a group of about 1,000 truckers gathered around an airplane hangar in Harrisburg, Pa. 
 The White House made tax reform its latest big policy push following a series of failures on other legislative priorities, such as repealing Obamacare. 
 But Trump expressed confidence in lawmakers' ability to succeed during his speech Wednesday  despite the fact that the plan has been criticized by Democrats and nonpartisan tax analysts alike as disproportionately benefiting the wealthy, and might be a tough sell to some conservative lawmakers, as well. But passing his vision for tax reform, the president said, would open up a world in which American trucks will glide along the highways, smooth and beautiful ... no potholes. 
 When your trucks are moving, America is growing, Trump said, with a big rig sporting a huge Win Again sign parked behind him. 
 Trump ran through the details of his plan during his 35-minute appearance, lauding his audience as heroes who would benefit from the level playing field" that he said would be created by his tax plan. 
 Trump added that his top economic advisers had estimated the proposed changes to the tax code would help give typical American households "a $4,000 pay raise. 
 Youre going to spend that money, youre going to be put to work, jobs are going to be produced, he said. Wouldnt that be nice. 
 Trumps stop in Pennsylvania marks his latest pitch to working-class voters. He previously touted the plan as having "beautiful, massive" tax cuts that benefit small businesses and manufacturers. 
 But critics and tax policy analysts have said the plan helps the wealthy profit even more. For example, an analysis by the nonpartisan Tax Policy Center found that the plan would disproportionately benefit the top 1 percent of earners. 
 Under the plan, individuals would see their tax brackets collapsed from seven rates into just three: 12 percent, 25 percent and 35 percent. The bottom rate would go up from 10 percent and the top would go down from 39.6 percent. 
 The plan would also lower the corporate rate from 35 percent to 20 percent. Corporate tax cuts tend to benefit wealthy investors, although proponents argue average Americans will benefit from increased investment and economic growth. 
