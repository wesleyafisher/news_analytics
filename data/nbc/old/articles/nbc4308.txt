__HTTP_STATUS_CODE
200
__TITLE
Ohio Gov. John Kasich Limits Opioid Prescriptions to Just Seven Days
__AUTHORS
Corky Siemaszko
__TIMESTAMP
Mar 30 2017, 12:18 pm ET
__ARTICLE
 The Ohio governor unveiled a plan Thursday that targets the place where experts say many opioid addictions begin  the doctors office. 
 Gov. John Kasichs order limits the amount of opiates primary care physicians and dentists can prescribe to no more than seven days for adults and five days for minors. 
 In addition to the pill limits, Kasich said the new rules require doctors to provide a specific diagnosis and procedure code for every painkiller prescription they write. 
 And Kasich warned that doctors who dont follow the rules will lose their licenses. 
 Youre going to have to abide by these rules, he said. 
 The new limits, which have gotten the blessing of the Ohio Board of Pharmacy, the State Medical Board, and the states dental and nursing boards, do not apply to patients who take prescription painkillers for cancer treatment or to dying patients who are already receiving hospice care, Kasich said. 
 "Health care providers can prescribe opiates in excess of the new limits only if they provide a specific reason in the patients medical record," the state said in a statement. 
 Kasich also said lawmakers and doctors cant do it alone and that Ohioans are going to have to speak up when they see something if they want to vanquish this plague. 
 We all need to stick our noses into somebody elses business, he said. 
 Kasichs announcement came a day after GOP lawmakers introduced bills in the state House and Senate that take a similar approach by setting dosage limits and making drug counseling and addiction education available online. 
 Previously, doctors in Ohio were allowed to write painkiller prescriptions for up to 90 days, according to the Columbus Dispatch. 
 The Ohio medical establishment has, in recent years, moved to limit the amount of painkillers being prescribed by issuing guidelines. Now, its law. 
 By reducing the availability of unused prescription opiates, fewer Ohioans will be presented with opportunities to misuse these highly addictive medications, Kasich said in a statement. 
 The governors announcement came on the heels of President Donald Trumps unveiling of a special commission  headed by New Jersey Gov. Chris Christie  to combat the opioid epidemic. 
 Christie, who like Kasich is a Republican, signed into law last month a five-day limit on the amount of opioids that can be prescribed by a doctor. 
 Ohio is one of several Rust Belt states that has been struggling to contain a raging opioid epidemic. 
 This year, the states capital city of Columbus has been average one fatal overdose per day from fentanyl  a powerful painkiller that the U.S. Drug Enforcement Agency says is 25 to 50 times more powerful that heroin and packs 50 to 100 times more punch than morphine. 
 And there were so many fatal drug overdoses in four other Ohio counties that the local coroners had to truck in cold storage mass casualty trailers from Columbus to stash the bodies because they ran out of room in their morgues. 
 As bad as things are in Ohio, its ever worse in West Virginia, New Hampshire and Kentucky, which have even higher drug overdose death rates, according to the federal Centers for Disease Control and Prevention. 
 While many Americans get hooked on locally prescribed painkillers, lawmakers say the drug cartels have made a mint feeding their habits by smuggling Chinese-made fentanyl and other opioids into the country. 
