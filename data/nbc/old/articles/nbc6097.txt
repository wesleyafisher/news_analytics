__HTTP_STATUS_CODE
200
__TITLE
Amazon Is Ready for the Next Generation Consumer: Your Teen
__AUTHORS
Alyssa Newcomb
__TIMESTAMP
Oct 11 2017, 1:23 pm ET
__ARTICLE
 If giving your teen free rein to shop on Amazon sounds like your worst nightmare, we've got bad news for you. It's officially here. 
 Amazon introduced a new feature on Wednesday that allows parents to create accounts for their children ages 13 to 17. The teens will have separate logins but will be tied to the master account. 
 Of course, the team at Amazon knew giving a teen carte blanche to spend mom and dad's money probably wouldn't be a popular choice for parents, so they've added a set of parental controls. 
 "What we're looking at is balancing independence for teens and convenience and trust with parents," Michael Carr, vice president of Amazon Household, told NBC News. 
 It's just the latest move in Amazon's play to draw the whole family into the company's ecosystem of products and services. 
 Already, when mom and dad aren't home to help with study time, kids can turn to Alexa for help with homework. Amazon also makes a kid-friendly version of its Fire Tablet, letting even the youngest members of the family experience its products. 
 With this new Amazon service, parents can add up to four teens to their account for free and set the payment methods and shipping addresses. After their teens are done filling their cart, parents will receive a quick text asking for approval. Parents can also choose to skip the approval step and instead set spending limits. If a parent has Amazon Prime, those benefits will also be extended to the teen shopper. 
 Patrick Moorhead, principal analyst at Moor Insights & Strategy, said the teen shopping announcement makes sense for Amazon as the company continues to dominate the online shopping space. 
 "Amazon has most households and therefore most adult Americans as customers. The company is now going after businesses and now kids as customers," he said. "If parents can tell their kids to 'get it on Amazon,' it saves parents time." 
 Ten to 15 years ago, parents of teens were faced with the decision of whether their child was ready to have their own cell phone or AOL Instant Messenger screen name. Now, they'll have to decide if their teen is ready to independently shop on Amazon. 
 "I see the benefit to this if it were used as a modern version of an allowance," Dr. Alok Patel, a clinical instructor of pediatrics at Columbia University, told NBC News. "It is another modern way to reward teens for good behavior and to teach them financial responsibility." 
 Anita Weiss, a Phoenix mom of two girls aged 16 and 17, said her teens already know her Amazon password. She's comfortable with them shopping on Amazon, as long as a parent is kept in the loop. 
 "I trust my kids completely. I'm not worried about them doing something bad, but they are kids and they might make judgement calls that are not the best," she said. For instance, "They may buy something I could find cheaper somewhere else." 
 Related: Amazon Just Announced It Is Slashing Prices at Whole Foods 
 Sara Lenertz of Minneapolis has a 16-year-old daughter and a son who will turn 13 in January. 
 "My daughter long ago memorized my credit card number, including the three-digit code on the back," she told NBC News. "I feel fine about her shopping on Amazon, but I am sure it would come down to the level of trust parents have with their children." 
 Lenertz said her son went a little wild with in-app purchases a few years ago, a problem plenty of parents (including Kanye West) have complained about. However, she thinks that even at the younger end of the age range, he'd still be ready to have his own account. 
"I don't know how much they would go off on their own and shop at will," she said. "I think [many teens] already know how Amazon works. This might make it a little more hands off, but kids are so intuitive with anything technological."
