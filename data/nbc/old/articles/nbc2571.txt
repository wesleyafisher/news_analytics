__HTTP_STATUS_CODE
200
__TITLE
Relief Team in Puerto Rico Brings Aid and Comfort to Elderly 
__AUTHORS
NBC News
__TIMESTAMP
Oct 6 2017, 12:30 pm ET
__ARTICLE
Sandra Alvarez, a doctor from Daytona Beach, Florida, examines Mercedes Perez, a resident at the Petroamerica Pagan de Colon assisted living facility in San Juan, Puerto Rico, on Oct. 1, 2017.
Alvarez is a member of the San Juan Medical Relief Team which is assisting elderly residents at the facility after Hurricane Maria ravaged the island. The team is mostly volunteers born in Puerto Rico that now live on the U.S. mainland.
Puerto Rico's elderly are made particularly vulnerable byshortages in medication and water anda lack of power that makes it difficult to combat the sweltering heat.
Alvarez, right, and Brenda Francisco, a registered nurse from Delaware, knock on residents' doors.
Allison Betof, a doctor from New York, examines resident Rosario Lugo-Lopez.
Allison Betof and Maria Smith, a registered nurse from Florida, examine Silvio Perez Chacon.
Chacon developed complications from diabetes and serious infected necrotic skin breakdown on his legs. They persuaded him to be taken to the hospital, warning him that he could lose his leg.
Chacon leaves the facility to be taken to a hospital.
Family photos are displayed inside Rosario Lugo-Lopez's residence at the facility.
Mayda Melendez, a family medical practitioner from Delaware, embraces resident Maria Diaz.
Resident Mercedes Perez is examined.
Alvarez and Francisco embrace on an elevator as they begin their day at the facility.
Elmer Vasquez Colon, 80, his wife Gloria Vasquez Diaz, 67, who have been married over 50 years, lie in a bed at the Petroamerica Pagan de Colon assisted living facility.
"I pray for Puerto Rico, Mexico and all the world," said Colon. Both are sick, and their daughter, Lisbeth Vasquez, has been helping but couldn't get the prescriptions they need when she went to the hospital.
Lisbeth Vasquez, who is also sick and hascancer said, " Please don't abandon us."
PHOTOS: Dark Days and Long Nights Descend on Puerto Rico
