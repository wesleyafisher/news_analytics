__HTTP_STATUS_CODE
200
__TITLE
Education or Humiliation? What Happens When a Drug Overdose Photo Goes Viral
__AUTHORS
Jon Schuppe
__TIMESTAMP
Nov 2 2016, 1:47 pm ET
__ARTICLE
 On a Saturday afternoon in late October, Jami Smith got a call from the police in her hometown of Hope, Indiana, asking if she knew someone named Erika Hurt. 
 "That's my daughter," Smith replied. 
 The officer told Smith to meet them at a nearby Dollar General Store, where she found Hurt, 25, in the parking lot, unconscious at the wheel of a car, needle in her hand, her 10-month-old son in back. 
 Smith didn't know it at the time, but one of the officers at the scene had snapped a photo that would turn a private struggle with heroin addiction into a public symbol of America's opioid crisis. 
 Medics revived Hurt with a shot of Narcan, and took her to the hospital  while police charged her with child neglect and possession of drug paraphernalia. Smith took her grandson home, and tried make things seem as normal as possible for him. 
 Related: More Children Sick, Dying From Opioid Overdoses: Report 
 Back at police headquarters, Town Marshal Matt Tallent saw the photo, and struggled over what to do with it. The nation's heroin epidemic was creeping into tiny Hope, population 2,800, and people needed to know about it, Tallent thought. But the police department typically didn't share much beyond mug shots and wanted posters, and there was no policy guiding him. 
 That Monday, Oct. 24, Tallent made his decision. He sent the snapshot to the Indianapolis Star, and afterwards shared it with other local media. He said he didn't know until a reporter told him that police in other small towns around the country  Ohio, Massachusetts, North Carolina  had been doing the same thing, as a way to draw attention to the drug problem. 
 "I hoped it would start spreading the message," Tallent recalled. "But, believe me, I never thought that picture would go as viral as it did." 
 Not long after, a friend called Smith and told her to turn on the TV news. On the screen was a photo of her daughter, in black pants and a sweatshirt, head slumped back, mouth slack, syringe pinched between the fingers of her left hand. 
 It was humiliating. 
 "They said it was a teaching tool. I say they wanted to bring big news to a small town," Smith said. "Unfortunately, my daughter gave them that opportunity." 
 Hurt had first gotten hooked on pain pills when she was 21, and eventually graduated to heroin, sinking deep after the death of her father, Smith said. The drugs got her in trouble with the law, and while in jail two years ago she got clean, Smith said. 
 Related: Ohio City Releases Shocking Photos to Show Effects of 'Poison Known as Heroin' 
 As far as Smith knows, Hurt was not using drugs when she got pregnant, and did not do them after her son was born. Smith said she believes that the overdose in the Dollar General Store parking lot was her daughter's first relapse, although Smith acknowledged that she couldn't know for sure. 
 Smith visited her daughter at the Bartholomew County Jail, where Hurt had told her how ashamed and remorseful she was. The picture made it much worse. 
 By then, the photo was spreading across the Internet. Tallent said he saw it in publications as far away as the Czech Republic and Vietnam. 
 Smith began receiving comments on Facebook, "horrible hate messages." The people called her names, and said Erika should have died. Smith deleted her daughter's Facebook page and reset the privacy settings on her own. She noticed unfamiliar cars driving past her house at all hours of the day and night. She wondered how the people of Hope  "just a little bitty small, small community town"  would respond. 
 "For the first week, we wouldn't hardly leave the house," Smith said. "We lived in fear." 
 Tallent said the feedback on his move fell into two categories. Some praised it as an act of public service. Others accused him of embarrassing Hurt and ruining her chances of finding employment. 
 "But the thing that bothered me the most, and I guess it's my fault for reading social media, is reading comments that we should have just let her die," Tallent said. "I did not in any way shape or form want this kind of negative publicity for this girl. I have nothing but compassion for her, her mother and the baby. This girl had made the wrong decisions in life, and she needs help. She doesn't need hate." 
 Asked if he'd do it again, Tallent paused and said it would depend on the circumstances. 
 As it turned out, the people on the Internet were far meaner than those in Smith's hometown. Lots of them, family and friends and people she didn't know, expressed their support, Smith said. 
 She took her grandson trick-or-treating Monday night, and didn't hear a judgmental word. 
 Her daughter, however, is still incarcerated. 
 "She hates herself. Her heart is broken. She's really down on herself for letting this happen to her," Smith said. 
 Hurt also tells Smith she's going to seek treatment, and hopes to be reunited with her son. 
