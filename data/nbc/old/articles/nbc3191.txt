__HTTP_STATUS_CODE
200
__TITLE
Rape Allegation Makes Muslim Teens Murder a Capital Case
__AUTHORS
Associated Press
__TIMESTAMP
Oct 17 2017, 8:12 am ET
__ARTICLE
 FAIRFAX, Va.  An already disturbing case about a 17-year-old Muslim girl killed as she walked to her mosque took another unsettling turn after an indictment alleged the victim was raped as well. 
 The indictment Monday includes capital murder and rape charges and will allow prosecutors to pursue a death-penalty case against Darwin Martinez-Torres, 22, of Sterling. 
 He is accused in the slaying of Nabra Hassanen of Reston, whose death back in June rattled northern Virginia's Muslim community. 
 Prior to Monday's indictment, much of the discussion surrounding the case revolved around whether prosecutors should bring hate-crime charges  as sought by some activists  or whether it was a case of road rage, as police have long maintained. 
 Monday's indictment, though, brought into focus another aspect of the case: While police had said previously they were investigating whether Hassanen had been sexually assaulted, the indictment is the first court document to spell out any kind of sex-crime charges against Martinez-Torres. 
 Virginia law allows prosecutors to pursue a death penalty case only under certain conditions. Those include murder in the commission of a rape and murder during an abduction with intent to defile. 
 The seven-count indictment handed up Monday actually includes four counts of capital murder, with occasionally graphic detail on the ways in which prosecutors believe the slaying qualifies for the death penalty. 
 The teenager's death continues to resonate. More than 200 supporters wearing "Justice for Nabra" T-shirts showed up at a preliminary hearing Friday for Martinez-Torres. The hearing was delayed when the girl's parents had to be restrained from charging at Martinez-Torres. Her mother threw a shoe at him. 
 Police have said Hassanen was out with a group of more than a dozen friends at about 3:40 a.m. June 18. The group was walking back to their mosque, the All Dulles Area Muslim Society, after eating at a McDonald's ahead of a daylong fast. Observant Muslims fast from sunrise to sunset during Ramadan, which this year coincided with the summer solstice. As a result, teens from the mosque sometimes went out for fast-food meals in the overnight hours between the late-night and early morning prayer services. 
 Police say Martinez-Torres encountered the group and got into a confrontation with some of the kids who had been in the roadway. Martinez-Torres chased after the group and caught Hassanen, at one point bludgeoning her with a baseball bat, police said. 
 In a search warrant affidavit, police say Martinez-Torres admitted killing Hassanen and that he led them to where he had dumped her body in a nearby pond. 
 The Council on American-Islamic Relations, a Muslim civil rights group, is representing the girl's family. The group's lawyer Gadeir Abbas said the family "is focused on ensuring that there is justice for Nabra and that the murderer is held accountable for his crimes. This tragedy has affected the family, but also the Muslim community across the country, coming as it did during Ramadan when the kids were gathering at the mosque to socialize and for prayer." 
 Some Muslim activists called for hate-crime charges. Police and prosecutors said they have seen no evidence of anti-Muslim bias that would warrant such a charge. 
 The public defender appointed to represent Martinez-Torres, Dawn Butorac, did not return a call Monday seeking comment. 
