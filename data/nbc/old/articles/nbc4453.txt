__HTTP_STATUS_CODE
200
__TITLE
Speaker Ryan Opens Door for Action on Gun Rapid-Fire Bump Stocks
__AUTHORS
Alex Seitz-Wald
__TIMESTAMP
Oct 5 2017, 12:16 pm ET
__ARTICLE
 WASHINGTON  House Speaker Paul Ryan said Thursday that he wants Congress to look into so-called bump stocks, joining a growing number of Republican leaders who have expressed interest in taking action on the modifications that increase a weapon's rate of fire. 
 "Fully automatic weapons have been banned for a long time," Ryan told MSNBC host Hugh Hewitt. "Apparently, this allows you to take a semiautomatic and turn it into a fully automatic. So clearly thats something we need to look into." 
 Ryan said that even though he's an avid hunter, he had never heard of the devices before Sunday's mass shooting in Las Vegas. "I think were quickly coming up to speed with what this is," Ryan said. 
 Republicans typically draw a hard line against any new rules to regulate guns, which they view as violations the Second Amendment. But the circumstances of the Las Vegas massacre, in which a dozen bump stocks were found in the shooters hotel room, and the longstanding consensus against automatic weapons, may create an opening for action. 
 The second and third-ranking Republicans in the Senate also have said they want to look into bump stocks, as have several other prominent GOP members in the House. Many Democrats have already announced their support for legislation to make bump stocks illegal. 
 "I have no problem in banning those," Sen. Ron Johnson, R-Wisc., told reporters. 
 Rep. Peter King, R-N.Y., who has led bipartisan efforts to expand background checks, told NBC News he was inclined to support a ban, too. "Right now my first instinct is to say yes, but I want to look at it more carefully," he said. 
 Still, as of Thursday morning, no Republicans had signed onto a bill to ban bump stocks introduced by Sen. Dianne Feinstein, D-Calif., according to her office. 
 And both Senate Majority Leader Mitch McConnell, R-Ky., and Senate Judiciary Committee Chairman Chuck Grassley, R-Iowa., who has jurisdiction over gun legislation, suggested it's too early to consider any new rules. President Donald Trump has said the same. 
 "The investigation into the Las Vegas shooting is still ongoing, and we need to get more information before making a decision on a hearing and what it might cover," Grassley said in a statement. 
 Bump stocks and related devices accelerate the rate of fire of semi-automatic weapons to mimic that of automatic ones, which are all-but banned in the U.S. The Bureau of Alcohol, Tobacco, Firearms and Explosives has ruled the devices legal under current law, because they do not alter the internal mechanism of gun. 
 Companies that sell the devices have reported brisk business as gun owners rush to purchase remaining supplies in case their sale is soon prohibited. 
 Watch more from Hugh Hewitt's interview with Paul Ryan on Saturday, Oct., 7 at 8 a.m. ET on MSNBC. 
