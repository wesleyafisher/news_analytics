__HTTP_STATUS_CODE
200
__TITLE
Man Charged In Heroin Death of Pregnant Mom and Unborn Child
__AUTHORS
Tom Winter
__TIMESTAMP
Mar 16 2016, 11:43 am ET
__ARTICLE
 The DEA has arrested a man they say gave heroin to a pregnant woman, killing both the woman and her unborn child. 
 Anthony Vita of Virginia Beach, Virginia, faces charges of dealing drugs in the Syracuse, New York, area, and causing the overdose death of 24-year-old Morgan Axe of Camillus, New York, who was five months pregnant when her family found her dead next to a syringe and a bag of heroin last November. 
 The arrest comes as local and federal law enforcement agencies are rushing to address the rise of heroin overdoses and addiction throughout the Northeast. 
 One of the more troubling trends authorities say they are trying to address is the rise in heroin-addicted pregnant women. A Reuters /NBC News Investigation found that in 2013, there were 27,000 drug-dependent newborns in the U.S., while a recent study showed that as many as one in five mothers enrolled in Medicaid used opiates during pregnancy between 2000 and 2007. 
 The DEA was able to get Axes iPad with the permission of her family and review messages between Axe and Vita. They say he told her, Call me I got the best of it all and They are called the king of torts. 
 According to the affidavit the bag of heroin found next to Axe was labeled king of torts. 
 She told Vita, Are you sure they are good I havent used in 5 months having a bad day need something good. 
 In her obituary, Axes family wrote, it was with that fiery spirit, she endeavored to conquer her demons in this life. 
 They say, Her dreams were to someday open a dog rescue, while also helping others with their battles against addiction. 
 The U.S. Attorneys Office for Northern New York says Vita waived his hearings in Virginia and will be brought back to upstate New York to face the charges. He could be sentenced to 20 years to life in a federal prison if convicted. 
