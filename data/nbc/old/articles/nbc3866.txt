__HTTP_STATUS_CODE
200
__TITLE
Meet the Mom Helping Parents Pass on Diwali Traditions
__AUTHORS
Lakshmi Gandhi
__TIMESTAMP
Oct 16 2017, 8:43 am ET
__ARTICLE
 Like many children of immigrants, Rupa Parekh felt that her knowledge of her culture and religious traditions was somehow incomplete. 
 Even before I was married, I always just thought, How do I bottle my mom up, Parekh told NBC News. For those of us who are second generation, its like overnight you need to be a teacher. 
 That need to teach was what helped Parekh come up with the idea for Jai Jai Hooray, a toy company that makes products featuring Hindu traditions, just a few months after her second child, Niko, was born in March 2016. As her daughter Uma (now 4) got older, Parekh and her Turkish-American husband had been thinking more about the meaning of cultural traditions and how to best pass them on. 
 Culture is so many things. Its not just the mythology. My parents were from Bombay, they were city kids. So we went to garba, but it was a medium amount of exposure to culture growing up, Parekh, 37, said, referring to traditional dance classes. 
 Researching diverse toy options for her children proved disappointing. 
 I started looking online but it seemed like the things that did exist were poorly designed and not very enticing, Parekh said. 
 It was a homemade gift that Parekhs mother made for Uma  hand-cut flash cards of Hindu deities from a calendar  that got her thinking about how to create accessible, culturally literate toys. 
 Parekh's daughter instantly loved the DIY cards and a friend suggested that she should have them made professionally for other Hindu-American parents. 
 I was unfulfilled at work and I had been thinking about it for a while, Parekh, who had spent her career in tech, said. She reached out to Mumbai-based artist Ajinkya Bane, and the pair began discussing creating their own versions of the deity flashcards her mother had created. 
 I have a unique perspective being married to a non-Hindu person," Parekh said, adding that her mother-in-law Caryl and her daughter liked to go over the homemade flashcards together. 
 RELATED: Childrens Book Lets Celebrate Diwali Explores Holidays Different Traditions 
 She stressed that her products aren't meant to teach the full breadth of Hinduism. 
 We wanted the cards to have a beautiful simplicity that showed that anyone could be a teacher. Its just the basics, its not Amar Chitra Katha, she said, referring to the popular Indian comic books that illustrate Hindu myths. The mission is cultural learning. 
Getting ready to introduce ourselves to the world! pic.twitter.com/JOsS291DAs
 Jai Jai Hooray released its second product last month ahead of the Hindu festival of Navaratri. Called the "Goddess Power Tower," the toy is made up of a series of stacking blocks, each featuring the image of one of the major Hindu goddesses. 
 We were thinking, how do you talk to a little kid about goddesses? So we have Mahadevi, who is like a mother goddess and inside of her is Saraswati, Durga, and Lakshmi, Parekh explained. Its a really cool way to express that. 
 As she gets ready for Diwali with her children, Parekh said she has been heartened by conversations with other parents about how they are planning to commemorate the holiday. Some are celebrating almost like Hanukkah, where they are introducing a block with a different goddess every day, she said. 
 Parekh also recently visited her daughter's school to talk about the various goddesses and the Goddess Power Tower with her class. It was in some ways a reminder of how many more options there are in terms of toys for young children today. 
 I didnt have a toy that felt like me growing up, Parekh said. 
 CORRECTION (Oct. 16, 2017, 12:40 p.m.): An earlier version of this article had an inaccurate quotation from Rupa Parekh about her mother-in-law Caryl. Her mother-in-law is neither blonde nor Turkish-American (her father-in-law is of Turkish descent). Parekh notified NBC News about the inaccuracy after the article was published. 
 Follow NBC Asian America on Facebook, Twitter, Instagram and Tumblr. 
