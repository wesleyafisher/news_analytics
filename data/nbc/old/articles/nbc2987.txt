__HTTP_STATUS_CODE
200
__TITLE
Trump Contraceptive Move Could Lead to More Abortions
__AUTHORS
Maggie Fox
__TIMESTAMP
Oct 8 2017, 1:13 pm ET
__ARTICLE
 The Trump administration's moves this week to cut back on free birth control and tighten abortion restrictions could add up to an unintended consequence, according to experts: Women may end up having more abortions. 
 On Friday, the administration rolled back a key Obamacare requirement for most employers to provide free birth control as part of health insurance coverage. The new rules it issued would allow just about any employer to opt out of the birth control requirement, provided they show a sincerely held religious or moral objection. 
 The new rules will also more strictly enforce a requirement that health insurance companies tell patients if their policies provide abortion services and, if they do, to pay for them out of a separate fund. 
 Officials said the goal is to protect religious freedom. But research done at the Washington University School of Medicine in St. Louis suggests the two moves conflict with the goal of reducing the number of abortions. 
 Related: Abortions Hit New Low in US 
 A 2012 study of more than 9,000 women found that when women got no-cost birth control, the number of unplanned pregnancies and abortions fell by between 62 and 78 percent. 
 "This study shows that by removing barriers to highly effective contraceptive methods such as IUDs and implants, we can reduce unintended pregnancies and the need for abortions," Washington University professor Dr. Jeff Peipert said in a statement. 
 "The results of this study demonstrate that we can reduce the rate of unintended pregnancy, and this is key to reducing abortions in this country." 
 Related: Trump Administration Just Made it Easier for Bosses to Say No to Contraception 
 Other studies have shown similar results. Close to half of all U.S. pregnancies are unplanned, and nearly all elective abortions involve unplanned pregnancies. 
 The most effective forms of birth control  intrauterine devices (IUDs) and injected or implanted contraceptives  are the hardest for women to get on their own, said Dr. David Eisenberg, associate professor of obstetrics and gynecology at Washington University. 
 "The cost upfront can be prohibitive," he said. Over time, they're cheaper than daily methods such as pills or condoms, he said, but the cost of the most effective methods is front-loaded. 
 Related: Unintended Pregnancies Fell 18 Percent in Recent Years 
 "Without the contraceptive mandate, some highly effective methods of birth control, such as intra-uterine devices and sterilization, could cost women more than $1,000, putting these options out of reach for many women," Human Rights Watch said in a statement. "Affordable family-planning options and contraceptive choice help to prevent unintended pregnancies." 
 The Guttmacher Institute, which studies reproductive health, says Planned Parenthood alone averted 140,000 abortions in areas it serves by providing birth control and family planning services. 
 The Trump administration and the Republican leadership in Congress aim to take federal funds away from Planned Parenthood because it also provides abortions. Its counterproductive policy, Eisenberg said. 
 "As a public health expert, I can say without question the most cost-effective interventions you can make are safe water, a vaccination program, and contraceptive and family planning care  in that order," he said. 
 Related: Global Gag Rule Will Cause More Abortions, Not Fewer 
 "If the outcome of interest is having the healthiest possible woman, the healthiest possible pregnancy, the healthiest possible family, the healthiest possible community, it would be to be sure that all women and men have access to the reproductive and sexual healthcare they need when they need it at no cost out of pocket." 
 The new regulations take effect immediately, and the Health and Human Services Department made no mention of health in announcing the new rules. 
 "No American should be forced to violate his or her own conscience in order to abide by the laws and regulations governing our health care system," HHS spokeswoman Caitlin Oakley said Friday. 
 "All across the executive branch, they are using their power to remove the protection for women to access the care they need when they need it," Eisenberg said. 
 "What about the religious freedom of women who want not to have a child until they are ready or the religious freedom of men who want to practice responsible behavior?" he asked. 
 "I am 100 percent certain that there is no medical, public health, scientific or other reason to prohibit women having access to the contraceptive care they need. It is a politically motivated kind of move." 
