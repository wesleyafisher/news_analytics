__HTTP_STATUS_CODE
200
__TITLE
Augmented Reality Apps Will Change the Way You Use Your Phone
__AUTHORS
Alyssa Newcomb
__TIMESTAMP
Aug 29 2017, 5:33 pm ET
__ARTICLE
 The line between the real world and the digital world is fading to grey, as Apple and Google gear up to bring augmented reality to your smartphone. 
 Augmented reality - AR for short - takes the digital world and brings it together with your physical environment all in one place. AR has been around for a while, but its latest incarnation could be a game changer. 
 A slew of new apps can help you see that meal before you order it, picture furniture in your home, play high-tech games, and even bring your favorite GIFS into the real world. 
 Related: Retailers Use Augmented Reality, High Tech To Lure Shoppers 
 IKEA has a new augmented reality app that will let potential customers view digital furniture in their real-life space to design their dream living room. If you like what you see, IKEA will let you order straight from the app. 
 Another app from GIPHY lets users drop their favorite GIFS, whether they're cute kittens or a cry baby, into their real-life environment, creating a hilarious hodge podge. 
 If you're having trouble deciding what to order, there's also an app for that. A demo video we first spotted at MacRumors shows how restaurants can make digital burgers and desserts appear on your plate to help you make a decision. 
 "We have seen AR in a simple way with Snapchat and Facebook filters where our faces get covered to look like something else, like the Night's King, and we take a video or picture of it," Patrick Moorhead, principal analyst at Moor Insights & Strategy, told NBC News. "But AR can be a lot more advanced and valuable." 
 He said he imagines use cases where doctors can hold up a phone or tablet to a patient to check their skin to detect any issues. AR can also be a valuable coaching tool, showing people step by step how to complete a task. 
 Apple announced ARKit, a platform for developers to build augmented reality apps, at its annual Worldwide Developer Conference in June. It will be released to the masses with an iOS 11 update, which is expected to be rolled out next month. 
 On Tuesday, Google released a similar platform, ARCore, to developers looking to create experiences for Android. 
 ARCore is available now and will run on the Pixel and Samsung S8, as long as the devices are running the Nougat operating system or newer. Google said it hopes to have ARCore on 100 million devices by the end of its preview. 
 The Google experience focuses on three areas, according to a blog post from the company. The first, motion tracking, uses the position of your phone to ensure that virtual objects are placed appropriately as you move. 
 Environmental understanding makes sure those objects are in the right spot, so you would have a couch in the corner, not on the ceiling. 
 Finally, it focuses on lighting to ensure that hamburger looks like the real deal and blends in the physical world so well, that you might - for just a split second - forget and want to take a bite. 
