__HTTP_STATUS_CODE
200
__TITLE
Opioid Crisis: The Awful Arithmetic of Americas Overdoses May Have Gotten Worse
__AUTHORS
Corky Siemaszko
__TIMESTAMP
Aug 7 2017, 5:34 pm ET
__ARTICLE
 The deadly drug overdose epidemic that has been ravaging the nation may be even worse than we realize. 
 A new University of Virginia study says the numbers of deaths due to heroin and opioid overdoses have actually been severely underreported. 
 Dr. Christopher Ruhm revisited thousands of death certificates from 2008 through 2014 and concluded the mortality rates were 24 percent higher for opioids and 22 percent higher for heroin than had been previously reported. 
 Opioid mortality rate changes were considerably understated in Pennsylvania, Indiana, New Jersey and Arizona, the study states. Increases in heroin death rates were understated in most states, and by large amounts in Pennsylvania, Indiana, New Jersey, Louisiana and Alabama. 
 Ruhms awful arithmetic emerged just days after the presidential opioid commission, led by New Jersey Gov. Chris Christie, urged President Donald Trump to declare a national emergency to deal with the crisis. 
 Nearly 35,000 people across America have died of heroin or opioid overdoses in 2015, according to the National Institute on Drug Abuse. 
 "My message to members of a Presidential commission would be that getting the most accurate statistics possible is a crucial first step towards developing policies aimed at stemming the fatal drug epidemic," Ruhm told NBC News. "This is particularly important when we have scarce funds to allocate and so would want to target them at the hardest hit areas." 
 Ruhm contends that one of the reasons U.S. officials have been unable to win this war is the lack of reliable information on the drugs causing fatal overdoses. 
 This occurs when no specific drug is identified on the death certificates, he said in the study. 
 So Ruhm, a professor of public policy and economics, began poring over death certificate data from the federal Centers of Disease Control and Prevention. 
 He discovered that in 2014, a specific drug was not identified in 19.5 percent of fatal overdoses. And in 2008, that figure was even higher  25.4 percent. 
 States like Rhode Island, Connecticut and New Hampshire specified the exact drug on death certificates 99 percent of the time but only around half the time in Pennsylvania, Indiana, Mississippi, Louisiana and Alabama, Ruhm reported in his study. 
 Armed with that info, Ruhm estimated how many of those deaths could be blamed on heroin and how many on opioids. 
 His results? The national rate of fatal opioid overdoses jumped in 2014 from 9 per 100,000 people to 11.2  and rate of fatal heroin overdoses climbed form 3.3 per 100,00 to 4. 
 Based on Ruhms research, the drug problem in Pennsylvania is a lot worse than the CDC figures indicate. 
 The Keystone state was ranked 32nd by the CDC for opioid deaths for 2014, with 8.5 per 100,000. But Ruhm concluded the Keystone State actually has the seventh highest rate of opioid deaths that year with 17.8 per 100,000. And, based on Ruhms calculations, it went from being the state with the 20th highest fatal heroin overdose rate to fourth. 
 "These numbers are alarming and underscore the need to continue expanding treatment, education, awareness, and resources for law enforcement and health professionals," J.J. Abbott, press secretary for Pennsylvania Gov. Tom Wolf, wrote in an email to NBC News. "As part of the ongoing strategy to address the opioid crisis, the administration is working on creating a public facing data dashboard to centralize and streamline information regarding overdoses, prevention, and treatment from all of the different sources." 
 That said, Susan Shanaman of the Pennsylvania State Coroners Association said "there are many reasons that a Coroner may not list all the drugs on a death certificate," including federal and state privacy rules. 
 "The average family gets 20 copies of a death certificate of their loved one," she wrote in an email to NBC News. "These are used to close out bank accounts, transfer loan accounts, transfer titles to vehicles, claim insurance and the like. Not every family member wants the public to know what drugs were all found in the deceased." 
 Hard-hit Ohios opioid death rate went from 18.2 per 100,000 (fifth highest in the country) to 20.5 per 100,000 (fourth highest in the country) in Ruhms report. 
 Ohio also has the nations highest rate of fatal heroin overdoses with 10.4 per 100,000. By Ruhms reckoning, the rate is 11.2 per 100,000, which is also the highest in the country. 
 Unchanged in Ruhms report is West Virginias woeful ranking as the state with the highest rate of deadly opioid cases. But even there Ruhm concluded the death rate for opioids should have been 30.3 per 100,000 in 2014 instead of 29.9. And the states fatal heroin overdose rate should have been 9 per 100,000 rather than 8.8. 
 Ruhms research also buttresses earlier studies that identified the primary victims of this plague. 
 Fatal overdose rates are higher for males than females, for whites than blacks or other non-whites, Ruhm said. 
 And the overdose death rates are highest for people ages 25 to 64 in the Rust Belt, in Appalachia and some western states. 
 There are also pockets with high death rates in many other parts of the country as well, Ruhm said 
