__HTTP_STATUS_CODE
200
__TITLE
A Generation at Risk: Children at Center of Americas Opioid Crisis
__AUTHORS
Daniel A. Medina
Kate Snow
ML Flynn
Eric Salzman
__TIMESTAMP
Oct 11 2017, 11:10 am ET
__ARTICLE
 DAYTON, Ohio  At 14, Tori Brinkman is a survivor. 
 Born premature, 15 weeks early and weighing just a pound and half, she spent the first three months of life in a neonatal intensive care unit. And her struggle had just begun. 
 Tori recalls being 8 when her mother asked her to carry out a drug deal. When she was 11, her uncle, whom she describes as a father figure, died of a heroin overdose. 
 Left to fend for herself, Tori says she did not even see a dentist or primary care physician  despite her fragile birth  until she was 10. 
 Her mother, a recovering heroin addict, has been clean now for three years, but Tori lives with her grandparents. 
 "I consider myself as a parent, and my mom the kid," Tori, describing her early childhood, said in an interview in her grandparent's home near Dayton. "She'd never take care of me, never feed me. I had to feed myself." 
 As the United States grapples with an epidemic of historic proportions  one President Donald Trump has called a national crisis  it is children like Tori who often get lost or overlooked in the discussion. NBC News recently traveled to three states  Ohio, Massachusetts and Utah  to meet a few of these children, some born dependent on opioids and others, like Tori, who have suffered from the epidemic's devastating toll. 
 Justine Dale, the principal at East Falmouth Elementary School on Cape Cod, says she first realized the opioid crisis had arrived on her doorstep when parents started dying. And they keep dying. 
 In a historic town once known for its whaling industry and now as a popular tourist destination, the death toll from the opioid epidemic is staggering. 
 At East Falmouth Elementary over the last three years, 12 parents of students have died  six from the fourth grade alone. 
 Students come to school "traumatized by what they experienced over the weekend or that evening," Dale said. "And lash out at people or withdraw." 
 Josiah is one of these children. 
 His mom, Crystal, turned to heroin when he was a toddler. "I was not a nice mom when I was using actively with him," Crystal said in an interview. 
 Crystal, now 20 months sober, said the tipping point was when she overdosed in front of her two children. That would be the last day she ever used. 
 "I have lovely children," she said. "I don't want to live any more days with regret." 
 Josiah is now thriving at home and in school. Special programs at school have helped him learn how to control his emotions. This fall, with so many children struggling, the Falmouth School District enlisted even more help  weekly teleconference calls with staff members at McLean Hospital, a psychiatric facility associated with Harvard Medical School, to help teachers evaluate individual students. The school now starts the day with music, offers a special room for kids to blow off steam and has hired more counselors. 
 Nationally, according to the Centers for Disease Control and Prevention (CDC), a baby is born suffering from opioid withdrawal every 25 minutes. 
 Dayton Children's Hospital has a program for such babies  a result of the mother using drugs like heroin or other opioids, like painkillers or fentanyl, while pregnant. The hospital's neonatal intensive care unit treats 20 to 30 babies a year with an average stay of 17 days, down from 58 in 2012. In a hospital where the norm was once broken bones and the flu, the impact of the opioid epidemic is felt in every corner. 
 Ashley Hudson's 12-day-old daughter A'Layjah was undergoing treatment at Dayton's neonatal ICU in September. Her newborn son passed away last year, and she blames her drug use. "I can't live through that again," Hudson said. 
 Hudson said she stopped using heroin during this pregnancy but was treated with a maintenance drug that left A'Layjah born dependent. The unit was helping to treat the baby girl with both medicine and nonpharmacological measures  including low lighting and skin-to-skin bonding  and doctors said her prognosis was good. 
 For those infants born dependent on opioids, there's a follow-up clinic, or developmental pediatrics program, where Jude Seidler, a precocious 2-year-old, was making his presence known one day last month. 
 Jude's mother had used heroin every day of her pregnancy, and at nine days old, he went home with adoptive parents, Jay and Ashley Seidler. Dayton Children's Hospital, they say, has been their lifeline. 
 "We've been able to chart his progress," Jay Seidler said. 
 At the clinic, five specialists meticulously track Jude's development to ensure that his speech, movement and coordination follow the normal track of a healthy child in his age group. 
 Whether these efforts will make a difference later in life for A'Layjah and Jude remains a question far beyond their doctors' expertise. 
 "Are they going to be able to work? Are they going to have families? Are they going to have problems with addiction themselves?" Dr. Eileen F. Kasten, the clinic's medical director, said. "I don't know those answers." 
