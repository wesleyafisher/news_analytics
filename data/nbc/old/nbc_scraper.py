# Import the packages installed from pip
import requests
from bs4 import BeautifulSoup

# Instantiate conditional variables
urlCount = 0
count = 0
titleExists = True
authorExists = True
timestampExists = True
articleExists = True
# Get the URL from each line in the file
with open("master-urls-final.txt", 'r') as masterList:
    for url in masterList:
        urlCount += 1
        # Strip the newline character off the url or it will 404
        stripNewLine = url.rstrip('\n')
        # Get the HTML page from the URL
        page = requests.get(stripNewLine)
        # Use BeautifulSoup to get the page content in an object
        soup = BeautifulSoup(page.content, 'html.parser')
        if (page.status_code == 200):
            # Get the HTML status code
            f_http = '__HTTP_STATUS_CODE' + '\n' + str(page.status_code) + '\n'

            # Get title
            f_title = '__TITLE' + '\n'
            tempTitle = soup.find('div', class_='article-hed')
            if tempTitle is not None:
                title = tempTitle.find('h1')
                if title is not None:
                    f_title += title.getText().encode('ascii', 'ignore') + '\n'
                    titleExists = True
                else:
                    titleExists = False
            else:
                titleExists = False

            # Get authors
            f_author = '__AUTHORS' + '\n'
            tempAuthors = soup.find_all('span', class_='byline_author')
            for author in tempAuthors:
                if author is not None:
                    f_author += author.getText().encode('ascii', 'ignore') + '\n'
                    authorExists = True

                else:
                    authorExists = False

            # Get timestamp
            f_timestamp = '__TIMESTAMP' + '\n'
            time = soup.find('time', class_='timestamp_article')
            if time is not None:
                f_timestamp += time.getText().encode('ascii', 'ignore') + '\n'
                timestampExists = True
            else:
                timestampExists = False

            # Get article
            f_article = '__ARTICLE' + '\n'
            tempArticle = soup.find('div', class_='article-body')
            if tempArticle is not None:
                articleText = tempArticle.find_all('p')
                if articleText is not None:
                    for p in articleText:
                        if p is not None:
                            f_article += p.getText().encode('ascii', 'ignore') + '\n'
                            articleExists = True
                        else:
                            articleExists = False
                else:
                    articleExists = False
            else:
                articleExists = False

            # Write the file
            if (titleExists and authorExists and timestampExists and articleExists):
                count += 1
                name = "articles/nbc" + str(count) + ".txt"
                f = open(name, 'w')
                f.write(f_http)
                f.write(f_title)
                f.write(f_author)
                f.write(f_timestamp)
                f.write(f_article)
                f.close()
            print "Processing URL " + str(urlCount)
