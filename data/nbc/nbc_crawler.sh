#!/bin/bash

httrack -p0 -r3 www.nbcnews.com/news/us-news
cp hts-cache/new.txt urls2/us-news_urls.txt
httrack -p0 -r3 www.nbcnews.com/politics
cp hts-cache/new.txt urls2/politics_urls.txt
httrack -p0 -r3 www.nbcnews.com/news/world
cp hts-cache/new.txt urls2/world_urls.txt
httrack -p0 -r3 www.nbcnews.com/business
cp hts-cache/new.txt urls2/business_urls.txt
httrack -p0 -r3 www.nbcnews.com/health
cp hts-cache/new.txt urls2/health_urls.txt
httrack -p0 -r3 www.nbcnews.com/investigations
cp hts-cache/new.txt urls2/investigations_urls.txt
httrack -p0 -r3 www.nbcnews.com/pop-culture
cp hts-cache/new.txt urls2/pop-culture_urls.txt
httrack -p0 -r3 www.nbcnews.com/science
cp hts-cache/new.txt urls2/science_urls.txt
httrack -p0 -r3 www.nbcnews.com/tech
cp hts-cache/new.txt urls2/tech_urls.txt

