
# Clear workspace
rm(list = ls())

# Set working directory
setwd("/Users/nicholasluce/Documents/FALL\ 2017/Data\ Science/mail_online/")

# Load libraries
library(xml2)

# Setup data file
file.create(file.path(getwd(), "mail_online.csv"))

# Gather data
mail_online <- NULL
urls <- read.table(file.path(getwd(), "finalURLs.txt"), sep = "\n", header = FALSE)$V1
articleNum <- 0

for(articleURL in urls){
  
  # Debug
  articleNum <- articleNum + 1
  print(paste("Processing article number: ", articleNum, sep=""))
  
  # Read page information
  #articleURL <- "http://www.dailymail.co.uk/news/article-5094457/Mistress-sent-thugs-lover-s-family-home-faces-jail.html"
  articlePage <- try(read_html(articleURL, options = c("RECOVER", "NOBLANKS")), silent = TRUE)
  if('try-error' %in% class(articlePage)) next
  
  # Get date
  articleDate <- try(xml_find_first(articlePage, "//meta[@property=\"article:published_time\"]"), silent = TRUE)
  if('try-error' %in% class(articleDate)) next
  articleDate <- as_list(articleDate)
  articleDate <- sub("[^0-9-].*", "", attr(articleDate, 'content'))
  
  articleYear <- as.integer(sub("-.*", "", articleDate))
  articleMonth <- as.integer(sub("-.*", "", sub("....-", "", articleDate)))
  articleDay <- as.integer(sub("....-..-", "", articleDate))
  
  # Get section
  articleSection <- sub("/.*", "", sub("http://www.dailymail.co.uk/", "", articleURL))
  
  # Get headline
  articleHeadline <- try(xml_find_first(articlePage, "//title"), silent = TRUE)
  if('try-error' %in% class(articleHeadline)) next  
  articleHeadline <- as_list(articleHeadline)
  if(length(articleHeadline) != 1) next
  articleHeadline <- articleHeadline[[1]]
  
  # Get story
  articleStory <- try(xml_find_all(articlePage, "//p"), silent = TRUE)
  if('try-error' %in% class(articleStory)) next
  articleStory <- as_list(articleStory)
  articleStory <- paste(articleStory, sep = " ")
  articleStory <- sub("[\"][^\"]*$", "", sub("[^\"]*[\"]", "", articleStory))
  for(i in 2:length(articleStory)){
    articleStory[1] <- paste(articleStory[1], articleStory[i], sep = " ")
  }
  articleStory <- articleStory[1];
  
  # Add to data unless scuffed
  if(length(articleYear) != 1) next
  if(length(articleMonth) != 1) next
  if(length(articleDay) != 1) next
  if(length(articleSection) != 1) next
  if(length(articleHeadline) != 1) next
  if(length(articleStory) != 1) next
  
  articleData <- data.frame(year = articleYear, month = articleMonth,
                             day = articleDay, section = articleSection,
                            headline = articleHeadline, story = articleStory)
  
  mail_online <- rbind(mail_online, articleData)
}
  
# Write data to file
write.csv(mail_online, file.path(getwd(), "mail_online.csv"), row.names = FALSE)
