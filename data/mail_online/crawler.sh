#!/bin/bash

httrack -p0 -r3 www.dailymail.co.uk/news/index.html
cp hts-cache/new.txt news.txt

httrack -p0 -r3 www.dailymail.co.uk/sport/index.html
cp hts-cache/new.txt sport.txt

httrack -p0 -r3 www.dailymail.co.uk/usshowbiz/index.html
cp hts-cache/new.txt usshowbiz.txt

httrack -p0 -r3 www.dailymail.co.uk/auhome/index.html
cp hts-cache/new.txt auhome.txt

httrack -p0 -r3 www.dailymail.co.uk/femail/index.html
cp hts-cache/new.txt femail.txt

httrack -p0 -r3 www.dailymail.co.uk/health/index.html
cp hts-cache/new.txt health.txt

httrack -p0 -r3 www.dailymail.co.uk/sciencetech/index.html
cp hts-cache/new.txt science.txt

httrack -p0 -r3 www.dailymail.co.uk/money/index.html
cp hts-cache/new.txt money.txt

httrack -p0 -r3 www.dailymail.co.uk/travel/index.html
cp hts-cache/new.txt travel.txt

httrack -p0 -r3 www.dailymail.co.uk/columnists/index.html
cp hts-cache/new.txt columnists.txt