__HTTP_STATUS_CODE
200
__TITLE

What I wish I could tell my boss: 'I felt unsafe about returning to work'

__AUTHORS

Anonymous
__TIMESTAMP

Friday 13 October 2017 02.00EDT

__ARTICLE
You interviewed me and offered me the job on the spot. You explained that the role was working nights in your dementia care home  with the plan to shadow other carers for the first week so I could get used to the role.
But on my third shift I was thrown in the deep end. You told me it would just be me and two agency carers on shift for the night.
One agency carer arrived late and the other didnt give me a handover  so I had to work out what needed doing for myself. I didnt even know the codes to get into different parts of the building and had to keep asking the agency staff, who got annoyed.
After a couple of hours, the two agency staff settled into the lounge chairs and said they were taking a break and I had to answer the call bells. I started to panic as I didnt know what the residents individual care needs were but I managed to assist them the best way that I could.
After they settled into lounge chairs, the agency staff slept while I looked after the residents. A few times I asked them to help, but they only woke in the early morning to start getting the residents washed and ready.
In that time I witnessed some of the quickest washes I have seen. One resident begged not to get up and I told the agency carer to stop but she shouted at me and told me to get out.
We assisted one woman to wash, dress and mobilise into her wheelchair. The agency carer explained that she should really be hoisted but as there was no time to do it in the mornings that never happened.
Personal care was also hit and miss. I asked about residents own prescribed incontinence pads and was told that the manager locked these away in a cupboard. This meant that each resident was not getting their own pad thickness and this could have caused them painful pressure sores.
All the residents were up and dressed in the dining room by the time the day staff arrived for handover. As I said goodbye to the agency staff, one turned to me and whispered, What happens on nights, stays on nights.
But I just couldnt keep it a secret. After an internal battle, I realised resident safety was more important, and decided to tell you what I had witnessed.
You called me in for a meeting and said my accusations were being investigated. Then you said I was being placed on paid leave to protect me while the agency workers continued to provide care. The paid leave lasted for over a month. I continually asked you when I could return, but received no reply.
I was eventually given a date to discuss my return to work, but I started being harassed in the town by other carers who said you were against me and supported them. I told you I was beginning to feel unsafe about returning to work, but you said there was nothing you could do.
In the end I felt like I had no other choice: I decided to leave the social care sector. The way you handled my complaint put me off care for life. It was difficult for me to tell you about the poor care I witnessed. Why didnt you support me rather than suspending me?
You not only neglected to give me the on-the-job training you promised, but, more importantly, by allowing those agency carers to continue you are still letting vulnerable residents down.
