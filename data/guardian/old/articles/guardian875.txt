__HTTP_STATUS_CODE
200
__TITLE

The White Lion Inn, Cray, Yorkshire dales: hotel review

__AUTHORS

Kevin Rushby
__TIMESTAMP

Friday 13 October 2017 06.43EDT

__ARTICLE
When Yorkshire hosted the grand dpart of the Tour de France in 2014, the first taste of the wild for the peleton was climbing out of Wharfedale past a series of roaring waterfalls on the austere flanks of Buckden Pike. Halfway up that climb they will have scarcely noticed a small cluster of stone barns and farmhouses, one of which was an old drovers pub, the White Lion, which closed soon afterwards, apparently forever.
It was the kind of place that had never really been the same since shepherds stopped arriving with their flocks, expecting food and lodging for man and beast, en route to the bright lights of Skipton. Then, in 2015, Dennis Peacock and Amelia Johnson, who also run a restaurant in York, bought the place. They revamped the bar, rebuilt the bedrooms, rescued various antique tools and lit the fire. The White Lion was back in business.
On a cool October afternoon I am pleased to find that fire roaring in the grate and three real ales on hand pump. We always have Black Sheep, says Dennis, And the Wharfedale Blonde  the farmers like that.
One of the things the couple has got right is clear from the start: they attract customers from near and far, those with muddy boots and those without. I have definitely got muddy boots: a photo session on the stepping stones across the road proved rather wet. But could Dennis cope with the unexpected arrival of a drover with his flock? He laughs: Wed manage. Our neighbour has barns. And I reckon he means it. Not many places could serve star fruit at breakfast  as I later discover  and also handle a flock of Swaledales fresh off the fell.
The menu is designed to please all tastes too: fish and chips is there, rump of lamb too, but also options such as a sage and ginger vegetable crumble with crispy kale, or pan-fried cod loin with a clam, samphire and chorizo butter.
Ive got a good appetite, having scaled Buckden Pike, a fell that is 118th on the English all-time highest list  dont laugh, its quite a steep ascent and I did stray off-piste to visit High Cray Force, a wonderful stepped waterfall visible from the pub door. Dennis tells me an old story about how the falls, when driven back by a strong wind, form a white mane and roar loudly  hence the pubs name.
On a cool October afternoon I am pleased to find that fire roaring in the grate and three real ales on hand pump
The age of the building is unknown but we do know that electricity only arrived in 1955. Visitors dont like lighting their way to bed with a candle, Mrs Edith Horner told the local press, with a hint of disdain for such weaklings.
I doubt whether the formidable Edith served canapes, then a pear salad with blue cheese and walnuts as a starter (7), nor would she recognise the artfully presented rump of lamb (24). As for the ice-cream  well, she did not possess a refrigerator. The food is all excellent. Plenty of mains come in at around 15.
The nine bedrooms have been totally redone: white-walled with plaid carpets and a decent tea and coffee tray (complete with flask of fresh milk and jar of biscuits). Some tastes might find them a bit minimal  there are no pictures on the walls  but Im happy with the warm window seat and the view of the fells. I also like the roar of the beck rushing past below.
The bathroom lacks a bath, but boasts a powerful shower plus bathrobes, heated rails and plenty of big fluffy white towels.
By eck, Mrs Horner, I can hear those drover ghosts grumble. Theres sheep in me room and water falling from tceiling!
 Accommodation was provided by the White Lion Inn (doubles from 100 B&B, 01756 760262, bestpubinthedales.co.uk)
 Phil Richards, Yorkshire dales ranger for Wharfedale 
 WalkA favourite route heads west from Cray and follows the contours around to Yockenthwaite. There it joins the Dales Way long-distance footpath, down the left bank of the Wharfe to Hubberholme and back to Cray.
 EatIn Hubberholme, the George Inn does a good Sunday roast and is just over the river from the 12th-century church.
 VisitFrom the park car park in Grassington, its a five-minute walk to the Wharfe and Linton Falls, which are especially spectacular after rain.
