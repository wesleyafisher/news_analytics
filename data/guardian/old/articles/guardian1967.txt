__HTTP_STATUS_CODE
200
__TITLE

Tom Dumoulin takes world time trial with Chris Froome third

__AUTHORS

Sean Ingle
__TIMESTAMP

Wednesday 20 September 2017 14.27EDT

__ARTICLE
Chris Froome pushed his weary body to its limit in the pursuit of his first world time-trial championship, but on a day of sunshine and showers in Bergen there was no rainbow jersey  or gold medal  awaiting him at the end of it.
However the 32-year-old Englishman took immense satisfaction in a spirited effort on the final climb, which pushed him into bronze, behind the Dutchman Tom Dumoulin, who obliterated his rivals on the 31km course to win in 44min 41sec  57.69sec ahead of the Slovenian Primo Rogli in second, with Froome 25sec further back.
Afterwards Froome, smiling but spent, admitted he was delighted with a second medal of the championships, alongside a team time-trial bronze with Team Sky on Sunday. Of course it would have been nice to be fighting for gold, but I have no regrets, he said. I gave it absolutely everything I had.
However he conceded that a combination of driving rain and weary legs from his victory in the Vuelta a Espaa, had made for a hard shift in the saddle. The weather conditions were tough, he added. It started raining halfway round after my first lap, and those corners were pretty slippery. I heard quite a few guys crashing as well. I wanted to stay upright but there was a hard balance between going too fast and being too conservative.
By the time Froome began his time trial, at 5pm Norwegian time, the gun-metal skies and damp roads that had greeted the early riders had given way to brighter skies and much easier conditions. However he was perhaps overly cautious to start with and at the 11.5km checkpoint was 13.66sec down in eighth place.
He had improved one place as he approached the final part of the course, up Mount Floyen, a 3.4km mini-beast of a climb with a gradient of 9.1% and 19 hairpin bends. But Froome gritted his teeth, got his pedals moving and was able to improve from seventh going into the final climb to make the podium by 7.27sec.
But he was a long way behind the pre-race favourite Dumoulin, who had targeted this race even before his surprise victory in the Giro dItalia this year. The 26-year-old, who won the Olympic time-trial silver medal in Rio last year and also won the team time-trial with Sunweb on Sunday, was simply a class apart.
I thought my power meter was off because it was so high, Dumoulin said. I felt really, really good. It started raining and I needed to take the corners really slow, especially in the first kilometre of the climb with all the twists and turns  on every corner my back wheel was slipping as I had the TT tyres on because I thought it was going to be dry.
However Dumoulin never looked in any real trouble, unlike many of the other 63 riders. Polands Maciej Bodnar came a cropper on the first bend after skidding on a pot hole at the first turn and coming off his bike, while Frances Alexis Gougeard was in contention for a podium place until his chain came off.
The devilish choice confronting every rider beforehand was whether to stick with their time-trial bikes up Mount Floyen, or to change to a conventional and lighter one at the bottom of the climb. The perils of getting it wrong were illustrated by the first rider of the day, Alexey Lutsenko of Kazakhstan, who struggled to get his feet into the pedals after changing bikes and lost around half a minute after needing several pushes to get moving again.
Dumoulin, however, decided to stick with his time-trial bike throughout  as did Froome  and was rewarded for his choice. I was doubting for a long time actually, he said. But I took the decision not to risk it. It was the right one, I think.
No one would dispute that. Dumoulins overwhelming dominance on the day, however, rather made it academic.
