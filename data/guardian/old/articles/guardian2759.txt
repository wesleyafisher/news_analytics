__HTTP_STATUS_CODE
200
__TITLE

A bad-tempered Brexit is a risky move for universities

__AUTHORS
Lesley Wilson
__TIMESTAMP

Wednesday 11 October 2017 07.24EDT

__ARTICLE
Brexit  that is not the future of Europe, said Jean-Claude Juncker, the president of the European Commission, in his annual agenda-setting state of the union speech in September. While few universities on the continent would be so dismissive, it is fair to say that Brexit has lost its sense of urgency in many places. It has happened, its a shame, lets get on with it, largely sums up the mood.
This is how the EU has handled Brexit. As opposed to the UK, where the issue is at the centre of political power struggles, it has been de-politicised in Brussels. Capable, trusted administrators led by Michel Barnier are handling the Brits, and political battles are fought elsewhere.
In Brussels, the Barnier approach to the negotiations has been to build a coherent, well-oiled machine that takes the UK out of the EU in a number of predefined steps. It focuses not on flexibility and imagination, nor on constructive ambiguity, but aims to be efficient and transparent. 
A much less passionate approach to Brexit means its the formal legal framework which counts. This reduces the level of threat for the university sector, compared to the warnings of a bad-tempered breakdown that the UK chancellor, Phillip Hammond, has articulated. The essential message from universities in the continent is that everyone would like collaboration to continue - and they will do their best to ensure this happens. 
Nevertheless, there are several risky areas where universities must encourage the UK government to keep its cool if this collaboration is to be maintained. The most pressing among these is the financial settlement, or Brexit divorce bill. In 2013, the UK agreed, like other members, to pay into the EU budget for the next seven years, and the EUs financial planning was worked out accordingly. If the UK fails to honour this commitment, as has been suggested during some of the more heated moments in the negotiations, this could have repercussions for research funding. 
For example, if a consortium of researchers is awarded an EU grant in 2018 for a three-year project, the EU needs to know whether it will have the money to cover those three years or not. If the UK decides that it will not pay into Horizon 2020, the research programme, the EU will be short of 10-15% of the money needed. 
The European Commission introduced a note on its research and innovation portal last week, which warned that if the UK withdraws from the EU without an agreement with the EU, British researchers funded under the Horizon 2020 programme will lose access to their grants.
Furthermore, the UKs participation in the programme from 2019 onwards has to be decided as soon as possible. Since the UK is the biggest participant at present, this is important for everyone. 
Once the divorce bill has been settled, association agreements formalising the UKs status as a third country will need to be signed so that it can continue participating in EU research initiatives. Legally speaking, this is not terribly complicated, and its likely to meet with a positive reception from EU universities which want to maintain their UK partners.
For UK universities to be fully active in the EUs higher education, science and research activities after Brexit, they will need to finalise trade agreements with individual countries. It will probably take around a decade to do this, and universities should seek to ensure their interests are represented in negotiations. For instance, countries that are sceptical about foreign providers could put onerous requirements in place to limit their activities. 
The area with greatest potential for conflict is the zero-sum game over the recruitment of researchers. Good researchers are a scarce and finite resource. The UK has excellent research environments that allow them to fulfil their potential; it is unmatched as the EU research superpower. If the UK makes it difficult for researchers to secure visas, other countries will want to get their share of the spoils. Equally, in the event that the UK leaves Horizon 2020  which could happen quickly if accounts are not settled  continental institutions will not be slow to use access to EU funding as a bait with which to snare UK-based researchers.
Looking at the big picture, the cool, rational continental approach favours university collaboration in future. This is a game for the university sector to win.
Join the higher education network for more comment, analysis and job opportunities, direct to your inbox. Follow us on Twitter @gdnhighered. And if you have an idea for a story, please read our guidelines and email your pitch to us at highereducationnetwork@theguardian.com.
Looking for a higher education job? Or perhaps you need to recruit university staff? Take a look at Guardian Jobs, the higher education specialist
