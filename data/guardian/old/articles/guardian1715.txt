__HTTP_STATUS_CODE
200
__TITLE

Whales and dolphins lead 'human-like lives' thanks to big brains, says study

__AUTHORS

Katharina Kropshofer
__TIMESTAMP

Monday 16 October 2017 14.02EDT

__ARTICLE
Life is not so different beneath the ocean waves. Bottlenose dolphins use simple tools, orcas call each other by name, and sperm whales talk in local dialects. Many cetaceans live in tight-knit groups and spend a good deal of time at play.
That much scientists know. But in a new study, researchers compiled a list of the rich behaviours spotted in 90 different species of dolphins, whales and porpoises, and found that the bigger the species brain, the more complex  indeed, the more human-like  their lives are likely to be.
This suggests that the cultural brain hypothesis  the theory that suggests our intelligence developed as a way of coping with large and complex social groups  may apply to whales and dolphins, as well as humans.
The researchers gathered records of dolphins producing signature whistles for dolphins that are absent
Writing in the journal, Nature Ecology and Evolution, the researchers claim that complex social and cultural characteristics, such as hunting together, developing regional dialects and learning from observation, are linked to the expansion of the animals brains  a process known as encephalisation.
The researchers gathered records of dolphins playing with humpback whales, helping fishermen with their catches, and even producing signature whistles for dolphins that are absent  suggesting the animals may even gossip. 
Another common behaviour was adult animals raising unrelated young. There is the saying that it takes a village to raise a child [and that] seems to be true for both whales and humans, said Michael Muthukrishna, an economic psychologist and co-author on the study at the London School of Economics.
Like humans, the cetaceans, a group made up of dolphins, whales and porpoises, are thought to do most of their learning socially rather than individually, which could explain why some species learn more complex behaviours than others. Those predominantly found alone or in small groups had the smallest brains, the researchers led by Susanne Shultz at the University of Manchester wrote.
Luke Rendell, a biologist at the University of St Andrews who was not involved in the study, but has done work on sperm whales and their distinctive dialects, warned against anthropomorphising and making animals appear to be like humans.
There is a risk of sounding like there is a single train line, with humans at the final station and other animals on their way of getting there. The truth is that every animal responds to their own evolutionary pressures, he said.
There is definitely a danger in comparing other animals to humans, especially with the data available. But what we can say for sure, is that this cultural-brain hypothesis we tested is present in primates and in cetaceans, Muthukrishna said.
There was still much more to learn, though, he added. Studies with underwater mammals are difficult and vastly underfunded, so there is so much we dont know about these fascinating animals, he said.
The fascination, however, should not only be interesting for people studying animals. We dont have to look at other planets to look for aliens, because we know that underwater there are these amazing species with so many parallels to us in their complex behaviours, said Muthukrishna. 
Studying evolutionarily distinct animals such as cetaceans could act as a control group for studying intelligence in general, and so help the understanding of our own intellect.
It is interesting to think that whale and human brains are different in their structure but have brought us to the same patterns in behaviour, Rendell said. The extent of how this is close to humans can educate us about evolutionary forces in general.
However, Muthukrishna points out that intelligence is always driven by the environment an animal finds itself in. Each environment presents a different set of challenges for an animal. When you are above water, you learn how to tackle fire, for example, he said. As smart as whales are, they will never learn to light a spark.
