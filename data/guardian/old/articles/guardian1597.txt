__HTTP_STATUS_CODE
200
__TITLE

A Woman of No Importance review  Eve Best takes a feminist grip on Wilde's melodrama

__AUTHORS

Michael Billington
__TIMESTAMP

Tuesday 17 October 2017 07.21EDT

__ARTICLE
A West End Oscar Wilde season, backed up by talks and lectures, is an excellent idea. Its just a pity that it begins with a play that is neither Wildes first nor one of his best. The one big surprise about Dominic Dromgooles production, starring Eve Best and Anne Reid, is that it perks up no end the more the play dwindles into absurdity.
 Wilde makes his intentions clear from the start when someone observes the world is made for men, not for women. We see this borne out through the social success of the cynical Lord Illingworth, who trades on his reputation for glibly phrased wickedness. But 20 years earlier, he had a son, Gerald, born out of wedlock to a woman who calls herself Mrs Arbuthnot. Where she has suffered for her sins, Lord Illingworth has got off scot-free and it is only when he proposes to take the boy on as his private secretary that she asserts her claims as a devoted mother and a wronged woman.
 The problem is that the play splits all too easily into two halves: sedentary chat and sentimental melodrama. In the first two acts we listen patiently as the characters sit around Lady Hunstantons house swapping endless epigrams about relationships or being lectured by an insufferable American puritan about the heartlessness of the aristocracy. It is only when Mrs Arbuthnot and Lord Illingworth battle over ownership of their son that the chat gives way to drama and even here we are aware of the situations implausibility. As the dramatist and critic St John Ervine pointed out many years ago, you wonder how Mrs Arbuthnot replied when her son, in his childhood and youth, asked about his fathers identity.
 If it is the melodrama that now grips us, it is because Best endows Mrs Arbuthnot with a fierce emotional intensity. Earlier this year she played another possessive mother in Rattigans Love in Idleness. Now, she ups the stakes to show us a woman who has used her status as a social victim to lovingly protect her son. But Best does two things that prevent Mrs Arbuthnot seeming a devouring monster.
 She lightly hints, in her climactic encounter with Lord Illingworth, at the sensuality that once drove him wild. Confronted by the idea that she might repair the situation by marrying him, she also breaks into subversive laughter. Best takes a character who can easily appear wreathed in pious rectitude and turns her into a real woman who actively relishes her independence.
 Dominic Rowan as Lord Illingworth has the right suave caddishness. There is good, too, work from some of the plays sofa-bound satellites: Emma Fielding is very persuasive as a society flirt, Eleanor Bron very funny as a velvet-voiced aristo who treats her husband as a piece of permanently lost property, and Phoebe Fildes makes her mark as a young wife steeped in romantic melancholy. Anne Reid seems less happily cast as a titled rural hostess, but she compensates by singing a number of Victorian ballads that help to cover the changes to Jonathan Fensoms set. It will never be anyones favourite Wilde play but at least it establishes the dramatists feminist credentials and, in Dromgooles revival, takes the mothballs out of the melodrama.
  At the Vaudeville, London, until 30 December. Box office: 0330-333 4814. 
