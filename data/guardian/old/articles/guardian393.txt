__HTTP_STATUS_CODE
200
__TITLE

Houston Astros move through to ALCS with 5-4 win over Boston Red Sox

__AUTHORS
Associated Press
__TIMESTAMP

Monday 9 October 2017 17.41EDT

__ARTICLE
Justin Verlander outpitched Chris Sale in a relief role reversal of aces, and the Houston Astros advanced to their first AL Championship Series, rallying past the Boston Red Sox 5-4 Monday in Game 4 of their playoff matchup.
Houston will open the ALCS on Friday, either at Cleveland or at home against the New York Yankees. The Indians held a 2-1 edge over the Yankees going into Game 4 of the AL Division Series on Monday night.
With Verlander and Sale  the Game 1 starters  both pressed into relief, the Astros prevailed to win the ALDS 3-1.
Alex Bregman homered off Sale to tie it in the eighth and Josh Reddick hit an RBI single off closer Craig Kimbrel later in the inning.
The Astros last reached the league championship series in 2005 as a National League team, and were swept in the World Series by the White Sox.
Carlos Beltran added to his legacy of postseason success with an RBI double in the Houston ninth for a 5-3 lead. Red Sox rookie Rafael Devers opened the bottom of the ninth with an inside-the-park homer off Ken Giles, the ball hitting above center fielder George Springers leap and bouncing off the angled Green Monster.
Giles retired the next three batters for a six-out save.
Springer and Yuli Gurriel each had three hits for the AL West champions, and Reddicks go-ahead single made up for misplaying a fly ball into a home run in Game 3 to force a fourth game.
Verlander allowed just one hit: A two-run homer to Andrew Benintendi  the first batter he faced  that gave Boston a 3-2 lead in the fifth. Verlander wound up with the win in his first pro relief appearance after making 424 starts in the majors and minors.
The big-hander, acquired late in the season from Detroit, also beat Sale in the playoff opener and is now 7-0 for his new team. Verlander went 2 2/3 innings in relief of starter Charlie Morton.
Sale pitched 4 2/3 innings, allowing two runs and four hits, striking out six.
On a rainy day at Fenway Park  the fourth straight day game  the Red Sox again saw a starter struggle early, with Rick Porcello giving up Houstons eighth first-inning run of the series. The reigning AL Cy Young winner, who led the AL with 22 wins last year and the majors with 17 losses in 2017, gave up two runs in three innings, walking three and striking out four while allowing five hits.
Like Houston, the Red Sox called on their ace in relief.
Sale was sharp before giving up Bregmans leadoff homer in the eighth. He allowed a one-out single to Evan Gattis before closer Kimbrel came on with two outs, walked Springer and gave up Reddicks single.
Xander Bogaerts also homered for the AL East champion Red Sox, and Hanley Ramirez had two hits a day after going 4 for 4 in Bostons only postseason win since the end of the 2013 World Series.
Red Sox manager John Farrell was ejected by home plate umpire Mark Wegner in the bottom of the second inning after coming out to argue a called third strike on Dustin Pedroia. The previous batter, Jackie Bradley Jr, had also been called out strikes on a close pitch.
It is the 19th ejection of Farrells career and his third this season.
The Red Sox ball girl was called for interference when she tried to field Gattis fair-ball grounder down the third base line in the eighth. Instead of a potential double, Gattis was sent back to first; pinch-runner Cameron Maybin took second on a wild pitch and scored the go-ahead run on Reddicks single.
The Red Sox loaded the bases with nobody out in the second inning and failed to score. They also ran themselves out of the third inning, when they got two singles and a double without getting a run.
Benintendi led off with a single and then got doubled off first on Mookie Betts hard liner to third. Mitch Moreland doubled, and then got thrown out at the plate  easily  on Ramirezs single to left. The Red Sox had 29 runners thrown out at the plate this season, the most in the majors.
The teams finished Game 3 before 6.30pm on Sunday but didnt find out until about 11pm what time they would be playing Game 4, because TV wanted to keep the Yankees in prime time. That left the Red Sox and Astros with a brief afternoon window before the rain began to fall, as expected.
The game started on time and was not delayed, but the rain kept the grounds crew busy raking drying agent on the infield.
