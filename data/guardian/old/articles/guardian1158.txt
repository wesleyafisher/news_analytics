__HTTP_STATUS_CODE
200
__TITLE

What is your experience of getting older and working? Share your stories

__AUTHORS

Tom Levitt
__TIMESTAMP

Tuesday 15 March 2016 04.55EDT

__ARTICLE
Workers in the UK will have the worst pensions of any major economy, according to research from the Organisation of Economic Cooperation and Development, and have to work longer than anywhere else before they qualify for a state pension. 
Many older people feel they dont have agency over their work lives once they approach retirement age. A quarter of people about to retire cant afford to give up work and for those who can, 51% would prefer to keep active but often lack opportunities. Businesses dont currently seem to have the flexibility or foresight to adapt employment models for an ageing population.
Wed like to hear from readers about what its like being an older person in the workforce. Do you worry about how youll ever afford to retire? Are you eager to slow down but concerned about surviving on your pension? Or, are you someone who longs to continue working as long as possible but are concerned about a lack of opportunity for you simply because of your age? 
Give us your views by filling out the form.
