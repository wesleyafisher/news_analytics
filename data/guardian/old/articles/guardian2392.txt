__HTTP_STATUS_CODE
200
__TITLE

Wellbeing enhanced more by places than objects, study finds

__AUTHORS

Caroline Davies
__TIMESTAMP

Thursday 12 October 2017 10.30EDT

__ARTICLE
The poet WHAuden is credited with first coining the word topophilia to describe a strong emotional pull to a special place.
Now scientific research, using cutting-edge brain imaging, suggests Auden was on to something. According to a study commissioned by the National Trust, people experience intense feelings of wellbeing, contentment and belonging from places that evoke positive memories far more than treasured objects such as photographs or wedding rings.
A Functional Magnetic Resonance Imaging (fMRI) study commissioned by the NT set out to understand this visceral but intangible feeling more deeply.
The NT report, Places That Make Us, commissioned Surrey University academics and research experts to conduct fMRI scans as well as in-depth interviews with volunteers, and an online survey of 2,000 people, about their special places.
Working with leading researchers and academics, and using cutting-edge fMRI brain technology, we examined how places affect people, how they become special and why we feel a pull towards them, said Nino Strachey, head of research for the NT.
It found places that are intensely meaningful invoke a sense of calm, space to think and a feeling of completeness.
Researchers measured 20 volunteers brain activity as they were shown pictures of landscapes, houses, other locations and personally meaningful objects. Places, rather than objects, with strong personal ties caused their brains to get the most excited. Specifically, an area of the brain associated with emotional responses, the amygdala, was fired up.
Therefore, we can conclude that significant places more likely contain greater emotional importance than objects, as areas in our brain involved in emotional processing respond more strongly to significant places, the report states.
fMRI opens a window into the brain allowing us to explore automatic and hard to verbalise emotional responses, said Bertram Opiz, professor in neuroimaging and cognitive neuroscience at the University of Surrey and the Cubic fMRI facility, who conducted the tests. 
Volunteers looked at images of 10 places and 10 objects meaningful to them. Ten images of everyday places and objects, and 10 positive and 10 negative images that had been quantified for their emotional content were also shown. Each image was presented three times.
Three response areas were identified, the left amygdala, which plays a key area in the automatic processing of emotion, the medial prefrontal cortex, which evaluates a positive or negative emotion, and the parahippocampal place area, which responds to personally relevant place.
Volunteers were interviewed in depth twice  once at home and once at the location of their special place.
The research showed favourite places stimulated a feeling of belonging, of being physically and emotionally safe, and of a strong internal pull to the place. The majority of those questioned (86%) agreed this place is part of me, while 60% felt I feel safe here and 79% described Im drawn here by a magnetic pull.
It found the brains emotional response to special places was much higher than towards meaningful objects. Two thirds of those surveyed (64%) said their special place made them feel calm, while 53% said it provided an escape from everyday life. Among younger people, 67% said their meaningful place had shaped who they were.
Dr Andy Myers, consultant on the research, said: For the first time we have been able to prove the physical and emotional benefits of place, far beyond any research that has been done before.
With meaningful places generating a significant response in areas of the brain known to process emotion, its exciting to understand how deep-rooted this connection truly is.
