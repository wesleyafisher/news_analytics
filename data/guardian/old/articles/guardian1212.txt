__HTTP_STATUS_CODE
200
__TITLE

Commercial flights resume to areas affected by Hurricane Irma

__AUTHORS

Will Coldwell and 
Sarah Marsh
__TIMESTAMP

Tuesday 12 September 2017 11.36EDT

__ARTICLE
Airlines and tour operators serving destinations affected by Hurricane Irma are expected to return to normal operations this week. This comes after thousands of flights were cancelled when Florida, Cuba and other islands across the Caribbean were battered by winds in excess of 130mph, killing at least 49 people, causing extensive damage and flooding and leaving millions homeless and without power.
Having passed the Leeward Islands, St Martin, the British Virgin Islands, Puerto Rico, the Dominican Republic and Cuba  where damage is now being assessed by authorities  Irma is now moving north across Florida into south-west Georgia and eastern Alabama. By Sunday (10 September), more than 12,500 flights had been cancelled because of Irma, according to flight-tracking service FlightAware, with airlines including British Airways continuing to make cancellations on Monday.
However, as the relief effort begins, travel is starting to resume. The Caribbean Tourism Organization (CTO) and the Caribbean Hotel and Tourism Association (CHTA) have been collecting reports from across the islands, which have been affected by the hurricane. The Dominican Republic, Haiti, Antigua and Anguilla experienced minimal damage, with some hotels expected to welcome guests from as early as Thursday. Tourism infrastructure in Puerto Rico is also operational and the island is continuing to welcome new visitors. 
Barbuda and the British Virgin Islands were devastated by Irma. In Barbuda 90% of homes were destroyed. Hotel infrastructure was also damaged but, according to the CTO, with fewer than 100 hotel rooms, the overall effect on tourism is minimal. According to a statement from the director of tourism for the British Virgin Islands, Sharon Flax-Brutus, the damage has been devastating, with communication and power down. Individual hotels have reported some manageable damage.
On the Turks and Caicos islands, hotels are assessing damage. Several were scheduled for annual closure ahead of Irma, with at least a dozen now remaining closed until at least October. Others are open, with more due to reopen in the coming weeks. In a statement updated today (Tuesday), British Airways said it expects to operate flights from Miami to Heathrow and one of its two daily flights from Orlando to Gatwick.
Though all BA flights between Gatwick and Tampa remain cancelled, planned services between Gatwick and Fort Lauderdale are expected to operate as normal on Thursday. British Airways flights across the Caribbean are planned to operate as normal, though the airline has chartered two regional jets to fly between Providenciales and Antigua.
However, some customers stuck on Turks and Caicos criticised the airlines communication. Steve Dandy, 55, from Greater Manchester, who has been stranded at his resort with his family said: The communication was poor. They sent one text and one email saying the flight was cancelled with a number to ring if we wanted a refund or to rebook. It was known worldwide that comms were down where we were. All it needed was an email saying that they were working on an alternative plan to repatriate us.
Helen Crosse, who is at the same resort, tweeted:Come on British Airways start giving some answers to your customerswho are stranded and struggling to contact you.
Come on British airways start giving some answers to your customers who are stranded and struggling to contact you 
British Airways said: We understand that being caught up in a hurricane of this scale must be a very distressing experience for all those who were in its path, and how important it is to get them back home as quickly as we can. Infrastructure damage on the affected islands has made communication with our customers challenging.
Airports in Cuba, where tourists are being evacuated, are expected to reopen today. In a statement Thomas Cook said: We continue to work on our evacuation programme for Varadero. The first evacuation flight to Cuba is in the air, and we expect to bring home all customers in Varadero by Thursday.
It continues: We are reviewing our flying programme back from Orlando and will advise customers of our plans to bring them home once Orlando airport has reopened. We are increasing our special assistance team to 66 offering support to our customers on the ground in Cuba and Orlando.
According to Thomas Cook, the Dominican Republic is now operating as normal. Damage to Holgun, Cuba, has been minimal, with the majority of hotels not suffering any significant impact. It is offering free amendments and cancellations to all customers due to travel to Holgun, Orlando, Miami and the Florida Keys up to and including Friday.
The operator is also offering free cancellation or amendments on departures up to and including 31 October to UK customers travelling to Cayo Coco or Cayo Guillermo in Cuba, as well as to those travelling to Varadero for departures up to and including 21 September.
First Choice is currently arranging flights home for its guests in Florida and Cuba; however it hopes to begin operating delayed flights from the UK to Sanford airport, Orlando, from tomorrow. Customers due to travel to Sanford before then can cancel or amend flights free of charge.
First Choice has cancelled flights from UK to Cuba until as late as 21 September. Customers who were due to travel on the cancelled flights to Cayo Santa Maria or Varadero between 7 and 21 September are able to rebook to any holiday currently on sale with a 50pp rebook incentive, or cancel and receive a full refund. Customers currently in Varadero will be flown home as soon as the airport becomes operational and will receive a full refund. First Choice customers arriving in the Dominican Republic will continue their holidays as planned.
Virgin Atlantic said customers booked to travel to, or via, affected destinations between 5 and 17 September may rebook free of charge to a later date or alternative destination. Customers due to travel to Orlando between 12 and 17 October are also being offered the opportunity to rebook free of charge. 
Virgin Holidays said that any customers travelling to Antigua, Havana, Miami, Atlanta or Orlando with Virgin Atlantic between 11 and 17 September have the option to amend travel dates, or travel to an alternative Virgin Atlantic destination.
Carnival Cruises confirmed its ships are safe, though some itineraries have been shortened. 
First Choice: firstchoice.co.uk 
Thomas Cook: thomascook.com
 Thomson: thomson.co.uk
Virgin Holidays: virginholidays.co.uk
Carnival Cruises: carnival.com
BA: britishairways.com
Virgin Atlantic: virginatlantic.com 
