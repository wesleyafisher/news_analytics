__HTTP_STATUS_CODE
200
__TITLE

How to cook, and eat, like an Italian: a foodie week in Puglia

__AUTHORS

Harriet Green
__TIMESTAMP

Sunday 6 August 2017 02.00EDT

__ARTICLE
We walk into the massive kitchen and Aldo, the chef, announces that he can tell at a glance who does the cooking at home. Hes not talking about me. My husband exudes calm, Aldo says. He has, it seems, the look of someone who can stand the heat. Stung by this, Im determined to prove Aldo wrong, and outshine my husband.
Weve come to Puglia with our teenage daughter, to cook like locals at Borgo Egnazia cooking school. On todays menu is orecchiette, classic Puglian pasta shaped like little ears.
We start with semolina flour. So much better for you than 00 flour, says Aldo. Its high in protein  and melatonin, which is why people feel sleepy afterwards.
Eggs are not used in Puglian pasta. We add water, little by little, until we have firm dough (think Play-Doh), roll it into thin snakes, snip it into tiny pieces. Then Aldo shows us the rapid movement necessary to shape the ears  a thumb print and a little flick. Women in the regions capital, Bari, turn this movement into a kind of street theatre, pressing out hundreds of ears every minute to sell to hungry tourists.
Pasta ready, we fold and skewer thin slices of steak around crumbs of dry cheese, make meatballs  polpette  and cook them in ragu while Aldo talks lyrically about Sunday lunch at his grandmothers. Then we eat what weve cooked. Food doesnt get much more simple or more delicious than this. I relax and stop feeling cross about my husband being singled out as the natural chef.
Puglia, the heel of Italy, has beaches, olive groves and ancient hilltop towns. But you come to Puglia to eat: orecchiette with turnip tops and cavatelli pasta with seafood; stracciatella (a type of buffalo milk cheese) from Andria; fiaschetto (flask-shaped) tomatoes from Torre Guaceto; capocollo (lightly smoked salami) from Martina Franca; grilled and raw fish from Polignano and Savelletri; raw sea-urchins and octopus from the market in Bari; meaty bombette from Cisternino.
Food is more important here, culturally, than anywhere Ive been before
Food is more important here, culturally, than anywhere Ive been before. And not just the food itself but the business of sitting down to eat together. Thats why people are willing to wait for as long as it takes at certain popular restaurants. On a hot Friday night we learn this for ourselves at Zio Pietro, in Cisternino when we join a long queue at the restaurant door. Moving slowly forward, we find ourselves inside what looks like a butchers shop: there is a till, scales and one of the staff manhandling raw steaks in pools of blood, sausages, rolled bombette, chicken legs and more.
It takes 45 minutes to reach the counter and order, then we take our seats in a cavernous room with a large-screen TV showing tennis highlights. After 90 minutes, we start wondering if our food has been lost. But looking around we see that everybody is waiting. And they all look really happy: shouty happy, in the special Italian manner. This is how it is meant to be.
When the steak, sausage, bombette and salad finally arrive, we fall upon the meat as if we havent eaten for months, which is ridiculous because in Puglia we never seem to stop eating. The secret is to pace yourself, as we learn another night at a restaurant in Alberobello, celebrated for its collection of round-turetted, slate-roofed white houses, or trulli, that typify the region. Next door to the largest trulli of all is a restaurant called Terra Madre (Mother Earth).
The journey from farm to fork doesnt get much shorter than here: an entire wall has been removed, so that you can step directly from your table into a field, neatly ploughed with lettuces poking through the soil. Each table is surrounded by baskets filled with organic produce plucked from the garden that day.
When we order, we aim for something modest, so we are unprepared for the series of plates that come to us, one after the other: stuffed this, grilled that, marinated the other. The presentation is amazing even though it isnt high end. The staff are casually dressed and informal, but obviously love great food, and sharing it with others.
Back at Borgo Egnazia, the hotel that runs the cookery class, we meet another chef, Mimina. Shes not the thrusting, ambitious purveyor of cutting-edge fusion cuisine you might expect to find at a smart resort hotel.
Mimina is not a chef, she insists. Shes a cook, serving strictly what she learned to cook at home: panzerotti, tiny fried turnover parcels filled with cheese, the softest focaccia (made with boiled potatoes and topped with tomato, unlike the more familiar drier version from Genoa), orecchiette with turnip tops, lasagne, and chicory beans, and piles of taralli, the addictive tiny bread biscuits.
After weve eaten, Mimina comes to talk to us about how devoted she is to Puglian cooking. As if there were any doubt about that, she hops up and dashes to the kitchen to load us up with sackloads of almond biscuits (dolcetti di mandorle) and more savoury taralli for the journey home.
Flying out of Puglia, six days after arriving, we roll into the airport considerably heavier than when we arrived, stuffed full, by good people, and their love of food.
Masseria Le Carrube has doubles from 125 a night B&B. Borgo Egnazia has doubles from 215 a night B&B. The cookery school runs throughout the year
