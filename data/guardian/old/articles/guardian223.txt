__HTTP_STATUS_CODE
200
__TITLE

University claims Viking burial clothes woven with 'Allah' discovered in Sweden

__AUTHORS

Martin Belam
__TIMESTAMP

Friday 13 October 2017 05.51EDT

__ARTICLE
A Swedish university has claimed to discover Arabic characters for Allah and Ali woven into Viking burial clothes. Researchers at Uppsala University describe the finding of the geometric Kufic characters in silver on woven bands of silk as staggering.
The researchers at Uppsala, Swedens oldest university, were re-examining clothes that had been in storage for some time. They had originally been found at Viking burial sites in Birka and Gamla Uppsala in Sweden. Textile archaeology researcher Annika Larsson told the BBC that at first she could not make sense of the symbols, but then, I remembered where I had seen similar designs: in Spain, on Moorish textiles.
#Viking Age script deciphered  mentions Allah and Ali https://t.co/zdkDy0BeRL pic.twitter.com/7wpUoWstR7
This led to the identification of the name Ali in the text and, when looked at in a mirror, the word Allah in reverse was revealed.
Perhaps this was an attempt to write prayers so that they could be read from left to right, said Larsson. Arabic characters are more typically inscribed right to left. The finding contradicts theories that Islamic objects in Viking graves are only the result of plunder or trade because, she explained, the inscriptions appear in typical Viking age clothing that have their counterparts in preserved images of Valkyries.
Larsson said the choice of burial clothes reflected the fineries of Viking life rather than the day-to-day reality, in much the same way that in the modern era people are buried in formal clothes. Presumably, Viking age burial customs were influenced by Islam and the idea of an eternal life in paradise after death.
However the finding has been disputed. In a blog post, Carolyn Priest-Dorman suggests, from analysis of the weaving technique in the clothes, that the recognition of the Kufic inscriptions is predicated on unfounded extensions of pattern, not on [the] existing pattern.
In addition, Stephennie Mulder, Associate Professor of Islamic Art and Architecture at the University of Texas at Austin, has suggested that the Viking burial finds pre-date the development of the Islamic artistic style Larsson claims to have identified.
Viking contact with the Islamic world, however, is a well-established fact. There have been finds of more than 100,000 Islamic silver coins known as dirhams in Viking-age Scandinavia. DNA analysis of Viking graves has also shown that some of them contain people who originated in Persia.
The Vale of York hoard, discovered near Harrogate in 2007, contained objects relating to three belief systems  Islam, Christianity and the worship of Thor  and at least seven different languages. And in March 2015 a Viking womans glass ring was discovered bearing the inscription for Allah or to Allah.
The textile finds with Larssons claimed Arabic script are on display at Enkpings museums exhibition on Viking couture until February 2018.
