__HTTP_STATUS_CODE
200
__TITLE

Time for a rethink about locking people up

__AUTHORS
Letters
__TIMESTAMP

Sunday 15 October 2017 13.47EDT

__ARTICLE
Your report about the demonstration at Long Lartin (Disorder at Long Lartin raises fears about jails, 13 October) missed a key issue which has bedevilled the debate about the recent prison crisis. The debate has no sense of history. There were widespread demonstrations in the pre-cuts era when there were little or no staff shortages. Long Lartin is a maximum security prison. Between 1969 and 1983, there were at least 10 major disturbances in five of the eight male, maximum security prisons at the time. These demonstrations, involving long-term prisoners, were not about overcrowding or staff shortages but were about the bleak, alienating regimes which were operating in these prisons and about the violence that was being perpetrated by staff in places like Hull, especially towards black and Irish prisoners. Unless the government, and the Labour opposition, recognise this history, learn from the abject failures of the past and move towards a radical response to the prison crisis, these demonstrations are likely to continue, in long-term and short-term prisons, with potentially devastating consequences for all concerned.Professor Joe SimSchool of Humanities and Social Science, Liverpool John Moores University
 Prison has become a very expensive way of making people less good citizens. Short sentences are particularly illogical. When will a bold defence solicitor submit that it would be a breach of article 3 of the European convention on human rights to impose a short prison sentence? Article3 is an unqualified ban on inhuman or degrading treatment or punishment.
Imprisonment now exposes an inmate to a constant regime of almost continuous containment, inducing or exacerbating high levels of mental illness, disorder and violence, abuse of prohibited drugs and so-called legal (ie not yet illegal) highs, bullying, black marketing and, in the case of Muslim prisoners, attempts at indoctrination into radical beliefs, all in an atmosphere of cigarette smoking and managed by overwhelmed prison staff. Prison will always be unpleasant but standards have collapsed. Much cheaper and much more effective reformative sentences are available. A far smaller number than 86,000 men and women need to be kept in prison in order to protect the rest of us from them because they are dangerous. David Tucker(Retired senior crown prosecutor), Thirsk, North Yorkshire 
 Join the debate  email guardian.letters@theguardian.com
 Read more Guardian letters  click here to visit gu.com/letters
