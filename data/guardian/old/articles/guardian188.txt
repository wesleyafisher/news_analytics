__HTTP_STATUS_CODE
200
__TITLE

'We tried nice guys': conservative hardliners stay in a trance for Trump

__AUTHORS

Lauren Gambino in Washington
__TIMESTAMP

Sunday 15 October 2017 07.00EDT

__ARTICLE
Nearly a year ago, conservative Christians gambled on Donald Trump, a thrice-married Manhattan billionaire who bragged about sexual assault, ran casinos and used to support a womans right to abortion. The bet paid off.
This weekend, Trump became the first sitting president to address the Values Voters Summit, a yearly Washington symposium that brings together social conservative leaders and voters seeking to preserve the bedrock values of traditional marriage, religious liberty, sanctity of life and limited government that make our nation strong. He duly touted his administrations progress in returning moral clarity to our view of the world. 
How times have changed, Trump thundered across the cavernous Shoreham Hotel ballroom, lamenting the erosion of conservative values he said were the bedrock of America. But you know what? Now theyre changing back again, just remember that.
The crowd rose. Two years ago, Trump was booed at the same conference. Before the Republican primaries, he finished a distant fifth in the summits straw poll. This year, he rattled off a list of promises to evangelical Christians he said he had kept, including pledges to appoint a conservative justice to the supreme court and to impose a new healthcare rule exempting employers whose religious beliefs conflict with the mandate that they provide coverage for birth control.
I didnt have a schedule, he said, [but] if I did have a schedule, I would say we are substantially ahead of schedule. His administration would stop cold the attacks on Judeo-Christian values, he said.
Trump may be an unlikely favorite of the religious right but last November exit polls said he won 81% of white evangelicals  more than George W Bush, John McCain or Mitt Romney. Michele Bachmann, a former congresswoman from Minnesota and candidate for the Republican presidential nomination, called Trumps victory proof positive of what the Lord did.
He knows he is the president of United States today because evangelical Christians came out and supported him, she said in her address.
Its like a cloud has lifted. When Obama was in, everything was sad. Nothing was good. Now look at the smiling faces
In hallways after Trumps speech, attendees buzzed with excitement.
Its like a cloud has lifted, said Pat Flynn, who came with a group of women from Catholics for Freedom of Religion. When Obama was in, everything was sad. Nothing was good. Now look  look at the smiling faces. Look at people getting jobs again.
She said she had a conversation with a nun earlier who wished Trump would speak  and tweet  less. But Flynn believed that hisgift of gab and wiseguy bravado was exactly why he was elected.
We tried nice guys, she said. We had John McCain. Mitt Romney. They were nice, smiling at everybody, but they couldnt beat out Hillary. Romney, I mean come on. The only thing people remember about him is that he tied a dog to his roof.
Inside the frigid ballroom, where clusters of stars were projected on to the walls and Christian ballads played in breaks between speeches, the mood darkened. Angry speakers lamented political correctness and derided media bias.
Literature handed out in red totes included a flyer for a book, The Hazards Of Homosexuality. The mainstreaming of homosexuality, the flyer said, had created a public health crisis affecting us all. The bag also included a bumper sticker that read: I dont believe the liberal media.
In speeches, abortion was likened to genocide. The liberal resistance to Trump was compared to the Taliban. Journalists in attendance were called creepy little scribblers. Refugees were accused of spreading disease and terror. 
If there were more real men in Hollywood, Harvey Weinstein probably would have had a couple of black eyes
Speakers took turns denouncing NFL players, for kneeling during the national anthem to protest police violence, and Harvey Weinstein, the Hollywood mogul and liberal donor accused by dozens of women of harassment and sexual assault.
If there were more real men in Hollywood, Harvey Weinstein probably would have had a couple of black eyes, said Dana Loesch, a Dallas-based conservative radio host who has starred in controversial videos for the National Rifle Association. Thats what happens in the real world ... when women are mistreated.
Loesch also declared that feminism was dead as a doornail and accused third-wave feminists of trying to replace the patriarchy with a matriarchy. Women, she said, are not a good master. 
Kellyanne Conway, campaign manager turned special counselor to the president, was introduced at the summit as the woman who saved the world. During a question and answer session with Family Research Council president Tony Perkins, she too raised the Weinstein allegations.
The juxtaposition of that, she said, is that I am in a place where women are respected. 
No speaker mentioned accusations against Trump by more than a dozen women of sexual harassment and misconduct, all of which he has denied despite saying in a now infamous 2005 video that he could kiss and grab women without their consent. 
And yet the loudest applause was reserved for attacks not on Democrats and the left but on the Republican establishment, setting the stage for an internecine war that could dramatically affect next years midterm elections. 
Occasionally you come across one, you pull the trigger and its a dud, said Mark Meadows, a North Carolina congressman who chairs the hardline House Freedom Caucus, talking about firing guns. You have two choices: leave that in the chamber, pull the trigger again and again. Or you can eject it and put in another shell. 
I would suggest we have some members who are duds that have been left in the chamber too long and its time that we eject them.
The next morning, Steve Bannon, once Trumps campaign chair, then chief White House strategist, now back at Breitbart News, declared war on the Republican establishment, heralding a populist, nationalist, conservative revolt he said would sweep them from power. 
Nobody can run and hide on this one, these folks are coming for you, Bannon said, to sustained applause. 
Sally Boss, of Hudson, Florida, said she agreed with Bannon 100%.
Donald Trump is one of the few politicians that is doing what he said he is going to do, she said. And hes trying to do more if the Republican establishment would just get behind him.
Well, get behind him or get out of the way.
