__HTTP_STATUS_CODE
200
__TITLE

EU rules out tax on plastic products to reduce waste

__AUTHORS

Fiona Harvey in Malta
__TIMESTAMP

Friday 6 October 2017 09.53EDT

__ARTICLE
The EU has ruled out penalties on single-use plastic products, in favour of raising public awareness of the damage consumer plastics are doing to the worlds oceans.
Frans Timmermans, vice president of the European commission, said a tax would not be sustainable, but that changing the way plastic was produced and used could work. The only sustainable method is to create recyclable plastic and take out microplastics. You cant take out microplastics with a tax. You need to make sure things are reused, and not put in the ocean.
He said the commission was working with manufacturers to help change their products and packaging. Karmenu Vella, environment commissioner, also pledged that the EUs long-awaited plastics strategy would be published by the end of the year.
The European commission cannot raise taxes directly, but can encourage member states to do so, and can impose other penalties, as with the emissions trading scheme to reduce carbon from heavy industry.
Timmermans rejected outright charges and taxes on single-use plastic, and was reluctant to consider legislative measures, but called instead for public information campaigns on the problems plastics cause. It is not that we, through legislation, should force [producers of plastic to change], though if we have to we might, but through public awareness, to urge countries to raise awareness, he said.
Nothing disciplines companies more than consumer practices. We are on the verge of changing consumer habits. I sense a turning point, like that we saw 10 to 15 years ago on climate change, he told journalists at the Our Ocean conference in Malta.
That was what happened with recycling. Who made us recycle? Our kids. I dont think there is one producer of consumer goods that would go against the grain of public awareness.
At present, only about 6% of plastic waste is recycled within the EU. In part, this is because of the many different forms of plastic that are used in consumer goods, and the difficulty of returning them to the kind of versatility that virgin plastics enjoy. But Timmermans said consumers would accept less flashy and less aesthetically pleasing packaging, if they understood it would help remove pollution from the oceans.
Vella added that companies should design plastic products with reuse in mind from the outset: The circular economy is the most effective way to deal with plastics.
He promised that the forthcoming plastics strategy would include design, recycling, biodegradable plastics, single-use plastics and microplastics.
The commission is also to remove single-use plastics, including drinking vessels, from its own offices by the end of this year.
The commission is to devote 550m (490m) to projects that improve the health of the oceans, from marine protection zones and satellite monitoring, to plastic waste disposal. At the conference, more than 6bn was pledged in total by governments, institutions and private sector companies towards efforts to combat overfishing, pollution, plastic waste, ocean acidification and other threats to the marine environment.
This included a pledge of $150m (115m) from a group of NGOs and companies to prevent plastic waste reaching oceans in south-east Asia. Five countries  China, Indonesia, Philippines, Vietnam and Thailand  are responsible for half of all the plastic waste that enters the oceans globally each year. In those countries, on average less than 40% of plastic waste is recycled. The organisations include PepsiCo, Procter & Gamble, 3M, the American Chemistry Council, the World Plastics Council and Oceana.
Insurers are also taking action against illegal fishing, with several of the worlds biggest companies pledging to stop insuring vessels that have been pirate fishing. Illegal fishing costs the world an estimated $10bn to $23bn a year, amounting to about 25m tonnes of fish that are taken from waters against quotas, or in contravention of national fisheries rights and policies. The insurers include Allianz, Axa, Generali, Hanseatic Underwriters, and The Shipowners Club.
However, the environment lawyers ClientEarth said laws against illegal fishing in the EU were being undermined by failures among member states.
Analysing the enforcement system in six of the EUs biggest fishing countries  France, Spain, the Netherlands, Poland, Ireland and the UK  the lawyers found none were properly implementing the anti-piracy regulations of the Common Fisheries Policy, and the level of sanctions against offenders was low.
Elisabeth Druel, lawyer at ClientEarth, said authorities were doing little to combat illegal fishing. Strong and systematic sanctions are needed to deter illegal fishing and pay for the damage done to our marine environment. The fishing industry would have us believe they are heavily inspected and sanctioned, but our research shows that is just not the case.
