__HTTP_STATUS_CODE
200
__TITLE

The Ritual review  lads' weekend turns surreal in lost-in-the-woods Brit horror

__AUTHORS

Peter Bradshaw
__TIMESTAMP

Thursday 12 October 2017 10.30EDT

__ARTICLE
The Ritual is an efficient, well-made if programmatic Brit horror, with good effects and a big monstrous reveal intelligently withheld. It is adapted by Joe Barton from a 2011 bestseller by Adam Nevill, and directed by David Bruckner  who worked on the very interesting and creepy compilation movie V/H/S.
A group of guys go on a hiking weekend in a remote Swedish wood  they are Luke (Rafe Spall), Hutch (Rob James-Collier, who deserves more than to be known only as Mr Barrow from Downton Abbey), Phil (Arsher Ali) and Dom (Sam Troughton). Theyve been mates since uni and going away together is a tradition with them. But one of the gang is missing, having been killed during a robbery in a late-night shop after one of their lads nights out, while Luke cringed invisibly in a separate aisle, leaving his mate to face the robbers alone. So this whole trip is a sort of propitiatory adventure, offered up glumly to their dead friends memory while trying not to talk (or think) about his death being Lukes fault. 
But, as they go off-trail into the supposedly unoccupied forest, horrifying things start happening. The movie implies that these events could be cosmic manifestations of guilt and shame  with loads of surreal convenience-store-related hallucinations in the middle of the forest  and also an opportunity for mysterious, violent redemption. There are obvious hints of The Blair Witch Project and Deliverance, and the film is sometimes like a John Landis black-comedy spoof, only played dead straight, with tiny post-shock gaps where the acrid gags might otherwise go and the comedy restricted to the guys initial banter. Spall is good casting in the lead: miserable, hangdog, humorous and scared, like a handsomer version of Josh Widdicombe. James-Collier is a fierce screen presence: some film-maker needs to find something more for him to do.
