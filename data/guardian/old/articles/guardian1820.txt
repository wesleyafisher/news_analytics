__HTTP_STATUS_CODE
200
__TITLE

Energy firms warned to drop worst-value tariffs or face action

__AUTHORS

Adam Vaughan
__TIMESTAMP

Wednesday 18 October 2017 03.09EDT

__ARTICLE
Big energy companies face a regulatory crackdown after the industry watchdog threatened to impose targets forcing suppliers to shift millions of customers off the markets worst-value tariffs. 
Dermot Nolan, the Ofgem chief executive, said companies had failed to act quickly enough and should move faster to take customers off the standard variable tariffs which about two-thirds of households are on.
British Gas and SSE, the two companies that have the highest percentage of customers on such tariffs, were accused of failing to move users on to a better deal. They have not made progress or initiatives in the way that perhaps some other companies have announced, Nolan told MPs.
He said that while 2m fewer customers were on standard variable tariffs than a year ago, the market was still not working as well as it should. On the prospect of setting targets to mandate the energy companies to move people off such tariffs, he said: We are still likely to do that.
Recent plans by E.ON and ScottishPower to phase out the tariffs were not perfect, he said, but were signs of progress. Centrica, British Gass owner, told the business select committee that it wanted to see the death of the standard variable tariff rather than the implementation of a price cap as planned by the government.
In our view there is an issue with engagement but we dont think the solution is price caps, we think price caps will hinder competition, said Sarwjit Sambhi, the managing director of Centricas home unit, which includes British Gas. Our proposal is to end the evergreen nature of the standard variable tariff.
Sambhi denied that British Gas was subsidising its cheapest fixed deals by overcharging customers on its standard variable tariff. But under questioning he admitted that 70% of its supply businesss 550m profits last year were from standard variable tariffs, despite them accounting for 65% of its customer base.
He was asked five times by Rachel Reeves, the committees Labour chair, whether he would mount a legal challenge against Ofgem if a cap was not backed up by legislation. 
He would not be drawn on the question, and did not rule out a challenge. But Sambhi said that a narrower cap which came into effect in April did not reflect the costs energy suppliers faced, in particular for fitting smart meters in millions of homes. 
Ofgem said last weeks publication of draft legislation for the cap would not be enough to see off such challenges on its own, and the law would need to be passed before a ceiling was imposed. Before setting any final cap Im afraid we would need royal assent to have that degree of certainty, said Nolan. 
Smaller energy companies told the MPs that, contrary to what Centrica argued, a cap was essential for stopping suppliers exploiting customers.
After 19 years of deregulation, nothing has worked, said Stephen Fitzpatrick, the chief executive of OVO, one of the biggest challenger companies. By far and away the simplest way to protect consumers  is to have a well-set, regulatory price cap, he said.
Greg Jackson, the chief executive of small supplier Octopus, said: Weve already heard the usual argument of the big six, essentially blaming consumers for not engaging. The idea that you should have to switch supplier every year, in order to avoid getting literally ripped off, is absurd.
