__HTTP_STATUS_CODE
200
__TITLE

Genius crossword 157

__AUTHORS

Picaroon
__TIMESTAMP

Sunday 3 July 2016 19.01EDT

__ARTICLE
Half the solutions are to be entered in a 10 across manner. Ambiguities about some entries are resolved by the appearance of four names with something in common appearing in the completed grid.
Deadline for entries is 23:59 GMT on Saturday 30 July. You need to register once and then sign in to theguardian.com to enter our online competition for a 100 monthly prize.
Click here to register.
