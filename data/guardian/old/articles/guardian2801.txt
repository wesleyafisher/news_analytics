__HTTP_STATUS_CODE
200
__TITLE

Midwife shortages blamed for home births falling to 15-year low

__AUTHORS

Denis Campbell Health editor
__TIMESTAMP

Monday 16 October 2017 13.51EDT

__ARTICLE
The number of women having a home birth has fallen to a 15-year low as concern rises that some expectant mothers are being denied one because there are too few midwives.
Only one in 50 babies in England and Wales were born at home last year, according to National Office of Statistics data  the lowest number since 2001 . Just 2.1% of the 676,271 babies born were delivered at home.
Childbirth experts claimed the fall is due to midwives being called in to help out in overstretched hospital labour wards, who were meant to be assisting home births while working in community-based services.
Staffing and resource issues mean that expectant mothers arent always offered the opportunity to have a home birth. Women are being failed as they are being denied choices, said Elizabeth Duff, senior policy adviser at the National Childbirth Trust. 
Under National Institute for Health and Clinical Excellence (Nice) guidelines women in England and Wales should be able to choose whether to have their baby in a hospital unit with doctors in charge, a unit staffed only by midwives or at home with a midwife present. Were concerned that some women arent being given the full range of choices, said Duff.
Louise Silverton, the director for midwifery at the Royal College of Midwives, said: We do hear anecdotally that women arent choosing a home birth because they are worried that the service may not be available because of staff shortages. 
Given that maternity services are very much under pressure and have no spare capacity its not at all surprising that there are fewer home births. 
When a woman who has planned to have a home birth rings up to say that shes in labour, she can be told that they dont have a midwife for her. Thats no good for anyone as it means that the woman and her partner are anxious and that all the rapport shes built up with the midwife during the months of antenatal care are lost. Thats a shame. For women that can be very disruptive. 
Childbirth units are under such strain that four in 10 in England had to close temporarily last year and divert women elsewhere. In all, 42 out of 96 hospital trusts which responded to a Labour freedom of information request shut their maternity unit a total of 382 times  70% more than occurred during 2014. 
In the 1960s, when childbirth records began, almost a third of babies were born at home. But that figure has fallen dramatically since, to the extent that only one in 100 arrived that way in the 1980s. 
That figure rose slowly again, reaching 2.9% in 2008. But that fell again to 2.3% in 2015 and then to 2.1% last year. 
Women aged 35 to 39 are the most likely to give birth at home; about 2.9% of mothers that age do so. Those aged under 20 are the least likely; only 1% opt for home births. More women in Wales than in England have a home birth. Rates are much lower in Scotland and Northern Ireland.
The NHS needs to do more to enable women to have a home birth, said Duff. Women who want a home birth do so for a number of reasons including because they have already given birth and now feel confident about a birth at home, they want continuity of care with a midwife they know attending the birth, they dislike being in hospital, or want to avoid medical interventions. Ultimately the decision is theirs and services should be in place to give them what they want.
In 2016 the lowest rate of stillbirths in England and Wales for 34 years was recorded  4.4 out of every 1,000 births  after determined action by the NHS to reduce them. 
There was also a small drop in the number of women having a multiple birth, from 16.1 to 15.9 births per 1,000. Women over 45 were by far the most likely to deliver twins, triplets or quadruplets or a large number of babies. 
This decrease was greatest in women aged 30 and over, particularly those aged 45 and over where the proportion of women having multiple births decreased by 15%, said ONS statistician Nicola Haines. 
Since 1993, women aged 45 and over have consistently had the highest proportion of multiple births, partly due to higher levels of assisted fertility treatments at these ages, Haines said.
