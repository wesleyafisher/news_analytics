__HTTP_STATUS_CODE
200
__TITLE

Power Rangers review  colour-coded superpowers revealed in goofy origins story

__AUTHORS

Mike McCahill
__TIMESTAMP

Tuesday 21 March 2017 19.12EDT

__ARTICLE
You can rationalise and contextualise and say that the Marvel effect means any Lycra-clad saviour with an iota of brand recognition is now apt for revival in some format. Once the lights dim, however, nothing can prepare you for the ontological strangeness of watching a Power Rangers movie in 2017. Especially one that is  forgive me if my voice rises an octave here  not entirely terrible? That is, in fact, basically harmless, if you dont object to feeding your kids pop-cultural leftovers, with odd flickers of charm besides? In an age of hype, some films are bound to benefit from massively reduced expectations; this would be one of them.
Being a 21st century reboot, of course, director Dean Israelites hands are tied by the deadening demands of the origin story, yet this remains one of the goofier ones, chortlingly realised: five small-town kids assuming colour-coded superpowers after trapping themselves beneath a slough of prehistoric alien space rock. If the groups trajectory from detention through training montage to final, city-trashing battle is diagrammatic, Israelite senses its silly enough not to belabour the throwaway plot points generated. Any questions? asks Bryan Cranston, operating behind a Blu-Tack carapace as galactic guardian Zordon. Nah, I think Im good, responds wiseacre Blue Ranger Billy (RJ Cyler). Thats the spirit.
No one is pushing the subvert button too hard: the much-reported gay subtext proves so muted as to make Beauty and the Beast seem like Paris Is Burning. Nevertheless, those leftfield choices Israelite does make (bovine masturbation gags, batty product-placement, Elizabeth Banks vamping as cosplay-ready villain Rita Repulsa) are welcome, and the New Rangers such likable types its a pity they should eventually suit up. We didnt really need any of this: not the repackage, nor more superheroics, nor the closing-credits cover of Snaps The Power. Yet the film achieves a functioning mediocrity we perhaps might have thought beyond this franchise, offering a modicum of diversion in return for the cash disappeared from your wallet.
