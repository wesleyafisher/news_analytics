__HTTP_STATUS_CODE
200
__TITLE

Australia's household power prices rose 63% in past decade, says watchdog

__AUTHORS

Katharine Murphy Political editor
__TIMESTAMP

Sunday 15 October 2017 13.00EDT

__ARTICLE
Residential electricity prices have increased by 63% on top of inflation over the past decade, according to Australias competition watchdog, mainly due to higher network costs, which comprise 48% of a household bill.
An interim snapshot, to be released by the Australian Competition and Consumer Commission on Monday ahead of the expected unveiling of the Turnbull governments new energy policy this week, found there was a severe electricity affordability problem across the national electricity market, and the price increases over the past 10 years are putting Australian businesses and consumers under unacceptable pressure.
It noted that large increases in electricity prices have not been matched by price increases in other areas of the economy, nor in wage growth and it said the burden of higher electricity prices disproportionally affects those segments of society least able to afford it.
The report pointed to the 63% increase over the past 10 years, and it broke down the components of a residential power bill of $1,524.
It said 48% of that bill was network costs, 22% wholesale costs, 16% retail and other costs, and 8% retail margins. Green schemes, which have been the focus of most public attention in recent times, made up only 7%.
In an effort to identify the underlying factors driving increases in power prices, the ACCC pointed predominantly to a lack of competition, with the power-generation market highly concentrated, and with substantial levels of vertical integration between generation and retailing.
It said the big power generators  AGL, Origin and EnergyAustralia  continued to hold large retail market shares in most regions, and control in excess of 60% of generation capacity in New South Wales, South Australia and Victoria making it difficult for smaller retailers to compete.
It said the current lack of competition in the market raises real concerns, because the national electricity market was predicated on competitive markets.
It said electricity network operators had been able to overinvest in poles and wires as a result of the suboptimal network regulation framework, and that behaviour would continue to burden consumers for decades to come.
While pointing the finger squarely at anti-competitive market behaviour by power companies as the principle driver of higher power prices, the ACCC also noted there was uncertainty in Australias electricity sector, with a trend of insufficient investment.
The competition watchdog noted that ageing coal assets leaving the system were not being replaced by sufficient new capacity.
It said 5,000 MW of generation capacity had left the national electricity market in the last five years. An additional 2,000 MW had come on stream, with 92% of that renewable generation  leaving a net decline in capacity of more than 3000 MW.
Prior to 201617, all regions in the national electricity market had a surplus of electricity generation capacity, but the supply/demand balance had shifted because of reduction in generation capacity, with old coal plants leaving the system, and an uptick in demand.
Australias energy sector and many business groups argue that policy uncertainty caused by a decade of toxic political debate on climate and energy policy is one of the factors driving up power prices, because the lack of settled policy makes it difficult to make future investment decisions.
While the ACCC pointed the finger back at the anti-competitive market behaviour of generators and retailers currently making the complaints about the destructive impact of policy uncertainty, it does accept some of the uncertainty argument.
It noted the Turnbull governments as-yet unresolved position on the clean energy target recommended by the chief scientist Alan Finkel. The ACCC also pointed to the fact the Australian Energy Market Operator had reported power generation proposals comprising 23,000 MW which suggests additional capacity is ready to be developed given the right conditions.
The report found that Queenslanders would be paying the most for their electricity this year, followed by South Australians and people living in NSW, while Victorians would have the lowest electricity bills.
On the power price impact of green schemes, the ACCC noted that subsidies aimed at achieving sustainability objectives have also increased costs and created cross-subsidies.
It noted that the costs of policies like the feed-in tariffs adopted by some states for solar photovoltaics have been passed through to all electricity users.
Some measures to improve environmental sustainability have been overly generous and poorly targeted, with outcomes that appear inequitable, the report said.
The ACCC chairman, Rod Sims, said ahead of the release of Mondays report there was currently to much ill-informed commentary about the drivers of Australias electricity affordability problem.
Sims warned there was no prospect of governments being able to land on a fix unless there was a clear understanding of the underlying issues.
The treasurer, Scott Morrison, said a final report from the ACCC, expected in coming months, would help to further drive action from the Turnbull government in order to secure more affordable and reliable energy for Australian households and businesses.
Ensuring we keep the lights on and that the prices Australian families pay for energy are affordable is central to the Turnbull governments approach to energy policy.
The government is expected to put its new energy policy to cabinet and the Coalition party room this week, after spending the past few weeks playing down expectations it will adopt the clean energy target modelled in the Finkel review.
The clean energy target has been opposed from the outset by Tony Abbott and a small group of conservatives within the Coalition party room, including the Nationals MP George Christensen, who has said several times he wont vote for it.
The governments policy is expected to include comprehensively overhauling the national electricity market rules to ensure more dispatachable energy is made available to the market to make the system more reliable, and it is also understood to include measures to lower greenhouse gas emissions.
Labor warned the government on Sunday not to ditch Finkels clean energy target. The shadow climate change minister, Mark Butler, said the loss of the clean energy target meant there was little prospect of bipartisan agreement on energy policy between the major parties.
Butler declared if the prime minister dumped the clean energy target, then he wont get the support of the Labor party.
