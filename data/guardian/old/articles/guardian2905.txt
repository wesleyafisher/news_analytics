__HTTP_STATUS_CODE
200
__TITLE

Castlefords Zak Hardaker out of Grand Final because of breach of club rules

__AUTHORS

Aaron Bower
__TIMESTAMP

Thursday 5 October 2017 08.11EDT

__ARTICLE
Zak Hardakers career has been blighted by controversy again after the England international was dropped for the Grand Final against Leeds for what Castleford described as a breach of club rules.
Forty-eight hours after finishing second behind his Castleford team-mate Luke Gale in the man of steel awards, Hardaker was omitted from the Super League leaders squad for their first appearance in the final following an incident unrelated to rugby.
Castleford issued a brief statement that said: Zak Hardaker is unavailable for selection due to a breach of club rules. The club will make no further comment at this point as full attention is focused on Saturdays Grand Final. The Rugby Football League also refused to comment and it is understood it will not do so before the Grand Final.
The 25-year-old has been a hugely influential figure for Castleford since joining from Leeds at the beginning of the season for 150,000. Yet while he is undoubtedly one of the finest talents of his generation, he has been involved in incidents that have cast a cloud over his career.
Hardaker was stood down from the England squad during the 2013 World Cup for an unspecified breach of team discipline. The following year he was suspended for five matches and fined 300 after being charged with making a homophobic insult during Leedss game against Warrington.
In March 2015, Hardaker and his former Leeds team-mate Elliot Minchella were detained and questioned by police over an alleged assault. He missed Leeds game against Warrington as a result and, while he did not face criminal charges, he agreed to pay 200 in compensation and write an apology to the victim.
While Hardakers omission from the Grand Final is already clear, it could yet have ramifications on his international future. This latest incident is likely to cost him a place in Englands World Cup squad due to be announced on Monday.
