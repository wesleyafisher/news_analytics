__HTTP_STATUS_CODE
200
__TITLE

Eco road trip: Los Angeles to Las Vegas by Tesla

__AUTHORS

Stephanie Theobald
__TIMESTAMP

Sunday 19 February 2017 05.00EST

__ARTICLE
Stephanie (my namesake, as it happens) has a wicked grin on her face as she flips open the boot of the car. Were about to embark on a road trip from Los Angeles to Las Vegas but I suspect this is going to be very different from the one chronicled by Hunter S Thompson. In 1971s Fear and Loathing in Las Vegas, the trunk of his Great Red Shark resembled a mobile police narcotics lab filled with blotter acid, cocaine and quarts of tequila. Our black Tesla Model X is stocked with gluten-free, non-GMO protein bars and organic energy drinks.
Tesloop, a pioneering long-distance shuttle service launched in 2015, is like a better class of Uber, using the Lamborghini of eco-cars: the Tesla. Tesloops founder, 17-year-old Haydn Sonnad, had the idea of leasing the cars and charging from $59 for the Los Angeles to Las Vegas route (an airfare can be triple that) and $35 for the new Los Angeles to Palm Springs route.
Sonnad estimates that the LA to Vegas trip will save up to 70kg of carbon emissions, but the main buzz for customers is that the Tesla is the brainchild of LA visionary Elon Musk, who is also behind SpaceX, the company that aims to make the Earth-Mars route a viable option.
Ive heard that a Teslas drives itself, and as we get in the Model X (more like a falcon than a shark, thanks to De Lorean-style doors that open upwards) a figure from the Tesloop office appears on an iPhone on the dashboard. She talks about the radars, sonars and front-facing cameras that allow the car to accelerate, brake and steer for itself. Basically, she says, your pilot doesnt have to use their hands or feet while driving on the highway today!
Stephanie has three passengers: Whitney, an accountant in her 30s; Julius, an events organiser off to Sin City to celebrate his birthday with friends; and me. The Model X can carry six passengers (as opposed to three in the Model S sedan), so we have plenty of room. Were all excited. I feel like we could drive under water in this thing, exclaims Whitney.
I feel like were going to blast off! Julius adds. 
Sonnad sums it up: Tesloop is a new kind of road trip. Its clean and green and by far the safest way to travel by car.
At a Tesla Motors shareholders meeting in 2015 (he purchased one share for $210 to get in) he stood up and pointed out to Musk that Teslas werent being used over long distances. He was planning to use them for two return journeys to Palm Springs each day and one return trip to Las Vegas. It was a bold publicity blast for his fledgling company and his father, Rahul, helped raise much of the investment. Tesloop started with one leased Tesla Model S, which normally does the Las Vegas drive, and now also has three model X SUVs in its fleet. New routes are planned in Texas.
This month Tesloop announced that bookings had increased to the point at which the company was covering all operational expenses. It has also signed a technology agreement with Goodyear: the tyre giant is curious about the performance of electric cars over huge distances. (Tesloops first 2015 Tesla Model S has now done more than 250,000 miles.)
Sonnad, too, is optimistic  even given a new president who doesnt seem too down with the eco world. People in LA have always been willing to try new stuff, and now its really paying off, he says with a smile.
I had worried that this trip might be a bit worthy, but 10 minutes into our 278-mile drive, our pilot, a pro inline skater, hemp activist and musician, is talking about a recent ayahuasca ceremony in Joshua Tree (ayahuasca is a hallucinogenic plant medicine from the Amazon that induces visions). Whitney, off to Vegas for a weekend of shows and all you can eat crab leg buffets, nods matter-of-factly and asks if Stephanie has tried the shamanic frog poison cleanse. (Stephanie has.) Julius talks about his last Tesloop trip to Vegas, when he shared the car with Mariah Careys DJ, Suss One. Like Julius, Suss hates flying. Julius is not bothered by the less rocknroll aspects of travelling to Vegas in a Tesla. You dont want to peak too early, he reasons, sucking on a free carton of coconut water. 
There is fast wifi and headphones if you dont feel like chatting. Stephanie occasionally puts on favourite podcasts: one about the bitcoin currency, and another on the antiviral properties of mushrooms. Every so often a bell sounds, signifying that Stephanie has put the car in self-drive mode. She tells us that, strictly speaking, the Tesla is only semi-autonomous. Musk reckons it will be another couple of years before youll be able to go to sleep at the wheel.
Still, it all feels very modern, and Tesloop is at the forefront of LA transport ideas. Only a couple of years ago youd have been laughed out of town if youd suggested holidaying here without hiring a car. But even Angelenos are realising that something has to give. Carpooling has become as normal as juice cleansing thanks to Uber, and in the November elections, Los Angeles County voters approved Measure M, to provide nearly $42bn for new rail and rapid transit options as well as bike share expansion.
Last May, a new metro line, the Expo, linked downtown LA to Santa Monica, and new bike sharing schemes in Venice, Santa Monica and West Hollywood are proving popular. But as Sonnad has realised, cars are not going to go away. We just need to make more of them electric and let ourselves be driven more.
Including a 40-minute stop in Barstow to charge the battery at a Tesla station, we arrive in Vegas in just under five hours. Were certainly not bucking and skidding with a bottle of rum in one hand and jamming the horn to drown out the music, as Thompson was, but Julius seems to be high on organic chia seeds and coconut water.
Stephanie drops him and me off at the new W hotel. Hes meeting his buddies here  theyve hired the penthouse designed by Lenny Kravitz and Philippe Starck. Julius chuckles: I heard they have, like, mirrors above the bed.
You can bring in the new but you dont necessarily have to chuck out all of the old.
The journey was provided by Tesloop.com, from Los Angeles to Las Vegas from $59pp one-way. Accommodation was provided by the W Las Vegas (doubles from $150 room only).
