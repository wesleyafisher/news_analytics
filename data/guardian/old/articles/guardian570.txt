__HTTP_STATUS_CODE
200
__TITLE

The Young'uns: 'We were out busking and a bystander said, "Shut up singing or I'll glass you"'

__AUTHORS

Dave Simpson
__TIMESTAMP

Wednesday 11 October 2017 01.00EDT

__ARTICLE
In 2003, three teenagers started frequenting the Sun Inn in Stockton-on-Tees, County Durham. In the back room, they found the Stockton Folk Club in full voice. We didnt even know there was a folk club, says David Eagle, the trios baritone, sitting in the self-same room today. When someone started singing, I thought, What the heck? But we stayed to listen.
To their amazement, they found that traditional songs about working peoples lives  sung by much older people  spoke to them more than pop. Wed stumbled across this rich tradition that wed no idea existed, says Eagles bandmate Michael Hughes. Unaccompanied songs about ordinary people being belted out in Teesside accents.
The trio returned so often that people started asking, When are the young uns gonna give us a song? So eventually we did. They liked the name and, 14 years on, those unlikely lads have twice been voted best group at the Radio 2 Folk awards, having gathered fans around the world for their rousing mix of youthful vim, (mostly) unaccompanied singing, rapidfire banter and songs that tackle contemporary politics.
When we won the first award, we expected a massed, Who? Eagle says. But there was this huge round of applause. We were like, My God. They know who we are. We werent just nominated as a drunken bet. 
We were crap for years. If YouTube had been prevalent back then, we wouldn't be sitting here now
As teen folk newbies, Eagle, Hughes and Sean Cooney had staggered from that pub realising that singing unaccompanied meant they could start a band without needing any instruments. Back then, their experience of singing had been limited to the terraces of Middlesbrough FC. Same tradition though, says Eagle. Someone starts singing and others take it up  and if they dont, you look a right nit.
He laughs. We were crap for years. If YouTube had been prevalent back then, and people had got to hear us, we wouldnt be sitting here now. Or rather, we would be sitting here, but not being interviewed by the Guardian. Wed be crying into our beer.
Weve got recordings of us basically screaming, Hughes grins, shaking his head. And because we were drunk
Wed do harmonies, interrupts Eagle, and wed all go for the same note. Then it would be, Im having that one, you bastard. Once, during an attempt at busking, one bystander said, Shut up singing or Im going to glass you.
But their devotion paid off and the gigs started piling up. Banter became a part of the act after they saw local folk group the Wilson Family, whom Eagle describes as five big, beer-swilling brothers from Teesside. We thought, Well banter as much as them.
At a London gig a couple of years back, Eagle, who is blind, walked on stage, collided with the microphone and shot back: Im not blind really. Im just doing it for effect. Revealing now that, on another occasion, he fell of the stage, he says: I love it when something happens. I mean, obviously, it hurts. But how can you do a bad gig with an opening like that, when people are laughing before the first song?
As integral to the Younguns as their humour is their politics. One of the first songs they sang  shouted, really  was Sydney Carters Sing John Ball, an anthem about the 1381 Peasants Revolt in England, while Billy Braggs Between the Wars has long been a staple of their live set. But things took on an extra dimension once Cooney started writing songs himself.
People would take the mick, 'Who sings about Hartlepool?' But a good story's a good story
As a history student, Cooney had loved Bob Dylan, but discovering the wider folk canon led to a feeling that everything had been said. Then things started happening, he says. Some of my first songs were about the Iraq war and I realised that [late Teesside folk giant] Vin Garbutt sang about modern life. So I started writing about fishermen Id meet in pubs in Hartlepool. People would take the mick, Who sings about Hartlepool? But a good storys a good story.
Cooney has written nine of the 10 tracks on the new album, Strangers, the exception being Maggie Hollands award-winning 1999 lament A Place Called England. The Hartlepool Pedlar tells of Jewish refugee Michael Marks, who fled the pogroms in the late 19th century and went on to found Marks & Spencer, having started out selling fruit and vegetables in the north east. Not many people in Hartlepool know that, he says. With the rise of anti-immigrant feeling these last 18 months, I just love telling the story.
Cooney wrote Ghafoors Bus after reading a newspaper story about Stockton grandfather Ghafoor Hussain, who in 2015 converted a bus into a mobile kitchen and went off to feed refugees in Europe. And he penned the tremendous Cable Street  about 1930s local Johnny Longstaff, who went on the Jarrow March and fought Oswald Mosleys fascists  after Longstaffs son Duncan, now in his 70s, approached them at a gig to tell them his fathers incredible life story.
Two years ago, the singer-songwriter was deeply moved by a Guardian article about Nazim Mahmood, whod committed suicide because his family couldnt accept that he was gay. How do you begin to write a song about that? the Youngun asks. But he did, and sent Be the Man to Mahmoods grieving partner, Matthew Ogston.
He emailed to say he listened to it on the bus with tears running down his face, Cooney says softly. When we told him wed perform it for the first time at Glastonbury, he posted on Facebook, Nazs dream was always to go to Glastonbury, but we never made it. Now these incredibly kind strangers have written a song about him, Naz is going to Glastonbury.
The once beery teenagers see their role today as uplifting the community and galvanising people. But theyre still having some good laughs. Hughes recounts a gig in Scotland just before the independence vote. David Cameron had urged the English to encourage Scottish acquaintances to vote to remain, so the band began: Were here at the behest of the prime minister to make friends in Scotland. So what better way than to sing songs from the capital country of Britain? England! 
The joke fell flat, people left in droves, but the three decided to make a night of it anyway, yelling: Left side, take the chorus! at the few remaining punters and even shouting for their own encore. The sound man came up afterwards crying with laughter, Hughes sniggers. He said, Lads, thats the best gig Ive ever done.
