__HTTP_STATUS_CODE
200
__TITLE

Patagonia joins forces with activists to protect public lands from Trump

__AUTHORS

Tom McCarthy
__TIMESTAMP

Saturday 26 August 2017 07.00EDT

__ARTICLE
Environmental activists, Native American groups and a coalition of outdoor retailers have vowed to redouble their efforts to protect public lands, after the US interior secretary, Ryan Zinke, recommended on Thursday that Donald Trump change the boundaries of a handful of national monuments.
Advocates fear that Zinkes recommendation, which has not been revealed beyond its most basic outline, could mean the largest reversal of federal monument status in history and the first alteration of any national monument boundary since 1964.
Secretary Zinkes recommendation is an insult to tribes, said Carleton Bowekaty, co-chairman of the Inter-Tribal Coalition, which asked Barack Obama to create the Bears Ears monument in Utah in 2015, citing increasing thefts and vandalism at more than 100,000 native cultural sites in the area.
Millions of petitioners have joined an urgently assembled advocacy effort to dissuade the Trump administration from moving against the monuments. On Friday, the outdoor retailer Patagonia, which spearheaded the industry initiative, said the group would continue its efforts.
Were willing to take every step necessary, including legal action, to defend these public lands, said Hans Cole, director of environmental activism at Patagonia. For us, all of these awful scenarios present something that is unacceptable to us and to a great majority of the American public.
Brian Sybert, director of the nonprofit Conservation Lands Foundation, said Zinkes review made a mockery of the decades of work that local communities have invested to protect these places for future generations.
Just this week, an independent analysis of the public comments on Zinkes review showed that more that 99% of Americans want our national monuments to remain unchanged, Sybert said in a statement.
Zinke seemed to discount the near-unanimity of the public response. 
Comments received were overwhelmingly in favor of maintaining existing monuments and demonstrated a well-orchestrated national campaign organized by multiple organizations, he said in a summary report.
Zinke has called for the downsizing of monuments including Bears Ears and Grand Staircase-Escalante in Utah and Cascade-Siskiyou in Oregon, the Washington Post reported. National monuments are protected sites of historical, cultural or scientific interest managed by a patchwork of federal agencies.
Opponents of the 1.3m-acre Bears Ears monument have said it is too big and warned that an increase in tourists attracted by the federal status could damage the site. Land conservationists have rebutted such claims, arguing that federal dollars are essential to managing tourism and preserving the cultural sites, including policing the theft of artifacts.
Energy interests are also in play, with environmentalists warning that Bears Ears could see expanded uranium mining and oil and gas drilling. While the area is not currently a focal point for energy extraction statewide, the president of the oil and gas industry group Western Energy Alliance told a trade publication in April that there certainly is industry appetite for development there, or else companies wouldnt have leases in the area.
Utah Republicans have sided with the energy interests. When Obama established Bears Ears in December 2016, the state legislatures Republican majority issued a wild statement comparing the move to the unilateral tyranny exercised by the King of England against the American colonies two and a half centuries ago.
Trump, who in April ordered a review of all large presidential land designations made since 1996, voiced an eagerness to revise federal land holdings. 
It sounds like the largest real estate deal I could ever be involved in, a local report quoted him as telling the Utah senator Orrin Hatch.
Phil Lyman, a county commissioner who lives inside Bears Ears and who went to jail last year for organizing an illegal ATV ride through a protected canyon, told the Guardian the monument designation was an inappropriate federal intervention in Utah. He also accused Patagonia and its partners of corporate cynicism.
Its posturing, Lyman said. When you have Patagonia  theyre not in a conservation business. Theyre in an industrialized tourism business.
Lyman was echoing critics of activist retailers who have pointed out that the $887bn outdoor industry does well when people go outside. A Wall Street Journal opinion page headline went so far as to conjure a rock-climbing industrial complex it said was bullying energy companies off public lands.
But Cole, of Patagonia, said: Our 30-year history of working on these issues points to something else. The company has donated nearly $90m to environmental groups since 1985, according to its figures. It closed its operations on election day so employees could vote, started an editorial page flame war over the monument issue and led the exodus of a huge annual outdoor retailer convention from Salt Lake City in protest of Utah officials opposition to the monument.
A second San Juan county commissioner contacted by the Guardian, Rebecca Benally, a Democrat of Native American heritage, said she opposed the monument but declined further comment. In April 2016, she released a statement expressing concern that the federal government would mismanage native cultural sites, which are ostensibly protected under state and federal law. 
Bowekaty, of the Inter-Tribal Coalition, countered in a statement: Zinkes recommendation today would leave tens of thousands of sacred sites without protection ... We have no choice but to continue the fight for our ancestors and for contemporary uses of the lands by our Tribal members.
A Patagonia spokeswoman, Corley Kenna, said: Once these places are developed, you cant go back. It truly is about protecting for future generations, as much as anything else. Youll never get it exactly as it was. And these places are pristine.
