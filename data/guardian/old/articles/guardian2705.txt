__HTTP_STATUS_CODE
200
__TITLE

They use money to promote Christianity: Nepal's battle for souls

__AUTHORS

Pete Pattisson in Manahari
__TIMESTAMP

Tuesday 15 August 2017 02.00EDT

__ARTICLE

Ram Maya Sunar had two miscarriages. Then she had a daughter, who died of pneumonia when she was one. My second child died from tuberculosis at just six months. Im still haunted by it, Sunar says, sitting outside her concrete block hut in the village of Thakaldanda, in southern Nepals Makwanpur district.
When she became pregnant again, Sunar sought out an unlikely remedy. Rather than call the local shamans, as she had done before, she joined a church.
The 35-year-old, a Hindu, converted to Christianity and gave birth to a healthy daughter, and later a son. They are my gifts from God, she says.
It is a story repeated across Thakaldanda, a village of about 50 households. Theres a church here, and another church there, and another over there, says Kajiman BK, who converted to Christianity after surviving an unknown disease.
It is not just survival stories that Sunar and Kajiman have in common. They are also Dalits (formerly untouchables), members of the lowest Hindu caste, who continue to suffer discrimination and abuse.
It is Dalits, and other marginalised groups, who are leading a surge in the growth of Christianity in Nepal. More than a million people in Nepal identify as Christians, and the country has one of the fastest growing Christian populations in the world. The Federation of National Christian Nepal says 65% of Christians are Dalits.
Nepal, formerly the only Hindu state in the world, became a secular republic following the 10-year civil war, and the fall of the monarchy in 2008. Hinduism remains the countrys dominant religion, but the transition to secularism opened up space for other religions, even though proselytising is outlawed in the constitution.
The growth of Christianity is driven by motivations that appear to have more to do with health, discrimination and poverty than pure belief. And behind the conversions, critics say, is the presence of well-funded foreign missionaries.
Off the main road of Manahari, a town in Makwanpur, dozens of small churches have sprung up. Local Christian pastors estimate the town has two mosques, five Buddhist gompas, 10 temples and 35 churches.
Many of the churches are led by Chepangs, one of Nepals most disadvantaged indigenous groups.
After the earthquakes, Christian missions in Chepang areas became more and more active and the number of churches is dramatically increasing, says Diana Riboli, an Italian anthropologist who has spent years researching Chepang shamanism in the area around Manahari. Missionaries emphasise the healing aspect of Christianity, trying to put an end to the traditional rituals and charismatic power of shamans.
Riboli says conversions are creating an alarming situation in Chepang communities, where internal conflicts are becoming more frequent.
Purna Bahadur Praja, a Chepang shaman, says many Chepangs are turning to Christianity just for the money. They are greedy  after the earthquake, they got Bibles, rice, clothes, blankets, money to build churches. Pastors were getting motorbikes  They spend the whole time emailing foreigners to ask for money, he says.
Mahibal Praja, a Chepang pastor, vehemently denies this. In a small shop off the main road from where he runs a cable TV business, dusty monitors, coils of wire and soldering irons lie scattered across the shop. Look at this. If I was making lots of money would I be working here? he asks.
His church is a small building made of concrete blocks and corrugated iron. On Saturday morning, when Christians worship in Nepal, it is packed with more than 50 worshippers, sweating in the humid air.
Mahibal gives an energetic sermon that seems to confirm Purna Bahadurs accusation that there is some profiteering. Many people are making money from Christianity, but they are not real Christians, he shouts into the microphone, before closing the service in prayer. This church needs a new building. God, please provide a nice building. 
In the nearby village of Ramantar, Dalit Christians already have that prayer answered; they worship in an airy two-storey building constructed with the support of American missionaries. The pastor, Jit Bahadur Sunar, says there are three other churches in the village. Ten years ago there were just four or five Christian families here, but every day the number is increasing, he says. Virtually all the Dalits here are Christian.
Caste discrimination is the main reason Dalits are turning to Christianity, he says. The higher castes in the village used to treat dogs better than us  and generally they still do. [Upper caste] priests refuse to do marriage or death rituals for Dalits, but among Christians there is no discrimination  we are all equal.
Hari Gopal Rimal, a local Hindu priest, accepts this. Untouchability is a weakness in Hinduism  these things need to be changed, he says.
But in the battle for souls, Rimal feels Hinduism is losing out to a more powerful force. Different foreign organisations are providing funds when Christians are ill. And after the earthquake they provided help only to Christians. Bibles came in sacks of rice. Bibles came with everything  They are using money to promote Christianity and we have to compete with them.
Purna Bahadur Praja agrees. Im unhappy because they are forgetting their culture and rituals in the name of money, which they are getting from outside.
If it continues like this, there will be no more Hindus in this town. 
