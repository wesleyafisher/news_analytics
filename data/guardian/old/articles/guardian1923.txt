__HTTP_STATUS_CODE
200
__TITLE

Police investigate alleged assault of 15-year-old cricket umpire

__AUTHORS
Australian Associated Press
__TIMESTAMP

Sunday 15 October 2017 22.40EDT

__ARTICLE
The alleged assault of a teenage cricket umpire at a Melbourne under-11s match has sparked a police investigation and a political warning that such behaviour will not be tolerated.
Police were called to a sports reserve at Sunshine North about 7pm on Friday after reports a 15-year-old boy had been assaulted during an under-11s game between Sunshine and Caroline Springs.
The boy was taken to hospital and a number of witnesses were interviewed by police. 
Victorias sports minister, John Eren, said he was aware of the incident.
The perpetrator of the alleged incident should be brought to justice, Eren told reporters on Monday. When are people going to learn this sort of behaviour will not be tolerated?
The codes of all different sports are taking action on this very important issue.
The Caroline Springs club president, Steve Collins, who had been called to the scene, appealed for restraint in response to the clash, which allegedly involved an adult on the sidelines.
We do ask that any concerned members of the club direct any questions to myself or the committee and not look to social media to comment, Collins posted on the clubs Facebook page on Saturday. We do not want to drop to the low standards of others.
We would also like to thank the parents that were present at the incident for remaining calm and ensuring the safety of our junior players.
Police said the matter continued to be investigated but no charges had been laid.
The Sunshine cricket club and the Western Region Junior Cricket Association have been contacted for comment.
