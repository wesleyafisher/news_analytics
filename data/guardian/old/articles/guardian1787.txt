__HTTP_STATUS_CODE
200
__TITLE

Malcolm Turnbull says he expects more complaints about NBN

__AUTHORS

Anne Davies
__TIMESTAMP

Tuesday 17 October 2017 20.30EDT

__ARTICLE
 The prime minister, Malcolm Turnbull, has conceded complaints about the national broadband network will increase as more people are connected, with the latest figures showing complaints about internet services have more than doubled in a year.
New figures from the telecommunications industry ombudsman show complaints about the the internet now exceed those about mobile and fixed line services.
Total NBN complaints, including those about delayed connections, soared from 10,487 to 27,195, according to the annual report released on Wednesday.
The picture the complaints show is we are frustrated when we cannot rely on technology to stay connected, to be informed and to do business, the ombudsman, Judi Jones, said. Sharing high-quality videos immediately, holding an online meeting or watching Netflix on the way home is now the norm and part of our daily routine.
Complaints about services delivered over the national broadband network more than doubled and, while this is somewhat to be expected given the accelerating rollout, the increase is a cause for concern.
16,221 complaints were recorded about faults in services delivered over the national broadband network. This is 6.7 fault complaints per 1,000 premises activated.
 11,224 complaints were recorded about connection delays, a rate of 8.3 per 1,000 premises activated.
One of the problems has been service providers offering cheap internet packages that do not meet customers needs, leading to slow speeds and services that drop out. 
 But the figures also show there is still an ongoing problem with the rollout as well. Customers often find themselves caught between NBN Co, which is undertaking the infrastructure rollout, and their retail service provider with whom they have the contractual relationship. Sometimes third-party contractors are involved in the actual installation.
Some of the smaller providers saw massive jumps in their complaint rates. Southern Phone, for example, received 2,068 complaints, a year on year increase of 266.7%.
iiNet also saw its complaints jump by 79%.
Not surprisingly the three major carriers, Telstra, Optus and Vodafone, received the most complaints but these were up by a third of more year on year.
Telstra received 76,650 complaints, a year on year increase of 43.5%. Optus received 28,766 complaints, a year on year increase of 31.2%. Vodafone received 10,684 complaints, a year on year increase of 37.5%.
It was residential customers who bore the brunt of the problems with the NBN with 88% of complaints coming from this customer group.
Turnbull acknowledged it was never good enough to have complaints and said he was working with the minister and management to improve the experience for customers.
It is a bit like television. If youve got hardly any viewers, you wont get a lot of complaints, he told the Nine Network. [Customers] are rolling on and around 30,000 to 40,000 a week. Clearly you are going to get more complaints.
Complaints about mobiles also continued to increase.
We are very concerned about the significant, across the board, increase in complaints for landline, mobile and internet services, the Australian Communications Consumer Action Networks deputy chief executive, Narelle Clark, said.
 This reverses the previous downward trend in complaint levels. We are therefore calling on all providers to lift their game and act to immediately improve customer service and the consumer experience.
 Australian Associated Press contributed to this report 
