__HTTP_STATUS_CODE
200
__TITLE

Airbus takes majority stake in Bombardier jet project

__AUTHORS

Nicola Slawson and 
Angela Monaghan
__TIMESTAMP

Tuesday 17 October 2017 04.54EDT

__ARTICLE
European aircraft giant Airbus is taking a majority stake in Bombardiers controversial C-Series jet programme, potentially safeguarding 1,000 jobs in Belfast.
The French-based plane maker is acquiring 50.1% of the programme, the future of which was left in doubt after Canadian company Bombardier was hit by a 300% import levy by the United States. The huge tariff followed a complaint from Boeing that the company had dumped its C-Series jets at absurdly low prices.
Unite, the UKs largest union, welcomed the new partnership and said the manufacture of wings for the C-Series would remain in Belfast where 1,000 people are employed on the programme.
We have received assurances that this will mean that employment associated with the manufacture of C-Series wings will remain in Belfast, said Davy Thompson, regional officer of Unite.
Unite will continue our efforts to ensure the withdrawal of the US tariffs on the C-Series but this is a welcome development - one that gives breathing space to the C-Series itself and which we anticipate should safeguard the future of C Series production jobs in Belfast for the foreseeable future.
In a sign of the extent of the challenges facing Bombardier, Airbus did not pay anything for the majority stake. The European aircraft manufacturer will use its global firepower and supply chain to provide procurement, sales and marketing, and customer support to the jet programme.
The business secretary, Greg Clark, said the partnership was a very big step forward.
Not only has Airbus committed to Belfast being the home of the wing manufacturer for the C-series, but they are pointing to the possibility of expanding the output and the order book, he said.
The deal will leave Bombardier with about 31% of the C-Series programme, while financier Investissement Qubec will own about 19%. 
Boeing complained to US authorities in April that aid received by Bombardier from the Canadian and UK governments amounted to illegal subsidies, allowing it to sell its C-Series jets to the US airline Delta for below cost price. Delta has placed a $5.6bn (4.2bn) order for up to 125 of the new jets, with delivery due to begin next year.
The US Department of Commerce found in favour of Boeing, and slapped a 300% tariff on C-series jets imported into the US. 
Airbus is hoping to solve the problem posed by the tariffs by assembling the C-Series jets destined for US customers at its own factory in Alabama. 
Ross Murdoch, national officer at the GMB union, welcomed the deal between Airbus and Bombardier but warned the devil will be in the detail, with potential new problems created by Airbuss plans to assemble the C-Series in the US.
This deal is liable to further scrutiny from the US administration that may see it as an attempt to dodge their trade tariffs, Murdoch said. GMB hopes both Bombardier and Airbus have taken cast iron legal advice to ensure they dont get rid on one legal challenge only to open themselves up to another.
Bombardier employs a total of 4,000 people in Belfast, including 1,000 on the C-Series programme. It is unclear what the future holds for those workers in Northern Ireland not employed on the jet project.
While we welcome this announcement in regard to the C Series, there are continued challenges associated with employment on other Bombardier contracts in Belfast, said Unites Thompson. We will be engaging with management and our membership to safeguard all workers interests in the coming period. 
The proposed tariffs have put huge pressure on Bombardier, leaving workers in Northern Ireland fearful for their jobs. Both the UK and Canadian governments have previously threatened to hit back at Boeing by denying the company defence work.
Airbuss chief executive, Tom Enders, said in a statement: This is a win-win for everybody. The C Series, with its state-of-the-art design and great economics, is a great fit with our existing single-aisle aircraft family and rapidly extends our product offering into a fast growing market sector.
I have no doubt that our partnership with Bombardier will boost sales and the value of this programme tremendously. Not only will this partnership secure the C Series and its industrial operations in Canada, the UK and China, but we also bring new jobs to the US. Airbus will benefit from strengthening its product portfolio in the high-volume single-aisle market, offering superior value to our airline customers worldwide.
Alain Bellemare, president and chief executive officer of Bombardier, said it was very pleased to welcome Airbus to the C Series programme. Airbus is the perfect partner for us, Quebec and Canada, he said. This partnership should more than double the value of the C Series programme and ensures our remarkable game-changing aircraft realises its full potential.
The move was also welcomed by Dominique Anglade, Quebecs deputy prime minister, who said: The arrival of Airbus as a strategic partner today will ensure the sustainability and growth of the C Series programme, as well as consolidating the entire Quebec aerospace cluster.
