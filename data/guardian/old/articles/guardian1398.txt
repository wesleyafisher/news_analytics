__HTTP_STATUS_CODE
200
__TITLE

Azed crossword 2,350

__AUTHORS

__TIMESTAMP

Saturday 24 June 2017 19.01EDT

__ARTICLE
Special instructions: The Chambers Dictionary (2014) is recommended. 
Prize Rules: 25 in book tokens for the first three correct solutions opened. Solutions postmarked no later than Saturday to AZED No. 2,350, The Observer, 90 York Way, London N19GU
