__HTTP_STATUS_CODE
200
__TITLE

Post-Brexit invisible border is impossible, says Irish report

__AUTHORS

Lisa O'Carroll Brexit correspondent
__TIMESTAMP

Sunday 8 October 2017 10.49EDT

__ARTICLE
An invisible border between Northern Ireland and the Irish Republic after Brexit is impossible and hopes for such an arrangement are naive, a leaked report from Irelands equivalent of HMRC says.
In the most authoritative piece of research so far conducted on the challenges faced in Ireland, Irelands Office of the Revenue Commissioners (ORC) concludes that customs posts will be needed, with significant facilities on border roads.
While some form of common travel area may exist post-Brexit, a completely open border is not possible from a customs perspective, says the report, leaked to the national broadcaster RT.
 It is probably somewhat naive to believe that a new and entirely unique arrangement can be negotiated and applied to the EU/UK land frontier. 
The report suggests local frontier points will have to be agreed with the UK, and says it is not inconceivable there will be eight crossing points, including a permanent customs post on the M1 between Dublin and Belfast.
The report will make sober reading for Irelands taoiseach, Leo Varadkar, who came to office this summer warning that Ireland would not design a border for the Brexiteers.
It makes plain that customs checks on the south side of the border will be unavoidable under EU law.
The leak, which comes as the fifth round of Brexit talks start in Brussels, could change the balance in negotiations and put pressure on EU leaders to help Ireland find a solution. 
Up to now the EUs efforts have been focused on putting pressure on the UK to come up with a means of achieving the seamless and frictionless border that Theresa May and Varadkars predecessor Enda Kenny had promised after the Brexit vote.
The findings will increase anxiety among border communities that have flourished since the Good Friday agreement and that have been campaigning for the current open border to continue after Brexit.
Some have pointed out that the only way to achieve this is for Northern Ireland to remain in the customs union, but this special status has been rejected by the Democratic Unionist party leader, Arlene Foster.
We believe in the single market  the single market of the UK. All this talk of a border in the Irish Sea is a complete non-starter, she told a meeting at the Conservative party conference last week.
"We believe in single market. Single mkt of U.K. All this talk of border in the Irish Sea is a complete non-starter" - Arlene Foster pic.twitter.com/KLJdcqg5ff
The ORC report says 13,000 commercial vehicles cross the Irish border with freight ranging from meat and dairy to Guinness, which travels from Dublin to Belfast for bottling and back to Dublin for export to the UK. 
Once negotiations are completed  the UK will become a third country for customs purposes and the associated formalities will become unavoidable, it says. While this will affect all member states, the effect will be more profound on Ireland as the only EU country to have a land border with the UK.
It says customs checks will have a negative impact on trade flows and delay the release of goods.
As all of these goods will be subject to the customs import procedure in the post-Brexit era, the administrative and fiscal burden on the traders involved cannot be underestimated, the report says.
Despite claims that electronic checks could prove a solution, the ORC says this is unrealistic as some goods, such as animals and animal feed, will have to be checked under EU law.
Automatic number plate recognition (ANPR) will not be enough, it says. Regardless of any efficiency arising from an ANPR system, the inevitability of certain consignments being routed other than green and goods or documents having to be examined would still require investment in suitable facilities at all designated crossing points.
 Customs cannot permit the release into free circulation of goods or animals not already cleared by the relevant co-located border inspection post. 
Cargo will have to be fully inspected post-Brexit, and goods will not be released until all customs formalities have been completed, the report says. 
Goods will be deemed to be in temporary storage once they arrive for customs checks. In order to end temporary storage, the goods must be placed under a customs procedure or re-exported, the report says.
The findings show the scale of the challenge of reintroducing customs checks not seen since the single market was established in 1993.
Specific challenges facing customs officers will be the standards of goods. For example, Britain will be free post-Brexit to import beef impregnated with growth hormones from the US. The EU would not allow this meat to cross the border into Irish supermarkets.
While cross-border traffic is significant, the greater trade for both Northern Ireland and Ireland is conducted across the Irish Sea. The report warns of significant challenges at Dublin port, which handles most of the goods from Northern Ireland and Ireland exported to the UK and freight bound for the continent.
Existing physical infrastructure and traffic streaming are likely to be stretched, if not overwhelmed, by the increased demand for customs controls, says the report.
It says that while temporary importation is uncommon at the moment, there is is likely to be major growth. Given our geographical proximity to the UK, the temporary movement of goods between the two states is very significant.
Construction equipment going across the border or tools would also be subject to customs checks. 
The ORC said the paper was an internal working paper considering possible administrative implications post Brexit. It said it predates the many developments and papers since Article 50 was triggered and other papers have been written since.
In August the ORCs chair, Niall Cody, told an Irish parliamentary committee there were 12,000 businesses exporting to the UK and more than 60,000 importers.
Most of these businesses are unfamiliar with customs procedures, he said, adding that the ORC was advising them to assume that customs procedures of some form would apply after Brexit.
