__HTTP_STATUS_CODE
200
__TITLE

US navy Seal killed in clash with al-Shabaab militants in Somalia

__AUTHORS
Reuters
__TIMESTAMP

Friday 5 May 2017 12.33EDT

__ARTICLE
A US navy Seal has been killed and two troops wounded in a clash with al-Shabaab militants in Somalia, US officials said on Friday, in what appeared to be the first US combat death in the country since the 1993 Black Hawk Down disaster.
The White House has granted the US military broader authority to carry out strikes in Somalia against al-Qaida-linked al-Shabaab, the latest sign Donald Trump is increasing US military engagement in the region.
US Africa Command said a service member was killed by small arms fire on Thursday while US forces were advising and assisting a Somali national army operation in Barii, about 40 miles west of Mogadishu. It did not give further details on the identities of any of the three casualties.
 A US official, speaking on condition of anonymity, said the man killed was a navy Seal. It was not immediately clear whether the wounded US forces were also from the elite military unit.
 The US troops were hunting an al-Shabaab commander near the Shabelle river alongside Somali special forces, a Mogadishu-based security source told Reuters. There were no Somali casualties.
 The raid took place at Darusalam village, where Abdirahman Mohamed Warsame, known as Mahad Karate, was believed to be hiding, said another security source. Warsame is the deputy leader of al-Shabaab and US authorities have offered $5m for information that brings him to justice.
 Warsame played a key role in the Amniyat, the wing of al-Shabaab responsible for assassinations and the April 2, 2015 attack on Garissa University College that resulted in 150 deaths, said a statement on the Rewards for Justice website, which is run by the US state department.
 It was not clear yet if Warsame was the target of the raid, the security source said.
 A spokesman for al-Shabaab, which wants to overthrow the weak western-backed Somali government and impose its own strict brand of Islamic law, said US troops had attacked one of its bases.
 Last night at 1am US forces attacked us in Darusalam village, in lower Shabelle region. After fighting, the US forces ran away, said Sheikh Abdiasis Abu Musab, al-Shabaabs military operation spokesman.
 They left ammunition and weapons on the scene and blood stains. First they had a helicopter which landed some kilometres away from our base, to which they walked. We inflicted heavy casualties  some forces died and others were wounded but we do not have the exact figure.
 Residents of Darusalam village said the gunfire lasted for about 10 minutes. Last night, helicopters hovered over us and we were scared. Then late at night, there was fighting, a resident, Mohamed Hassan, told Reuters.
Africa Command said US personnel were working alongside members of the Somali military.
US forces are assisting partner forces to counter al-Shabaab in Somalia to degrade the al-Qaeda affiliates ability to recruit, train and plot external terror attacks throughout the region and in America, it said in a statement.
 Although US forces are not engaged in direct action [in Somalia], advise and assist missions are inherently dangerous, added Robyn Mack, a spokeswoman at the US militarys Africa Command.
 Somalia has been shattered by civil war that began when clan-based warlords overthrew a dictator in 1991 and then turned on each other.
 A US military intervention there in 1993 ended after the Black Hawk Down incident, when 18 US soldiers were killed after Somali militia shot down two US helicopters in the capital of Mogadishu.
 African Union peacekeeping forces have been in Somalia since 2007, gradually expanding from the airport to secure the capital and eventually pushing into other major towns. US military advisers have also secretly operated in the country since approximately 2007.
