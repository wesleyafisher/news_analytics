__HTTP_STATUS_CODE
200
__TITLE

NBA 2017-18 predictions: our writers forecast the winners and Trump baiters

__AUTHORS

Les Carpenter, 
DJ Gallo, 
Hunter Felt and 
Bryan Armen Graham
__TIMESTAMP

Tuesday 17 October 2017 07.39EDT

__ARTICLE
Seeing how Oklahoma City will fit Paul George and Carmelo Anthony with Russell Westbrook. The Thunder have essentially rented George and Anthony for a season. Both will want the ball and while Westbrooks reputation is as a player who craves control he also knows he must share to win  and challenge Golden State. LC
The regular season is the least exciting part of the NBA calendar  with the offseason and playoffs being tied for most exciting, of course. But since the October to April portion of the season has to happen, the best part will be watching how the Sixers look with all of their young players in place. Joel Embiid, if healthy, is instantly a Top 10 player. But if he gets hurt again, the regular season officially becomes the worst part of the league. DG
How the Western Conference plays out after this offseason. Itll be intriguing to see Chris Paul and James Harden together for the Houston Rockets. It will be interesting to see if Russell Westbrook, Paul George and Carmelo Anthony combine to form a Big Three (Big Two & A Half?) for the Oklahoma City Thunder. The Warriors will be the best team in the West, but the battle for No2 will be as fierce as any in recent memory. HF
The Sixers bold bet on failure demanded patience from their fans but has paid off with a Big Three of Joel Embiid, Ben Simmons and Markelle Fultz, who will finally take the court together. Whether they can stay healthy and intact could determine just how far Philly can go in a wide-open Eastern Conference. BAG
Remember, San Antonio were dominating the Warriors when Kawhi Leonard went down in Game 1 of the conference finals last year. Golden State are beatable, especially against a team that can challenge the Warriors shooters defensively. LC 
Injuries can stop the Warriors, but no one wants to see the Warriors get injured, even Cavaliers and Thunder fans. (Well, maybe.) It doesnt seem possible, but Golden State enter this season in even better shape than last. This time last year we didnt know how Durant would fit with all the other talent, but now we know that he fits quite well. And that he is capable of raising his game in the playoffs. The only way they lose is if LeBron and his new cast of stars straight up outplay the Warriors stars in their inevitable Finals matchup. DG
The crazy part is that even with the Western Conference being as competitive at is, its still going to be a battle for second place for the teams that arent the Warriors. Its hard to see anyone coming out of the West to block the Warriors barring an injury to Durant or Curry. That leaves it up to the Cleveland Cavaliers, who probably took a step back during the offseason. Basically, were looking at LeBron James having the best season of his career. Do I think that will happen? Read on. (Yes, Ive learned to write teasers during the off-season.) HF
Injuries, I guess, but Golden State managed to win 14 in a row without Durant last year before closing the show with wins in 20 of their final 22. Its the Warriors and everybody else. BAG
The Los Angeles Clippers. The West is loaded again and five or six teams will be fighting to separate from the middle of the playoff pack. The Clippers are intriguing, having lost Chris Paul but might have more chemistry and a more-balanced offense with the additions of Danilo Gallinari and Patrick Beverley. They might be the team who faces Golden State in the conference finals. LC
With all of the big names finding new teams in the offseason, the Nuggets signing Paul Millsap didnt register much. But adding the solid veteran to a nice young core of Nikola Jokic, Jamal Murray and Gary Harris should be enough to let the Nuggets fly under the radar all the way to the fifth or sixth seed. DG
I dont really expect there to be a true Cinderella team this season. The Western Conference is too stacked for an unexpected team to push through and the Eastern Conference is too weak. That said, theres a chance that the Utah Jazz could end up standing their ground despite losing Gordon Hayward and George Hill. Itll take a breakout year from Rudy Gobert and the piecing together of a workable offense, but this is a defense that could throw a wrench even in the best teams plans. HF
If Joel Embiid plays 65 games without a minutes restriction, the Sixers will challenge for a top seed in the East. BAG
Draymond Green. The Warriors star forward is a good choice for any takedown. But already he has asked: Still wondering how this guy is running our country. Its only a matter of time before he says more, especially since Golden State wont be visiting the White House this year. LC
While Colin Kaepernick gets all the credit for launching this era of athlete activism, it really started a few months prior to his kneeling when LeBron James, Carmelo Anthony, Dwayne Wade and Chris Paul spoke at the start of the 2016 ESPYs. So Im picking the NBA as a whole giving Trump the biggest takedown just by being itself night after night. The NBA is a league full of strong, outspoken players and its backed by a diverse fanbase that will only support the league more if the players fall into the presidents crosshairs. Any attacks by Trump will just show hes powerless against the NBA. DG
Itll come from LeBron James and it wont have anything to do with basketball. Trump will somehow cross the line further than he ever has. Maybe he goes on Twitter to brag about how much better he is than a pediatric cancer fund? To this, LeBron James simply asks the question that the vast majority of the world is thinking: what is wrong with you, man? It becomes the most RTd tweet in history. HF
It may have already come this week when Spurs coach Gregg Popovich, long regarded as the NBAs most fearless political voice, called the president a soulless coward in a memorable broadside. But its early. BAG
John Wall. Many of the leagues best individual players have added big-name sidekicks this year  which means the numbers for Westbrook and James Harden will be down. Wall, the Wizards point guard, had a tremendous season last year and is just coming into his prime. Washington are the sleeper Finals contender in the East. LC
Russell Westbrook wont put up quite the same numbers with Carmelo Anthony and Paul George  nor will he have to now that he has some help. Kevin Durant and LeBron James candidacies will be hurt by all the star power around them, same as James Harden due to Chris Paul and vice versa. So Ill pick John Wall, who should earn plenty of votes for one-manning the Wizards into the upper half of Eastern Conference teams. DG
LeBron James. This is going to sound out there considering how good hes been so far, but Im betting on LeBron as having the best year of his career. Think about it: the decline years are coming, this will probably be his last year in Cleveland, he is as motivated as ever with last years loss to the Warriors and Kyrie Irvings betrayal. Also Durant and Curry will split the votes, but mostly its the LeBron James On A Rampage script. HF
Its hard to remember an MVP field with this depth: LeBron James, Kevin Durant, Russell Westbrook, James Harden and Giannis Antetokounmpo each have a reasonable shot. But it says here San Antonio point forward Kahwi Leonard takes the final leap forward for the NBAs top individual honor. BAG
No coaches will be fired during the season. This is a bold prediction, indeed, because NBA coaches are always getting fired. But this offseason has been especially unstable with so many key players switching teams. Owners and general managers will be more patient and wait until after the season to make their coaching changes. LC
The Spurs finish in the bottom half of the West. While the Rockets, Thunder and Timberwolves all made huge upgrades to their rosters, the Spurs added  Rudy Gay? All due respect to Gregg Popovichs coaching abilities and the San Antonio system, but at some point talent matters against the top teams. But if theres an upside of the Spurs losing more, its more angry Popovich tirades against the president. DG
The Boston Celtics will falter early on, stumbling below .500 for the first half of the season. Expect their new players to struggle to play together as a team before stabilizing right when the did we get the Celtics wrong? thinkpieces start to emerge. Theyll have the best record in the second half and then go on to win exactly one game against the Cavaliers in the postseason yet again. HF
The Nets had the NBAs worst record last year but an intriguing mix of young talent including DAngelo Russell, Caris LeVert, Allen Crabbe and Rondae Hollis-Jefferson will propel Brooklyn into the playoffs. BAG
Ben Simmons. Believe it or not, last years top rookie is a rookie again since he missed the entire season through injury. A year of building himself physically while experiencing the NBA life should make Simmons a veteran with a rookie designation this season. Finally we will see the versatility and scoring ability he promised before last season. LC
Ben Simmons. The No1 overall pick of 2016 missed all of last season to injury, but has a clean bill of health now. Hell fill the stat sheet for the Sixers. As for 2017 rookies, lets go with Dennis Smith Jr of the Mavericks. He put up big numbers in summer league and has the starting point guard job from the get go. As for impressing with the amount of media coverage, the obvious choice is Lonzo Ball. DG 
Oh Lord help us: its going to be Lonzo Ball isnt it? Hes going to make flashy, game-winning plays, appear on multiple Sports Illustrated covers and hes going to keep the Los Angeles Lakers on SportsCenter despite not being that good. LaVar Ball is going to be all over the news and hes going to spend all of his time predicting that LeBron is going to join his son in the offseason and (deep sigh) he may end up being right again. HF
The Ben Simmons and Markelle Fultz show will be appointment viewing, but the high-flying athleticism of the Mavericks Dennis Smith Jr will be the talk of the league. BAG
Warriors, Thunder, Spurs, Rockets, Clippers, Timberwolves, Pelicans, Jazz. LC
Warriors, Thunder, Rockets, Timberwolves, Spurs, Nuggets, Trail Blazers, Grizzlies. DG
Warriors, Rockets, Thunder, Jazz, Spurs, Trail Blazers, Clippers, Nuggets. HF
Warriors, Rockets, Thunder, Spurs, Timberwolves, Jazz, Nuggets, Clippers. BAG
Celtics, Cavaliers, Wizards, Raptors, Bucks, Heat, Hornets, Sixers. LC 
Cavaliers, Celtics, Wizards, Sixers, Raptors, Heat, Bucks, Pistons. DG
Cavaliers, Wizards, Celtics, Raptors, Bucks, Pistons, Heat, Hornets. HF
Cavaliers, Wizards, Celtics, Hornets, Sixers, Raptors, Bucks, Nets. BAG
Cavaliers over Wizards, 4-2. LC
Cavaliers over Celtics, 4-0. DG
Cavaliers over Celtics, 4-1. HF
Cavaliers over Wizards, 4-3. BAG
Warriors over Thunder, 4-1. LC
Warriors over Thunder, 4-1. DG
Warriors over Rockets, 4-2. HF
Warriors over Thunder, 4-0. BAG
Once again, we get the Warriors and Cavs and once again Golden State wins. The Warriors have too many weapons for anyone to challenge their dominance. Even the remade Cavaliers fall short offensively. Golden State 4-2. LC
The Cavaliers are better than they were last year, assuming Isaiah Thomas is 100% healthy for the playoffs. Derrick Rose, Jose Calderon, Jeff Green, Dwyane Wade, Jae Crowder, Thomas  all added to the remaining core of LeBron, Kevin Love, Tristan Thompson and JR Smith. Will I be surprised if the Warriors still win it all? Not at all. But dont be shocked if this much deeper Cavs teams wins in seven. Thats my pick. DG
Cleveland Cavaliers 4-3 Golden State Warriors. How the heck does this possibly happen? LeBron James caps off the best year of his career with a ridiculous Finals performance. Isaiah Thomas has the best series of his career after being outplayed by Kyrie Irving in a brief but surprisingly competitive Eastern Conference Finals. Kevin Durant and Steph Curry suffer the on-court after-effects of a whose team is this drama that flairs up in the Western Conference Finals, James hands Cleveland one more championship before The Decision 3.0. HF
Warriors in six. The core four of Durant, Curry, Thompson and Green make Golden State as unbeatable an enterprise as weve seen in the NBA in some time  and that was before the shrewd off-season additions of Nick Young, Jordan Bell and Omri Casspi. The Cavs, even without Kyrie Irving, will be better equipped this time around, but it wont be enough. BAG
