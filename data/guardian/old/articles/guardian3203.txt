__HTTP_STATUS_CODE
200
__TITLE

Hermes couriers rebel over 20 days non-stop Christmas workload

__AUTHORS

Jamie Doward
__TIMESTAMP

Saturday 14 October 2017 19.04EDT

__ARTICLE
Couriers working for Hermes, which delivers for major retailers including Amazon, Asos and Next, claim they face pressure to work 20 days without a break in the run-up to Christmas.
Unions say this would be illegal under the working time directive were the drivers not deemed to be self-employed. Hermes rejects the claim, insisting that the couriers can choose when they work. It says that last year  when similar concerns were raised  nearly two-thirds of its couriers chose not to work on all 20 days, proving that they could opt out.
However, this year couriers have been alarmed by a letter from Conor Ormsby, the firms head of courier services, asking them to ensure that a substitute or cover is available to deliver on our normal service days if you are unable to provide service during the peak period. The firm says it has 4,500 couriers who provide normal cover and has drafted in an extra 6,000 relief couriers who will be in place over the Christmas period.
But many couriers are sceptical and fear they will have to work 20 days consecutively if and when cover cannot be found  or risk losing their contracts.
Now, in one of the first signs of workers in the gig economy rebelling against employers working practices, the GMB union has written a letter on their behalf telling Hermes they are not prepared to work such long schedules.
The letter states: I write to confirm that I will not be working 20 days non-stop for Hermes over the upcoming peak period. Under the Health and Safety at Work Act 1974 (HSWA), I have a personal duty to ensure that I take care of my own health and safety.
Last week, a parliamentary inquiry was told that one Hermes driver had his contract cancelled after being unable to work as he was in hospital because his wife had given birth prematurely.
The GMB is bringing a legal case against Hermes that will be heard next year. The union says that by classifying its couriers as self-employed, Hermes avoids giving them basic rights such as holiday pay and the national living wage. The reality is that Hermes are more focused on their own profits than considering the health and safety of their own drivers and the general public, said Maria Ludkin, the GMBs legal director.
Many of Hermes 15,000 couriers have expressed support for the company and say they enjoy working the peak delivery season because it means they earn more. However, a courier who spoke to the Observer said he expected the amount of parcels he would deliver each day in the peak period  which begins on 26 November  to double from around 100 to 200. He said some couriers might expect to do considerably more than this following the online discount days, Black Friday and Cyber Monday.
We run a six-day round, but because of Black Friday and Cyber Monday, Hermes say they cant cope in their depots so they need to open on Sundays, he said. They dont want to use Sunday couriers, they want the everyday couriers doing it, so it means we have to work the middle Sunday. We say its dangerous, they say get cover. Some have cover but the vast majority dont.
Hermes director of legal affairs, Hugo Martin, told MPs on the House of Commons Business, Energy and Industrial Strategy Committe last week that its couriers earn a minimum of 8.50 an hour and an average of 12.20  or 10.60 once expenses such as fuel are taken into account.
However, this was disputed by the courier, who requested anonymity. Im on about 6.50 an hour and last month, if you average it over the month, Ive dropped to about 5.80 an hour.In a statement, Hermes said: It is entirely unfounded and irresponsible to suggest Hermes would require couriers to put themselves or the public at risk. No courier is expected to work longer hours, more days or handle bigger volumes  and there is absolutely no penalty for choosing not to do so.
