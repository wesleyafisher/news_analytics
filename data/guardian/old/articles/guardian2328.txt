__HTTP_STATUS_CODE
200
__TITLE

In defence of sugary drinks: five fancy cocktails that don't work without sugar

__AUTHORS

Chad Parkhill
__TIMESTAMP

Wednesday 9 August 2017 02.30EDT

__ARTICLE
In an 1897 interview with the New York Herald, an anonymous proprietor of a fashionable drinking place let slip a few gems about the subject of sugar in drinks.
Sweet drinks, quoth he, were only for young fellows from the farm, with their rosy cheeks and sound stomachs; of all of his city friends, I know not more than half a dozen who can stand drinking sweet things.
For this bar owner, the reason was simple: The more sugar a man takes into his stomach the less he can stand of liquors ... People are beginning to realize that their stomachs are not of cast iron. They want everything dry, the drier the better.
The above exchange  as reported in David Wondrichs history of the cocktail, Imbibe!  shows that the presence of sugar in cocktails has long been the subject of controversy. While contemporary drinkers are more likely to shy from it using the language of calories and sugar addiction, the same logic applies in this day: sweet drinks are seen as the tipple of choice for rubes, and dry ones for sophisticates. So when we belly up to the bar to order a round of drinks, it comes with a request: Not too sweet, thanks.
The current preference for dryness in drinks is perhaps a natural response to what was, until relatively recently, the sorry state of cocktail bartending practised around the world. 
Throughout the 1970s, 80s, and most of the 90s, the trend was for garish drinks with bad names, each loaded with sugary juices, liqueurs and mixers: think of the Japanese slipper, the fluffy duck, the sex on the beach, or the Alabama slammer. It makes sense that, in a ruthlessly dialectical movement of taste, drinkers cutting their teeth during the great flourishing of the craft cocktail movement would differentiate themselves from their benighted forebears by insisting on drinks with more challenging flavours  drier and more bitter concoctions.
In the world of wine, too, the current preference is for the ultra-dry. Champagne house Ayala had an instant hit on its hands after launching a cuve labelled zro-dosage  ie without the small dosage of grape sugar usually added to brut champagnes  in 2005, and other houses soon followed suit with zro-dosage or brut nature cuves of their own. (For Ayala, this was a little case of history repeating itself: the house first made waves in 1865 when it introduced an extra-dry champagne for the British market bottled with a mere 22g per litre of sugar.)
Similarly, most Australian riesling producers vinify their wines bone-dry, preferring to let the acidity of the grapes screech rather than tame it with a bit of the grapes natural sugar. Certain experienced wine drinkers disdain sweet, lightly fizzy moscato, dismissing it (with no small amount of gender-based condescension) as a drink for young women still learning about wines  which causes some problems for the producers of such appellations as Bugey-Cerdon, whose lightly sweet sparkling wines find themselves orphaned on wine lists despite some serious love from in-the-know sommeliers.
This preference for dryness above all else has become orthodoxy of its own  and one with damaging results. Sugar performs a few key roles in a drink  its not just there to add sweetness but also to balance against acidity, to add a richer mouth feel, and to help make flavours latent in the drink pop out. Cut the sugar from a mixed drink and it wont merely be less sweet but less flavoursome and more Lenten. 
The five cocktails below each make a good argument for the virtues of sweetness in drinks  and they prove that sweet drinks arent necessarily the sole province of young fellows from the farm with rosy cheeks and cast-iron stomachs.
As befits the preferred tipple of Reggie Vanderbilt (of the New York Vanderbilt clan), the stinger is something of an icon of American high society. This after-dinner drink has the advantage, thanks to the minty freshness of the white crme de menthe, of working equally well as a late-afternoon bracer or even as a kind of aperitif. The sweetness of the white crme de menthe here is more than balanced out by the hefty dose of brandy.
67.5ml brandy22.5ml white crme de menthe
Build ingredients in a shaker. Add ice and shake to chill. Strain into a chilled cocktail coupe. Leave ungarnished.
A quick word here about technique and ingredients: despite the current bartenders orthodoxy that drinks containing only clear ingredients should be stirred, the stinger has always called for shaking, which gives it a little extra chill and dilution. If you cant stand the thought of shaking, give it a nice long stir. 
The brandy, meanwhile, should be a team player and on the leaner side  think something along the lines of a nice young cognac rather than an exuberantly rich and round brandy de Jerez. The crme de menthe should be white rather than green, and a high-quality one to boot  if you can get your hands on them, try Giffards Menthe-Pastille, Tempus Fugits crme de menthe glaciale, or the French classic Get 31.
Pair with a book: Stingers are one of the preferred drinks of Harry Rabbit Angstrom in John Updikes Rabbit is Rich. 
This elegant little cocktail from Rob Krueger of Brooklyns Extra Fancy is a spiritual descendant of the infamous corpse reviver No 2. Like all of the corpse revivers progeny, remembering the recipe is a doddle  its simply equal parts of each of the four ingredients. 
The sweetness here comes from both the Lillet Blanc  a lightly-sweet, lightly-bitter fortified wine from Frances Bordeaux region  and the grapefruit liqueur. The end result is a touch sweeter than the corpse reviver, but the zippy acidity of the limes and the fresh, grassy flavours of the ubrwka keep it from becoming cloying.
 What a difference warm weather makes! Toast to spring with Baby You're Driving--vodka, Combier, Lillet & fresh lime  #springhassprung #patioseason #latenightmenu
A post shared by Extra Fancy (@extra_fancy) on Apr 15, 2017 at 11:23am PDT
22.5 ml ubrwka (or other bison grass vodka)
22.5ml pink grapefruit liqueur22.5ml Lillet Blanc22.5ml lime juice
Build ingredients in a shaker. Add ice and shake to chill. Strain into a chilled cocktail coupe. Garnish with a sprig of mint and a twist of orange peel.
Pair it with a film: This cocktails exuberant sense of fun and quirky ingredients make it a natural pairing for Edgar Wrights action flick Baby Driver. The name helps, too.
Its hard to get past this drinks name  an ugly artifact of its 2002 creation, when retro-sexism was ironically cool and every drink served up in a coupe was called a martini of some kind. But if you can bring yourself to drink something so called, youll discover a surprisingly sophisticated sweet drink balanced by the acidity of lime and fresh passionfruit. 
Its a testament to the quality of this cocktail that, despite its un-woke and mixologically inappropriate name, it has made a small comeback in recent years.
2 fresh passionfruit60ml vanilla vodka (preferably home-made)15ml passionfruit liqueur15ml vanilla syrup (preferably home-made)15ml lime juice60ml ros champagne (optional)
To make vanilla syrup and vanilla vodka, infuse split vanilla pods in bottles of vodka and simple syrup until your desired level of vanilla flavour has come through. Remove vanilla pods and keep both vodka and syrup refrigerated.
Chop the passionfruit in half. Scoop the pulp from three of the four halves, reserving the last half intact for garnish. Build passionfruit pulp, vodka, passionfruit liqueur, vanilla syrup and lime juice in a cocktail shaker. Add ice and shake to chill. Strain into a large chilled cocktail coupe. Garnish by floating the reserved passionfruit half, cut side up, in the drink. Serve with the ros champagne in a shot glass on the side.
Pair with a film: What else would you watch while clutching a porn star martini than Paul Thomas Andersons gaudily excessive tale of the adult film industry, Boogie Nights?
The early encounters between Japanese culture and American cocktail mixology were hardly fruitful. The first cocktail supposedly invented on Japanese soil, the bamboo, doesnt contain a single Japanese ingredient  and, as it turns out, was probably invented elsewhere.
Similarly, the Japanese cocktail  one of the few drinks likely invented by Jerry Thomas, the self-styled Jupiter Olympus of the bar  doesnt contain anything remotely Japanese, and was created in the good old US of A. (As for the name: it was supposedly invented in 1860 to honour the first Japanese legation to the United States.)
The sweetener that gives this drink its character, orgeat, is a French almond syrup perfumed with a dash of orange blossom water. You can purchase more than a few good brands online, or if you have access to large quantities of cheap almonds, you can make your own at home. Just avoid the cheap stuff, which will make your cocktail taste of bad wedding cake icing.
60ml cognac or other high-quality brandy15ml orgeat3 dashes Angostura or other aromatic bitters
Build ingredients in a mixing glass. Add ice and stir until chilled. Strain into a chilled cocktail coupe or a rocks glass filled with ice. Garnish with a twist of lemon peel.
Pair with an album: Given that the drink was inspired by Japanese arrivals in New York City, and that it has a sweet and softly aromatic flavour, it makes a natural pairing with the music made by NYC-based Japanese musician Kazu Makino in her band Blonde Redhead, particularly the album 23.
Given the near-ubiquity of the Aperol spritz in contemporary drinking culture, its hard to imagine that there was once a time when drinkers might need to be introduced to its namesake, aperitivo bitters. Yet in many parts of the world Aperol is a recent arrival  indeed, it only landed in the US in 2006. Back then, US drinking culture was largely unfamiliar with the Italian aperitivo ritual that goes hand-in-hand with the spritz, so Aperol needed to be introduced via a more traditional style of cocktail. 
Enter Audrey Saunderss Intro to Aperol cocktail, which did exactly that. The drink is in many ways the victim of its own success  the number of cocktail drinkers yet to sample Aperol dwindles by the day  but its worth revisiting this sweet, lightly bitter sipper to remind yourself that Aperol is more than a one-trick pony.
60ml Aperol30ml gin22.5ml lemon juice7.5ml simple syrup2 dashes Angostura bitters
Build ingredients in a shaker. Add ice and shake to chill. Strain into a chilled cocktail coupe. Garnish with a twist of orange peel.
Pair with a song: The summery good vibes and Italian heritage of this cocktail mesh perfectly with Erlend yes ode to his new Sicilian home, La Prima Estate.
