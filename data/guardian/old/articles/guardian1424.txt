__HTTP_STATUS_CODE
200
__TITLE

Cyber cold war is just getting started, claims Hillary Clinton

__AUTHORS

Patrick Wintour Diplomatic editor
__TIMESTAMP

Sunday 15 October 2017 19.25EDT

__ARTICLE
Hillary Clinton embarked on a speaking tour of Britain with a message that the Brexit referendum was won on the basis of a big lie and warning that Vladimir Putin has been conducting a cyber cold war against the west.
She urged more women to enter politics and praised those who spoke up about the Hollywood movie mogul and Democratic donor Harvey Weinstein, saying his reported behaviour was disgusting.
But Clinton observed that Donald Trump had admitted to sexual attack and had a long history of misogyny, yet his supporters were comfortable that he was now in the Oval Office.
Some Democrats had urged her to be silent after her defeat to Trump but she was not going to go away, said Clinton. She vowed to play her part in an attempt to win back Democratic seats in the forthcoming midterm elections. She admitted she just collapsed with real grief and disappointment after her election defeat.
Clinton, who is touring the country to promote What Happened  her memoir reflecting on the election defeat, told the BBCs Andrew Marr: Looking at the Brexit vote now, it was a precursor to some extent of what happened to us in the United States.
She decried the amount of fabricated information voters were given: You know, the big lie is a very potent tool and weve somewhat kept it at bay in western democracies, partly because of the freedom of the press. There has to be some basic level of fact and evidence in all parts of our society.
She urged Britain to be cautious about striking a trade deal with Trump, saying he did not believe in free trade.
In other comments during the Cheltenham literary festival, she accused the Kremlin of waging an information war throughout the 2016 US election process. The tactics were a clear and present danger to western democracy and it is right out of the Putin playbook, she said. 
We know Russian agents used Facebook, YouTube, Twitter and even Pinterest to place targeted attack ads and negative stories intended not to hurt just me but to fan the flames of division in our society. Russians posed as Americans pretending to be LGBT and gun rights activists, even Muslims, saying things they knew would cause distress. 
She said some of the basics of the Russian interference in the 2016 election had been known, but we were in the dark about the weaponisation of social media. She cited new research from Columbia University showing that attack ads on Facebook paid for in roubles were seen by 10 million people in crucial swing states and had been shared up to 340m times.
Clinton said the matter of whether Trumps campaign cooperated with Russian interference was a subject for congressional investigation. But she called for anyone found guilty of such cooperation with Moscow to be subject to civil and criminal law. The Russians are still playing on anything and everything they can to turn Americans against each other, she said. 
In addition to hacking our elections, they are hacking our discourse and our unity. We are in the middle of a global struggle between liberal democracy and a rising tide of illiberalism and authoritarianism. This is a kind of new cold war and it is just getting starting.
The Russian campaign was leading to nationalism in Europe, democratic backsliding in Hungary and Poland, and a loss of faith in democracy, she said.
Clinton said she had thought the US electorate would have valued calm and composure as traits in their prospective president, and admitted she may not have been suited to reality TV-style campaigning. I am an authentically reserved person, she said. Maybe that is all out the window now and maybe it does not matter how real you actually are, if you are not dancing on the table insulting people, using every rude thing you say to act as if you are tearing down political correctness.
She said Barack Obama was very measured in his use of words and yet she was branded inauthentic when she tried the same approach. Women are always judged more harshly. Clinton said that the way Trump played on bigotry, prejudice and paranoia was a very important motivator in his election success.
Clinton dismissed reports that her husband, Bill Clinton, had criticised What Happened: I am going to start calling things that are lies lies. I am not going to be tiptoeing around about it. There was a small city industry churning out lies about me and my family, Clinton said.
When challenged about whether she had been wrong to denigrate women who had claimed her husband had made improper sexual advances, Clinton said the issue had been litigated, but the pattern of behaviour by powerful men needed to be addressed.
Speaking later at the Southbank Centre as part of the London literary festival, Clinton said both Democrats and Republicans were scratching their heads thinking about how to interpose a system amid concerns that Trump could  in a fit of pique  gain access to the nuclear codes and order a nuclear attack. She said she had never expressed such a fear about a US presidents access to nuclear weapons.
She said one proposal was for the president to be required to seek the support of the defence secretary or secretary of state before launching an attack. She added she did not know if the Trump presidency would survive, claiming that many Republicans were as concerned as her about the damage being done to American institutions.
