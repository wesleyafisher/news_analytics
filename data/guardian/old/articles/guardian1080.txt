__HTTP_STATUS_CODE
200
__TITLE

Somalia: deadly truck bombing in Mogadishu

__AUTHORS

Damien Gayle
__TIMESTAMP

Saturday 14 October 2017 10.09EDT

__ARTICLE
At least 20 people have died in Mogadishu after a truck bomb detonated in a busy district of the Somali capital that is home to hotels, shops, restaurants and government offices. 
Security forces were trailing the truck on KM4 street in Hodan district when it exploded on Saturday, police captain Mohamed Hussein told the Associated Press. It is believed the target was a hotel. At least 15 people were injured.
The explosion left a trail of destruction across a busy intersection, and the windows of nearby buildings were shattered. 
 There was a traffic jam and the road was packed with bystanders and cars, said Abdinur Abdulle, a waiter at a nearby restaurant. Its a disaster.
Another witness, Ismail Yusuf, told Agence France-Presse: This was very horrible, the bomb went off alongside the busy road and left many people dead. I saw several dead bodies strewn about but could not count them. It was horrible.
There was no immediate claim of responsibility, but the Islamist al-Shabaab group has carried out regular attacks. The organisation, which is allied to al-Qaida, is waging an insurgency to topple the weak UN-backed government and its African Union allies.
The attack comes two days after the head of the US Africa command was in Mogadishu to meet Somalias president.
Al-Shabaab has lost most of the territory it controlled to African Union peacekeepers and government troops in recent years, backed by US drone attacks.
The militants still launch frequent deadly gun, grenade and bomb attacks in high-profile areas of Mogadishu and other regions controlled by the federal government. Many attacks are aimed at military bases, but some have targeted civilians. 
