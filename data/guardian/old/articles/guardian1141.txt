__HTTP_STATUS_CODE
200
__TITLE

Can food transparency backfire? My pig farm tour made me feel queasy

__AUTHORS
Amy Wu
__TIMESTAMP

Friday 30 June 2017 12.24EDT

__ARTICLE
For a second we locked eyes, 300lbs of pig staring at me. The pig inched closer, its body pressing again the metal barrier, its snout rubbing against my jeans.
Theyre pretty social animals, and like to be petted sometimes, said Logan Sweers, a tour coordinator at Iowa Select Farms, one of the largest pork producers in the world. I petted the pig, surprised at how soft she was and how much she looked like Wilbur from Charlottes Web  or Babe in the movie.
I was here in May with fellow journalists to get a behind-the-scenes farm tour of a company that hit the $1bn mark for pork sales in 2016. Iowa Select, whose customers include Costco, opened a $24m, hi-tech pig farm in March. It also built a welcome center to offer public tours, as part of an effort to respond to a viral video by animal rights group Mercy for Animals, which showed Iowa Select cramming pigs into cages and chopping off the tails of the piglets. (Sweers said their tails are cut off because piglets have a natural tendency to chew on them).
During the tour, I learned about the companys efforts to improve the sanitary conditions on the farm (showers are a must for both visitors and workers). I heard about how they keep the pigs in good health (through vaccinations and the use of antibiotics).
But I also witnessed something I didnt want to see: animals stuck in cages and in a concrete environment. Rather than making me feel more connected to my pork chop or barbecue, it saddened me.
That part of farming life didnt strike me as humane, and I wondered why the company thought it would create a positive impression on a consumer like me.
Before they seek to connect with consumers, farmers and food companies need to consider carefully how the public might react to the the way the animals, or workers, are treated. Otherwise the message could backfire.
The farm tour reflects this growing movement by farmers, food companies, restaurants and grocery stores to tell stories about the origin of the vegetables, fruits and meats they use or sell. Its called food transparency, and its rise coincides with consumers growing thirst for information about food production and whether it matches their values. Did farmers use genetically engineered crops or synthetic pesticides? Did the produce travel a long distance from the fields?
According to an Academy of Nutrition and Dietetics report, consumers who actively sought out information about nutrition and healthy eating more than doubled from 19% in 2000 to 46% in 2011.
For businesses, promoting food transparency is not only about educating the public; its also about marketing. When done right, it can spawn more thoughtful consumers with a greater appreciation for what goes into their shopping cart and on their plate.
I asked several experts for their advice on the dos and donts for crafting a good background story for food.
Karen Caplan, CEO of Friedas, which distributes exotic produce such as jackfruit and mangosteen to companies including Trader Joes, told me that creating a connection between food producers and consumers has become a big thing in the retail world. Many grocery stores now promote local produce and even spotlight the names of the farmers.
Social media can be a powerful tool, too. Videos posted on Facebook or YouTube, for example, offer a chance for growers to connect directly to consumers about how they care for, harvest, pack and ship their produce. The most effective videos convey something genuine and personal, Caplan said.
Consumers can smell a pitch from miles away. Being authentic is incredibly important to consumers these days, she said.
The food transparency movement has certainly changed my cooking and dining habits.
For a season, I subscribed to Blue Apron, which sends me pre-packaged ingredients for several dishes in week. I love how the recipe cards included a deep dive into ingredients, such as the origin of the fingerling potatoes and what is Khorasan wheat.
I moved to Salinas Valley in central California in 2016, the salad bowl of the world, which grows about 80% of the countrys leafy greens and generates $9bn in annual farm revenues. Being a native New Yorker, Id given little thought to the salad bars I frequented. When a grower of romaine lettuce invited me to a field tour, I jumped on the opportunity.
Watching crews of field workers hunched over in fields of lettuce, picking them in breakneck speed was eye-opening. It gave me a greater appreciation for the intense human labor that goes into growing and harvesting each head of lettuce. I asked the field manager if I could walk through the fields, imagining I was walking on top of someones meal.
No, go ahead, it will all be washed, he reassured me.
Not everyone gets a chance to visit a farm and learn more about the food we eat. There is an untapped opportunity to open more dialogue between food producers and consumers and create goodwill. Showing hard-working farm workers is a great way to establish that rapport. Showing the crowded living quarters of pigs is not.
