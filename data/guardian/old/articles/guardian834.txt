__HTTP_STATUS_CODE
200
__TITLE

How to treat your terrarium

__AUTHORS

James Wong
__TIMESTAMP

Sunday 8 October 2017 01.00EDT

__ARTICLE
You know things are bad when you recognise the trends all the cool kids are doing from the last time round. Back in the 1980s I pored over Dr DG Hessayons The House Plant Expert. It was my childhood bible. Out of all his projects, the bottle garden was the one that captured my imagination the most: a little enclosed world of virtually self-sustaining plants, even containing its own micro-weather system. Indeed, the very word terrarium comes from the Latin for enclosed earth. Amazing!
Flash forward a generation or two and the beautiful terraria that grace Instagram, hipster coffee shops and trendy clothing stores everywhere are usually rather different. Instead of using the amazing science behind how terraria work to grow otherwise-tricky plants, these often do the exact opposite. Its a horrible irony and sets newbies up for horticultural failure. If youd like to try your hand at making these little pieces of nature, skip the glossy images and look at what the Victorians did instead.
The terrarium was invented in the mid-1800s when bug collector Nathaniel Bagshaw Ward discovered a tiny bit of soil in his glass insect-collection jars that began to spring to life from stowaway seeds. In the dingy humidity of the enclosed environment, away from the chilly draughts and dry, polluted air of Victorian houses, these woodland plants grew happily with no help at all for four years  until the seal broke, which caused them to dry out. In fact, some tightly sealed terraria have thrived for decades with essentially no intervention.
The secret here is the glass, which seals in moisture and heat as well as filtering out part of the suns scorching rays. Inside these little life pods, jungle-floor plants that are normally awkward to grow indoors, such as ferns and mosses, thrive in the low light and very high humidity. Sadly, most modern terraria use, well, the exact opposite  cacti and succulents. Putting plants adapted to scorching UV rays and bone dry air in the muggy shade of an enclosed terrarium is almost always a kiss of death. Go for rainforest ferns, pileas, begonias, mosses, orchids and bromeliads for best results.
Also, 99% of the terraria I see around today are just too small  way too small. They dont have either enough air space or soil space (usually both) for plants to root in properly or produce new leaves or flowers. Aside from physically fitting your plants in, the bigger the volume of air in your container, the more functional its microclimate will also be. Id say a terrarium of at least 30cm x 30cm is necessary. Pick simple shapes without funny angles, too  spheres, cubes and oblongs are ideal. A roomy fish tank or bowl filled with moss and ferns beats a tiny geodesic shard with a crushed cactus every time. 
Email James at james.wong@observer.co.uk or follow him on Twitter @Botanygeek
