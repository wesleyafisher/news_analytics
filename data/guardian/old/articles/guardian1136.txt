__HTTP_STATUS_CODE
200
__TITLE

'If diversity means giving white men more work writing about black women, we've failed'

__AUTHORS

Kirstie Brewer
__TIMESTAMP

Thursday 16 June 2016 02.24EDT

__ARTICLE
Screenwriter Misan Sagay doesnt identify with the sassy black women portrayed in films and on television.
I have never met a black woman who behaves like that. I wouldnt know how to be sassy, says the Anglo-Nigerian, a former A&E doctor. The way black women are portrayed in film has never been in the hands of black women  until really very recently  and so there are certain stereotypes people are comfortable with.
Sagay is best known for writing critically acclaimed Belle, a period drama about Britains first mixed race aristocrat, Dido Elizabeth Belle. She has just penned an episode of Guerrilla, a Sky Atlantic drama by 12 Years a Slaves John Ridley, starring Idris Elba. 
But getting her ideas onto the screen hasnt always been easy. She has been told that there is no audience for films with black female leads and that the concept of a black film about a woman throws up an uneasy disconnect. For Sagay growing up, black films were something predominantly male and usually violent; how would women, domesticated and gentle, fit into that? 
This was the sort of received wisdom I would encounter  ridiculous, she scoffs. I write because I am a wrinkle, she says. A wrinkle in a smoothed sheet that refuses to be ironed out. Film is the major narrative art form of today; we all share memories and are joined together because of it, she says, so it is terribly important wrinkles are heard too.
Despite the push-back, the first screenplay Sagay ever wrote  interracial romcom The Secret Laughter of Women  was made in 1999 and stars Nia Long and Colin Firth. She went on to work with Oprah Winfrey on a teleplay adaptation of Zora Neale Hurstons classic Their Eyes were Watching God, starring Halle Berry.
Oprah Winfrey is an extraordinary force of nature and a very powerful, generous woman, she says when asked what working with the media matriarch was like. But I also believe this isnt the time just for people who are as extraordinary as that. Equally, a smattering of celebrated absolute geniuses like Ava DuVernay (of Selma and Scandal fame) at the top just isnt enough.
I am not fighting for the right for black people to be extraordinary, I am fighting for the right for black people to be ordinary. Sagay wants to see hundreds of good black people find work in the industry, at every level: There cant only be room for the Oprahs. 
But just how to get better representation into the industry has become something of a political hot potato. The BBC was accused of being anti-white by The Sun this month, for advertising two junior writer roles on Holby City for black, Asian and minority ethnic (BAME) people. Recent figures suggest the BBC is struggling to meet its aim to have 15% of staff and leadership coming from BAME backgrounds by 2020.
Sagay, meanwhile, is trying to do her bit to address the imbalance. She is creating a programme with the Tisch School of Fine Arts in Florence, which will set up aspiring black screenwriters with established ones. The aim is to increase the pool of black talent and give those without legs in the industry the benefit of a Rolodex so they arent starting out cold, she explains.
Diversity has become something of a buzzword and it is important to clarify the aims of any diversity exercise, she insists. It isnt a word she likes much. What am I diverse from? I think the word can be a way of establishing a norm, and me outside that norm, and that worries me. 
It feels like a dance people are doing somewhere over there, when the solution is over here and very simple, she says, hire more black people, hire more black women. 
Simply having more black people on screen isnt enough. If the outcome of diversity is to give white men more work writing about black women like me, weve failed; we need to be be the ones originating the projects - telling our stories. 
A visit to John Ridleys writers room left her inspired: The people there represented what you see on the street; it wasnt diversity, it was life. It was astonishing, but it shouldnt be. 
There is always more power in a pack and networking is important. To this end, Sagay is also a member of the Wolfe pack, a guild of 50 top female screenwriters working in Hollywood which was founded just over a year ago.
We are lowering the drawbridge to invite more women into the business, she explains. As a mother of two, she knows how difficult it can be to carve out the time to write; the Wolfe pack sponsors a writers retreat for female writers just starting out, who are then helped with gaining exposure.
What advice does she give the aspiring female screenwriters she meets? I am not advocating working twice as hard for half the cake  but be professional, be passionate and be confident, she replies. And get off the internet  it is the greatest distraction for this generation of writers.
You will get a lot of nos, but dont be put off - several of the scripts that got me the most work have not been made, but they got me in front of people, she adds. Work in an organised manner, set yourself goals so as not to drift and be ruthless in your editing. Screenwriting is coal mining and the blank page can seem tyrannical but above all, write, and write lots. 
Sagay is certainly following her own advice on being prolific. Besides Guerrilla, she is working on a contemporary drama called Imprinted for ITV Studios and a period drama for the BBC. Both have black female leads. There is nothing in film that cant be experienced through a black female protagonist  not love, war or paying the mortgage. 
Talk to us on Twitter via @GdnWomenLeaders and sign up to become a member of the Women in Leadership network and receive our newsletter.
