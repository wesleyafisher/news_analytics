__HTTP_STATUS_CODE
200
__TITLE

Chaplain to ceramicist: social care careers you never knew existed

__AUTHORS

Suzanne Bearne
__TIMESTAMP

Monday 16 October 2017 02.00EDT

__ARTICLE
When we think of social care jobs, we might think of the wonderful carers, housing officers and nurses who work tirelessly to help the most vulnerable people in society. But with a whole host of other inspirational occupations in the sector, we throw a spotlight on three workers in roles that might not at first spring to mind.
I actually became a chaplain by default. Id been working as a nurse at a Marie Curie hospice for 18 years but the team needed cover for the chaplain position for six months so I stood in. I became permanent after that. 
As a nurse I always felt like I never had enough time to offer spiritual care to patients, as youre always very task-orientated. Being a chaplain is about listening and taking notice. 
Patients often want to look back at what theyve done with their life, and we give them the time and space to do that. A while ago we had a man in who was a Liverpool supporter and we got hold of the European Cup trophy and took a photo of him with it. It meant so much to him and it was a special picture for his mum.
Im lucky that I have the chance to really get to know patients and build relationships. If somebody has a faith, I make sure we can give them the opportunity to do whatever they want with that faith. For example, we had a Muslim patient who couldnt get out of bed to pray so we got a compass and found out where east was facing so he could pray from his bed. 
Every day the patients are an inspiration to me. Theres a line in a hymn that reads strength for today and bright hope for tomorrow and thats what I try to provide as part of my job.
Of course it is hard, but its very rewarding. I go home every day and know I have made somebodys life better, even if its only for their last few hours. 
People think of hospices as sad and dark places but it can be a nice and cheerful place to work too. Theres always quite a bit of laughter and we help our patients love until the day they die.
Ruth Pryce is a chaplain at the Marie Curie Hospice in Woolton, Liverpool
I work as a ceramicist at Nightingale care home in Clapham, south London, two days a week and freelance across four other care homes across Surrey and south London. Id never had any experience of working in a care home before. I entered the profession soon after graduating with a degree in 3D design, specialising in ceramics. 
I work with all sorts of residents  some people have dementia, others may have had a stroke and can only use one hand. I teach residents either as part of a big group session held in an activity room or one on one.
Sometimes we run themes, other times residents want to create something functional to display in their room, or perhaps a vase to give to a relative. Some pop their head round to look at what were doing but then, over the weeks, slowly join in.
The classic line from the more nervous residents is, I want to have a go but my art teacher told me I was never very good at art. My response is always the same: How long ago was that? 80 years? Its time to have a go.
We uncover people with amazing talents who could have been incredible potters. Sometimes they create brilliant pieces. We recently had an exhibition with some of the ceramics from one of the care homes works for sale and it made 800.
For me, its not only about the creative side. Its also about the stories they tell me, and sometimes I try to connect those stories with what theyre building. 
Residents end up meeting pensioners theyve never met before, so its good for camaraderie, and theres a sense of pride in making something to show off. 
Emily Hall is a ceramicist who works in residential homes across Surrey and south London 
A woman finds a place at our refuge after calling the National Domestic Abuse helpline. Well meet her at the local train station because we want to ensure she has not been followed. Women have left behind their families, jobs and homes to come here and get away from the abuser. Some come with their children. 
Many break down as soon as they arrive, relieved that theyre in a safe environment. We help them with everything from finding a new GP to applying for benefits, and well also do a risk assessment to find out how safe they are. Some women stay with us for a few weeks, some six months.
Before I joined, the refuge wasnt able to take in women who couldnt speak English as there were no translators. As I can speak Hindi, Punjabi and Urdu, I can support women speaking those languages. 
A while back one young woman arrived whod only been in the country for a few months. She didnt speak any English and didnt know things such as what a zebra crossing was. I literally taught her the basics. But she took an English class and volunteered locally. She really flourished. That makes me proud of what I do. It can be challenging, especially advocating on behalf of our women and working with different agencies, but its a job I love. 
The author is a womens domestic abuse advocate specialising in BME (black and minority ethnicity) for Womens Aid in Surrey 
Looking for a job? Browse Guardian Jobs or sign up to Guardian Careers for the latest job vacancies and career advice
