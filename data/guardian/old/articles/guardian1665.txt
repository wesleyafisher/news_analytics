__HTTP_STATUS_CODE
200
__TITLE

Crossword blog: Let's make US puzzles work in the UK

__AUTHORS

Alan Connor
__TIMESTAMP

Monday 16 October 2017 04.34EDT

__ARTICLE
So, I recently eulogised American-style crosswords. These witty, moreish puzzles keep solvers on their toes through allusive, often terse clues and through answers that merrily include abbreviations, fragments of phrases, elementary foreign vocabulary and other unfamiliar thrills.
Admitting entry to odds and ends that are not to be found in a dictionary is not merely a necessity in a grid that has far fewer black squares, and in which every square is part of an across as well as a down; it also asks the solvers brain to work in a different way, dredging answers from parts of the brain that do not typically work together. As a solver, its a different feeling.
(A quick note of reassurance to the UK cryptic: your lustre remains untarnished, from the steady-as-she-goes Times  through the rowdier broadsheets  to Azed and beyond. Just as American constructors may discover the merits of British setters, so can we acknowledge that those multiply-interlocking transatlantic grids add to the gaiety of puzzling.)
After a couple of tries, the hurdle for a British solver is not the form but the occasional piece of content. American puzzles are a bag of fun once you get used to them but, every so often, an entry requiring familiarity with an unfamiliar toothpaste brand intersects with another requiring knowledge of the nickname of a former associate justice of the US supreme court ... and the grid remains unfilled.
What, though, if we were to fill an American-style grid with culture, abbrev.s and language fragments familiar to a Brit?
Reader, I tried it.
My first stab at an American-style crossword for a UK audience is here (in digital form) and here (in printable form). I would love it if you solved it.
Some notes:
And, if youre solving using software:
Above all, I hope that every entry is ultimately gettable through crossing letters, common sense and logical and/or lateral thinking. I recommend trying a few genuine American puzzles first, at the gentle end: the LA Times (which you can solve in your browser), then the (still gentle) dazzling archive of constructor Matt Jones (for which youll need the software mentioned above).
Do let me know what you think. It will inform the next puzzle. And if youre tempted to contribute an American-style puzzle of your own: I concocted a theme, filled it using an existing grid and then created the file according to this technical advice. (Next time I may use the app called Sympathy.) Id be thrilled if you assembled something similarly US-meets-UK.
Good luck and enjoy. In the meantime, back to cryptics.
 The solution can be revealed in the digital version or seen in this PDF (though please try to solve before looking!). If there is a second puzzle, we might hold back the solution for a time  again, what do you think?
