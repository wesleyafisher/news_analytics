__HTTP_STATUS_CODE
200
__TITLE

Boris Johnson refuses to apologise for Libyan 'dead bodies' remark

__AUTHORS

Patrick Wintour Diplomatic editor
__TIMESTAMP

Tuesday 17 October 2017 09.17EDT

__ARTICLE
Boris Johnson is facing fresh calls to resign after he sidestepped calls to apologise over his claim that a Libyan coastal town could be turned into a luxury resort once the dead bodies were removed. 
The foreign secretary was also challenged over why he described war-torn Mogadishu as a thriving international city.
Johnson caused uproar, and infuriated Theresa May at the Conservative conference, when he suggested the town of Sirte in Libya could become the next Dubai once the dead bodies were cleared away. 
 Speaking in the Commons on Tuesday, he refused to apologise and said the attacks on him were political point scoring and trivialised a serious issue. 
The Scottish National partys international affairs spokesman, Stephen Gethins, had challenged Johnson, saying: Hes certainly right that hes managed to bring people together in Libya.
 Quite remarkably, hes been criticised across the political divide and by a former British ambassador, and he was described as having dishonoured the sacrifice of those who fought and died in Sirte.
 Labours Emily Thornberry tweeted: 
 In his appearance at Foreign Office questions in the Commons, Johnson accused Russia of behaving as if it was in a new cold war. The foreign secretary is due to visit Moscow in December. 
 He told MPs: In many ways Russia is behaving as though there is indeed a new cold war and our objective is to prevent that from getting any worse, to constrain Russia, to make sure we penalise Russia for its malign and destructive activities where they are taking place, but also to engage where we can.
 The UK couldnt have normal relations with this country [Russia], but we understand that we should keep a dialogue.
 Johnson also chose a surprising form of words to describe the political reality in Somalia. Discussing the deadly bomb attack, he said: The whole house will wish to join me in condemning the atrocity in Mogadishu on Saturday, which claimed at least 281 lives. Those who inflicted this heinous act of terrorism on a thriving capital city achieved nothing but to demonstrate their own wickedness.
 The US state department and most western embassies have long advised against any travel to Mogadishu.
