__HTTP_STATUS_CODE
200
__TITLE

Crossword roundup: who do cryptic setters want as new prime minister?

__AUTHORS

Alan Connor
__TIMESTAMP

Monday 12 June 2017 05.02EDT

__ARTICLE
The pollsters may as well have used hepatoscopy. How did the crossword setters do? Sylvanus recalled the far-off days ...
21ac Those standing hope it will be comfortable for them (8)
... of a working MAJORITY; in another cryptic definition, Samuel offered some straightforward guidance for what to do ...
11d Ones cross to be penned in here (7,5)
... in the POLLING BOOTH; as far as I can see, the only actual prediction was from KCit ...
 7ac Share vote (Labours third, out of it) (5)[ synonym for vote without (out of it) the third letter of LABOUR ][ BALLOT without B ]
... in a psephologically inaccurate but still persuasive clue for BALLOT. So now cryptic setters must come to terms with the imminent loss of a prime minister whose surname, even more than BROWN, could be mistaken for non-prime ministerial things. From an auxiliary verb to a popular month, MAY was a gift for misdirection. Can the same be said of RUDD or FALLON? Of course not. 
What setters need, looking across the parties, is one of the EAGLEs, Angela or Maria (both L) or perhaps Mhairi BLACK (SNP), Neil (SNP) or James (C) GRAY, Lyn (L), Nick (L) or Alan BROWN (SNP), or Chris (C), Kate (L) or Damian GREEN (C). Failing that, we would surely all be happy to see Prime Minister Kirstene HAIR. And do feel free to share your own favourite ambiguous MPs, from Liz TWIST to Tracey CROUCH below.
We do not expect hidden messages in the Times crossword, aside perhaps from a significant birthday for whoever the current monarch might be, or some equally stately anniversary. So this was quite a surprise ...
... and inadvertently poignant, published exactly one week before the death of Adam West. The puzzle also featured that rara avis, the prime numbers clue:
17ac Manage surety in prime locations in dread (5)
Here we look at the letters of MANAGESURETY and take the second, third, fifth, seventh and eleventh (2, 3, 5, 7 and 11 being the first prime numbers) for the answer ANGST. This is a device seen far more often in the competition run at these pages  and is the topic of our next challenge. Reader, how would you clue PRIME?
Many thanks for your clues for the new variant NUCULAR. I understood some solvers incredulity that this spelling should appear in, say, the Oxford English Dictionary  and the decision on the part of many to help the solver by alerting them to its quirkiness, nicely executed in GappyTooths Popular science revision that should otherwise have been potentially unclear.
The runners-up are Schroducks Simply turning up in radium, lutetium, carbon, uranium, nitrogen? and Obi23s Not Authorised Version of avuncular, bumbling Bushs bomb?; the winner is Catarellas ingenious Like a crazy presidents bomb, explosive supercritical plutonium should be separated from politics (ie Trump).
Kludos again to Catarella; please leave this fortnights entries and your pick of the broadsheet cryptics below.
And thanks to solver Tony Collman for nominating this Times clue ...
26ac  Man United soon to control opening of game, sweeper claims (10)[ synonym for control + first letter (opening) of GAME, both contained in synonym for sweeper ][ RIDE + G, both inside BROOM ]
... with a consistent surface that consistently misdirects us from spotting the BRIDEGROOM. Score!
