__HTTP_STATUS_CODE
200
__TITLE

Hackers rack up 12,000 phone bill and providers passed it on to me

__AUTHORS

Anna Tims
__TIMESTAMP

Thursday 12 October 2017 02.00EDT

__ARTICLE
I run a small company and incur monthly phone bills of about 140. Recently, however, I was charged 3,075 for more than 200 calls to overseas premium rate numbers over a four-day period. My provider, Focus Group, was unaware of the charges until I contacted it. It placed a bar on all international calls and premium rate numbers, but advised me that a further 8,282 had been racked up in the previous 11 days.
Pennine supplies my actual telephone systems, and it and Focus are blaming each other. Pennine says Focus should have noticed the large call rates, which were occurring at night and were out of character, while Focus says Pennine should have offered a more secure system.
In the meantime, I am left with a bill for around 12,000 that I cant afford to pay. LMN, Blackburn, Lancashire
It sounds as though you have fallen victim to a type of fraud whereby criminals hack into corporate telephone systems and install software that automatically calls premium rate numbers run by fellow gang members. Its one of the most lucrative scams in the UK, costing companies an estimated 1.5bn a year.
Ordinarily, you would expect your provider to flag up any unusual activity on your phone lines. Focus, however, says that as a reseller of services it only receives information about customer usage from carriers periodically. We rely on the alerts provided to us by the carriers, as they are the only ones with real-time information about the calls that customers are making, says a spokesperson. Its terms and conditions advise customers to have secure passwords on their phone systems to prevent scams, and warn that they are liable for any fraudulent charges.
Security is also something you might think your hardware provider would take care of. Pennine tells me that it changes all passwords from manufacturers settings when installing. If we maintain the system, we retain those passwords and take responsibility for system integrity in relation to them, says a spokesperson. We cannot comment on an individual client; however, there have been no reported instances where passwords we are responsible for have been compromised.
According to Ombudsman Services, both providers have questions to answer about how such a large bill accrued without anyone noticing. If we were to investigate a complaint about the company that installed equipment, we would consider whether they changed the password to one that could easily be guessed, or allowed it to fall into the wrong hands, says a spokesperson.
As for Focus, it says: We would want to know what promises the provider made to the customer about monitoring their usage, and whether any usage limits had been discussed.
You had to wait eight weeks before you were allowed to refer the complaint to the ombudsman and, in that time, Focus relented a little. It issued you with a credit note for 9,180, leaving you to pay 3,059. Exhausted by weeks of wrangling, you accepted this without invoking the ombudsman.
A police investigation found the fraudsters are based in Russia and there is nothing they can do.
If you need help email Anna Tims at your.problems@observer.co.uk or write to Your Problems, The Observer, Kings Place, 90 York Way, London N1 9GU. Include an address and phone number.
