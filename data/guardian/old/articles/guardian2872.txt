__HTTP_STATUS_CODE
200
__TITLE

Novice boxer who died 'wasn't given a chance', inquest told

__AUTHORS
Press Association
__TIMESTAMP

Tuesday 10 October 2017 08.29EDT

__ARTICLE
A novice boxer who died after being knocked out at an unlicensed event did not stand a chance of recovering due to a lack of medical provision, his twin sister has told an inquest.
Jakub Moczyk, 22, known to friends and family as Kuba, was rendered unconscious by a punch to the head during the third round of his first public fight, in Great Yarmouth, Norfolk. He was taken from the Atlantis Tower arena to hospital on 19 November 2016 and died two days later.
His sister, Magdalena Moczyk, told an inquest at Norfolk coroners court in Great Yarmouth of her concerns about the fight, including that the referee was the opponents coach, the opponent had more boxing experience than her brother, and that she felt there was insufficient medical cover.
There were no doctors, no ambulances for over an hour, she said in a written statement read to the court. No stretchers, nobody was prepared for this. It was a total nightmare. Nobody seemed to be in charge or responsible. Kuba didnt recover. He wasnt given any chance.
She was at the ringside for the fight and felt it should have been stopped in the second round, when his 17-year-old opponent, Irvidas Juskys, appeared to be struggling.
[Kuba] was clearly winning the second round as his opponent was sick, not once but twice, she said. He was hanging over the ropes and showing no desire.
The referee, Melvin Payne, allowed the bout to continue. Moczyk said in her statement: We found out later the referee was the opponents coach. I think this is really wrong and [Kuba] had no chance of winning. If the fight stopped in the second round, my brother would still be alive.
The senior coroner for Norfolk, Jacqueline Lake, asked Payne if it concerned him that he was both the referee and a coach at the gym Juskys attended. 
None whatsoever, he said. Everyone has to be impartial, you know.
Neither fighter was wearing a headguard, and Payne said there was no requirement for them to do so.
Asked why he had not started counting out Juskys when he was on the ropes in the second round, Payne said: He wasnt hit by a punch. He was gagging on his gum shield. Thats more [a case of] anxiety. He said the boy had wanted to carry on.
Asked by Moczyks mother about his qualifications, Payne said he had 40 years of experience. 
Moczyk, a Polish-born factory worker, lived in Bath Hill, Great Yarmouth. His sister said: Kuba was my twin brother, my only brother, my everything  the perfect brother and we were very close. He was hard-working, very protective and loving of his family. He was full of life and never stopped smiling.
The inquest continues.
