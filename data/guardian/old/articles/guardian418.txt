__HTTP_STATUS_CODE
200
__TITLE

Carmelo Anthony to join Thunder from Knicks in multi-player trade

__AUTHORS
Associated Press in New York
__TIMESTAMP

Saturday 23 September 2017 14.19EDT

__ARTICLE
The New York Knicks have agreed to trade Carmelo Anthony to the Oklahoma City Thunder in a multi-player deal, according to a person with knowledge of the trade.
The Knicks will get Enes Kanter, Doug McDermott and a draft pick.
The source spoke on condition of anonymity on Saturday because the trade had not been announced.
A day after saying they expected their 33-year-old all-star small forward to be at training camp Monday, the Knicks finally found a trade they have been seeking since last season. The deal puts Anthony into a loaded Oklahoma City lineup that includes NBA MVP Russell Westbrook and Paul George, who was acquired from Indiana this summer.
Anthony agreed to waive his no-trade clause to complete the deal, which was first reported by the Vertical. It saves the Knicks and their longtime star from what could have been an awkward reunion next week.
Phil Jackson spent the latter part of his time in New York making it clear he wanted to move Anthony. But a deal was difficult because the 33-year-old forward has two years and about $54m left on his contract, along with the ability to decline any trade.
He had long maintained that he wanted to stay in New York, but constant losing and a chance to play with a talented lineup convinced him it was finally time to go.
After making the postseason each of his first 10 seasons, Anthony has been on the sidelines the last four years and said at the end of last season his priority was a chance to win. He wouldnt have that in New York, where the Knicks are emphasizing youth and have little proven talent with which to surround him.
He is close with Westbrook and George and should fit in nicely in Oklahoma. He can possibly settle into the spot-up shooter role he has played in the Olympics, where he has won a record three gold medals and is the career scoring leader for the US men.
Anthony will see his old team-mates soon: the Knicks open the regular season at Oklahoma City on 19 October.
