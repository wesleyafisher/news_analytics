__HTTP_STATUS_CODE
200
__TITLE

'People can change': ex-prisoner smashes stereotypes, one workout at a time

__AUTHORS
Emily Wasik in New York
__TIMESTAMP

Saturday 17 June 2017 06.00EDT

__ARTICLE
For ex-convicts, getting back on your feet is hard. Former drug kingpin and inmate Coss Marte is on a mission to change that, with his prison-style fitness studio that opened its doors at Saks Fifth Avenue in May. In The Wellery, Saks 16,600 sq ft health mecca teeming with high-end designer clothes, salt rooms and meditation classes, ConBody is decked out like a jail, with cell bars, metal fences and a backdrop for taking post-workout mugshots.
But this isnt just another bourgeois boot camp plus some cute jail-striped jumpsuits. These workouts are actually taught by former inmates themselves. Marte employs 10 former prisoners, and they incorporate the same no-frills exercises Marte carried out when he was locked up in a 9ft x 6ft solitary confinement cell with nothing but a bed and a Bible.
We dont use any equipment  just our own body weight. And were not just working out; were solving a real problem, Marte said.
How Marte got to where he is today could be mistaken for your quintessential Orange Is The New Black flashback, a show he loves: I love the show. Obviously they exaggerate some things, but its TV after all. Im addicted!
Having grown up in New Yorks Lower East Side in the 1990s, Marte was peddling drugs on his very own street corner by age 12. By his late teens, he was overseeing a $2m cocaine and marijuana empire, affording him a lavish lifestyle of cars, bling and women. I used to spend $2,000 on Louis Vuitton shirts, but Id never do that now, he said.
Aged 20, Marte was arrested and sent to jail for seven years. After receiving a full physical upon entering jail, doctors told the then-overweight Marte that he only had five years to live because of high blood pressure and cholesterol.
I ended up becoming the Forrest Gump of the prison yard, running circuits every morning despite sneers from other inmates, he said.
In just six months, Marte lost 70lbs, and helped 20 other inmates lose 1,000lbs collectively. However, just two months before his release, an altercation with a correctional officer landed him in solitary confinement and tacked another year on to his sentence.
I was so devastated that I wrote my family a 10-page letter to them telling them to forget about me. After I finished the letter, I realized I didnt even have a stamp to send it, so I threw the letter into the corner of the room.
A week later, he received a letter from his sister, telling him to read psalm 91 from the Bible. As soon as I flipped to psalm 91, a stamp fell out of it. I felt chills all over my body. Then I started reading the Bible from front to back, and realized that what I had been doing with my life was affecting the lives and families of so many people. Thats when I had the lightbulb moment to start ConBody. To give back, Marte said.
And that he did. In 2014, less than a year after he was released, he opened his first ConBody studio on the same street corner he used to hustle drugs. Former convicts find it notoriously difficult to get back into the workforce. One New York City study found having a criminal record reduced the likelihood of a callback or job offer by nearly 50% (28% for applicants without a criminal record versus 15% of applicants with).
Although the business has really taken off over the past three years  with the Saks opening and even being featured on Saturday Night Live on 20 May  hes definitely come across some negative reactions too.
I tried to give a woman a high-five after a class once, saying Dont be scared. I only did six years in prison! She screamed, Dont touch me, and picked up her things and walked away.
Marte believes the whole ex-con stigma should be smashed, one boot camp-style slug at a time. Im now at Saks. This is crazy. I have tons of people supporting me and helping me along the way. People can change. This is what its all about. Giving people a second chance.
Today Marte speaks at conferences across the country and at schools and juvenile detention centers in the Tri-State area to deter young people from going down the road he did.
Next on the horizon for ConBody is a book called ConBody: Do the Time to be published in 2018, and a documentary film slated for release in 2019.
Were also launching a Kickstarter campaign to raise money for the ConBody app where people will be able to book classes, buy apparel, and access workout videos, he said.
Down the line, Marte is also planning to expand his cell footprint within and beyond the concrete jungle. Im looking to open up on the east coast, the west coast and everything in the middle!
