__HTTP_STATUS_CODE
200
__TITLE

What Happened by Hillary Rodham Clinton  digested read

__AUTHORS

John Crace
__TIMESTAMP

Sunday 24 September 2017 12.00EDT

__ARTICLE
What happened? I wish I knew for certain. What I can say is that I absolutely gave it my best shot and that I couldnt have had a wiser team around me. When I shed a tear as I waited in line at President Trumps inauguration, it wasnt for me. It was also for all the people in America who would have to live with that man as their leader.
I wont deny that the six months since I lost the election have been some of the toughest of my life. But Ive gotten by a day at a time thanks to the love of my husband, Bill, the support of close friends, such as Sheryl Sandberg, and the knowledge that I won the popular vote. Ive learned to concentrate on taking pleasure from the little things. Like my daily yoga practice and buying a farm in upstate New York. In particular I find alternate breathing through each nostril helps me centre myself. It sounds hard but it gets easier with practice: you breathe in through one nostril in one of your homes, then get in the car and breathe in through the other in another.
Friends have asked me why I did it, why I agreed to run for president. To tell the truth, I had no option. I knew that many people didnt like me. And I knew that it would be hard to defend Barack Obamas desperately poor record in office. But I had to do it. All my life Ive never wanted to do anything other than serve my country. And be president. And in a way I am president because I did win the popular vote. Yet unfortunately, even though I won, I lost. And I take full responsibility for everything that went wrong apart from those things that were other peoples fault. 
When I think back on my campaign I cant think of anything I would have done differently apart from those things that I might have done differently. Everything from the two stylists who did my hair every morning while I was eating scrambled egg whites to ringing Bill at night to tell him how much I loved him and was grateful for his support was just perfect. We also had the best ever campaign slogan in Stronger Together. I loved that slogan because it said everything I wanted to say. Though maybe it should have been Together Stronger. Just as we are together stronger, we are also stronger together. 
Now let me tell you a little bit about me. Im a woman. I know many people found that hard to accept. But Im proud of being a woman. More than being a woman, I am also a Mom, a wife, a daughter and a grandmother. Theres nothing I adore more than being a Mom. Apart from being a wife, a daughter and a grandmother. Theres also nothing I adore more than being a grandmother. Apart from  (can someone edit this bit for me as my stylist has just turned up).
It was an honour and privilege to serve the American people by meeting them out on the campaign trail. Perhaps I should have heeded my teams advice and not stopped so long to talk to people. But when I see a poor black girl crying in the rain because shes been told to wait for hours to see me, my heart just yearns to reach out. 
Dont get me wrong, Im not blaming anyone but me for losing the presidency. Even though I did win the popular vote. Did I mention that? And yes, I cant deny that it was a bit bone-headed of me to use my private email for state business, but it was a mistake anyone could have made if they hadnt read the rules. Besides, none of them were in the least incriminating; 20,000 were to my assistant asking her to use the fax machine as I couldnt cope with email. And the other 10,000 were to Bill telling him how much I loved him and that I wasnt still angry about Monica.
Yes, losing the election was all on me. But dont forget the Russians; they were in it up to their neck. As was James Comey the head of the FBI. But I can do no more. My race is run. All that is left to me is to gently weep bitter tears of sorrow while praying for the future of American democracy under that complete orange slimeball douchebag. Breathe in through alternate nostrils. And relax.
Digested read, digested: Shit happened.
