__HTTP_STATUS_CODE
200
__TITLE

British MEPs who voted to delay trade talks 'facing witch-hunt'

__AUTHORS

Daniel Boffey in Brussels
__TIMESTAMP

Tuesday 17 October 2017 06.40EDT

__ARTICLE
Guy Verhofstadt, the European parliaments Brexit coordinator, has condemned David Davis for running a witch-hunt against British MEPs who recently voted in favour of delaying trade talks because of insufficient progress in the negotiations.
Davis, the UKs Brexit secretary, has called  with the support of Theresa May  for 18 Labour MEPs and one Liberal Democrat who supported a recent European parliament resolution critical of the British government to be sacked for acting contrary to the national interest.
The Conservatives have already removed the party whip from two Tory MEPs who supported the motion, Julie Girling and Richard Ashworth. Girling has spoken of her sadness at the move. It is the party which has changed, not me., she said.
Verhofstadt, a former Belgian prime minister who played a key role in drafting the European parliaments resolution, was said to be astonished by Daviss demand in letters to the Labour leader, Jeremy Corbyn, and the Lib Dem leader, Vince Cable, for them to discipline their MEPs.
Outrageous witch-hunt of British MEPs for standing up for the rights of UK & EU citizens, he tweeted.
A source close to Verhofstadt said: It is difficult to believe this is a real letter. We had to double-take. Is this real? It is not fair play.
Britain wants to discuss its future trading relationship with the EU because44% of UK exports go to, and 53% of imports come from, the EU 27 countries. Post-Brexit conditions of trade could, therefore, have a major effect on Britains economy.
TheWorld Bank estimates UK trade with the EU in goods and services could fall by 50% and 62% respectively if no trade deal is agreed after Brexit, against 12% and 16% if the UK stays in the single market through a Norway-style agreement.
Clean Brexit campaigners say the shortfall can be offset through more trade with non-EU countries, but those who argue the UK must retain close links with the single market doubt this, certainly any time soon. Both groups want certainty.
However, theEU27s negotiating guidelines for the two-year Brexit talkssay discussion of the framework of a future relationship can only take place in phase two of the talks, once sufficient progress has been made on the separation phase and particularly the UKs exit bill.
The resolution, passed by a large majority, advised the European council to rule that insufficient progress had been made in protecting citizens rights, finding a solution to the issue of the Irish border and reaching agreement on the principles of the UKs Brexit divorce bill.
In his letter to Corbyn and Cable, Davis wrote: While I would not expect opposition political parties to agree with us all the time about the end state we seek, it is self-evidently part of the national interest to support a discussion about our future relationship with Europe.
Cable told the Guardian that Daviss letter followed a pattern in which the government accused its critics, including the media, of acting against the interests of the nation.
He said: It is both extraordinary and alarming that David Davis is demanding opposition parties sack representatives simply for doing their jobs. Conservative ministers should stop making these absurd demands and focus on protecting the countrys prosperity and security from Brexit.
In response to losing the whip, Girling said in a statement: I am disappointed to have had the Conservative whip withdrawn, especially since it happened before any discussion was held with me about the reason for my vote last week.
I have been active in the party for 40 years  20 as an elected representative  and for 39 of those years our policy has been pro-Europe. I have never considered myself a rebel and have not altered my beliefs. It is the party which has changed, not me, but that does not stop me feeling sadness that such a precipitate response was felt to be appropriate.
TheEU27s negotiating guidelines for the two-year Brexit talksstipulate that they must take place in two phases: separation and orderly withdrawal, followed by future relationship. Only when the EU27 decide sufficient progress has been made on phase one can phase two begin.
Broadly, phase one is about providing clarity and certainty to people and businesses on Brexits consequences and agreeing a sum covering the commitments the UK made as an EU member: avoiding a legal vacuum, protecting citizens rights, solving the Irish border, and reaching a financial settlement.
Phase two of the talks will then focus on agreeing the framework of the future trading relationship between the UK and the EU. A transition period can also be agreed as part of this second stage, but the detail of the future relationship can only be worked out once the UK has left.
Britain wants to move to stage two fast, but in order to keep as much leverage as possible in talks on the future relationship aims to delay agreeing the financial settlement as long as possible. The EU27 are adamant that all phase one issues must be addressed to their satisfaction before any talk of the future relationship.
The partys whip in the European parliament, Dan Dalton, said in a letter to the two Tory MEPs: The Brexit negotiations are the most important negotiations our country faces and reaching a new partnership with the European Union is in the interests of both the UK and the EU.
The resolution by the European parliament sought to delay progress in the negotiations between the UK and the EU by holding back talks on the future relationship. It also proposed that one part of the UK, Northern Ireland, could remain in the single market and customs union, while the rest of the UK departs  which is not acceptable.
Downing Street has backed the calls. It is surely in everyones interests, both in Britain and in Europe, that talks can progress on trade and our future relationship, a source said. 
The prime minister also raised the issue with Corbyn during prime ministers questions. 
