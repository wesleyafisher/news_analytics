__HTTP_STATUS_CODE
200
__TITLE

Schools closed as Ireland braces for landfall of ex-Hurricane Ophelia

__AUTHORS

Henry McDonald Ireland correspondent
__TIMESTAMP

Monday 16 October 2017 02.25EDT

__ARTICLE
Ireland is bracing itself for the tropical storm Ophelia with a red weather warning issued for the Republic and an amber warning issued for Northern Ireland.
Education officials have ordered the closure of all educational facilities across the island. A statement from Irelands education department said the closures were necessary following official advice on the unprecedented storm. 
All Schools,Colleges and other Education institutions closed tomorrow 16 October see https://t.co/IrcHWyyBKM
Met ireann  the Republics weather service  warned on Sunday night that lives could be in danger as Ophelia hits the countrys Atlantic coast with winds of up to 100mph.
Red alert is the highest warning Met ireann can issue.
A Met ireann spokesperson said on Sunday: Ophelia is forecast to track directly over Ireland during the daytime tomorrow.
Violent and destructive gusts are forecast with all areas at risk and in particular the south-west and south in the morning, and eastern counties in the afternoon. 
Heavy rain and storm surges along some coasts will result in flooding. There is potential risk to lives.
The eye of the storm was around 300km off the south-west coast of Ireland at 5am, moving at 70kmh, was expected to bring strong winds to Ireland and the UK on Monday, according to US hurricane forecasters. They added that Ophelia had been downgraded from a hurricane to a post-tropical cyclone overnight.
Eyes to the south west  Heres #HurricaneOphelia which is still a Cat. 3 storm(earth.nullschool) pic.twitter.com/SgUqzbxFfI
To underline the serious risk Ophelia poses to public safety, Met ireann also warned that it could be as bad the 1961 Hurricane Debbie, the most powerful cyclone ever to hit Ireland, which caused 18 deaths.
Joanna Donnelly, a Met ireann meteorologist, told viewers during Sunday nights weather forecast on RTE television that this is not the remnants of a hurricane  this is a hurricane.
Forecast position of Storm #Ophelia at midday on Monday.Read the latest guidance from Met Eireann & the NHC here:https://t.co/LhHdGAtYWb pic.twitter.com/vpkMvFCJaF
As well as the nationwide closure of schools and colleges, court sittings in the counties expected to be battered most by Ophelia have been cancelled.
The Irish coastguard has advised the public to avoid any visits or walks to coastal or cliff areas. The countrys national emergency coordination committee, which met earlier on Sunday, has also advised cyclists not to go out on their bikes. 
Stena Line has cancelled ferries on its Dublin to Holyhead, Rosslare to Fishguard and Belfast to Liverpool sailings. At least 50 Aer Lingus flights out of Ireland on Monday have also been cancelled.
 A number of flights on Mon 16 Oct are cancelled due to severe weather. Live flight info is available at https://t.co/X9sMVK8t8y #Ophelia
The Irish Defence Forces have been put on standby to help out with flood defences and any potential evacuations.
In Northern Ireland the Met Office in the region said there could be winds of up to 80mph in the south-east on Monday.
John Wylie, a Met Office spokesman, said: The Met Office has issued an amber warning. That means a greater likelihood of damaging winds through the latter part of Monday afternoon and evening, particularly for the evening rush hours.
There is the potential for damage to trees, there could be a danger to life from flying debris.
Northern Ireland Electricity said it has engineering crews on standby to deal with potential outages caused by the storm.
