__HTTP_STATUS_CODE
200
__TITLE

Mrs Osmond by John Banville review  superb Henry James pastiche

__AUTHORS

Edmund White
__TIMESTAMP

Saturday 14 October 2017 02.30EDT

__ARTICLE
John Banville is one of the best novelists in English, andan expert ventriloquist, among other things. In his case, ventriloquism is his wayof embodying the past. In The Untouchable, for instance, he channelled the high-class Cambridge twaddle of Anthony Blunt. Mrs Osmond is his sequel to Henry Jamess The Portrait of a Lady. At times it has the glacial pace of the original, endless psychological dithering punctuated by brilliant flashes of melodrama. Even stylistically it is a perfect fit: the actual descriptions of places are rather vague, but the metaphors are devoted to extremely vivid, even over-the-top, language. For instance, Isabel Archer wanders into the unwonted solitude of a hotel lobby  a general, rather abstract wording. Twolines later, she is savouring her freedom, which is rendered in a startling, original metaphor in which the thing itself is freedom: She was being given a sample of the thing itself, as a seamstress might press upon her without charge a sample of fine silk.
Isabel, who has married Gilbert Osmond, is of course an American, as is her very refined, Europeanised and devious husband. She reflects on how she was lured to Europe from Albany, New York, again in an unadorned statement backed up by a vivid metaphor: Yet she should not have allowed her aunt to thrust her upon that fabled continent so precipitately, as a free-traders posse might snatch from the doorway of a dockside tavern some poor young hearty fuddled on rum and press him into a captive life upon the roiling ocean; indeed, she should not have allowed it. The movement between the refinement of a civilised womans dwelling on the past and the kidnapping of a young male sailor precisely follows Jamess way of injecting a coarse energy into descriptions of what he called his super subtle fry.
Banville has described James as his principal influence; here he even imitates Jamess way of putting slightly slangy expressions in inverted commas (taken a shine; things, meaning belongings). James, we must never forget, wrote plays, even if they were unsuccessful, and some of his most powerful scenes (which Banville imitates perfectly) are toe-to-toe theatrical confrontations that startle us because they emerge out of grey reams of introspection and indirect discourse.
I suppose some people will compare this book to Colm Tibns The Master, his novel based on moments from Jamess life, but Mrs Osmond is a sequel to his most famous novel, not a fictional look at the author himself. Moreover, Banvilles book is faithful to Jamess manner, while Tibns avoids the long sentences, the placid descriptions and the hectic metaphors, the ordinary words in inverted commas. Tibn is mapping out what he imagines are Jamess thoughts in modern language.
Although many of the twists and turns of the plot could have been predicted from The Portrait of a Lady, there are also quite a few surprises, a tribute to Banvilles ingenuity, though all in Jamess spirit. Isabel, who realises in Portrait that Osmond married her for her money and that his real partner is Madame Merle, here uses her wealth and the power of inheritance to effect aneat revenge.
Mrs Osmond is both a remarkable novel in its own right and a superb pastiche. But I found irritating the very mannerisms that try my patience in James; I remember once reading in The Golden Bowl, Her thick hair was what could vulgarly be called brown, and throwing the book against the wall (why should the colour brown be vulgar?). When his enigmas are all lined up, no one is more gripping than James. Still, I prefer Banvilles own nuanced, exciting voice, as in Ancient Light or The Sea.
 Edmund Whites latest novel is Our Young Man (Bloomsbury).
 Mrs Osmond is published by Viking. To order a copy for 12.74 (RRP 14.99) go to bookshop.theguardian.com or call 0330 333 6846. Free UK p&p over 10, online orders only. Phone orders min p&p of 1.99.
