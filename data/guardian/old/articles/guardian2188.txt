__HTTP_STATUS_CODE
200
__TITLE

The Ballad of Shirley Collins review  brilliant story of lost folk singer

__AUTHORS

Cath Clarke
__TIMESTAMP

Thursday 12 October 2017 01.00EDT

__ARTICLE
Life imitating art? In a tale of treachery and tragedy straight out of a ballad, the English folk singer Shirley Collins dramatically lost her voice on stage in the late 70s. Her husband had just her left for another woman, whod taken to showing up at Collinss gigs  rubbing salt in the wounds by wearing the offending exs jumpers. Humiliated, Collins opened her mouth but nothing came out. He undid me. I should have got angry, but I got heartbroken, she explains in this portrait of the artist as an older and wiser woman.
Diagnosed with dysphonia, Collins dropped off the folk map until, aged 82, she released a comeback album, Lodestar, last year. With the help of old letters, yellowing photos, audio recordings and old home-movie footage, Collins recalls her life  with additional interviews by adoring young folk singers, plus superfan Stewart Lee. We also watch her twitching with nerves, recording again for the first time in decades. Collins drinks tea out of a mug with diva on it  nothing could be further from the truth. She is a brilliant documentary subject  plain-speaking, authentic, feisty and seemingly without ego. 
