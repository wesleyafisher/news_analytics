__HTTP_STATUS_CODE
200
__TITLE

Sir Peter Hall, RSC founder and former National Theatre director, dies aged 86

__AUTHORS

Caroline Davies
__TIMESTAMP

Tuesday 12 September 2017 07.22EDT

__ARTICLE
Sir Peter Hall, the former director of the National Theatre and founder of the Royal Shakespeare Company, has died aged 86, the theatre said.
He died on Monday at University College hospital in London, with his family at his bedside.
During a career that spanned more than half a century he staged the English-language premiere of Samuel Becketts Waiting for Godot and the world premiere of Harold Pinters Homecoming.
In a statement, the National Theatre said it was deeply saddened to announce the death of one of the great names in British theatre.
Peter Hall was an internationally celebrated stage director and theatre impresario, whose influence on the artistic life of Britain in the 20th century was unparalleled, it said. 
He founded the RSC in 1960, aged 29, leading it until 1968.
The RSC realised his pioneering vision of a resident ensemble of actors, directors and designers producing both classic and modern texts with a clear house style in both Stratford and London, the National Theatre said.
After being appointed director of the National Theatre in 1973, Hall was responsible for the move from the Old Vic to the purpose-built complex on the South Bank.
He successfully established the company in its new home in spite of union unrest and widespread scepticism, the theatre added. 
After leaving the NT in 1988, he formed the Peter Hall Company, and in 2003 became the founding director of the Rose Theatre Kingston. 
Hall was diagnosed with dementia in 2011.
Former directors of the NT were among the first to pay tribute. Sir Nicholas Hytner, its director from 2003 to 2015, said: Without him there would have been no Royal Shakespeare Company, the National Theatres move to the South Bank might have ended in ignominious failure, and the whole idea of the theatre as a public service dedicated both to high seriousness and popularity would not have seized the public imagination. He was a man of great warmth, and mischievous wit.
Sir Trevor Nunn, who was NT director from 1997 to 2003, said: In originating the RSC, he created an ensemble which led the world in Shakespeare production, but which triumphed to the same extent in presenting new plays of every kind. Not only a thrilling and penetrating director, he was also the great impresario of the age.
He alone had the showmanship and energy to establish the three-ring circus of our unique National Theatre on the South Bank. Peter Hall is a legend, whose legacy will benefit many generations to come. And yes, he was my beloved friend for 50 years.
Sir Richard Eyre, NT director from 1988 to 1997, called Hall the godfather of British theatre. Peter created the template of the modern director  part-magus, part-impresario, part-politician, part celebrity, he said. Like countless directors, writers and actors of several generations I have much to be grateful to him for.
Rufus Norris, the current NT director, said: We all stand on the shoulders of giants and Peter Halls shoulders supported the entirety of British theatre as we know it. All of us, including those in the new generation of theatre-makers not immediately touched by his influence, are in his debt. His legendary tenacity and vision created an extraordinary and lasting legacy for us all.
Halls prolific work as theatre director included the world premiere of John Bartons nine-hour epic Tantalus in 2000 and the London and Broadway premieres of Alan Ayckbourns Bedroom Farce in 1977.
Other landmark productions included Hamlet (1965) with David Warner, Antony and Cleopatra (1987) with Judi Dench and Anthony Hopkins, The Merchant of Venice (1989) with Dustin Hoffman, As You Like It (2003) with his daughter Rebecca Hall, and A Midsummer Nights Dream (2010) with Judi Dench. His last production at the National Theatre was Twelfth Night in 2011.
He was also an internationally renowned opera director, and was artistic director of Glyndebourne from 1984 to 1990, directing more than 20 productions. He worked at many of the worlds leading houses, including the Royal Opera, the Metropolitan Opera and Bayreuth where, in 1983, he staged Wagners Ring Cycle to mark the 100th anniversary of the composers death.
He is survived by his wife, Nicki, and children Christopher, Jennifer, Edward, Lucy, Rebecca and Emma, and nine grandchildren. His former wives, Leslie Caron, Jacqueline Taylor and Maria Ewing, also survive him. There will be a private family funeral and details of a memorial service will be announced at a later date.
The actor Toby Stephens wrote on Twitter: So sad to hear of the death of Sir Peter Hall. He gave me my first break as an actor. A great director and shaper of British theatre.
Other actors tweeted their gratitude to and respect for Hall. Patrick Stewart wrote: He transformed classical and modern UK theatre and gave me a career.
Laurence Fox tweeted a picture of himself with Hall, Halls daughter Rebecca and Brenda Blethyn. 
He gave me my first theatre job, and boy did he whip you into shape...Rest in peace Sir Peter. #PeterHall pic.twitter.com/FmSDI6clHd
The executive chairman of Glyndebourne, Gus Christie, hailed Halls productions as timeless. 
The playwright Sir David Hare said: Every living playwright owes him a debt. 
