__HTTP_STATUS_CODE
200
__TITLE

Charity worker stabbed to death in west London

__AUTHORS

Nadia Khomami and 
Vikram Dodd
__TIMESTAMP

Tuesday 17 October 2017 11.45EDT

__ARTICLE
A murder investigation has been launched after a 28-year-old charity worker was stabbed to death in west London just metres from his home.
Abdul Samad, an economics graduate from London, was attacked in the street by assailants on mopeds, who knifed him in the heart in an attempt to steal his mobile phone.
His family said Samad managed to reach the front door of his block and buzz for help, at which point they found him on the ground, covered in blood.
He was taken to St Marys hospital in Paddington but was pronounced dead an hour later. Police said a 17-year-old male and an 18-year-old male were arrested on suspicion of murder following the incident.
Samad worked for the Dragon Hall Trust, which helps young people develop their computer skills. 
Paying tribute, his brother, Abdul Ahad, told the Evening Standard: He loved helping children. Thats the sort of person he was. This is a young life gone and such a lovely life. He was full of energy and loved the kids he worked with. Youre not going to find a nicer person. It was over a phone, its just so senseless.
He came to the door here and buzzed and was covered in blood. The knife went through his heart. He pressed the buzzer and my mum and dad came down and he was lying there on the floor. My mum then called me and said, Your brother is dying. When I got there he looked half dead already.
Ahad said his brother had never done anything wrong and had never been in trouble with the police. He was geeky and loved his computers and thats why he was helping the young people, he said. I could understand if it was some kind of postcode war but hes not that type of person, his friends are all geeks.
He was born in St Marys hospital and hes died there. We want these people to go to jail and never come out.
Nicole Furre, director at Dragon Hall, said that Samad was very talented and a key part of a close team. He was great with children and young people and really explained things well, she said. He managed to even make coding exciting.
Samads parents arrived in the UK from Bangladesh in the 1960s to give their children a better life, Ahad said. He was due to marry his girlfriend next year.
It is understood he was at a friends house playing computer games before leaving, and the stabbing occurred at about 11.45pm.
The stabbing has shocked police chiefs tackling a surge in moped crime. The level of violence used is alarming and concerning. The violence was unprovoked, said one source.
 The investigation is being led by Scotland Yards homicide command and extra patrols are planned in the area to reassure local people.
 Police believe the level of violence used was unusual for thieves on mopeds. Their usual method is to rely on surprise and speed to snatch a phone, carrying out a spate of thefts to maximise their haul and profits.
 The Met said in a statement: Police were called by the London Ambulance Service (LAS) at 23:45hrs on Monday 16 October to a report of a stabbing outside Fleming Court in St Marys Terrace, W2.
 Officers attended along with the LAS and found a 28-year-old man suffering from stab injuries. He was taken to a central London hospital where he was pronounced dead at 00:52hrs the following morning. At this early stage, police believe the suspects are two males who were riding a moped.
 An 18-year-old male and a 17-year-old male were arrested on suspicion of murder near to the scene in the early hours of Tuesday 17 October. Both have been taken to north London police stations where they remain in custody.
The stabbing came hours after a 20-year-old man was chased down and killed by a knifeman on a moped in a triple stabbing outside Parsons Green underground station.
According to witnesses, the young man was treated by medics but died at the scene in front of family members who rushed to his aid. The other two victims were taken to hospital with stab wounds.
 Police do not believe there was any link between the latest stabbing and the murder in Parsons Green.
