__HTTP_STATUS_CODE
200
__TITLE

Crossword roundup: DJs hiding in the corners

__AUTHORS

Alan Connor
__TIMESTAMP

Monday 11 September 2017 08.19EDT

__ARTICLE
A tweet of delight from Classic FMs Tim Lihoreau:
The solution to yesterday's @guardian crossword, set by @enigmatistelgar Check out the letters upwards on the left. #allMyChristmases pic.twitter.com/kjgSku1CFg
Lihoreau is not the only presenter hiding in the margins of this exacting Guardian puzzle, and its clues resemble a playlist on a particularly good day for the station. And if youre thinking, I solved a puzzle with a Classic FM theme, but Im pretty certain it wasnt in the Guardian, perhaps youve had a crack at the same days Financial Times puzzle, where the answers include 
16ac Civilisation divided, decoy blurring boundaries (7)[ synonym for divided + synonym for decoy, swapping the letters where they meet (blurring boundaries) ][ CUT + LURE with the T and L swapped ]
 19ac Maybe Arsenals big striker? (4)[ name for an item used for striking]
... CULTURE above CLUB, Culture Club being Charlotte Greens show  and where most of the rest of the schedule lurks elsewhere in the grid. 
Both puzzles, by the way, are from the setter known locally as Enigmatist, whose Q&A is here, and who has just published his 120th Telegraph Toughie puzzle with a truly hidden theme that I shant spoil here. And if you in fact solved both those puzzles marking Classic FMs silver jubilee, and still feel there should be more, Im happy to direct you to Micro Cryptic XWords: 
Cryptic crossword with a Classic FM theme! #classicfm #classicalmusic #radio3 #bbcradio3 #crossword #crosswords #crypticxword pic.twitter.com/FBQpOuzOnQ
Of the many indignities suffered by the Tory grassroots movement Activate  from leaks of genocidal banter to poignant passwords  surely the greatest is not making it in to Tees recent witty topical puzzle in the Independent. After all, every other schism and column is there, from ...
25ac Dirty work identifies party faction (4,6)[ synonym for dirty + synonym for work ]
... BLUE LABOUR to their cousins ...
 8d Bloody judge imprisoning old Nick Timothy? (3,4)[ synonym for bloody, then synonym for judge containing abbrev. for old ][ RED, then TRY containing O ]
... who identify as RED TORY. Perhaps its down to us to remedy this obloquy. Reader, how would you clue ACTIVATE?
Thanks for your clues for UNRIDICULOUS. Steverans cautious Sensible Dublin curious about British pulling out was countered by JollySwagmans verdict: Barniers version of Article 50 is entertaining: Free? Ive calculated at the outset you owe us. Surely thats only sensible.
The runners-up are Alberyalberys ominous Using logical reasoning improperly I could ruin us? and Schroducks meteorological Cumulonimbus swirled around, released many mighty bolts, fronts occluded  thats serious; the winner is Chrisbeees ludic Absurdly ludicrous? Primarily not, I understand.
Kludos to Chris; please leave this fortnights entries and your pick of the broadsheet cryptics below.
The down clues in Julius Financial Times puzzle end with BIG and start with FRIENDLY  among the many extra hints pushing the solver towards the key answer ...
4d/13d/21d Hes chalked up extraordinary recent gains hedging Swedish celebs (5,5,5)[ anagram (extraordinary) of RECENTGAINS, surrounding (hedging) some famous Swedes ][ CERNESGIANT surrounding ABBA ]
... CERNE ABBAS GIANT. Delumptious, as they say.
