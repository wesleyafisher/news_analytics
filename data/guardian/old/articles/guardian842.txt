__HTTP_STATUS_CODE
200
__TITLE

How our visual memories are made

__AUTHORS

Mark Cousins
__TIMESTAMP

Sunday 15 October 2017 01.00EDT

__ARTICLE
We have work lives and love lives, but we also have looking lives. If were lucky enough to have eyesight, an inner photo album accrues throughout our lives. On its pages are the sunsets weve seen, the dead bodies, and many other defining images  these are the visual shocks and pleasures which help us understand and read emotion.
In a refugee camp in Calais last year, I played football with a young teenage boy from Syria. We had no common language, but we had a laugh as we played. Afterwards I wondered what he had seen in his life so far.
Before he left Syria its likely he would have been an eyewitness to the destruction of buildings around him and, alas, of lives, too. Our immediate worlds, the things, people, animals and buildings around us, are the first act in the story of our looking lives. This boys first act is likely to have been one of visible rubble and rage.
Then he went on the road, north towards countries  Germany or Sweden  that he will never have seen in real life. When I was in Sarajevo during its siege, and even when I went on a much less difficult trip to China, I noticed how it was the unfamiliar things  buildings, faces, clothes, threats  that popped up visually. I can only begin to imagine my football friends forward tracking shot up through the Balkans.
There are two different streams in our visual cortex: one has a what function, the other tells you where that object is
Did the Alps excite him or did they just look like a vast wall between him and safety? Did he get close to any of the monumental non-Arab cities of Europe, such as Vienna or Paris? Did their stucco or baroque buildings look beautiful to him, or excessive? Did the visual experience calm him, as horizons often do for travellers, or did it make him realise that he had miles to go before he slept?
There seem to be two different streams within our brains rear visual cortex. The first, the ventral stream, assesses objects. Its the what function, and tells you, if you are on a tennis court, that this is a ball barrelling towards you. The second, the dorsal stream, assesses the position and movement of the ball, the where function.
As the young Syrian and I played, both of these streams were working, but his will have been tested in a far more serious way than mine. His visual encounter with Europe will have been unfamiliar. Embedded within the panorama before his eyes, there will have been people who wanted to cause him harm, or verbally abuse him. Tired but adrenalised and therefore in fight-or-flight mode, he will have been doing ventral and dorsal visual scans of his immediate environment, looking out for oncomers, or faces that are unwelcoming or aggressive.
We all do this to a certain extent when we are walking home at night and there are strangers around. I dont know what happened to him  he was probably sent to somewhere in rural France, or backwards, down the line, like a movie in rewind. But if this refugee did find refuge, he will I hope have discovered another aspect of looking  its consolation.
If he is safe, his sense of each will derive in part from what he sees: a place to live, modest comforts, food, hospitable people, FaceTime conversations with people back home, football, television.
The story of his looking will have been one about the immediate world, encounters and consolation. His inner photo album will already be more dramatic than most of ours.
The Story of Looking by Mark Cousins is published by Canongate on 19 October at 25. Order a copy for 21.25 at bookshop.theguardian.com
