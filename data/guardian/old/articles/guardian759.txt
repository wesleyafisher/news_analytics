__HTTP_STATUS_CODE
200
__TITLE

Meera Sodhas recipe for vegan mouth-numbing noodles with chilli oil and red cabbage

__AUTHORS

Meera Sodha
__TIMESTAMP

Saturday 30 September 2017 04.29EDT

__ARTICLE
We need to talk about heat  the kind that comes from fresh chillies and peppercorns, and also from ginger, horseradish or too much garlic. Each plays a different, nuanced role, but our undeveloped language when talking about discrete types of heat mean theyre usually described in a one-size-fits-all way, as simply mild, medium or hot.
Understanding the various types of heat that various ingredients offer is an important skill for any cook, and can make the difference between a successful meal or slow, culinary torture. Some heat hits you smack at the top of your head, as wasabi and mustard do, and feels as though you need a whales blowhole for it ever to escape. Other heat, such as that of ground Kashmiri chilli or black pepper, has a gentle, rounded flavour that builds gently to make you feel as though youre carrying a hot-water bottle in your stomach.
Birds-eye chillies are the ones to watch out for: unless combined with a heady dose of sugar and lime, as they are in Thai cooking, they can unleash an unholy hell in your mouth that lifts you out of your pants. My favourites are those slim, green Indian finger chillies that have just enough buzz to tickle you behind the ears.
The hero of todays recipe is the Sichuan peppercorn. Muddy pink in colour, smelling of grapefruit and lending an unusual citrus flavour to whatever they touch, they create a unique, lip-tingling and mouth-numbing sensation that isnt at all hot. In fact, theyre so gentle that, in Sichuan cooking, chilli flakes are often added to pep them up.
This is an elegant dish that wont leave you on your knees. The noodles are coated in peanut butter and mixed with as little or as much chilli pepper oil as you like, all freshened up with some gently pickled red cabbage. Youll need a blender or electric spice grinder. Any leftover chilli oil can be kept in the fridge. Serves four.
For the cabbage pickle220g red cabbage (ie, about  cabbage), cored and finely shredded  cucumber, seeds removed and sliced into thin strips1 handful mint leaves, roughly chopped4 tbsp rice vinegar1 tbsp toasted sesame oilSalt1 tbsp sesame seeds
For the mouth-numbing chilli oil
2 tbsp Sichuan peppercorns4 tbsp dried chilli flakes6 tbsp rapeseed oil4 garlic cloves, finely chopped1 tsp caster sugar
For the noodles4 tbsp crunchy peanut butter4 tbsp sesame oil400g dried wheat noodles 
Put the cabbage, cucumber and mint in a bowl, add the vinegar, sesame oil and a pinch of salt, and mix with your hands. Scatter with sesame seeds and leave to one side.
In a blender or spice grinder, blitz the peppercorns and chilli flakes to a rough powder. Heat the oil on a medium flame, then add the garlic and let it sizzle for a minute or two, until it turns a pale gold. Stir in the sugar, half a teaspoon of salt and the ground peppercorn and chilli mixture, and take off the heat.
In a bowl, mix the peanut butter and sesame oil for the noodles with a third of a teaspoon of salt. Cook the noodles according to the packet instructions, then drain, rinse under cold water and drain again.
Put the noodles in a bowl and add two tablespoons of chilli oil and the peanut mixture. Toss with your hands, making sure all the noodles are well coated, then season to taste.
To serve, lift the noodles on to a platter (or four shallow bowls), top with a handful of the sharp crunchy cabbage, then add a drizzle of chilli oil, depending on your threshold for mouth-numbing heat.
