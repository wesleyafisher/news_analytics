__HTTP_STATUS_CODE
200
__TITLE

John Carpenter: 'Could I succeed if I started today? No. Id be rejected'

__AUTHORS
Interview by 
Lanre Bakare
__TIMESTAMP

Tuesday 10 October 2017 05.00EDT

__ARTICLE
Hi John. What have you been up to?
Ive been playing the video game Destiny 2. Its a sci-fi game. Its fun.
Ive heard of that. Isnt it really hard and involves shooting aliens?
Oh, it is hard. Thats why Ive dedicated my life to learning how to play it. At my age why not? It keeps me out of trouble.
Indeed. You recently said  there arent a lot of things that scare John Carpenter, but [playing music live] is one of them. Does the thought of your upcoming tour scare you?
Its not that frightening now. I overcame my stage fright. That started in the 60s when I was doing a play in high school. I got up onstage and I forgot my lines in front of the entire school. Thats what my original scar was, but Im doing much better now.
Are you going to play any music from your 80s synth-pop group the Coupe De Villes?
[Laughs] Well, no, I dont think so. Were promoting my Anthology album, which is music from my career from the 70s to now. Well be playing that mostly and a couple of tunes from the Lost Themes albums.
Is it true you had a John Wayne bumper sticker on your car in the 70s?
I did. I had it to irritate my friends. I really loved John Wayne as an actor; his politics were not mine, but as an actor I grew up with him. He was our symbol of masculinity. I love irritating my friends. Its a bad thing to do.
Youve described your scores as being like carpet. Does that mean that theyre soft, luxurious and comforting like a shag pile?
Well, what I do in a movie is carpet the scene so that you watch them and my music supports the sequences  so its like a carpet in that sense. I come in and Im like a guy who carpets your house. I put down carpet on the floor, and you walk across it and its very comfortable.

Is it true you never watch your old films?
Oh God, no. Dont ever make me do that.
But your films are widely viewed as all-time classics, John.
I dont want to see them again. I see the mistakes. Thats all I can see. Itd be torture. Are you kidding? I dont want anything to do with them after Im done.
The horror mogul Jason Blum recently gave us his rules for the genre, which were essentially a John Carpenter template. Why do you think your approach is still the go-to for new horror directors?
I have no clue. My attempts at self-evaluation are doomed to fail because Im terrible at it. All I do is go off instinct. I dont have the rules that Jason Blum has. Hes much smarter than I am. Im just a poor director trying to get by in this terrible world.
I dont miss working. Working is hard because you have to get up in the morning.
Do you miss the low-budget days of Dark Star and Halloween?
No. I dont miss working. Working is hard because you have to get up in the morning. I just did a music video for the single from our new album Christine. I had to work all night and, my God, I forgot how rough that is. Stay up all night? Wow.
Robert Rodriguez is set to remake Escape From New York and theres another Halloween project coming in 2018. How do you feel about people remaking your films?
I love it, if they are going to pay me money. If they pay me, its wonderful. If they dont pay me, I dont care. I think its unfair if they dont pay me. I think everyone should pay me. Why not? Im an old guy now and I need money. Send me money.
Youve spoken about how contemporary film-makers give up creative control and the final cut too easily. Would you like to be starting out now?
It was such a different time back then and the kind of movies were different. I started when you could actually make an exploitation film, a low-budget exploitation movie and get it into theaters. Nowadays its so ridiculously expensive. Could I succeed if I started today? Probably not. Id be rejected.
You grew up a huge fan of comic books. Do you think the modern adaptations do them justice?
The older I get, the less interested I am, because theyre movies about things that cant happen. I just dont care.
Youve said that Horrors always been the same. Its flexible. It changes with societal changes. What do you think todays horror films say about contemporary America?
There are a lot of different things. Horror films have modernised along with our culture  Get Out, for example. Some genres can die, but I dont think horror ever will.
What did you think when you first heard neo-Nazis were trying to claim They Live was about Jewish media control?
It happened on my Twitter feed where these idiots came along. I just thought: Youre kidding me? I thought about it and I can see why they think that. It isnt the intention. That whole thing is just the biggest lie. Its really terrible that thats happened. What a world we live in.
You recently paid your respects to Harry Dean Stanton. What made him so special?
He was an accomplished character actor and a unique man. He would come absolutely prepared for his part and ad-lib things that werent in the script, but which were hilarious. He did that especially on Escape From New York, he made that character his own. He saw that character as a sort of poet, like Lord Byron. I hadnt thought of it, but it was great.
Anthology: Movie Themes 1974-1998 is out on Sacred Bones from 20 October. Carpenter starts a US tour on 29 October
