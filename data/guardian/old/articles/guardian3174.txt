__HTTP_STATUS_CODE
200
__TITLE

Local government innovators recognised on Guardian Public Service Awards shortlist

__AUTHORS

Emma Sheppard
__TIMESTAMP

Wednesday 27 September 2017 03.19EDT

__ARTICLE
Nine local government organisations have made the shortlist for the Public Service Awards 2017, which aim to reward the best teams and projects across public service in the UK. 
In all, 24 organisations and people have been shortlisted across central and local government as well as the housing, health, social care and voluntary sectors, with councils making a strong showing on the shortlist.
In the care category, Hertfordshire county council has been nominated for its family safeguarding service, which has created multi-disciplinary teams to tackle domestic abuse, substance abuse and mental health problems, partnering with police, health and probation services. 
Warwickshire county council has been shortlisted in the finance category for its franchising of recycling centre reuse shops, diverting 700 tonnes of goods into a social enterprise that now generates 1m each year. Also on the shortlist for its innovative approach to finance is the London housing directors group for its pan-London accommodation rates project that ran from 2016-17.
Up for the health and wellbeing award is the Dragons Den wellbeing challenge group organised by the Vale of Glamorgan council, which asked its employees to pitch low-cost ways to improve community wellbeing. Four initiatives, out of 25 submissions, have subsequently been launched. 
In learning and development, two Welsh councils  Carmarthenshire county council and the city and council of Swansea  have been shortlisted. Carmarthenshire was shortlisted for its Dewis Sir Gar project that retrained 35 staff to deliver a 24-hour information, advice and assistance service, while Swansea has trained school staff to help officers assist vulnerable families earlier with parenting, mental health and behavioural issues. 
The city of Wolverhampton council and Lincolnshire county council have both been nominated for their approaches to recruitment and HR. In a bid to address the skills gaps in Wolverhampton for specialist roles, the council has redesigned its online system, provided more guidance for hiring managers, and introduced apprenticeships. In Lincolnshire, social worker recruitment policy was changed to focus on possibility, progression and change, and targeting specific groups via social media. 
 And finally, Paul Allen, team manager at Manchester city council, is one of three contenders shortlisted for the leadership excellence award. Allen is described by his colleagues as an inspirational and passionate leader, who keeps children and families at the heart of what he does. The other two shortlisted contenders in this category are Dr Fiona Jenkins, executive director of therapies and health science at Cardiff and Vale University health board, and Peter Sandiford, chief executive of PAC-UK. 
Voting is now also open for the Public Servant award, which will be decided by the public. Voting closes at midnight on 9 October.
 
The winners and runners up will be announced at our awards ceremony on 28 November. 
Sign up for your free Guardian Public Leaders newsletter with comment and sector views sent direct to you every month. Follow us: @Guardianpublic.
Looking for a job in central or local government, or need to recruit public service staff? Take a look at Guardian Jobs.
