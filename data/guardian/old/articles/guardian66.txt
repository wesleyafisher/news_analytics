__HTTP_STATUS_CODE
200
__TITLE

Manchester City rely on early goals to overcome Napoli in Champions League

__AUTHORS

Barney Ronay at the Etihad Stadium
__TIMESTAMP

Tuesday 17 October 2017 16.46EDT

__ARTICLE
Manchester City extended their perfect record on an edgy, absorbing night here that saw City purring up through the attacking gears before finding themselves reeled in by fast-breaking opponents Pep Guardiola described as one of the best teams I have faced in professional football.
Goals from Raheem Sterling and Gabriel Jesus inside the first quarter of an hour had suggested another free-scoring sky blue romp. But by the end Napoli could feel hard done by in a game where the score really could have been anything and where a missed first-half penalty by Dries Mertens proved crucial.
I knew before we played and now I know again this [Napoli] is one of the best teams in Europe, Guadiola said. Its one of my proudest games. I know against which team we won. Teams that are at that level you cannot beat easily.
The game had a crackle of expectation around it from the start. At times watching City rack up 39 goals before the end of October has felt a bit like stumbling across some wild inter-village tomato throwing contest, an autumn glut, all juice and drooling excess. They faced another bunch of dashers. Top of Serie A with eight wins out of eight, Napoli have set off like a train, revelling in their own slick Sarriball, a direct, hard-running style that promised plenty of space in behind.
They made two changes here, Piotr Zielinski and the excellent Amadou Diawara coming into midfield. For City Sergio Agero was on the bench, with another outing for that excitingly youthful Sterling-Jesus-San front three.
The early moments were a little scrappy. Fabian Delph continued his funky left-back variations, tucking into midfield, then breaking wide and it was down that side that Citys opening goal came out of the blue on nine minutes. Fernandinho played a hard flat cross-field ball out to Leroy San. He fed David Silva, whose cut-back was dummied by Jesus, shanked by Kyle Walker and then spanked into the net by Sterling.
It was Sterlings sixth goal in the past eight games, albeit in a team that just cannot stop burping them out right now. With 13 minutes gone it was 2-0. City were helped by some poor defending from Ral Albiol, who headed the ball straight into the onrushing press, then fell over trying to match Jesuss spin and dart behind him. Kevin De Bruynes low cross was millimetre-perfect, the finish a formality.
Rack em up. That made it five goals in seven days for Jesus and three assists in 80 minutes for De Bruyne. There could have been more. San laid the ball back for De Bruyne to smack a shot on to the underside of the bar. Jesus had a shot cleared off the line. On the touchline Maurizio Sarri, the Napoli manager, capered about, tracksuit jacket sleeves yanked up in exasperation as De Bruyne and the front three simply ran through his midfield and defence.
At which point City seemed to lose a little intensity. Napoli began to pick some holes of their own, Jos Callejn and Lorenzo Insigne finding space down the sides of the defence. With 37 minutes gone Walker pulled Albiol down inside the penalty area. Mertens plonked the kick straight at Ederson who saved with his legs, Fernandinho lunging in with great presence of mind to clear the rebound.
Napoli began the second half like a team that had been slapped around the chops and cleared its head, Marek Hamsik seeing more of the ball and both full-backs pushing high up the pitch. De Bruyne continued to drive City on, carrying around with him that personal pocket of space he unfurls wherever he goes like a fine green rug. It was in defence they looked a little open.
Guardiola defended Citys tactic of playing out from the back, perilously at times, as Napoli pressed. We have to do it more, he said. You play long balls against that team in two seconds they are attacking. In football thats how fast it goes. So you have to play that way.
It is a tactic that carries its own risk. With 65 minutes gone an extended passage of keep-ball ended with Napoli stealing possession and John Stones blocking Hamsiks shot at full stretch.
Napoli finally pulled one back six minutes later, Diawara beating Ederson easily from the spot after Fernandinho had tripped Faouzi Ghoulam.
Both teams might have scored again. City perhaps missed a little Agero-style razor edge near goal. With nine points from their three games they will expect to cruise the rest of this group. The trip to Napoli in two weeks time will provide a little more information about this evolving team.
