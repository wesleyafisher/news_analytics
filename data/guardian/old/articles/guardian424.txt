__HTTP_STATUS_CODE
200
__TITLE

Cavaliers send Kyrie Irving to Celtics for Isaiah Thomas in blockbuster trade

__AUTHORS
Associated Press
__TIMESTAMP

Tuesday 22 August 2017 21.24EDT

__ARTICLE
Kyrie Irving was tired of being teammates with LeBron James.
Now he has to figure out how to beat him.
Clevelands All-Star guard, who asked owner Dan Gilbert to trade him earlier this summer, was dealt Tuesday night to the Boston Celtics in exchange for star guard Isaiah Thomas, forward Jae Crowder, center Ante Zizic and a 2018 first-round draft pick.
Irving, whose late three-pointer helped Cleveland win the 2016 NBA championship  and the citys first title since  is on his way to Boston, where hell join a Celtics team that lost to the Cavs in last seasons conference finals.
And as fate will have it, the Cavs will host the Celtics in their season opener on 17 October.
Kyrie is one of the best scorers in the NBA, Celtics president Danny Ainge said. He has proven that on the biggest stage, the NBA finals, the last three years. Hes been an NBA champion, an Olympic gold medalist, and a four-time All-Star. For all hes accomplished, we think his best years are ahead of him.
The blockbuster deal caps a wild summer for the Cavs, who lost their title defense in five games to Golden State in June. Since then, general manager David Giffin left, the team failed to convince Chauncey Billups to join its front office and James has played with fans emotions with veiled postings on social media about his future.
Irvings trade demand cast a dark shadow over the entire organization.
But Cleveland may have salvaged its offseason with this trade.
In Thomas, theyre getting a proven playmaker with a stellar reputation. The 5ft 9in guard is one of the leagues most dynamic backcourt players with an uncanny ability to get to the basket. Irving may be the only better finisher among point guards.
The 28-year-old Thomas was taken with the final pick in the second round in 2011, but he has steadily scaled his way up to elite status.
His reputation in Boston was cemented when he led the Celtics through the playoffs last season despite the death of his sister on the eve of the postseason. He also had a front tooth knocked out during the second-round series against Washington and a hip injury eventually forced the team to shut him down early in the East finals, won by the Cavaliers in five games.
Isaiah embodied what it meant to be a Celtic, Ainge said. He captured fans hearts not only with his spirit, but his personality. Jaes toughness was contagious for our team. He improved his skills each year, but its his energy and fight that will be remembered. We wish them and their families the very best.
Thomas is eligible for free agency next summer and believes he is worthy of a maximum contract. He has been quoted saying, They better bring out the Brinks truck.
Cleveland also is getting Crowder, a solid perimeter defender, and a first-round pick that Boston got from Brooklyn. The package could help the Cavs reload if James opts out of his contract next summer and leaves Cleveland for a second time.
The Cavs drafted Irving with the No1 overall pick in 2011. He struggled in his first few seasons but blossomed in recent years alongside James. However, that didnt seem to be enough for the 25-year-old, who has wanted to be the prime player and focal point on his own team.
Minnesota showed some interest in Irving after his trade request became public, but were unwilling to part with young star Andrew Wiggins as the centerpiece of a deal. Wiggins is expected to sign a max contract extension in the coming weeks to stay with the Timberwolves.
The Phoenix Suns reportedly were not interested in parting with rookie Josh Jackson in a deal that also would have required Eric Bledsoe and the New York Knicks gave no indication that Kristaps Porzingis was ever on the table in a potential Irving trade.
Then the Celtics swooped in, landing the kind of star Ainge has long coveted. With the possibility of having to break the bank to retain Thomas next summer, Boston finally tapped into the treasure trove of assets that Ainge has assembled to get Irving, who is under contract for two more years.
Irving got his wish to get away from James, but hes not free of him yet.
