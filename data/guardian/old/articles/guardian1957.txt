__HTTP_STATUS_CODE
200
__TITLE

Nick Kyrgios: Don't put me in same box as 'lost' Bernard Tomic

__AUTHORS
Australian Associated Press
__TIMESTAMP

Thursday 14 September 2017 22.10EDT

__ARTICLE
Nick Kyrgios says Bernard Tomic has lost his way and that its unfair to lump him in the same category as Australias fallen tennis star.
As he prepares to lead Australia into the Davis Cup semi-final with Belgium in Brussels, Kyrgios has opened up about his relationship with Tomic, who remains in exile with former team-mates after a disastrous 2017 campaign.
The two former junior grand slam champions were jostling for Australias top ranking last year before Tomic tumbled to 146th in the world and Kyrgios soared into the top 15 for the first time this season.
While the enigmatic Kyrgios admits hes still not the professional tennis needs me to be, the 22-year-old insists Tomic is an even greater conundrum.
Youd also be wrong if you tried to lump me in the same category as Bernard Tomic, as [Rio Olympics Australian chef de mission] Kitty Chiller and tons of others have over time, Kyrgios said.
Bernie has lost his way. We were pretty good mates when I was younger. I obviously didnt know the tennis tour too well back then and we were guys of similar age, representing the same country, on the road at many of the same tournaments.
But a lot has changed since then. He needs to figure out what he wants to do. I cant relate to anything he says anymore. He says one thing and he does the other. And he contradicts himself all the time.
He says tennis doesnt make him happy, that he doesnt really like the game, yet he says the only thing that will really make him happy is winning a grand slam. It doesnt make sense at all.
Kyrgios, who was banned from the ATP Tour last year for not giving his best efforts in a lame loss at the Shanghai Masters, concedes hes not perfect, either. But hes trying to get it together under the the Davis Cup leadership of trusted mentor Lleyton Hewitt.
When Im in the right frame of mind, I feel unbeatable, he said, writing for sports storytelling platform playersvoice.com.au. That period this year where I beat Novak Djokovic a couple of times in a row in Acapulco and Indian Wells? Yep. Unbeatable.
Its easy to get up for a match like like that  big-name opponent, centre court, huge challenge. I love that. Its against the lower-ranked guys on the back courts where I cant get it together and tank.
Obviously, my grand slam season has been terrible. I meant what I said straight after losing to John Millman in New York. Maybe its time for [coach] Sebastian Grosjean to work with someone more dedicated than me. I dont know.
