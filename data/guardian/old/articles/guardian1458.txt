__HTTP_STATUS_CODE
200
__TITLE

Trump threatens to rip up Iran nuclear deal unless US and allies fix 'serious flaws'

__AUTHORS

Julian Borger in Washington, 
Saeed Kamali Dehghan in London and 
Peter Beaumont in Jerusalem
__TIMESTAMP

Friday 13 October 2017 16.44EDT

__ARTICLE
Donald Trump has threatened to terminate the 2015 Iran nuclear deal if Congress and US allies fail to amend the agreement in significant ways.
In a vituperative speech on Friday that began by listing Irans alleged crimes over the decades, Trump announced he would not continue to certify the agreement to Congress, but stopped short of immediately cancelling US participation in the deal.
Based on the factual record I have put forward, I am announcing today that we cannot and will not make this certification. We will not continue down a path whose predictable conclusion is more violence, more terror and the very real threat of Irans nuclear breakout, Trump said at the White House.
Trump put the onus on Congress and US allies to agree to means to toughen the conditions on Iran  and to make restriction on the countrys nuclear programme permanent. He made clear that if those negotiations fail to reach a solution  which is almost certain  he would unilaterally pull the US out of the international agreement, a move likely to lead to a return to nuclear confrontation in the Middle East.
In the event we are not able to reach a solution working with Congress and our allies, then the agreement will be terminated, Trump said. It is under continuous review and our participation can be cancelled by me, as president, at any time.
The president also announced he had ordered the US Treasury to impose new sanctions on Irans Islamic Revolutionary Guard Corps (IRGC) as a backer of terrorist groups in the region, although the state department did not designate the IRGC as a terrorist group itself. 
The international backlash to Trumps speech was immediate. The leaders of the UK, France and Germany  also signatories of the nuclear deal  issued a statement vowing their commitment to the agreement.
The EU foreign policy chief, Federica Mogherini, insisted that the agreement, formally known as the Joint Comprehensive Plan of Action (JCPOA) was working, and that no single country or leader could terminate it.
The president of the United States has many powers, but not this one, Mogherini told reporters in Brussels.
Yukiya Amano, the head of the International Atomic Energy Agency (IAEA), the UNs nuclear watchdog, issued a statement restating the agencys finding that Iran was abiding by its obligations.
Within minutes of Trumps speech, Irans president, Hassan Rouhani, went live on state television.
He said: What we heard tonight was a repeat of the same baseless accusations and insults that weve heard over the past 40 years. It had nothing new; we werent surprised because for 40 years weve got used to these words. With your baseless speech you made our people more united.
Rouhani went on: Today, the US is more isolated than ever against the nuclear deal, [more] isolated than any other time in its plots against people of Iran.
The Iranian president shrugged off Trumps call for constraints on Irans ballistic missile programme.
Our missile and defence activities have always been important to us for our defence, and today its more important, Rouhani said. We have always made efforts to produce weapons that we need, and from now on we will double our efforts. These weapons are for our defence and we will continue strengthening our defence capabilities.
Trump received rapid support, meanwhile, from Israel and Saudi Arabia, who have emerged as his own major allies on the world stage.
Israels prime minister, Benjamin Netanyahu, said he wanted to congratulate President Trump for his courageous decision today and for boldly confront[ing] Irans terrorist regime.
For European diplomats seeking to salvage the JCPOA, the days leading up to Trumps long-awaited speech were a roller-coaster. Initially fearful that Trump could immediately trigger a possible collapse of the deal, the Europeans were buoyed when they were briefed that Trump would not call for the reimposition of sanctions by Congress. 
However, in the wake of the presidents speech on Friday, the JCPOAs survival looked tenuous.
In the speech, Trump declared: I am directing my administration to work closely with Congress and our allies to address the deals many serious flaws so the Iranian regime can never threaten the world with nuclear weapons.
He noted that congressional leaders were already drafting amendments to legislation that would include restrictions on ballistic missiles and make the curbs on Irans nuclear programme under the 2015 deal permanent, and to reimpose sanctions instantly if those restrictions were breached.
However, any such changes would need 60 votes in the US Senate to pass, and Democrats are high unlikely to give them their backing. Even if they did pass into law, the restrictions would represent a unilateral effort to change the accord that would not be acceptable to the other national signatories.
Hours earlier, the US secretary of state, Rex Tillerson had acknowledged that it was very unlikely that the JCPOA agreement could be change, but suggested that the issue of Irans ballistic missile programme and the time limits on some of the nuclear constraints in the deal, could be dealt with in a separate agreement that could exist alongside the JCPOA.
Trump, however, made no reference to such a way out of the looming impasse.
He appeared to go out of his way to goad Iran, even linking Tehran with al-Qaida and the attacks on US embassies in 1998. He referred to Tehran as a fanatical regime and a dictatorship. He even referred to the body of water almost universally known as the Persian Gulf as the Arabian Gulf.
How come a president has not yet learned the name of a famous gulf in the world, the same Persian Gulf that US vessels always pass through aimlessly? a riled Rouhani said in his response.
He needs to study geography, but also international law. How come an international agreement that is endorsed by a UN resolution, which is a UN document  how a US president can annul such an international document?
The exchange of insults mirrored Trumps continuing spat with the North Korean leader, Kim Jong-un, adding personal animus to already tense situations on opposite sides of the world.
