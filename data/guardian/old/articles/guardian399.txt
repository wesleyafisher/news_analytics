__HTTP_STATUS_CODE
200
__TITLE

Unstoppable Cleveland Indians set baseball record with 21st straight win

__AUTHORS
Associated Press in Cleveland
__TIMESTAMP

Wednesday 13 September 2017 15.26EDT

__ARTICLE
The Cleveland Indians won their 21st straight game on Wednesday, 5-3 over the Detroit Tigers, to set a AL winning-streak record and join only two other teams in the past 101 years to win as many consecutive games.
Jay Bruce hit a three-run homer off Buck Farmer as the Indians matched the 1935 Chicago Cubs for the second-longest streak since 1900. The run has put Cleveland within five wins of catching the 1916 New York Giants, who won 26 straight without a loss but whose century-old mark includes a tie.
The Indians havent lost in 20 days, and theyve rarely been challenged during a late-season run. However, they had to overcome a costly error and rely on their bullpen to hold off the Tigers, who closed within 4-3. Roberto Perez added a homer in the seventh and four Cleveland relievers finished, with Cody Allen getting his 27th save.
With the crowd of 29,346 standing and stomping, Allen retired Ian Kinsler for the final out, giving the Indians the leagues longest streak since the AL was founded in 1901.
