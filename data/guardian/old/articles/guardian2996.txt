__HTTP_STATUS_CODE
200
__TITLE

The Cors Country House, Laugharne, Carmarthenshire: hotel review

__AUTHORS

Robert Hull
__TIMESTAMP

Friday 29 September 2017 01.30EDT

__ARTICLE
Lovely Laugharne, on the Taf estuary, with its grand but dilapidated 12th-century castle and views over Carmarthen Bay is linked in verse and visitors to Dylan Thomas. On the way into town, we pass St Martins Church, where the poet is buried and Browns Hotel, where he guzzled and gossiped (and gave out the bars phone number as his own).
Theres more Thomas to come but for now we head to The Cors. A small stone bridge over the Corran leads into a large garden where, on a sunny autumn day, a Japanese pagoda tree dazzles in red and orange. Behind lies the Victorian country house, two storeys of shabby beauty that for over 20 years have been home to chef-patron Nick Priestlands fine-dining restaurant.
But the times they are a-changin, as another famous Dylan once observed, and Nick is now scaling back the restaurant in favour of providing accommodation in three artfully decorated, comfortable and well-appointed guest rooms.
Over afternoon tea, Nick tells my wife and me about his early artistic life in London  his own abstract paintings are dotted around the restaurant, bar and rooms  and the years he has now spent back in Laugharne, running the restaurant and hosting performers at the towns annual Laugharne Weekend festival.
I ask, to laughter from Nick, if its now all about the rooms.
Well, its supposed to be, but I have so many regulars Im still opening the restaurant Thursday to Saturday.
Hes also guilty of understatement when he tells me the rooms were tidied up a bit before being opened to guests. From the moment he shows us the garden-facing Blue Room its obvious he understands what it takes to create atmosphere. Shutters pull back to reveal windows on to the garden, while inside theres an old fireplace, lilac walls and objets dart providing plenty of character. Add a double bed thats soft and inviting and you have perfect short-break material.
Theres time before dinner for a look around Laugharne, with its pubs and restaurants (the pick of which is the Portreeve Tafarn), and a castle-and-waterside stroll that is bathed in magic-hour sunshine. We walk past Thomass writing shed to his home, now the Dylan Thomas Boathouse  a museum and tearoom with a terrace that has views over the estuary.
Over slices of bara brith in the tearoom we discuss the following days possible adventures: a walk around the bay and salt marshes; a trip to Pendine Sands (famous for its long beach and land speed record attempts in the 1920s); or seaside fun along the coast at Tenby.
That evening Nick is ready in the bar with wine, laughter and gossip. I forget he is also meant to be cooking. He hasnt but adds a caveat: My kitchen hero is Keith Floyd, so youll have to bear with. Ill get to it soon, though.
And indeed he does. Our starter of smoked haddock crme brule is creamy and has sizable chunks of fish, while a main of salt marsh lamb with dauphinoise potatoes is modern-European done with balance and finesse. Dessert brings lemon tart for me and sticky toffee pudding for my wife. It also brings contentment.
Its easy to see how Nicks cooking has earned him plaudits, and though some will miss out as he steps back from the restaurant, many well-fed, well-entertained guests will now get a chance to book in for the night and soak this up  at a leisurely pace for them and for Nick.
 Accommodation was provided by The Cors Country House, doubles from 90 B&B or 150 for dinner B&B, 01994 427 219, thecors.co.uk. For more information on the county visit discovercarmarthenshire.com
 Maggie James, arts co-ordinator, Carmarthenshire county council
 WalkThe coast and Carmarthen bay views are stunning on the Dylan Thomas Birthday Walk, inspired by his Poem In October. Its around two miles and mainly uphill.
 EatGrab a light lunch (of Welsh cheeses and local pies) at the Ferryman Deli in Laugharne. Or for fine dining, take a trip out to the Grove in Narberth.
 VisitYou have to see Laugharne Castle. Then head up the road to the Tin Shed Experience, a quaint 1940s museum with memorabilia, models and packaging, as well as occasional art exhibitions.
 ShopVisit the Gate gallery in St Clears, which offers a range of artisan Welsh crafts and gifts. Then pop in to Hughes the Butcher, the traditional butchers shop next door, for homemade sausages, pies and faggots.
