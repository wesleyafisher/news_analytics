__HTTP_STATUS_CODE
200
__TITLE

The Seagull review  Chekhov gets new wings

__AUTHORS

Susannah Clapp
__TIMESTAMP

Sunday 15 October 2017 02.50EDT

__ARTICLE
So Masha has become Marcia. She is not subjected to the most famous line in The Seagull: Why do you always wear black? There would be no point: in the 21st century, pretty much everyone wears black all the time. Nina appears in tiny shorts and trainers. Doctor Dorn is a gynaecologist. And when Irina (we are all on first-name terms here) wants to persuade her lover to stay, she backs him up against a wall for a handjob. Surely the first double-tissue emergency in Chekhov.
The indefatigable Simon Stephens  definitely the same as Heisenbergs author  has written an incisive 21st-century version of Chekhov. Sean Holmes, artistic director of the Lyric, has given it a vivid production. More full of anger than languor. Desperation and delight are more evident than in most Chekhov productions. Adelayo Adedayos Nina is completely without guile. When Trigorin looks at her, it is as if he has shone a flashlight on her face. Her wonder makes you fear for her future. 
At first Lesley Sharps Irina looks like a lovely comic turn, preening and mocking and artificially praising. In a nice touch Stephens gives her a line which twists on the seagull theme  and she flutters around as a chaffinch. But bring the question of age near her and her face darkens. Her plumminess slips into wry unposhness  she might be Sheila Hancocks younger sister  and into fear. In the final scene she is jangled and shrunken.
The Seagull is one of the best ever dramas about the theatre. Holmes underlines this by making even the scene changes into a shadow play. The keen edge of his production makes the parallels with Hamlet more evident: the disturbed young woman, the semi-incestuous mother  Mummy. In an arresting innovation characters frequently come forward, out of the action, to address the audience directly. The result is both startling and humorous. And never more urgent than when, as the aspirant young playwright, Brian Vernel steps towards the stalls to proclaim that theatre can be the most tedious, old-fashioned, prejudiced, elitist form. Not here it isnt.
 At the Lyric Hammersmith, London, until 4 November
