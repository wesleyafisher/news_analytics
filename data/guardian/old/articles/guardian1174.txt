__HTTP_STATUS_CODE
200
__TITLE

Indonesia's forest fires: everything you need to know

__AUTHORS

Oliver Balch
__TIMESTAMP

Wednesday 11 November 2015 10.37EST

__ARTICLE
As satellite data of the fire hotspots shows, forest fires have affected the length and breadth of Indonesia. Among the worst hit areas are southern Kalimantan (Borneo) and western Sumatra. The fires have been raging since July, with efforts to extinguish them hampered by seasonal dry conditions exacerbated by the El Nino effect. As well as Indonesia, the acrid haze from the fires is engulfing neighbouring Malaysia and Singapore and has reached as far as southern Thailand. 
 The most obvious damage is to the forest where the fires are occurring. Indonesias tropical forests represent some of the most diverse habitats on the planet. The current fire outbreak adds to decades of existing deforestation by palm oil, timber and other agribusiness operators, further imperilling endangered species such as the orangutan. 
The human cost is stark; 19 people have died and an estimated 500,000 cases of respiratory tract infections have been reported since the start of the fires. Its estimated that the fires could cause more than 100,000 premature deaths in the region.
Financial damage to the regions economy is still being counted, but the Indonesian governments own estimates suggest it could be as high as $47bn, a huge blow to the countrys economy. A World Bank study (pdf) on forest fires last year in Riau province estimated that they caused $935m of losses relating to lost agricultural productivity and trade. 
Forest fires have become a seasonal phenomenon in Indonesia. At the root of the problem is the practice of forest clearance known as slash and burn, where land is set on fire as a cheaper way to clear it for new planting. Peat soil, which characterises much of the affected areas, is highly flammable, causing localised fires to spread and making them difficult to stop. 
Its a blame game, with everyone pointing the finger at someone else. Environmental group WWF Indonesia, which has been highlighting the problem of Indonesias recurrent fires for years, says that the fires are caused by the collective negligence of companies, smallholders and government (which isnt investing sufficiently in preventative measures). 
Many blame big business. According to a recent analysis of World Resources Institute data in September, more than one third (37%) of the fires in Sumatra are occurring on pulpwood concessions. A good proportion of the rest are on or near land used by palm oil producers. Many of these fires are a direct result of the industrial manipulation of the landscape for plantation development, says Lindsey Allen, executive director of the conservation organisation Rainforest Action Network. 
In September, the Indonesian police arrested seven executives in connection with the fires, including a senior executive from Bumi Mekar Hijau (BMH), which supplied Jakarta-based paper giant Asia Pulp and Paper (APP). 
Others look away from the big corporations for blame. According to Henry Purnomo, professor at Indonesias Bogor Agricultural University and a scientist at research group CIFOR, there are two culprits: poor small-scale farmers looking to expand their farmland, and rogue operators intent on illegally clearing forests for land acquisition.
Global corporations operating in the area also blame smallholders and under-the-radar companies. The Roundtable on Sustainable Palm Oil, which counts many big palm oil businesses as members, has consistently said that the instances of fire on certified palm plantations in the affected region (which number 137) measure in single digits. Brendan May, chairman of sustainability advisory firm Robertsbridge (which has APP as a client), argues that its not in companies best interest to set fire to their own assets  an argument some campaign groups dispute. 

Ending the practice of slash and burn is vital. Companies  big and small - must be held to account, before the law and the market, if found culpable. 
Smallholders need assistance and incentives to pursue alternative, less harmful practices of forest management. Forest-dwelling communities often lack the skills and training, according to a recent CIFOR report. Meanwhile processors and buyers often fail to pay smallholders a fair price, something the signatories to the Sustainable Palm Oil Manifesto (which claims to go beyond RSPO certification standards) have pledged to correct.
Many big firms, such as palm producer Wilmar and timber giant APP, have signed zero-deforestation pledges in recent years. But the real test comes in pushing their commitments beyond the boundary fences of their plantations and down into their supply chains, where smaller firms operate with less public scrutiny. 
Many are calling on the Indonesian government to step up. WWF-Indonesias deputy director, Irwan Gunawan, says the government action lacks clout and has not yet resulted in deterrent effect to prevent any recurrence. 
Removing the culture of political patronage that protects private companies in Indonesia is essential, says Purnomo. A budgetary rethink is also required. At present, the ratio of public spending on fire suppression versus prevention is around 80:20, Purnomo says. 
Another major contribution to reducing future fires would be an up-to-date online, searchable land registry. Land tenure in Indonesia is often unknown or disputed, making it difficult to establish where responsibility lies. Coupling such a database with digital mapping technologies such as WRIs Global Forest Watch could make identifying the culprits a whole lot easier. One Map, a government-backed project to develop such a spatial mapping solution, is currently under development. 
