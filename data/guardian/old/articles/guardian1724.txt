__HTTP_STATUS_CODE
200
__TITLE

Did you solve it? The pain and pleasure of Japanese puzzles

__AUTHORS

Alex Bellos
__TIMESTAMP

Monday 9 October 2017 12.01EDT

__ARTICLE
In my column earlier today I set five examples of a new Japanese puzzle called Snake Place and we also played a re-run of the Nikoli Derby, where I asked you to submit a number, with the winner being the person submitting the lowest number that no one else also submits.
The solutions to Snake place can be seen here (on a printable page).
The results of the Nikoli Derby were fascinating. Last time 2560 people took part and the lowest number that only one person submitted was 69.
This time 1176 people took part and the lowest number submitted by only one person was ...5.
Congrats to that person. Sometimes it pays to aim low! And commiserations to the person who submitted the next lowest number that only one person submitted, which was 16.
(Someone submitted every number from 1 to 130, but I disqualified them because it is against the spirit of the competition, even though they obviously spent more time on it that anyone else.)
I find it fascinating to see the distribution of the numbers chosen. Just like last time, numbers ending in 7 are very popular. If anyone wants a full set of the numbers then please email me.
I hope you had fun. Ill be back in two weeks with a new puzzle.
My latest book is Puzzle Ninja: Pit Your Wits Against The Japanese Puzzle Masters. It contains more than 200 hand-crafted puzzles from Japan as well as profiles of prominent puzzle creators. You can buy it from the Guardian Bookshop for only 9.99 (rrp is 14.99), or from other outlets.
I set a puzzle here every two weeks on a Monday. Send me your email if you want me to alert you each time I post a new one. Im always on the look-out for great puzzles. If you would like to suggest one, email me.
