__HTTP_STATUS_CODE
200
__TITLE

Lagerfeld retains Coco Chanel strengths in Paris fashion week spectacle

__AUTHORS

Jess Cartner-Morley in Paris
__TIMESTAMP

Tuesday 3 October 2017 10.55EDT

__ARTICLE
Karl Lagerfeld took over the house of Chanel in 1983, a decade after Coco Chanels death. He has kept her alive ever since.
Her fierce elegance and formidable character remain the brands biggest assets and were on display at its latest Paris fashion week spectacular. 
Even though Korean pop star G Dragon was in the front row and 16-year-old second-generation supermodel Kaia Gerber was on the catwalk, the star of the show was the woman who opened her first millinery boutique in Paris in 1910. 
The Grand Palais has housed a replica of the Eiffel Tower and a space rocket for recent Chanel shows, but this time transformed into the Gorges du Verdon in Provence. 
Six waterfalls, 10 metres high, cascaded under a boardwalk catwalk which wound through 2,600 seats. The aquatic theme was the link for the witty catwalk touches which Lagerfeld loves  perspex earrings in the shape of giant raindrops; two-tone rain boots, scallop shell edges  but the essential silhouette of many looks in this collection was recognisably that of the founder in her neat skirt, low block heels, fitted tweed boucl jacket and a straight-brimmed hat.
But Coco Chanel is so last year, because 2017 is all about Gabrielle Bonheur Chanel. Gabrielle, the houses first major fragrance launch in 15 years, is branded with the real name of the woman who became Coco. The buzz around the name Gabrielle, which is also given to this seasons it handbag, is significant. 
Lagerfeld and the houses president of fashion, Bruno Pavlovsky, have constructed a semi-visual global language for Chanel. The word Gabrielle now joins Coco and Chanel, but also the double C, the No 5, the stoppered perfume bottle, the flat-brimmed hat, the tweed suit and the pearl. This represents a formidable form of luxury hieroglyphics which can be understood anywhere in the world  a bonus in an industry where communication with Chinese and Middle Eastern consumers is key.
Gabrielle Chanel represents the younger, more free-spirited era of the designers life. The name channels the rebellious side of Chanel, rather than the strictly rule-bound refinement of the later years in her Rue Cambon apartment. 
Everyone knows Coco Chanel, but before there was Coco, there was Gabrielle, said Pavlovsky in an interview before the show. We believe that Gabrielle was a woman with a strong point of view, and it was the right time for us to illustrate that. Gabrielle is about the freedom, the values of a freedom, of femininity without limits. 
Bringing to life the notion of Coco Chanel as a young woman is a no-brainer for a brand that must win over millennials to survive. And the adoption of Cocos real name is timely at a moment when authenticity is fashions greatest buzzword. 
This is not about authenticity as a trend, said Pavlovksy. Authenticity is everything at Chanel. This is about what we make, about how we communicate, about our passion for creativity. I believe the brand is doing well because people can feel where there is truth. We are not just a marketing brand. 
Distinct from the Instagram guns for hire who hop from front row to front row at fashion week, Chanel has its own ambassadors, many of whom, like Vanessa Paradis and Ins de la Fressange, have been friends of the house for several decades. These are people with their own style, their strong point of view, with real relationships with Mr Lagerfeld and Virginie Viard  Lagerfelds director of studio.
The value of the Paris fashion week show, says Pavlovsky, is the wow effect. We have 20 minutes to convince  not just you in the audience, but everybody watching on social media. It is not about showing how successful we are, it is about the fact that Chanel can take you somewhere else. The experience of a show represents that. When your boss is Mr Lagerfeld, you are very lucky. 
He revealed that the day before the installation of the waterfalls was completed, the talk in Lagerfelds inner circle moved immediately on to the topic of next seasons show. 
