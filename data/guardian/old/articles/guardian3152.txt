__HTTP_STATUS_CODE
200
__TITLE

Six steps to improving mental wellbeing at work

__AUTHORS

Joanne O'Connell
__TIMESTAMP

Wednesday 11 October 2017 02.00EDT

__ARTICLE
Three in five employees have experienced mental health issues in the past year because of work, according to a YouGov survey commissioned by charity Business in the Community. So what steps can be taken to improve wellbeing in the workplace?
Working out what triggers stress or poor mental health can help you anticipate problems and think of ways to solve them, says mental health charity Mind.
Emma Mamo, head of workplace wellbeing at Mind, says: Take some time to reflect on events and feelings that could be contributing to your poor mental health. You might be surprised to find out just how much youre coping with at once.
The triggers may well be problems with certain tasks at work, one-off events like doing presentations, as well as regular issues such as attending interviews and appointments. 
Remember that not having enough work, activities or change in your life can be just as stressful a situation as having too much to deal with, Mamo says.
Managing when and where you work can be helpful, says Rachel Farr, senior professional support lawyer at Taylor Wessing. Since 2014, all employees (not just parents and carers) have had the right to request flexible working for any reason, and this can include switching your shifts, working different hours and sometimes working from home, says Farr. 
Working from home, for example, can mean you skip the commute and instead spend that travelling time with your family, exercising or even getting up slightly later (while still getting to work on time).
Farr says: I can take my children to school, go for a run and then log in to my computer to work at the same time I would have arrived in the office, but feeling energised rather than stressed before my day has even begun.
Unlike in France where employees have the right to disconnect, in the UK many people feel they cant switch off, which can be detrimental to mental health.
Theres no such thing as work/life balance, according to Tom Oxley, lead consultant at Bamboo Mental Health. He says: You think about home life when youre at work and work life when youre at home and in truth, they are both integrated.
However, that doesnt mean that you need to be constantly on, he says. Scrolling through work emails or your work social media accounts 24/7 doesnt give your brain a break and can lead to problems.
When you leave work, actually leave work, says Oxley. This means turning off your work phone. Like a laptop, we need to switch ourselves off and recharge. Its particularly vital not to have your work phone near your bed at night, as it interrupts your sleep. 
If your workload is regularly spilling into your personal life, speak to your manager about it, says Mamo. See if you can jointly come up with solutions, such as agreeing a different approach or delegating work to other members of staff.
It can be difficult for people to open up about mental health concerns, but fortunately some companies are taking steps to encourage discussions about the topic.
Clive Johnson, head of health and safety at property company Landsec, has signed up to Mates in Mind. The charity works with the construction industry to improve mental health by helping people talk openly with their employer and colleagues.
Johnson has set up a network of trained mental health first aiders who offer a supportive ear to colleagues and says this training is a crucial step in tackling the issue.
Its easy to spot someone coming into the office on crutches and arrange some help, but it takes more awareness to pick up on the subtle signs of declining mental health, says Johnson.
Often people arent aware that they can access confidential counselling paid for by their employer. The free sessions can be a safe way to talk about troubling issues.
Increasingly Im finding that GPs refer their patients back to their employer and recommend they ask whether they can access counselling via their workplace because they know they will get seen quickly, says Nicola Banning, a counsellor who specialises in mental health at work and a member of the BACP Workplace Executive Committee.
Its not unusual for me to have arranged to see a client within 48 hours of them being referred, so employees really dont have to suffer in silence, she says. 
When youre not at work, pack in plenty of healthy food, sleep and exercise, says Oxley. Its well known that these can all boost our mental and physical health, but being outdoors can also help, he says.
Even going for a 15-minute walk during the day can help clear your mind, according to the Royal College of Psychiatrists. Mamo says: It might be difficult to take breaks at work when youre stressed, but it can make you more productive.
Many people have demanding jobs and when youre caught up in an exhausting cycle of relentless hours, its easy to be hard on yourself.
Very often we dont need our boss or colleagues to give us a hard time as were good at doing that ourselves, says Banning.
If youre struggling at work, give yourself some space. This could mean taking a few days off, requesting flexible working or getting some support outside of work, she suggests. Once youve had some space, you can make choices.
Joanne OConnell is editor of www.EmploymentSolicitor.com
Looking for a job? Browse Guardian Jobs or sign up to Guardian Careers for the latest job vacancies and career advice
