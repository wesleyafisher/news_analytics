__HTTP_STATUS_CODE
200
__TITLE

Rebecca Warren review  a creepy comic carnival on the edge of Cornwall

__AUTHORS

Jonathan Jones
__TIMESTAMP

Wednesday 11 October 2017 10.52EDT

__ARTICLE
The sea and sky are the first rivals any artist exhibiting at Tate St Ives has to reckon with. A big, bold view of both fills its beach-facing galleries. In the spectacular new concrete-roofed extension that opens this week, there are no views, just lots of bright natural light streaming in from above. Then again, instead of views, there are the works of all the 20th-century artists who settled in Cornwall and created the strand of British modernism that led to this museums existence.
The airy paintings of Peter Lanyon and the organically orotund casts and carvings of Barbara Hepworth have never looked better than they do in the reborn Tate St Ives. Even for a sceptic like me, who doesnt believe British abstract art ever rivalled the likes of Mark Rothko or Jackson Pollock, the union of this art with the seascapes that inspired it is compelling. 
Unfortunately, the abstract artists who created the St Ives school are much more at home here than the first contemporary artist to show in its grand box of a new exhibition space. Rebecca Warren is left seeming like a metropolitan interloper whos not sure why she is here or what her art is for.
Warrens exhibition fails to fill the vast room it is in. Her sculptures are dotted around the space rather than inhabiting it convincingly. Some are terrific, but the whole is less than the sum of its parts. Its a pity because Warren is a hugely interesting sculptor. Her art is sensual, tactile, meatily physical. She moulds clay and casts it, and forces massive pieces of steel together. Her new works include colossal totemic figures cast in bronze and painted with a deliberate childlike sloppiness. Their grotesque hints of faces and bulbous hands give them a crudely humanoid aspect, their chaotic forms warped by bulges, ridges, drips and gloops.
What are these beings? They resemble ancient pagan standing stones or primitive Celtic statues, but the joke is surely that they also resemble the intense abstract homages to such mysterious totems that were sculpted by 20th-century modernist artists, from Hepworth to Henry Moore. Were on familiar terrain here: artists have been parodying modernism for some time. Warrens parodies assault the patriarchy of Moore. They look like soft molten phalluses, with pink bows as a feminine touch. She makes the same point with a steel construction in the style of Anthony Caro: it has been painted pink and sports a fluffy pom-pom.
This is all fine as far as it goes. Yet Warren does not seem to know what she wants this exhibition to achieve. Her collages of wood, paper and other scraps mounted on MDF, some lit by glowing pink neon tubes, dont communicate very much. Her hard-edged metal abstractions dont even look like they were made by the same artist as the blobby monoliths.
Yet, amid the confusion and lack of impact, a group of brilliant figures stand in postures of monstrous, creepy grandeur. They are called Los Hadeans and they truly belong in Hades. Weird monkey faces, bizarre postures and long spindly outgrowths make these creatures truly strange, funny-peculiar and haunting. 
Warren has a comic talent for the carnal, the erotic, the monstrous that is perfectly encapsulated in her 1998 sculpture Helmut Crumb. Shes a surreal figurative artist, not a cool abstract one. Why does she get lost in abstraction here? It drains the sex and humour from her art. Perhaps it is hard to resist taking on the abstract tradition at St Ives, its home in Britain. The uplifting modernism of this new exhibition space also seems to demand something different from the playful wit that comes naturally to her. 
The trouble with parodies of 20th-century modernism is that they ultimately suggest a lack of belief in any purpose to art. Leaving Warrens show and contemplating the Hepworths and Ben Nicholsons, you have to admire the idealism and vision of the artists who, in the last century, headed for Cornwall to find some kind of truth.
