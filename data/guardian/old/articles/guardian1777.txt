__HTTP_STATUS_CODE
200
__TITLE

'Girls aren't less than boys': Kabul's female veterinarians hope to cure inequality

__AUTHORS
Fran McElhone
__TIMESTAMP

Monday 18 September 2017 07.19EDT

__ARTICLE
Unpredictable and indiscriminate bomb blasts dont deter the three women heading up Afghanistans only large-scale animal shelter and veterinary clinic, in Kabul. Neither do the attitudes of the people who told them they couldnt, or shouldnt, be vets.
Afghanistan is one of the lowest-ranked countries in the world for gender equality. In this strictly patriarchal society, women are still traditionally married off soon after school age and remain housewives for the rest of their lives. They may face imprisonment for running away from home and many are still behind bars for moral crimes.
So for Dr Maliha Rezayi, Dr Malalai Haikal and Dr Tahera Rezaei to be at the helm of the Nowzad Conrad Lewis Clinic in Kabul, they are not only changing the course of animal welfare in Afghanistan, but resetting the course of history for women in society too.
The women, all in their 20s, say they are always busy at the clinic, which was founded by former British Royal Marine Pen Farthing. These are Afghanistans next generation of women, the millennials changing the future of their country, animal by animal, person by person, through a gradual but potent ripple effect.
Rezayis parents were born in Iran after her grandparents moved there to escape the civil war in the early 1990s. When she was 13, the family, including her six siblings, moved back to Afghanistan. Although her parents, a shopkeeper and a housewife, backed her determination to attend school and eventually go to university  and to become Afghanistans female boxing champion while still a teenager  her extended family were unremittingly scornful.
My parents and brothers and sisters supported me and made me feel that I could be anything I wanted to be, says Rezayi, 27, who had an amicable arranged marriage two years ago. But my wider family members were not supportive. Most of them grew up in Afghanistan and thought girls cant go out. This is not a good culture here.
Of course it was hard, but it made me work harder, to prove everyone wrong. The attitude was that girls cant do anything and I wanted to show them that girls arent less than boys.
When she went to Kabul University in 2012, there were around 20 women and 140 men on the course. Her experience was mixed; although there were some supportive tutors, there was also a culture of oppression towards female students.
Some of the teachers didnt want us to do well and would mark my grades down, she says. In our society, there is a belief that you cant do things because youre a woman. So when I go out to help someones animal, sometimes people dont want me to touch it. But after I get them to trust me, they thank me.
For three years, the trio has been leading clinic-based sessions with the next wave of veterinary students, giving them the opportunity to gain vital experience.
The women are happy to have a female teacher, Rezayisays. But the men often ignore us at first and dont want to listen to us. Then when we start teaching them interesting information, they start to listen.
The women live with the constant fear of civil unrest. When we speak, Rezayis uncle is recovering in hospital after being severely injured several weeks before in a truck bomb explosion that killed more than 150 people on May 31, making it the deadliest single attack since the start of the war in 2001. 
Rezayi was only a few hundred metres away from the blast. The situation is hard, she says. Our families are always worried about us. But were used to it. It is normal for us.
Haikal, 24, was educated at home in Pakistan, where her parents had gone to flee the civil war, and upon her return to Afghanistan under Taliban rule, she was confined to the family home. When she was at university, she says, people  especially men  didnt want women to do well out of fear that they would become stronger than them.
Rezaei grew up in Iran with her nine siblings as refugees of the civil war. Now 26, she moved to Afghanistan aged 15. After graduating, she worked at Kabul Zoo, where visitors were dumbfounded when she told them she was working there.
All the women agree that the single most important thing, other than family support, that will help change attitudes and stifle gender inequality is education.
[It is] the only solution, says Rezayi. The media can help, but the main thing is education.
When I first went to uni, some members of my family said it was a bad thing. But when I graduated, they started saying to their daughters, you need to go to school so you can be like Dr Maliha.
