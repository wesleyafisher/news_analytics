__HTTP_STATUS_CODE
200
__TITLE

Virgin Media is chasing me over a bill for a phone I dont even have

__AUTHORS

Miles Brignall
__TIMESTAMP

Tuesday 17 October 2017 02.00EDT

__ARTICLE
Can you please help me to get Virgin Media to call off the debt collectors for a mobile phone contract that is nothing to do with me?
I have never owned a mobile, but in June I noticed the company had taken two 13 payments by direct debit. I contacted the bank to cancel the direct debit, and staff confirmed that I had been refunded the money. They advised me that it was probably a clerical error and nothing to worry about. But in August I received a threatening letter from debt collectors demanding 39 outstanding from Virgin Mobile.
I have since spent many hours trying to resolve this. Over 50 phone calls have ended in failure because the call handler demands the number of the mobile and/or password, which not having ever opened an account with Virgin Mobile I cannot supply. Please help. IW, Croydon
Virgin Media got on the case and have now assured you that the matter is resolved. The company said that staff failings would be followed up with the individuals who declined to escalate calls and their managers. A goodwill payment of 50 to reflect the loss of time and phone costs is being sent.
I would advise you to sign up with the fraud prevention service Cifas. This will mean you will be notified if anyone else applies for credit in your name in the future. It is clear that a fraudster used your identity to purchase the phone, and they may try and do the same again.
We welcome letters but cannot answer individually. Email us at consumer.champions@theguardian.com or write to Consumer Champions, Money, the Guardian, 90 York Way, London N1 9GU. Please include a daytime phone number
