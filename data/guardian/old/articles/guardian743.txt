__HTTP_STATUS_CODE
200
__TITLE

Jeremy Lees alluring recipe for lemon tart

__AUTHORS

Jeremy Lee
__TIMESTAMP

Saturday 14 October 2017 05.00EDT

__ARTICLE
A beautiful lemon  preferably with leaves attached and picked somewhere near Naples  has an almighty ability to cheer even the most frazzled of cooks. Depicted in every form imaginable, they are a fruit that inspires artists and, in more modern times, photographers, almost as much as they do chefs.
Lemons have an allure unlike any other fruit. They have become vital to sparking up almost any dish with an elusive vigour that only they can provide. They feature in an abundance of dishes, savoury as well as sweet  from dressing a leafy salad to finishing the caper and black butter sauce one tips on a wing of charred skate for raie au beurre noir. And, when added to puddings, cakes, creams, custards and biscuits, a lemon is the perfect foil for any cloying sweetness.
Like so many ingredients, it is hard to imagine a time when lemons were rare and precious fruits. That said, their cost is still almighty if it is the great, unwaxed lemons from the Amalfi you are buying. There are myriad others, but just as it is only the white truffles from Alba make the grade, so it is with these legendary lemons. It is not even the juice so much as the oils in the skin, when zested, that make the heart skip a beat.
Worry not: I shall not ask you to spend the months rent on lemons, although sometimes it feels like that at Quo Vadis. Once we used to fill huge baskets with lemons in the reception. However, the heap seemed to diminish rather quickly  it appeared everyone thought the lemons so lovely they would take one or two home with them. This expense is not, thankfully, necessary with this lemon tart, a pudding that sparks the imagination and gathers attention like few others.
Now, there are a great many recipes for lemon tart; indeed, there may be a real beauty when the lemons are in season in winter. But I like this recipe very much, for it retains the finely grated zest, which adds a curious curd-like quality  both homely and comforting. And, crucially, there is much lemony zip.
Should you have a few pieces of candied lemon peel at hand, to add some joviality to the tart, by all means cut thin slices and strew them liberally around the edges. A bowl of whipped cream, as ever, seems somehow appropriate.
The pastry is best made the day before. Have ready a deep-fluted tart case, about 22cm wide by 2cm deep, with a removable base.
Serves 12 For the pastry170g flour, plus more for rolling100g cold unsalted butter, cubedA pinch of salt1 tsp icing sugar1 egg 1 tbsp cream
For the lemon fillingZest and juice of 9 medium lemons6 eggs375g caster sugar525g creme fraiche6 tbsp rum
1 Set the oven to 170C/335F/gas 3. Sift the flour into a bowl. Cut the butter into small pieces. Drop the butter into the flour with a pinch of salt. Lightly work these together with your fingertips until a fine crumb forms, then add the icing sugar.
2 Beat the egg. Add the cream and stir well. Pour this into the pastry and knead into a dough. Tip on to a clean surface and knead lightly and deftly until the pastry is smooth. Wrap in clingfilm and refrigerate for at least an hour  preferably overnight.
3 It is vital the tart case does not leak, so clear the surfaces of all clutter before proceeding. On a lightly floured surface, gently roll out the pastry in a disc and line the tart case. Put the tart case in the fridge to settle for at least 30 minutes. Meanwhile, make two discs of greaseproof paper. Have ready a big bag of dried beans and/or rice.
4 Remove the tart from the fridge. Carefully line the tart case with the greaseproof paper. Pour the beans and or rice on top of the paper to fill the case. Put the tart case in the oven and bake for 15 minutes, or until the edge of the pastry is golden. Turn down the heat to 140C/275F/gas 1 and cook for a further 10 minutes.
5 Remove the tart case from the oven. With care, slowly lift the paper from the case. Keep the rice or beans for future baking endeavours. Return the tart case to the oven for 5 minutes more. (There may be cracks. Should these appear, remove the tart from the oven. Place on a surface. Crack two egg whites into a bowl and keep the yolks as an excellent excuse for mayonnaise. Beat the whites with a fork until lightly frothed. Brush the inside of the tart case liberally with the beaten whites. Return the tart to the oven for 1 minute only. Remove and set to one side.)
6 Finely grate the zest from the lemons, then juice them.
7 Crack the eggs into a large bowl and beat them well. Add the sugar and beat well. Now add the creme fraiche, then the lemon juice, then the zest, and finally the rum, mixing all very well together. Let the lemon cream settle.
8 Spoon away any froth that settles upon the surface. Decant the lemon cream into a jug, put the tart case back in the oven, holding the shelf and tray out far enough to allow the cream to be poured from the jug into the tart.
9 Carefully push the shelf and filled tart back into the oven. Close the door. Bake for 45 minutes, checking from time to time that the tart is not colouring. It should take an hour, but often takes 15 minutes, or even half an hour, longer. It is wise to be patient here.
10 When the tart is cooked and there be just the faintest wobble upon the surface when jiggled, remove from the oven and cool. 
