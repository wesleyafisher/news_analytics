__HTTP_STATUS_CODE
200
__TITLE

Eimear McBride: I can never finish Dickens  its sacrilege

__AUTHORS
Eimear McBride
__TIMESTAMP

Friday 13 October 2017 05.00EDT

__ARTICLE
The book I am currently reading
Chris Krauss After Kathy Acker  I recommend it. When the energy of the writing matches the energy of the subject, its magic.
The book that changed my life
Anne of Green Gables. I read it when I was eight and it was the first time I realised what a weapon the imagination could be. Im not sure that was the point ...
The book I wish Id written
I cant imagine having written anyone elses books but I wouldnt have minded being Dostoevsky for a while, just to see how it was done.
The book that had the greatest influence on my writing
Oh, Ulysses. As a writer, once you have read that, you really have to up your game.
The book I think is most underrated
I dont know about most underrated, as it won the Booker prize, but Keri Hulmes The Bone People is a beautiful novel, looking at difficult subjects in an uncompromisingly complex way, and its really owed more of a following than it has.
The last book that made me cry
Garth Greenwells What Belongs to You. Its a long time since I read a book that made me feel less alone in the world.
The book I couldnt finish
Any Dickens  sacrilege.
The book Im most ashamed not to have read
Any Faulkner  the kind of sacrilege Im bothered by.
The book I most often give as a gift
On balance, I think its probably been Susan Faludis Backlash. It really helps you get to grips with the medias war on women and feminism.
The book Id most like to be remembered for
My multi award-winning, worldwide bestselling, as yet untitled, seventh novel. Its going to be brilliant.
