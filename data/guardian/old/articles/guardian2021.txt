__HTTP_STATUS_CODE
200
__TITLE

Not the Booker prize 2017: Dark Chapter by Winnie M Li wins

__AUTHORS

Sam Jordison
__TIMESTAMP

Monday 16 October 2017 05.40EDT

__ARTICLE
Winnie M Lis Dark Chapter is the winner of this years Not the Booker prize. It was the voting publics favourite, and our judges concurred.
Coming into the final round, Dark Chapter was the clear favourite with its victory in readers polling meaning it had two votes to take into the final round. Heres how the public vote played out:
Dark Chapter by Winnie M Li 130Man With a Seagull on His Head by Harriet Paige 106The Threat Level Remains Severe by Rowena Macdonald 103Not Thomas by Sara Gethin 55Anything Is Possible by Elizabeth Strout 11
(Thanks to the excellent lljones for tallying and verifying those votes.)
But even though Dark Chapter was ahead, the final choice was not clear cut. Our judges also had one vote each  and two of them favoured Harriet Paiges Man With a Seagull on His Head. They shared my general opinion that this book is beautifully written, engaging and strange. It fell to our third judge, Hannah Bruce Macdonald, to make a case for Dark Chapter as the book that had stayed with her the longest. She argued that it wasnt just an important and brave novel, it was also a story well told.
All of our judges were admirably civil, but Hannah must have felt some pressure as we interrogated what prizes are for and how we define literary merit. She stood firm and made effective and persuasive arguments. As a result, we have a worthy and powerful winner.
The only pity is that we could only choose one book. This was an excellent year and the other four books on our shortlist were excellent contenders. Commiserations are due to them all. Our winner, meanwhile, deserves hearty congratulations.
