__HTTP_STATUS_CODE
200
__TITLE

French (almost) without tears: a summer school break in France

__AUTHORS

Amelia Gentleman
__TIMESTAMP

Sunday 13 August 2017 05.00EDT

__ARTICLE
Its perhaps not surprising that there wasnt universal enthusiasm about a holiday at a French language school. I did my best to rebrand it as a French fun camp, but my attempts were met with weary cynicism by Rose, 13  who wrote Make escape plan on her hand on the first day  and William, 11. Why would anyone want to spend part of the summer holidays in a classroom? they asked.
To begin with, maintaining a front of unflinching good humour in the face of their gloom was hard work. Gradually, though, they were won over. I wouldnt say they were exactly skipping to catch the morning bus to classes, but by the end of our stay they were certainly scowling a lot less.
But it turns out that the Alpine French School, in the ski resort of Morzine, is genuinely pretty fun  and I know because I took classes too. Set up by a British woman, Helen Watts, the school is housed in a big beautiful chalet with a pool, and runs a summer season designed for families. The children are picked up at 8.30am, and study until lunchtime, while their parents have the morning free. After lunch the children go canyoning, rock-climbing, tree-climbing, swimming, summer sledging and mountain biking, while the grown-ups spend a few hours doing lessons. Its all flexible though, so if you want to spend more time as a family you could simply sign them up for the morning.
Classes are small, with children divided up according to age and ability (predominantly Brits, but there are students from all over the globe, and some are here alone, boarding in the schools dormitories). Juniors might practise buying things at a market, while the more advanced debate whether the internet is a social evil  in French. While GCSE and A-level students have clear targets, the aim for younger children is simply to have fun and inspire an enthusiasm for French.
To keep costs down, we stayed in a shared self-catering chalet, organised for us by the school. Its a bit of a gamble, spending a week with people youve never met before, but we lucked out with a friendly Swiss teenager (ranked third in Switzerland for mountain biking), and a cheerful family from England, also studying at the school and happy to collaborate in cooking industrial quantities of pasta. The kitchen was huge, with dramatic views down the valley.
I loved being back in the classroom  the happiness of writing down new vocabulary in tidy rows, of being set and then (genuinely) forgetting to do homework  and the teachers were brilliant. My group of students was interesting, too: a doctor from Poland, a man from South Carolina recently made redundant, a recently bereaved English woman living in Switzerland, and a novelist from the Netherlands.
It was, admittedly, a bit strange to be on holiday with my children but not see them from when they set off in the morning until 5.30pm each day. I read a lot, and enjoyed the unusual amount of peace and isolation.
There was family time too: we went walking in the mountains above Morzine at the weekend, picking wild raspberries, paddling in waterfalls and looking our for (but not finding) Alpine marmots. We visited Les Lindarets, a malodorous nearby village overrun by goats, and swam in Lakes Monriond and Geneva. Morzine works hard in the summer to please tourists: a multi-pass, subsidised with your accommodation, works on the ski lifts, the ice-skating rink and the 50-metre outdoor swimming pool, with more dramatic views along the valley. Its not a place where people lie around the pool  tourists seem to be racing non-stop between canoeing, tennis and riding and cycling.
There are a lot of British permanent residents, and shop keepers respond in English if they detect a note of an English accent; the French school hands all its students a card to be flashed at local shop workers, requesting that they speak French (slowly).
My French improved because I used it every afternoon for a week, and revisited bits of grammar that had always remained pretty opaque. Rose and William werent really able to say how much better their French got, but I do know they enjoyed the experience. Rose loved the mountain excursions, and was grudgingly cheerful by the end. Unexpectedly, William was enthusiastic about the whole thing, French classes included.
Ill find out some time next year if their language skills have improved  if the mildly sardonic reports I get from their French teachers about their willingness to apply themselves to the subject have shifted to something a bit more appreciative.
 The trip was provided by the Alpine French School (+33 4 50 79 08 38, alpinefrenchschool.com). A weeks morning language lessons is 229 a child, a weeks afternoon activities 169; adult intensive courses are 275 a week; family self-catering accommodation from 700 a week. The next schools are between 8 July and 18 August 2018. Transfers to Morzine were provided by ski-lifts.com
