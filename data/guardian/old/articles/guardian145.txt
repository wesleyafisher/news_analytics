__HTTP_STATUS_CODE
200
__TITLE

Mogadishu atrocity may provoke deeper US involvement in Somalia

__AUTHORS

Jason Burke Africa correspondent
__TIMESTAMP

Sunday 15 October 2017 15.15EDT

__ARTICLE
For many years, Somalia was a forgotten front among the various campaigns against violent extremist Islamists around the world.
The massive bombing of the centre of Mogadishu, the capital of Somalia, will bring the international spotlight back on to the battered country  at least for a few days.
Al-Shabaab, the Islamist group based in the country, is almost certainly responsible for the huge truck bomb that killed as many as 300 people in Mogadishu on Saturday.
The attack proves once more it is among the most capable and tenacious militant organisations anywhere. 
Al-Shabaabs roots run back through a series of violent  and sometimes non-violent  revivalist Islamist movements in Somalia over the past 40 years. In the past decade, it has been fighting local, regional and international forces, and has survived significant strategic setbacks primarily by exploiting the weaknesses and failings of central government in the shattered state.
One reason for the relative lack of attention devoted to al-Shabaab in recent years in Washington, London and other western capitals is that the group has ruthlessly purged anyone who wanted to swear allegiance to Islamic State in Iraq and Syria from its ranks.
That al-Shabaab  the name means the youth  is not seen as particularly dangerous beyond its immediate region is another reason.
Though the group has been a formal affiliate of al-Qaida since 2011, it has not engaged in terrorist planning against European or US targets. Though it has attracted militants from the west, it has not sent many back the other way.
Al-Shabaab has, however, launched a series of bloody attacks in east Africa, such as the assault on an upscale shopping mall in Kenya in 2013 in which 67 people were killed.
It has been regional powers, including Kenya, that have done the heavy lifting in terms of military deployments in Somalia in recent years. 
More than 20,000 troops have been deployed by the African Union there. But they have been much criticised, accused of being arrogant and sometimes brutal toward local populations, corruption and military incompetence.
A series of assaults by al-Shabaab on African Union bases have undermined political will to continue this commitment among regional states  as the extremist strategists intended it would. 
The bombing in Mogadishu may now intensify a growing US commitment to pursuing a more active role in Somalia.
 Earlier this year, the US president, Donald Trump, designated Somalia a zone of active hostilities, allowing commanders greater authority when launching airstrikes, broadening the range of possible targets and relaxing restrictions designed to prevent civilian casualties. He also authorised the deployment of regular US forces to Somalia for the first time since 1994.
 The US in effect pulled out of Somalia after the Black Hawk Down episode of 1993, when two helicopters were shot down in Mogadishu and the bodies of American soldiers were dragged through the streets. 
 In May a US special forces soldier was killed in a skirmish with al-Shabaab, the first US casualty in Somalia since then. 
Any deeper involvement in Somalia would come against a background of greater involvement across Africa. Earlier this month, four US servicemen were killed in a firefight in Niger with militants there.
Yet the same challenges experienced in conflict zones such as Iraq and Afghanistan face any counter-insurgency effort in Somalia. 
Somalia is suffering its worst drought in 40 years, with the effects of climatic catastrophe compounded by war and poor governance. Al-Shabaabs control over populations in rural areas in much of the south and central Somalia is such that the group was able to impose a ban on humanitarian assistance in areas they control, forcing hundreds of thousands of people to choose between death from starvation and disease or brutal punishment. 
