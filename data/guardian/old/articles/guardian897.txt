__HTTP_STATUS_CODE
200
__TITLE

Even moderate drinking by parents can upset children study

__AUTHORS

Denis Campbell Health policy editor
__TIMESTAMP

Tuesday 17 October 2017 19.01EDT

__ARTICLE
Three in 10 parents say they have been drunk in front of their children and half have been tipsy, and such behaviour can trigger family rows or make children anxious, research suggests.
Even moderate drinking by parents can leave children feeling embarrassed or worried or lead to their bedtime being disrupted, according to the study led by the Institute of Alcohol Studies (IAS). 
It is the first evidence that even low-level parental drinking  at no more than 14 units a week  can prove damaging to children. Children who see a parent tipsy or drunk are less likely to see them as a positive role model, the report says.
The findings have prompted calls for parents to think more carefully about their alcohol consumption and for the government to overhaul official advice about when and how much people should drink. 
It is worrying that the majority of parents reported being tipsy or drunk in front of their child. All parents strive to do whats best for their children, but this report has highlighted a troubling gap in their knowledge, said Katherine Brown, the IASs chief executive. 
Parents who have a glass or two of wine in the evening deserve to understand how this might affect their children and the steps they can take to minimise this impact. 
The report, co-produced with the Alcohol and Families Alliance and Alcohol Focus Scotland, says 29% of parents believe it is acceptable to get drunk in front of their children as long as that does not happen very often. 
Other findings include:
Anne Longfield, the childrens commissioner for England, said: Many parents make the unwise assumption that even moderate levels of drinking doesnt bother their children. Parents need to make a judgment before they drink on what impact their drinking might have on their kids.. 
We know some children become very anxious when their parents use alcohol in ways that lead to uncertain, unusual or unpredictable behaviour. In my view the best way to make that judgment, as in lots of areas to do with children, is ask them, listen to what they say and act accordingly. 
The report is based on interviews with a representative survey of 997 adults and their children around the UK. All the parents said they consumed no more alcohol than the 14 units a week recommended in official guidelines.
The former health minister Caroline Flint, who is the daughter of an alcoholic, said: We too quickly dismiss parental drinking as harmless fun and relaxation, but this report shows that parents do not need to be regularly drinking large amounts for their children to see a change in their behaviour and experience problems. 
Alison Douglas, the chief executive of Alcohol Focus Scotland, said the switch in recent years from drinking in pubs to buying alcohol in supermarkets and off-licences meant children now saw parents drinking more than before.
This means children are more likely to be around alcohol and to witness drunkenness. As well as the negative impacts on childrens wellbeing, seeing how adults drink can have a big influence on our childrens future drinking habits, she said.
The Department of Health said it was aware that parents drinking habits could harm their offspring and it was considering what measures it might take. 
While no one would want to interfere with the right of adults to enjoy a drink responsibly, we are committed to giving people the information they need to make informed choices about their drinking, which we provide through the UK chief medical officers guidelines, a spokesman said. 
We are also acutely aware of the impact adults drinking can have on their children. Thats why we are looking at what further support we can provide to tackle alcohol harms, focusing on vulnerable groups including families and children.
Jon Ashworth, the shadow health secretary, who has spoken about his fathers death as a result of drinking, said: This crucial report highlights that even non-dependent parental drinking has serious health implications on children and families. 
Children are incredibly perceptive of their parents drinking habits and this analysis must serve as a wake-up call to the government. 
