__HTTP_STATUS_CODE
200
__TITLE

The Inequality Project: the Guardian's in-depth look at our unequal world

__AUTHORS

Mike Herd
__TIMESTAMP

Tuesday 25 April 2017 00.58EDT

__ARTICLE
Inequality is all around us  some all-too visible, much of it obscure and insidious. Experts are lining up in ever-greater numbers to warn of its harmful effects  from Professor Stephen Hawking, who wrote in the Guardian of technologys role in growing levels of income inequality, to Christine Lagarde, head of the International Monetary Fund, who told the worlds business leaders earlier this year: I hope people will listen [to my warnings] now.
Not everyone is listening, though  in part because the picture can be very confusing. In the UK, for instance, the Office for National Statistics revealed the gap between the richest and poorest fifths of society fell significantly last year. However, it also said Britains generational divide is growing, with a boost to pension payments masking the continued struggle of many other households.
In Australia, the countrys sex discrimination commissioner, Kate Jenkins, has highlighted her concerns about the surprising and concerning prevalence of opposition to advancing gender equality. And in the US, a new book by Professor Tom Shapiro, Toxic Inequality, reveals how ingrained, systemic racism is responsible for the widening gap between the wealth of white and African American households.
Over the coming year, the Guardians Inequality Project  supported by the Ford Foundation  will try to shed fresh light on these and many more issues of inequality and social unfairness, using in-depth reporting, new academic research and, most importantly, insights from you, our audience, wherever you are in the world and whatever your experiences of inequality.
 To get in touch, email us at inequality.project@theguardian.com and follow us on Twitter @GdnInequality.
 To read all of the Guardians coverage of these issues, check out our inequality homepage.
The Inequality Project is supported by the Ford Foundation. All our content is editorially independent, with a remit to focus on inequality of all kinds, all over the world. The project is anchored from the Guardians London office, and is edited by Mike Herd.
All our journalism follows GNMs published editorial code. The Guardian is committed to open journalism, recognising that the best understanding of the world is achieved when we collaborate, share knowledge, encourage debate, welcome challenge, and harness the expertise of specialists and their communities.
The only restriction to the Guardians coverage is where the Ford Foundation is prohibited under US law from directly funding or earmarking funds to: (a) carry on propaganda, or otherwise to attempt to influence any legislation through an attempt to affect the opinion of the general public, or through communication with any member or employee of a legislative body; (b) conduct programmes to register voters; or (c) to undertake any activity for any purpose which is not exclusively charitable, scientific, literary or educational.
This means any communications to the public in which a view is expressed about a specific legislative proposal, and the recipients of the communications are urged through a call to action to contact government officials, must provide a reasoned, objective consideration of facts and issues in a full and fair manner that enables third parties to develop their own positions on any legislation that may be discussed. You can read more about content funding at the Guardian here.
