__HTTP_STATUS_CODE
200
__TITLE

Sadiq Khan asks car manufacturers to give funds towards tackling Londons toxic air

__AUTHORS

Matthew Taylor
__TIMESTAMP

Friday 6 October 2017 05.34EDT

__ARTICLE
The mayor of London, Sadiq Khan, has written to three leading car manufacturers asking them to contribute to the fund set up to tackle the capitals air pollution crisis.
Khan has accused BMW, Mercedes-Benz and Volkswagen of double standards after it emerged they had paid 223m to the German governments Sustainable Mobility Fund for Cities earlier this year, but have given nothing to the UK. 
Londoners will be baffled by the double standards of these car manufacturers. On the one hand, they admit theyve got to cut emissions from their vehicles, but they confine their funding to Germany alone.
VW has also paid $15bn compensation in the US after the dieselgate scandal  when it was found to have fitted millions of cars with defeat devices that made their cars appear less polluting in emissions tests than they were on the road.
Khan said: In July, the UK managing director of VW sat in my office and said they couldnt contribute anything to fund cleaning up Londons air, but their German colleagues are providing money. Londoners will find that unacceptable.
The scale of Londons air pollution crisis was laid bare on Wednesday, with new figures showing that every person in the capital is breathing air that exceeds global guidelines for dangerous toxic particles.
The research revealed that every area in the capital exceeds World Health Organisation (WHO) limits for PM2.5s  ultra-fine particles of less than 2.5 microns which have serious health implications, especially for children.
London is widely recognised as the worst area for air pollution in the UK, although there is growing evidence that dangerously polluted air is damaging peoples health in towns and cities across the country.
Khan has also written to the secretary of state for transport, Chris Grayling, to ask the government to do more.
The government must act urgently to secure a meaningful amount of funding from these manufacturers, which could help people to scrap the most polluting diesel vehicles and take these off our streets, said Khan.
The mayor has set out a range of plans to tackle pollution from diesel cars in the capital. The first stage, the new T-Charge, which will charge older, more polluting vehicles entering central London, starts later this month.
Clean air campaigners have welcomed the moves but called on him to go further and take more immediate action to tackle the crisis. 
They are also calling on him to halt major road developments  such as the Silvertown Tunnel project  which they say will increase pollution in the capital.
Areeba Hamid, clean air campaigner at Greenpeace said: Asking car manufacturers to take responsibility for their part in Londons air pollution crisis is an essential first step towards holding them accountable.
A clean air fund that major car manufactures contribute towards could help, provided manufacturers agree to ditch diesel completely. In the meantime, new Euro 6 cars should be subject to ULEZ regulations if we are to clean up Londons air and keep it that way.
