__HTTP_STATUS_CODE
200
__TITLE

Grant Shapps is but the latest in a long line of Tory plotters

__AUTHORS

Martin Kettle
__TIMESTAMP

Friday 6 October 2017 07.07EDT

__ARTICLE
As a Conservative plotter, Grant Shapps stands on the shoulders of giants. There have been Tory plotters for as long as the Tory party has existed. It is more than 200 years since a member of the Tory cabinet during the Napoleonic wars, the war minister Viscount Castlereagh, fought a duel with another, the foreign secretary George Canning, because of the latters plotting (they both had to resign). 
And it is nearly 150 years since Benjamin Disraeli, who in his early career had plotted against Robert Peel (successfully), became prime minister and told friends, in words that all politicians remember to this day: Yes, I have climbed to the top of the greasy pole. 
Few Conservative leaders forget that strung out beneath them on the greasy pole are an endlessly replenishing and competing succession of rivals who want to reach the top, or at least pull the incumbent down in support of someone they hope will better recognise their talents. 
Most Tory premiers, and Theresa May is no exception, rely on the chief whip, today Gavin Williamson, and key allies to keep an eye on plotters. Mainly they can echo Labours Harold Wilson who, when challenged about plots against him in the 1960s, responded: I know whats going on. Im going on.
May is a very weak prime minister because of her election loss in June, but she is in no way unique in facing malcontents who want her out. Winning leaders and losing leaders have rivals and enemies. It is less than two years since Conservative backbenchers such as Nadine Dorries and Andrew Bridgen were trying to force David Cameron out of Downing Street shortly after he had led the Conservatives to a majority victory in 2015, well before the EU referendum. 
A Tory leadership contest can be triggered in two ways: if the leader resigns, as David Cameron did after the EU referendum, or if 15% of the partys MPs demand one.
In the current parliament, that would mean 48 Tory MPs would have to write to Graham Brady, the chair of the powerful backbench 1922 Committee, to say they have lost confidence in Theresa May.
Once triggered, MPs narrow down the field of potential candidates (five in the 2016 contest) in a series of weekly votes, with the weakest being eliminated each time until two remain. This pair are then presented to party members, who have the final say in a one-member-one-vote contest.
Crucially, if May resigned, she would not be able to stand against her challengers.
As so often in Tory intrigues, Europe is at the heart of the recent plots and discontent. Mays fundamental difficulties are that she blew her majority in June and is trapped between pro-European Tories who think her approach to Brexit is too hard and anti-Europeans who suspect she will sell them out in the end. Hostility against Cameron crystallised around Europe, while John Major and Margaret Thatcher were felled by Conservative splits over the EU.
Major was eventually brought down because, taking their cue from the retired but still influential Thatcher, the Tory right thought he should have vetoed the Maastricht treaty, rather than relying on substantial opt-outs on issues including the single currency and social chapter. 
Tory backbenchers and cabinet ministers stuck by Major in public, but behind the scenes, the disloyalty and disarray was venomous. Major referred to them as the bastards and called his opponents bluff by standing for re-election as party leader (and thus as prime minister) in June 1995, defeating John Redwood and pushing Michael Portillo to bottle his own challenge. It is just conceivable that May could resort to this ploy.
Thatchers fall in 1990  Philip Hammonds budget next month is scheduled to take place on the 27th anniversary of her forced departure  was the result of a brilliantly executed pincer movement between the ousted Geoffrey Howe, who delivered the speech of his life after his resignation, and the ambitious pretender Michael Heseltine, although Major was the ultimate beneficiary when the cabinet forced her to quit. 
Thatchers ultimate problem was hubris. She took the party for granted. But she had faced quixotic challenges before, potentially from Geoffrey Rippon in 1981, and Ian Gilmour and David Howell later in the decade. But it was not until the anachronistic but carefree pro-European Sir Anthony Meyer ran as a stalking horse against her in 1989, collecting 33 votes and with 27 MPs (including Heseltine) abstaining, that the Thatcher mystique began to crack.
Thatcher had been the beneficiary in 1975 of another well-plotted coup, this time against Edward Heath, the first elected leader of the Conservative party. It was not the first assault on Heath, who had been challenged by Edward du Canns Milk Street Mafia the year before Thatcher ran against him and won. 
Though a former chief whip, Heath had also become detached from his party in ways that echo through the years in criticisms of Thatcher and May. Former party official John Ranelagh once wrote that the high Tory wing of the party always looked down on Heath, while business-focused Tories thought he was too soft. But, Ranelagh continued, the largest body of enmity towards him consisted of backbench MPs. They were people who made a temperamental, not a class or an operational, critique of Ted. They were simply in the ordinary [battle]field of politics observing that they might well be killed in an attack their general did not believe was coming.
In the end, its the backbenchers who count more than anything. That will be as true for May as it was for Cameron, Major, Thatcher and Heath. But threats from rivals are a universal political reality. 
In the latest volume of Alastair Campbells diaries, the former No 10 aide describes Tony Blairs anguish about the constant plotting against him by his eventual successor Gordon Brown. What more does he want? Blair asked Campbell in 2005. How do you deal with him? I say Im not standing at the next election. I endorse him as successor. I say if he cooperates Id be happy to fuck off now. And still he wants more. No other PM has had to put up with something like this.
Oh yes they have. And they always will.
