__HTTP_STATUS_CODE
200
__TITLE

Transport and power disruption continues as Storm Ophelia moves on

__AUTHORS

Matthew Weaver, 
Henry McDonald and 
Nicola Slawson
__TIMESTAMP

Tuesday 17 October 2017 14.39EDT

__ARTICLE
Storm Ophelia is finally moving away from Britain and Ireland, leaving a path of destruction in its wake and sparking a major cleanup operation.
Hundreds of thousands of people remain without power in the UK and Ireland while several key rail lines were blocked in northern England and Scotland.
A yellow weather warning for high winds in Scotland and northern England was lifted a day after the storm ripped through Ireland, causing three deaths and widespread disruption. Although the Met Office confirmed the storm had moved on to the North Sea, a spokesperson said a spell of very windy weather is likely.
In northern England, Electricity North West confirmed that engineers had restored power to another 1,100 properties affected by the former hurricane in Cumbria, taking the total restored since Monday evening to more than 19,000.
Our Teams working to restore you as quickly as we can after #Powercuts caused by storm #Ophelia - Current outages below - Rachel pic.twitter.com/djcO0I0XuQ
Chris Fox, network systems manager for the supplier, said just 700 properties remained without power on Tuesday evening. When the engineers completed work they would be sent to Ireland to help restore power there, he added. 
About 216,000 customers were still without power on Tuesday evening in the Republic of Ireland, with the worst damage in the southern part of the country, where around 80,000 people remain without water. Irelands electricity supply board warned it could take up to four days to restore power. 
In Northern Ireland over 50,000 households lost power during the storm and 2,000 customers remained without power, a statement from NIE Networks said. A further 2,000 in north Wales and over 1,000 in Scotland were still without power on Tuesday night. 
 Rail services between Edinburgh and Aberdeen, Dundee, Fife and Perth were suspended after a freight train was thought to have hit a tree on the line near Markinch.
The train was removed but disruption continued through much of Tuesday. 
Services have also been affected between Glasgow and Edinburgh after a branch hit overhead power lines in Bellshill. Services to Neilston, south-west of Glasgow, were cancelled because of an obstruction on the track. 
The train operator Northern said several trees were blocking the line between Halifax and Bradford Interchange. It warned that trains may be cancelled or diverted. 
There was a further report of a landslip on the line and passengers were warned that rail replacement services could be affected by the poor road conditions. 
Trains were also unable to run between Manchester airport and Wilmslow station in Cheshire after a tree fell on overhead electrical wires. 
Virgin Trains said a tree blocking the railway at Lockerbie was causing disruption to journeys and work was under way to remove it. Police in the coastal Cumbrian town of Barrow urged people to avoid the towns football ground after the wind ripped off part of the roof of its main stand.
Meanwhile, schools on both sides of the Irish border remained closed for a second day as authorities began to assess the damage. 
Ireland experienced the worst of the weather on Monday, with winds of almost 100mph damaging electricity networks and causing widespread disruption and three deaths. 
An Irish cabinet minister condemned those people who swam in the Irish sea, walked on piers and windsurfed while the storm was raging. 
The defence minister, Paul Kehoe, accused the thrillseekers of putting at risk the lives of Garda officers and members of the emergency services. Kehoe said it was absolutely ludicrous, stupid and totally inappropriate that some chose to risk not only their own lives but the lives of their potential rescuers during the storm. 
He added: A garda in Galway tried to stop those people going into Salthill, but they didnt heed his advice. Had they gotten into trouble the emergency services would have been called.
A spokeswoman for the Department of Education in Northern Ireland justified the decision to keep the regions schools closed despite the worst of the storm having passed. She said: Other considerations included potential power outages in schools, risks to travel arrangements including school buses and timelines for inspection of safety at schools as a result of the severe weather conditions. 
Northern Ireland, south-west Scotland, north-west England and north-west Wales were no longer covered by the latest Met Office warning.
The forecaster Steven Keates said commuters should expect very gusty conditions, with winds of up to 70mph. He said: The strong winds will continue but should moderate a little bit compared to what we have seen. Theres still a risk of gales and its still strong enough to cause disruption, but a little bit down on what we have seen. 
