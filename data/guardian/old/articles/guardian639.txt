__HTTP_STATUS_CODE
200
__TITLE

'We're not Game of Thrones'  Curb Your Enthusiasm's stars on their unsexy TV sensation

__AUTHORS

Jake Nevins
__TIMESTAMP

Sunday 1 October 2017 08.03EDT

__ARTICLE
Curb Your Enthusiasm is not like other shows. Episodes are essentially written while theyre being filmed, with stars improvising from story outlines. There are few deadlines, little direction and barely anything that would count as a costume. Often, the gaps between seasons stretch into years and nobody, apart from the man at the centre of it all, has any idea what happens next.
That man, of course, is Larry David, Americas most gifted documentarian of trivial social injustice. And after six years, a TV movie and a Broadway show, David returns this week with the ninth season of Curb. As the promotional posters put it: We need him now more than ever.
When season eight ended back in 2011, with David convinced that Michael J Fox was using Parkinsons disease to torture him, Jeff Schaffer, the shows executive producer, wasnt sure thered be another. But there was one person even more dubious: David himself.
Every season is the last, Schaffer tells me. The last episode of season five was literally called The End. Larry would never want to do another season if he didnt have any good ideas. But hes always the only person on the planet who doesnt think hes going to come up with more.
David always carries a notebook in which he scribbles his daily gripes and grumblings, which often take shape as storylines. In the last few years, the stories stacked up and up. Larry kept saying things like, This would be good on Curb, Schaffer says. Hed ask me, Do you think anybody would want to see it? And I go, Yeah!
When David called HBO, which gives him a huge and unprecedented amount of creative freedom, the network was chomping at the bit to bring back its comedy juggernaut. Hes the only person in Los Angeles who works like that, says Cheryl Hines, who plays his now-ex-wife on the show. Theres always an open-door policy for Larry. 
The reason for that, Hines adds, is simple. Its kind of an easy, low-maintenance show. Were not Game of Thrones. Nobody is flying through an exotic land. Larry wears his clothes and gets up and shoots. 
Once, when filming an episode, David forgot to bring back the clothes he wore in his original scene. This mishap led to one of Curbs best episodes, Krazee-Eyez Killa, where David races around LA to find an exact replica of a sports coat he wore in the fictitious Martin Scorsese film hes starring in.
How the show gets written is Larry comes in and says, Oh my god, do you know what happened to me last night?, Schaffer explains of the everyday nuisances  people who take up two parking spots, eavesdropping waiters, dinner party fiascos  that worm their way into Curb. This guy did this and I should have said that. But Show Larry will say that, because Show Larry is the neurotic Superman to Larrys Clark Kent. As long as he keeps walking around in the world, hes going to get himself into trouble  and thats going to get on the show.
You would imagine him sitting by himself in the corner just waiting for the scene to start, says Hines of filming Curb, crossing her arms in imitation of the curmudgeon David portrays on-screen. But he is the exact opposite of that. When Susie is reaming him out he is so happy, it makes him joyful.
Hines is referring to Susie Essman, who plays the wife of Davids on-screen manager Jeff and has at her disposal a Rolodex of alliterative insults  four-eyed fuck; misanthropic moron  for when David commits yet another social indignity. She and David have been in each others orbits since their days at Catch a Rising Star, the comedy club where they used to share the stage with the likes of Chris Rock and Jon Stewart.
My recollection of Larry in those days is him standing around the bar, regaling his tales of dating woes, says Essman. All of those became Georges storylines [on Seinfeld]. Hes somebody that knows how to use his life as a goldmine. But if I said in 1986 that Larry David is going to be richer and more successful than any of us, they wouldve laughed in my face. He was always a brilliant comedian, but he was never the kind who could connect with an audience. He was not going to have a career as a standup comic.
But if Davids comic sensibilities didnt translate on stage, they surely do on set, where he often thinks of lines mid-take and can barely stay composed while he delivers them. The great thing about Larry is hes writing as hes acting, says Schaffer. Its like a live comedy sporting event. Hell be in a scene and start to laugh because he knows what hes about to say.
That approach not only brings out the best in David, but in his co-stars, too. I have gotten some comedy scripts that are so not funny, says Essman, and they say, you can make it funny. No you cant! Thats not my job. But I know that if Larry sets it up and its supposed to be funny, then its going to be funny.
JB Smoove, whose character Leon takes refuge in Davids house in season six, becoming his brother-in-arms and misanthropy, routinely runs into fans who quote his best lines back at him. I was in New Orleans, helping my wife and kids out of the car door, Smoove tells me. Some guy did a drive-by and said get that ass, Larry! Were still living off lines we said nine years ago. 
Fans are ready for David to once more be their voice of reason in an unreasonably chaotic time. According to Smoove, there is no one better to help navigate the fraught political terrain. This is a good time for honest humour, he says. Comedians have the highest tolerance for pain, for rejection, and we take it, filter it through our own bodies and spit that shit right back out.
Though it first aired 17 years ago, Curb  and the lovable, incorrigible pariah at its core  still has plenty to say: As long as there are awkward situations and petty grievances and social interactions that dont go the way you think they should, Larrys voice will ring true, says Schaffer. The one thing were always making more of is terrible people. That tap is still flowing quite nicely.
Curb Your Enthusiasm returns to HBO in the US at 10pm on 1 October, and in the UK at 10pm on 2 October on Sky Atlantic and NOW TV.
