__HTTP_STATUS_CODE
200
__TITLE

Genius crossword No 172

__AUTHORS
Chandler
__TIMESTAMP

Sunday 1 October 2017 19.01EDT

__ARTICLE
The solution to 22,23 across, not further defined, explains how eight others need to be treated before entry in the grid. The clues for these eight contain a conventional definition and their wordplay produces the letters to be entered, which are not real words.
 Deadline for entries is 23:59 GMT on Saturday 4 November. You need to register once and then sign in to theguardian.com to enter our online competition for a 100 monthly prize.
Click here to register
