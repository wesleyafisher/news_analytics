__HTTP_STATUS_CODE
200
__TITLE

10 national monuments at risk under Trump's administration

__AUTHORS

Oliver Milman
__TIMESTAMP

Monday 18 September 2017 16.22EDT

__ARTICLE
A total of 10 US national monuments are in the Trump administrations sights to be either resized or repurposed, in order to allow activities such as mining, logging and grazing within their borders. Environmental groups have vowed legal action to stymie any alterations to the protected areas. Here are the 10 national monuments identified for change by Ryan Zinke, the secretary of the interior.
Designated in December 2016 by Barack Obama, Bears Ears national monument is a 1.35m-acre expanse of mesas, buttes and Native American archaeological sites that sprawls across south-eastern Utah. Its many splendors include a series of stunning rock bridges as well as the aptly named Grand Gulch, an intricate canyon system thick with thousand-year-old ruins.
A whopping 1.9m acres, south-central Utahs Grand Staircase-Escalante was set aside by Bill Clinton in 1996 and is the largest terrestrial national monument in the US. It contains a series of gigantic plateaus and cliffs, the Grand Staircase, as well as a string of deep gorges known as the Escalante River Canyons.
The first national monument established solely to protect its rich biodiversity, Clinton deemed the Cascade-Siskiyou an ecological wonderland when he protected it at about 52,000 acres in 2000. In his final week in office, Obama responded to calls from local conservationists and scientists and expanded the monument, adding approximately 48,000 acres.
Covering nearly 300,000 acres of remote desert north-east of Las Vegas, the Gold Butte monument was created by Obama in December 2016. Its chiseled red sandstone towers, canyons and mountains contain a treasure trove of rock art and are an important habitat for species such as the Mojave desert tortoise, bighorn sheep and the mountain lion.
Roxanne Quimby, co-founder of Burts Bees cosmetics, and her foundation purchased tracts of land in the northern reaches of Maine with the purpose of creating a national park. When this plan was opposed by various state and federal politicians, Obama stepped in to create a 87,000-acre national monument, dominated by mountains and lush forests.
Another Obama creation, the marine monument was designated in September 2016 and sits off the New England coast. The area was protected to safeguard an ecosystem of deep sea corals, three species of whale and an endangered species of sea turtle, the Kemps ridley.
A huge monument, spanning nearly 500,000 acres and proclaimed in May 2014. There are several hundred known archaeological sites in this mountainous stretch of New Mexico, including some of the earliest-known native American settlements. In the 1960s, US astronauts used the area to train for lunar missions.
Declared by President George W Bush in 2009 and expanded by Obama in 2014, the monument covers 480,000 square miles in marine areas to the south and west of Hawaii. The scattered reserve contains rare birds, trees and grasses as well as largely untouched coral reefs.
Found at an average elevation of 7,000ft, this New Mexico monument was created in 2013. The area is riddled with volcanic cones, with the Rio Grande flowing through an 800ft gorge in the layers of volcanic basalt flows and ash. The monument has several archaeological sites and is considered a key wildlife corridor for migrating animals.
The enormous 8.5m acre monument in the south Pacific was declared by Bush in January 2009. Rare petrels, shearwaters and terns are found there, as well as giant clams, reef sharks and rose-coloured corals. It is considered by the Fish & Wildlife Service as the most important seabird habitat in the region.
