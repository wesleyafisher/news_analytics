__HTTP_STATUS_CODE
200
__TITLE

Schubert/Zimerman: Piano Sonatas D959 and D960 CD review  thoughtful and technically impeccable

__AUTHORS

Andrew Clements
__TIMESTAMP

Wednesday 11 October 2017 10.57EDT

__ARTICLE
Krystian Zimerman was 60 last December. As he reveals in an interview included with these performances, approaching that milestone convinced him it was time to find the courage to record the late sonatas by Schubert and Beethoven that he has been including in his recitals for more than 30 years. The first result of that decision is this pairing of the final Schubert sonatas. Other than a performance of Grayna Bacewiczs Second Piano Sonata (included alongside her two piano quintets on a DG disc of six years ago), these are Zimermans first solo piano recordings since his Debussy Prludes appeared in 1994.
Characteristically, the performances have been prepared with immense care. To recreate something of the sound world that Schubert would have known, Zimerman used a tailor-made piano, replacing the standard Steinway keyboard and action with one he designed and made himself. The hammers strike the strings at a different point, creating a new set of overtones and hence a different range of keyboard colours, and the action becomes lighter too. (Played on a modern grand, he says, the many repeated notes in Schubert could turn into Prokofiev.)
 Certainly there is nothing remotely percussive here. The piano sound lacks the fulsome insistence of a modern instrument, but is also without the differences between registers that a period one would have, and with strictly rationed use of the sustaining pedal, it allows Zimerman to create textures of fabulous transparency and flexibility, especially in the slow movements of both sonatas. 
The A major Sonata, D959, may lack some of the rugged grandeur of, say, Rudolf Serkins classic recording, just as the bass trill that punctuates the first movement of the B flat, D960, is less minatory than in some performances. Zimermans performance of that sonata is worlds away from the dreamy spaciousness of Sviatoslav Richters approach, too. But both sonatas have an unfailing sense of rightness and proportion to them, with every choice of tempo seeming natural and unforced, while the attention that Zimerman pays to the minutest detail of the phrasing never becomes an end in itself. 
There are certainly more strikingly individual recordings of both sonatas to be found, good and less good, but few that are so consistently thoughtful and musical, and so technically impeccable too.
