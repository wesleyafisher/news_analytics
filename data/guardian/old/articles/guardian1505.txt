__HTTP_STATUS_CODE
200
__TITLE

Anti-Isis coalition risks descending into war before caliphate crushed

__AUTHORS

Patrick Wintour
__TIMESTAMP

Monday 16 October 2017 10.37EDT

__ARTICLE
With Islamic State days from being ousted from its Syrian stronghold of Raqqa, and ejected two months ago from Mosul in Iraq, the western anti-Isis alliance should be congratulating itself.
Instead, it finds the two ground forces that did most to expel Isis, which are armed, trained and supported by Washington, at each others throats, with tensions concentrated on the oil city of Kirkuk.
The hardline military response from Iraq and Iran in the past 24 hours to the Iraqi Kurds decision to hold a referendum on independence, a vote strongly opposed by every western state, risks a new war that could destroy the unity of the Iraqi state.
The nightmare is a Turkish or Iranian occupation of parts of Kurdistan leading to a guerrilla war before the Isis dream of a caliphate is crushed.
Already there is revived talk of disenfranchised Sunnis following the Kurdish path to independence by seeking to form their own state in Iraq  the genesis of Isis in 2014.
Sunnis reason that, with the Kurds, they make up approximately 40%-45% of Iraqs population, but if the Kurds secede from Iraq, the remaining Sunnis will form a smaller rump in an overwhelmingly Shia state, which will make them more vulnerable to sectarian persecution.
There are also concerns about the influence of the Iranians over the Baghdad government; Maj Gen Qassem Suleimani, the leader of Irans revolutionary guard, has been in Baghdad for the past three days.
The consequences of the standoff in Kirkuk could be catastrophic, according to Jack Lopresti, the chairman of the all-party parliamentary group on Kurdistan. 
It would be catastrophic for Kurdish-Arab relations were the Iranian proxy militia to use American weapons against our vital allies in the peshmerga, he said.
It would demonstrate a complete abdication of responsibility for Iranian-backed forces that have no interest in a peaceful settlement between Baghdad and Erbil [the capital of the Kurdistan region]  the key actors in post-referendum discussions. The west cannot stand idly by as the Shia militia and Iran exploit differences within Iraq for their own selfish ends.
It is possible there was miscalculation on both sides. Some initially interpreted the Kurdish referendum not as an outright declaration of independence, but another bargaining chip to restart talks on greater autonomy from Baghdad. Instead, nationalism pushed the Kurdish leadership further than it intended.
An amicable divorce was always unlikely due to the formal incorporation into Kurdistan of disputed territories such as Kirkuk, an emotive issue for Kurds and Arabs given its oil and strategic position.
The Kurds may have been overoptimistic about the reaction of the Turks, believing Ankaras dependence on Kurdish oil, and wider commercial relations built up in recent years, would be enough for Recep Tayyip Erdoan to show flexibility.
The Kurds, too, may have underestimated the influence that Iran, and its popular mobilisation units, hold in Baghdad.
Western diplomacy may have been too little, too late. After watching the abject performance of Iraq state forces in the fight against Isis, the US under Barack Obama poured millions of dollars into the Kurdish peshmerga to try to defeat the terror group. They largely proved their value, and have been held in the highest standing in the US. The British and the Americans warned that a referendum would be destabilising and premature, but they found they did not have the leverage to stop the Kurdish leader, Masoud Barzani, pressing ahead.
Rex Tillerson, the US secretary of state, reportedly waited until 23 September, two days before the referendum, to offer Erbil a one-year window of dialogue on all outstanding issues, in conjunction with the UK. If Baghdad did not negotiate in good faith, Tillerson proposed, we would recognise the need for a referendum.
The Kurds say they have heard such promises before and no date for a referendum was offered. From the Kurdish perspective, the Shia ruling elites in Baghdad want majority rule and the majority of the oil revenue.
If the US is to prevent war inside Iraq between two of its allies, it may have to go back and strengthen the promise in the 23 September letter, or see its painstakingly assembled anti-Isis alliance dissolve in front of its eyes.
