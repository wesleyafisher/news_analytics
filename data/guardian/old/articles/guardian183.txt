__HTTP_STATUS_CODE
200
__TITLE

The Resistance Now: Sign up for weekly news updates about the movement

__AUTHORS
Guardian staff
__TIMESTAMP

Tuesday 7 March 2017 07.00EST

__ARTICLE
From the womens marches, to airport protests, to packed town hall meetings, millions of Americans have been seeking out ways to curb what they see as the damaging actions of an unpredictable regime. In the process, theyve begun to lay the foundations for a new era of grassroots activism thats becoming one of the defining news stories of the Trump era.
The Guardian is launching The Resistance Now to cover the emerging campaigns around topics such as climate change, reproductive rights, equality, immigration, racial justice and more.
Sign up above for a weekly newsletter and occasional updates detailing whats up with the resistance now.
