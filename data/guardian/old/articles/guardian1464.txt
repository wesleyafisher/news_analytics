__HTTP_STATUS_CODE
200
__TITLE

Trump optimistic at Neil Gorsuch's swearing-in after weeks of tumult

__AUTHORS

David Smith in Washington
__TIMESTAMP

Monday 10 April 2017 14.01EDT

__ARTICLE
The absence of three men hovered over the White House rose garden on Monday as Neil Gorsuch was sworn in as the 113th justice of the US supreme court.
One was Antonin Scalia, the conservative justice whose death 14 months ago created an abrupt vacancy on the court. Another was Merrick Garland, the judge who stood on the same spot in March last year as Barack Obama issued a dire warning that if Republicans blocked his nomination it would imperil American democracy.
The third missing man was Mitch McConnell, the Republican majority leader in the Senate, who ignored Obamas words and took an audacious gamble. It paid off when Trump won the presidential election and put forward Gorsuch instead, but the long-term consequences for democracy are unknown.
Although he could not be here today, I especially want to express our gratitude to Senator Mitch McConnell for all he did to make this achievement possible, Trump told a gathering that included Scalias widow, Maureen, and every sitting supreme court justice. So thank you, Mitch.
McConnell not only refused to grant Garland a hearing, contending that it was an election year and the next president should get to fill the vacant supreme court seat, but last week he also triggered the so-called nuclear option to change Senate rules when Democrats refused to give Gorsuch an up-or-down vote. 
This bitter partisanship, and the potential damage to the Senate, was studiously ignored during Mondays 15-minute ceremony, and there was no reference to Garland. Instead, basking in sunshine, Trump was eager to seize on what he hopes will be the biggest boost to his rocky presidency so far.
Spring is really the perfect backdrop for this joyful gathering of friends, because, together, we are in a process of reviewing and renewing, and also rebuilding, our country, he declared from the lectern. A new optimism is sweeping across our land, and a new faith in America is filling our hearts and lifting our sights.
He added: Ive always heard that the most important thing that a president of the United States does is appoint people  hopefully great people like this appointment  to the United States supreme court. And I can say this is a great honour. And I got it done in the first 100 days  thats even nice.
There was laughter as he asked: You think thats easy?
But for all the bombast and irreverence of his campaign and early presidency, there are occasions such as this when even Trump has to follow protocol and ceremony. He acknowledged the incredible wisdom of the founding fathers in creating the separation of powers and looked on as the oath was administered in time honoured fashion.
In nominating Gorsuch, the president said he fulfilled a campaign pledge to pick someone in the mould of Scalia, who spent three decades on the court with a similar originalist approach to the law, interpreting the constitution according to the meaning understood by those who drafted it. 
Trump praised Gorsuch, 49, a former appeals court judge from Colorado, as a jurist who will rule not on his personal preferences but based on a fair and objective reading of the law.
The president said Americans would see in Gorsuch a man who is deeply faithful to the constitution of the United States and added: I have no doubt you will go down as one of the truly great justices in the history of the United States supreme court.
Gorsuch was sworn in during the ceremony by justice Anthony Kennedy, for whom he once served as a law clerk. He rested his left hand on a Bible held by his British wife, Louise, whom he then embraced warmly as Trump applauded enthusiastically. It was the second of two oaths; the first was conducted privately in the justices conference room by chief justice John Roberts.
In remarks in the rose garden, Gorsuch said he was humbled by his ascendance to the nations high court and thanked his former law clerks, telling them: Your names are etched in my heart forever.
This process has reminded me just how outrageously blessed I am in my law clerks, and my family, and my friends, he said. And I hope that I may continue to rely on each of you as I face this new challenge.
Turning to Scalias widow, he said: To the Scalia family, I wont ever forget that the seat I inherit today is that of a very, very great man.
Gorsuch, watched by his daughters Emma and Bindi, promised to be a faithful servant of the constitution and laws of this great nation.
Matt Schlapp, chairman of the American Conservative Union, was among those present. Garland is a footnote to history, he said afterwards. Im sure his friends wish everything had turned out differently but the fact is presidents get to make these appointments in consultation with the Senate and it wasnt to be.
Schlapp described Mondays ceremony as as a huge boost for Trumps presidency. I dont think you can amplify it enough.
The location of the swearing-in marked a departure from recent precedent. The two supreme court justices successfully nominated by Barack Obama, Sonia Sotomayor and Elena Kagan, were both sworn in publicly at the Supreme Court. 
Former justice John Paul Stevens has argued that holding the public ceremony at the court helps drive home the justices independence from the White House, the Associated Press reported.
Gorsuch will be seated just in time to hear one of the most significant cases of the term: a religious rights dispute over a Missouri law that bars churches from receiving public funds for general aid programmes.
