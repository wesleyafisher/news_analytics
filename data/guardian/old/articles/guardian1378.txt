__HTTP_STATUS_CODE
200
__TITLE

Genius crossword 160

__AUTHORS

Puck
__TIMESTAMP

Sunday 2 October 2016 19.01EDT

__ARTICLE
Twelve solutions (not further defined) belong to one of two groups, which combined lead to a 23 25. The definitions in each of the remaining clues contain a single letter misprint. These incorrect letters, in clue order, spell out two possible locations for another sort of 23 25.
NB The following solution counts are for more than one word:
 Across19 (3,3)23,25 (6,6)Down6 (3,5)
Deadline for entries is 23:59 GMT on Saturday 5 November. You need to register once and then sign in to theguardian.com to enter our online competition for a 100 monthly prize.
Click here to register.
