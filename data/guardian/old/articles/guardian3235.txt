__HTTP_STATUS_CODE
200
__TITLE

Clearing the hurdles: why we're keeping the spotlight on women's sport

__AUTHORS

Owen Gibson
__TIMESTAMP

Saturday 12 August 2017 07.42EDT

__ARTICLE
Eight years ago, when I was sports news correspondent for this newspaper, I found myself hastily dispatched to Helskini to speak to Englands women footballers. They had reached the final of Euro 2009 and, a successful England team being a welcome novelty, we wrote of the hope that their success would change perceptions of womens sport in this country.
Similar predictions were made after Heather Stanning and Helen Glover took Britains first gold during that shimmering London 2012 summer, when the Lionesses came third in the 2015 World Cup in Canada, or after nine million people watched Britains heart-stopping hockey victory to win gold in Rio. Yet change remained incremental rather than incredible.
But, just as the expectations of Englands women footballers were worlds apart in 2009 and 2017, so this summer has felt different. 
First the ICC Womens World Cup was clinched by Heather Knights side against India in front of a capacity crowd at Lords. Then, Mark Sampsons England team swept to the semi-finals of Euro 2017 before coming a cropper against the swashbuckling Dutch hosts, who went on to win an entertaining final that captivated the country.
On Sunday, ITV1 will air Englands clash with Italy in Dublin, as Sarah Hunters side attempt to retain the womens Rugby World Cup, having thrashed Spain in their opening match. 
In a summer without an Olympics or a mens football World Cup, these events have been able to elbow their way into the public consciousness. They have done so entirely on their own terms  and not just in Britain. 
At the opening ICC Womens World Cup dinner, the ambition was not only for full stadiums and decent viewing figures but for an ignition of interest in India - the biggest cricket market on the planet. By the final, Indian media were trumpeting huge television audiences and the Guardians own over-by-over live blog was hitting 600,000 page views  the majority coming from India. 
The Guardian has always been more committed than most to covering the big female sports events, but the volume of our coverage, both online and in print, has visibly grown this summer. And with it, the readership.
National newspaper coverage of womens sport was estimated, in the most recent study in 2015, at just 2% of total sport coverage. This summer, womens sport has featured as the main article on the front page of the sports section nine times in the past three weeks, and has consistently been prominent on the sport section of the website, amid the usual transfer talk and mens Tests.
But as the Guardians head of sport, the question I find myself wrestling with is: how do we ensure that continues into the autumn, as the juggernaut of Premier League football starts rolling?
There are several, intertwined, issues here. One is that all sports have to compete for media coverage, and some are more popular than others. A second is that the Guardians resources and space are finite. A third is that the domestic game is at a much more nascent stage  in football, cricket and rugby  than the international one. 
The FA-backed Womens Super League (WSL) has never really quite taken off in the way that its most fervent supporters hoped. But some big clubs  Chelsea, Arsenal and Manchester City among them  are now fully committed and attendances are slowly growing. Yet still they remain more analogous to county cricket than even non-league football. It is a long game. 
Louise Taylor, who covered Euro 2017 and the 2015 World Cup for us, reflected on her return from the Netherlands: It felt significant that people were debating Mark Sampsons tactics rather than whether women should be playing football, or the quality on offer. The tougher challenge for the FA though is to generate interest in the weekly slog of the WSL, particularly now its a winter game. 
We have made a firm editorial commitment to promote womens sport where we can and, in particular, to make the absolute most of those big moments when the nation is engaged. But were also part of a patchwork that includes governing bodies, sponsors, broadcasters and fans. No part of that eco-system can manufacture interest  it has to be organically built.
But within that there are encouraging signs that momentum is building. Advertisers and sponsors are realising that backing womens sport takes them to new and different audiences. Sports once stuffy governing bodies  not least the three largest in the FA, the ECB and the RFU  are realising warm words wont be enough to turbo-charge growth and unlock major investment. 
The Guardian is doing its bit by committing to more regular coverage  we recently launched a weekly womens football blog helmed by Suzanne Wrack  and trying to prioritise womens sport where we can. We also devote our best people to finding the most interesting stories and interviewing the most engaging, talented personalities. 
But we cant necessarily manufacture an audience for this work. One of the disheartening things about this debate is the extent to which it is still measured in old media money. Because its easier to measure, surveys will often take into account the number of articles and photographs in print.
That, however, discounts our hugely engaging and popular over-by-over and minute-by-minute live blogs, which are just as important  if not more so  in terms of engaging large readerships.
Our Euro 2017 coverage, in which we live blogged all of England and Scotlands matches plus the final, was read by more than 1.8 million users. Our online womens cricket World Cup coverage topped more than two million. We live blogged La Course and, of course, Johanna Kontas run to the Wimbledon semi-finals. 
There may be a danger that we spend more time wringing our hands over the problems than getting on with finding a solution. As Anya Shrubsole, one of those victorious cricketers, told Andy Bull earlier this summer: Yes, we need to win, and we want to win, but the more we play in a way that people want to watch, the more of that coverage well get. So I think there is responsibility for both sides, for the players and the press.
Viewers, consumers and readers need to do their bit too. In a world where every click and interaction can be measured by publishers and advertisers, it is incumbent on those who want to read more about womens sport to make their views known, click on the stories and vote with their feet. 
There is an old adage that says you cant be what you cant see and for too long weve had to rely on a two-week window every four years to inspire generations of girls to want to play sport, says Alex Danson, another of that impressive group of hockey players who won gold in Rio.
This is clearly now changing but we still have a huge amount of work to do to make womens sport of interest and relevance 365 days of the year, every year. Responsibility for that lies with athletes, governing bodies, the media, broadcasters and sponsors  but if we get it right everyone wins. 
That is hard to argue with. And while the numbers are encouraging, there is something else about this summers boom that inspires a strange sort of confidence. 
In the backlash that followed Englands exit from Euro 2017, followed by more than 120,000 people on our live blog, discussions about Sampsons tactics and the way the players appeared to freeze on the big stage contained a tacit recognition that this was nothing to do with gender  but simply about the soaring highs and crushing lows of all sport.
