__HTTP_STATUS_CODE
200
__TITLE

Police show their true colours at fracking protest

__AUTHORS
Letters
__TIMESTAMP

Friday 13 October 2017 13.18EDT

__ARTICLE
When the police forcibly remove a 79-year-old woman for serving refreshments to fracking protesters, you know they have taken sides (Report, 11 October). Having wasted their time and our money dragging pensioners around, the Lancashire constabulary has asked the Home Office for an extra 3.1m to cover the cost of drafting in police from Somerset and Wales. It is time for the policing operation at New Preston Road to be scaled back, or called off altogether. The police are helping to impose a government decision to frack, which is opposed by local residents at every level of local government. The police should go back to patrolling the streets and arresting criminals, instead of defending corporate interests by harassing the protesters.Jenny JonesGreen party, House of Lords
 Join the debate  email guardian.letters@theguardian.com
 Read more Guardian letters  click here to visit gu.com/letters
