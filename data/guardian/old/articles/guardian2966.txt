__HTTP_STATUS_CODE
200
__TITLE

Country diary 1917: bungled wasps' nest theft leads to discovery

__AUTHORS

Thomas Coward
__TIMESTAMP

Sunday 15 October 2017 17.30EDT

__ARTICLE
Someone had taken a wasps nest. Perhaps as he carried it home some of the inmates objected, or he discovered that most of the cells were empty; at any rate it had been dropped, and lay broken by the path. Over the grubless cakes a few weary workers, chilled by the night exposure, crawled feebly, and three young queens refused to leave the ruins of what had once been their home. They seemed puzzled by the tragedy which had overtaken the busy colony, but they were too weak or too stupid to fly in search of shelter. Two of them died quietly in my killing bottle, but it was not until I pinned the bodies on a setting-board that I discovered that one was abnormal. Either through accident in her youth or from birth she was a cripple; the second and third legs on the right side were missing. Imperfectly developed insects are not rare; but the interesting point about this wasp was that she had made the best of a bad business. When she was alive I did not notice anything peculiar about her gait, but when I attempted to set her limbs I found that the third leg refused to remain on the left side. It was only then that I found that the right legs were missing, and that, in order to avoid the bias of three legs against one, the third left leg was bent under so as to work on the right side. I was sorry that I had not kept her alive to watch her manner of walking.
A Carnarvonshire correspondent writes that he saw a few swallows about a week ago, and, having seen some remarks in the Diary about late swallows, sends me the note. It is not late for stragglers; we often have some in November, though of course the majority have departed. We can, with fair accuracy, say when the last swallows go, for they are day-flying migrants. It is not so easy to tell when many other species leave. 
