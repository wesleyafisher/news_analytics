__HTTP_STATUS_CODE
200
__TITLE

Apocalypse wow: dust from Sahara and fires in Portugal turn UK sky red

__AUTHORS

Nadia Khomami
__TIMESTAMP

Monday 16 October 2017 11.18EDT

__ARTICLE
The strange reddish sky reported over parts of the UK may appear to some a sign of impending apocalypse or a celestial Instagram filter, but experts say there is a scientific explanation.
The hue is a remnant of Storm Ophelia dragging in tropical air and dust from the Sahara, while debris from forest fires in Portugal and Spain is also playing a part, according to the BBC weather presenter Simon King.
The dust has caused shorter-wavelength blue light to be scattered, making it appear red. Ophelia originated in the Azores, where it was a hurricane, and as it tracked its way northwards, it dragged in tropical air from the Sahara, King said.
The dust gets picked up into the air and goes high up into the atmosphere, and that dust has been dragged high up in the atmosphere above the UK.
The Met Office said the vast majority of the dust was due to forest fires on the Iberian peninsula, which have sent debris into the air. This has been dragged north by Ophelia.
Ophelia also pulled in unusually warm air from Spain and north Africa, which is why temperatures reached the early 20s over the weekend.
 An interesting phenomena that is a result of the movement of ex-Ophelia is the colour of the sky and the sun this morning, and dust on cars, the Met Office said.
 The same southernly winds that have brought us the current warmth have also drawn dust from the Sahara to our latitudes, and the dust scatters the blue light from the sun, letting more red light through much as at sunrise or sunset.
The red sky also created a vivid backdrop for television broadcasters; the BBCs Westminster interviews were conducted against a striking image of Big Ben.
