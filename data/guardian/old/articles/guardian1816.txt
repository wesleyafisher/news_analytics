__HTTP_STATUS_CODE
200
__TITLE

UK interest rate rise likely as inflation hits 3%

__AUTHORS

Larry Elliott Economics editor
__TIMESTAMP

Tuesday 17 October 2017 05.26EDT

__ARTICLE
The prospect of Britains first interest rate increase in more than a decade loomed large on Tuesday after inflation hit its highest level since 2012 and the Bank of England governor, Mark Carney, said it had further to rise.
Financial markets are now betting strongly on Threadneedle Streets monetary policy committee reversing the quarter-point cut in borrowing costs made in the aftermath of last years Brexit vote after the annual increase in the cost of living edged up from 2.9 to 3%.
Carney did nothing to defuse speculation of a rate rise when he gave evidence to the Treasury select committee, pointing out that the fall in the value of the pound since the referendum  which has pushed up import prices  will take up to three years to work its way through the economy.
Higher borrowing costs will come at a time when the gap between inflation at 3% and earnings growth at just over 2% is squeezing living standards. But in recent months a majority of MPC members appear poised to vote for a rate rise, fearing that the steady fall in unemployment will set off a spiral of rising prices and wages unless checked now.
One group that benefited from the increase in inflation will be Britains pensioners. The state pension will be uprated next spring by whichever is highest of the September consumer price index inflation rate, average earnings or 2.5%.

Inflation is when prices rise. Deflation is the opposite  price decreases over time  but inflation is far more common.
If inflation is 10%, then a 50 pair of shoes will cost 55 in a year's time and 60.50 a year after that.
Inflation eats away at the value of wages and savings  if you earn 10% on your savings but inflation is 10%, the real rate of interest on your pot is actually 0%.
A relatively new phenomenon, inflation has become a real worry for governments since the 1960s.
As a rule of thumb, times of high inflation are good for borrowers and bad for investors.
Mortgages are a good example of how borrowing can be advantageous  annual inflation of 10% over seven years halves the real value of a mortgage.
On the other hand, pensioners, who depend on a fixed income, watch the value of their assets erode.
The government's preferred measure of inflation, and the one the Bank of England takes into account when setting interest rates, is the consumer price index (CPI).
The retail prices index (RPI)is often used in wage negotiations.

Inflation has risen from 1% to 3% over the past year, largely due to the fall in the value of the pound, which has made imports dearer.
Speaking after the publication of the figures on Tuesday, Carney said it was more likely than not that he would soon be writing his ninth letter to the Treasury to explain his failure to keep inflation within 1 percentage point of its target of 2%. 
Under questioning from the Commons Treasury select committee, he said the weak pound after the Brexit vote was the sole reason for inflation rising as much as it has. He said the effect of the weak pound  still more than 10% down on the dollar  would still be felt three years after the referendum.
The governor said he needed to strike a balance between targeting rising prices and supporting jobs and activity with low rates. 
With unemployment at its lowest level since the mid-1970s, Carney said the trade-off has moved away, from using monetary policy to support growth and employment, paving the way for a rate rise. 
 In a breakdown of the latest inflation numbers, the Office for National Statistics said the cost of fuel and raw materials for industry was up by 8.4% on a year ago compared with a 7.6% increase in the 12 months to August. It said more expensive food and a smaller fall in airfares than a year ago were the main factors behind the rate rise, although clothes had come down in price.
The TUC general secretary, Frances OGrady, said: The government needs to face up to Britains cost-of-living crisis. The squeeze on household budgets is getting tighter by the month. 
The chancellor must use Novembers budget to ease the pressure on hard-pressed families. That means giving 5 million public sector workers the pay rise they have earned.
The head of inflation at the ONS, Mike Prestwood, said: Food prices and a range of transport costs helped to push up inflation in September. These effects were partly offset by clothing prices that rose less strongly than this time last year.
While oil and fuel costs continued to rise, overall the rates of inflation for raw materials and goods leaving factories were little changed in September.
Paul Hollingsworth, a UK analyst at consultancy Capital Economics, said a further increase in inflation to 3.2% was likely in October, forcing Carney to write to the chancellor, Philip Hammond.
However, we dont anticipate he will be writing letters for long. Indeed, we think inflation will be back below 3% by the end of this year. And while it looks set to remain above the monetary policy committee target throughout next year, we think it will end 2018 at around 2.25%. 
Meanwhile, Sir David Ramsden, the Banks new deputy governor, told the Treasury committee he wasnt among the majority of rate setters at Threadneedle Street who saw a need for a rate rise in the coming months. 
Silvana Tenreyro, who also recently joined the central banks monetary policy committee, said she could see the case for a rate rise but it would depend on the health of the economy. 
