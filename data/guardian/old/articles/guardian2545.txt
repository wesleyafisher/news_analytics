__HTTP_STATUS_CODE
200
__TITLE

Sir Chris Bonington: I was caught by the police when I was four years old

__AUTHORS

Martin Love
__TIMESTAMP

Saturday 14 October 2017 09.00EDT

__ARTICLE
Climbing Everest for the first time was fantastic. We had the mountain to ourselves. The summit is quite small, about the size of a pool table. Walking along the final ridge from the Hillary Step is a bit of a trudge. All I could think about was all the mates of mine who over the years had died up there.
The first time I showed my adventurous spirit I was about four. I opened the back gate of our flat in Hampstead to explore the heath. I was caught by the police and taken to the station at Belsize Park. Somehow I managed to get into their fridge and poured a pint of milk all over the inspectors desk.
I became a communist when I was 15. I wasnt idealistic, but I enjoyed the demonstrations
Climbing is not a matter of fear, even on very hard mountains. In a dangerous situation you are completely wrapped up in working out how you are going to get out of it. Solving that brings a wonderful satisfaction.
My dad always drank a lot. One night my parents had a huge argument and my mother knocked him unconscious with a fire poker. She panicked and fled. Later she crept back into the house to find a lot of blood, but no sign of him. That was the end of their marriage. I was about three months old.
I became a communist when I was 15. I wasnt very idealistic, but I enjoyed the demonstrations. But the activists were such dreary people. Then I decided Id rather like to go into the colonial service. Obviously, the colonial service and communism didnt sit very well together.
My wife Wendy and I were very liberal parents. We left our boys to do what they liked. They were wild teenagers  both were pretty much expelled from school. But they emerged as good men and have good marriages. And we made the transition from parental responsibility to loving friendship.
Ive always been interested in escape, not because I was unhappy, I think, but because my dad was in a German prisoner-of-war camp.
I was a management trainee with Unilever, working with the margarine people. It was worse than the army and dreadfully hierarchical. The offices, the canteen, even the toilets were all divided on class lines.
The climb that made me famous was the North Wall of the Eiger. It was the first British ascent. I sold the story to a chap from the Express for 1,000. The climb made more headlines than anything else Ive ever done.
Im delighted beards are back in fashion. Ive now had one continuously since July 1962
Wendy found she had motor neurone disease in 2012. With hindsight there were earlier signs. Shed started to fall over quite often. There is no cure and from there it was 19 months  mercifully it was quite fast. We dealt with it as best we could. She was incredibly positive and courageous.
Ive always wanted to please people, which in some ways is a good thing, but in others its very bad.
Im delighted beards are back in fashion. For me a beard was always a sign that I was on a climbing trip. Ive now had one continuously since July 1962.
Ive known Loreto [Herman Bonington, the couple married in 2016] for years. Ive always enjoyed her company, but shes younger than me and very vivacious. Our friends started to push us together so I invited her to dinner at the Alpine Club. I kissed her for the first time at a bus stop outside the Ritz.
Ascent by Sir Chris Bonington is published by Simon & Schuster on 19 October at 20. To order a copy for 17, go to theguardianbookshop.com
