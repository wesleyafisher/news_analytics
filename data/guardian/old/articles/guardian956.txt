__HTTP_STATUS_CODE
200
__TITLE

US special forces deaths in Niger lift veil on shadow war against Islamists in Sahel

__AUTHORS

Jason Burke Africa correspondent
__TIMESTAMP

Sunday 15 October 2017 04.00EDT

__ARTICLE
When four US special forces soldiers died in an ambush earlier this month in scrubby desert in western Niger, attention was suddenly focused on one of the most remote and chaotic war zones on the planet.
The US troops had been embedded with a larger unit of Nigerien troops and were attacked as they left a meeting with local community leaders a few dozen kilometres from the remote town of Tongo Tongo.
 Some reports claimed US troops were on a mission to kill or capture a high-value target in the area, perhaps even Adnan Abu Walid al-Sahraoui, the leader of the only local faction of fighters to have formally pledged allegiance to the Islamic State. 
 Given the light armament of the US detachment, the scramble to evacuate them, and the lack of medical backup or reinforcements, this seems unlikely. The US troops were eventually rescued by French aircraft, which flew from bases about 300 miles away in neighbouring Mali.
That there are conflicting accounts of the clash is not surprising. It occurred in an environment where hard fact is rare, and rumours swirl as fiercely as the dust storms that sweep the scrub and desert.
 Reuters news agency reported that the attackers were from al-Sahraouis group, which calls itself the Islamic State in the Greater Sahara.
Al-Sahraouis background and allegiance is evidence of the extremely fractured nature of the conflict across the swath of northern Africa known as the Sahel.
The 40-year-old is thought to have grown up in refugee camps in the south of Algeria, where he was committed to the nationalist cause of the Western Sahara. Little is known about how he became interested in Islamist extremism but by 2012 he was spokesman for the militant coalition that took over Timbuktu, the mythic city in northern Mali.
 French troops forced the militants back into the northern deserts of Mali in 2013, and a year later the rise of the Islamic State in Iraq and Syria (Isis) split their coalition.
Some factions maintained their ties to al-Qaida in the Maghreb, the tenacious and powerful local affiliate of the veteran group founded by Osama bin Laden in 1988.
 Al-Sahraoui went his own way with a few dozen extremist followers, eventually pledging allegiance to Isis in May 2015.
 In recent years disaffected extremist leaders have looked to Isis for sponsorship  and advantage over their rivals  in conflict zones from west Africa to the Philippines. 
 Often such pledges of support have been welcomed by Isis high command. But it took 18 months for al-Sahraouis bayat, or oath of allegiance, to be formally accepted  and only after he proved his mettle with a series of attacks. 
 This delay has raised doubts over the substance of al-Sahrouais links with Isis.
If it is hard to place factions in the Sahel on an organogram of extremist groups, it is even harder to place them on a map.
Other than Boko Haram, the Islamic State affiliate based in north-east Nigeria, no Islamic extremist faction in the Sahel currently has a fixed base, making it very difficult to track the whereabouts and evolution of any individual group.
 Even Boko Haram, which gained global notoriety by abducting more than 200 schoolgirls in 2014, has suffered serious losses of manpower, funds and territory in recent months, and is now very scattered.
Very few factions are looking to govern or control territory  These are highly flexible, mobile units moving frequently, changing areas and its very difficult to identify who is who and who controls what, said Rida Lyammouri, a US-based independent researcher on the Sahel.
The US defence secretary, James Mattis, described the group which attacked the US special forces as new to the area.
The dozen or so extremist factions operating across this vast area have, however, been responsible for scores of bloody attacks, and hundreds of deaths. Recent strikes have included a sophisticated assault on a military camp in Gao, Mali, which killed 77, and a series of attacks on hotels used by westerners and local elites.
Most of the groups responsible for these operations are linked to al-Qaida. yet even here there is little solidarity. A recent attempt by al-Qaida leaders to build a coalition among local jihadi groups is seen by analysts as evidence of quite how fractured the movement is in the region.
Ideology too is often unclear, with experts describing a complex mix of ethnic and other tensions which fuel militancy.
A recent report by the International Crisis Group noted that jihadist groups have established a presence in the northern Tillabery region, close to Tongo Tongo, through targeted recruitment of young members of the Fulani community  one of the largest ethnic groups in west Africa comprising mostly herders  who are looking for ways to counter their ethnic rivals or protect their businesses or communities.
All these armed movements demonstrate a genuine capacity to understand and adapt to local circumstances, and do so better than less-motivated capital-based elites or foreign peacekeepers  Instead, jihadist groups are willing to settle down and are developing an expertise in manipulating local intra and intercommunal tensions, the ICG said.
Extremist groups also provide limited services to some communities, most notably security, dispute resolution and rule of law.
 In central Mali, jihadist groups offer protection to pastoralists, and take the sides of local communities in contests over scant resources with government representatives or local elites.
 In this complex environment, western military units have to have significant autonomy in order to respond rapidly to local threats and conditions.
Reuters has reported that the US special forces soldiers abandoned, or at least extended, their more limited mission in Tongo Tongo when they learned of a raid nearby, deciding to engage the attackers themselves.
 Former western special forces officers with firsthand knowledge of current operations in the Sahel said this account was plausible.
Since [President] Trump took power, US forces deployed around the world have had a lot more room to manoeuvre. Decisions about when and what to engage have been devolved right down to unit level, the former officer said. Any soldier knows that if you give guys on the ground more independence, then they will be that much more aggressive and will take more risks.
