__HTTP_STATUS_CODE
200
__TITLE

Chipocalypse: potato shortage in New Zealand sparks crisp crisis

__AUTHORS

Eleanor Ainge Roy in Dunedin
__TIMESTAMP

Sunday 15 October 2017 22.52EDT

__ARTICLE
A year of heavy rains has devastated New Zealands potato crop, prompting fears of a chipocalypse.
The rainfall, which included two weather bombs and two serious floods in both the North and South Island, has wiped out 20% of New Zealands annual potato crop, and 30% in the regions most affected, with the crisping varieties for potato chips taking the biggest hit.
The news has sent some New Zealanders into a spin with concerns that the shortage might affect their Christmas feasts and favourite rugby snack.
Some supermarkets have put up signs in their crisp sections alerting customers to the shortage, and although no panic buying has yet been reported, the shortage has been well raked over on social media.
Re: NZ #chipocalypse. Yes, there's a potato shortage. But before anyone suggests them, kale chips are an abomination.
If we time the trademe auction right for Christmas, our 2 bags of Bluebird Chips should raise enough for a house deposit #chipocalypse
If it takes a #chipocalypse to make NZers serious about climate change, then so be it
Chris Claridge, the chief executive of Potatoes New Zealand, said it was concerning for such a serious shortage to hit a staple food product that many New Zealanders relied on to bulk up their meals, and supermarkets had begun to report shortages of North Island-produced crisps.
You can go for a week without politics but try going for a week without potatoes. It is a food staple and this is becoming a food security issue as the effects of climate change take their toll on our potato crop, said Claridge, who said access to farming land was also becoming scarce as urbanisation ramped up. 
Potato farmers have been severely impacted. If they cant harvest and process, they are not getting their income so there is that monetary impact. It is also quite distressing to be digging up rotting potatoes. It is a very important part of New Zealand psyche having potatoes.
Potato crops planted last year have either rotted in the soil due to the heavy rains, or had to remain unharvested because of the torrential downpours. Next years crop will also be affected because the ground has not been dry enough for planting.
Attention:  pic.twitter.com/tBRKFRmyGq
Potato-growing regions particularly affected by the weather include areas around Auckland and the Waikato, which have already experienced about 25% more rainfall than the annual average.
Foodstuffs, which owns New Zealands largest supermarket chains, said farmers had made them aware that potato products such as crisps and packaged, frozen chips might soon be affected, and the shortages could last into the new year. 
The grower community is highlighting a potential future potato shortage due to bad weather, said Antoinette Laird, head of external relations for Foodstuffs. This has not impacted our business as yet, and we are working with our suppliers to minimise any potential impact.
New Zealand produces 500,000 tonnes of potatoes a year and is the ninth-biggest exporter of potatoes in the world. About two thirds of the annual crop are destined for processing, with 250,000 tonnes being made into chips (as in fish and chips) and about 75,000 tonnes into crisps.
Bharat Bhanas family have been farming potatoes for 60 years in Pukekohe, which is south of Auckland. He said the heavy rains meant his business would run at a loss this year, with about a third of his potato crop destroyed.
This year has been very unusual, we havent experienced anything like it and it has really caught a lot of us out, said Bhana. I dont know how we can prepare for climate change. We could dig the spuds out earlier but then we wouldnt have enough space to store them and wed have to pay more to keep them cool.
Well do what we can, but what can we really do?
Potatoes have been grown in New Zealand for 250 years, according to Potatoes NZ, and were introduced by Captain Cook who gave potatoes to two Mori chiefs in Mercury Bay. The vegetable was quickly adopted as a staple part of the local diet because it could be grown in much colder areas than kumara, which was already a staple food in New Zealand.
