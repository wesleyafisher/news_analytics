__HTTP_STATUS_CODE
200
__TITLE

Six candidates who could succeed Bruce Arena as USA coach

__AUTHORS

Bryan Armen Graham
__TIMESTAMP

Friday 13 October 2017 12.59EDT

__ARTICLE
Porter had an undistinguished playing career that ended early due to injury but he is one of the better young coaches in MLS. He led the Portland Timbers to their first ever MLS Cup victory in 2015 and was successful working with younger players during his time in college soccer, where he won a national title with Akron. His teams tend to play possession based football. Porters time as USA Under-23 coach will count as a negative: they failed to qualify for the 2012 Olympics under his leadership.
As manager of the under-20s since 2011 and technical director for the past four years, Ramos has delivered a mixed bag of results: a group-stage flameout at the 2013 World Cup followed by quarter-final runs in 2015 and 2017. Developmental successes have included DeAndre Yedlin, Paul Arriola, Matt Miazga and Christian Pulisic, who will be integral parts of the senior team moving forward. A USA lifer who made 81 appearances and featured in three World Cups as a midfielder, the 51-year-old is the most logical in-house candidate.
The Sporting Kansas City skipper, who was capped 66 times as USA defender in the 1990s, and has thrived in Major League Soccer as both a player and manager, but his lack of international coaching experience could work against him.
The half-American manager responsible for saving Huddersfield Town from relegation, then overseeing their improbable promotion to the Premier League, was capped eight times for the US between 1996 and 1998. He oversaw the development of Pulisic as manager of Borussia Dortmunds reserves and prefers an attractive, full-throttle in attack and defense.
The Argentina native, who made 505 appearances with Newells Old Boys as a player, did a one-season spell with Barcelona as a manager three years ago and led Argentina to the Copa America final in 2015. His brief tenure with Atlanta United has been a rousing success and proof of his understanding of American soccer.
File this one under Amusing Things That Will Never Happen Department. Beckham has tried most other things: acting, charity work, video art, ambassadorships so why not coaching? He has close ties to US soccer after his time with LA Galaxy and his attempts to start an MLS franchise in Miami and knows the pressures and pitfalls of appearing at, and qualifying for, a World Cup (multiple World Cups in fact). He could even get his wife on board if the US need to redesign their jerseys.
