__HTTP_STATUS_CODE
200
__TITLE

Electric cars key to driving change in UKs energy supply industry

__AUTHORS

Adam Vaughan
__TIMESTAMP

Saturday 7 October 2017 18.08EDT

__ARTICLE
You drive home and plug in your electric car, telling an app how many miles you need the next day. As you eat dinner, relax and sleep, your energy supplier takes control of your battery, using it to buy power when its cheap, selling it back later when high demand pushes prices up.
The company offers national and local power grids services from your battery, making more money that it will later split with you, perhaps as free miles for your car.
This sounds like a far-off vision of the future. But the scenario will be reality next year for British households able to afford a new Nissan Leaf electric car and willing to allow energy firm Ovo to fit a special charger in their home.
The customer has to want to do it and understand it, and allow someone else to have some degree of control over their charging patterns for some form of rewards, said Tom Pakenham, head of electric vehicles at Ovo.
In his view, everyone benefits: individuals, suppliers and energy networks coping with the increasing, but variable, amount of wind and solar power flowing through their cables.
Electric vehicles, like all vehicles, are an under-utilised asset: cars are parked 92% of the time, doing nothing. So why not make some use of it, to allow us to deploy renewables on the grid, and to allow the owner of the car to benefit somehow? he said.
It is not just medium-sized challenger companies like Ovo eyeing this emerging opportunity. Big-six firm E.ON launched a tariff last week targeting owners of electric cars, offering electricity at night  when most of the UKs 100,000 plug-in cars are charged and when energy demand is usually at its lowest  at prices a third cheaper than in the daytime.
The next big challenge is going to be how we integrate all these electric vehicles into the grid system, said Dr Chris Horne, head of origination at E.ON. In time, you can see how this is a complete change in the way we think about energy, motoring and vehicles.
The German firm is exploring the sort of vehicle-to-grid technology being used by Nissan and Ovo. E.ON thinks the concept may begin to become more mainstream in the next decade, boosted by a 20m government fund for projects starting next year, and the greater availability of electric car models  not all are capable of it.
This switch, from individuals potentially giving back power to their supplier, as well as taking it, marks the beginning of a whole new relationship between energy company and citizen.
On the one hand, cheaper solar and energy storage at home could, as one researcher at Imperial College recently put it, bleed revenues from the utilities sector.
Our relationship with the grid will become quite different, from being utterly dependent on the grid for power, people may become a bit more interdependent, giving or taking, said Dale Vince, founder of green electricity firm Ecotricity.
Therell be a role for energy companies, itll just be very different. It could be considerably diminished in terms of energy supply, if you go far into the future, he added.
On the flip side, helping a customer with the complex task of exploiting their battery is an opportunity for energy companies to move beyond just supplying electricity and gas, where price is still the main differentiator, into being providers of a range of energy services.
British Gas has already started down this road, offering a range of smart home products under its Hive brand and launching a site to find tradesmen.
Horne said electric cars offered a chance for firm to change their relationship with consumers, and focus on offering them solutions, not just power.
We think people can become their own energy-independent unit and were excited by that vision and need to support it. But, obviously, if people dont buy energy from us, we need to find other ways to have relationships with them, and to supply them with products and services, said Pakenham.
Robert Llewellyn, the actor and writer, knows all about that changing relationship. An advocate for electric cars and renewable energy, he has had a battery-powered car and solar power for seven years, and the solar panels were upgraded this year. Its changed my relationship with my energy supplier  I give them much less money. Since May, Ive been around 85% powered by solar, said Llewellyn, best known for playing mechanoid Kryten in Red Dwarf.
My one house makes no difference, but if there were 5m homes with [solar and electric cars], it would really help the national grid and reduce the costs of generating power, he said, referring to the 3bn annual cost to consumers of running the UKs energy networks.
For Llewellyn, electric cars, batteries and solar power are most exciting not at individual level but at community level, citing his Gloucestershire villages efforts to generate its own electricity. Whats emerging rapidly is community-owned energy generation and distribution, he said.
This future, where electric cars are used as a key element of our energy system, is not without potential downsides. Vince said he had concerns about the impact on the car batterys longevity, though Ovo thinks active management of it could even be beneficial. Thinktanks have warned that just six electric cars in one neighbourhood could cause unplanned drops in voltage if they are not charged at the right times of day. And another worry is the social impact of lots of solar- and electric car-equipped homes reducing their reliance on the power grids.
Tom Edwards, an analyst at energy expert Cornwall Insight, said: If I install a battery in my home, because I can afford to put a battery in, that means Im avoiding the cost of using the national grid but my neighbours might not necessarily be, and theyre going to have to pick up the slack that Im not paying for.
The risk, as he puts it, is that one day, youre left with the whole 3bn bill landing on one old lady in Cornwall. To avoid that, regulator Ofgem is looking at how paying for the energy network might change in the future, such as a phone-line rental-style charge, rather than being based on the amount of energy used.
There is also disagreement over whether its more efficient to produce and store power at a local level, close to where its consumed, or at large central power stations with the energy transmitted over long distances.
Making power where its used at the home level is the most efficient way to do it because you avoid all the losses of the distribution and transmission system to get it to people, argues Vince. Under Jeremy Corbyn, the Labour party has made such a decentralised system one of the planks of its energy policy.
For Edwards, its not a given that greater energy independence for consumers is a good thing. [Its] not necessarily, because the most efficient way to generate electricity is in very large power stations. They will be more efficient than anything you can stick on your roof.
One thing Edwards agrees on is that batteries, along with trends such as more renewables and greater digitisation, mean upheaval is coming. The system will fundamentally change, he said.
