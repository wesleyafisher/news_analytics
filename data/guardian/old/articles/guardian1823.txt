__HTTP_STATUS_CODE
200
__TITLE

Automation will affect one in five jobs across the UK, says study

__AUTHORS

Larry Elliott, Economics editor
__TIMESTAMP

Tuesday 17 October 2017 01.16EDT

__ARTICLE
Workers in the constituency of shadow chancellor John McDonnell are at the highest risk of seeing their jobs automated in the looming workplace revolution that will affect at least one in five employees in all parliamentary seats, according to new research.
The thinktank Future Advocacy  which specialises in looking at the big 21st century policy changes  said at least one-fifth of jobs in all 650 constituencies were at high risk of being automated, rising to almost 40% in McDonnells west London seat of Hayes and Harlington.
The thinktanks report also found that the public was largely untroubled by the risk that their job might be at threat. Only 2% of a sample of more than 2,000 people were very worried that they might be replaced by a machine, with a further 5% fairly worried.
Future Advocacys report has been based on a PWC study earlier this year showing that more than 10 million workers were at risk of being replaced by automation and represents the first attempt to show the impact at local level.
The thinktank said McDonnells seat would be affected because it contains Heathrow airport, which has a large number of warehousing jobs that could be automated. Of the 92,150 employees in Hayes and Harlington in 2015, 36,170 (39.3%) were at high risk of having their jobs automated by the early 2030s. Crawley  the seat that includes Gatwick airport  was seen as the second most vulnerable constituency.
Future Advocacy said its report was an attempt to encourage a geographically more sophisticated understanding of, and response to, the future of work, and also an attempt to encourage MPs to pay more attention to this critical issue.
Opinion is divided on the likely impact of the artificial intelligence revolution on jobs. Optimists have said that the lesson from history is that technological change leads to more jobs being created than destroyed, while pessimists have argued that AI is different because the new machines will be able to do intellectual as well as routine physical tasks.
One thing that almost all economists agree on is that change is coming and that its scale and scope will be unprecedented. Automation will impact different geographies, genders, and socioeconomic classes differently, the report noted.
It added that the highest levels of future automation are predicted in Britains former industrial heartlands in the Midlands and the north of England, as well as the industrial centres of Scotland. These are areas which have already suffered from deindustrialisation and many of them are unemployment hot spots.
Olly Buston, one of the reports authors, said it was vital that lessons were learned from the 1980s. Lets not have a repeat of the collapse of the coal-mining industry, he said. Instead, we should have a smarter strategy.
Noting that there would be a political pay off for the party that came up with the best strategy for coping with the robot age, the report makes a number of recommendations for the government. 
They include: publishing a white paper on adapting the education system so that it focuses on creativity and interpersonal skills in addition to the stem subjects of science, technology, engineering and maths; developing a post-Brexit migration policy that allows UK-based AI companies and universities to attract the best talent; exploring ways to ensure the benefits of the AI revolution are spread through research into alternative income and taxation models, including investigation of a universal basic income; and conducting further detailed research to assess which employees were most at risk of losing their jobs.
The report said that it was arguably automation  rather than globalisation  that has created the economic and social conditions that led to political shockwaves such as the election of Donald Trump and the vote for Brexit.
As artificial intelligence supercharges automation over the next decade, and this hits different groups differently, there will again profound social and political consequences. Our politicians should surely consider this carefully.
The report found that the leaders of the four main Westminster parties represented seats where more than 25% of jobs were at high risk of being automated, while the constituency with the lowest proportion of high-risk jobs was Labour-held Edinburgh South.
High-risk constituencies typically contained large numbers of people working in transport or manufacturing, while lower-risk constituencies  including Edinburgh South, Wirral West and Oxford East  had high concentrations of workers employed in education and health. 
