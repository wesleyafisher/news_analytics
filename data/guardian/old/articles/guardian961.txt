__HTTP_STATUS_CODE
200
__TITLE

Somalia, Libya ... and Chad? The surprising country on Trumps travel ban list

__AUTHORS

Jason Burke
__TIMESTAMP

Tuesday 26 September 2017 10.57EDT

__ARTICLE
It is fair to say that one of the more infrequently asked geopolitical questions of recent decades has been: Why Chad? The large, poor African country has rarely played even a marginal role in international power politics. Its inclusion on a newly revised list of eight countries whose citizens are subjected to an indefinite ban on almost all travel to the US has led to somebewilderment. 
The addition of other states on to Donald Trumps list, which will come into effect on 18 October, appear more easily explained. Trump has been trading threats and insults for weeks with North Koreas Kim Jong-un, while the US president told the UN last week that Venezuela was a socialist dictatorship. Sudan has disappeared from the list, while Iraq has apparently been deemed aproblem that can be dealt with in other ways. Somalia, Libya, Syria, Yemen and Iran were on the original list and remain on the new one.
But Chad?
The presidential proclamation provides few clues. It notes that the countrys government  Chad has been ruled by the authoritarian General Idriss Dby since 1990  is an important and valuable counterterrorism partner of the US with whom the US looks forward to expanding that cooperation, including in the areas of immigration and border management. However, it says, Chad has failed to adequately share public-safety and terrorism-related information. This has surprised many analysts, who point out that Chad has been one of the more effective US counter-terrorist allies in the sub-Saharan region for several years, and has along and deep security relationship with several other western states, especially France.
A second reason given by the White House is that several terrorist groups are active within Chad or in the surrounding region, including elements of Boko Haram, Isis-West Africa, and al-Qaida in the Islamic Maghreb (AQIM). The ban is justified by the significant terrorism-related risk from this country.
While it is true that all those organisations have some presence in Chad, it is minimal in comparison with that in other regional states. Boko Haram has spilled into Chad but is a fundamentally a Nigerian-based group. Indeed one of the most effective roles played by Chadian forces has been to act as an anvil to aNigerian military hammer in successive offensives against the militants. US officials have consistently praised them for this.
Other groups, particularly AQIM, are much more active in Mali, Niger and Algeria, which are not subject to any ban. A new coalition led by al-Qaida is very active currently across the Sahel region, but not in Chad. Terrorist experts point out that no major international militant plot has ever been hatched from the country. In Mali, jihadists occupied swaths of territory including the famous city of Timbuktu. The French troops sent to expel the extremists remain in the region  based in Chad.
If Trump has a rational and realistic explanation of why Chad is on the new list, he is not sharing it for the moment.
