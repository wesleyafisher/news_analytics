__HTTP_STATUS_CODE
200
__TITLE

Hillary Clinton praises NHS after broken toe prevents interviews

__AUTHORS

Nadia Khomami
__TIMESTAMP

Monday 16 October 2017 14.02EDT

__ARTICLE
Hillary Clinton has paid tribute to the NHS after she was forced to rearrange a series of interviews because she fell and broke her toe.
The former US presidential candidate had been due to appear on Womans Hour, This Morning and the Graham Norton Show as part of a promotional tour for her book What Happened.
But all three interviews were cancelled or delayed after the accident, which she described when she finally made it to Nortons set.
I was running down the stairs in heels with a cup of coffee in hand, I was talking over my shoulder and my heel caught and I fell backwards, she said. I tried to get up and it really hurt. Ive broken my toe. Ive received excellent care from your excellent health service. 
Her communications director, Nick Merrill, posted a picture of Clinton on set wearing a protective boot, saying she had twisted her ankle.
HRC on The Graham Norton Show, joking about how she caught her heel on some steps & twisted her ankle earlier today. Back to the book tour! pic.twitter.com/Fk30HLjNat
Clintons praise of the NHS, a publicly funded healthcare system, is in sharp contrast to Donald Trump, who has spent much of his presidency unsuccessfully attempting to dismantle the insurance subsidy programme known as Obamacare. Some Democrats are making the case for a single-payer system that would resemble the NHS. 
The incident may bring back unhappy memories of constant questions from Trump during the campaign about her health, speculation over which reached fever pitch when a video emerged showing her unsteady on her feet. Clintons doctors later said she was treated for pneumonia and dehydration, and made a full recovery.
The Womans Hour host, Jane Garvey, had earlier been forced to apologise live on air after the former secretary of states heavily trailed appearance was cancelled at the last minute. Hillary Clinton has been delayed this morning, as Ive already said, Garvey said. It does look like shes not going to make it before the end of the programme.
Im really, really sorry about that. I think you can understand just how sorry I am. Were working very, very hard to get this interview rescheduled and Hillary Clinton is very apologetic. So what can I say, thats the situation.
Id read the book and everything. Apologies. #HillaryClinton @BBCWomansHour pic.twitter.com/Dh66CuYVph
But Merrill later tweeted that Clinton had been able to reschedule the interview for later in the day and was taping it now for tomorrow morning.
Amid frantic reshuffling of Clintons commitments on a publicity tour that has already made her near-ubiquitous in the British media, the This Morning presenter Philip Schofield suggested it had not immediately been possible to schedule a new interview for the ITV show. 
Schofield posted on his Snapchat account: Supposed to be interviewing Hillary Clinton  but shes fallen over and hurt her foot!! Gutted.
He later said he had been asked to rush back to the studio to do the interview, before posting a picture of Clintons book on the studio sofa with the caption not happening!
Clinton was more than an hour late for a speech to Cheltenham literature festival on Sunday, where an audience of 2,500 people awaited her arrival.
During her talk, she blamed her loss to Trump on sexism and the maddening double standards women are held to in public life. The only way well get sexism out of politics is to get many more women into politics, she said.
Clinton recalled how she collapsed with grief after her loss and turned to yoga, mystery novels and long walks in the woods to overcome the shock.
Everybody gets knocked down. What matters obviously is whether you get yourself back up and keep going. As a person, Im OK; as an American, Im worried, she said, citing Trumps position on North Korea and the US withdrawal from the Paris climate accord as causes for concern.
Hillary Clinton finally arrives 1hr 40 mins later than scheduled for her live chat at the Cheltenham Literature Festival pic.twitter.com/ZWxep0TkfW
Jenni Murray recently wrote for the Guardian about Clintons previous appearance on Womans Hour, in 2014. At the time, she was preparing to announce her plan to stand again for the Democrats. 
She was late for the Womans Hour start time of two minutes past 10am, Murray said. Not her fault, but minders underestimating London traffic 
Into my ear, as my conversation with Shirley [Williams] and [her friend] Eva came to a close, were the whispered, thrilled words of the producer. Shes here! Hillary Clintons here.
