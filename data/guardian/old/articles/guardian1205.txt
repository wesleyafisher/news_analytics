__HTTP_STATUS_CODE
200
__TITLE

The trick to having it all: asking for help  and hiring a butler

__AUTHORS

Jana Kasperkevic in New York
__TIMESTAMP

Sunday 31 July 2016 07.00EDT

__ARTICLE
Hello, its Monika! a woman next to me calls out as we enter an empty New York City studio apartment. Neither of us lives here. Neither of us has ever met the owner. We are not breaking in. We are here to help.
Monika, 46, is an Alfred  a modern-day butler  and I am tagging along to one of her jobs. 
Forget Siri and other so-called intelligent personal assistants. Can Siri let the plumber in to repair that leaky sink? Make your bed? Wait for the cable guy? No. But Alfred can.
Founded by two women in their late 20s, Hello Alfred is supposed to be an accessible and affordable way to get the help you need to balance work and personal life. The service is named after Batmans trusty butler and focuses on building relationships between clients and Alfreds. After all, once youve signed up online, downloaded the app and had your in-person consultation, you are handing over the keys to your house.
Its a different feeling to have a human being, who you trust, who you feel is taking care of you and when you come home, you have a feeling of relief and a connection with another human being, says Marcela Sapone, one of Alfreds co-founders.
You meet almost everyone, Monika tells me, noting that sometimes people work from home or end up being sick. You feel like you have a connection with everyone. And thats crucial. You add your personal touch to it.
She has been with this particular client for about a year, but has yet to meet her.
She is one of the tidiest people I know, Monika says. The studio looks ready for a photoshoot and I wonder whether my place would look as stylish if I had a butler of my own.
Monika gets right to work  disappearing in a walk-through closet (there is a bathroom on the other end) to put away dry cleaning we picked up from the buildings concierge. Swiftly she transfers her clients clothes from the wire hangers they were delivered on to her own and puts them into their appropriate spots.
Today is Monikas second weekly visit to this particular clients home. Alfred charges $32 a week for one visit and $58 for two visits. A typical appointment lasts an hour. Earlier this week, Monika had dropped off dry cleaning and run some other errands. Usually her duties include making the bed, taking out the garbage, getting groceries and putting them away, dealing with the clothes and letting in the cleaner sent by MyClean, a tech-enabled cleaning service.
You get to know the client better over time, Monika says as she moves over to make the bed. Its not like the first visit when you have to look where everything is.
By now, she has finished making the bed. This client likes her pillows fluffy, Monika explains as she lines up pillows on the bed. Dont tell anyone, but a pillow likes a little bit of a harsh treatment, she adds, moving to the sofa and fluffing one pillow after another.
Monika might have never met this particular client but she knows quite a bit about her. As she moves through the apartment, she picks up a stray pair of shoes to put away. She grabs an empty glass from the table and puts it into the dishwasher.
Before we leave, Monika opens the refrigerator to make sure its stocked with enough water. Satisfied, she closes it.
You learn peoples habits, Monika says smiling. For someone, one pack of paper towels is enough. For others, its not. 
Lack of time and too many errands is exactly how Alfred first began. Its co-founders  Sapone and Jessica Beck  met at Harvard business school. They both knew they wanted to start a business and were trying to figure out how to fit it into their everyday life.
We said: How are we going to have a family, have a career and have a personal life? said Sapone. To figure out the formula for being able to juggle all that, they turned to other women for answers. They called women in venture capital, media, and tech and asked: how are you able to do this? 
Everyone of them had a similar answer and that was: help. And it was usually personal help  personal assistant, a nanny, [or] their mom lived with them, explained Sapone. So, Jess and I said: OK how can we create leverage in a more accessible way? Because at 27 years old, we couldnt afford personal assistants. So we booked help for ourselves.
At the time, help came in form of Jenny  a woman they found on Craigslist. 
We asked Jenny to help us with the basic chores that were time consuming: picking up dry cleaning, letting the cleaner in and grocery shopping for us. Basically, we trusted her to do what she thought was right. We gave her keys to our home and split the cost, said Sapone. At the time, she and Beck lived in the same building. Over time, our neighbors said: Thats really interesting. Could we share Jenny?
And so Alfred was born.
We accidentally made Alfred for ourselves, to solve our problem, says Sapone. That was back in March 2013. Over the next year, the duo encouraged their friends to sign up for the service as they ran a year-long experiment. Hello Alfred was officially launched in September 2014 at a TechCrunch Disrupt event in San Francisco, where it won $50,000.
Hello Alfred differs from some of the other on-demand startups in that Alfreds are all company employees and not independent contractors, like Uber drivers for example. Some, like Monika, work nine to five, others work more flexible hours and maybe just two days a week. Employees range in age and profession  some are actors who attend auditions in between taking care of their clients and some, like Monika, are working moms. 
According to Sapone, the starting salary is $16 and after two months it automatically increases to $18. Alfreds can earn up to $30 an hour and are encouraged to move up the ladder and become part of corporate staff, she said. 
When I ask Monika how much she makes an hour, she laughs. 
I thought no one in this country talked about money, she says, before telling me that she makes $25 an hour. 
Sapone would not say exactly how much Alfred has grown in the past two years, but notes that its clients are in the thousands. At the moment, Alfred is focused on growing and improving its operations in New York, Boston and San Francisco.
We want to become very large in those three cities first before we expand, says Sapone.
Alfreds growth has had a very direct impact on Monika, who has been with the company for over a year. 
We have many, many more clients, she tells me. Back when she first started, she used to have clients in Midtown as well as in Greenwich Village where we are at the moment and had to take subways in between appointments. At one point, when she first started the job, she decided to track how many miles she walked in one day and found out that it was between 12 and 13 miles. Yet as Alfred grew, it became easier to group clients by area. Now that her clients are closer together, she does not walk as much.
On a typical day, Monika has eight clients. Today, four of them live in the same building and the other four live nearby.
By itself the number of clients doesnt mean much, she explains. Instead it depends on how messy or organized the client is. Do they need you to replace candles? Water the plants? Drop off shoes to be repaired? After a while, knowing all this becomes a part of a routine especially when you have the same clients all the time, the way Alfreds do.
Monika does not mind all the walking and running around required for the job.
Its a good way to see new things, meet new people, she says as I walk with her back to the Alfred headquarters where she will do paperwork before wrapping up for the day.
Among things that Monika dealt with today was a dry cleaning delivery that was rescheduled and a new cleaner who was running a few minutes late. The clients, of course, know nothing of this. They come home and everything is where it is supposed to be. Alfreds slogan, after all, is Come home happy.
Worrying is Monikas job. 
