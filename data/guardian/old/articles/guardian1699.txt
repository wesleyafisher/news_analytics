__HTTP_STATUS_CODE
200
__TITLE

Even the Brexiters do not want us to crash out without a deal

__AUTHORS

Dan Roberts Brexit policy editor
__TIMESTAMP

Tuesday 10 October 2017 12.30EDT

__ARTICLE
Theresa Mays no-deal scenario is one that neither side in the Brexit negotiations says it wants to see, but what are the risks of it happening anyway?
The British government has revealed its contingency planning for walking away  ostensibly to demonstrate that it has a viable alternative if Brussels refuses to give ground in the talks. Two white papers outlining the consequences demand at least to be taken seriously.
The trouble is that EU leaders still think May is bluffing. The former Irish prime minister John Bruton voiced the fears of many in Brussels when he warned that the British government was too weak to be taken at its word on anything at the moment.
A key reason for that weakness is that Mays cabinet is divided between those who find it inconceivable that Britain could survive without an EU deal, and those who not only think it might be palatable but actually quite relish the prospect.
All the ingredients could therefore be brewing for Mays no-deal brinkmanship to blow up in everybodys faces. Brussels may find that its refusal to compromise drives the British government into the welcoming arms of an extreme wing that never wanted a deal anyway. Mays determination to show strength could upset the delicate balance in her cabinet and leave the clean Brexit diehards in charge.
The hope is that one side will blink first at the Brussels talks. Either the Brits stop trying to have their cake and eat it  and make concessions on money and sovereignty  or the EU breaks the deadlock by agreeing to discuss compromises on trade in parallel.
Whether this happens in time in another matter. We are already halfway between the EU referendum in June 2016 and the effective cut-off point for agreeing an article 50 exit deal in early 2019. Many companies warn that by early 2018 they need at least an agreement to extend this timetable with a transition phase, or they will have to start assuming the worst.
Even many Brexiters paint a bleak picture of what crashing out without a deal might entail for the British economy. We are looking at a 10-year recession. Nothing ever experienced by those under 50, said Pete North, of the Leave Alliance, in a chilling blogpost on Tuesday. This is not the Brexit I was gunning for, he added. I wanted a negotiated settlement to maintain the single market so that we did not have to be substantially poorer.
Other EU countries could fare almost as badly. Belgium, Ireland and the Netherlands face losing several percentage points worth of GDP in the event of a very hard Brexit, according to recent studies.
But just as the nuclear brinkmanship practised by Donald Trump and Kim Jong-un is in constant danger of escalation, the Brexit talks risk being derailed by rising belligerence and miscalculation. If May decides the only way to show she means it is to walk out of the talks, who can be sure she would be allowed to walk back in to pick up the pieces?
