__HTTP_STATUS_CODE
200
__TITLE

The inside track: how to spot the telltale signs of badgers, otters and more

__AUTHORS

Emine Saner
__TIMESTAMP

Saturday 14 October 2017 01.00EDT

__ARTICLE
Wild animals are all around us whenever we venture into the countryside, but many of us have no idea where they are, what they are or how to recognise the clues they leave. Once you know what you are looking for, you can decode the forest floor or riverbank and unlock the secrets of where a badger has been sleeping, what a fox has been eating and where an otter has been playing. You will need beady eyes  and occasionally a willingness to sniff some horrid things  but you will be rewarded with new insight into the lives of some of Britains most delightful mammals.
If you want to know for sure which wild beasts are waiting in the woods or hills near you, learning the signs of these five critters is a good place to start.
First, learn what a dogs pawprint looks like, for elimination purposes: thats the one you are most likely to find in the countryside. Both animals have four toes, but dog prints tend to be square-shaped, says veteran tracker Simone Bullion, a senior conservation adviser at Suffolk Wildlife Trust. A fox has quite a small palm pad and if you drew a box around the print it would be rectangular. It is a narrow print; they are quite delicate-looking.
Fox droppings are long and thin, between little finger and index finger size. They usually have a long, wispy end, says Bullion. They are often greyish, because they tend to contain the fur of things such as rabbits. But if a fox has been eating blackberries it will be purpley.
Badgers have a bean-shaped palm pad, very large compared to the size of the toes, says Bullion. The toes in front of the pad will be almost aligned and the front paws leave a distinctive claw mark. Be aware that badger tracks can be deceptive: they have five toes, but sometimes the fifth toe doesnt print.
Look for their setts in banks  the entrance will be around 30cm (1ft) across. Badgers are fantastic excavators, so they would excavate a large amount of soil, which would be in front of the hole.
Want to know what a badger has been eating? Look for a small pit of droppings near the sett. If it is filled with what looks like diarrhoea, that means they have been munching earthworms. Badgers tend to deposit their droppings in a pit they have dug themselves, says Bullion. You might find several pits close by in a small area, invariably made by the same badger or the same badger clan. Badgers use these pits to mark the boundaries of their territory.
Look for large paw prints, with five small toes arranged in a semicircle around the palm pad. With a good print, you might be able to see that the otters paw is webbed.
The otter is a large animal, but their droppings, called spraints, are surprisingly small. Given the fishy contents, they smell sweet and aromatic. They deposit them in obvious places  obvious to other otters, that is. They would put them on a tussock of grass, a mound, a molehill, a piece of wood by the water. If theres only one tree, go to the base of that tree. The spraints are crammed with fish bones and scales. When they are fresh, they are very black; old ones turn grey.
If you open up a small, dark, sausage-shaped dropping  about the size of your little finger  and it is crammed with the remnants of insects, such as beetle wing cases that look quite shiny, it can only be a hedgehogs, says Bullion. In long grass, the spiny mammals are almost impossible to find, but you may spot them on lawns or on playing fields. It is a bit needle-in-a-haystack, admits Bullion. You probably wont spot their pawprints in the wild, but you can buy or make a simple tunnel for your garden that you can fill with bait, an ink pad and a sheet of paper to capture a print.
You wont be able to track droppings or paw prints, but you can spot a harvest mouses nest. It is about the size of a tennis ball, says Bullion. Look for a woven nest in grass, in areas that arent regularly mowed. If it is low-growing, tussocky grass  a species such as Ccocks-foot  they would form a woven nest at the base. In taller grasses, the nest will be higher up  even as high as chest height. They are made the same way: living blades of grass are shredded and weaved into a ball-shaped nest, which forms part of the plant. This time of year is the best to spot them as surrounding vegetation dies back.
