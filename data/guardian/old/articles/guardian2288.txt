__HTTP_STATUS_CODE
200
__TITLE

Herv Lger obituary

__AUTHORS

Veronica Horwell
__TIMESTAMP

Monday 9 October 2017 13.22EDT

__ARTICLE
Herv Lger never intended a life in fashion. He wanted to be a sculptor, and endured a year studying at the cole des Beaux Arts in Paris before he fled education to get his hands directly on materials. His curiosity about the properties of fibres, from human hair to silk jersey, was the base of his greatest success, the bandage dresses that were the most sympathetic approach to the body-conscious fashion of the 1990s.
Lger, who has died aged 60 of a ruptured aneurysm, instinctively moulded cloth to display and flatter the body beneath. Fabrics incorporating elastane (aka Lycra or Spandex) fibres for stretch were extensively used in underwear, and exercise and dance kit in the 80s, but less so in fashion; too vulgar. Lger found his first stretch-assisted knitted jersey as a discarded experiment at a textile factory. Because he had experience of shaping hats from fabric strips, he realised he could make the weird stuff into a dress that would follow female curves by assembling strips, panels and tubes to run round or diagonally across the body (most dresses are cut vertically, falling from shoulders or waist). His first full collection in this style was shown in 1991, when supermodels such as Cindy Crawford, plus actresses on the new red carpets, still had considerable curves, and a professional wish to show them off.
His was not an overnight success. His real name was Herv Peugnet, and he was born in Bapaume, northern France, which he left at 18 for art school; after he dropped out, he took a hairdressing apprenticeship to support himself. This was excellent training (hair is a difficult fibre, yet susceptible to moulding), and gave him access to couture, working on models at the collections, including at Chlo, where Karl Lagerfeld was chief designer. Young Peugnet taught himself fashion design and cut, making accessories and hats, doing contract jobs at the house of Lanvin and for the costume designer Tan Giudicelli.
He met Lagerfeld at a Chlo party in 1980. Lagerfeld thought little of the young mans sketches (his ideas did not translate well to drawings) but, because of his practicality, hired him as a personal assistant at both Fendi and Chanel. In 1984, Lagerfeld encouraged him to start his own Parisian boutique and to change his name from unmemorable Peugnet to Lger, to suggest lightness.
Lger was ceaselessly inventive and his clothes mobile; part of the appeal of the bandage dresses which he slowly began to introduce in the late 80s was that, for all their tightness, which positively held the figure in and up, they allowed unrestricted poses for the cameras. He designed active swimwear and tights, too, and ballet costumes for Roland Petits company at the Paris Opra.
In order to compete in 90s fashion, Lger needed financial backing. The Seagram corporation, a Canadian firm in the drinks business busily diversifying into film and music, offered the cash that allowed him to expand into a substantial ready-to-wear operation. Seagram incrementally acquired 95% ownership, and when it suddenly sold off its non-liquor holdings, the Herv Lger enterprise went in 1999 to Max Azria, whose Los Angeles-based BCBG firm produced French-influenced ready-to-wear.
Azria wanted to own the bandage dress company, but he did not want Lger himself. He was soon dismissed and legally deprived of ownership of his name and label. The Azria company spent more than five years purchasing 90s originals from collectors to assemble a research archive so the dresses could be recreated; and Azria launched his version in 2007.
The dresses creator, however, had long since moved on. In 2000, he opened a boutique in Paris, and let Lagerfeld choose him a fresh name, Herv Leroux, inspired by his red hair. His passionate interest was still in garments that clung to a female shape, but he replaced strip and panel structures with draped cloth, working in soft viscose and silk jerseys. As the burlesque queen Dita Von Teese pointed out, Leroux was in the same classical class as the great Parisian couturire Madame Grs, both having been inspired by the drapery on Greek sculpture.
The Leroux label floundered commercially, never achieving major sales, but both loyal and new private customers  Kardashians among them  multiplied, since Leroux always managed to accentuate their positives and eliminate their negatives. He liked to see women confident, even happy, and regretted the cult of starvation; his draperies were meaningless on coathangers.
The Fdration de la Haute Couture asked him to return to the Paris collections as a guest in 2013, recognising his deserved place as an original.
His sister Jocelyne, who partnered him in the Leroux business, survives him.
 Herv Lger (Herv Peugnet/Herv Leroux), couturier, born 30 May 1957; died 4 October 2017 
