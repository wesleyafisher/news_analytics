__HTTP_STATUS_CODE
200
__TITLE

Ruby Walsh and Willie Mullins plunder Limerick prize with Total Recall

__AUTHORS
Press Association
__TIMESTAMP

Sunday 8 October 2017 14.26EDT

__ARTICLE
Total Recall landed a monster gamble in the Munster National at Limerick on Sunday. Running off a rating of just 129, the handicapper will no doubt have his say after this stroll in the park, but his biggest danger on this occasion was avoiding the fallers.
Having his first start for the trainer Willie Mullins after the retirement of Sandra Hughes, Total Recall was sent off the 2-1 favourite in the 16-runner field and Ruby Walsh managed to get the horse, who was keen in the early stages, settled at the rear and his mount was always cantering thereafter.
The Michael OLeary-owned Alpha Des Obeaux was still in front turning for home and ran a fine race off top weight, but there was never any real question who was going to win. Walsh briefly shook Total Recall up after the last and he sprinted clear to win in fine style, scoring by seven lengths. Phils Magic was third with Tulsa Jack fourth.
Walsh said: He travelled and jumped well and I suppose the runner-up has run a blinder under top weight and we only had 10st 5lb on his back. Hes a nice horse to get, he jumped super and was obviously well taught as a novice last year. Hes a very loose horse, is like an elastic band and has great use of himself and has great power.
He had a great racing weight on his back which is a huge plus and Id say the runner-up has run an unbelievable race on his first run for a while. I was lucky as there were a couple of fallers and a lot suffered interference, but I was lucky to miss a good bit of that.
He definitely got three miles and if you get into these type of handicaps with less than 10st 7lb, youve a huge chance and its such an advantage. Its my second time winning the race after Dear Villez [2008] and its great to win another big handicap with good prize-money.
At Kelso, the Grand National-winning jockey Derek Fox was taken to Borders General hospital after briefly being knocked unconscious.
Fox was riding Justatenner for Barry Murtagh in the first division of a handicap hurdle, when falling at the third flight. The clerk of the course, Anthea Morshead, said: Hes going for a check-up. He was briefly knocked unconscious, but by the time he went to hospital he was talking. Obviously it is in everyones interests for him to go and get checked out.
Fox partnered the Lucinda Russell-trained One For Arthur to victory at Aintree in April and the eight-year-old was favourite to repeat the feat next year before being ruled for the season with a tendon injury a few days ago.
Felix de Giles was denied a famous victory in the 127th Velka Pardubicka in the Czech Republic when the 14-1 shot Urgent De Gregaine was reeled in close to home by No Time To Lose, who was previously trained in Britain by Jamie Osborne, winning on the Flat at Lingfield in 2012.
De Giles said of his mount: He was a little bit keen. Im very happy with his second and hope to come back again.
4.55 Augenblick 5.25 Bakht Khan (nap) 5.55 Buffer 6.25 Precious Angel 6.55 Glassalt 7.25 Banish 7.55 Daring Guest 8.25 Maazel
3.10 Marchingontogether 3.40 Our Place In Louie 4.10 Royal Icon 4.40 Storm Cry 5.10 Thesme (nb) 5.40 Hitman 6.10 Harbour Patrol
1.55 Eyecatcher 2.25 Rastacap 2.55 Me Too Nagasaki 3.25 Dan Emmett 3.55 Mama Africa 4.25 Dukes Girl 5.00 Graphite
2.05 Hurricane Lil 2.35 High Seas 3.05 Vixen 3.35 Rolling Dice 4.05 Global Applause 4.35 Haulani 5.05 Rydan
2.15 Jack Regan 2.45 Prerogative 3.15 Delsheer 3.45 Holiday Girl 4.15 Zamfir 4.45 Black Trilby 5.15 Nutini 5.45 Concur
