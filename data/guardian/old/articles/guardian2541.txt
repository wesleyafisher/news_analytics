__HTTP_STATUS_CODE
200
__TITLE

Jos Mourinho defends Manchester United tactics after Anfield stalemate

__AUTHORS

Daniel Taylor at Anfield
__TIMESTAMP

Saturday 14 October 2017 11.46EDT

__ARTICLE
Jos Mourinho defended Manchester Uniteds tactics after a 0-0 draw at Anfield that left Jrgen Klopp remarking that he would never set up his Liverpool side in such a defensive system.
Klopp chose his words carefully to make sure it could not be construed as a verbal attack on Mourinho but the Liverpool manager was clearly frustrated after the two sides fought out a second successive goalless draw in the Anfield fixture, for the first time ever.
Mourinho, however, would not accept that it was a poor spectacle or that he had deliberately set out to leave Anfield with a draw. It depends on what is an entertaining game, the United manager said. One thing is an entertaining game for fans, another thing is an entertaining game for people who read football in a different way.
For me, the second half was a bit of chess but my opponent didnt open the door for me to win the game. We came for three [points] but in the second half it was difficult to do that with the dynamic as it was. I was waiting for Jrgen to change and go more attacking but he kept the three strong midfielders at all times.
I was waiting for him to give me more space to counter but he didnt give me that. So I know that probably you think we were defensive and they were offensive  well, you [Liverpool] are at home and you dont move anything. I dont know why. I was waiting for that and he didnt do it. I think he did well, honestly [not changing it].
Mourinho went on to explain that he had brought on two of his substitutes, Marcus Rashford and Jesse Lingard, to try and catch Liverpool on the break if the home side had been emboldened to change their system. However, the more notable substitution was the arrival of Victor Lindelof, a centre-half, in place of Ashley Young, who was playing as a winger, in stoppage time.
You could not play this way at Liverpool but its OK for Manchester United, Klopp said. I thought we were worthy of three points. There were a lot of good individual performances. But when an opponent has this kind of defensive approach you will not create 20 chances.
