__HTTP_STATUS_CODE
200
__TITLE

Jimmy Greaves takes a curtain call  archive, 1972

__AUTHORS
Albert Barham
__TIMESTAMP

Wednesday 18 October 2017 00.30EDT

__ARTICLE
Tottenham Hotspur 2, Feyenoord 1
The crowd was only slightly smaller than for the visit of West Ham United, the mood was nostalgic, the entertainment excellent, and the result predictable, even to the scorer of the first goal. Everything was as it should be at White Hart Lane last night for positively the last performance of Jimmy Greaves. As Tottenham Hotspur beat their Dutch visitors Feyenoord 2-1 in this testimonial match the bovver boots stamped out a rhythm of approval, the chanting was warm and witty, and the notorious North Bank led a session in which they invited 45,000 others to join them in All who love Jimmy clap your hands. 
In his 15 playing years the mention of the word workrate to Greaves brought a wince of pain. Workrate was for artisans; genius came from a moment of movement. One did not have to run like a racehorse or to train like a slave; sticking a goal in the back of the net was the object of the exercise, Greaves has said. Thirty months away from the dressing rooms of White Hart Lane and a season of Sunday works football is not quite the preparation to play against one of the best sides in Europe and for one of the best teams in the First Division. Yet within three minutes of this match beginning last night the old intuition was shown again.
The familiar flashing movement was followed by a typical goal from Greaves. He seized on to Perrymans pass and the old lethal left foot flicked the ball past the black-clad Reitsma, who was beaten just as hundreds of goalkeepers before him. Greaves might well have had another.
There was good football to follow with Chivers sharp and eager. Gilzean, the new king of White Hart Lane, was applauded for his perfectly headed passes and there was some fine work by Coates. But Coates, Gilzean, and England were all substituted later.
The Dutch helped to put an edge of competition on the match and perhaps overdid this at one time. Van Daele, a tall young man who plays in horn-rimmed glasses, was shown the yellow card of caution for one of several foul tackles on Chivers. There was clever support for Ressel, one of the best players on the field, and De Jong from Hasil and Vos in particular. The scores were brought level after 24 minutes. Ressel was the scorer.
Feyenoord might well have scored again but for the quick reaction of young Daines, getting an unexpected taste of European opposition in goal. He saved extremely well from Wery, who was later injured and replaced by Kristensen. Greaves, of course, got a cheer every time he touched the ball and he was particularly unlucky when he outsmarted Rijsbergen and stabbed the ball across the face of the goal only for Reitsma to fall at the feet of Chivers. The match was won in the last few minutes through a shot by Evans following a move created by Evans. 
At the end the crowd sang for Greaves and chanted Jimmy for England. But Greaves will not be back. He will be seen on the Parks pitches, playing for his works side on Sunday mornings. For one Greaves devotee the night was especially worthwhile. He came up from Torquay and won a car in a competition. 
