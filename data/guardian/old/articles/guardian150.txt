__HTTP_STATUS_CODE
200
__TITLE

California fires: at least 15 killed in 'unprecedented' wine country blaze

__AUTHORS

Julia Carrie Wong in Napa County and 
Alastair Gee in San Francisco
__TIMESTAMP

Tuesday 10 October 2017 15.42EDT

__ARTICLE
At least 15 people have died in northern California after what officials are describing as an unprecedented wildfire that has already destroyed 2,000 structures and devastated large swaths of wine country.
We often have multiple fires going on, but the majority of them all started right around the same time period, same time of night  its unprecedented, Amy Head, the fire captain spokeswoman for Cal Fire, the state agency responsible for fire protection, told the Guardian. I hate using that word because its been overused a lot lately because of how fires have been in the past few years, but it truly is  theres just been a lot of destruction.
As of Tuesday afternoon, the Sonoma sheriffs office said about 150 people were still reported as missing.
About 20,000 people have been evacuated, including hundreds of senior citizens from nursing homes, and public schools in Napa and Sonoma counties were closed on Tuesday. Major fires in those regions remain completely uncontained, threatening thousands of homes and vineyards in the wine country north of San Francisco.
Still, calmer conditions on Monday night helped fire crews get a handle on the situation, said another Cal Fire spokeswoman, Heather Williams. 
The acreage isnt going up drastically, and hopefully over the next few days were going to start to see those containment numbers go up.
Californias governor, Jerry Brown, has declared a state of emergency in eight mostly northern counties  Butte, Lake, Mendocino, Napa, Nevada, Orange, Sonoma and Yuba counties. On Monday, during a visit to the state, Vice-President Mike Pence said he had spoken with Brown. 
The dryness of the climate, the strength of the winds  you all in California know much better than this midwesterner does, Pence said. But I can assure you, as I did the governor, that the federal government stands ready to provide any and all assistance to the state of California.
Eleven of the deaths occurred in Napa and Sonoma counties, according to Cal Fire. Three were further north, in Mendocino county, and the last in the eastern Yuba county, officials said. 
Entire neighborhoods and a trailer park in the town of Santa Rosa, 55 miles from San Francisco, have already been razed, along with a Hilton hotel, according to local reports.
Years of drought in California, followed by an extremely wet winter, have meant that vegetation is thicker and more susceptible to the fires that tend to be at their worst in the autumn. Officials say the high winds are hampering firefighting efforts in the region about 140 miles (225km) north of San Francisco. To assist with the efforts the countrys largest firefighting aircraft  a converted 747  has been deployed.
It was an inferno like youve never seen before, said Marian Williams, who caravanned with neighbors through flames before dawn as one of the wildfires reached the vineyards and ridges at her small Sonoma County town of Kenwood.
Williams could feel the heat of her fire through the car as she fled. Trees were on fire like torches, she said.
Mandatory evacuations were ordered in counties north of San Francisco Bay and elsewhere after blazes broke out late on Sunday. 
Williams, said that 17 major fires had started in the past 36 hours in the California, burning about 115,000 acres, mostly in the northern part of the state.
She added that unusually high winds had made the fires spread so quickly. Night-time is when humidity is the highest and temperatures are cooler, but that wind, fueled by denser vegetation, really pushed these fires so quickly.
The high number of deaths in one series of connected fires is unusual. There have been, on average, 13 wildfire deaths a year in the whole of the US since 2014, according to figures collated by the National Interagency Fire Center.
Head, the Cal Fire captain, said the fires were probably linked to a warming climate. It has been hotter, it has been drier, our fire seasons have been longer, fires are burning more intensely, which is a direct correlation to the climate changing, she said.
With so many fires, residents of Sonoma County struggled to figure out which roads to take, finding downed trees or flames blocking some routes.
Fires also burned just to the east in the Napa County wine country as well as in Yuba, Butte and Nevada counties, all north of the state capital. Cal Fire tweeted that as many as 8,000 homes were threatened in Nevada County, which lies on the western slope of the Sierra Nevada.
Smoke was thick in San Francisco, 60 miles (96km) south of the Sonoma County fire.
Pillars of heavy smoke rose from the hills surrounding the town of Napa as the sun set on one of the deadliest days of wildfires in Californias history. As darkness took over, the hills around wine country glowed red. 
Weve never had a fire like this before, said Mike Willmarth, a Napa middle school teacher who has lived in the area for 30 years. Weve never had devastation like this. 
Margaret Beardsley, 92, sat in a wheelchair next to her husband, Robert, and daughter, Nora. Beardsley used an oxygen tank and wore a surgical mask  the smoke in the air had irritated her lungs. 
Its terrible, said Beardsley, who has lived in Napa for 46 years and never been evacuated before. When we left we had no idea if we would have anything to come back to. I want to go home and sleep in my own bed.
The lack of information was frustrating George Bradley, 74, a retired cement mason who has lived in the Napa area for his entire life. He hadnt been able to contact his mother, and was worried about the house he had evacuated at about 1.30pm.
We just paid it off. We just got the deed, he said. We dont know if its still standing or not. 
The fires economic impact is not yet known  the hundreds of wineries in Napa and Sonoma valleys are their lifeblood. Reports indicate that two wineries  Signorello Estate Winery and Paradise Ridge Winery  were destroyed and that portions of another, Stags Leap, were also burned.
Although most of this years grapes have been harvested, the remaining 20% are some of the most valuable, said Jennifer Putnam, executive director of Napa Valley Grapegrowers, the Napa Valley vineyard trade association. Its some of the best fruit that Napa produces, all of the cabernet sauvignon, so it will have a major economic impact if this last 20% cant be picked, she said. We will have to see where we are tomorrow and next week to see how the grapes are metabolizing all this smoke.
Agencies contributed reporting
