__HTTP_STATUS_CODE
200
__TITLE

F1 fight for safety continues on third anniversary of Jules Bianchi crash

__AUTHORS

Giles Richards in Suzuka
__TIMESTAMP

Friday 6 October 2017 13.39EDT

__ARTICLE
Three years ago the Japanese Grand Prix came to a sombre, muted close. There were no post-race celebrations at Suzuka in 2014 and the crowd filed away in a hushed silence as the seriousness of Jules Bianchis accident sunk in across the circuit. It proved to be every bit as dreadful as everyone had feared.
Bianchi had sustained a serious head injury and died as a result nine months later. His was the first on-track fatality since Ayrton Senna was killed at Imola in 1994. The sport had been dealt the harshest possible reminder that, despite the vast improvements in safety, it remained inherently dangerous. At Suzuka, three years on, it is clear the FIA is still relentlessly pursuing the goal of making F1 safer. But it is a task that requires them to strike a difficult balance and one that demands a single-mindedness that is rewarded with little but criticism.
Laurent Mekies is the FIA safety director. He was in the role when Bianchi crashed into a track-side recovery vehicle in heavy rain and vividly recalls its effect on F1. It was a big shock, he says. The sport had not seen any fatalities at the top level for many, many years. We are aware it can strike at any point but it doesnt take away the fact that when it happened it was very shocking.
That sense of shock has not entirely dissipated, especially when returning to Suzuka but safety no longer attracts headlines and of late, when it has, the FIA has been on the receiving end of the unfavourable ones. It has faced the brickbats from drivers, fans and teams for everything from trying to regulate the danger out of the sport to ruining the aesthetics of the cars.
Much of it is undeserved, although it could doubtless better communicate what it is trying to do and why it is doing it. Mekies, who was a chief engineer in F1 before joining the governing body, acknowledges it should do so but he is also insistent that behind the scenes its pursuit of improving safety is not at odds with the sport itself.
There is not a single thing we do that is there to prevent accidents, he says. What we do is to make sure that the guy who has an accident can walk away from it. We know accidents are part of the sport. We just want to make sure the guys can walk away afterwards.
This goal has underpinned the changes that have seen F1 move from the fatality-ridden 1970s to those 20 years without a death. The work is often not seen but has been continuing apace in the background throughout, which was still the case before Bianchis incident.
If you look at what the FIA had done prior to that you cannot talk about complacency, he says.
We are pushing safety improvements  more often than not against public opinion because people forget so quickly about the potential risk. We are in the line of fire.
Of late they have publicly ventured over the parapet. Cockpit protection had been on the FIA agenda and was being researched since before Dan Wheldon was killed when he crashed at the Las Vegas Motorspeedway in 2011 and this year the FIA announced the Halo cockpit protection device would be mandatory on all cars next year. It had to impose the regulation since agreement on the device could not be reached but has won few supporters for doing so. The Halo has been proved to reduce the chance of head injuries but its aesthetics have led to condemnation.
We looked at so many concepts, says Mekies. Our job is to put all the points of compromise in different designs on the table and somebody had to make the call  and as is very often the case with the FIA we went for maximum safety. When it comes to safety that is where we wont compromise.
Changes that affect the look of the sport always attract the most ire. Others go unnoticed. Mekies points out that the strength of the cockpit part of the chassis has improved by 300% in the past three years, a direct result of the FIA pursuing this goal.
Those are numbers that drivers can take great comfort in, even those who vehemently oppose Halo. Daniel Ricciardos assessment, however, is instructive. Its not taking away anything from the driver in terms of courage, he said. Its a simple little benefit that we can all gain from. No one wants to see another fatality.
This strikes a chord with Mekies. There is nothing that we do to try and prevent drivers showing courage on track. Quite the opposite. This year we made the fastest car in history. We have made the cars faster because we think it is the right thing for the sport.
Making the cars quicker is one thing but the process of ensuring they are safe remains a work in progress.
On track in Suzuka, rain washed out the second practice session on Friday, with Sebastian Vettel topping the time sheets in the morning, edging out Lewis Hamilton by two-tenths of a second. Hamiltons former McLaren team-mate Jenson Button made a return to the F1 paddock, and the 2009 world champion admitted he had lost his passion for the sport before he retired in 2016. I miss the racing, the 37-year-old, who raced in F1 for 17 seasons, said. But I sort of fell out of love a little bit with motorsport. I think maybe I left it a year too long racing in Formula One.
He is still contracted to McLaren but admitted their uncompetitive car was a struggle to cope with at the end of his career. The last two years were tough, hesaid. Once you have been lucky enough to be in a position to achieve in this sport and had some great years with McLaren, to suddenly have a few years of not being able to achieve and seeing the guys at the front fighting for wins every race ... It is tough, it really is. I probably left it a bit too long.
