__HTTP_STATUS_CODE
200
__TITLE

Lets move to Dollis Hill, north-west London: It could have been Silicon Hill

__AUTHORS

Tom Dyckhoff
__TIMESTAMP

Friday 6 October 2017 11.30EDT

__ARTICLE
Whats going for it? When Im bored, I like to imagine another fate for Dollis Hill. Today, its a synonym for suburbia, the hill striped with 1920s semis, the tube station sucking in and belching out its daily diet of commuters; but its a cosmopolitan kind of suburb, and those semis soon filled with aspirant Jewish migrs from the East End. Dollis Hill coulda been a contender. It could have been Silicon, er, Hill. In its heyday, when research labs lined the North Circular, the Colossus computer, of code-breaking fame, and Ernie the computer for premium bonds, were built in its Post Office Research Station. (Yes, there was a Post Office Research Station. I feel an Ealing comedy coming on.) Had global fortunes not turned the way they did, Dollis Hill might have had its own Steve Jobs or Bill Gates. Instead, theyre probably pushing the grandkids around Gladstone Park, dreaming about what might have been.
The case against Suburban ennui. The lovely Gladstone Park aside, theres not much to it, just a parade of newsagents and offies by the station and endless avenues.
Well connected? Trains: Dollis Hill is on the Jubilee line, with central London 15 minutes away. Driving: not far from the North Circular and M1; a 45-minute drive into central London for the brave.
Schools Primaries: Gladstone Park, St Mary Magdalens Catholic and Avigdor Hirsch Torah Temimah are all good, Ofsted says, with Convent of Jesus and Mary Catholic Infant and Our Lady of Grace Catholic outstanding. Secondaries: the Crest Academy and Menorah High for Girls are both good.
Hang out at The Stables Caf in Gladstone Park: great cakes and glorious views.
Where to buy The, ahem, old town hugs the tube station, with lovely leafy roads like Dewsbury, lined with red-brick Edwardians. But its mostly a 1920s affair, with wide roads of stately semis such as Kendal Road and Dollis Hill Lane fronting the park, and Park Avenue, plus a nice grid around Olive Road. More expensive south and east towards Willesden Green and Cricklewood, cheaper north to the North Circular. Not that many flats. Detacheds, 900,000-2m. Semis, 650,000-2m. Terraces and cottages, 520,000-1.5m. Flats, 400,000-650,000. Rentals: one-bed flat, 1,000-1,400pcm; three-bed house, 1,800-2,500pcm.
Bargain of the week Roomy, four-bedroom 1920s detached on Dollis Hill Lane, in need of modernisation, 740,000, with hoopersestateagents.co.uk. 
Megan and Will Clark Quiet, leafy and village-like, with a wonderful sense of community. Con: lack of a decent pub on our doorstep  you need to walk to Willesden Green, or hop in an Uber to Kensal Rise.
Fiona Jones Twice a year you can visit Churchills underground bunker, one of Dollis Hills biggest secrets.
 Live in Dollis Hill? Join the debate below.
Do you live in Hereford? Do you have a favourite haunt or a pet hate? If so, email lets.move@theguardian.com by Tuesday 10 October.
