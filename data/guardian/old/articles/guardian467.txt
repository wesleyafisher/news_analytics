__HTTP_STATUS_CODE
200
__TITLE

James Corden's Peter Rabbit: another kids' classic wrecked forever

__AUTHORS

Stuart Heritage
__TIMESTAMP

Monday 25 September 2017 08.16EDT

__ARTICLE
In a concrete bunker situated miles below civilisation lives a crack team of scientists dedicated to one thing and one thing only: ruining Peter Rabbit as comprehensively as they possibly can.
Parents of young children might be fooled into thinking that their mission has already been a success. After all, theres already a CBeebies Peter Rabbit adaptation that paints the sedate, 115-year-old Beatrix Potter character as a go-get-em adventurer whose escapades are typically soundtracked by a series of nightmarish sub-Levellers songs about standing your ground and laughing in the face of danger.
But the scientists persisted. Yes, theyd hugely damaged the Peter Rabbit brand, but they hadnt yet irreparably ruined it. Then one day, one of the scientists called his colleagues around. Shaking with the power of his own awful imagination, he pointed at what hed written on his blackboard. It read 3D live-action/CGI animated adventure comedy movie. The other scientists gasped, but one  emboldened by this hideous new vision  went one step further. He grabbed a piece of chalk and, trembling, added Starring James Corden beneath it. The scientists applauded. One started vomiting. Theyd done it.
At least based on its trailer, the Peter Rabbit film appears to have been aggressively engineered to make people sad. Even more so than other kids properties that have been given needless updates over the years, the Peter Rabbit film looks like the result of some blisteringly inept manhandling. Sure, Thunderbirds might have opted for a live-action remake directed by Riker from Star Trek. And, sure, The Magic Roundabout might have self-immolated twice over, casting Robbie Williams as Doogal in the UK version and rebranding entirely as Doogal (Things are about to get hairy!) in America. And, obviously, Postman Pat was crushed into dust the moment someone made a film about him entering X Factor and fighting off a legion of robot cats.
But theres something genuinely harrowing about the sight of Peter Rabbit  gentle, Edwardian Peter Rabbit  thoughtlessly injuring some birds, or grabbing a pile of lettuce leaves and making it rain like a banker in a stripclub, or literally twerking. No joke, all of these things happen in the trailer. Theres even a moment where a badger hurls a hedgehog at a dartboard, and its so much like the dwarf-throwing scene from The Wolf of Wall Street that youre left with the incontrovertible feeling that this entire venture was put together by people who genuinely hate Peter Rabbit. 
Just look at Mr McGregor. In all previous versions hes been a villain  his primary objective has always been to cook Peter Rabbit in a pie  but in this trailer hes the only sympathetic figure. Hes the guy who comes home to find his house trashed by Peter Rabbit, and his despair is palpable. Not again, hed say if he hadnt been so utterly numbed by trauma. Thats how much of a dick this Peter Rabbit is. He actually makes you side for the baddie. For shame. 
The fact that Corden  who, thanks to his roles in Trolls, The Emoji Movie and now this, seems like a man hellbent on screwing up as many kids as he can  voices Peter Rabbit just underlines it all. Something has gone terribly, terribly wrong along the way here.
Beatrix Potter was a famous stickler for protecting her work  she refused to sell the Peter Rabbit rights to Walt Disney  so there is no way on Earth that shed have ever given the green light to a slow-motion car crash like this. But now the floodgates are open, lets really go for it. Lets give Tom Kitten a machine gun and a thirst for vengeance. Lets give Squirrel Nutkin a rocketpack and a sleeve tattoo. Weve gone this far already. Why not finish the job?
