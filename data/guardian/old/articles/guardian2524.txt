__HTTP_STATUS_CODE
200
__TITLE

Labour losing ground to Tories in left-behind towns as it gains in cities

__AUTHORS

Michael Savage Policy Editor
__TIMESTAMP

Saturday 14 October 2017 19.03EDT

__ARTICLE
Labour is losing ground to the Tories in Britains left behind towns and increasingly becoming the party of major cities, according to a new study of the UKs fracturing political landscape.
Analysis by Professor Will Jennings of the University of Southampton for the New Economics Foundation thinktank shows a persistent and growing difference in political affiliation between cities and towns.
The general election in June saw a 10.2% swing from Conservatives to Labour in cities. However, there was just a 4.1% swing in small towns. Since 2005 the Tories have increased their share of the vote in small towns from 34.5% to 48.0%, while Labour support in small towns has remained stable.
The findings will worry Labour MPs concerned that the party is losing touch with traditional supporters and is too focused on metropolitan issues. It comes as the party is struggling to come up with a position on Brexit that satisfies both its urban and traditional working-class supporters.
Analysis of recent elections also suggested that the more a place has experienced relative decline, the worse Labour tends to perform in electoral terms (and the better the Conservatives do). At the last election the swing to Labour was far higher in cities than in either large towns (+5.0) or small towns (+4.1), and even smaller in other areas (+3.0).
Lisa Nandy, the former shadow cabinet minister and Labour MP for Wigan, said her party needed to take on concerns about issues such as immigration to tackle the problem. There is a long-term problem here that shows it is not only Labour has problems in towns, but increasingly people in towns think there is no one in the political establishment who is speaking for them, she said.
At the moment in the party, there is a tendency to see everything through the lens of the current leadership, but this is a trend that goes back 20 years - we see more and more of our decisions driven by those who are concentrated in cities, the values of people who live in towns are not reflected in the political debate. We see it in the real anger about immigration.
The New Economics Foundation is proposing a manifesto for towns, including improved local infrastructure, jobs and supply chains. Will Brett, co-author of the report, said: Many towns are being left high and dry, disconnected from global growth and sidelined by our economy.These are places which people call home. They are infused with history and meaning, they serve as anchors for peoples identity, and yet they are being left behind.
