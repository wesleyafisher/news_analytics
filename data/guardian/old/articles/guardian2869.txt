__HTTP_STATUS_CODE
200
__TITLE

Journeyman review  Paddy Considine rolls with the punches in heartfelt boxing drama

__AUTHORS

Peter Bradshaw
__TIMESTAMP

Thursday 12 October 2017 17.32EDT

__ARTICLE
Paddy Considine now presents his second feature as writer-director, and its a powerful and sincerely intended personal project about a championship boxer who must confront a terrible personal crisis. The performances are strong and committed  it reminded me a little of Johnny Harriss boxing film Jawbone  and Considines instincts as actor and director are towards self-scrutiny without narcissism. Yet the audiences buttons are not just pushed, they get hammered with uppercuts and there is a mile-wide streak of Hollywood emotion here, together with a lenient and even celebratory tone about boxing itself, which you may not entirely share. Yet this all gives the film force, and the quality of the acting is absolutely plain.
Matty Burton (Considine) is a boxer who is approaching the end of his career, a mature and likable guy who unselfconsciously enjoys the good things that boxing have brought him; and hes devoted to his beautiful wife Emma (Jodie Whittaker, soon to star in Doctor Who) and their baby daughter. Nonetheless, he is preparing to defend his title one last time  a bit nettled at those saying his last victory was unconvincing, due to the referee over-cautiously stopping the fight. 
His challenger is the brash and arrogant Andre Bryte (Anthony Welsh), who needles him outrageously at the press conference and weigh-in. These scenes, along with his tense farewell to his wife before the fight in their lovely home, show that Considine has perhaps absorbed something from Jake Gyllenhaals boxing film Southpaw. The fight itself is a fierce grudge match but Matty goes the distance, wins the judges decision, but has taken terrible blows to the head. He collapses at home, and the real struggle now begins.
The succeeding scenes with Considine and Whittaker are the very best of the film: intimately painful, agonising, moving and scary. Emma helps Matty back into his palatial home, after what has clearly been a brain operation and that luxurious house now looks eerily empty and clinically white. We see it the way he sees it, as a kind of one-person hospital ward that he does not entirely recognise. He has become a second child, with halting movements, clouded memory, impaired speech and a change of personality involving a terrifying new temper that he cannot understand. Whittakers performance is outstanding: showing her undiminished love, her need to understand and readjust, and showing a moving willingness to make sacrifices. Considine is focused and yet unselfish in his scenes opposite her.
However, the plot progresses in such a way as to give Matty a melodramatic and not wholly credible crisis on a bridge over a fast-flowing river. It also takes Whittaker out of the story for a long period, and it becomes more about Mattys reconciliation with the guys: his training team, played by Tony Pitts and Paul Popplewell. These scenes are potent and well-acted, too, but I have to say I regretted Whittakers absence, and would have liked more scenes about what Emma was going through on her own.
There is a nice moment when Matty is having his hair cut and the conversation turns to the question: who is the best boxer of all time? Well, no prizes for guessing the answer to that one: Ali, because he was bigger than boxing. (Give the truth drug to a cinephile or actor, on the other hand, and the answer might be Jake LaMotta.) Similarly, a boxing film must be about more than just boxing, and Whittakers presence shows that Journeyman is capable of that kind of reach. Emma is facing a fight (sometimes an actual, physical fight) with someone she loves. Journeyman is flawed, but intelligent and heartfelt.
