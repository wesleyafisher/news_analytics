__HTTP_STATUS_CODE
200
__TITLE

Venus in Fur review  Natalie Dormer dominates S&M; game of cat and mouse

__AUTHORS

Lyn Gardner
__TIMESTAMP

Tuesday 17 October 2017 18.00EDT

__ARTICLE
Game of Thrones star Natalie Dormer is dominant in every way in this tricksy, mildly entertaining two-hander written by David Ives, in which 21st-century sexual politics meet 19th-century S&M. Dormer plays Vanda, a mysterious actor who turns up late to audition for the female lead in a play adapted by writer-director Thomas Novachek (David Oakes) from the novel Venus in Furs by Sacher-Masoch, the man who gave his name to masochism.
 With auditions finished, Thomas has just been on the phone to his girlfriend complaining that young women cant even play feminine these days and out of the 30 hes already seen, half are dressed like hookers, half like dykes. Then Vanda appears with a flash of lightning. Patrick Marbers production lays on the Hammer Horror effects with gusto.
 Thomas initially refuses to hear the brash, motormouth Vanda read for the part, using the age-old excuse were looking for somebody different. With the weariness of the powerless actress used to the meat market of casting calls, she counters: Somebody whos not me. Im too young. Too old. Im too big, Im too small. My rsums not long enough. OK. But when Vanda slips into a dress she brought with her she is transformed. As Thomas starts to read alongside her, he becomes Severin von Kushemski, the protagonist in Sacher-Masochs novel, whose early experience of being birched by an aunt while lying on a fur cape has left him longing to be dominated. So begins a game of cat and mouse, an intricate two-step operating as a play within a play in which the power balance continually shifts. Soon Vanda is giving Thomas notes and direction, pointing up his sexism and taking control.
 Ives script wears its influences on its sleeve, whether its The Bacchae, Strindbergs Miss Julie or the dangerous dressing-up games of Genets The Maids. In the showier role, Dormer negotiates the switches in mood with real virtuosity, but it is never quite the engrossing power struggle it might be, because Oakes Thomas is such a bland chap.
 One might think that in the light of the recent Harvey Weinstein allegations this would have added pungency. Thomas admits he once said that being a playwright was an opportunity to get laid, but excuses it as his younger self talking. That sounds familiar. But the plays uncertainty of tone  veering between comedy, campness and mild erotica  undercuts any serious points it might make about the way sex and power are intimately entwined.
 It has a problem, too. From the moment Vanda arrives dressed in a revealing black corset and bends over offering Thomas an eye-popping glimpse of her bottom, the audiences gaze is also his gaze  and therefore the male gaze.
 Ives and Marber cant resist titillating even as the play appears to question the objectification of women. Even the casting of Vanda as the goddess turned femme fatale plays to Hollywood conventions.
 Venus in Fur has become one of Americas most regularly staged plays since its 2010 premiere. Its no surprise: its a mere tease, and one that never strips bare the way some men use sex and power to dominate, control and obliterate women to satisfy their own desires. 
At Theatre Royal Haymarket, London, until 9 December. Box office: 020-7930 8800.
