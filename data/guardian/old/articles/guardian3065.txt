__HTTP_STATUS_CODE
200
__TITLE

Underground city set to become Chicagos next attraction

__AUTHORS
Ella Buchan
__TIMESTAMP

Saturday 9 September 2017 07.30EDT

__ARTICLE
In some ways its just like any other Chicago neighbourhood. There are three Starbucks, a gym, a department store and a friendly dive bar. But no one lives here. Theres no natural daylight. And many Chicagoans have never heard of it.
This is the Pedway  a network of tunnels running beneath 40 blocks of the Loop, Chicagos central business district. Built piecemeal since 1951, it provides a weatherproof route for pedestrians to walk between buildings, including Macys and City Hall  though relatively few use it. Many stores, hotels and bars connected by the network have entrances at street level and underground, while a few cafes serve only the Pedway.
Margaret Hicks owns Chicago Elevated, which runs tours of the tunnels. Even locals dont know about the Pedway, she said. They certainly dont understand it.
The layout is more maze than grid. Each building linked to the underground network is responsible for its section of the Pedway, so no one is in charge. Few attempts have been made to promote its use  until now. 
A local not-for-profit organisation, the Environmental Law & Policy Center, has raised $125,000 (around 100,000) to spruce up the tunnels and turn them into a tourist attraction. In July, the centre announced plans to promote the Pedway both as a destination and as a desirable way to move around downtown. Its vision includes an underground library, art galleries and a farmers market. A glass cube in Millennium Park would provide public access  an attractive alternative to the stairwells and escalators within buildings connected to the tunnels that are currently the only way in.
If we had better navigation, signage, arts and entertainment, it would be a really cool place, said executive director Howard Learner, adding that city officials have given the project the green light.
While the time frame has yet to be announced, others have spotted the Pedways potential. For this years Chicago Architecture Biennial (until January 2018), Los Angeles architect Erin Besler and artist Fiona Connor are creating installations inspired by the walkways textures, fixtures and fittings. Their work will highlight a bit of functional infrastructure that gets far less attention than the more iconic structures it serves above ground, said Biennial executive director Todd Palmer. These will be displayed in the Chicago Cultural Center, one on the ground floor and another where its basement joins the Pedway.
However, Hicksworries that the plans could strip her favourite neighbourhood of its offbeat charms. Obviously, there is lots of room to improve the Pedway. I dont want people to feel lost and confused in it, she said, but what I love about it is its strangeness.
Hicks introduced me to wedding photographer Ed. He spends his days watching the doors of the marriage court beneath City Hall, for potential customers. Ive never seen him in daylight, she whispered. Strip lights flickered in the ceiling. On our right was an entrance to Macys basement floor. Opposite was a gleaming row of 22 stained-glass windows, including one by Louis Comfort Tiffany. Built into the tunnel wall and permanently backlit, this gallery was organised by the Smith Museum of Stained Glass Windows at Navy Pier, one of Chicagos better-known attractions.
The effect is incongruous, like a Picasso painting hanging in a barn. But everything about the Pedway is odd and, if it becomes a tourist hotspot, Hicks doesnt want that to change. The Pedway is one of Chicagos neighbourhoods. I dont want to see it gentrified. Save the weird, you know?
 Margaret Hicks runs 90-minute tours of the Pedway, $23, chicagoelevated.com
