__HTTP_STATUS_CODE
200
__TITLE

Trump travel ban extended to blocks on North Korea, Venezuela and Chad

__AUTHORS

Oliver Laughland
__TIMESTAMP

Monday 25 September 2017 02.09EDT

__ARTICLE
Donald Trump has announced new travel restrictions on visitors to the United States that will expand his controversial travel ban to eight countries.
The new proclamation, which will come into effect on 18 October, will continue to target travellers from Somalia, Yemen, Syria, Libya and Iran, but also adds North Korea, Chad and Venezuela to the list of targeted countries. Sudan has been dropped from the administrations list of nations and Iraqi citizens will be subjected to additional scrutiny but will not face any blanket bans.
Seven of the countries face wide-ranging restrictions, which effectively block travel for most citizens, while the limits imposed on Venezuela will only apply to a group of government officials and their families. 
As president, I must act to protect the security and interests of the United States and its people, Trumps statement said. The president later tweeted: Making America Safe is my number one priority. We will not admit those into our country we cannot safely vet.
Unlike the administrations previous travel bans, which were intended as temporary measures as homeland security officials were instructed to review vetting procedures, the new restrictions are not time-limited. 
The proclamation marks the administrations third move to limit travel into the United States after Trump called for a total and complete shutdown of Muslims entering the US on the campaign trail. 
The first ban, which was chaotically rolled out in January, targeted refugees and seven Muslim-majority countries and was subsequently abandoned by the administration after a series of federal courts blocked it on grounds it violated the US constitutions protection of religious freedom. 
The second order, issued in March, targeted six of the same countries. A limited version of the ban was allowed to come into effect over the summer following a temporary ruling by the supreme court. 
The new policy is likely to throw a major hurdle in front of the ongoing supreme court challenge to Trumps second order. The nations highest court was due to hear arguments in that case on 10 October. The two groups of challengers, a collective of Democratic states and migrant legal advocacy groups, have argued Trump has exceeded his legal authority and has deliberately targeted Muslims.
But Trumps updated order, which now includes non-Muslim majority countries, is likely to affect the arguments in the case. 
I expect that the supreme court will seek briefing from the parties on how the new guidance affects the appeal and specific legal issues raised in the appeal, said Carl Tobias, a law professor at the University of Richmond, who added the proceedings could be delayed as a result. 
Nonetheless, advocates seized on Trumps evening announcement to call for continued legal opposition to the travel ban. 
Just because the original ban was especially outrageous does not mean we should stand for yet another version of government-sanctioned discrimination. It is senseless and cruel to ban whole nationalities of people who are often fleeing the very same violence that the US government wishes to keep out, said Naureen Shah, senior campaigns director for Amnesty International USA. 
This ban must not stand in any form. 
The administration claimed the order was a more targeted policy than previous iterations. According to the proclamation, department of homeland security officials carried out a review of security measures in almost 200 countries, and found the eight nations remain deficient... with respect to their identity-management and information-sharing capabilities, protocols, and practices.
As a result, the administration claimed, these states were handed tailored travel restrictions. Somali citizens face a total ban on permanent migration to the US and will be subjected to additional scrutiny for visitor visas. Only Iranians on student exchange visas will be allowed entry to the US but will also face further vetting. Libyans also face a ban on permanent migration and common business and tourism visas. 
Although North Koreans face a block on all travel, the net migration rate from the country is zero and diplomatic visas, for all the targeted countries, are exempt from the ban. 
The administration will continue to allow those from the targeted countries to request a waiver from the restrictions on a case-by-case basis. 
Although the new order provides these country specific bans, it effectively blocks travel for most citizens of the targeted nations, apart from Venezuela. 
Let us not be fooled by the administrations attempted tricks and semantics, this is still the same Muslim ban, said Johnathan Smith, legal director of Muslim Advocates, a legal advocacy group. The administration is once again making cosmetic adjustments to the Muslim ban in hopes that it will pass the barest possible definition of anything else; but theyve failed again.
Trump issued the order on Sunday as the 90-day temporary limit on visa issuances, imposed by the previous ban, was due to expire at midnight. A 120-day, limited freeze on refugee admissions, also included in the previous ban, will elapse in late October. 
The administration has not yet indicated whether it plans to try and extend the moratorium on refugees too, but faces a 1 October deadline to set the annual cap on refugee admissions. 
According to a report in the New York Times, the White House has yet to decide where to place the cap. While the total is expected to be far lower than the 110,000 refugees Barack Obama sought to admit in his final year in office, some in the administration, led by conservative senior advisor Stephen Miller have asked for a historic low. 
