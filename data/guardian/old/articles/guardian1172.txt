__HTTP_STATUS_CODE
200
__TITLE

The rise of electric cars could leave us with a big battery waste problem

__AUTHORS

Joey Gardiner
__TIMESTAMP

Thursday 10 August 2017 04.15EDT

__ARTICLE
The drive to replace polluting petrol and diesel cars with a new breed of electric vehicles has gathered momentum in recent weeks. But there is an unanswered environmental question at the heart of the electric car movement: what on earth to do with their half-tonne lithium-ion batteries when they wear out?
British and French governments last month committed to outlaw the sale of petrol- and diesel-powered cars by 2040, and carmaker Volvo pledged to only sell electric or hybrid vehicles from 2019.
The number of electric cars in the world passed the 2m mark last year and the International Energy Agency estimates there will be 140m electric cars globally by 2030 if countries meet Paris climate agreement targets. This electric vehicle boom could leave 11m tonnes of spent lithium-ion batteries in need of recycling between now and 2030, according to Ajay Kochhar, CEO of Canadian battery recycling startup Li-Cycle.
However, in the EU as few as 5% (pdf) of lithium-ion batteries are recycled. This has an environmental cost. Not only do the batteries carry a risk of giving off toxic gases if damaged, but core ingredients such as lithium and cobalt are finite and extraction can lead to water pollution and depletion among other environmental consequences. 
There are, however, grounds for optimism. Thus far, the poor rates of lithium-ion battery recycling can be explained by the fact that most are contained within consumer electronics, which commonly end up neglected in a drawer or chucked into landfill. 
This wont happen with electric vehicles, predicts Marc Grynberg, chief executive of Belgian battery and recycling giant Umicore. Car producers will be accountable for the collection and recycling of spent lithium-ion batteries, he says. Given their sheer size, batteries cannot be stored at home and landfilling is not an option.
 EU Regulations, which require the makers of batteries to finance the costs of collecting, treating and recycling all collected batteries, are already encouraging tie-ups between carmakers and recyclers. 
Umicore, which has invested 25m (22.6m) into an industrial pilot plant in Antwerp to recycle lithium-ion batteries, has deals in Europe with both Tesla and Toyota to use smelting to recover precious metals such as cobalt and nickel. Grynberg says: We have proven capabilities to recycle spent batteries from electric vehicles and are prepared to scale them up when needed.
Problem solved? Not exactly. While commercial smelting processes such as Umicores can easily recover many metals, they cant directly recover the vital lithium, which ends up in a mixed byproduct. Umicore says it can reclaim lithium from the byproduct, but each extra process adds cost.
This means that while electric vehicle batteries might be taken to recycling facilities, theres no guarantee the lithium itself will be recovered if it doesnt pay to do so. 
Investment bank Morgan Stanley in June said it forecast no recycling of lithium at all over the decade ahead, and that there risked being insufficient recycling infrastructure in place when the current wave of batteries die. There still needs to be more development to get to closed loop recycling where all materials are reclaimed, says Jessica Alsford, head of the banks global sustainable research team. Theres a difference between being able to do something and it making economic sense.
Francisco Carranza, energy services MD at Nissan, says the fundamental problem is that while the cost of fully recycling a battery is falling toward 1 per kilo, the value of the raw materials that can be reclaimed is only a third of that. 
Nissan has partnered with power management firm Eaton for its car batteries to be re-used for home energy storage, rather than be recycled, and this economic problem is a big reason why. Cost of recycling is the barrier, says Carranza. It has to be lower than the value of the recovered materials for this to work.
The lack of recycling capacity is a tragedy, says Amrit Chandan, a chemical engineer leading business development at Aceleron, a hi-tech British startup looking to transform end of life batteries. It takes so much energy to extract these materials from the ground. If we dont re-use them we could be making our environmental problems worse, he says.
Aceleron, like Nissan, thinks the answer lies in re-using rather than recycling car batteries  for which the company has patented a process. Chandan says car batteries can still have up to 70% of their capacity when they stop being good enough to power electric vehicles, making them perfect  when broken down, tested and re-packaged  for functions such as home energy storage. 
Fresh from recognition by Forbes as one of the 30 most exciting hi-tech startups in Europe, Aceleron is looking for investors to help it roll out pilot projects. Theres going to be a storm of electric vehicle batteries that will reach the end of their life in a few years, and were positioning ourselves to be ready for it, says Chandan.
This is not the only alternative. Li-Cycle is pioneering a new recycling technology using a chemical process to retrieve all of the important metals from batteries. Kochhar says he is looking to build a first commercial plant to put 5,000 tonnes of batteries a year through this this wet chemistry process. However, it is early days for the commercial exploitation of this technology. 
Linda Gaines, transportation system analyst and electric vehicle battery expert at the Argonne National Laboratory in the US says: The bottom line is theres time to build plants. But, she adds, we dont know what kinds of batteries theyll be yet. It would help if the batteries were standardised and designed for recycling, but theyre not. 
