__HTTP_STATUS_CODE
200
__TITLE

Joe Biden says European leader likened Trump to Mussolini

__AUTHORS

Ben Jacobs in Newark, Delaware
__TIMESTAMP

Tuesday 17 October 2017 14.58EDT

__ARTICLE
Fourteen heads of state have contacted Joe Biden in an attempt to better understand the actions of the Trump administration, the former vice-president said on Tuesday, with one even comparing the US president to Mussolini.
Speaking at an event on Tuesday alongside Ohios Republican governor, John Kasich, Biden bemoaned Trumps bizarre conduct and claimed that one European prime minister went so far as to liken the president to Il Duce.
Biden, vice-president under Barack Obama, said the comment was made after Trump elbowed aside Duko Markovi, the prime minister of Montenegro, before a group photo at a Nato summit in Brussels in May.
The former vice president graphically described the scene of his meeting with a European head of state, saying that the unnamed leader had described to him how Trump took the president [sic] of Montenegro, shoved him aside, stuck his chest out and his chin, and all I could think of [was] Il Duce.
Biden reenacted the mock pose to some laughter from a University of Delaware audience composed mainly of college students. He then corrected the crowd and said: Not a joke, not a joke. Thats what people are thinking.
Bidens remarks  made at an event meant to promote and discuss bipartisan cooperation with Kasich  violated the precedent that outgoing presidents and vice-presidents do not criticize incoming administrations. 
Biden talked about Trump in terms of fundamental concern about his effect on the world order.
This breaking down of international and national norms is the glue that holds the liberal world order together, is the glue that holds together our system, he said, noting growing concern among foreign policy poobahs about the possibility of nuclear war.
His words echoed those of Republican senator John McCain, who warned against fear[ing] the world we have organized and led for three-quarters of a century and abandon[ing] the ideals we have advanced around the globe in a speech on Monday which was interpreted as criticism of Trump.
Biden said he had urged holdovers from the Obama administration still in government to please stay, in an attempt to provide stability. He also expressed his concern that top-level cabinet officials might leave the Trump administration.
I dont want to see the chief of staff quit, I dont want to see the secretary of state quit, I dont want to see the secretary of defense quit, he said.
Biden also provided profound criticism of Trumps ability to run the country. He noted that the Trump campaign didnt expect to win  they werent prepared to govern and said: We have a president that doesnt understand governance.
The blistering critique of the Trump adminstration contrasted with the nominal topic of the discussion: bridging the political divide. The format involved Biden and Kasich sitting next to each other in armchairs with a moderator occasionally trying to get a word in between the two notoriously verbose politicians. 
The two men, both of whom have for run for president in the past and have been mooted as potential 2020 candidates, shared their concerns about Trumps attacks on the press. Biden pointed a finger at the rightwing website Breitbart  run by Trumps former chief strategist Steve Bannon  saying when you delegitimize the press to degree where they arent believable at all then Katie bar the door.
But they also complained about last years campaign coverage. Kasich said he had called the head of an unnamed news organization that had made a billion dollars from the 2016 election and asked if he could look himself in the mirror.
The two politicians also criticized increasing partisanship in Washington DC. Kasich bemoaned what he saw as political pressure on Democrats to support the Affordable Care Act. If you dont stand behind Obamacare, Bernie and the boys will come and get you, said the Ohio governor.
Biden struck somewhat similar notes, noting that increasingly politicians faced electoral threats only in primaries, which moved political parties further to the political extremes. 
The center is shrinking, said Biden, although he quickly added that that doesnt mean center per se is good. Instead, it simply meant there was less common ground for compromises to be struck.
Biden and Kasich did disagree on the issue of free speech on campus. The former vice-president bemoaned efforts to shut down any speaker on college campuses.
Dont be like these other people, said Biden to the crowd. Dont give the Trumps of the world the ability compare you to Nazis, compare you to the racists because youre doing the same thing, youre silencing.
In contrast, Kasich said he supported blocking extreme speakers. Were not talking now about people who have very diverse ideas, whether left wing or right wing, someone who is there for the explicit purpose of trying to incite.
But their disagreements seemed mild as both politicians happily went back and forth with each other, freely exchanging compliments. The only awkward moment was early on when Biden said that Kasichs father worked as a milkman. The Ohio governor, who invariably mentioned during his 2016 presidential campaign that his father was a mailman, blanched at the statement and immediately corrected it.
