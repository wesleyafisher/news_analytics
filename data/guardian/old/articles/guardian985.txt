__HTTP_STATUS_CODE
200
__TITLE

Treasury will need to plug gap in tax as drivers switch to electric cars

__AUTHORS

Phillip Inman
__TIMESTAMP

Wednesday 26 July 2017 12.54EDT

__ARTICLE
The switch to electric cars poses a big financial problem for the government  because every time a driver switches from a petrol or diesel car to an electric vehicle, the government loses 57.95p per litre in fuel tax at every fill-up. 
In total, duties on petrol and diesel add up to almost 28bn a year for the exchequer. Worse for the chancellor of the day, petrol and diesel sales make a contribution to VAT. VAT is charged at 20% of the wholesale price plus the duty, which equates to 16.7% of the final price. Thats a form of double taxation and explains why more than 65% of the cost at the pumps goes to the exchequer.
An electric car charged from the grid will currently generate just 5p in VAT for every pound spent. If the car is charged directly from solar panels on a garage roof, the Treasury is likely to go empty-handed.
To help plug the gap, the Treasury could increase VAT on energy to 20%. Road tax could also be increased with an emphasis on larger and less fuel-efficient cars  even more than now. But a tax on car ownership is unlikely to be popular, especially with green campaigners, who would prefer to tax the use of cars.
Mike Hawes, head of the car industry body, the SMMT, said it was likely that the government would look to road pricing in some form to take over from fuel duty. He said the technology already existed to track vehicles and charge drivers per mile. 
Kevin Nicholson, head of tax at PwC, said the government would need to move quickly to reverse a decline in fuel duty that is already under way. The Treasurys official forecaster, the Office for Budget Responsibility, has predicted a decline of a fifth in receipts by 2020/21.
The government will need to find alternative sources of tax revenue. And it would be a mistake to simply look for ways to plug the hole left by fuel duty. The fourth industrial revolution, with new technologies like artificial intelligence, is going to continue to shake up the tax base.
More immediately, Brexit will force change to the tax systems as rules need to be revisited. The need for a fundamental review of the UK tax system can no longer be ignored, he said.
