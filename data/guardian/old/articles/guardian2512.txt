__HTTP_STATUS_CODE
200
__TITLE

EU prepared to delay detailed trade talks after failure to break impasse

__AUTHORS

Daniel Boffey
__TIMESTAMP

Saturday 14 October 2017 17.20EDT

__ARTICLE
EU plans to offer Britain a detailed vision of a future post-Brexit trading relationship by Christmas, if sufficient progress has been made on the divorce bill by then, have been thrown into doubt following a meeting of diplomats from the 27 member states.
Leaked documents suggested earlier last week that European leaders would present an agreed position on a transition period and a trade deal in December, should the UK make further concessions.
That promise was expected to be made at a European council summit this week, where leaders are likely to rule that insufficient progress has been made at this stage on the opening withdrawal issues of citizens rights, the financial settlement and the Irish border, and so trade talks would be delayed.
However, it is understood that at a meeting of key diplomats on Friday evening, EU member states discussed weakening the language in the draft statement about their intentions in December, to give themselves greater flexibility in how they respond when they assess the rate of progress.
Some member states argued during the 90-minute meeting in Brussels that the EU should not make any promises at all about having an agreed position by the time of the European council summit on 14 December.
Multiple EU sources said the member states were concerned that they might be boxing themselves in.
How detailed do we want to be about what we will do in December? one said. Some feel that maybe we should be more general.
The development suggests there is some concern within the EU itself that it might not be able to find agreement on trade and the transition period by Christmas, although none of those involved in the meeting suggested that was the case.
It does illustrate how precarious the negotiations now are, with the EU in no mood to make promises to the UK, despite the dwindling amount of time left under the two years of the article 50 withdrawal process.
The Observer understands EU diplomats did agree that the bloc will promise at the European council summit this week to start internally scoping out their own vision of a future transition period and trade deal, however.
One source said the move was both prudent planning by the EU and an attempt to encourage the British negotiators to be more forthcoming in the most difficult areas of the talks. EU ministers will redraft the wording of the so-called European council conclusions on Wednesday in Luxembourg in preparation for leaders signing off on the statement on Friday in Brussels.
The development follows a difficult week for the British government which was told by the EUs chief negotiator, Michel Barnier, that the talks were in a very disturbing state of deadlock due mainly to Britains reluctance to offer any detail on what financial commitments it is willing to honour. The European commissions president, Jean-Claude Juncker ,while acknowledging Europes debt of gratitude to the UK for its role during the war, after the war, before the war, also insisted: Now they have to pay.
Relations were further undermined by suggestions from the chancellor Philip Hammond, that the EU was the enemy.
The Dutch finance minister, Jeroen Dijsselbloem, who heads the 19-member state eurozone group, responded: I dont think those kind of etiquettes are helpful, I think the situation is quite worrisome because we are stuck. I think we all realise that being stuck is first and foremost for the UK a huge risk but of course could also be negative for the EU. I dont think we should use names but try to find a solution.
