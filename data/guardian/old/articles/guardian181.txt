__HTTP_STATUS_CODE
200
__TITLE

Trump says Cuba 'responsible' for alleged sonic attacks, but offers no evidence

__AUTHORS
Staff and agencies
__TIMESTAMP

Monday 16 October 2017 15.51EDT

__ARTICLE
Donald Trump has said that he believes Cuba is responsible for unexplained incidents that the US says have injured at least 22 American government workers.
The United States has avoided casting blame on Cuban President Raul Castros government for the mysterious sonic attacks that began last year and have eluded an FBI investigation. 
Trump did not say whether he believed that Cuba was behind the diplomats injuries or merely shared responsibility for the fate of the Americans for failing to keep them safe on the countrys soil.
I do believe Cubas responsible. I do believe that, Trump said in a Rose Garden news conference. And its a very unusual attack, as you know. But I do believe Cuba is responsible.
There was no immediate reaction from Cubas embassy in Washington. Castros government has repeatedly denied both involvement in and knowledge of the alleged attacks.
Trump offered no new details about what type of weapon might have caused damage ranging from permanent hearing loss to mild traumatic brain injury, or concussion. The state department has said that despite the lengthy investigation and FBI visits to the island, the US still cant identify either a culprit or a device. 
Trumps ambiguous allegation against the Cubans was likely to increase tensions even further between the two longtime former enemies. The US and Cuba reopened diplomatic relations in 2015 after a half-century of estrangement, but the alleged attacks on Americans and steps taken by Washington in response have started to unravel those budding ties.
Since first disclosing the incidents in August, the Trump administration has avoided accusing Cuba of perpetrating an attack.
Responding to Trumps comments, Ben Rhodes, Barack Obamas foreign policy adviser, who was involved in negotiating the previous administrations rapprochement with Havana tweeted: Trumps own State Dept has not said this  has gone out of their way to say they dont know who is behind attack. 
Still, the US has pointed to Cubas tight control over security in Havana and its close surveillance of Americans working there as reasons to believe that Cuba might know more about what has transpired than it has let on. The state department has also said that no matter who is attacking Americans, it was Cubas responsibility under international law to protect US embassy workers.
We believe that the Cuban government could stop the attacks on our diplomats, the White House chief of staff, John Kelly, said last week.
The state department has said 22 Americans are medically confirmed to be affected and that the number could grow. The symptoms and circumstances reported have varied widely, making some hard to tie conclusively to the alleged attacks. The incidents began last year and are considered ongoing, with an attack reported as recently as late August.
