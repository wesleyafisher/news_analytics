__HTTP_STATUS_CODE
200
__TITLE

Spice World: the feminist movie? When girl power hit the big screen

__AUTHORS

Lizzy Dening
__TIMESTAMP

Thursday 28 September 2017 01.00EDT

__ARTICLE
Im pretty sure that when my dad took me and my best friend to see Spice World: The Movie in 1997, it was for a quiet life rather than to make a point about female identity. Although for the record I always respected his favourite Spice Girl  Sporty  as being the thinking mans choice. But, for all its stereotyping and campy scenarios, perhaps the trip did have more to offer an 11-year-old girl than just a fun afternoon and salted popcorn.
For one thing, I suspect it was the first film I saw that knocked the Bechdel test out of the park  its packed with female-centric conversations and barely touches on men, even less in a romantic context. The closest our heroes come to concerning themselves with relationships is when Emma says she wishes boys could be ordered like a pizza. Or when Geri is seen talking to an awkward bloke at a party, using the F-word itself: it was the first time Id seen a woman I aspired to be labelling herself a feminist.
The male roles, aside from gratuitous cameos, are essentially Richard E Grant as their highly strung manager (who struggles to contain the girls), Roger Moore as his boss (who speaks in unintelligible metaphors) and Barry Humphries as the evil newspaper boss who wants to take the band down. And spits a lot.
Then theres the inversion of the male gaze, which happens as the credits roll and the Spice Girls address the audience  calling out cinema-goers for snogging in the back row (and, quaintly, those watching on video). We may be voyeurs in the Spice Girls world, but they want us to know its only at their invitation.
Plenty has been written on girl power and whether it was a third-wave tool of empowerment for young fans, or a cynical soundbite designed to sell T-shirts. At their core, though, the Spice Girls were a manufactured group who overthrew their creators like a Union Jack-clad Frankensteins monster.
A sense of this anarchy is captured in Spice World, as they cut short photoshoots and escape their tour bus. Two decades down the line, I have to confess that its a joy to watch. I was anxious about how my memories of it as a colourful explosion of silliness and joy would stand up, especially from a feminist perspective, but I was pleasantly surprised.
Even as girls, we knew it wasnt a brilliant film, but that wasnt the point. The fact of the matter was that it was for us, and us alone; like all self-respecting teens, we wanted a little slice of pop culture that our parents wouldnt understand. Films allow women to consider self-transformation, the possibility of agency, and the sheer ordinariness of the Spice Girls, with their unpolished British accents and their home-dyed hair, made it easy for us to see ourselves in them.
Empire magazine criticised the curiously asexual ordinariness of the central quintet. But as awkward young girls, this was exactly the reassurance we needed. We didnt want gleaming otherworldly pop vixens  we wanted women we could, in a few years, end up like.
The secret of the bands appeal was that they werent dazzlingly good at anything, and that didnt stop them doing whatever they wanted with gusto (and ridiculously 90s lip liner).
Ultimately, the Spice Girls were just relatively normal women, accepting, or indeed exploiting  in the case of Victorias so-bad-its-brilliant acting  their limitations to their best advantage. And that, for any schoolgirl worth her Tammy Girl platform trainers, was an important lesson in self-acceptance.
