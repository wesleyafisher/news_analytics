__HTTP_STATUS_CODE
200
__TITLE

Wonga loses 65m but insists it is on track to be profitable again

__AUTHORS

Jill Treanor
__TIMESTAMP

Friday 29 September 2017 02.06EDT

__ARTICLE
Wonga made a loss of nearly 65m last year, but has insisted that it is on track to return to profitability in 2017, in part due to its Newcastle United sponsorship ending.
The UKs biggest payday lender was forced to tear up its business model in 2014 after running into regulatory problems and because of the cap on loan rates introduced by the Financial Conduct Authority in 2015.
A new management team took over in late 2014 and said in the lenders annual report that it expected to report a significantly improved performance for the current financial year.
Among the reasons listed for this was the end of its football sponsorship, which is reported to have cost 24m over four years, and Wongas efforts to cut costs, including moving its head office across London.
Tara Kneafsey, the chief executive of Wonga, said that since 2014, the business had been transformed as we have expanded our product offering, strengthened our governance, rationalised our operations and reduced our cost base.
She is thought to be the highest-paid director disclosed in the 2016 annual report, with yearly remuneration of 467,000.
In 2014 Wonga was ordered to pay 2.6m in compensation to about 45,000 customers after sending threatening letters from non-existent law firms, and was then forced to write off 220m of debts of 375,000 borrowers who it admitted should never have been given loans.
The cap on payday loans means interest and fees on all high-cost, short-term loans have been capped at 0.8% per day of the amount borrowed. Before interest rates were capped, the company had come under attack for charging annual interest rates as high as 5,853% on short-term loans. 
Products on its website  within the cap  show that rates offered on a six-month 600 loan with six monthly repayments are the equivalent of a 1,086% annual rate, although Kneafsey said annual comparisons were not a realistic gauge of the borrowing rate.
The annual report shows revenue was 76.7m, up from 65.2m but still well below the 300m Wonga was generating in 2012. The loss of 64.9m is lower than the 80m incurred in 2015, however.
About 60% of the lenders revenue is generated in the UK, with the rest made in Poland, South Africa and Spain. 
