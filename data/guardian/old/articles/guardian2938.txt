__HTTP_STATUS_CODE
200
__TITLE

Flat pack to the future: how Ikea shaped our lives

__AUTHORS

Nell Frizzell
__TIMESTAMP

Sunday 8 October 2017 01.00EDT

__ARTICLE
I have measured out my life in coffee spoons and mattresses, bath mats and bookcases, cushions and double duvets. These touchstones of my life may seem sporadic  the cooking pot I went off to university with, the first towel I bought (rather than stole from my mother), the rucksack I wore during my pregnancy, the changing table that will, I hope, one day serve as my childs desk  but they all unite under a single Swedish philosophy: To create a better everyday life for the many people.
I didnt know about this philosophy until I stood, bathed in the gentle air of meatballs, in the foyer of the Ikea Museum in lmhult, Smland and read it off a giant white wall beside a portrait of the Ikea founder, Ingvar Kamprad. The museum, which opened in 2016 on the site of Ikeas first ever store, tells the story not only of Swedish life via its common household objects (not to mention the more arresting ones such as bog ore and sausage horn), but gives a glimpse into the life of Kamprad, his staff, those early furniture experiments (denim divan, anyone?) and the unlikely rise to global proportions of this small, backwood business.
If you havent heard of lmhult, this may be because it is, essentially, a small Swedish town in the middle of the woods, so far south its actually closer to Copenhagen than Stockholm, with fewer than 20,000 residents. Imagine a village in the Home Counties, but with more cinnamon buns and where everyone is dressed in H&M.
Ikea is based on the founding principles of form, function, quality and low price
However, it was here, in 1943, that the young entrepreneur, Kamprad, founded his business, moving on from selling cheap matches to neighbours to using the local milkman to distribute small household items across Sweden at a discounted rate. In 1953 there followed his first furniture showroom, in 1956 his first flat-pack furniture and in 1958 his first Ikea store.
It would take until 1987 for Ikea to reach the UK, opening its blue and yellow doors for the first time in Warrington. Why Warrington? Why not? If you have a business based on the founding principles of form, function, quality, sustainability and low price, setting up in a northern manufacturing town with plenty of room and a good rate of home ownership doesnt seem a crazy idea.
Thirty years later, it is hard to imagine any student bedroom, any couples first flat, home office or granny annexe untouched by the sharp lines of a Billy bookcase, Pong chair, Klippan sofa, Lack table or Ribba picture frame. You might not love them, you might not even remember them, but you own them nonetheless. Just as the font in a good book is the very one you dont notice, so the ubiquity of Ikea in our homes has rendered the Kallax unit, the Fargrik mug and Fado lamp near-invisible.
Ikea is currently preparing to open its first store in India
During my trip to lmhult, and after filling up on a cheese and crispbread brunch at Icom  the Ikea communications department  one of Ikeas longest-serving staff members (a Danish woman in an immaculate white shirt) let me into the companys little inside joke about those funny Scandinavian names. It goes that while the sofas are given Swedish place names and the beds are named after towns in Norway, the Danish place names are reserved especially for toilet brushes.
These days, Ikea can sometimes feel like an emblem of the northern European, socially democratic future we could have won: a principal partner of the Living Wage Foundation with a strict 50/50 gender split target for management teams across the globe. Ikea also serves under founder Kamprads philosophy that waste is a mortal sin. As we sat in a small, white room surrounded by LED lights and flooring made from recycled wood, Nanette Weisdal, the companys sustainability leader, explained to me that their code of conduct strives to achieve ever higher rates of recycling, reuse and repair, and to source sustainable materials, labour and products across the world.
Afterwards I strolled across the carpark from the Ikea Museum canteen to the Ikea hotel under a light Swedish drizzle and wondered what comes next for the company and for me, a customer.
Today, as the company prepares to open its first store in India, I scroll through baby bouncers and nursing chairs. As they deploy more than 16,000 flat-pack refugee shelters to crisis locations from Nepal to Baghdad to Dijbouti, I wonder what sort of world my child, rolling about in his Charmtroll sleeping bag, will inherit. And as we prepare to be cut adrift from the safety, protection and culture of the EU, will we look north to our Scandinavian cousins, or turn inward in a fit of nationalism?
Either way, one thing is for sure: home will never be the same again.
