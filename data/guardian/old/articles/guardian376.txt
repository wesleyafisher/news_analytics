__HTTP_STATUS_CODE
200
__TITLE

Lleyton Hewitt implores struggling Bernard Tomic to 'work harder'

__AUTHORS
Australian Associated Press
__TIMESTAMP

Sunday 8 October 2017 22.31EDT

__ARTICLE
Lleyton Hewitt insists Bernard Tomic will need to put the work in or face the prospect of wilting during the brutal Australian summer of tennis. Tomic has spiralled to 144th in the rankings after a disastrous 2017 campaign and will likely need to qualify for the Australian Open unless Hewitt and Tennis Australia offer the struggling star a wildcard.
Tennis Australia is making no promises after the one-time world No17 opted out of last years Olympic Games and also decided not to represent Australia in Davis Cup this year.
Hewitt is hoping the 24-year-old can somehow salvage something from the last month of the season following last weeks second-round loss in Tokyo. Hes still got a few tournaments, Hewitt said on Monday. With Bernie, its about getting into that routine of playing a lot of matches again. He just hasnt played enough this year. 
Then that cycle of trying to get out of that rut of losing first and second round isnt easy to do. His ranking drops off so hes not seeded in those tournaments. Hes getting tougher draws earlier on then.
As Davis Cup captain, Hewitt will have a big say in whether or not Tomic is granted a wildcard into the first grand slam of 2018. Tomic has traditionally performed well at Melbourne Park, where he won the junior title at 15 and became the youngest player to win a mens main-draw match at 16. 
At the end of the day, hes still got to work a lot harder, Hewitt said. Hes got to put the hours in on the court, but off the court as well [as] in the gym to go out there and hopefully be ready to play the Australian summer. The Australian Open, over five sets, you get some pretty bloody hot days there as well that hes got to be ready to play (in).
Once tight with Tomic, Hewitt said he was disappointed and frustrated that the Queenslander made himself unavailable for Davis Cup in 2017 but hoped the former Wimbledon quarter-finalist could revive his career.
Ive had a few chats with him, so have a few people from Tennis Australia as well. I get along really well with Bernie, Hewitt said. Obviously I really tried to to help him out a lot last year, in 2016, and its been disappointing. Its pretty much been 12 months since about September 2016 that hes really struggled.
I think the tennis community, especially Tennis Australia, would really love him to get back there because hes a quality top-20, top-25 player easily and his goal should be to try and get into the top 10 some day.
