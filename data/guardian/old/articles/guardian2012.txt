__HTTP_STATUS_CODE
200
__TITLE

Colin Kaepernick to file lawsuit against NFL owners over collusion

__AUTHORS

Tom Lutz in New York
__TIMESTAMP

Sunday 15 October 2017 16.03EDT

__ARTICLE
Colin Kaepernick, who many believe has been blackballed by the NFL following his protest against racial injustice in the US, is reportedly set to file a lawsuit against the leagues team owners. 
According to Mike Freeman of Bleacher Report, the former San Francisco 49ers quarterback has hired attorney Mark Geragos and will claim NFL owners have colluded to keep him out of the league. Under the leagues Collective Bargaining Agreement teams and the NFL are forbidden from coming together to deprive a player of employment.
If the NFL (as well as all professional sports teams) is to remain a meritocracy, then principled and peaceful protest  which the owners themselves made great theater imitating weeks ago  should not be punished and athletes should not be denied employment based on partisan political provocation by the Executive Branch of our government, Geragos said in a statement. 
Such a precedent threatens all patriotic Americans and harkens back to our darkest days as a nation. Protecting all athletes from such collusive conduct is what compelled Mr Kaepernick to file his grievance. Colin Kaepernicks goal has always been, and remains, to simply be treated fairly by the league he performed at the highest level for and to return to the football playing field.
Kaepernick chose to kneel last season during the national anthem. He was joined by other NFL players and the protests have continued into this season: on Sunday seven San Francisco 49ers players knelt during the anthem. 
The movement has angered Donald Trump, who ordered Vice-President Mike Pence to leave a game last week in which players were protesting. 
The protests have also been blamed for falls in the leagues TV ratings this year, although other factors, such as the decline of television viewership in general, could also be at play. Surveys have found the majority of white fans are opposed to the protests while most black fans support them.
Kaepernick, who led the 49ers to the Super Bowl in 2013, left San Francisco last season and has not been picked up since. The fact that many teams with a need for a quarterback have passed up the chance to sign Kaepernick left many to believe owners are either punishing him for his stance or believe his presence would alienate fans.
Its difficult to see because [Kaepernick] played at such a high level, and you see guys, quarterbacks, who have never played at a high level being signed by teams. So its difficult to understand, said Seattle Seahawks cornerback Richard Sherman earlier this year.
Obviously hes going to be in a backup role at this point. But you see quarterbacks, there was a year Matt Schaub had a pretty rough year and got signed the next year. So it has nothing to do with football. You can see that. They signed guys who have had off years before.
Another potential opening for Kaepernick appeared on Sunday when the Green Bay Packers lost their star quarterback, Aaron Rodgers, to a broken collarbone.
