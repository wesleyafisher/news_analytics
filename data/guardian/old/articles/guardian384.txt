__HTTP_STATUS_CODE
200
__TITLE

Borg vs McEnroe review  a five-set thriller of a film

__AUTHORS

Wendy Ide
__TIMESTAMP

Sunday 24 September 2017 03.00EDT

__ARTICLE
A tale of sporting rivalry is given a brooding, introspective Scandinavian twist by Janus Metz, hitherto best known for his award-winning documentary Armadillo. Stoic, seemingly unflappable Bjrn Borg (Sverrir Gudnason) and volatile, temperamental brat John McEnroe (Shia LaBeouf) competed for the Wimbledon title in 1980. On paper, it was a clash of opposites. But in fact, this film argues that the two men had more in common than anyone suspected at the time. Ice-man Borg, the main focus of this film, was in fact a volcano; his obsession with detail, his superstitions, were all part of the meticulous control mechanism he constructed to prevent the eruptions of anger that so tarnished McEnroes early reputation.
Whether or not you know the outcome, this is a cracking watch. Metz captures the period effectively  the Mondrian-style colour blocks of the tennis court; the saturated, synthetic palette of the 1970s and early 1980s. But the performances carry the film. The two competitors are exceptionally well cast: LaBeouf is a little old for the role but conveys McEnroes isolation and vulnerability along with the adolescent petulance. But Gudnason is remarkable; not only is he an uncanny physical match, he also manages to combine a stewing inner turmoil with a demeanour that is as impassive as a blank scoreboard.
