__HTTP_STATUS_CODE
200
__TITLE

Same-sex marriage survey: help us track targeted ads on Facebook

__AUTHORS

Nick Evershed
__TIMESTAMP

Monday 16 October 2017 20.26EDT

__ARTICLE
Organisations are paying Facebook to target Australians with ads about the same-sex marriage postal survey.
Guardian Australia has been tracking some of these ads with the help of readers, and has so far uncovered ads without proper authorisation, an ad from no campaigners instructing people how to change their vote, and ads that contain or link to misleading or homophobic material.
 If these ads dont appear in your feed, it is almost impossible to know about them. This makes it very difficult to fact-check their content, or determine if they breach any laws around the same-sex marriage survey. Its also very hard to work out which parts of society the campaigns are targeting. 
The only way you can see if a post has been sponsored (that is, turned into an ad) is if it appears in your feed. For anyone else looking at the post, there is no definitive indication that it was sponsored.
Sponsored posts can also be hidden from the public and not show up on advertisers pages at all. This tactic was seen throughout the Trump campaign in the 2016 election and is believed to have contributed to his victory. Facebook has since come under significant political pressure in the US, and has committed to improving the transparency of political advertising on its site.
So, what can be done about it?
Throughout the marriage campaign the Guardian has been using a form for readers to send us their sponsored ads (as well as pamphlets, and other things). Now were partnering with the US-based news organisation ProPublica to collect political Facebook ads on a larger scale.
ProPublica have developed a browser plugin for Chrome and Firefox that collects ads when you are on Facebook, and allows you to classify the ads collected as political or not political.
The political ads are sent to ProPublicas database, along with the ad-targeting information. The ad-targeting information categories are usually things like an age bracket, gender, a general geographic location and interest in a topic. You can see the information Facebook is using to show you ads by looking at the adverts menu in the Facebook settings.
No other information is sent to the database. We are not recording your name, date of birth or any other specifics  just the categories that Facebook provides under the Why am I seeing this ad? information panel. 
The targeting information is important as it gives us an insight into the demographics or areas political campaigns are interested in targeting.
If you are not comfortable with sharing this information, we suggest you do not install the browser plugin.
We will make these ads available in a searchable database on our site, along with the targeting demographics. This will enable greater transparency around political advertising in Australia, and give an insight into which segments of the population both sides of the marriage survey are targeting. It may also be of use in upcoming state and territory elections.
If you would like to participate in this project, you can install the browser plugin for Chrome here, and the plugin for Firefox here.
Once you have installed the plugin, click on it to go through the set-up process. After the plugin is set up, you can collect ads by scrolling through Facebook, and you can classify the ads collected by clicking on the plugin again.
