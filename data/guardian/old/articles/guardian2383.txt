__HTTP_STATUS_CODE
200
__TITLE

Cocktail of the week: Henry Moore Bound To Fail (Bronze) recipe

__AUTHORS
Paul McCarthy
__TIMESTAMP

Friday 25 August 2017 11.00EDT

__ARTICLE
Like many of our cocktails, this is inspired by our neighbours, Hauser & Wirth art gallery. Serves one.
50ml gin (we use Tanqueray)25ml creme de cassis/blackcurrant liqueur (we use one made locally by the Somerset Cider Brandy Company)1 dash apple juice1 squeeze lemon juice2 sugar cubes dissolved in a tiny amount of hot water1 sprig fresh rosemary 
Put the gin, liqueur, apple and lemon juice, and sugar in a shaker. Rub the rosemary between your hands, to release the essential oils, add to the shaker with a handful of ice and shake hard. Strain over crushed ice into a highball glass and serve. 
 Paul McCarthy, Roth Bar & Grill at Hauser & Wirth, Bruton, Somerset.
