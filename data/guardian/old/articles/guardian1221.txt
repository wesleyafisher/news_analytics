__HTTP_STATUS_CODE
200
__TITLE

Shia LaBeouf embarks on a digital road trip: #TAKEMEANYWHERE

__AUTHORS

Will Coldwell
__TIMESTAMP

Wednesday 25 May 2016 06.24EDT

__ARTICLE
At the time of writing, we can confirm that the exact location of Shia LaBeouf is 410331N 1045245W. Want to take him for a ride? Better get yourself to Interstate 25, about 90 miles north of Denver, Colorado. Hes waiting for you by the side of the road and, as far as his latest project is concerned, isnt too bothered which way youre heading.
From 23 May to 23 June, LaBeouf, along with the other members of his meta-modernist art collective, Nastja Rnkk and Luke Turner, is embarking on a road trip to anywhere, and theyre letting the public know their whereabouts by tweeting their coordinates from their Twitter account @thecampaignbook, along with the hashtag #TAKEMEANYWHERE.
The journey will be documented in real time on takemeanywhere.vice.com and for 30 days the trio will document the people and places they encounter along the way.
Commissioned by Boulder Museum of Contemporary Art as part of MediaLive 2016, and the Finnish Institute in London, the project follows in the same vein as the collectives other works, which play with the interactions that exist between digital and physical communication. 
These have included #ELEVATE, in which the trio occupied a lift in Oxford for 24 hours during which visitors could join them and speak to the artists  a performance that was also live streamed on YouTube  and #IAMSORRY, in which LaBeouf took a seat in a gallery in Los Angeles while members of the public were invited to choose items from a table of objects to take into the room with them and interact with the artist. During this performance one woman took the interaction to extremes by raping him.
While the latter was evocative of Marina Abramovis infamous performance The Artist is Present, the latest also brings back memories of Guardian Travels TwiTrips, in which travellers were guided in real time with tips and suggestions from locals via Twitter. 
Speaking to Vice, which is partners in the project, Rnkk said: Its about trust, and also a journey. Im more interested in the in-between state than arriving at a destination.
They also described #TAKEMEANYWHERE as their take on the Great American Road Trip, alluding to the paradox of wanting to escape into the landscape, while being tracked by the public the whole time. 
Its the most expansive and most intimate thing weve done, said LaBeouf. 
One Reddit user raced against time to find the trio at the coordinates they tweeted. They tracked them down successfully and joined the artists for lunch, posting a video of the meeting on Youtube. 
 You can follow the artists journey on @thecampaignbook and takemeanywhere.vice.com
