__HTTP_STATUS_CODE
200
__TITLE

The sleepy side of the Italian Riviera

__AUTHORS

Mary Novakovich
__TIMESTAMP

Saturday 26 August 2017 05.00EDT

__ARTICLE
As the plane circled over the Ligurian coast towards Genoa, my fellow passengers were getting excited. They all seemed to be heading east  to glamorous Portofino, or the Cinque Terre for hiking. But I was going west, along a road that was practically built by the British but has been curiously forgotten by them.
The 19th-century British habit of wintering on the Mediterranean changed the fortunes of many seaside villages, and Alassio was no exception. But this small seafront resort between Savona and Imperia seems to have slipped off the British radar. Even my Italophile friends hadnt heard of it.
Until the 1930s, it was so popular with Britons that the winter population would swell by about 5,000. They built the three sure signs of a British resort: an Anglican church (now a cultural centre), Hanbury Tennis Club (still going strong) and an English-language lending library (the second-largest in Italy). Now the permanent British population is about 15.
Tastes changed and people moved on  away from one of the loveliest town beaches Ive seen in Italy. Its wide expanse of smooth sand is framed by typically Ligurian terracotta and green-shuttered ochre houses, and sheltered from the wind by mountains to north and west. In July and August, it is teeming with holidaying Italians. By going in September, I still caught the heat but missed the crowds.
The aptly named Hotel Eden, a pleasant three-star with balconies for grandstand sunset views, was less than 15 minutes stroll along the seafront from Alassios gleaming new pier. In the town centre, romantic beachfront restaurants with candlelit tables sat comfortably with rustic-chic cafes. A tiny hole-in-the-wall bar made a satisfying spot for a late night grappa.
A few minutes further on, Osteria La Sosta was charging 20 for three courses with wine and coffee. It was pure Ligurian food: ravioli-like pansoti in creamy walnut sauce; trofie pasta with pesto, green beans and potatoes; fall-off-the-fork rabbit cooked with olives, capers and pine nuts.
Running parallel to the promenade is Alassios heart, or rather its intestine, which is what Budello translates as. More prosaically known as Via XX Settembre, this narrow street of tall terracotta buildings is home to shops, delis and cafes, including the superb Gelateria a Cuvea.
In the pine-covered hills above Alassio are the villas built by the British, few more evocative than Villa della Pergola (villadellapergola.com). Built in 1875 by two Scotsmen, the villa has been owned by Virginia Woolfs cousin, Sir Walter Hamilton Dalrymple, and Daniel Hanbury (whose family created Villa Hanbury gardens further west, near Ventimiglia). Now its a luxury hotel, with a lobby display recalling frequent visitors including Edward Lear and Edward Elgar, who was inspired by a stay here to write his 1904 overture In the South (Alassio).
Back in the town centre, the Muretto di Alassio is a low wall covered with more than 550 ceramic plaques of celebrities signatures. Along with Ernest Hemingway (who started it with the cafes owner back in 1953), are scores of Italian stars, plus Jean Cocteau and, er, Eric Sykes and Max Bygraves. Evidently, Brits left their mark here in more ways than one.
Alassio had me thoroughly enchanted by now, and I was keen to discover the rest of the area. A few kilometres south I came to what looked like Alassio in miniature: Laigueglia, with a similarly pretty beach, colourful seafront houses and a Budello of its own. The town was gearing up for its San Matteo festival, with market stalls and delightfully old-fashioned wooden games. Lunch was a big plate of grilled anchovies at U Levantin. This, I discovered, was an osteria sociale, a social enterprise that provides jobs for refugees and people with disabilities. Gratifyingly, every table was full.
In Ligurias rugged hinterland, medieval villages hide among the olive groves and wooded peaks. Rising from the coast road 10km south of Laigueglia is Cervo, a medieval hilltop village whose maze of ochre alleys lead to the magnificent baroque San Giovanni Battista church, and sweeping views of the Mediterranean.
The air got fresher and more alpine as I headed north along the Valle Arroscia towards Pieve di Teco, not far from the Piedmont border. Its massive 18th-century San Giovanni church and neoclassical dome hinted at a grand past when the village was an important border town. Now its pleasingly sleepy, with a medieval arcaded high street and a huge monthly antiques market.
Back on the coast, I visited the beautifully preserved Roman town of Albenga, whose medieval towers have given it the nickname of the San Gimignano of Liguria. Luckily, its not; otherwise it would have been heaving. Maybe the crowds were at Albengas beach, leaving the narrow lanes free for relaxed ambles past quirky art-covered walls. I ate deliciously simple Ligurian food at Turl on Via Torlaro (look out for the menu scribbled on bits of paper taped to the wall).
On the drive back towards Genoa airport, I stopped at another beautiful village, medieval-walled Noli. As I sat on the beach snacking on focaccia, another Ligurian speciality, I recalled the chat I had with the English librarian in Alassio. Jacqueline Rosadoni had been on her way to Florence in 1959 when she stopped in Alassio, met an Italian and ended up staying. I fell in love with Alassio, she said. I fell in love with Liguria. I fell in love with the Mediterranean, and I couldnt live anywhere else.
I was beginning to feel the same.
 The trip was provided by the Liguria tourist board. Hotel Eden has doubles from 90 B&B or 128 half-board. Car hire was provided by Enjoy Car Hire, which offers four days car hire from 95. Flights from Stansted to Genoa (from 40 one-way) were provided by Ryanair
