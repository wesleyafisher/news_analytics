__HTTP_STATUS_CODE
200
__TITLE

Leicester opt to twist rather than stick for a second time in eight months

__AUTHORS

Stuart James
__TIMESTAMP

Tuesday 17 October 2017 16.16EDT

__ARTICLE
The conversation lasted only a couple of minutes and there was little by way of explanation when Jon Rudkin, Leicester Citys director of football, delivered the news to Craig Shakespeare that he was out of a job only four months after he had been appointed manager on a three-year contract.
It was not the sort of day that the 53-year-old had in mind when he drove through the gates at the clubs Belvoir Drive training ground earlier that morning and prepared to watch a number of his fringe players take part in a friendly match against a team from Nottingham Forest, totally oblivious to the fact that the best part of a decade of service was about to come to an end in the blink of an eye.
The statement that was issued a few hours later, at 6pm, shed little light on the real thinking behind the decision, saying only that the early promise under Craigs management has not been consistently evident in the months since and that a change is necessary to keep the club moving forward.
In reality it is hard to escape the feeling that Leicesters hierarchy never truly believed in Shakespeare in the first place, that deep down they wanted a bigger name yet felt they had little option but to give him the job on a permanent basis when he engineered such a dramatic turnaround as caretaker in the wake of Claudio Ranieris sacking.
That was eight months ago, when the Italian departed, and now Leicester find themselves back in the same position, knocking around at the wrong end of the table and scouring the managerial landscape for someone to satisfy the ambitions of Thai owners who are not prepared to tolerate the sight of another season of underachievement.
Shakespeare is entitled to think that it is too early to make a judgment on whether that would have been the case and could also be forgiven for citing some mitigating circumstances when it comes to Leicesters poor early results, starting with the fact that the season is only eight matches old and he has already come up against four of last seasons top six.
Another bone of contention would be the clubs business in the summer transfer window, most significantly the shambolic handling of the Adrien Silva deal, which was completed 14 seconds late and after Leicester had already agreed to sell Danny Drinkwater to Chelsea.
Shakespeare felt that Leicester should have dug their heels in over Drinkwater, refusing to sell at any price, and his disappointment over that transfer was compounded by the farce surrounding Silva, who is unable to play for the club after Fifa refused to ratify his move. Rudkins own thoughts on that fiasco would make for interesting reading.
As for the case against Shakespeare and why Leicesters owners acted, the statistics show that he had won only one of his last 11 league matches in charge  against newly promoted Brighton & Hove Albion at home in August. It would also be fair to say that the flat performance against West Bromwich Albion on Monday night, when Riyad Mahrez equalised 10 minutes from time to salvage a point, did not instil any confidence that Leicester were closer to getting back on track.
The players, however, remained behind Shakespeare and had not lost faith in him in the same way as they did with Ranieri last season. He was popular as a No2  a role that he fulfilled first for Nigel Pearson and then Ranieri  and staff as well as players were impressed with the way he adapted to the managerial role that required him to change his approach. With that in mind, players were surprised and, more than anything, bitterly disappointed when they found out via social media that Shakespeare had been sacked.
The fact that Leicester are now trying to appoint their third manager since February hints at wider problems that will be not be solved simply by putting a new face in the dugout. The truth is that ever since they wrote their names into the history books with that extraordinary Premier League title success, Leicester have made a pigs ear of things in many respects, especially when it comes to recruitment.
Reflecting on that triumph in 2016 and reacting to the announcement that Shakespeare had been sacked, Gary Lineker was scathing in his assessment of senior figures at Leicester on social media when he tweeted: Was always a miracle, but its even more remarkable really that Leicester won the league given the ineptitude of those that run the club.
What happens next is critical to bringing some stability to a club that is still trying to work out where it belongs in the Premier League. The word from inside the club is that Leicester do not have a replacement for Shakespeare lined up, with names as diverse as Carlo Ancelotti, Martin ONeill, David Wagner and Sean Dyche doing the rounds as possible replacements.
In the meantime Michael Appleton, the assistant manager, has been tasked with taking the team against Swansea City on Saturday at the Liberty Stadium, where Leicester were beaten 2-0 in February in what turned out to be Ranieris last league game in charge. It has been a curious eight months since.
