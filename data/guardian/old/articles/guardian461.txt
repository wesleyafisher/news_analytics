__HTTP_STATUS_CODE
200
__TITLE

Polyamory, bondage and feminism: the film that tells Wonder Woman's story

__AUTHORS

Stuart Miller
__TIMESTAMP

Thursday 12 October 2017 07.00EDT

__ARTICLE
Last year, as the US presidential election approached, the cast and crew of Professor Marston and the Wonder Women were finishing their shoot in suburban Massachusetts, thinking that William Marstons prescription for a better world was finally coming true. Marston, a psychologist who invented a forerunner of the modern polygraph, is best remembered for giving birth to Wonder Woman, a comic he created to show that if men could step back and let women run the world, wed have peace for our time.
Well, that didnt happen.
Instead, America is now ruled by a bully who routinely threatens war. Yet Marstons comic-book vision of the empowered woman did lasso control of the box office this summer. Wonder Woman became Americas second-highest-grossing film of 2017, and earned more than $800m worldwide. The movie was celebrated for giving women and young girls a stereotype-shattering superhero of their own, even if  just like the 1970s hit TV show with Lynda Carter  it simplified a complicated and kinky character as well as the quirky feminist propaganda Marston created. 
He thought: men wont give up their power unless women learn to sexually dominate or use what he called captivation emotion  basically, to take the power away from men for their own good, says Angela Robinson, writer-director of Professor Marston and the Wonder Women.
Robinsons film aims to frankly and fully explore Marstons theories and the unorthodox relationship he shared with his wife, Elizabeth, and their younger lover Olive Byrne, whose intellect and strong personalities inspired his conception of the superhero. (They are played by Luke Evans, Rebecca Hall and Bella Heathcote, respectively.)
They were brave, they were strong, they were forward-thinkers, Evans says. They were so adventurous in their relationship and in their opinions on life.
Heathcote adds that their tripod relationship would still be pretty scandalous today but that it worked, and the asymmetrical dynamic gives the actors the opportunity to play nuanced characters with an unusual arc.
A polyamorous relationship in the 1920s was radical but not much more than Marstons Disc theory, which stood for dominance, inducement, submission and compliance. Marston believed in dominance and submission but only when the dominant figure used inducement  which he argued that women were far better at  to gain submission. If someone was forced into compliance (the more male approach), it was accompanied by a simmering resentment. 
Even more subversive was Wonder Woman herself, whom Marston created in 1941 as a propaganda vehicle for his ideas. He filled the comics with images that were deemed to have deviant sexual undertones, from homosexuality to spanking and bondage  lots and lots of bondage. 
The comics are wild, theyre insane, Heathcote says. Its not surprising Marston got called before a tribunal  theyre overtly sexual and there are so many bondage elements.
The movie follows Marstons career  as a professor, developing the lie detector, and later writing the comics  along with the arc of the three-way affair, but it frequently jumps ahead to a scene in the 1940s where a committee head is interrogating Marston about the inappropriate material in the Wonder Woman comics. (It also drops hints about how Elizabeth and Olive each shaped Wonder Woman, such as through Olives signature bracelets, which became bullet-blockers in Marstons superhero.)
When Evans first read the script, he couldnt believe it was a true story, which it is and isnt. Robinson reshaped the characters to fit the story she wanted to tell. I started doing a lot of research on Olive, but the Olive in the script is very different from real-life Olive and it started spinning me out, Heathcote says, adding that while many of the situations were true, she had to approach her role as a fictional character.
The Marstons led an incredible sprawling tale of a life, and this film is just a haiku of their life, Robinson says. I tried to distill the most essential story I wanted to tell, which was about how the love affair between Marston, Elizabeth and Olive came together to create Wonder Woman.
Rebecca Hall says she was fascinated by the idea that Wonder Woman was written by a guy who wanted to teach boys that its all right to have women in positions of power, that you can be saved, and that women can be strong.
She says the film touches on the stigmas and internalized cultural prejudice that has made it so rare for female superheroes like Wonder Woman to lead big-budget movies. In light of the election campaign last year, she added: It feels like it is time to talk about all these issues in a new way.
Robinson has a line in the script that probably seemed hopeful when it was written but is bittersweet today. When Marston is facing interrogation about his ideas, he states that young boys must learn these lessons if they are going to grow up respecting powerful women and declares: It is important to me that young girls of today realize they have the power within themselves to create their own destiny, to be president of the United States if they want.
But the Marstons private life also created a dynamic that made the movie resonate for those involved. They were more accepting of what love or a relationship can look like, says Heathcote.
It is timely as an examination of what feminism is and for re-conceiving what a family is, Amy Redford, a producer, adds.
Robinson, a Wonder Woman fan, only discovered the background after an actor in one of her movies gave her a coffee table book about the superhero that briefly touched on the polyamorous relationship. She thought about what each of the women sacrificed and gained through this and realized their story needed telling as much as Marstons did. She spent four years writing and rewriting at night and on weekends and then spent several more trying to raise the money to get the film made.
Its hard enough to get a good film made these days and harder still when two of the main characters are women and it is a period piece without A-list stars, says Terry Leonard, another producer.
Fittingly, it was Wonder Woman herself who saved the day, as Robinson and her team were able to capitalize on the summer blockbuster as long as they got their movie done on time and on a tight budget.
It was pants on fire, we have to do this now for whatever money we can get, says Redford, who credits Robinson for being a warrior and making sure that whats on the page was something we could achieve with what we have.
Heathcote says she was grateful to have a woman telling the story. She says Robinson made the actors feel completely safe during the unusual sex scenes, by playing music and keeping everything within character and part of the narrative.
A womans perspective gave this story a lot more color, Evans says, adding that Marstons line about women being better at inducement than men rings true for him. Female directors get what they want in a very different way  they dont have to shout, they have tools men wouldnt think of using, and it works every time. Trust me, its inducement all the way.
