__HTTP_STATUS_CODE
200
__TITLE

Send us a tip about Emilia-Romagna for a chance to win a 200 hotel voucher

__AUTHORS

__TIMESTAMP

Wednesday 11 October 2017 12.38EDT

__ARTICLE
The Italian region of Emilia Romagna is particularly blessed when it comes to visitor appeal: slow food and fast cars, busy cities and unspoilt hills, glitzy seaside and sleepy villages. Wed like to hear about your best experiences. Tell us about things to do and see, places to stay and great things you ate.
Send us your tips via GuardianWitness, with as much detail as you can (including website and prices etc, if possible) in around 100 words.
The best tips will appear in print in next weekends Travel section and the winner, chosen by Tom Hall of Lonely Planet, will receive a 200 hotel voucher from UK.hotels.com, allowing you to stay in more than 260,000 places worldwide. Submit your tip by clicking the blue button and using the text tab.
Youre welcome to add a photo if you own the copyright to it  but its the text well be judging.
Terms and conditions
Closes Wednesday 18 October 2017, 10am GMT
