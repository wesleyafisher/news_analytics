__HTTP_STATUS_CODE
200
__TITLE

'Way ahead of the curve': UK hosts first summit on mindful politics

__AUTHORS

Robert Booth
__TIMESTAMP

Friday 13 October 2017 09.17EDT

__ARTICLE
British and Sri Lankan government ministers and an MP for Israels Likud party are among politicians from 15 countries due to meditate together at the House of Commons next week in an event to explore whether mindfulness can help reset the conduct of national and international politics.
The group will meet at parliament on Tuesday to be led in a series of secular meditations intended to focus their awareness and increase compassion. 
They will be joined remotely by a US presidential hopeful, Tim Ryan, who has been tipped as a possible Democratic nominee for 2020. The Ohio congressman has said meditation guides his response to Donald Trump, whose reactive tweeting may seem to many as being the opposite of mindful.
The event, claimed to be a world first, has been organised by senior Conservative and Labour MPs, who said they would discuss the potential of meditation to help political leaders stay resilient, clear-minded and creative in the face of constant change.
Since 2013, 145 UK parliamentarians have undertaken an eight-week course in the practice. The most senior British politician taking part in Tuesdays event will be the sports minister Tracey Crouch. 
The conference will be addressed by Jon Kabat-Zinn, a US scientist who in the 1970s pioneered the use of meditation for people with chronic illnesses, partly by playing down its Buddhist roots.
He is seen as the driving force behind the rising popularity of the practice in western countries, where it is being tested in schools and prisons as well as being used to treat mental health problems.
Sweden, which also has a mindfulness training programme for MPs, is sending three parliamentarians, and among others they will meditate with Sri Lankas education minister, Mohan Lal Grero, and Jlia brahm, a director of the Hungarian opposition LMP party. MPs from France, the Netherlands, Ireland and Italy will also attend.
During the sessions, the politicians will sit on chairs, close their eyes and be invited to focus their attention on their breath, their bodies and their thoughts to develop mindfulness. 
Ryan said: Mindful meditation allows me to take a timeout, step back and see issues as interconnected. That kind of big-picture problem-solving is desperately lacking in both US political parties today, and has in many cases been replaced with an almost hyper-partisan kind of hate. 
I try not to be a part of that. If you hate, youre just adding hate. Whether youre hating from the left or from the right, its still hate, and it is undermining our ability to come together as a nation to solve big problems.
Kabat-Zinn said the use of mindfulness in the political process could improve policymaking and that by training the mind to be more attentive, politicians may listen better to the needs of the country and the world.
The UK is way ahead of the curve, Kabat-Zinn said. I dont know of any other country that is doing this, and now you have inspired politicians in many different countries. That is what this event is about. They can look each other in the face and strategise and support each other. 
This is not a weirdo lunatic fringe trying to take over the world, but an oxygen line straight into the heart of what is deepest and most beautiful in us as human beings.
Since 2004, courses of mindfulness-based cognitive therapy have been recommended by the UKs National Institute for Health and Care Excellence for the treatment of recurrent depression. The therapy is available in some areas on the NHS, and hundreds more private courses have sprung up amid rising demand, although there are concerns about the standard of teaching in some cases. 
Mindfulness has helped me make better decisions, said the Labour MP Chris Ruane, who has practised meditation for a decade and is co-chair of the all-party parliamentary group on mindfulness. If you make decisions from a position of balance and equilibrium, it is far better, not just for personal but political decisions that affect a whole nation. 
In times when you have political leaders who may not be making political decisions from a position of balance, it doesnt do them, their country or the world much good.
The other co-chair, Tim Loughton, a Conservative former minister, said: There is an affinity amongst those who have been through this [mindfulness] course and a rather more considered approach to exchanges of differing views.
Part of the course offered to UK MPs examines the difference between reaction and response, or, as Ruane puts it, knowing when to hold your finger [on social media] or to hold your tongue.
If you are aware of what is happening in your mind and body you can see it is an immediate reaction and think: I have to slow down, contain it and give a more considered response.
Ruane said he wanted the next step to be politicians taking mindfulness to their constituents, in policy  health, educations, prisons, the workplace  because there is a massive need for it. 
