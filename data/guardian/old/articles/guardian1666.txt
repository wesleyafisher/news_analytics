__HTTP_STATUS_CODE
200
__TITLE

Sudoku 3882 medium

__AUTHORS

__TIMESTAMP

Tuesday 17 October 2017 19.01EDT

__ARTICLE
Fill the grid so that every row, every column and every 3x3 box contains the numbers 1 to 9.
For a helping hand call our solutions line on 0906 200 83 83. Calls cost 1.03 per minute from a BT landline. Calls from other networks may vary and mobiles will be considerably higher. Service supplied by ATS. Call 0330 333 6946 for customer service (charged at standard rate).
Buy the next issue of the Guardian or subscribe to our Digital Edition to see the completed puzzle.
