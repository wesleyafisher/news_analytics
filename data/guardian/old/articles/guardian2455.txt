__HTTP_STATUS_CODE
200
__TITLE

Readers travel photo competition: October  win a trip to Costa Rica

__AUTHORS

__TIMESTAMP

Monday 2 October 2017 13.02EDT

__ARTICLE
There is no theme this year, so the field is open. Whether its a stunning landscape, poignant portrait or a vibrant street scene, we are just looking for great travel shots.
Well choose and publish a selection of our favourites, and the winning image for the month will receive a 200 voucher that can be used against an Exodus Travels holiday.
All the monthly winners will be entered for the overall annual prize, a fantastic 16-night holiday for two to Costa Rica with Exodus Travels.
Please read this before you post your image:
 You must be a UK resident to enter Upload the highest possible resolution of your shot via GuardianWitness.  Well only consider one photograph from each person, so dont submit more than one. We cant consider photos that have been published elsewhere . Youll also need to provide a caption of up to 40 words on where it was taken, whats happening in the shot, what inspired you to take it, etc. You must also supply your full name with the caption.
 Dont forget to read the terms and conditions before you enter. Click here to see previous winners.
The closing date for entries is 23:00 on 25 October 2017
GuardianWitness is the home of user-generated content on the Guardian. Contribute your video, pictures and stories, and browse news, reviews and creations submitted by others. Posts will be reviewed prior to publication on GuardianWitness, and the best pieces will feature on the Guardian site.
