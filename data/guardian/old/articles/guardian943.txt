__HTTP_STATUS_CODE
200
__TITLE

Annotated solutions for Genius 170

__AUTHORS

__TIMESTAMP

Sunday 3 September 2017 19.01EDT

__ARTICLE
 * The middle letters of the central down solution, followed by the seven words running clockwise round the circumference of the grid from the top left-hand corner spell out Pucks words in A Midsummer-Nights Dream: Ill put a girdle round about the earth 
 1  aeon A/E/ON
 2 airs AIR/S(oprano)
 3 babushka B(ishop)/A/BUSH/KA(te)
 4 Barr BAR/R(un) [Epstein-Barr virus]
 5 Bissau chihuahUAS SIBkings (hidden rev)
 6 brio B(I/R)O
 7 emergent GENT(s) after E<ME>R
 8 enamelware MEL in AREA/NEW (anag)
 9 Esau E(astern)/(bis)SAU
 10 even-handed EVE/N(ame)/HANDED
 11 ghat G(ood)/HAT
 12 hoax HO(ur)/A(nswer)/X
 13 honorarium (f)OR 1 MAN-HOUR (anag)
 14 inched (p)INCHED
 15 invalidate IN + TV L(icenc)E AIDA (anag)
 16 keen KE<(needl)E>N [Barbies boyfriend]
 17 letter bomb LETTER + BO<M(ale)>B
 18 marmot TOM/RAM (rev)
 19 moot M(edical)O(fficer)/O(ld)T(estament)
 20 nutshell NUTS/HELL
 21 politician Polly/Titian (hom)
 22 Real warwickshiRE ALlegedly [Real Madrid]
 23 RICO eccentRIC Only (hidden) [1970 Racketeer Influenced and Corrupt Organizations Act]
 24 televisual LEVIS in A LUTE (anag)
 25 terminated (Hilary)TERM + END IT A (anag)
 26 tierce C(hurch of)E(ngland)/ after TIER
 27 trio T(ennis)/RIO
 28 unlit UN<L(eft)>IT
 29 uxorious (l)UXOR/(p)IOUS
 30 Villa double def + VILLA(in) [Aston Villa]
