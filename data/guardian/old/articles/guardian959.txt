__HTTP_STATUS_CODE
200
__TITLE

Trump's latest travel ban: what's new, who's covered, and why now?

__AUTHORS

Oliver Laughland in New York
__TIMESTAMP

Monday 25 September 2017 15.26EDT

__ARTICLE
On Sunday evening the Trump administration issued its third travel ban in less than a year, opening yet another chapter in the heated legal and civil rights battle that has dominated much of the presidents first nine months in office.
Trumps ban has gone through many iterations, from a chaotically implemented first attempt that was blocked by a series of federal courts, to a streamlined version that was refined even further by the supreme court and eventually allowed to come into effect in June.
With all the legal challenges, policy revisions, and Trumps own incendiary rhetoric on immigration, it has been hard to keep up with what has often felt like a set of ever-evolving restrictions. In the latest twist, the supreme court announced on Monday it had cancelled arguments on the ban set for 10 October, asking for updated briefings from the government and the bans challengers.
Here we answer some of the key questions about the new ban.
There are some key revisions to this ban that make it more expansive than its predecessors.
The restrictions now target more countries than before. Trumps last ban was aimed at travellers from six Muslim-majority countries and all refugees. The new ban now targets the issuing of visas for citizens of eight countries, five of which  Syria, Iran, Somalia, Yemen and Libya  were included in Trumps first two bans, and three of which  North Korea, Chad and Venezuela  were added in the latest ban. The order will also place Iraqi travellers under additional scrutiny, but does not ban entire visa classes as it does with the other eight nations.
Trumps first and second ban sought to freeze the issuing of visas from the targeted countries for 90 days to allow the Department of Homeland Security to assess worldwide screening and visa vetting procedures.
By contrast, the new restrictions announced on Sunday are essentially indefinite, although the administration has said it will review them if the targeted countries improve co-operation with the US government.
The president had to issue the new order by Sunday, as the 90-day freeze on visas, which had been heavily revised by the supreme court, was due to expire at midnight.
Trump received the DHS vetting report, which examined almost 200 nations, last week and chose to target the eight countries and Iraq because they remain deficient  with respect to their identity-management and information-sharing capabilities, protocols, and practices.
Sudan had been included in the previous two bans but was dropped this time by the administration without any specific explanation. The presidents proclamation says that the DHS vetting review led to some improvements and positive results in certain countries, but again provides no specific detail.
Advocates have suggested that the DHS decision to end temporary protected status for Sudan last week, which gave immigration status to Sudanese citizens in the US due to ongoing conflict in the region, may have had something to do with the decision. According to Becca Hella, director of the International Refugee Assistance Project, it suggests the government of Sudan was pressured into agreeing to accept massive numbers of deported Sudanese nationals from the US in exchange for being dropped from the travel ban.
The central African nation of Chad, which is 52% Muslim, has been added to the ban ostensibly because of a failure to adequately share public-safety and terrorism-related information. But a number of observers have found the Trump administrations move perplexing because of Chads close counter-terrorism partnership with the US.
John Campbell, a former US ambassador to Nigeria, said he thought the move was driven by a matter of incompetence over anything else.
Here you have a country that in terms of the most important political issue in Africa, terrorism, is on the right side. It is one of the poorest countries in the area. American airports are not overwhelmed by Chadians arriving. You put all this together and I fall back on incompetence, Campbell told NBC News.
The additions of North Korea and Venezuela add a further twist. Although the administration argues that both countries fail to share adequate information with the US, it is probably more accurate to interpret the restrictions on these countries as official sanctions rather than a travel ban.
The curbs placed on Venezuela only affect a small group of government officials and their families. And while the ban on North Korean travel is all-encompassing, with the exception of diplomatic entries, relations with the country have been frozen for some time already. The United States issued only nine immigrant visas to North Koreans last year, one in 2014 and seven in 2015. 
The inclusion of these countries, however, could help with part of the administrations legal battle over the bans.
The supreme court was due to hear arguments on Trumps second ban on 10 October, which is eight days before the new ban is due to go into effect. But on Monday it cancelled that hearing and asked for updated briefings from the government and the two groups of challengers.
The claim that Trumps previous two orders have been motived by religious animus, in this case discrimination against Muslims, has been central to the challenges to the ban throughout. But the administration will likely argue to the supreme court that the inclusion of Venezuela and North Korea, neither of which are Muslim majority nations, alleviates these concerns.
Rights groups involved in the challenge have already indicated they will argue the inclusion of these two countries is a smokescreen for discrimination, as the restrictions on both countries are limited.
Although the administration argued that it has tailored restrictions to each country, the ban is likely to affect most travellers from the targeted nations, with the exception of Venezuela. 
If it comes into full effect, it will be far broader than the revised ban allowed by the supreme court over summer. 
The supreme court had said that those in the targeted countries (six, at the time) who could prove a bona fide relationship with family members or US entities, such as a university or employer, should be allowed entry. This order carries no such exception, although it continues to honour existing visas and permanent residency. 
Trumps proclamation contains country-specific guidance and is viewable here. 
Sundays order does not include guidance for refugees. Thats because the administration technically has another 30 days to decide how to proceed with limiting refugee admissions. 
However, as the end of the fiscal calendar year approaches in America, the Trump administration will have to decide what cap to place on annual refugee admissions by the start of October. Reports have indicated that Trump is planning to bring the cap to a historic low. 
