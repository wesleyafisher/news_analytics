__HTTP_STATUS_CODE
200
__TITLE

Share your best money saving tips

__AUTHORS

Guardian readers
__TIMESTAMP

Thursday 31 August 2017 10.35EDT

__ARTICLE
From collecting coupons to joining cashback sites and switching energy providers, there are tactics available for those seeking to reduce weekly bills and improve their personal finances. If you have come up with any crafty tactics for saving money, and are the kind of person who gets a warm fuzzy feeling from sharing such tips with fellow citizens, youve landed on the right webpage.
Whatever stage of life youre at and however big or small your ideas, if you think a fellow reader could benefit from something you do, share your techniques with us below. Well put together some of the best suggestions in an article on the Guardians Money section.
Fill out the form below and explain your ideas in as much detail as you can. Wed like to know how much money you have saved, and hear the details of how you discovered or invented the technique. What kind of impact have your money saving tips had on your finances or those of your family?
