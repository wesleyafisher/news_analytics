__HTTP_STATUS_CODE
200
__TITLE

Is paying off the mortgage a better bet than saving?

__AUTHORS

Virginia Wallis
__TIMESTAMP

Thursday 13 July 2017 02.00EDT

__ARTICLE
Q We are a family with some savings  around 70,000  but are worried they might be worth less once Brexit is decided: ie, the pound will go downhill.
So if we want to put the savings into something, is it best to pay off a big lump sum on a mortgage of 135,000 with 12 years to run, or use the money to buy another property to let out? Or do both?  The mortgage on our home is a two-year interest-only deal that expires in June. We save 1,000 a month to pay off lump sums each time we switch fixed-rate deals. 
A Your savings are already worth less than they were before the Brexit vote, because the sharp fall in the value of the pound pushed inflation (as measured by the consumer prices index) to a four-year high of 2.9% in May 2017.
Unlike when the UK crashed out of the European exchange rate mechanism back in 1992, the Bank of England did not hike interest rates in an attempt to push the value of sterling back up, which would have been brilliant for savers. Instead it cut the base rate to 0.25%, which makes keeping cash in a savings account a distinctly unattractive proposition. 
So your instinct to use your money to pay off a large part of your mortgage is a good one, as the interest you save on your mortgage payments is likely to be a lot more than the interest you are earning on your cash, which at best could be in the region of 1%.
Next time you switch mortgage deals, as well as paying off part of your mortgage you might also want to consider changing from an interest-only deal to a repayment one, so it is guaranteed to be paid off in full in 12 years time. As you can currently afford to save 1,000 a month, you should be able to cope with the larger monthly mortgage payments of a repayment mortgage because they include repayment of capital as well as interest.
As to whether you should invest in property to let out, there is no clear cut answer. To compare the return on the investment with other forms of saving you need to calculate the rental yield.
To do this you need to know how much rent you could get each year and then subtract costs such as mortgage repayments (youre unlikely to be able to buy a property outright), maintenance costs and possible agents fees. Divide this figure by your buying costs  including your cash deposit, legal fees and the higher rate of stamp duty land tax that you pay on buy-to-let properties  and multiply the result by 100 to get the percentage yield.
But even if this looks good on paper, you should ask yourself if you are genuinely up for being a landlord with all the extra work and responsibility it entails.
