__HTTP_STATUS_CODE
200
__TITLE

David Attenborough urges action on plastics after filming Blue Planet II

__AUTHORS

Graham Ruddick
__TIMESTAMP

Sunday 15 October 2017 04.22EDT

__ARTICLE
Sir David Attenborough has called for the world to cut back on its use of plastic in order to protect oceans. His new BBC TV series, Blue Planet II, is to demonstrate the damage the material is causing to marine life.
Speaking at the launch of Blue Planet II, which will be broadcast 16 years after the original series, the broadcaster and naturalist said action on plastics should be taken immediately and that humanity held the future of the planet in the palm of its hands.
His comments come amid growing global calls for cutbacks in the use of plastic. Last week, the former boss of Asda, Andy Clarke, said supermarkets should stop using plastic packaging.
A Guardian investigation established that consumers around the world buy a million plastic bottles a minute. Plastic production is set to double in the next 20 years and quadruple by 2050. Around the world, more than 8m tonnes of plastic leaks into the oceans, and a recent study found that billions of people globally are drinking water contaminated by plastic.
Blue Planet II will include evidence that plastic has flowed into ocean waters thousands of miles from land, and will show albatrosses unwittingly feeding their chicks plastic.
The new series of Blue Planet has seven episodes and is expected to be a global hit for the BBC. The programme has already been sold to more than 30 countries and the first episode will air on BBC One on Sunday 29 October.
Attenborough said rising global temperatures and plastic were the biggest concerns for the ocean. What were going to do about 1.5 degrees rise in the temperature of the ocean over the next 10 years, I dont know, but we could actually do something about plastic right now, he said.
I just wish we would. There are so many sequences that every single one of us have been involved in  even in the most peripheral way  where we have seen tragedies happen because of the plastic in the ocean.
Weve seen albatrosses come back with their belly full of food for their young and nothing in it. The albatross parent has been away for three weeks gathering stuff for her young and what comes out? What does she give her chick? You think its going to be squid, but its plastic. The chick is going to starve and die.
There are more examples of that. But we could do things about plastic internationally tomorrow.
Attenborough, 91, did not specify what could be done, but cutting back on plastic packaging and plastic bags in supermarkets would be a major step.
He said everyones actions had an impact on the ocean. We have a responsibility, every one of us, he said. We may think we live a long way from the oceans, but we dont. What we actually do here, and in the middle of Asia and wherever, has a direct effect on the oceans  and what the oceans do then reflects back on us.


 
