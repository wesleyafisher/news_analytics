__HTTP_STATUS_CODE
200
__TITLE

Bomb and gun attack on Mogadishu hotel kills dozens

__AUTHORS

Jason Burke Africa correspondent
__TIMESTAMP

Wednesday 25 January 2017 06.56EST

__ARTICLE
Islamic militants from al-Shabaab have killed at least 28 people in an attack on a hotel in Somalias capital, Mogadishu.
Police said the attackers rammed a car bomb into the gates of the Dayah hotel shortly after 8am on Wednesday before storming inside.
A series of attacks have underlined the resilience of al-Shabaab, despite a decade-long effort to eradicate the group in the east African state. It has repeatedly attacked high-profile targets in Mogadishu and elsewhere in an attempt to derail ongoing presidential polls.
Dr Abukadir Abdirahman Adem, head of the citys ambulance service, told AFP that at least 28 people had been killed and 43 injured. 
Somalias Andalus radio, which is linked to al-Shabaab, reported that well-armed mujahideen [fighters] attacked the hotel, and now they are fighting inside the hotel.
But security experts in Mogadishu said that police, security guards and soldiers had prevented the militants from penetrating the hotel building itself, though a fierce firefight took place outside.
Al-Shabaab is affiliated to al-Qaida. Though it has suffered significant casualties and has lost several key leaders, particularly in strikes from US drones, the movement has notched up some significant victories.
A base manned by Kenyan soldiers who were part of Amisom, the troubled stabilisation effort mounted in Somalia by regional powers, was overrun by al-Shabaab attackers last year and up to 180 soldiers were reportedly killed.
An attempt by the militant group to bring down a passenger jet on takeoff from Mogadishu airport failed last year.
Though al-Shabaab has a limited presence in urban centres, it retains significant control over some rural areas.
Last year leaders defeated a faction within the movement that wanted to break its affiliation with al-Qaida in favour of Islamic State.
The US president, Donald Trump, and his team have yet to signal any firm direction on policy in Africa, but analysts believe they will prioritise security issues, particularly the fight against al-Shabaab, al-Qaida and Isis affiliates such as Boko Haram in Nigeria. In recent years the US has significantly increased its military and counter-terrorism presence on the continent. 
The elections for a new president and national assembly in Somalia were due to have been completed last year. The poll has been billed by international backers, including the US and the UK, as the countrys first truly democratic election for decades. But the process has been tarnished by delays, violence and allegations of endemic corruption.
The continuing violence and instability has impacted food supplies and aid agencies have warned Somalia risks slipping back into famine, as worsening drought has left millions without food, water or healthcare.
Five million people, almost half the population, do not have enough to eat, the UN said this month. Famine last struck pockets of Somalia in 2011, killing 260,000 people, caused by drought, conflict and a ban on food aid in territory held by al-Shabaab.
