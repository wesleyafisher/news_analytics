__HTTP_STATUS_CODE
200
__TITLE

Students cheat in ever more creative ways: how can academics stop them?

__AUTHORS
Anonymous academic
__TIMESTAMP

Thursday 12 October 2017 11.12EDT

__ARTICLE
I volunteer to sit as a lecturer on our academic misconduct board several times a semester, joining a small panel that decides whether or not students flagged up by their lecturers for cheating have broken the rules.
We get a stack of roughly 10 cases, and for two or three hours we pore over them, not only deciding if students are guilty as charged but also what the punishment should be, according to our university guidelines.
OK, I admit it: its intriguing work. Ways in which students cheat are either ingenious or surprisingly obvious. Among the day-to-day banality of preparing lectures, marking assessments and dealing with the bureaucracy of university life, sitting on the board is often a welcome escape.
Students have been known to hide earphones in headscarves, buy essays online or articles from content writers, and steal other students papers. One grabbed another students USB stick when he went to the toilet, downloaded a project and sent it to himself. Another submitted the exact paper his sister had submitted for the same module a year earlier.
Dont be shocked at how gormless students can be (theyd have to be, or they wouldnt cheat, right?). One left the sales receipt from the essay mill in his book. Another advertised online  using her photograph  for someone to do her work for her. A third denied that the text he had so meticulously copied was plagiarism  until he was shown the original, in a book written by the tutor. Another sent an army of male students pretending to be him to sit his exams, all equipped with fake IDs.
When it comes to pure plagiarism, youd think that using our online plagiarism checker, Turnitin, would be a deterrent, but evidently it isnt. Sometimes those crafty kids just change the nouns using an online thesaurus, as if that would make their work plagiarism-free. But the nouns they substitute often make their writing look weird. Is that alarm bells ringing?
Sometimes their English is poor  at least in the first three paragraphs  and then miraculously becomes perfect. Being too lazy to change the typeface when their work reverts to someone elses is another giveaway, as is forgetting to change the spelling from American to British English. 
Extreme cases include that of one student where 84% of her work came from Wikipedia, complete with links and superscripts. The other 16% was her own work, and was entirely incomprehensible.
Its not hard catching someone in flagrante, as Turnitin will flag up in bright pink anything either turned in before or published elsewhere. What is hard is catching someone who has paid to have something tailor-written for them, although often its of such a higher standard that those bells start ringing again.
The government has tried unsuccessfully to crack down on essay mills, where desperate  or lazy students pay up to 3,000 for a BA dissertation (and about 150 for a run-of-the-mill essay). But since a proposed amendment that would have made it illegal to sell essays to the higher education and research bill didnt pass earlier this year, the onus is now on universities to solve the problem. Weve been asked by the Quality Assurance Agency to block the websites, provide more support to students and implement widespread use of plagiarism software. This is difficult because contract cheating allegations are still pretty rare - but thats because theyre difficult to evidence.
 I once spent seven hours finding definitive proof that a student had purchased her assessment, which eventually resulted in her expulsion from university. Sadly, some lecturers dont go the full mile, as catching a cheater and filling out a report can be very labour-intensive.
Banning essay mills would be great, but its a free market and a free country, so you cant do it. Even if you did, they would just start up in Russia or somewhere, one of my colleagues told me. Its about making people understand that whatever stress theyre under, its not OK to pay people to do their work. Its a moral education thing. Paying means they are not getting the education process. There is no point cheating, as the rules are the game. Youre not learning.
So why do students cheat, and risk having to retake a module, having their degree classification lowered, or even being kicked out of university? There are many reasons  including financial pressure, poor organisational skills and panic  sometimes among young people who should never have gone to university in the first place or, at the very least, who should have had more support structures in place when they started.
The fact that students feel they need to get a 2:1 or above to succeed pressurises them into cheating to achieve it, a member of our university registry management team told me, estimating that the number of cheaters is rising. I also think some universities are taking students who are not capable of achieving that outcome, due to pressures on universities to fill places.
The much-decried university mental health crisis is also a contributing factor. Students are facing undue pressure to succeed  not just financially - and many are ill-equipped to make the transition from home or work. The UPP Annual Student Experience Survey said that 48% of men 67% of women find the stress of studying difficult to cope with at university.
Now that they are paying 9,250, some students feel they are entitled to a degree without doing the work, my colleague added. That money just entitles them to begin the learning process.
Students can provide mitigation to the board, and often its a heart-breaking litany: ill children, mental health issues, alcoholic parents. But I tell my first-year students that while I sympathise, there is no excuse for cheating of any kind, and if they cannot meet a deadline to tell me so, we can extend it.
What else do I tell them? Dont cheat, because if you do, I will catch you. And I do.
Join the higher education network for more comment, analysis and job opportunities, direct to your inbox. Follow us on Twitter @gdnhighered. And if you have an idea for a story, please read our guidelines and email your pitch to us at highereducationnetwork@theguardian.com.
Looking for a higher education job? Or perhaps you need to recruit university staff? Take a look at Guardian Jobs, the higher education specialist
