__HTTP_STATUS_CODE
200
__TITLE

Genius crossword No 166

__AUTHORS

Picaroon
__TIMESTAMP

Sunday 2 April 2017 19.01EDT

__ARTICLE
 Two across solutions taken together indicate something missing from all the other 14 across clues, either as the definition or as part of their wordplay.
 Deadline for entries is 23:59 GMT on Saturday 29 April. You need to register once and then sign in to theguardian.com to enter our online competition for a 100 monthly prize.
 Click here to register
