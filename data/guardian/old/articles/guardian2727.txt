__HTTP_STATUS_CODE
200
__TITLE

Top UN official to leave Myanmar amid criticism of Rohingya approach

__AUTHORS

Oliver Holmes South-east Asia correspondent
__TIMESTAMP

Thursday 12 October 2017 12.10EDT

__ARTICLE
The United Nations most senior official in Myanmar is to leave the country at the end of the month, her office has said, amid allegations the world body failed to promote the rights of persecuted Rohingya people.
Renata Lok-Dessallien, the UN resident coordinator since January 2014, would take on another assignment at headquarters, the statement said without elaborating.
Having spent close to three decades at the UN, the Canadian has faced charges that she prioritised good relations with the Myanmar government, to pursue development projects, over humanitarian access and human rights advocacy, especially for Rohingya, a contentious subject for the authorities.
Although her departure was announced earlier this year, UN sources said Lok-Dessallien had been kept on because the Myanmar government rejected her replacement. Many expected her to remain until someone was approved, although her office did not say if a replacement had been confirmed.
The announcement comes in the wake of increased scrutiny of UN work in Myanmar, following the worst outbreak of violence in decades in western Rakhine state where the Rohingya live. 
Responding to attacks by Muslim rebels, the military has conducted a severe crackdown that has forced more than half a million Rohingya to flee to neighbouring Bangladesh. More than 200 villages have been burned and refugees allege the army has been responsible for mass killings and rapes, claims the government denies.
A BBC report cited aid workers who alleged that in the run-up to the recent bloodshed, Lok-Dessallien had tried to stop human rights activists travelling to Rohingya areas and attempted to shut down public advocacy on the subject.
Sources in the UN and aid community have also told the Guardian that Lok-Dessallien suppressed an independent report she commissioned. The final document was highly critical of the UNs strategy in Myanmar and said it needed to undertake serious contingency planning for a potential Rohingya crisis.
The reports author said Lok-Dessallien was put in an impossible position as senior figures at UN headquarters in New York did not have a coherent or well-coordinated approach to Myanmar. 
Meanwhile, the UNs strategy had not worked, aid workers said. After the attacks, Aung San Suu Kyis government blocked off vital parts of the region to aid agencies, meaning thousands were not receiving adequate food or medicine. And despite UN efforts to work with authorities, the government has lashed out at humanitarians, in one instance accusing aid workers of helping terrorists. 
The world body has denied that Lok-Dessallien suppressed the report, and has repeatedly supported her, saying the senior official has been a tireless advocate for human rights.
The statement announcing her departure said Lok-Dessallien will use her remaining time until the end of the month to further the UN systems efforts to promote peace and security, human rights, as well as humanitarian and development assistance for all people in Myanmar.
It added that Lok-Dessallien appreciates the secretary generals confidence in the UN team in Myanmar.
The UN under-secretary general for political affairs, Jeffrey Feltman, is due to visit Myanmar on Friday to push for an end to military operations in Rakhine and to open access for humanitarian support. 
His discussions will also focus on building a constructive partnership between Myanmar and the United Nations to tackle the underlying issues impacting all communities in the affected areas, a UN statement said.
The visit may include delicate discussions on Lok-Dessalliens replacement, which needs approval from Myanmar, UN sources said.
