__HTTP_STATUS_CODE
200
__TITLE

Danny McGuire proud of fairytale finish before Leeds departure

__AUTHORS

Aaron Bower at Old Trafford
__TIMESTAMP

Saturday 7 October 2017 17.28EDT

__ARTICLE
Danny McGuire reiterated the time was right to leave Leeds after finishing his near two-decade association with his hometown club with another Grand Final success and the Harry Sunderland Trophy.
For only the second time in Grand Final history, a single player pooled each and every one of the votes for the man-of-the-match award, following in the footsteps of his Leeds team-mate Rob Burrow, who achieved that feat in 2011.
McGuire was sensational as Leeds won the Grand Final for the eighth time in only 14 seasons, comfortably beating Castleford 24-6 in his 424th and final game for the Rhinos. He will now embark on a new challenge with Hull Kingston Rovers in 2018, and the half-back was delighted to end his time with Leeds with the title.
Its a fairytale, he said. I went to bed on Friday night and dreamed of the fairytale finish. A new challenge will be good for me but I was determined my Leeds career would finish on a positive note, and it couldnt have gone any better. I said it was the right thing for the club and myself and I stick by it.
McGuires final game for Leeds evoked memories of 2015, when the clubs long-serving trio of Kevin Sinfield, Jamie Peacock and Kylie Leuluai also left the club with a Grand Final win. Their coach, Brian McDermott, admitted the challenge was not to replace McGuire in 2018 but to evolve without him. Its the same as when Kevin and JP retired, you dont replace them, you adapt, he said.
For Castleford, the post-match discussion surrounding their first Grand Final appearance revolved around Zak Hardaker, the England international who was dropped 48 hours before kick-off for a breach of club rules. Their coach, Daryl Powell, admitted the details of the players indiscretions  which are understood to have ruled out his chances of a place in Englands World Cup squad  will be made public shortly.
It was a big disruption, he said. Its nothing to do with me but youll all be made aware of it pretty quickly, I think.
Powell was left to rue a slew of handling errors by the Tigers. We saved our very worst till last, Powell said. We just created too many errors and credit to Leeds, they deserved to win. We just made unforced errors, mistakes and dropping the ball is so hugely disappointing. You have to wait a long time before we can straighten it up but it is what it is.
Bradford Bulls became the first winners of the Womens Super League Grand Final after they beat their West Yorkshire rivals Featherstone 36-6. Played at the Manchester Regional Arena as a curtain-raiser for the mens Grand Final at Old Trafford, the Bulls were far too good for Featherstone as they ended their domestic season unbeaten, adding the league to their Challenge Cup success. Led by the outstanding Lois Forsell, tries from Kirsty Moroney and Forsell herself put Bradford ahead and, while Georgia Roche replied for Featherstone, that was as good as it got for them. Charlotte Booth scored to open up a commanding lead before half-time and second-half tries from Claire Garner, who also kicked four goals, Amy Hardcastle, Charlene Henegan and Haylie Hields rounded off the victory.
