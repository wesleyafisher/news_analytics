__HTTP_STATUS_CODE
200
__TITLE

Dolphins coach out after video appears to show him snorting white powder

__AUTHORS
Associated Press
__TIMESTAMP

Monday 9 October 2017 14.00EDT

__ARTICLE
Miami Dolphins offensive line coach Chris Foerster resigned Monday and said he was seeking medical help after a social media video surfaced appearing to show him snorting three lines of a white powdery substance at a desk.
Hours after the 56-second video was posted on Facebook and Twitter, Foerster announced his resignation in a statement released by the team.
I am resigning from my position with the Miami Dolphins and accept full responsibility for my actions, he said. I want to apologize to the organization, and my sole focus is on getting the help that I need, with the support of my family and medical professionals.
This is the video supposedly of Dolphins offensive line coach Chris Foerster snorting coke before a meeting pic.twitter.com/L7ZsdZMH7U
Its unclear when or where the video was made or how it became public.
Foerster, 55, has been an NFL assistant since 1992 and joined head coach Adam Gases staff in Miami last year.
The video appears to show Foerster and the powdery substance on the desk. He is holding a rolled-up $20 bill in his right hand, and appears to adjust the camera with his left hand.
Hey babe, miss you, thinking about you, he says. How about me going to a meeting and doing this before I go?
He then snorts the substance into his nose though the $20 bill and notes those big grains falling as residue lands on the desk.
What do you think, Im crazy? Foerster asks after snorting the second line. Ah, no, babe.
Its going to be a while before we can do this again  he says, But I think about you when I do it. I think about how much I miss you, how high we got together, how much fun it was. So much fun.
Before snorting the final line he says, Last little bit before I go to my meeting.
The video surfaced hours after the Dolphins (2-2) beat Tennessee 16-10 on Sunday. Foersters line has played poorly this season, and the Dolphins rank last in the league in points and yards per game.
