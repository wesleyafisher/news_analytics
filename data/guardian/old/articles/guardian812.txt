__HTTP_STATUS_CODE
200
__TITLE

Cocktail of the week: Spurious Barrel  recipe

__AUTHORS
Adam Wyatt-Jones
__TIMESTAMP

Friday 22 September 2017 12.00EDT

__ARTICLE
Inspired by John Glasers Compass Box in west London, which makes some of the most progressive blended whisky around, we tried to replicate his genius while creating something original and cool. Serves one.
40ml whisky (you need a good one for this  we use Chivas Regal 12)15ml fig liqueur (we use Briottet)15ml amontillado sherry (an aged one)1 dash bourbon, for rinsing the glass
Pour the whisky, liqueur and sherry into a large glass, add ice and stir to lower the temperature (dont overstir, or it will dilute). Swirl the bourbon in a(posh, cut-glass) brandy glass, add the stirred spirits and serve.
 Adam Wyatt-Jones, Craft London
