__HTTP_STATUS_CODE
200
__TITLE

Gregg Popovich calls Donald Trump a 'soulless coward' after Obama comments

__AUTHORS

Guardian sport
__TIMESTAMP

Tuesday 17 October 2017 07.53EDT

__ARTICLE
On the opening day of the NBA season the San Antonio Spurs coach, Gregg Popovich, has launched his latest broadside at Donald Trump.
Popovich, who has won five NBA championships with the Spurs, was incensed after the US president falsely claimed Barack Obama and other presidents didnt contact the families of soldiers killed in action. Popovich, is an air force veteran and considered a career with the CIA before committing to basketball.
Trumps comments came after the deaths of US servicemen in Niger, and Popovich was angry enough to contact the Nations Dave Zirin to go on the record about the subject. Zirin is known for his coverage of sports and social issues.
Ive been amazed and disappointed by so much of what this president had said, and his approach to running this country, which seems to be one of just a never-ending divisiveness, Popovich told Zirin. But his comments today about those who have lost loved ones in times of war and his lies that previous presidents Obama and Bush never contacted their families are so beyond the pale, I almost dont have the words.
Popovich also spoke of his contempt for Trumps inner circle. This man in the Oval Office is a soulless coward who thinks that he can only become large by belittling others. This has of course been a common practice of his, but to do it in this manner and to lie about how previous presidents responded to the deaths of soldiers  is as low as it gets, Popovich added. 
We have a pathological liar in the White House, unfit intellectually, emotionally, and psychologically to hold this office, and the whole world knows it, especially those around him every day. The people who work with this president should be ashamed, because they know better than anyone just how unfit he is, and yet they choose to do nothing about it.
Popovich has been a constant critic of the US president. In May he was asked if he was ever distracted by events outside sports. While he did not mention Trump by name, it was clear to whom he was referring.
Gonna guess that Pop is talking about Donald Trump here. pic.twitter.com/BP1ml37nP6
Its interesting you would ask that, Popovich said at the time. Usually things happen in the world, and you go to work, you know, and youve got your family and youve got your friends and you do what you do, but to this day I feel like theres a cloud, a pall over the whole country  in a paranoid, surreal sort of way.
Its got nothing to do with the Democrats losing the election. Its got to do with the way one individual conducts himself. And thats embarrassing, its dangerous to our institutions and what we all stand for, what we expect the country to be. But for this individual, hes in a gameshow. And everything that happens begins and ends with him  not our people or our country. Every time he talks about those things, thats just a ruse. Thats just disingenuous, cynical and fake.
Other NBA players and coaches have been critical of Trump. LeBron James has called the president a bum, while the coach of the NBA champion Golden State Warriors, Steve Kerr, said Trumps election was a blow for respect and dignity.
