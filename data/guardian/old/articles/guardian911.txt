__HTTP_STATUS_CODE
200
__TITLE

Crown casino accused of tampering with pokies as Andrew Wilkie drops bombshell

__AUTHORS

Anne Davies
__TIMESTAMP

Tuesday 17 October 2017 21.31EDT

__ARTICLE
Crown casino deliberately tampered with poker machines, allowed cannabis to be smoked in gaming rooms and avoided money laundering rules, whistleblowers have alleged in a video statement tabled in federal parliament.
The video evidence was collected by independent MP Andrew Wilkie and Senator Nick Xenophon as part of their PokieLeaks campaign to expose illicit practices in the pokies industry. The explosive allegations of misconduct by whistleblower staff could raise questions of Crowns fitness to hold a casino licence.
The allegations of tampering were denied by Crown Resorts Ltd in a statement to the ASX on Wednesday. The company called on Wilkie to hand all material he had to the authorities.
Three staff appear on the video, which has been heavily altered to disguise their identity. One describes himself as a technician and another as a gaming attendant.
They make a number of allegations including that staff were told to use other peoples IDs to avoid reporting amounts over $10,000.
 Any transactions over $10,000 are required to be declared to Austrac, which monitors cash transactions to detect money laundering and illicit activities.
Staff also alleged that marijuana was smoked daily in the Teak Room, the lowest of the VIP gaming rooms, and that they had been told to turn a blind eye to domestic violence by international gamblers so they could continue gambling.
One staff member also alleged that Crown management had them shave down buttons on gaming machines to allow illegal continuous play on the machines.
Under Victorias rules governing poker machines, the practice of allowing a machine to spin without a user pressing a button for each spin is banned.
But staff alleged they were instructed to use tools to shave down buttons on new machines to create space for punters to wedge something in the button, so it could be held in place and keep the machine playing without buttons being pressed.
The best tool was a Leatherman. I used a file, one of the whistleblowers said on the video.
Wilkie said in a statement to media: Today very serious allegations have been levelled at the poker machine industry.
Although the allegations focus on Crown in Melbourne, they could also suggest a broader pattern of behaviour in the poker machine industry, which would obviously have grave implications for people right around Australia. 
If members and senators, law enforcement and regulatory agencies, and the media, scrutinise the video record of the whistleblowers testimony, Im sure theyll agree that the claims warrant an immediate and strong response. 
Wilkie tabled the allegations, including a 30-minute recorded interview with the three whistleblowers, in what is believed to be the first case of video evidence being tabled in federal parliament.
The Australian Greens, senator Jacqui Lambie, and the Nick Xenophon Team are now pushing for a Senate inquiry into the regulation of Australias casino industry.
The Greens leader, Richard Di Natale, said Crown should stop operating its poker machines until a full and independent audit had occurred. 
Victorian Greens MP Colleen Hartland has asked the Victorian ombudsman to investigate the Victorian Commission for Gambling and Liquor Regulation, which has responsibility for regulating poker machines.
Jennifer Kanis, a Maurice Blackburn lawyer, who is heading a pro bono case against Crown casino and Aristocrat Technologies Australia seeking to draw attention to misleading and deceptive conduct by poker machine manufacturers and venues, said the expose had brought to light significant new material about alleged misconduct within the industry.
The Victorian government began its five-yearly review of Crown casinos licence in August.
 Crown said it rejected the allegations by Wilkie concerning the improper manipulation of poker machines and other illegal or improper conduct at Crown casino in Melbourne. 
Crown calls on Mr Wilkie to immediately provide to the relevant authorities all information relating to the matters alleged, the brief statement to the ASX said.
