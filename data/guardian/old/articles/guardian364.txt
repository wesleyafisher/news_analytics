__HTTP_STATUS_CODE
200
__TITLE

Cowboys owner Jerry Jones trying to 'help' players despite anthem protest ban

__AUTHORS

Guardian sport
__TIMESTAMP

Thursday 12 October 2017 15.18EDT

__ARTICLE
The Dallas Cowboys owner, Jerry Jones, wants to help his players bring attention to racial injustice despite telling them they would be benched if they knelt during the national anthem. 
NFL players have been kneeling during the anthem this season to highlight racial injustice this season but Jones reiterated in a team meeting on Wednesday that he would no longer tolerate the practice. On Sunday he said that any player who disrespects the flag or does not stand for the anthem will not play in the game. He added that players were free to express themselves before the anthem. Jones himself had knelt before the anthem with his players earlier this season. That decision came after Donald Trump had called players who protested sons of bitches.
On Thursday, the Cowboys head coach, Jason Garrett, said that Jones cared deeply for his players, and Wednesdays meeting was to show the owners love, admiration and respect for the players. According to ESPN, Jones told the players how protests could damage television ratings and sponsorship for a team estimated to be worth $5bn, factors that could end up affecting the players themselves. Many Americans see kneeling during the anthem as disrespectful to the country and military veterans. Hes very sensitive to some of the issues, as we all are, that the players are talking about, Garrett said. We all want to make an impact, and hes someone that can help the players do that. He wanted to make sure they knew that.
Some Cowboys players were reported to be unhappy with Joness stance over the anthem, and an ESPN source said Jones had claimed to have taken a bad guy role to deflect negative attention from his players. 
The Cowboys have a bye week and will not play until next Sunday. Garrett said that break will allow them to discuss how to approach social issues going forward. I think like a lot of things in life, a lot of people say, just focus on football, Garrett said. But as we all know in our lives, theres a lot of other things going on in our lives. So, sometimes as a player, a coach, as a football team, you have to focus on this particular thing to address it, to solve it, to move on, so you can get back to the business of football. And that happens with our players and our coaches each and every day  situations they have with their families, whether theyre health concerns or some of the off-field issues that we all deal with in our lives.
Sometimes you have to focus on those things so you can get back to the business of focusing on football. I think our teams done a really good job on that.
