__HTTP_STATUS_CODE
200
__TITLE

Kakuro 1573 medium

__AUTHORS

__TIMESTAMP

Thursday 12 October 2017 19.01EDT

__ARTICLE
Fill the grid so that each run of squares adds up to the total in the box above or to the left. Use only numbers 1-9, and never use a number more than once per run (a number may reoccur in the same row, in a separate run).
Buy the Guardian next Friday or subscribe to our Digital Edition to see the completed puzzle.
