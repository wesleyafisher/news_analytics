__HTTP_STATUS_CODE
200
__TITLE

Trump pick for drug czar Tom Marino pulls out after report on opioid bill role

__AUTHORS

Ben Jacobs in Washington and 
Martin Pengelly in New York
__TIMESTAMP

Tuesday 17 October 2017 19.15EDT

__ARTICLE
Tom Marino, the Pennsylvania congressman who Donald Trump nominated to be his drug czar, has withdrawn from consideration, the president announced on Tuesday.
Rep Tom Marino has informed me that he is withdrawing his name from consideration as drug czar, the president tweeted. Tom is a fine man and a great congressman!
Marino is a four-term representative who in February 2016 became the fifth member of Congress to endorse Trumps campaign for the White House. From 2002 to 2007 he was US attorney for the middle district of Pennsylvania, under George W Bush.
Marino was nominated to lead the National Office of Drug Control Policy, a key role in efforts to tackle the epidemic in opioid addiction and abuse that Trump on Monday called a massive problem, saying that he would make a major announcement on the subject next week.
On Sunday, Marino was the subject of a joint report by the Washington Post and 60 Minutes about his role as the sponsor of a bill that critics say undermined federal enforcement efforts against the opioid epidemic.
The bill made it far more difficult for the Drug Enforcement Administration (DEA) to crack down on drug companies that made suspicious shipments of opioids.
On Tuesday afternoon, Marino said in a statement that he had decided to remove the distraction my nomination has created to the utterly vital mission of this premier agency.
At the same time, he added, given my lifelong devotion to law enforcement, I insist on correcting the record regarding the false accusations and unfair reporting to which I have been subjected.
Citing his legislative work as evidence of his commitment to fighting drug abuse, he blamed conspiracy theories from individuals seeking to avert blame from their own failures to address the opioid crisis that proliferated during their tenure, specifically false allegations made by a former DEA employee.
Trump said in an interview with Fox News Radio on Tuesday, Tom Marino said, Look, Ill take a pass. I have no choice. I really will take a pass, I want to do it.
And he was very gracious, I have to say that. He didnt want to have  he didnt want to have even the perception of a conflict of interest with drug companies or, frankly, insurance companies.
On Monday, at a Rose Garden press conference with Senate majority leader Mitch McConnell, Trump called Marino a good guy but said: Were going to look into the report. Were going to take it very seriously.
The president then said he planned to speak to Marino and if I think its 1% negative to doing what we want to do, I will make a change.
Among Democrats, Senator Joe Manchin of West Virginia, a conservative representing a state ravaged by opioid addiction, called on Trump to withdraw Marinos nomination.
In a letter to the president, Manchin said the opioid crisis was the biggest public health crisis since HIV/Aids. The leader of the Office of National Drug Control Policy, he wrote, should protect our people, not the pharmaceutical industry.
Congressman Marino no longer has my trust or that of the public that he will aggressively pursue the fight against opioid abuse, Manchin wrote.
Senate minority leader Chuck Schumer said on Monday confirming Marino would be like putting the wolf in charge of the henhouse. On Tuesday morning, the New York Democrat welcomed Marinos decision to withdraw.
[Representative] Marinos decision to withdraw from consideration as drug czar is the right decision, Schumer said, though the fact that he was nominated in the first place is further evidence that when it comes to the opioid crisis, the Trump administration talks the talk but refuses to walk the walk.
