__HTTP_STATUS_CODE
200
__TITLE

Flying Lotus apologises after defending the Gaslamp Killer over rape allegations

__AUTHORS

Ben Beaumont-Thomas
__TIMESTAMP

Tuesday 17 October 2017 05.28EDT

__ARTICLE
Grammy-nominated electronic music producer Flying Lotus has apologised after he made comments supporting fellow producer the Gaslamp Killer, who has been accused of rape.
The Gaslamp Killer has been accused of drugging and raping a woman and her friend in 2013  one posted an account of the alleged attack on Twitter. He has since issued a statement denying the allegations, saying: I would never hurt or endanger a woman. I would never drug a woman, and I would never put anyone in a situation where they were not in control, or take anything that they werent offering.
Flying Lotus, whose label the Gaslamp Killer is signed to, supported him by playing one of his tracks during a live set, and told the audience: The internet is a fucking liar. Aint nobody judge and jury but the fucking law, OK? ... Let truth and justice have its day.
After widespread condemnation online, he has now apologised for the comments, telling HipHopDX: I wanted to sincerely apologise for my comments at my show. I realise they were insensitive. This is a tough time for all of us, men and women. Im having trouble finding my voice in all of this. I am truly heartbroken. My stage has always been a place for whats in my heart until now. I feel as internet-wielding people we have to learn to give each other space to feel, to honour each others reactions and experiences without bullying. I care about this community and its impact so much.
The accusations against the Gaslamp Killer come amid a wave of condemnation against sexual harassment, triggered in part by the numerous allegations against Hollywood producer Harvey Weinstein. Bjrk is another high profile case, seeming to accuse Danish director Lars Von Trier of sexual harassment on the set of Dancer in the Dark; Von Trier has denied the accusations. 
