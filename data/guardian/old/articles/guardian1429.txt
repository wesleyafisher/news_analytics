__HTTP_STATUS_CODE
200
__TITLE

West Virginians struggle for answers in America's worst hit opioid epidemic state

__AUTHORS

Chris McGreal in Charleston, West Virginia
__TIMESTAMP

Monday 28 August 2017 03.00EDT

__ARTICLE
In the end, there were just too many names. The toll of the dead was to have been read on the steps of West Virginias capitol building, in the state worst hit by Americas opioid epidemic. Misty Hopkins, Ryan Brown and Jessie Grubb were among them.
But by the time overdose awareness day was held on Saturday, more than 1,000 names of those killed by prescription painkillers, heroin or artificial opioids had been submitted.
There was no way we could read a thousand names, said organiser Cece Brown, whose son Ryan died of a heroin overdose. So the names were painted on to purple remembrance canvasses. On the steps of the capitol, families and friends lined up the shoes of the lost. 
Kaylen Barker put down three pairs, for her brother, her sister and a friend of her wife. Barker, 29, counted about 15 opioid deaths in her high school graduation year.
Theres a lot of people who I graduated with that are addicted, she said. High school cheerleaders that have been picked up for prostitution to feed their drug habits. Youll be hard pressed to find a family in this state that hasnt been directly impacted by addiction.
Barker said she was scared off hard drugs by other deaths. In high school there was a quadruple homicide in Huntington on prom night, she said. They suspected it was a drug-related issue. I knew three of the four people that died. I went to high school with one of them. So that kind of took me in a different direction.
Stephanie Hopkins had two photographs of her younger sister, Misty, pinned to the front of her T-shirt above the date of her death: 31 May 2015. Misty fell into addiction after a car crash at the age of 16. She was prescribed the powerful painkiller OxyContin, which kickstarted the epidemic that is now claiming around 50,000 lives a year. Misty became a nurse but the pills had unleashed a force she could not shake.
That drug took over her life, said Hopkins. She couldnt do anything without it. She met this guy and he introduced her to the needle. She was on heroin about five years. She lost her job.
If we had the support in Congress we wouldnt be talking about cutting back on Medicaid
Misty was killed at the age of 37 by an extremely powerful artificial opioid, fentanyl, laced into a batch of heroin. Overdoses from fentanyl have risen sharply in the city over the past two years, including the mass overdose of 27 people in four hours from a single batch of heroin last year. 
For many families, with remembrance comes frustration at what they regard as the indifference of politicians as the epidemic ballooned.
Even now, with Donald Trump and leaders in Congress saying it is a priority, relatives of victims are sceptical. Republicans want to cut Medicaid and scrap Obamacare, which have provided at least some access to treatment. 
I dont think we have the support in Congress, said Brown. Clearly not. If we had the support in Congress we wouldnt be talking about cutting back on Medicaid. You dont pull the rug out from under these folks who need help.
In West Virginia, politicians continue to cut the education budget in a state where lack of opportunity and despair are widely regarded as a driving force behind the opioid epidemic. The legislature passed laws for treatment centres but did not fund them.
Theres a lot of talk about wanting smaller government and less money but were in this mess and we need help, said Brown. We just have to have compassion and not turn our backs on people.
Brown invited the states elected politicians to come to Saturdays event, but only two turned up and no one from the governors office. David Grubb, a former senator in the West Virginia legislature who laid a pair of pink and purple running shoes to remember his daughter Jessie, said politicians shied away from taking the epidemic seriously because of the stigma around addiction. 
Were trying to build this treatment centre here in Charleston and its like pulling teeth finding money
I think its a really sad thing, he said. But the crisis has to reach critical mass and all of a sudden people say, Oh my God, why is this happening? And they start paying attention.
Grubb said West Virginia does not have the money to fund addiction treatment on the scale required, even with a boost from a $36m legal settlement with two major drug distributors this year.
One of the states thats richest in natural resources is one of the poorest in terms of social services, public education, all of that, he said. Its a travesty that we have allowed the powers that be  which tend to be coal, oil and gas  to reap tremendous profits from this state and not put back enough to fully fund the infrastructure that we need.
Right now were trying to build this treatment centre here in Charleston and its like pulling teeth finding money. Everybodys very excited, thinks its a wonderful idea. But nobodys said, Heres the money.
Grubb does look set to claim one important victory.
His daughter, Jessie, became addicted to heroin after she was sexually assaulted in college. Her brain became a different thing when it was in the throes of addiction, he said. She was unrecognisable. Stealing. Lying. Cheating. Who is this person?
Jessie veered in and out of addiction, but she was clean when she went into hospital for a routine hip operation for a running injury. Her medical record noted she was recovering from addiction but it was buried amid other information. The discharging doctor didnt see it and prescribed her 50 opioid painkillers. 
Grubb thinks Jessie should not have been prescribed them at all, and that 50 is way above what any normal person would need. The doctor said it was more convenient. Jessie began taking them and the craving kicked back in. The pills killed her. 
West Virginia senator Joe Manchin read about Jessies death and asked Grubb how he could help. The result was legislation, now working its way through Congress, requiring medical records to prominently display if someone is a recovering addict in the same way they are marked if a patient is allergic to penicillin. It has the backing of Trumps opioid commission. 
I was really surprised by that because I had not anticipated any support from the administration in that way, Grubb said. So it does have some momentum. If it does pass, then I think we really will save lives.
