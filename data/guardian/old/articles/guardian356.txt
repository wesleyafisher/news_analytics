__HTTP_STATUS_CODE
200
__TITLE

Top NFL commentator sorry for comparing New York Giants' form to Weinstein

__AUTHORS

Guardian sport
__TIMESTAMP

Monday 16 October 2017 08.51EDT

__ARTICLE
Al Michaels, one of Americas most respected sports commentators, has apologised after jokingly comparing the New York Giants problems to those of the disgraced Hollywood producer Harvey Weinstein, who has been accused of multiple cases of sexual abuse.
Michaels made the comments during NBCs Sunday Night Football, which regularly draws audiences in excess of 25 million. Referring to the Giants poor start to the season  they had not won a game before Sunday  Michaels said: And lets face it: the Giants are coming off a worse week than Harvey Weinstein, and theyre up by 14 points! His co-commentator, Cris Collinsworth added: only my LA guy comes up with that one.
Al Michaels' apology for Weinstein joke pic.twitter.com/QrIPTb6l2X
Michaels was widely criticised on social media for his comments, and later in the broadcast apologised. Sorry I made a reference earlier ... I was trying to be a little flip about somebody obviously very much in the news all over the country and it was not meant in that manner. So, my apologies, and well just leave it at that.
The 72-year-old is not the first media figure forced to apologise for comments about Weinstein. The British TV presenter James Corden said sorry after joking about Weinstein at a charity event in Los Angeles, while Woody Allen clarified comments about the producer after saying he felt sad for him.
Michaels has been a nationally recognised figure in the US for 40 years, winning five Emmys for his work. He has covered multiple World Series, Super Bowls and Olympics.

