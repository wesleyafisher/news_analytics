__HTTP_STATUS_CODE
200
__TITLE

Rising star: Ruth Davidson to appear on Great British Bake Off

__AUTHORS

Caroline Davies
__TIMESTAMP

Tuesday 17 October 2017 07.32EDT

__ARTICLE
Fresh from packing them in at the Conservative party conference, Scottish Conservative leader Ruth Davidsons profile is set be further raised with an appearance on a celebrity episode of The Great British Bake Off.
Davidson, beloved by the party faithful for overseeing a remarkable revival in Conservative fortunes in Scotland, will take part in a charity special of the Channel 4 programme later this year to raise money for Stand Up to Cancer.
The 38-year-old former journalist is no stranger to the baking show, having previously been a guest on the spinoff, An Extra Slice, when it aired on the BBC. She appeared alongside former Bake Off champion Nadiya Hussain, comedian Tom Allen and presenter Jo Brand last October, when she critiqued the 2016 contestants semi-final performances.
The Edinburgh Central MSP has also made a guest appearance on the satirical quiz show Have I Got News For You.
Indications are Davidson could be quite a messy Bake Off contestant; she was pictured throwing flour around at a bakery photo call on the general election campaign trail in May.
Former Labour shadow chancellor Ed Balls and former prime ministers wives Samantha Cameron and Sarah Brown have appeared on past Bake Off charity specials.
Davidsons popularity is growing . She was second only to the foreign secretary, Boris Johnson, when a recent YouGov poll asked Conservative members who should to step up as leader if Theresa May called time on her premiership, though as an MSP Davidson would currently be ineligible to stand.
