__HTTP_STATUS_CODE
200
__TITLE

Smoke on the water: a boating holiday adventure in France

__AUTHORS

Emma Cook
__TIMESTAMP

Sunday 8 October 2017 02.00EDT

__ARTICLE
Its a beast of a boat, more Puerto Bans than Canal du Rhne, satin white curves and chrome handrails gleaming in the sun.
And its ours for the week. This is Horizon, the shiniest and newest addition to the range by Le Boat, the canal boat specialist which offers self-drive craft along the waterways of mainland Europe, UK and Ireland.
At a little over 4m wide and 13m long, its footprint is around the same size as a small Victorian terrace, our terrace in fact, but with more en suites, a bigger outdoor area and a much newer, sparklier kitchen. So why does my husband look uneasy?
Its kind of  big isnt it? he says.
I know, the kitchen is amazing  two fridges!
But how are we going to park it?
Just dont reverse, well be fine, I tell him, pointing out a large rubber rim encircling the boat. See, were basically a bumper car on water.
As a rule, we dont do boats. There was an Isles of Scilly ferry crossing when the only upside, as waves crashed across the deck and my youngest threw up in my lap, was spotting dolphins following us through the wake. Then the fishing trip off a Greek island  more vomiting but no dolphins. Even a speedboat around a Cornish harbour left us reeling. But I want to overcome our dread of water and figure a gentle canal trip may convert us. We have six days ahead of us to wend 104km along one of Frances most picturesque canals, from Boofzheim in Alsace to Hesse, following the Canal du Rhne au Rhin and changing over in Strasbourg to the Canal de la Marne.
The water is serene, the route straight and the pace glacial. What could possibly go wrong? The next morning is an introductory lesson in how to steer and negotiate locks. The other families around us look relaxed and jolly about the journey ahead. We look like a family who have come on a boating holiday by mistake.
Were way out of our comfort zone, my husband hisses, out of earshot of the boat manager. She tells us we have to grab a green pole that will dangle over the boat as we approach each lock, pull it firmly then have our ropes ready to lasso the bollards once were stationary. If the ropes miss we will need to leap on to the bank to tie it up and avoid drifting towards the torrent of water flowing in behind. How many times do we have to do that? Only 33, says the boat manager cheerfully. Youll have to get your eldest to help.
Our 15-year-old has yet to surface, after discovering USB sockets in his cabin and free wifi. All three of our children are still in bed when we pull out of the harbour and strike out into the unknown. The steering is tricky at first and the boat zigzags from one bank to the other. But we get the hang of it and feel absurdly pleased with ourselves  even more so now I have an authentic rope burn on my hand from the last lock change.
My six-year-old is entranced by two otters following the boat. There are moor hens, newts and dragonflies, too
The eldest has appeared, lying on deck, and my husband is an expert now  a beer in one hand, the other resting casually on the wheel.
We enjoy a baguette, brie and beer from the last village, watching the absurdly pretty scenery pass by. Wouldnt it be lovely living on the water like this? we say. Its so peaceful and quiet, and you get a sense of freedom and adventure, knowing you can moor up wherever you want along the canal bank.
Im still seduced by the design detail below deck: seamless storage, sleek lighting, dinky bathrooms and a curvaceous master bed. Its a floating penthouse below and a nature reserve above. My six-year-old is entranced by two otters following the boat. There are moor hens, newts and dragonflies, too. Rows of poplars fringe the water framed by golden cornfields: empty, endless and utterly silent.
Then it happens. An alarm that pierces that silence like a knife. Red lights flash from every dial. Turn the engine off! I scream up the stairs to my husband but it cuts out anyway. I phone an emergency number and an engineer arrives. The engine is new, he explains, with no filters and it sucks up weeds until it overheats. Teething problems, he says, which shouldnt happen again. Just a blip then. We open a bottle of Alsace, laughing already about the dramatic highlights: white smoke billowing from the engine, my husband dragging it through the mud and reeds African Queen style.
Joy comes in the form of Strasbourg marina: we eat picnic lunches on deck and enjoy day trips into town by tram
We sit on deck, miles from the nearest village, playing cards as the light fades, and looking forward to an early start behind the wheel. But the next day the same thing happens and much of our time is spent waiting for an engineer to appear. But its a new boat, says a woman at reception when I call the second time. So was the Titanic, I want to say, ground down by the inevitability of it all.
The third breakdown is on a drab stretch on the outskirts of Strasbourg where I watch schoolboys throw stones at floating cans as it starts to rain. Heavily. My children are restless and bored below deck, what happened to the otters? No amount of Gerwrztraminer will lighten this moment, I think.
But eventually joy does come in the form of Strasbourg marina; when the boat is fixed, we moor up there for the final three days. It is close to town and it serves as a handy floating hotel. We eat picnic lunches on deck and enjoy day trips into town by tram. Strasbourg is an unexpected gem, compact and easy to navigate, with plenty to see. There is a striking Gothic cathedral and La Petite France, the old heart of the city, is a Germanic-looking Venice with wood-timbered houses and medieval waterways which we are only too happy to admire from a distance.
Then there are the winstubs (wine bars) and restaurants serving frothy beer and specialities like delicious handmade Alsatian pasta in a cream and mushroom sauce.
The second half of the holiday isnt exactly what wed planned but it is lovely; so when Le Boat offers us a brand new model to complete our canal journey, we decline. There were some idyllic moments but its a sign, I decide. No more leaving our comfort zone; were happiest on dry land and thats where we are going to stay.
The trip was provided by Le Boat. Sail from Boofzheim to Hesse via Strasbourg on the Arzviller Experience. A seven-night self-catered stay for five costs from 725 a boat or 1,436 on the Horizon. British Airways flies from Heathrow to Stuttgart (closest to Boofzheim) from 47 each way
