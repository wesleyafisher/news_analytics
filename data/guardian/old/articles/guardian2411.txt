__HTTP_STATUS_CODE
200
__TITLE

Japan sends in experts to rescue world's bedraggled bonsais

__AUTHORS
Agence France-Presse
__TIMESTAMP

Thursday 5 October 2017 08.06EDT

__ARTICLE
Tokyo plans to send green-fingered experts to every corner of the world on a mission to spruce up Japanese-style gardens that have fallen into disrepair.
There are about 500 traditional gardens across the globe  from France to the US, Thailand and Argentina  with dozens in dire need of help after years of neglect, Japans land ministry said.
About 40 gardens need immediate repair work, with trees growing out of control and overturned stone lanterns among the problems being faced, a ministry official said.
Tokyo, worried about a national embarrassment, Tokyo is tackling the problem by sending gardeners to the worst-affected sites, starting with a five-person team travelling to Romania and California this year.
The ministry will also organise lectures on how to keep the gardens in good condition.
The initial team will spend several weeks at the sprawling gardens, which measure about 5,000 sq metres each. Japanese gardens are distinguished by aesthetic features such as stone bridges, ponds, mossy paths, lanterns and perfectly pruned bonsai-like trees.
Most are maintained at a near-obsessive level, but many of those built outside Japan since the late 19th century, often as part of expositions, lack the experienced staff needed to keep them in good shape.
Reports of the unattended gardens in the Asahi Shimbun newspaper prompted requests to Japanese embassies abroad for help in repairing the damage.
Japanese gardeners left manuals after building the sites, but its difficult to maintain them, the ministry official said. We hope these ... gardens can be a place for spreading Japanese culture to people overseas.
