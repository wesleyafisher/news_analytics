__HTTP_STATUS_CODE
200
__TITLE

Generation rent: why Ill never have a house big enough for bulk-buys

__AUTHORS

Coco Khan
__TIMESTAMP

Friday 6 October 2017 08.00EDT

__ARTICLE
My mum loves a cheap buy. She hunts for them, possessed by an almost unconscious, spiritual determination. Like Captain Ahab searching for Moby-Dick, she wanders all the Cs (Currys, Co-op, Cash Converters) looking for the impossible: the Bargain. Shes adept at it, too, which might explain why, when I told her the Costco card she had proudly presented me with wouldnt be useful as I didnt have the storage space for bulk-buy, she looked crestfallen. But, Icorrected myself, watching her lips curl into asmile, I love it, lets go!
And so we went, hours lost in super-aisles, with Mum, the zealous bargain evangelist, home at last.
Remembering her joy brings me comfort as I clamber out of the pile of toilet rolls that now fall on me when I open the under-stairs cupboard or find Fairy liquid all over my dress because the only place I can store 24 bottles is in my wardrobe.
Shame I dont live in this five-bed in Wakefield (589,950, with Tepilo) where storage space itself comes in bulk. Imagine how many tins of plum tomatoes you could keep in its pantry, how many boxes of dishwasher tablets you could stack in the utility room. The four-poster bed looks great, but think of the suitcases you could keep under it. And if thats not enough, it has five (five!) cellar rooms for storage. I just want a place I can keep a winter coat, but a room for each food group wouldnt go amiss.
