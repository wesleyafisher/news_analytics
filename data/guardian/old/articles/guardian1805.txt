__HTTP_STATUS_CODE
200
__TITLE

Amazon Fire HD 8 review: easily the best tablet you can buy for 80

__AUTHORS

Samuel Gibbs
__TIMESTAMP

Tuesday 27 June 2017 02.00EDT

__ARTICLE
Amazons bigger, 8in HD version of its rock-bottom tablet, the Fire HD 8, has always played second fiddle to the 50 Fire 7, but not any more.
Unlike the new Fire 7 tablet, the 2017 Fire HD 8 is actually thicker and heavier than the previous version, weighing 28g more and being 0.5mm thicker at just under 1cm deep.
Side-by-side with 2015s Fire HD 8 its hard to see any difference. The outside is still hardwearing plastic, the sides and corners still rounded, but now it comes in a collection of bright colours.
Amazon claims its twice as durable as Apples iPad mini 4 and it certainly feels like it could take a knock or two without much issue. At 369g, it weighs as much as a large paperback.
One of the best bits about the Fire HD 8 is its speakers, which are mounted in the side (or top if held in landscape orientation) and produce fairly good, clear and loud audio, for a tablet. They were pretty good for watching a TV show while cooking, and make using Amazons voice assistant Alexa  which is now built into the tablet  much more like the experience you might get from an Echo Dot than the tinny speaker of the Fire 7.
The 8in, 720p screen is good but not great. Text is crisper than on the Fire 7, but still not up to the standards you might expect of a modern smartphone or premium tablet. It has good viewing angles, solid colours and is bright enough to be watched indoors, but struggles a bit outdoors.
I found reading on it surprisingly good though its nowhere near as clear and sharp as an e-reader. The integrated Blue Shade feature helps keep reading at night comfortable by reducing brightness and the amount of blue light emitted by the screen.
The new Fire HD 8 has the same 1.3GHz quad-core processor and 1.5GB of RAM as the last one, and performs similarly. It will handle most apps and games perfectly fine, if with slightly longer load times and slower frame rates than you might be used to on a top-end smartphone.
It is not what I would call snappy, but the Fire HD 8 scrolled, launched apps and switched between apps without any moments of lag that made me question whether I actually tapped a button or not.
With the brightness set at around 70%, the Fire HD 8 easily lasted long enough between charges to view three movies with a bit of light internet use in between and battery to spare. Playing games hurt the battery life a bit more, as did setting the brightness to max, but all in youll get around a days usage out of the HD 8. Charging it took forever, though, at around six hours, so best done overnight.
The Fire HD 8 runs Amazons customised version of Android called Fire OS 5.4, the same version as the Fire 7. It looks quite different to the traditional Android experience from Google, lacks Google apps and only has access to the Amazon App Store, not the Google Play Store.
Navigating it is easy with clearly marked panes filled with apps, games, books, video, music, magazines, and audio books etc. It makes the best of what is a media consumption device, rather than a working device.
There is an email app, Amazons Silk browser, contacts, calendar, WPS Office and other bits pre-loaded that do general information management and light office duties should you need them.
The Fire HD 8 has Alexa built-in and works pretty well. You have to hold the home button to get it to listen to you, but the speakers are loud and clear enough to hear the answer to your query from across the room. The screen also displays interactive cards such as music controls or more details on a weather forecast while Alexa speaks to you.
The 2017 Amazon Fire HD 8 tablet costs 80 for 16GB of storage with special offers, which are little adds on the lockscreen for recommended content from Amazons store. It costs 10 to remove the adverts. The version with 32GB of storage costs 100 with adverts.
The Amazon Fire HD 8 has always played second fiddle to the cheap-as-chips 50 Fire 7, but this year with the price cut by 10 to just 80 for the starting Fire HD 8, it is actually worth the extra money.
You get twice the storage, much longer battery life, a slightly better screen and much better speakers, which make the Fire HD 8 quite a compelling package, if all you want to do is consume media. Its no iPad, but then it doesnt try to be. It cuts the right corners to get the job done at a price worth paying.
There are some cheaper no-name alternatives, but with the Fire HD 8 you get a brand name, support and experience very difficult to match at 80. If youre just looking for a cheap tablet to watch a bit of video on, play the odd game and occasionally find the answer to a question, this is the one to buy.
Pros: it costs just 80, HD screen, microSD card slot, good battery life, feels durable, good speakers, Alexa
Cons: rubbish cameras, very slow charging, chunky, heavy for the size, no USB-C
