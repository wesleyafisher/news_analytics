__HTTP_STATUS_CODE
200
__TITLE

David Lurie obituary

__AUTHORS
Norma Cohen
__TIMESTAMP

Tuesday 17 October 2017 11.06EDT

__ARTICLE
My friend David Lurie, who has died of heart disease aged 65, pioneered the practice of Chinese medicine in the UK and also popularised the renaissance of the Argentinian tango.
Born in Cape Town to an Orthodox Jewish family, he was the son of Maisie (nee Kahn) and Joseph Lurie, known as Yankie, who ran the fishery set up by his Lithuanian father, and who died when David was 11. David lived in the affluent Bantry Bay suburb and was sent to the Herzlia Zionist school. The experience quashed any interest in Zionism and he became an atheist.
Intending to become a psychiatrist, he studied medicine at Cape Town University, but a visit to the Valkenberg psychiatric hospital prompted a realisation that asylum inmates were, as he said, saner than the fascists running the country. He switched to philosophy and, rejecting his familys comfortable lifestyle, plunged himself into anti-apartheid activism, including guerrilla theatre.
During a riot on the steps of the university in 1972, he met a fellow student, Yana Stajno, who was born in Zimbabwe to a Polish father and French mother. David and Yana married in 1974, had a son, Yabu, and moved to Knysia, where David built a traditional mud hut, designing an irrigation system for his vegetable plot.
As activists they were in danger from the regime, and, recalled Yana, violently racist farmers objected to us organising and housing the appallingly treated farm workers. So the following year the family moved to London as political refugees. By day, David sold home-made pancakes outside Swiss Cottage swimming baths, studying Chinese medicine and language by night. He and Yana became practitioners of traditional Chinese acupuncture. Their north London practice attracted diverse celebrity clients.
After seeing a tango show at the Roundhouse, Highbury in 1993, David was hooked on the dance form and its origins with the poor and dispossessed. He set up a string of tango communities across Britain, and when the Bosnian dancer and art student Biljana Lipi visited a tango salon he ran in the socialist club hall in Kentish Town, north London, she and David formed a dancing and teaching partnership, transforming the Dome, Tufnell Park, into a popular London salon that funded her studies. Davids favourite dance was the milonga, whose African origins inspired his own elegant and witty dance style.
A passionately keen allotment-holder, he developed three plots behind his house in Bounds Green, north London, dispensing horticultural advice with gluts of produce that overwhelmed his kitchen table. His enthusiasms encompassed bonsai trees, koi carp, surfing, open-water swimming, Alfa Romeos and the beaches of Paleochora in Crete.
He is survived by Yana, Yabu, his grandchildren Tillie, Bea and Dylan, and his sister, Ethel.
