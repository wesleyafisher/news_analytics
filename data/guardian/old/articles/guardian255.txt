__HTTP_STATUS_CODE
200
__TITLE

Facebook is buying anonymous teen compliments app TBH

__AUTHORS

Alex Hern
__TIMESTAMP

Tuesday 17 October 2017 07.34EDT

__ARTICLE
Facebook has acquired TBH, an app that allows teens to send anonymous compliments to each other. The cost has not been announced, but is reportedly less than $100m. 
The app, launched this summer in 37 US states only, has received more than five million downloads in a short space of time, thanks to its unique twist on the anonymous-messaging model of previous viral hits such as Secret, YikYak and Sarahah.
Users of the app are shown a positive question, asking them who makes you laugh the hardest or has the most integrity, along with a selection of four of their Facebook friends. The person they select as the answer is told that they were given the compliment, but not by who.
The model allows for the same sort of pleasant interaction with friends that previous anonymous messaging apps have enabled at their best moments, receiving nice messages from people who might be too embarrassed to say the same in person  as well as enabling a fair amount of anonymous flirting.
But it avoids the downside of many of its competitors, which is their capacity for anonymous bullying. By forcing users to only communicate through pre-checked questions, the app can keep things positive.
It seems to have worked: in a statement posted to the companys website, TBH said that more than 1bn messages had been sent since it launched.
At least for the moment, the acquisition wont change how TBH works. Similar to Instagram and WhatsApp, Facebook is leaving the app to run largely unaltered, with the same staff now operating from its Menlo Park headquarters. 
When we met with Facebook, we realised that we shared many of the same core values about connecting people through positive interactions, TBH said in its statement. Most of all, we were compelled by the ways they could help us realise tbhs vision and bring it to more people.
The acquisition also underscores Facebooks dislike of social networks it doesnt own or control. Following its purchase, Facebook now owns the second, third, fifth and sixth most downloaded free apps on the US iOS App Store  two, Facebook and Messenger, because it built them, and two, Instagram and TBH, because it bought them.
The fourth is Snapchat, an app it tried and failed to buy; and the first is YouTube, a service owned by its number one competitor which it is aggressively competing with through live streaming, original content and algorithmic tweaks to promote video on the newsfeed.
