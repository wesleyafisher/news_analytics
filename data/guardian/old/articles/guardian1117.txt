__HTTP_STATUS_CODE
200
__TITLE

Insult to injury: how Trump's 'global gag' will hit women traumatised by war

__AUTHORS
Tara Sutton in Amman, Joe Parkin Daniels in Fundacin, and 
Ruth Maclean in Maiduguri
__TIMESTAMP

Tuesday 1 August 2017 02.00EDT

__ARTICLE
If the first victim of war is the truth, the second is often female. And the people who pick up the pieces are usually aid workers, as it is their health centres and safe spaces and camp programmes that help women to work through the trauma of loss, displacement and sexual violence.
Funding for this work quite often features the US government. US global health funding has topped $10bn in each of the past three years.
But all that is now at risk, after President Donald Trumps decision to reinstate the so-called Global Gag rule, which will ban funding to any non-US aid groups that offer abortion services or advice funded from other partners.
As the cases below demonstrate, often the support given to these victims of war will have nothing to do with abortion. But, because the provider might be linked to abortion advice elsewhere in the world, the life-saving programmes they offer are in jeopardy. Without US funding, some are likely to close by the end of the year. 
And through no fault of their own, women suffering the agonies of war will find themselves alone.
Menash had been a sex slave for Islamic militants Boko Haram for months when one of her kidnappers declared he wanted to marry her.
She refused  she was already married with six children  although her husband had fled their home when she and her sister were abducted. Eventually, she escaped.
As she sits in a quiet cubicle in Muna camp in Maiduguri with her baby son, the trauma Menash experienced shows in her movements and on her face: her head, her eyes, her strong hands, all seem heavy under the weight of what she went through in the Sambisa forest.
Five men used to come and rape me, she said. I complained about them to the man who wanted to marry me, but he just said: Thats their tradition. Thats what they do. Even if Id married that man, the others would have kept raping me.
We are sitting in a safe space created by the UNFPA to help women like Menash, of whom there are thousands in this camp alone. 
This is a place that welcomes women who have fled their homes or, like Menash and like the Chibok girls, escaped captivity under Boko Haram, which abducted, raped and murdered thousands of Nigerians, and left millions homeless.
Far from home, with no money or food and still facing violence and sexual assault in the camps, women can come to these hastily thrown-up buildings, sit on colourful plastic mats spread on the concrete floor, talk to each other, watch television, and learn to sew or make detergent to sell.
But as the US defunding of UNFPA threatens projects like this, the question is: for how much longer?

Here, the fund not only helps women get contraceptives, if they want them, but does much more. In camps across Nigerias north-east, they hand out soap, sanitary towels and clothes to those who have just escaped from Boko Haram and who often arrive in rags with nothing. Midwives examine pregnant women, sending those with complications for further medical help. Women who need counselling, like Menash, also get help.
Menash had not anticipated what her punishment would be for refusing to marry her captor, but she thought it could not be much worse than what she was already living through. However, then she was taken outside and made to kneel in the dirt, surrounded by other women including her sister.
As the sewing machines whirr outside our cubicle, she lifts her hijab over her face and leans forward, her fingers tracing a scar on the back of her neck. They tied me up and tried to cut my head off, she said. I thought my life was over.
She had been struck several times by a knife when a plane overhead suddenly made her attacker flee, along with other insurgents. But Menashs hands were still tied behind her, with blood pouring down her back. Suddenly, her sister ran to her and helped her up.
She cut the rope around my hands, and I ran, I just kept running. I couldnt stop, said Menash. But my sister was not running fast enough. I havent seen her since.
In Maiduguri, her husband rejected her, she said. He shouted at her to get away, calling her a Boko Haram whore and beating her. 
Nobody took my side, they were all yelling Boko Haram wife at me, she said quietly. I just turned around and left.
When she met Zainab Umar, a counsellor at Munas safe space, several months later, Menash was starving and dirty, and her hands shook violently. She said strangers sometimes laughed about her to her face and spread her story around.
Around 60% of women are estimated to have experiencedgender-based violence in the north-east of Nigeria and rape is rife in the camps. The need is huge for Umar and her colleagues, who work non-stop to counsel women like Menash and help them to get on their feet financially. Nevertheless, health workers and counsellors in the 20 safe spaces for women and girls across the region are not sure how much longer they will have jobs.
The UNFPA  which suffered another blow last month, when its director Babatunde Osotimehin died suddenly  is trying to find other donors to fill the gap left by the US. But it is a difficult time to raise money in Nigeria, where less than a third of the $1bn needed to address the humanitarian crisis created by the rise of Boko Haram has been raised. 
Menash approached Umar and asked for something to eat after she had seen her talking to women in the camp. Umar explained that she wasnt offering food, but family planning advice, neonatal checks and counselling. Initially, I thought I wouldnt bother, said Menash. But then I thought  even if he cant give me anything, maybe I should see her. No one had been kind to me.
Even after she had decided to go, it took her a while to open up. But then Umar told her that Boko Haram had made her suffer, too. She told me: They killed my own son , so then I thought, I can share my story with this woman.
Umars approach to the dozens of women she sees each day is simple. She reassures them that their conversation is confidential and encourages them to talk about their experience, and then tries to find ways for them to keep busy as a distraction.
Many of their husbands have been killed, and many are traumatised, said Umar.
Aseel, 25, had done the hard bit. She had escaped from Isis and its stronghold in Raqqa, her hometown, and she had made it to the relative safety of Jordan.
But thats where her problems deepened. Penury stalked Aseel, who lived with her husband in a makeshift shack on a roof. The birth of her first child merely deepened her sense of loss, alienation and depression.
She stood on a roof edge and prepared to jump, but a neighbour intervened and took her to the Noor Al-Hussein Clinic in Amman, Jordan, a one-stop shop for womens reproductive and mental health.
I was so lonely then, said Aseel, sitting in her counsellors office. I didnt know a soul. As we had no money we had to keep moving every four months to cheaper and cheaper apartments.
She joined 10 other women in a group therapy session led by counsellor Shiraz Nsour. Aseel was the last woman to talk. By the time I had heard the other nine womens stories, I already felt that my problems were not so heavy. I heard about awful cases of domestic violence.
Nsour called her three times a day until she believed that Aseel was no longer a suicide risk, and Aseel finally felt she had some friends. This centre saved my life, she said, as her toddler son squirmed on her lap playing with the straps of her handbag.
The women love to come here, they feel comfortable in the space, said Nsour. The safe spaces allow women a level of privacy because they offer a variety of services. A woman might choose to tell her family she is going for a medical checkup when really she is going for psychological help.
Depression, anxiety and even suicide are what I commonly see in Syrian refugee women, said Nsour, who has been working with refugees since they began arriving in 2012. Jordan is home to more than 660,000 Syrians. 
People know about the physical need of refugees for food and shelter but they dont consider their mental health. Many are extremely traumatised by what they have gone through in the war and then this is coupled with the poverty and uncertainty of their lives here, which leads to a lot of psychological issues.
The future of the centre is now under threat. It receives a large part of its funding from the UN population fund, UNFPA, and is one of many in Jordan that will be affected by the Trump administrations decision to defund the agency in April. In Jordan, that equates to a $3.2m hole in its funding this year.
UNFPA is already facing a global funding crisis, with predictions that it will have a funding gap of more than $700m (537m) by 2020. Last year, US contributions to UNFPA totalled $69m, with most of the money used to support short-term, non-core projects such as those that support people displaced by conflict.
At the moment, UNFPA supports 19 safe spaces across Jordan inside and outside refugee camps. In April alone, the safe spaces were accessed by 3,470 women and girls. But these services are now at risk.
What was so great about the US funds is that they were not earmarked, said Fatima Khan, a gender-based violence officer at UNFPAs Amman headquarters. It makes life much easier for us, in terms of planning to know we have funds that can be used where they are most needed.
We have made up most of the funding shortfall for the remainder of 2017, but beginning in 2018 the gap will be difficult to fill, said Christina Bethke, emergency reproductive health officer for UNFPAs Syria cross-border programme. With competing emergency priorities all over the world, the withdrawal of a major UNFPA donor will likely mean we will need to make some hard and unfavourable choices.
Merlis Castro was living in a poor village in Colombias banana-growing region when the civil war swept through her home. Paramilitaries accused her father-in-law of collaborating with Marxist rebels of the Revolutionary Armed Forces of Colombia, or Farc.
The militiamen killed him and dismembered his body with a chainsaw. Then they stripped Castro naked, sexually assaulted and beat her. I held my son close to my breast while they had their hands all over me, said Castro, sitting outside the small shop she now runs.
She fled her home, and eventually found refuge in Villa Fanny, a makeshift hamlet on the outskirts of Fundacin, a dusty town in the Magdalena department.
There, Castro set about rebuilding her life, with help from the Asociacin Pro-Bienestar de la Familia Colombiana (Profamilia), a Colombian womens health and reproductive rights NGO. The group, which is partly funded by the US Agency for International Development (USAid), provides classes on sexual health, self-esteem, and what to do following a sexual attack.
For Castro, the therapy was invaluable: after the attack, she had found it difficult to maintain a relationship and had separated from her partner. I couldnt look at my husband after what happened to me, she said, with tears in her eyes. Profamilia has helped all of us victims here.
But that help will soon come to an abrupt end. Profamilia is a member of the International Planned Parenthood Federation, which has refused to sign the global gag.
Its fair to say that around December our services will stop, said Mara Elena Santo, a psychotherapist working with Profamilia in Fundacin. Its a big risk because we have been working to empower the women in these neighbourhoods and make sure they are able to exercise their sexual and reproductive rights.
Many in Fundacin argue that it is unfair to destroy the wide range of social work carried out by Profamilia, simply because the organisation also provides abortions.
Profamilia is among countless NGOs worldwide providing healthcare to vulnerable women that will suffer. IPPF has said that it will forego around $100m between now and 2020, money that could have prevented 20,000 maternal deaths, apart from its other social work.
Fewer communities will be hit harder than the Villa Fanny neighbourhood, whose residents are almost all victims of Colombias half-century of civil conflict, and where many women have endured sexual violence. They have killed our husbands and our sons, and they have raped and abused us, said Castro, who was elected community leader last year. We are all victims dealing with trauma.
They did a lot of damage to me, she said. I still wake up in the middle of the night unable to escape what happened.
Mara Henrquez was forced from her home by paramilitaries in 2005, and now helps women in Villa Fanny to build their self-esteem. I have worked with Profamilia, and the work has been great, she said. Teaching victims to respect themselves is very difficult.
In addition to its history of violence, Colombia is affected by health issues that are prevalent in the wider region, including teenage pregnancy and high rates of maternal mortality.
In Colombia, one in every five girls aged between 15 to 19 are or have been pregnant, according to a study by the UNFPA, and Magdalena ranks among the five departments with the highest rates of adolescent pregnancy.
Fundacin has one public hospital, which has been classified as among the most basic in Colombia. Staff there worry that without programmes such as Profamilias, unwanted pregnancies and unsafe abortions will rise steeply.
We are battling with a culture that does not look at the risks of unprotected sex, said Sofia Snchez, the chief nurse. Families dont want to talk about contraception, girls worry that their father might see a condom in their handbag, and the result is that they choose to have sex without one.
