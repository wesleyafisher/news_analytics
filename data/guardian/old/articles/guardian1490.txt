__HTTP_STATUS_CODE
200
__TITLE

Israel will not negotiate with Palestinian unity government if Hamas is involved

__AUTHORS

Peter Beaumont in Jerusalem
__TIMESTAMP

Tuesday 17 October 2017 14.01EDT

__ARTICLE
Benjamin Netanyahu, Israels prime minister, will refuse any diplomatic talks with an emerging Palestinian unity government if the Hamas militant group plays any role within it.
In a statement released on Tuesday evening, Netanyahus security cabinet set out a series of conditions which appeared to complicate Egypt-sponsored efforts to end a decade-long split between Palestinian president Mahmoud Abbass Fatah movement, which rules in the West Bank, and the Islamist group Hamas, which has been in control of Gaza.
Following previous decisions, the Israeli government will not hold political talks with a Palestinian government that is supported by Hamas, a terror organisation calling for the destruction of Israel, the statement read.
Fatah and the rival Hamas this month reached a preliminary reconciliation agreement. The two groups signed a provisional unity agreement in Cairo on Thursday.
Israels lengthy list of preconditions for any negotiations includes a number of demands:
The Israeli statement also called on the Palestinian Authority to continue acting against Hamas on the West Bank and to sever its ties with Iran. 
The issue of the disarmament of Hamass 25,000-strong armed wing was already a key issue in the Palestinian reconciliation negotiations. 
Although it is being pushed by Abbas himself, most observers see it as one of the most difficult sticking points. Unlike Hamas, the Palestine Liberation Organisation  which Abbas leads  has recognised Israel.
Diplomats had been cautiously optimistic that the Palestinian reconciliation negotiations hosted by Cairo might trigger positive steps for internal Palestinian politics  and ultimately could lead to a renewed effort in the Israeli-Palestinian peace process.
According to Israeli media reports, the security cabinet also authorised Netanyahu to consider taking punitive steps against the Palestinian Authority in the event of a unity government being formed  including potentially withholding funds.
It stopped short, however, of demands from Naftali Bennett, the leader of Israels far-right Jewish Home party, to sever all links with the Palestinian Authority. 
Although peace negotiations between Israel and the Palestinian Authority have been frozen since 2014, the Israeli demands would appear to have killed any hopes of movement. Most analysts believe that any renewed peace progress requires both Palestinian internal reconciliation and negotiations with Israel. 
The statement is in line with previous moves by Netanyahu, who has often sought to impose preconditions on Palestinians for any movement on the peace process.
On Tuesday a senior Hamas spokesman denied reports that Hamas had secretly agreed to halt attacks against Israel from the West Bank as part of reconciliation talks.
There are no secret clauses in the reconciliation understanding, and what the occupation published on the resistance halting in the West Bank is not true, senior Hamas spokesperson Husam Badran told Palestinian news website Quds News Network.
The position to choose resistance is not connected to any person or entity, but rather it is the position of the entire Palestinian people to decide. The natural situation is that when there is an occupation, there will be a resistance to confront it, said Badran.
The Israeli statement came just days after Tony Blair, the former British prime minister, said for the first time that he and other world leaders were wrong to yield to Israeli pressure to impose an immediate boycott of Hamas after the Islamist faction won Palestinian elections in 2006.
