__HTTP_STATUS_CODE
200
__TITLE

The question that should be asked at Oxford interviews

__AUTHORS
Letters
__TIMESTAMP

Sunday 15 October 2017 13.47EDT

__ARTICLE
Despite his profound knowledge of world history, in describing the Iran nuclear deal as the worst deal ever (Report, 11 October), President Trump seems to have forgotten, for starters, the Munich agreement and the Molotov-Ribbentrop pact.Tim OttevangerLutterworth, Leicestershire
 The grounds for the appeal court decision (Segregation of boys and girls at religious schools illegal, 14 October), that every child suffers illegal discrimination when not allowed to mix with the opposite sex, must logically apply to single-sex schools too. And not before time.Tim BellNottingham
 You publish an intriguing set of questions for Oxford interview candidates. (Why Oxford asks its applicants would you run a red light?, 13 October). Another one might be: how can it be that 50% or more of Oxbridge undergraduates come from the 7% of the population who go to private schools? As with the other questions, maybe there is no right answer.Steve DempseyEdinburgh
 It may not have happened on 27 September as Colin Barr (Letters, 9 September) hoped, but on Saturday East Fife did manage to win by the magical score of 5-4. Sadly not against Forfar but Albion Rovers. Lets see what happens on 27 January.Steven EdgarBristol
 I was fascinated by the report of the house with spherical rooms (Home and away, 14 October) but the details should have made it clear that any new owner would have to invest heavily in beer matsto keep the furniture level.James CairdLudlow, Shropshire
 Discussion of the dual use of lavatorial reading matter (Letters passim) reminds me of Scouse Mouse, George Mellys autobiography of his childhood years. His father, determined that the household should economise, ordered a three-sheet limit: One up, one down and a polisher.Neil LevisLondon 
 Join the debate  email guardian.letters@theguardian.com
 Read more Guardian letters  click here to visit gu.com/letters
