__HTTP_STATUS_CODE
200
__TITLE

New airplane biofuels plan would 'destroy rainforests', warn campaigners

__AUTHORS

Arthur Neslen
__TIMESTAMP

Thursday 12 October 2017 13.17EDT

__ARTICLE
A new plan to accelerate production of biofuels for passenger planes has drawn stinging criticism from environmentalists who argue that most of the worlds rainforests might have to be cleared to produce the necessary crops.
Aviation is one of the fastest growing sources of greenhouse gas emissions, with an 8% leap reported in Europe last year and a global fourfold increase in CO2 pollution expected by 2050.
To rein this back, the industry has promised carbon neutral growth by 2020  to be met by biofuels, if a blueprint is approved at an International Civil Aviation Organisation (Icao) conference in Mexico City tomorrow.
The green jet fuel plan would ramp up the use of aviation biofuels to 5m tonnes a year by 2025, and 285m tonnes by 2050  enough to cover half of overall demand for international aviation fuel.
But this is also three times more biofuels than the world currently produces, and advanced biofuels are still at too early a stage of development to make up the difference.
Environmentalists say that the most credible alternative fuel source would be hydrotreated vegetable oil (HVO), even though this would probably trigger a boom in palm oil plantations and a corresponding spike in deforestation.
Klaus Schenk of Rainforest Rescue said: Citizens around the world are very concerned about burning palm oil in planes. The vast use of palm oil for aviation biofuels would destroy the worlds rainforests, the basis of life for local people and the habitats of endangered species such as orangutans. We urge Icao to scrap its misguided biofuels plan.
 It is impossible to quantify the precise extent of deforestation that the proposal could cause, but based on the Malaysian Palm Oil Councils crude palm oil yields and Total conversion figures, Biofuelwatch estimate that 82.3m hectares of land (316,603 sq miles) would be needed to meet the target, if it were sourced from palm oil alone. That is more than three times the size of the UK.
Carlos Calvo Ambel, a spokesman for Transport and Environment, said: Most biofuels are worse for the climate than jet fuel. Quality should always go before quantity. Establishing a goal even before the rules are set out is putting the cart before the horse. The European experience has been that biofuels targets sucked in palm oil exports whose emissions were far greater than those of fossil fuels.
T&E, Oxfam and Friends of the Earth are among nearly 100 environmental groups protesting the proposal, while 181,000 people have signed a petition calling for the initiative to be scrapped.
Inside the conference hall, several states are also opposing the biofuels pitch which, if passed, is expected to go on to an Icao assembly for formal adoption within two years.
Brazil and Indonesia strongly support the plan but China has questioned its feasibility, the EU wants more robust sustainability criteria, and the US says it will not support globally coordinated emissions reductions targets.
An industry proposal to limit the biofuels target to 2025 is one possible compromise, but others may emerge before the plan is put to a vote.
Almuth Ernsting, a spokeswoman for Biofuelwatch, said the current proposed target was so huge that it would be unlikely to be fulfilled  but you could still have massive negative impacts from much smaller uses of palm oil.
Within four years of the EU setting a binding target to source 10% of its transport fuel from renewable sources in 2009, studies show that European investors had bought 6m hectares of land for biofuels production in sub-Saharan Africa.
The EU took very little of its biofuel feedstock from Africa in the end, but the use of palm oil from elsewhere for biodiesel had soared 500% by 2014, according to industry trade figures. 
