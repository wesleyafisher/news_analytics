__HTTP_STATUS_CODE
200
__TITLE

Observer killer sudoku

__AUTHORS

__TIMESTAMP

Saturday 14 October 2017 19.01EDT

__ARTICLE
Normal Sudoku rules apply, except the numbers in the cells contained within grey lines add up to the figures in the corner. No number can be repeated within each shape formed by grey lines.
For a helping hand call our solutions line on 0906 200 83 83. Calls cost 1.03 per minute from a BT landline. Calls from other networks may vary and mobiles will be considerably higher. Service supplied by ATS. Call 0330 333 6946 for customer service (charged at standard rate).
Buy next weeks Observer Digital Edition to see the completed puzzle.
