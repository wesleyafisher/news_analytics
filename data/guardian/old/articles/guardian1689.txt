__HTTP_STATUS_CODE
200
__TITLE

Jeremy Corbyn urges Twitter and Facebook to tackle religious hatred

__AUTHORS

Harriet Sherwood Religion correspondent
__TIMESTAMP

Monday 16 October 2017 18.03EDT

__ARTICLE
Social media platforms such as Twitter and Facebook must shape up to tackle racism and abuse, Jeremy Corbyn has said.
Speaking at a packed meeting at Finsbury Park mosque in north London called to address a rise in attacks on Muslim women over recent months, the Labour leader said: A lot of abuse takes place on online media and on social media.
He said he sometimes goes through the train of events of someone whos been abused and was shocked at what he found.
He had examined social media reaction to comments made by him and Labour MP Diane Abbott following the stabbing of a young Kurdish man in Croydon last year. It was unbelievably vile, revolting, disgusting stuff that was thrown particularly at her and to some extent at me  Thats just not acceptable. These media platforms that are hosting social media conversations must shape up. Racist language is wrong, evil and divisive and should not be allowed.
The Labour leader also criticised mainstream media, echoing repeated complaints voiced by members of the public at the meeting over its distorted reporting of Islam and lack of reporting of Islamophobia. The medias reporting of attacks on Muslims was not sufficient  [all attacks] should be reported, Corbyn said.
Hate crime must be challenged wherever and however it raises its head, he told the meeting. As communities we must stand up for each other. An attack on any community  whether it be a mosque, a synagogue or a church  was an attack on all of us, he said.
Nobody should ever feel alone, isolated, at risk because of their faith, the clothing they choose to wear, the lifestyle they lead, he added.
Local Muslims were targeted in an attack in June, when a van ploughed into worshippers outside the Muslim Welfare House, 100 yards from the mosque, shortly after midnight during the holy Islamic month of Ramadan. Makram Ali, 51, died of multiple injuries and 11 people were injured.
The attack came amid a sharp rise in Islamophobic attacks in the aftermath of terrorist atrocities in London and Manchester. The London mayors office recorded a fivefold increase in anti-Muslim attacks following the London Bridge attack in June.
Women were particular targets, especially on public transport, according to monitoring groups. We all experience it, and we often dont even realise its a crime, said one woman in the audience. Vulgar things yelled at us, being pushed, being confronted, being terrorised.
Others spoke of routine harassment and abuse, both in person and online. Some women said they were disappointed by the outcome when they had reported attacks to the police. One said that she and others faced a triple whammy of discrimination as Muslims, ethnic minorities and women.
Corbyn said he would continue discussions with Transport for London and the police about how they dealt with reports and complaints. If youre a woman alone and youve been abused in the street, or abused where you live or abused anywhere at all, youve taken a big step in going to tell the police about it  There must then be an appropriate response by the police to acknowledge and understand the seriousness of the crime. 
He suggested that reporting systems could be put in place in schools, colleges, workplaces and places of worship.
Det Supt Treena Fleming drew attention to the Self Evident app, in which hate crime could be reported to the police, evidence could be filmed and statements could be recorded without the need to go to a police station.
