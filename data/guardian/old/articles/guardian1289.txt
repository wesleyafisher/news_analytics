__HTTP_STATUS_CODE
200
__TITLE

Lets go to ... spooky south-west Wales

__AUTHORS

Jane Dunford
__TIMESTAMP

Sunday 30 October 2016 05.00EDT

__ARTICLE
Launched in time for Halloween, the Legends Tombstone Trail offers a novel way of exploring Camarthenshire, Neath Port Talbot, Swansea Bay and Pembrokeshire  by visiting graves and memorials in cemeteries across the four areas. Taphophiles (yep, tombstone tourism is a thing) are in for a treat  and for everyone else theres beautiful scenery plus plenty of good pubs and pretty villages en route.
There are five tombstones with tales attached in each area, with an interactive map to guide you. Allow two-to-three hours for each, otherwise make a mini-break of it and spend two days visiting all of them on a 200-mile road trip.
Saints, sinners, pirates, wizards and everyone in between. From Welsh poet Dylan Thomass final resting place in Laugharne to fearsome Welsh pirate Barti Ddu  buried near Fishguard in 1722  there are fascinating and spooky stories galore. Sites associated with mythical characters, from King Arthur to Merlin, feature too, alongside mere mortals such as gamekeeper Robert Scott, murdered and buried in Margam Abbey cemetery, where his ghost has been spotted.
Try the Kings Head at Llangennith (doubles from 85 B&B), on the Gower peninsula, or the famous Browns Hotel in Laugharne, Camarthenshire, which dates from 1752 (doubles from 95 a night).
There are brilliant pubs a-plenty. The Pilot in Mumbles, near Swansea, serves great real ale; in St Davids, near the end of the trail, the Farmers Arms is a cosy place for a tipple. For food, go to Danteithion Wrights Food Emporium in Llanarthne, Carmarthenshire, worth it for its pork belly cubano alone. The Swigg is a fab, newly opened cafe and deli in Swansea Marina.
South Wales is made for outdoor adventure. Richard Burton fans will love new walking trails in his honour in Port Talbot. The Botanic Gardens of Wales in Carmarthenshire have lovely autumn colour or, if the weather isnt so kind, LC Swansea is the countrys largest indoor waterpark, complete with surf machine, while the Glynn Vivian Art Gallery reopened in the city last weekend after a multi-million pound transformation.
