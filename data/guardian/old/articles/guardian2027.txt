__HTTP_STATUS_CODE
200
__TITLE

Tips, links and suggestions: what are you reading this week?

__AUTHORS

Guardian readers and 
Sam Jordison
__TIMESTAMP

Monday 9 October 2017 10.00EDT

__ARTICLE
Welcome to this weeks blog, and our roundup of your comments and photos from last week.
Lets start with good news. Last week, bluefairy didnt know what to read. This week, the problem has been solved thanks to Pattern Recognition by William Gibson:
 And I think it was the right choice. I really enjoyed it, especially the descriptions of Japan in there (brought back some lovely memories). There are quite a few memorable quotes in this book, I think this one is my favourite:
She knows, now, absolutely, hearing the white noise that is London, that Damiens theory of jet lag is correct: that her mortal soul is leagues behind her, being reeled in on some ghostly umbilical down the vanished wake of the plane that brought her here, hundreds of thousands of feet above the Atlantic. Souls cant move that quickly, and are left behind, and must be awaited, upon arrival, like lost luggage.
Heres a good way to amp up the reading, courtesy of fuzzywuzz - who also provides a few recommendations:
I have been keeping a record of my reading pursuits for about a year or so now. From January this year I have read 22 books, which for me, is quite an accomplishment. I thought that by keeping a record it will spur me on to read more, which it has. 
Highlights so far this year include We Need to Talk About Kevin by Lionel Shriver, Skulduggery Pleasant by Derek Landy, The Hearts Invisible Furies by John Boyne, In Cold Blood by Truman Capote and Let the Right One In by John Lindqvist.
Alas, things have recently been moving more slowly for Kemster: 
Yesterday I finished Henry James novella The Beast in the Jungle. For the most part it was like wading through glue and cement. 
Somtimes, judging a book by its title alone can be a good strategy. As Dylanwolf explains in relation to Another Bullshit Night in Suck City by Nick Flynn:
Yeah, OK, I bought this one because of the title. Who could resist it? But I was rewarded too with a good read. ABNISC is a memoir focusing on his estranged father, Jonathan who he meets up with again as a guest at a homeless shelter where Nick is working in Boston. His father is an alcoholic and some, Nick is merely an alcoholic... Jonathan is an horrifically unsympathetic character, self-aggrandising and well as self-delusional and a complete pig to anyone around him. Unsurprisingly I guess.
The book is a fairly easy read, but unrelentingly depressing in spelling out the unremitting squalor of the life of down and outs that is so disgracefully prevalent in our cities in the UK as well as the US. Judge a society by how well it treats its most deprived? We are utterly condemned.
Elsewhere, Ann Patchetts The Magicians Assistant has done the trick for juliewhitney:
It is a story of a womans grief for her husband. Sabine is studying architecture and by chance, while waiting tables, is asked to assist in a magic act being performed on the stage by a beautiful magician, Parsifal. There then begins an unusual 22-year relationship which has now come to an end with the death of Parsifal...
It is a story about being trapped in small places: Parsifals claustrophobia, MRI scanners, fridges and small towns. And of escape: Sabines parents fled Poland for Israel, then left Israel for firstly Canada and then Fairfaix in Los Angeles; Phan fled Vietnam ending up in LA via Paris; and Parsifal fled Nebraska for reasons we presume to know but find out we are wrong. 
Patchetts simple prose beautifully brought to life a picture of that small town in Nebraska, the inter-dependencies of Parsifals family and the contrast with Sabines life in LA... I really enjoyed this book.
Finally, a Kazuo Ishiguro celebration. The news that he had won the Nobel prize in literature went down well here on Tips,Links and Suggestions. Yaaaaay! wrote goodyorkshirelas. Kazuo Ishiguro, Nobel Laureate. Well deserved. A charming, modest chap too. It was, according to WebberExpat: Another surprise. A happy one. And samye88 was squealing with delight. 
If you would like to share a photo of the book you are reading, or film your own book review, please do. Click the blue button on this page to share your video or image. Ill include some of your posts in next weeks blog.
If youre on Instagram and a book lover, chances are youre already sharing beautiful pictures of books you are reading: shelfies or all kinds of still lifes with books as protagonists. Now, you can share your reads with us on the mobile photography platform  simply tag your pictures there with #GuardianBooks, and well include a selection here. Happy reading!
