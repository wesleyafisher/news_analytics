__HTTP_STATUS_CODE
200
__TITLE

Brexit healthcare deal is good news for pensioners

__AUTHORS

Lisa O'Carroll Brexit correspondent in Brussels
__TIMESTAMP

Thursday 31 August 2017 13.54EDT

__ARTICLE
British pensioners who have retired to other EU countries will continue to have their healthcare paid for by the NHS post-Brexit, after a deal in principle was agreed by negotiators in Brussels.
In one of the few advances made in discussions about EU citizens future rights, the Brexit secretary, David Davis, said there had been agreement on four key areas, including reciprocal healthcare for British and EU retirees affected by Brexit.
This is good news for British pensioners in the EU, he said.
Other areas of agreement included protection for frontier workers, those who live in one EU member state and work in another. This would include people who live in the UK and commute to Europe, or Britons settled in one country, for example Germany, who commute to work in another, say Luxembourg.
Also, professional qualifications would be recognised across the bloc after Brexit, allowing lawyers, doctors, accountants, seafarers, train drivers and others who have moved to or from the UK to another EU country to work under their existing credentials.
 There was also agreement to coordinate on social security post-Brexit. However, there was still disagreement on more than half the issues discussed, including the eventual oversight of the legal rights of EU citizens.
The EUs chief negotiator, Michel Barnier, hinted that Brussels was insisting that such oversight should be held by the European court of justice  a red line for Britain.
In his opening address at the press conference after the third round of talks, Barnier raised serious concerns about the Home Offices capacity to oversee or police any deal on EU citizens rights given the recent debacle when it mistakenly sent letters to about 100 EU citizens, threatening deportation.
The UK quickly recognised that it was a mistake, but this is not the first time this has happened and it reinforced the point that [this] needs to be under the control of the ECJ, a point [on] which we disagreed today, Barnier said.
British pensioners across Europe will be relieved, however, that there has been progress on reciprocal healthcare rights.
According to figures issued to a parliamentary select committee this year, Britain spends 650m reimbursing other EU countries for treating British patients. Of that, about 500m goes on 190,000 registered pensioners, including 70,000 in Spain, 44,000 in Ireland, 43,000 in France and 12,000 in Cyprus.
The agreement will allow a British pensioner who has retired in another EU country to travel to other EU countries on holidays and use the existing European Health Insurance Card should they need medical attention.
It is understood Britain was pushing for this agreement to cover British tourists as well, but the EU said it was not an issue to be discussed in a deal for EU citizens.
Twenty-seven million Ehic cards have been issued in Britain.
Davis said EU citizens would remain a top priority and there had been a wide range of agreements in discussions this week.
It is understood Britain reiterated its commitment not to enforce the requirement that EU citizens who are self-sufficient, including students and stay-at-home parents, have private health insurance. They will be able to be treated by the NHS. 
