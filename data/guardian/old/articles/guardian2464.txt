__HTTP_STATUS_CODE
200
__TITLE

Old 1 coin: relaxed retailers mean round pound will be around for longer

__AUTHORS

Rebecca Smithers and 
Rupert Jones
__TIMESTAMP

Monday 16 October 2017 07.28EDT

__ARTICLE
As the lockdown on the old pound coin came into force on Monday, there was plenty of evidence that retailers and machines were taking a relaxed attitude towards the coin.

Despite the widely publicised deadline of midnight on Sunday  when the old coin ceased to be legal tender  our reporters had no problem offloading outdated money. 
John Lewis and Waitrose  both part of the John Lewis Partnership  became the latest retailers to soften their approach on Monday when they said their stores were free to accept the old coins until Saturday. John Lewis said: All our systems and trolleys are ready for the new coin. However, we want to be helpful to our customers and, if they have no alternative ways of paying, we will continue to take old-style coins until the close of trade on 22 October.
Meanwhile, clearly with an eye on childrens pocket money, The Entertainer toy store said it will accept the old pound coins in all 140 UK stores until the end of the year. Aldi, Iceland and Poundland have already said that they will continue to accept the coin until 31 October, while Tesco has agreed to take the old-style coins for a week after the 15 October deadline.
But on Monday branches of Boots, Asda and Morrisons visited by the Guardian were all accepting the old coins without question, along with Transport for London (TfL) ticket machines. At a branch of Asda in Walthamstow, north-east London, a cashier accepted 1 to pay for some chewing gum, while at Boots an old 1 was accepted as payment for some sweets.
TfLs ticket machines  used by people buying tube and London Overground tickets  have been updated to not accept the old 1 coin, but a few do appear to still be taking it. A machine at Walthamstow tube station accepted two of our old 1 coins without quibble.
Despite the midnight expiry date, the self-checkout tills at Morrisons in Letchworth, Hertfordshire, were accepting old pound coins this morning. Staff on nearby tills said they would continue to to take the old coins until 28 or 29 October.
Sainsburys has said it would continue to accept the old coins at its tills on Monday,[see footnote] and that shoppers would be able to donate them to this years poppy appeal until 12 November. But when the Guardian went into a Sainsburys store to pay for a bar of chocolate with a round pound, the member of staff firmly shook her head, adding: No mate, its not legal tender in here. She handed our 1 back, but did point out that Poundland accepted them.
At Kings Cross station in London, Marks & Spencer, Pret a Manger, Caff Nero and giftshop Oliver Bonas refused to accept the old coins, and it was rejected in the self-service machine at WHSmith  and at two branches of Waitrose.
The Royal Mint and the Treasury were keen for retailers to stick to the deadline for a clean break from the round pound to avoid chaos being created by some shops continuing to accept them. Now that the deadline has passed, businesses have the right to stop accepting it and will not automatically be giving it as change for notes.
The new 12-sided 1 coin, which resembles the old threepenny bit, entered circulation in March and has new hi-tech security features to thwart counterfeiters.
Poundland stores have posters in their windows, saying: Breaking News  Spend your old 1 coins with us until 31st October! Sure enough, the man on the till accepted the Guardians old 1. 
 
 This footnote was added on 18 October 2017. Sainsburys got in touch after publication to clarify that customers could have spent old 1 coins in their stores until 16 October, meaning until 23.59 on Sunday 15 October. 
