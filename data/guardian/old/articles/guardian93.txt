__HTTP_STATUS_CODE
200
__TITLE

Illegal by Eoin Colfer and Andrew Donkin review  moving story of a child migrant

__AUTHORS

Sarah Donaldson
__TIMESTAMP

Tuesday 17 October 2017 04.00EDT

__ARTICLE
Writers Eoin Colfer and Andrew Donkin and illustrator Giovanni Rigano created the graphic novel adaptations of Colfers classic fantasy action series, Artemis Fowl. With Illegal, they turn to the here and now and have created a deeply affecting and thought=provoking account of the 21st-century refugee experience.
A kind of documentary fiction, the book weaves real stories of migration into the tale of Ebo, a spirited, motherless 12-year-old from Niger who follows his older brother from his hopeless village to the city of Agadez, where traffickers take them across the Sahara to Tripoli. Here, the boys again put their lives in the hands of nefarious men, who grant them space on a boat heading for to Italy. The story comes alive in the details: at his lowest ebb, Ebo lucks upon a packet of antiseptic wipes that he can trade, one by one, for food. One of his fellow voyagers, a Chelsea FC obsessive, jokes about becoming a World Service commentator (see how these boys are just like our own?).
The language  just dialogue and Ebos thoughts  is minimal. The drama is in the moody, naturalistic drawings, which depict the desert in dusty pastels and the night voyage in inky blues. Rigano is a master of facial expression: Ebos traffickers are truly intimidating and his looks of despair  and hope  heartbreaking. The boat scenes brilliantly evoke the patient fear of tightly packed ranks of people, and the terrible power of the ocean as it envelops those who cannot swim.
An epilogue reminds readers of the vertigo-inducing fact that, in 2015, one million people made this desperate journey across the Mediterranean. Colfer and Donkin dont shy away from the cost of these journeys and some children will find the story upsetting. But this is a book for adults too. These stories must be heard.
 Illegal by Eoin Colfer and Andrew Donkin is published by Hodder (14.99). To order a copy for 12.74 go to guardianbookshop.com or call 0330 333 6846. Free UK p&p over 10, online orders only. Phone orders min p&p of 1.99
