__HTTP_STATUS_CODE
200
__TITLE

'Theresa May's silence on women's issues is deafening'

__AUTHORS

Jessica Elgot Political reporter
__TIMESTAMP

Sunday 27 November 2016 11.23EST

__ARTICLE
She may be the UKs second female prime minister, but Theresa Mays silence on womens issues has been deafening, the leader of the Womens Equality party has said, criticising the government for its lack of action on pay transparency, childcare and social care.
Ahead of the partys first annual conference in Manchester, Sophie Walker said 2016 had been a testing time for women in politics, despite Mays elevation to No 10, adding that the election of Donald Trump meant politicians could no longer deny the scale of sexism in politics.
On the steps of Downing Street after she became prime minister, May said the gender pay gap was one of the key issues of economic inequality that she was determined to tackle. She has done nothing and it has been a huge disappointment, Walker said. 
There was this hope she had come into the job with an understanding of what needed to be done for equal opportunities. The Conservatives said they would axe section 78 of the Equalities Act and insist businesses move towards total pay transparency and we were delighted, but since then, nothing. 
The government said it would issue guidelines this autumn to companies and weve seen absolutely nothing. Theres nothing in the autumn statement about childcare or social care. The silence is deafening.
Walker, a former London mayoral candidate, called Trumps victory in the US a devastating blow and said her party expected to see a jump in membership figures in the weeks to come. 
At least now we dont have to debate if misogyny exists. We dont have to debate sexism, she said. Im so used to starting interviews with people who begin by saying, women are equal, come come, there isnt a problem. 
In some respects, now all that awfulness is exposed to the light, it is very clear what we are up against and there is a real push for an alternative.
Formed 18 months ago, Walker said many of the partys 65,000 members and registered supporters had expressed interested in standing in strategic seats, though the party has joined the Greens in a progressive alliance in Richmond Park to give its backing to the Lib Dems Sarah Olney, who is fighting the former Tory candidate Zac Goldsmith, now an independent. 
As well as stepping up its electoral strategy, Walker said the party would continue to challenge the idea that affordable childcare, fair pensions, ending violence against women are somehow something to be done once the more important stuff has happened.
The party has devised specialist training for candidates to prepare them for being in the public eye. It is very difficult for women to be public figures. They come under levels of abuse that are entirely disproportionate and are specific to our sex, Walker said. 
I am very conscious, doing this job, I am saying, come with me, be part of this, lets stand together and I am aware what a big ask that is because you are offering yourself up to abuse. But the more women there are in politics, the more we can look out for each other and normalise womens voices.
