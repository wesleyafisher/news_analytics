__HTTP_STATUS_CODE
200
__TITLE

Time running out for Brexit transition deal, Bank of England warns

__AUTHORS

Richard Partington
__TIMESTAMP

Tuesday 17 October 2017 12.27EDT

__ARTICLE
The governor of the Bank of England has warned that Britains financial industry is running out of time for ministers to strike a transitional deal with Brussels over leaving the European Union.
Mark Carney told MPs that without a transitional deal banks would need to start moving staff and setting up offices in the EU from as early as the first three months of next year, given it would take at least 18 months to make the changes.
Although he said the Bank expected a transitional deal to be agreed before the article 50 process completed in March 2019, Carney insisted Threadneedle Street was making plans for a hard exit as a precaution. 
Some European banks operating in London had failed to make sufficient plans due to an expectation that a deal would be struck and they would be allowed to remain operating branches in the UK from headquarters in the EU, he added. 
I think they now understand the latter is not necessarily going to be the case. Well have more to say about that in due course, he said, speaking at the Treasury select committee on Tuesday. 
Carney said it was important not to ask banks to make too many changes in a short space of time, for fears over the difficulty of shifting complex financial products such as derivatives, which are used for a variety of reasons including protecting businesses from interest rate changes. 
Carneys comments came as City firms were starting to lease office space in Frankfurt, Paris, Dublin and other EU financial centres amid concerns they may not be able to conduct business freely from London after Brexit.
Goldman Sachs has booked space in a tower under construction in Frankfurt and it has also reportedly reserved school places in the city in anticipation of moving staff. 
Uncertainty over the prospects of a transitional deal could put up to 75,000 finance jobs at risk as well as up to 10bn in UK tax revenue, according to lobby group The CityUK.
Under questioning from the Commons Treasury select committee, Carney said it would take 18-24 months for banks to take the initial steps required to retain access to trading in the EU. 
Theres a very little amount of time between now and the end of March 2019 to transition large complex financial institutions and activities, he added.
He said anything more ambitious than that, such as Brussels making demands for certain banking activities to take place solely within the EU after Brexit, would be very, very risky to do without a transitional deal.
It is absolutely in the interests of the EU27 to have a transition agreement, he said.
The Bank of Englands deputy governor, Sam Woods, also used a speech at Mansion House in central London this month to warn that delays in securing a transitional deal would limit the ability of such a deal to stem an exodus from the City.
