__HTTP_STATUS_CODE
200
__TITLE

Johanna Kontas WTA Finals hopes end with Kremlin Cup withdrawal

__AUTHORS
Press Association
__TIMESTAMP

Thursday 12 October 2017 05.39EDT

__ARTICLE
Johanna Kontas hopes of qualifying for the WTA Finals are over after she pulled out of the Kremlin Cup next week with an injury to her left foot.
The British No1 looked a near certainty to claim the final eighth spot for the showpiece event in Singapore this month until Caroline Garcias remarkable run. The Frenchwoman has taken advantage of Kontas poor form with back-to-back titles at the Wuhan and China Opens, two of the biggest events on the WTA Tour.
Konta could still have overtaken Garcia by reaching the final in Moscow but has not recovered from the foot problem that forced her to pull out of the tournament in Hong Kong this week.
It is the second year in a row that the 26-year-old has narrowly missed out on reaching the WTA Finals. Twelve months ago, she travelled to Singapore and took part in all the pre-tournament publicity only for Svetlana Kuznetsova to usurp her at the final moment by winning the Kremlin Cup.
Konta, who has lost her past five matches, must now decide whether to accept an alternate place in Singapore, which would see her take the spot of any player forced to pull out.
Garcia joins Simona Halep, Garbie Muguruza, Karolina Pliskova, Elina Svitolina, Venus Williams, Caroline Wozniacki and Jelena Ostapenko in the field.
