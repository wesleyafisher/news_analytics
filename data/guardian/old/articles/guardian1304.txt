__HTTP_STATUS_CODE
200
__TITLE

The world is going slow on coal, but misinformation is distorting the facts

__AUTHORS

Adam Morton
__TIMESTAMP

Sunday 15 October 2017 19.38EDT

__ARTICLE
This is a story about how misinformation can take hold. Its not always down to dishonesty. Sometimes its just a lack of time, a headline and the multiplying power of ideological certainty.
Last week, China announced it was stopping or postponing work on 151 coal plants that were either under, or earmarked for, construction.
Last month, India reported its national coal fleet on average ran at little more than 60% of its capacity  among other things, well below what is generally considered necessary for an individual generator to be financially viable.
Neither of these stories gained much of a foothold in the Australia media. But one story on global coal did: that 621 plants were being built across the planet.
The line was run in print, repeated on national radio and rippled out on social media among likeminded audiences. Some politicians and commentators claimed it showed it was strange, maybe even ridiculous, that MPs, financiers and energy companies said new coal power stations had no role to play in Australia.
But the figure is wrong. Way off, in fact. According to the most recent data, there are 267 coal stations under construction. More than 40% of those are not actually new ones, but expansions of existing generators.
A bit of background: the figure dates back to 19 June, when Nationals senator John Wacka Williams asked the parliamentary library to answer a few reasonable questions. How many coal plants are there in other countries? How many have been built recently or are being built? How many have closed? According to the parliamentary library analysis, he wanted the answers by 4pm the same day.
Fast forward to September, and the Australian ran a page one story quoting the analysis under the headline World building new coal plants faster than it shuts them. The Oz (correctly) reported that the library found 621 coal-fired power units were being built. This was mis-repeated by several people who dont accept that climate change is a present threat  including blogger and broadcaster Andrew Bolt and government backbench energy and environment committee chair Craig Kelly  as 621 plants being built.
In reality, coal power stations are usually made up of several units. Victorias Hazelwood, which shut in March, had eight. But the distinction mattered little once Bolt had provided the shareable online headline: New coal-fired power stations: World 621, Australia zero. Now understand?
While the stations/units confusion is relevant, it is not the main issue. The 621 is incorrect, however you cut it.
By the time it appeared in the media, it was more than a year out of date. How do we know? The guy behind the data that was the initial source for the library analysis says so.
The parliamentary library used as its source the Comstat Data Portal, a trade-focused African website not known for its energy expertise. As the library noted, the African portal copied its data from the Global Coal Plant Tracker, the widely respected database run by US-based anti-coal organisation CoalSwarm and used by the OECD, International Energy Agency and Bloomberg publications. But none of the players involved in spreading this story in Australia contacted CoalSwarm directly to check if the African database was accurate.
Its not. Ted Nace, the director of CoalSwarm, says there appeared to be numerous transcription errors. More significantly, the data on Comstat is out of date  from August last year. It did not reflect that new construction of coal plants plummeted in 2016 and 2017 following declines in construction in China and India.
More coal-fired capacity is still being built than closed each year, though the gap has narrowed significantly.
But, crucially, coal stations are not being used as much. The amount of electricity produced across the planet by burning coal has fallen each year since 2013.
A distinction needs to be kept in mind between capacity and electrical output, Nace says. Even though there are more power plants, the actual production of electricity from those plants  and likewise the amount of coal used worldwide  has fallen every year since 2013, with a small drop in 2014 and larger drops in 2015 and 2016.
The parliamentary assessment, and subsequent reporting, would have benefited from a closer glance at a report released in March by CoalSwarm, Greenpeace and the Sierra Club. Titled Boom and Bust 2017, it found an extraordinary 62% drop in new coal plant construction across the globe last year, and an 85% fall in new coal plant permits in China.
Analysis of CoalSwarms database shows that in July, construction was taking place at 300 plants globally. Of those, 183 were new power stations and 117 extensions of existing plants. But that number is changing rapidly.
As in so many things, the extraordinary story in coal comes from China. It still uses a stack of it, and is still building plenty of power stations. But according to a breakdown of the latest cancellation data announced last week by Simon Holmes  Court, senior adviser at the University of Melbournes energy transition hub, it stopped construction at 33 sites in the past three months alone.
It means that since July, the number of new coal stations being built in China has fallen from 103 to just 74. There has also been a slight decrease in the number being expanded, down to 46. The reason? A glut in the Chinese electricity market. The Institute for Energy Economics and Financial Analysis found its coal fleet ran at only 47.5% capacity last year.
India is the other big player, with 45 power stations under construction  19 new plants and 26 being expanded. While debate continues to rage over whether the Australian government should subsidise Adanis planned giant export coal mine in outback Queensland, existing Indian coal plants  including those owned by Adani  are struggling.
Among countries comparable to Australia in terms of development, Japan  which signalled it would make a significant investment in coal after shutting down its nuclear fleet following the Fukushima disaster six years ago  has 14 construction projects, many of them small by Australian standards.
Germany has been held up by lobby group the Minerals Council of Australia as an industrialised country investing in high efficiency, low emissions, or HELE, coal technology. It has one station under construction. Building work at what is known as Datteln 4 started a decade ago next month. After several delays, it is due to be commissioned next year.
It is the only coal station being built in western Europe. Britains Conservative party has promised to phase out coal by 2025, and Justin Trudeaus Canadian government by 2030. The two countries last week said they would work together to push other countries to join them. Despite Donald Trumps grand promises about reviving the coal industry, there are no new stations under construction in the US.
What does all this mean for Australia? In terms of the political debate, probably very little. Given the modern aversion to the persuasive power of evidence, misinformation will find a way.
But dont let yourself be kidded into thinking only local investors are leaving coal behind.
 Support our independent journalism and critical reporting on energy and the environment by giving a one-off or monthly contribution
