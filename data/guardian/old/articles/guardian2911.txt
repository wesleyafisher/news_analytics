__HTTP_STATUS_CODE
200
__TITLE

Leicesters Riyad Mahrez rediscovers ruthless streak to peg back West Brom

__AUTHORS

Ed Aarons at the King Power Stadium
__TIMESTAMP

Monday 16 October 2017 17.00EDT

__ARTICLE
Riyad Mahrez chose the perfect moment to finally rediscover his goalscoring touch as Craig Shakespeares faith in the Algerian was repaid with an equalising goal that ensured Leicester avoided the ignominy of three successive home Premier League defeats by the skin of their teeth.
That Shakespeare has now followed a certain Claudio Ranieri in not recording a victory in six league matches  a run which ended with the Italians sacking in February  is an unwanted statistic with his side in the bottom three, although the re-emergence of the player who did so much to inspire the famous title-winning season will be more welcome in these parts.
Having tried and failed to engineer a move away from the King Power Stadium in the summer, Mahrez has endured a torrid few months that also saw him and his Leicester team-mate Islam Slimani dropped by Algeria after some indifferent displays. But the way the pair combined after Slimani was brought off the bench to replace the 25m striker Kelechi Iheanacho  so ineffective on his full home Premier League debut  and cancel out Nacer Chadlis brilliant free-kick will give Shakespeare hope that the spirit of 2015-16 has not all evaporated just yet.
He didnt go away with Algeria, worked really hard at the training ground and gave me an issue over whether to start him at home, said Shakespeare of the goalscorer, the relief evident in his voice. He had an opportunity before that which he would have expected to score but he showed again he has that character to try and keep going and hes got his just rewards.
The pressure was on Leicester to show some signs of improvement after their poor run and Shakespeare responded by making three changes to the side that drew 0-0 at Bournemouth before the international break, with Jamie Vardy back from a groin injury and Mahrez recalled on the right flank. Tony Pulis also made three changes, with Boaz Myhill making his first league start for 18 months in place of Ben Foster, who injured his knee in an impromptu training session with his son in his back garden, as West Brom went in search of a first league victory since the second game of the season eight weeks ago.
But other than a frenetic opening in which Ahmed Hegazi  fresh from helping Egypt reach their first World Cup finals since being knocked out by England in 1990  impeded Vardy with a variety of limbs which eventually led to a deserved booking from Mike Dean, neither side carried much of a threat during a dull first half. A brilliant free-kick from Mahrez that just eluded Iheanacho was the closest Leicester had come to breaking the deadlock until Danny Simpsons effort from distance in the 35th minute was well saved by Myhill after a corner was only half cleared.
Given the lack of action, whether there had been any takers for the Shakespeare/Pulis half-and-half scarves on sale before the match was the main topic of discussion at half-time but Vardy ensured there was something to get the home fans off their seats within 30 seconds of the restart. A sloppy back pass from Hegazi was seized upon by the England striker and his touch took the ball beyond Myhill before the pair collided just outside the box.
After some treatment, the keeper was allowed to continue but afterwards Pulis seemed concerned by his options in goal. I dont know what happened with Bo, said the manager. I dont know if it was his hamstring, his back or his groin. Bo was in because Ben has hurt his knee, which is an enormous worry for us.
The free-kick given as a result of Myhills challenge on Vardy was headed against the outside of the post by Harry Maguire. A few minutes later, a brilliant back-heel from Vardy played in Marc Albrighton down the left and his pull-back presented Mahrez with a golden opportunity to score his first goal of the season, only for his shot to sail harmlessly over the crossbar.
But just as Leicester seemed to be building some momentum, a sloppy piece of control from Iheanacho on the edge of his own box gifted West Brom a chance to take the lead. The Nigerian brought down Grzegorz Krychowiak 30 yards out, and Chadlis superb curling free-kick totally deceived Kasper Schmeichel.
Shakespeare had to respond quickly and, to his credit, the decision to switch to a three-man defence as Slimani and Ben Chilwell were introduced from the bench paid dividends. Christian Fuchss deep cross to the back post was headed down by Slimani to Mahrez, who did the rest via a brilliant piece of chest control and a powerful right-foot finish.
The team is evolving, said Pulis, whose side moved back into the top half of the table with this point. Weve got better players in the team now than when I started. Leicester threw everything at us in the final 20 minutes and there were times when we could have picked them off but the final pass wasnt there. Weve got to do better than that but this a relentless league. We know where we are and what we are trying to do.
