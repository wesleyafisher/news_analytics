__HTTP_STATUS_CODE
200
__TITLE

Goldberg Variations, complete sessions CD review  Glenn Gould's obsession, meticulously assembled

__AUTHORS

Andrew Clements
__TIMESTAMP

Wednesday 13 September 2017 10.15EDT

__ARTICLE
Few pianists are more closely identified with a single work than Glenn Gould. Though over his short career he played and recorded a vast range of the keyboard repertoire  from William Byrd and Orlando Gibbons to Schoenberg and his pupils  it was the music of JS Bach, and the Goldberg Variations in particular, with which his name became indelibly linked. Gould made two studio recordings of the Goldberg, the second in 1981, just over a year before his death at the age of 50, but it was his earlier recording, made in 1955 and released the following year, that acquired legendary status, and defined him as an outstanding musician of the 20th century.
Gould famously gave no public performances after 1964, opting to work only in the studio. Since his death, his recorded legacy has maintained his stature. In the forefront of that have remained the two Goldberg recordings, which Columbia/CBS, and later Sony Classical, have re-released in many different editions. This latest concentrates exclusively on the 1955 recording, and is the most exhaustive yet, including every take from the sessions, presented variation by variation.
It constitutes detailed documentation of Goulds quest for his ideal Bach performance  his obsession with every detail, his insistence on getting the articulation of every semiquaver in every bar exactly as he imagined it, sometimes to the audible exasperation of the producer, Howard Scott. There are multiple takes of every variation, each subtly distinct in tempo and nuance, from which the definitive performance as it eventually appeared on LP was later spliced together. Taken together they also demonstrate the unwavering virtuosity of Goulds playing, and the startling clarity he seemed effortlessly able to bring to the densest contrapuntal textures.
Its sumptuously packaged. As well as the complete sessions, which take up five CDs, the set includes a disc containing Goulds 1981 interview on the Goldbergs with the critic Tim Page, and the final edited performance, both as a facsimile of the original 1956 LP, remastered and pressed on high-density vinyl, and as a CD. Theres also a fascinating coffee-table book, copiously illustrated, which includes all the original sound engineers tape sheets and transcripts of exchanges between Gould and his producer, printed in parallel with an Urtext edition of Bachs score.
Unless youre a Gould completist perhaps it isnt a must-have, but it is an extraordinary document, meticulously assembled, and as a bit of a Gould fanatic myself I found it totally compelling. In any case, one or other of Goulds Goldberg recordings, and preferably both, should be in every CD collection.
