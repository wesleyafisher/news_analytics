__HTTP_STATUS_CODE
200
__TITLE

Refugee MP Golriz Ghahraman on love, loathing and entering New Zealand politics

__AUTHORS

Eleanor Ainge Roy in Dunedin
__TIMESTAMP

Monday 16 October 2017 21.54EDT

__ARTICLE
When Golriz Ghahraman last week stepped into the Beehive, the executive wing of New Zealands parliament, along with her came her Twitter feed.
My Twitter feed is going into the national archive, it will be interesting for others to see what happens when for the first time a Middle-Eastern woman, a refugee, ran for parliament here, says Ghahraman. 
Both the support and the attacks. 
Iranian-born Ghahramans Twitter page is a fascinating testimony to love and loathing in 2017.
There are the vile, racist attacks on her background and heritage. The suggestion that a terrorist has been elected to parliament, that she may enforce sharia law, smuggle a bomb into the debating chamber, or push back against New Zealands socially progressive culture.
Then, to balance it, there is the love. The outpouring of support from elated New Zealanders. The words of encouragement from admiring Australians, worn down by years of that countrys Pacific solution, cheers and congratulations from Brexit escapees and Trump survivors. All rallying around a woman who has proven the value and potential of every refugee life.
For Ghahraman the social media abuse was a reminder of what brought her into politics, and the Green party, in the first place  a life-long interest in protecting human rights, whether they be her own, or those of persecuted strangers on the other side of the world.
In 2012 Ghahraman shifted back to New Zealand after working as a prosector for the United Nations Khmer Rouge tribunal in Cambodia.
I could see something had changed in NZ and it wasnt for the better, she remembers. We were having our child poverty statistics criticized by the United Nations. We were doing things like prospecting for coal in our national reserves. Democratic institutions were eroding. Things like that kind of started to catapult me into wanting to be much more politically active.
Ghahraman arrived in New Zealand at the age of nine after her family fled Iran. They sought asylum at the airport after arriving on a plane from Malaysia.
The country accepted the Ghahraman family and they settled in West Auckland. They lived off state benefits until they secured employment, and adapted to the socio-economically diverse and multicultural precinct of New Zealands largest city, where Golriz attended a high school whose student body was 70% Mari and Pacific Islanders.
It was in this South Pacific melting pot, says Ghahraman, that she acquired the confidence to study human rights law at Oxford University, and, later, to stand up in court representing the UN in tribunals prosecuting some of the worlds worst war criminals, including perpetrators of the Rwandan genocide.
My eternal gratefulness to New Zealand is that I got to grow up in a very diverse place. So the fact we didnt have anything was actually OK. I remember that freedom and getting to grow up with lots of different types of people; and that was just part of being Kiwi.
The final block of votes in this years New Zealand election were counted 14 days after voting closed, giving Ghahraman her seat in parliament after a nail-biting delay.
The Green leader, James Shaw, said he was thrilled to welcome his newest MP to the house, taking the Greens total number of seats to eight. In a time of increasingly divisive politics around the world, Golrizs election to our parliament sends a strong message about the kind of country New Zealand is, he said.
Ghahraman was 20 when the Tampa crisis erupted; and the Australian government refused to accept hundreds of asylum seekers rescued by a Norwegian sea captain off the northern coast of the lucky country.
New Zealands neighbour defiantly closed its borders, ushering in a draconian border protection policy that has been repeatedly condemned by the United Nations High Commissioner for Refugees.
Ghahraman watched as Prime Minister Helen Clark put up her hand to take 150 of the Tampa refugees. She read their case files while interning at Amnesty International and heard the cheers of welcome from locals who gathered at Auckland airport to greet the bedraggled survivors.
At that time, New Zealand really owned our solidarity with refugees and our lack of prejudice. I think comparative to Australia that still exists, but it has been eroded, says Ghahraman, citing the Tampa crisis as a galvanising moment in her career.
Since I was elected ... I have realised Kiwis have really rejoiced and sort of owned that idea that we can stand as a counterpoint to these other developments happening in the world. That politics of representation is becoming more and more important because of global political events. Whether its Brexit or Trump being elected.
For me to be able to enter the House of Representatives meant so many different things to so many different people.
The archiving of Ghahramans occasionally tumultuous journey to parliament means New Zealanders will be able to look back on the first refugee elected to represent them. And representation is everything, says Ghahraman, because without a voice you become a stereotype, and when youre a stereotype, its easier to dehumanise you. 
Post 9/11, I began to realise at least somewhere out there in the world I wasnt welcome and I wasnt trusted and I wasnt equal, says Ghahraman. It didnt matter that I felt Kiwi; it is the way people look at you. When they start blaming a whole group of people based on their race, or religion or ethnicity or nationality for something like terror, or any social ill.
That is the basis of all the atrocities Ive worked on. That is how it starts.
