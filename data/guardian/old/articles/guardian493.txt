__HTTP_STATUS_CODE
200
__TITLE

To Kill a Mockingbird by Harper Lee taken off Mississippi school reading list

__AUTHORS
Guardian staff and agencies
__TIMESTAMP

Saturday 14 October 2017 17.36EDT

__ARTICLE
To Kill a Mockingbird, Harper Lees classic novel about racism and the American south, has been removed from a junior-high reading list in a Mississippi school district because the language in the book makes people uncomfortable.
 The Sun Herald reported that administrators in Biloxi pulled the novel from the eighth-grade curriculum this week.
Kenny Holloway, vice-president of the Biloxi school board, told the newspaper: There were complaints about it. There is some language in the book that makes people uncomfortable, and we can teach the same lesson with other books. Its still in our library. But theyre going to use another book in the eighth-grade course.
A message on the Biloxi schools website said To Kill a Mockingbird teaches students that compassion and empathy do not depend upon race or education.
Published in 1960, Lees Pulitzer-winning novel deals with racial inequality in a small Alabama town, in the aftermath of an alleged rape of a white woman for which a black man is tried. It has sold more than 40m copies and it was made into a film in 1962, winning three Oscars.
An email to the Sun Herald from a concerned reader referred to the books use of the word nigger when it said the school boards decision was made mid-lesson plan.
The students will not be allowed to finish the reading of To Kill a Mockingbird, the email said  due to the use of the N word.
The newspaper quoted the reader as writing: I think it is one of the most disturbing examples of censorship I have ever heard, in that the themes in the story humanize all people regardless of their social status, education level, intellect, and of course, race. It would be difficult to find a time when it was more relevant than in days like these.
The Sun Herald reported that the school board superintendent, Arthur McMillan, did not answer any questions about the withdrawal. The book has been withdrawn from schools before, in 2016 in Virginia.
Lee died last year at the age of 89, after the discovery and controversial publication of a second novel, Go Set a Watchman, that describes events after those depicted in To Kill a Mockingbird. In June this year, the authors estate approved plans for a graphic novel version of the first book.
