__HTTP_STATUS_CODE
200
__TITLE

Kevin Durant spurns Trump as teams vow to fund Confederate statue removal

__AUTHORS

Tom Lutz
__TIMESTAMP

Thursday 17 August 2017 17.02EDT

__ARTICLE
Kevin Durant is the latest sports star to disassociate himself from Donald Trump.
The NBA finals MVP said he will not join his team-mates should the Golden State Warriors visit the White House to celebrate their championship win earlier this year. Nah, I wont do that, Durant told ESPN. I dont respect whos in office right now.
On Tuesday, the NBAs biggest star, LeBron James, took a swipe at Trump, describing him as a so-called president who has made hate fashionable. This week Trump has been sharply condemned for defending some of the far-right marchers involved in the recent violence in Charlottesville, saying: Not all of those people were neo-Nazis, not all of those people were white supremacists. 
On Saturday, a woman was killed after a car was deliberately driven into a crowd of anti-fascist protesters in Charlottesville.
Teams traditionally visit the White House after a championship victory, although the Warriors have yet to receive a formal invitation. There have been rumors Golden State will boycott the ceremony. The teams coach, Steve Kerr, has been critical of Trump in the past. 
I dont agree with what [Trump] agrees with, so my voice is going to be heard by not doing that, said Durant, who added he did not speak for his team. Thats just me personally, but if I know my guys well enough, theyll all agree with me.
Durant said he has no doubt that Trump is having a negative effect on race relations in the US. Hes definitely driving it, Durant said. I feel ever since hes got into office, or since he ran for the presidency, our country has been so divided and its not a coincidence. When [Barack] Obama was in office, things were looking up. We had so much hope in our communities where I come from because we had a black president, and that was a first.
So, to see that, and to be where we are now, it just felt like we took a turn for the worse, man. It all comes from who is in the administration. It comes from the top  leadership trickles down to the rest of us. So, you know, if we have someone in office that doesnt care about all people, then we wont go anywhere as a country. In my opinion, until we get him out of here, we wont see any progress.
The violence in Charlottesville came after white supremacists gathered to protest the removal of a statue of the Confederate general Robert E Lee. On Thursday, Trump decried the removal of such beautiful statues and monuments. Some professional sports team disagree: on Thursday, Tampa, Floridas teams  the Lightning (NHL), the Rays (MLB) and the Buccaneers (NFL)  said they would help fund the removal of a Confederate statue that stands in front of the local courthouse. 
A joint statement from us, @raysbaseball, & @tbbuccaneers regarding the movement to remove the Confederate monument from downtown Tampa. pic.twitter.com/U87JQsYXO4
Also on Thursday, another team made steps to break with its past. The Boston Red Sox principal owner, John Henry, said the team would lead efforts to rename Yawkey Way, on which Fenway Park stands. The street is named for Tom Yawkey, owner of the team from 1933 until his death in 1976. During his tenure, Yawkey earned a reputation for being one of the most racist figures in baseball. His beliefs were the main reason why the Red Sox were the last of the pre-expansion MLB clubs to accept black players. They did not have an African American player until calling up Pumpsie Green in 1959, 12 years after Jackie Robinson broke major league baseballs color line. 
The Red Sox dont control the naming or renaming of streets, Henry told the Boston Herald in an email. But for me, personally, the street name has always been a consistent reminder that it is our job to ensure the Red Sox are not just multi-cultural, but stand for as many of the right things in our community as we can  particularly in our African American community and in the Dominican community that has embraced us so fully.
Professional sports in the US have seen a number of protests over the past year. Colin Kaepernick and Michael Bennett are among the NFL players to have sat out the national anthem in protest at racial injustice in the US. Durant paid tribute to their efforts on Thursday. As far as whats going on in our country, for one as an athlete, you have to commend Colin Kaepernick, LeBron James, Carmelo Anthony, CP3 [Chris Paul], Dwyane Wade for starting that conversation last year. Russell Westbrook also said something ... A lot of guys with platforms have drove the conversation in a good direction. And whats going on in Charlottesville, that was unfathomable.
