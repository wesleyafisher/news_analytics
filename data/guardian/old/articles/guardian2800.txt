__HTTP_STATUS_CODE
200
__TITLE

We cannot afford to fund 'dementia tax' proposals, councils warn

__AUTHORS

Jessica Elgot
__TIMESTAMP

Tuesday 17 October 2017 01.00EDT

__ARTICLE
Conservative council leaders have warned that county councils cannot afford to be hit by a 308m rise in care home costs if controversial social care plans dubbed the dementia tax go ahead.
Tory-dominated shire councils have warned they cannot afford the extra burden of the manifesto proposal that would offer state support to people with assets of 100,000 or less  a sharp increase on the current 23,250.
The County Councils Network (CCN), which represents the 37 county councils, said new analysis showed raising the threshold would push far more people into state care than local authorities could fund under current budgets.
Colin Noble, the Conservative leader of Suffolk council, said the care home system was propped up by private fee payers, who could be paying up to twice as much as council clients. 
If more people qualified for state support  and care home providers received the lower council rates  many providers could be pushed towards bankruptcy, Noble said. 
Substantial reductions to social care budgets have left councils with little choice but to negotiate lower fees for their taxpayers money, but this means the system is being propped up by private fee payers, he said. This is clearly unsustainable, with many care home providers at a very real risk of collapse.
A typical private residential self-funder pays 797 a week for care, while the local authority typically pays 543, a 46% subsidy, according to the CCN analysis. 
Noble said local care providers were under acute pressure, and reforms which meant more people qualified for state support could inadvertently push care homes closer to financial crisis without additional funding from government to close the gap.
He said the additional cash for social care announced by the Treasury in March was not enough to tackle the issue. The government should use the autumn budget to inject further cash into the system: with counties funding black hole in social care alone projected to reach 1.6bn by 2021, we will not be in a position to raise fees [that councils pay to care providers].
Martin Tett, the Conservative leader of Buckinghamshire county council, said he shared the concerns. The principle that those who have the assets should contribute to their care is not unreasonable, but clearly if more people are qualifying for council-funded care, we in local government will regard that as a new burden and absolutely insist on the need for more funding, Tett told the Guardian.
Councils are already under substantial pressure on adult social care, particularly in the north, where there are more people eligible for state-funded care. 
If we get to a situation where the cross-subsidy of the care home sector by private residents is more imbalanced, that could destabilise the sector, he said.
The CCNs analysis, by independent consultants LaingBuisson, found the gap between private fees and council fees had reached 670m a year across all 37 county councils, which represent a total of 26m people.
The increase in the number of people who would qualify for state care under the proposals would cost county councils an additional 308m a year, the report suggested.
Noble said he did not deny the system was in need of reform. While limiting catastrophic care costs is important, the much more fundamental question is how we pay for it, he said. We need an honest and open conversation with the public, we must make sure that the promised social care green paper is not kicked into the long grass.
Pre-election plans for changes to social care were thought to have been dropped by the government after they were poorly received during the general election campaign, but a Conservative minister recently resurrected the idea of asking people to contribute more to the cost of care.
The social care minister, Jackie Doyle-Price, told a Conservative conference fringe meeting the party was still looking to reform the funding of social care and that peoples homes should not be seen as an asset to give to their offspring.
The government has indicated that an adult social care green paper is likely to be published next year. Doyle-Price said the government will be looking at the whole issue of caps and floors.
