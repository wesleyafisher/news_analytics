__HTTP_STATUS_CODE
200
__TITLE

Crossword roundup: dosh, dosh, loadsamoney

__AUTHORS

Alan Connor
__TIMESTAMP

Monday 9 October 2017 08.41EDT

__ARTICLE
Perhaps there exist some clues and themes somewhere in crosswording that are supportive of and enthusiastic for the 45th president of the United States. Let me know if you spot any. Meanwhile, the combined response from the Independents team is itself a combination of alarm and outright derision.
Exhibit A, from Crosophile ...
24d  The presidents no leader, a butt (4)[ presidents name missing its first letter (no leader) ][ TRUMP - T ]
... is a display of a RUMP, while in the following days exhibit B ...
19ac Trump starts to harass emigrants, sending them the greatest distance possible  (8)[ synonym for trump + first letters of (starts to) HARASS EMIGRANTS SENDING THEM ][ FART + HEST ]
... Eccles gets to FARTHEST via an obnoxious equivalence of Trump.
Heres a poignant image in a Telegraph clue that, if my assumed rota is correct, comes from the setter known locally as Pasquale: 
23ac Saint in ruin needing money (6)[ synonym for ruin, then (needing) slang term for money ][ MAR + TIN ]
TIN meaning cash comes from the 18th century when coins were made of a silver that became worn so heavily that the devices disappeared and the discs resembled little tin circles. Our next challenge concerns another slang term for pecuniaries, one of those with a tantalisingly obscure origin. In his book Words in Time and Place, David Crystal notes that:
... many suggestions have been made, such as a blend of DOLLAR + CASH, or a variant of DOSS, get a cheap bed for the night, with the meaning narrowed to the cost involved.
So, reader: how would you clue DOSH?
Many thanks for your clues for TAG. As usual, a short word prompted myriad inventive approaches, and some fittingly terse clues, such as Alberyalberys Name label, Peshwaris Hart loses top billing, JollySwagmans Greta Garbos nickname and Steverans abbaesque Its the name of the game.
The runners-up are Ixioneds ominous Label for men society forgot? and DameSweeneyEggblasts Its touch and go at first; the winner, also using the playground game as definition is GappyTooths beguiling Its strange, not odd. 
Kludos to Tooth; please leave this fortnights entries and your pick of the broadsheet cryptics (and American puzzles!) below.
Two anniversaries are of note, and it would be spoiling each to say any more. From these pages, the Observers Azed 2,363 is recommended once youve digested the special instructions. Impatient solvers are directed to Monks recent Financial Times puzzle, but really you should print off both PDFs.
