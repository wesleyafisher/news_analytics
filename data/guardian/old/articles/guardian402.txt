__HTTP_STATUS_CODE
200
__TITLE

New York Mets pitcher Noah Syndergaard roasted alive in Game of Thrones cameo

__AUTHORS

Guardian sport
__TIMESTAMP

Monday 7 August 2017 09.55EDT

__ARTICLE
When it comes to cameos, Game of Thrones may be typecasting. Last month, pop star Ed Sheeran played a musician. On Sunday, New York Mets pitcher pretty much played  a pitcher.
The baseball star was seen hurling a spear as part of the Lannister army, taking down a horse before being roasted alive by a dragon. Despite his grisly end, Syndergaard was a good call-up by Cerseis forces: he stands 6ft 6in and his fastball regularly tops 100mph.
.@Noahsyndergaard with the sweet spear toss on GoT pic.twitter.com/FKHtt4tXbr
Syndergaard, who in a confusing mix of fantasy universes is nicknamed Thor, filmed the scene during MLBs off-season. He is currently injured, and his Mets team are taking a beating similar to the Lannisters: despite a supposedly strong roster they are all but certain to miss the playoffs and their mascot recently got in trouble for showing fans the middle finger.
Take that you mean Dothraki #shottotheheart #andyouretoblamehttps://t.co/3ZERpVDwvm
Syndergaard is a long-term fan of the show and spoke of his delight at his role in April. I think its the greatest TV show of all time, so just to be able to say I was in Game of Thrones is an unbelievable feeling, he told Sports Illustrated in April.
The Mets did not fare much better than Syndergaard on Sunday night: they lost to the Los Angeles Dodgers 8-0. 
