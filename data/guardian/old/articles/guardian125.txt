__HTTP_STATUS_CODE
200
__TITLE

I wore a RompHim for a day. I probably wouldn't do that outside Sydney

__AUTHORS
Samus Jabour
__TIMESTAMP

Thursday 22 June 2017 14.00EDT

__ARTICLE
The Guardian had a meeting to discuss important things in the world (I imagine). The powers-that-be wanted a man to walk around for the day in a RompHim  a one-piece shorts and T-shirt ensemble for men  and write about it. They found themselves hitting a brick wall. No one in their right mind would do that, remarked a male journo in their right mind. Step forward my sister, Bridie Jabour: Ive got just the guy. And that is why Im writing this. Im Samus Jabour and I wore a RompHim for a day.
The RompHim was an idea conceived by some business students in the United States. As the legend goes, the boyz were sitting around blowin the froth off a couple when the conversation turned to mens fashion.
Everything was either too corporate, too fratty, too runway, or too basic, as stated by the Kickstarter website. So they set out to correct this minor glitch in mens fashion and came up with something that is all too much.
 The idea travelled around the world and materialised one Friday evening in Sydney with me preparing for a birthday party of about 15 guests. To set the scene: a burgundy onesie, sorry, RompHim, that was kinda furry, sleeveless and with a hoodie option. Turn heads and break hearts when you take your RompHim for a spin, Kickstarter tells you in the What is a RompHim? section of the website. Turning heads did occur but I was nearly breaking necks instead of hearts as the eyes would dart back for a second glance.
Dude, you remind me of that shit Flinstones movie with John Goodman
The first reaction was from my father: What the [expletive deleted] is that? he asked as I left the bathroom to find my shoes. Its a RompHim, Dad. He persisted: A what? A RompHim, all the cool kids are wearing them, apparently. I am pretty sure by the end of the night he wanted one for himself. Strange old man.
I didnt have long to settle in before the guests started to arrive for the dinner party. OMG YOU LOOK LIKE A TROLL! one exclaimed, reffering to the 1960s toy craze in the US. 
Dude, you remind me of that shit Flinstones movie with John Goodman, I was told multiple times. Like Bamm-Bamm or something. 
Hey man  go the Maroons, another mate said, referring to the Queensland rugby league team and not acknowledging at all that I was wearing a onesie. 
 Several people implored me to never wear it again and pleaded with me to take it off immediately. A guest who also happened to be a psychologist was psychoanalysing me and my reasons for wearing it (I couldnt say it was for an article for the Guardian) so I was bombarded with Freudian insights as to why I would do such a thing. 
It was good craic copping heat from a pretty diverse bunch of people. The RompHim, to its credit, was super comfortable with the zips at both ends making for easy access when it came time to go to the bathroom. 
Throughout the night, I never felt uncomfortable in the company although I copped a barrage of questions from just about everyone such as So, whats this about? and Please explain and JUST WHAT THE FUCK DO YOU THINK YOURE DOIN? 
I could comfortably fall into a conversation about the Guided by Voices tribute band, Guided by Guided by Voices, give myself to the chat and forget that I was wearing the onesie. This, I feel, would probably not have been been achieved if I had tackled the task in my regional hometown of Grafton (northern New South Wales just above the Big Banana and just below the Big Prawn and home to the South Grafton Rebels).
I love Grafton  the people, the area, the Rebels. But introduce something new, or try to, and it takes a while to process. In the meantime, questions will be blatantly asked. When I was 17 I must have been one of the first guys to wear skinny jeans around the place. I only had sisters to trade clothes with so I was at a loss. I started wearing their jeans whenever mine were misplaced. 
 That was not very well received.
To send in a RompHim to the Clocky on Friday night would send the city into meltdown. So would I wear it again or even encourage others to? No. 
