__HTTP_STATUS_CODE
200
__TITLE

Woman drowned two children after reporting fears of deportation, police say

__AUTHORS
Associated Press
__TIMESTAMP

Tuesday 17 October 2017 18.11EDT

__ARTICLE
A woman drowned her infant son and his five-year-old half-brother in a bathtub hours after she called police and told them she was worried about being deported, a Delaware police chief said on Tuesday.
Kula Pelima, a native of Liberia who has lived in the US for two decades, called 911 about 3.45am on Monday, fearing that her visa had lapsed, the Wilmington police chief, Robert Tracy, said at a news conference.
An officer visited her home and said local authorities were not going to arrest her. The officer saw the older boy during the visit and he was awake and well, the chief said. The officer gave Pelima an immigration hotline number to call.
Pelima called 911 again about 8.30am and said she had drowned her son, four-month-old Solomon Epelle, and five-year-old Alex Epelle, authorities said.
Pelima, 30, was charged with two counts of first-degree murder and was being held on $2m bond. It is not clear if she has a lawyer yet.
Immigration and Customs Enforcement (Ice) could not immediately provide any information on Pelimas status.
There was a strong odor of gas in her apartment, but it was not clear if the gas stove had been turned on intentionally, the chief said. The building was evacuated but no one was hurt by the fumes.
Theres no way of knowing what could have led to the deaths of these children, Tracy said, adding that the crime was shocking even to hardened officers.
Pelimas boyfriend, the father of both boys, is a native of Nigeria. He was taken into custody by immigration authorities in Pennsylvania on 14 September. The couple lived together in Wilmington, and she helped care for both children, the police chief said.
Victor Epelle, 38, was in the United States legally, but he had violated the terms of his status, Ice said. It was not immediately clear what he had done.
He was released from custody on Monday on humanitarian grounds after being notified of the deaths of his sons. 
