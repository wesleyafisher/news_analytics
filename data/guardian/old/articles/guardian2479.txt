__HTTP_STATUS_CODE
200
__TITLE

Have you spotted any anti-EU slogans?

__AUTHORS

Guardian readers
__TIMESTAMP

Tuesday 17 October 2017 03.15EDT

__ARTICLE
As Theresa May heads to Brussels for a diplomacy blitz to try to convince key EU figures to move negotiations along, the odd sign of frustration with the Brexit process has been spotted this side of the channel.
You can now buy Jean-Claude Juncker face masks for Guy Fawkes night  though, if you prefer, Theresa May and David Davis masks are also available. 
And this bumper slogan referencing an infamous Sun headline was also spotted on a vehicle heading smoothly across to one of our neighbouring EU countries.
If youve seen any examples of anti-EU products, signs, literature or other examples of frustration with the current signs of deadlock, please share them with us. Or if your area is, instead, festooned with pro-EU banners: please share those with us too.
You can share your photos and stories by clicking on the blue Contribute button on this article or fill out the form below. You can also use the Guardian app and search for GuardianWitness assignments.
You could always email us on guardian.witness@theguardian.com instead, and you can also message the Guardian on WhatsApp by adding the contact +44(0)7867825056.
