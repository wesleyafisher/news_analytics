__HTTP_STATUS_CODE
200
__TITLE

Season's eatings: red capsicum salsa

__AUTHORS
Words by 
Romy Ash, recipe by 
Sarah Trotter and Romy Ash, pictures by 
Lauren Bamford and styling by Sarah Trotter
__TIMESTAMP

Tuesday 9 February 2016 19.41EST

__ARTICLE
Capsicums can be eaten fresh, pickled, smoked, dried or roasted. From the nightshade family, native to the Americas, the large bell peppers or capsicums commonly come in traffic-light colours: red, orange/yellow and green.
The fruit of most species of capsicum contains capsaicin. This is a chemical that produces the hot pungency or spiciness  the burning sensation found in a chilli pepper. This is not a taste, rather it is a reaction. The capsaicin is not present in the seeds, as most people think, but in the membranes surrounding them; in the white bits and, to a lesser extent, in the flesh. 
Most mammals can taste the capsaicin, finding it either unpleasant or delicious depending on perspective, whereas birds are unaffected. The bright colours of the fruit attract the birds, who then devour them and distribute the seeds. These fruits come in a huge number of varieties, including paprika, jalapeno, habanero and the many varieties of chilli. The capsicum itself does not contain capsaicin. 
At the market, choose a squeaky skinned capsicum. The flesh should be firm and clear, without any soft spots. 
Some kind of alchemy happens in the roasting of a ripe red capsicum. The smoky flavours infuse the sweet red flesh that softens into a delicious slippery mess. You might as well roast more capsicums than this recipe calls for as they make such a delicious addition to a pasta sauce or can be used in myriad other ways. They keep in the fridge, submerged in oil, for more than a week. 
Start a salsa with fresh and in-season ingredients and youll be ahead. Once you have that, the work of a good salsa is only in the chopping, which can take a little time. Also, its always best to use whole spices and toast and grind them yourself  it makes a great deal of difference to their pungency. They will be fresh and beautifully fragrant. Use an electric spice grinder, or the rough grind of a mortar and pestle will be fine. After chopping the fresh ingredients, and toasting and grinding the spices, its simply a matter of assembling the salsa with a gentle stir and youre ready. 
This salsa should be a small but important part of a Mexican feast. Serve it alongside a salsa verde and guacamole with tortilla, as part of a taco. Or as we have, serve with corn chips or toasted tortilla strips as an assemble-yourself-tostada  with cold beers, on a summer evening.
2 red capsicum, roasted, deseeded, and the skin removed3 large tomatoes, deseeded1 red capsicum, deseeded1 red onion, chopped finely cup of fresh coriander, washed, picked and chopped roughly1 tbsp fennel seeds, toasted and ground1 tbsp coriander seeds, toasted and groundJuice of 1 lemon, or juice of 2 limesSea salt, to taste3 tbsp olive oil
To roast the capsicums, put capsicums on a roasting tray in a hot oven (200C) and roast until their skin is black and their form has collapsed. This should take about 40 minutes.
Once their skin is black, remove from the oven and place the roasted capsicums in a clean plastic bag to sweat. This sweating separates the skin from the flesh. Once the capsicums have cooled, remove from the bag and discard the skin and seeds. Chop them and the fresh ingredients, and assemble the salsa with the spices, lemon juice, salt and oil. The salsa gets better after sitting for a little while. 
Serves 8 as a starter with corn chips or toasted tortilla strips. 
