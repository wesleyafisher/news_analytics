__HTTP_STATUS_CODE
200
__TITLE

Saigon Calling by Marcelino Truong review  an amazing achievement

__AUTHORS

Rachel Cooke
__TIMESTAMP

Tuesday 17 October 2017 02.30EDT

__ARTICLE
Marcelino Truongs complex, finely judged and utterly riveting memoir is a sequel to his 2016 book, Such a Lovely Little War, in which he told the story, as seen through his boyhood eyes, of Saigon at the beginning of the 60s, when his diplomat father, Khanh, was the personal interpreter of the prime minister, Ngo Dinh Diem. This volume, however, is set not in Saigon, but in London, where Khanh has been appointed to a high-ranking job at the Vietnamese embassy  though the grim news from home is, of course, never any further away than the next BBC bulletin. In South Vietnam, General Khanh, widely seen as an American puppet, has taken over as the countrys leader following the assassination of President Diem. In the communist north, the Viet Cong are growing ever stronger and more vicious, with the result that President Johnson is shortly to put US troops on the ground.
For all the citys outward stiffness, the young Marcel likes almost-swinging London. He and his three siblings adore Doctor Who and Top of the Pops; its no secret that he longs to own a pair of the new pointed shoes and a Beatles-style military jacket (and failing that, a Dinky toy Batmobile). But life isnt always easy. Lonely in Wimbledon, his French mother, Yvette, continues to struggle with bipolar disorder, relying on Valium to get her through the dank, British days. Meanwhile, his father, worried sick for his parents trapped in Saigon, must deal with the growing realisation that his dreams for democracy in Vietnam are likely, now, to come to nothing. Aware of what lies ahead, he quits his job at the embassy, swapping it for less lucrative work as a translator for Reuters.
And then there is the attitude of British leftists (and pretty much all young people) to the war in Vietnam. What to make of it? Marcel shares, to a degree, their anti-Americanism. But what he cant understand is why the young, bearded types who demonstrate in the streets are seemingly so supportive of the communists. As Vietnamese, we suspected our Viet Cong adversary was a wolf in sheeps clothing, he notes quietly, beside a frame of a lipsticked protester shouting: Ho Chi Minh!
What an amazingly capacious comic book Saigon Calling is (like Marcels beloved Tardis, its appearance is deceptive). Somehow, Truong, a successful illustrator who now lives and works in France, manages to provide the reader with much of the historical and political background to the quagmire that was the Vietnam war without ever derailing the family story that lies at the heart of his book. On to every page he sneaks so many evocative little details, from the Action Man tableaux he and his brother photograph on their Brownie Starflash, to his sisters adventures as an aspiring hippy at Durham University, to the dreamy teacher at his Lyce who is a confirmed Maoist. It is an amazing achievement: a familiar story (Vietnam) told from (what was to me) an entirely new point of view, with great wit as well as pathos.
 Saigon Calling by Marcelino Truong is published by Arsenal Pulp Press (22.99). To order a copy for 19.54 go to guardianbookshop.com or call 0330 333 6846. Free UK p&p over 10, online orders only. Phone orders min p&p of 1.99
