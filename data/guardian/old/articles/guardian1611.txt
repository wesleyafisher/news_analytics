__HTTP_STATUS_CODE
200
__TITLE

Sean Hughes's greatest TV moments: from DIY nursing to Finbar the talking shark

__AUTHORS

Stuart Heritage
__TIMESTAMP

Monday 16 October 2017 11.34EDT

__ARTICLE
Sean Hughes, who died today, was an accomplished writer, film actor and standup comedian  but he was also pretty skilled as a talking shark. In fact, his various TV appearances, from fourth wall-smashing sitcoms to a regular spot on the first incarnation of Never Mind the Buzzcocks, are what he was perhaps best known for. Here are some of the greatest moments from his 25 years on screen, all shot through with an underlying spark of dishevelled Seanness 
Until the end, Hughes finest television work remained his first. Heavily  some would say shamelessly  inspired by Its Garry Shandlings Show, Seans Show was a transparent deconstruction of the sitcom form. Fourth walls were smashed, crew members were dragged on screen and the general artifice of the genre was hauled to the surface as often as possible. Not only did Seans Show function primarily as a Shandling-style series of monologues, but it also had a Shandling-style belter of a theme tune. Full episodes are available on YouTube. You could do much worse than spend an afternoon revisiting them.
After wrapping up Seans Show, Hughes defected to the BBC to make a series of brief films where he travelled the country talking to people. Markedly less manic than Seans Show, it was nevertheless determined to deconstruct itself whenever possible. It was very much of its time. A case in point: the first episode began with the Neighbours theme tune, launched into a failed Carpool Karaoke prototype with The Wonder Stuff and then swerved into a Bob Mortimer interview via a strange encounter with Carter the Unstoppable Sex Machine.
Between 1996 and 2002, Hughes served as a regular team captain on BBC 2s now defunct music panel show Never Mind the Buzzcocks. His role largely involved acting as a sleepy counterpoint to Mark Lamarrs grump and Phill Jupituss shtick, which positioned him to best react to the toe-curling antics of his pop star guests. A perfect example of this was his sly bafflement when Boy George attempted to hijack the show for his own means. Hughes also left Buzzcocks long before anyone else, which, if nothing else, demonstrates a keen sense of foresight.
Not even Hughes was immune to the odd slab of bill-paying gruntwork. This is why, in 2007, he appeared on a Channel 5 documentary entitled So You Think You Can Nurse. Here he  along with the equally unqualified Janet Street-Porter and Gail Porter  got sent to a hospital to pose as a nurse for some reason. Although it may arguably rank as the worst idea in television history  nobody can truly say they ever needed to see Janet Street-Porter scream Go on! Go on! at the crowning head of a newborn  Hughes still managed to impress his superiors by demonstrating an enviable bedside manner. NB: I only know that So You Think You Can Nurse exists because Sean reposted it on his own YouTube channel.
If you happened to have small children during the first decade of this century, you may well know Sean Hughes as Finbar the Mighty Shark in the bath-based CBeebies show Rubbadubbers. Although the majority of other voices were performed by John Gordon Sinclair, Hughes still managed to stand out, largely because he always sounded ever so slightly distracted. Rubbadubbers deserves a place on this list for one reason and one reason only; like many other Sean Hughes vehicles, it has a very brilliant theme tune.
