__HTTP_STATUS_CODE
200
__TITLE

MLB playoffs: Yankees come back from 2-0 series deficit to beat Indians

__AUTHORS
Associated Press
__TIMESTAMP

Wednesday 11 October 2017 23.56EDT

__ARTICLE
Didi Gregorius, following in the October footprints left by Derek Jeter, homered twice off Corey Kluber as the New York Yankees beat the Cleveland Indians 5-2 in Game 5 on Wednesday night to complete their comeback from a 2-0 deficit in the Division Series and dethrone the AL champions.
These Yankees staved off elimination for the fourth time in this postseason and advanced to play the Houston Astros in the AL Championship Series starting on Friday at Minute Maid Park.
.@Yankees shortstops coming up big in October. Its a thing. #ALDS pic.twitter.com/UopO7zJiJ0
The AL West champion Astros, led by 5ft 6in second-base dynamo and MVP candidate Jose Altuve, went 5-2 against the wild-card winners this season.
After winning twice in New York, the Yankees  with little offensive help from rookie star Aaron Judge  came into Progressive Field and finished off the Indians, who won 102 games during the regular season, ripped off a historic 22-game streak and were favored to get back to the World Series after losing in seven games a year ago to the Chicago Cubs.
Clevelands title drought turns 70 next year  baseballs longest dry spell.
The Indians closed to 3-2 in the fifth against starter CC Sabathia before David Robertson pitched a hitless spell for the win. Yankees closer Aroldis Chapman, who faced Cleveland in last years spine-tingling World Series and signed an $86m free agent contract in December, worked two innings for the save. Chapman went to the mound with a three-run lead in the ninth after Brett Gardner battled Cody Allen for 12 pitches before hitting an RBI single, with New Yorks fifth run scoring when Todd Frazier raced home on right fielder Jay Bruces throwing error.
Gardners gritty at-bat was symbolic of these Yankees. They wouldnt give in.
These baby Bronx Bombers became the 10th team to overcome a 2-0 deficit to win a best-of-five playoff series. New York also did it in 2001, rallying to beat Oakland  a series remembered for Jeters backhand flip to home plate.
Gregorius, who took over at shortstop following Jeters retirement after the 2014 season, hit a solo homer in the first off Kluber and added a two-run shot in the third off Clevelands ace, who didnt look like himself during either start in this series.
One win shy of a Series title last year, the Indians had only one goal in mind in 2017. They came up short again, and have now lost six consecutive games with a chance to clinch a postseason series dating to last years World Series, when they squandered a 3-1 lead to the Chicago Cubs. Cleveland is the first team in history to blow a two-game series lead in consecutive postseasons.
