__HTTP_STATUS_CODE
200
__TITLE

Azed crossword 2,353

__AUTHORS

__TIMESTAMP

Saturday 15 July 2017 19.01EDT

__ARTICLE
Special instructions
The Chambers Dictionary (2014) is recommended.
Prize rules
25 in book tokens for the first three correct solutions opened. Solutions postmarked no later than Saturday to Azed No. 2,353, The Observer, 90 York Way, London N1 9GU
