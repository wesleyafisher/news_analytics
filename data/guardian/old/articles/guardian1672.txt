__HTTP_STATUS_CODE
200
__TITLE

Cant be bothered to make stock? Here are the ways to keep cooking simple

__AUTHORS

Felicity Cloake
__TIMESTAMP

Monday 9 October 2017 10.00EDT

__ARTICLE
Top chef in stock shock! Tom Kerridges admission that he uses stock cubes at home raised afew well-manicured eyebrows at the Cheltenham literary festival last weekend the accepted wisdom being that any cook worth their Maldon sea salt has afreezer full of neatly labelled, lovingly homemade stuff. Indeed, Ive probably implied this in the past myself, and its abig fat lie; most bones in my house go to the dog in one form or another, and with freezer space at apremium, ice-cream is always going to be apriority there. (And no, thats not homemadeeither.)
This is not to say all ready-made stock is made equal Diana Henry eschews horrible cubes in favour of liquid concentrates, and aquick Twitter survey reveals the Michelin-garlanded Marco Pierre White is not alone in his devotion to jellied stock pots, while Ifavour organic cubes (wont someone think of thechickens?). Whatever your preference, the very fact you have one suggests that you dont spend every Sunday evening boiling up the bones of the lunchtime roast. Frankly, for many of us, life is just tooshort.
The same goes for dried beans and pulses: they may be far cheaper, but the snooty insistence that they taste better too is, in most cases, codswallop. Though theyre ruinously expensive, Ive never tasted afiner chickpea than the buttery Spanish kind sold in jars in fancy delicatessens; and if theyre good enough for Anissa Helou, theyre good enough for me. Canned tomatoes (the king of tinned goods according to Hugh Fearnley-Whittingstall) are Britains best bet for the next six months, and Ben Tish surely isnt the only chef to enjoy chicken nuggets with Hellmans mayo even if hes the only one to admitit.
Frozen pastry tends to be very decent, especially all-butter puff and filo (even Mary Berry buys filo), and after failing to get through enough sourdough to keep my starter alive, Ive nobly decided to support my local bakery instead. Croissants fall similarly into the why-bother-unless-you-live-miles-from-anywhere? category (and, even then, Ive heard good things about the Lidl frozen variety), and after slaving over aperfect crumpet recipe, if Im honest, Im still more likely to pop over the road for theCo-op buttermilkversions.
Theres no shame in ready-made meringues just add ajar of boozy fruit for instant gratification but my favourite cheat of them all is custard powder. You can turn your nose up all you want, but Ill take asturdy jug of Birds over adelicate drizzle of creme anglaise any day. Michelin inspectors, Im home andwaiting.
