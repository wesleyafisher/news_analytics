__HTTP_STATUS_CODE
200
__TITLE

How to grow great brassicas  from brussels sprouts to cabbages

__AUTHORS

Alys Fowler
__TIMESTAMP

Saturday 14 October 2017 06.00EDT

__ARTICLE
If I have a true love, it must be the genus, brassica. I didnt expect to fall so hard. Its hardly a glamorous one: its members are wide and varied, and often pungent  think of the sulphurous tang of boiled cabbage or the whiff of fermented sauerkraut. But I find that when I look upon abrassica I see the most beautiful of vegetables. Ilove how they look nestled among the flowers of my garden; I regularly make vases of kales, and marvel at the packed interiors of cabbages.
The wild cabbage, Brassica oleracea, is a diffuse and polymorphic species, which is another way of saying it is good at self-love. A long time ago we took one wild and wayward species, and selected and selected and selected until a wildling became a kale, became a cabbage, became broccoli or cauliflower or a brussels sprout. For brassica nerds, there are eight major cultivar groups.
Depending on your taste buds youre salivating or are about to turn the page; this is because you can taste the chemical compound, phenylthiocarbamide or PTC, which brassicas are packed with. If you can taste it, youll find the genus, particularly brussels sprouts, bitter; if you cant, you really dont get the aversion. Children can detect PTC much more readily, which is why the poor sprout endlessly rolls around a plate while sweeter-tasting peas are devoured.
Regardless of your tastes, brassicas make up the mainstay of our winter eating, so its worth paying attention now to those you planted earlier in the year, particularly if you want to brandish your own brussels and braise your own cabbages on Christmas Day. Brussels sprouts will start to ripen from now until early spring. Pick the lowest sprouts first as they ripen from bottom to top and dont forget to eat the delicious sprout tops, which are like tender small cabbages. Eat them last, as they are protecting the sprouts below. Cabbage heads are ready when they feel firm. Some cultivars of cabbage bolt quickly after maturing, others will stand for months. The trick is the name: Tundra, January King, Noelle, Siberia, are all long-standing winter types.
Wild brassicas are easy-going and hardy; their highly bred cultivated offspring are easily upset and prone to blow out  those much-desired tight heads of brussels or cabbages grow loose as the plants go straight to flowering. They produce superfine root hairs to take in nitrogen and if the plant rocks in the wind, the root hairs are damaged and they blow open their heads. Staking brussels is essential on anything but the most sheltered of sites. Earthing up around stems of brussels and heavy-headed cabbages helps to stop rocking. Feed them bacteria-rich comfrey or nettle tea (from rotted-down leaves), or mulch with nitrogen-rich grass clippings. Kales are closer to their wild relatives and tend to be much tougher about these things.
