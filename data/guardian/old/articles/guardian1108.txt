__HTTP_STATUS_CODE
200
__TITLE

London mayor launches unprecedented inquiry into foreign property ownership

__AUTHORS

Matthew Taylor in London and 
Tom Phillips in Hong Kong
__TIMESTAMP

Friday 30 September 2016 01.42EDT

__ARTICLE
London mayor Sadiq Khan is to launch the UKs most comprehensive inquiry into the impact of foreign investment flooding Londons housing market, amid growing fears about the scale of gentrification and rising housing costs in the capital.
Khan said there are real concerns about the surge in the number of homes being bought by overseas investors, adding that the inquiry would map the scale of the problem for the first time.
Its clear we need to better understand the different roles that overseas money plays in Londons housing market, the scale of whats going on, and what action we can take to support development and help Londoners find a home, Khan told the Guardian.
Thats why we are commissioning the most thorough research on this matter ever undertaken in Britain  the biggest look of its kind at this issue  so we can figure out exactly what can be done.
Earlier this year, the Guardian revealed how a 50-storey block of 214 luxury apartments by the river Thames in Vauxhall was more than 60% owned by foreign buyers. In one of the starkest examples of the impact of foreign investment, it found that a quarter of the flats were held by companies in secretive offshore tax havens, and many were unoccupied.
In China, experts predict the current scale of global investment in UK property could rise significantly over the next decade, with a new wave of middle-class investors from mainland China quadrupling the amount of money flowing annually into foreign real estate  including the UK  to $200bn (150bn) in the next 10 years.
Charles Pittar, chief executive of Juwai.com, a website that aims to pair Chinese investors with property developers overseas, said he expected a major jump in investors looking for a return in Britain, adding: The UK market, particularly post-Brexit, is really picking up.
Our thesis  and this is supported by quite a lot of evidence  is that in many ways the international Chinese investment journey is probably just starting  The exciting thing about China is that there are 168 cities with more than a million people. So this is just such a huge market.
That view is supported by Victor Li, a director of international project marketing for the US real estate giant CBRE, who also predicts a spike in investment in British homes over the next decade.
I think it is just beginning, said Li, predicting that only 3% of the potential investors in overseas property had so far been located across mainland China: It is a big market, and they are getting wealthier and wealthier.
Critics say the influx of foreign investors is contributing to a spiralling housing crisis in the capital. Earlier this week, it emerged that the number of thirtysomethings leaving London has leapt in recent years, as high housing costs have forced people to move out.
Overseas buyers are also increasingly focusing on towns and cities outside the UK capital  with Manchester, Liverpool and Birmingham all identified as hotspots as buyers try to get more for their money while avoiding new stamp duty rules. Property in Londons outer suburbs and even satellite towns such as Slough is being marketed in Hong Kong to potential Chinese buyers. 
Foreign investment has helped drive a fresh property building boom around the UK. Liverpool has received millions of pounds of overseas investment in housing and property in the past five years, including a 200m New Chinatown development that is under construction and is being heavily marketed in China. Earlier this year, Sheffield announced a multibillion pound deal with a Chinese construction firm that would generate four or five city-centre projects over the next three years and create hundreds if not thousands of jobs in south Yorkshire.
The Chinese are the biggest buyers of new-build residential accommodation globally, with the Singaporeans second and the British fourth, according to international property agents Knight Frank.
Khans inquiry will focus on the scale and impact of different types of overseas investment in London. It will examine how foreign cash has changed the housing market  from exclusive high-cost accommodation to middle- and low-cost homes  in different parts of London, and explore how other international cities are tackling the problem.
Khan said: We welcome investment from around the world in building new homes, including those for first-time buyers. At the same time, as more and more Londoners struggle to get on the property ladder, there are real concerns about the prospect of a surge in the number of homes being bought by overseas investors.
One key aim of the research will be shining a light on who is investing and where the money originates from.
Khan said: We urgently need more transparency around overseas money invested in London property. Londoners need reassuring that dirty money isnt flooding into our property market, and ministers must now make all property ownership in London transparent so we can see exactly who owns what.
But some housing market commentators warn it would be a mistake to focus only on foreign investment when tackling Londons housing crisis. Yolande Barnes, director of Savills world research department, said foreign buyers accounted for only 7% of property purchased across Greater London, although that figure is likely to be higher in inner-London hotspots. And she said that investment had helped bring forward nearly all the affordable homes built in London since the 2008 financial downturn.
Foreign buyers are often the focus in discussions about the housing crisis, but really they are only one element in an incredibly complicated picture. Without them investing in properties at the top end, we would not have been able to fund very much social or affordable housing since the financial crash.
Barnes said the real issue was the price and scarcity of land available for development in the capital.
Like any major world city, the issue in London centres around the fact that there is not enough of the most popular bits of the city to go around. Inner London land rarely comes on to the market, and it only makes sense for people to release land and turn it into residential units if they are going to get a suitable return  which normally means high-end luxury developments.
However, a recent study by academics at Londons Goldsmiths University found that the influx of cash into the capitals luxury housing market from the global super-rich was having a wider impact on gentrification across the city.
It discovered that foreign investment at the top end had pushed Londons traditional elite residents from their wealthy enclaves in places such as Mayfair, Chelsea and Hampstead, and created a trickle down effect  raising prices beyond the reach of most people in previously cheaper London neighbourhoods.
Are you experiencing or resisting gentrification in your city? Share your stories in the comments below, through our dedicated callout, or on Twitter using #GlobalGentrification.
Producing in-depth, thoughtful, well-reported journalism is difficult and expensive  but supporting us isnt. If you value the the Guardians coverage of the global housing crisis, please help to fund our journalism by becoming a supporter

