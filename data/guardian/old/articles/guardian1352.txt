__HTTP_STATUS_CODE
200
__TITLE

Crossword blog: when quick crosswords get cryptic

__AUTHORS

Alan Connor
__TIMESTAMP

Monday 3 July 2017 06.18EDT

__ARTICLE
Ill be quixotic to avoid spoilers, but today seems quite appropriate for this question: is this a place to discuss quick crosswords?
Take the Timess concise puzzle, which has been in the news. Devotees of that puzzle will have noticed, over time, that it is now never merely a set of definitions. One day, you might spot that some of the rows of the completed grid contain two-word phrases. Another day, perhaps, there are some symmetrically arranged Ks  as has happened in that paper and elsewhere. 
Every so often, the theme is more explicit. Heres a selection of down clues from Times2 Concise 7,358, published last month:
1. Large cat (5)3. Cat (4)6. Large cat (7)7. Large cat (7)12. Large cat (7)13. Cat (7)19. Cross between 21 down and 1 down (5)21. Large cat (4)
And heres the peculiar thing. The more obvious a setter makes his or her special feature, the more likely it is to be mistaken, not for a feature, but for a bug. The Mirror reported on the puzzle like this:
Clearly there was a malfunction in the printing of yesterdays Times crossword, because it had many more people than usual scratching their heads.
Imagine how much head-scratching would be caused by the revelation that every single day, the Timess second section publishes a puzzle with just such an error, some of them disguised with Bletchley-like levels of ingenuity (incidentally, the crosswords-in-D-Day story has reappeared this week in the Washington Post).
Ive yet to work out when this Times tradition began: there is a long period in which, as far as I can see, there are no hidden extras, and a transition phase when special tricks appeared now and again. If you have any intelligence on this, please do share. 
Because for me, the answer to the question at the top is: yes. If a quick crossword tries out tricks where, say, all the four-letter answers are anagrams of each other, that is surely wordplay. And if the wordplay might help the solver to get some answers where the definitional clues have not provided clarity, then youre really talking about cryptic crosswording in disguise.
Over to you.
