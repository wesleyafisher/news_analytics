__HTTP_STATUS_CODE
200
__TITLE

Thomasina Miers quick and easy recipe for blackberry and pecan tart

__AUTHORS

Thomasina Miers
__TIMESTAMP

Friday 15 September 2017 12.00EDT

__ARTICLE
When I entered MasterChef way back in 2005, it was as if I had thrown down the gauntlet to my father. Ever since, the more Ive discovered and cooked exotic ingredients, the more he has, too. I remember once going through his kitchen cupboards on the off-chance that he had a bottle of pomegranate molasses, only to be asked, rather snootily, why I didnt make my own. Well, not all of us have the time to make everything from scratch, but when it comes to pastry, my father and I see eye to eye: once youve made shortcrust a few times, its a marvel of speed and efficiency, and the results are much more memorable than shop-bought substitutes. Flaky, buttery, crisp pastry is as much of a treat as any tart filling will ever be, though, irritatingly, my father still makes the best pastry I have tasted, and thats including my chef friends. But stick to this recipe and I think you will, too.
The frangipane here is very moist, because blackberries are just so juicy. You can use gluten-free flour, if you want, but the results wont be quite as light. Serves eight to 10.
For the pastry190g plain flour, plus extra for dusting30g icing sugarSalt100g chilled butter, chopped, plus extra for greasing the tin1 egg, separated, the white lightly beaten with a fork
For the frangipane200g pecans200g softened butter100g soft brown sugar 100g golden caster sugar3 eggs25g plain flour tsp baking powder250g blackberries
To make the pastry, blitz the flour, icing sugar, a pinch of salt and the butter in a food processor for 20-30 seconds. Add the egg yolk, blitz again until incorporated, then add just enough egg white to bind the mix together (reserve the rest). Roll the dough into a ball, flatten slightly, wrap in clingfilm and leave to rest in the fridge for half an hour while you gather together the frangipane ingredients and read the paper.
Once the pastry is chilled, lightly butter a loose-bottomed 25cm tart tin and coarsely grate in the pastry. Press evenly across the base and up the sides with your fingers, sprinkle with a little flour and use a small glass to roll it flat. Prick the base all over with a fork, then freeze for 20 minutes (this will stop the pastry shrinking too much in the oven).
Heat the oven to 220C/425F/gas mark 7. Blitz the pecans in a food processor until fine. Beat the butter and sugars for the frangipane in the food processor until pale, then beat in the eggs one at a time, fully incorporating each one before adding the next. Add the flour, baking powder, a pinch of salt and the ground pecans, and pulse until just combined.
Remove the pastry from the freezer, cover with baking paper and some baking beans (or rice), then bake for 10 minutes. Remove the beans and paper, brush the base with the remaining egg white and return to the oven for five to 10 minutes, until the pastry is golden brown. Remove and leave to cool.
Turn down the oven to 180C/350F/gas mark 4, then spoon the frangipane into the cooled case and smooth the top with the back of a spoon. Arrange the blackberries on top in a neat pattern (I like concentric circles), then bake for 40-45 minutes, until the frangipane is set and the tart is deep golden all over.
Leave the tart to cool completely, then remove the tart ring, cut into slices and serve with creme fraiche, cream or Greek yoghurt.
This pastry is a corker, but if you are short on time, buy an all-butter shortcrust instead. Individual tarts bake in half the time, although you will spend more time on shaping the pastry cases. If you are in a savoury mood, the same pastry recipe, minus the sugar and with a generous few pinches of salt, will have similarly great results for, say, a gruyre and leek filling. Meanwhile, a dash of blackberry liqueur would not go amiss in the frangipane, should the mood take you.
