__HTTP_STATUS_CODE
200
__TITLE

Julian Rosefeldt's Manifesto review  13 Cate Blanchetts in search of a meaning

__AUTHORS
Toby Fehily
__TIMESTAMP

Tuesday 8 December 2015 18.31EST

__ARTICLE
Theres a clinking of champagne glasses, and Cate Blanchett moves to address an affluent crowd. Reading from cue cards in her hand, she praises the great art vortex and describes the poor as detestable animals. The past and future are the prostitutes nature has provided, she adds. The crowd chuckles politely.
The scene plays out on one of 13 screens dangling from the ceiling at the Australian Centre for the Moving Image (ACMI) in Melbourne for the world premiere of Julian Rosefeldts multi-channel video work Manifesto.
Its a scripted scene, of course, its words drawn from English Vorticist Wyndham Lewiss Manifesto. On the other screens there are a further 12 Cate Blanchetts, playing roles ranging from a school teacher to a funeral mourner to a homeless man, all seamlessly incorporating famous manifestos from art history into their everyday speech.
The serious screeds of Futurists, Dadaists, Situationists and Suprematists are stolen from the page, robbed of context and given new jobs by their new speakers. Theyre no longer untouchable, standalone declarations of artistic intent, but strings of words vulnerable to the new environments they find themselves in. 
One Blanchett sits at for a roast dinner with three boys (her own as it turns out), eyes closed, hands steepled as she recites grace. I am for an art that grows up not knowing it is art at all, she intones, an art given the chance of having a starting point of zero. She pauses for a moment, breaking the repetitive cadence of Claes Oldenburgs I Am for an Art, to glare at her husband (her real one, Andrew Upton), who has arrived at the table late.
The manifestos not only struggle to be told in their own scenes, they are also scrambling to be heard over each other in the gallery space, like so many artists shouting over the din. While ballet teacher Blanchett barks at a group of dancers about Fluxus and Merz, her monologue is rudely interrupted by the sound of nearby factory worker Blanchetts motorbike revving. 
Thanks to Blanchetts convincing, almost effortless performances, the words come across as filler, verbal Lorem ipsum. Its tempting to feel betrayal at first, as though the manifestos and their authors are being done a disservice. Despite being texts, art manifestos are really more action than word and depend heavily on their context. 
Just as important as the words of the many Dada manifestos are the horrors of the first world war that gave rise to them. You cannot talk about the impact of Karl Marx and Friedrich Engels Communist Manifesto without also mentioning the large-scale printing of cheap editions that hastened its spread. But here in the gallery, the manifestos lose their gravity and perhaps some of their meaning.
But what matters in Manifesto isnt what is said but the way its said, and Rosefeldt has found a way to shrinkwrap the ambitious spirit and poetry of these texts into humble everyday actions. A manifesto is a schoolteacher, instructing a new generation. A manifesto is a ballet teacher, choreographing bodies rather than minds. A manifesto is a mourner, eulogising not the death of a person but the death of an idea.
 Manifesto is at the Australian Centre for the Moving Image until 14 March
