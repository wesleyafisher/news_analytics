__HTTP_STATUS_CODE
200
__TITLE

Intel laptops are too expensive. Will AMD Ryzen machines be cheaper?

__AUTHORS

Jack Schofield
__TIMESTAMP

Thursday 12 October 2017 06.13EDT

__ARTICLE
The light but mighty (in speed and capacity) laptops from Dell, HP and Microsoft seem to me to be very expensive because Intel sells processors at premium prices. Now that AMD has produced Ryzen chips, can you foresee if and when these makers will produce desirable laptops with cheaper AMD chips? I can afford to pay Intels price premium, but Id be more likely to buy if the 1,600 price came down to, say, 1,200. Cliff
The good news is that Ryzen-based laptops will be here soon. The bad news is that the first ones wont be ultralight models like the Dell XPS 13. Also, they probably wont bring prices down as much as you hope.
Suppose you buy a new laptop with the latest Intel Core i7-8550U. For that processor, Intel suggests a recommended customer price of $409 (310). You can safely assume that leading PC manufacturers, who are buying thousands of chips, are not paying the recommended price. Even if AMD supplied Ryzen Mobile processors free, it wouldnt knock 400 off the price of high-end laptop. In fact, Ill be quite surprised if it knocks 100 off, though that may change if AMD can sell chips in large volumes.
I know people are hoping that increased competition will force Intel to lower the prices of its high-end chips. However, so far there is no evidence that the current range of Ryzen desktop processors is having that effect.
For example, Amazon.co.uk is charging 281 for the Core i7-7700K, which is its best seller. It was 312.97 in July. The Core i5-7500 costs 161, down from 176.97 in July. However, the AMD Ryzen 7 1700 now costs 262, compared with around 278 in July, while the Ryzen 5 1600X has fallen from around 220 to 187. The Intel chip prices have fallen by around 10%, but the AMD chips are down by 6% and 15%.
Historically, AMD has undercut Intel: you can buy its 6-core FX6300, for example, for only 70. With the Ryzen line, its getting closer to Intel pricing levels but competing on features instead. It seems more likely to increase AMDs average selling price than to bring Intels crashing down.
However, it may have some effect in giving PC manufacturers a bargaining chip when they negotiate prices with Intel.
Its no secret that AMD had fallen a long way behind Intel both in high-end processor design and in manufacturing. AMD was still making chips on a 28nm fabrication process when Intel had moved to 14nm.
AMD needed to make a big leap forward. To do that, it re-hired a very experienced designer, Jim Keller, and asked him to design a next-generation architecture, which is called Zen. Keller had worked on the DEC Alpha  once the worlds fastest processor  before moving to AMD and working on the successful K8 and Athlon chips. After leaving AMD, he eventually ended up at PA Semi, a chip company that was taken over by Apple. There he worked on the A4 and A5 SoC (system on a chip) processors used in products from the iPhone 4 to the iPad 2.
Keller spent the next three years at AMD (2012-2015), with the Zen-based chips due to appear in 2016. The first Ryzen chips were manufactured on a 14nm process and finally reached the market in March this year.
These are hot-running chips for desktop PCs, and tests suggest they are quite competitive with Intels Core range.
AMD has even adopted a BMW-style 3/5/7 naming scheme, which prompts you to compare a Ryzen 3 with a Core i3, and so on. The Ryzen Threadripper tops the range  there are versions with 8, 12 and 16 cores and double the threads  and will compete with Intels new Core i9.
Selling desktop processors doesnt get you very far. AMD is now working to fill out the range with Ryzen Mobile and Mobile Pro chips for laptops and ultraportables, plus a version for servers, which is called Epyc. Ryzen Mobile chips are due to appear this year. Ryzen Mobile Pro chips are expected in the first half of next year.
This will involve reducing the heat generated from a TDP of 65W and 95W in the mainstream desktop versions to around 30W for pro laptops and 15W for ultraportables.
You may have heard that Asus has already launched a Ryzen laptop. However, it didnt wait for the mobile chips. Instead, its ROG Strix GL702ZC packs a Ryzen 5 or 7 desktop processor and an AMD Radeon graphics card into a gaming laptop with a 17in screen. (ROG stands for Republic Of Gamers.) Ultralight it is not.
As before with AMD, the Ryzen Mobile SoCs are APUs (Accelerated Processing Units) rather than CPUs, to indicate their built-in graphics processors. AMD says these chips offer up to 50% more processing power and 40% faster graphics than its previous 9000 series of APUs. This is comparing Ryzen Mobile (aka Raven Ridge) with its older Bristol Ridge chips, not with Intel processors.
Yes, theres a leaked Geekbench score, but I wouldnt put too much faith in that.
AMD said it expected to ship Ryzen Mobile chips in the second half of 2017, which could take you up to the end of December. Laptops may appear before then, of course, but they could be in short supply. Given the standard industry warning  never buy version one of anything!  Id certainly leave it an extra couple of months before parting with any cash.
If computing is your hobby, you might enjoy debugging systems. If not, let someone else do it. It often takes hardware a while to settle down, and early drivers can be notoriously buggy. Without good drivers, you wont get the best out of any new hardware.
I expect most of the large PC manufacturers will supply at least one or two Mobile Ryzen systems. However, this will involve extra costs in design and manufacturing, qualifying parts and system testing. There will also be extra stock-keeping, distribution, advertising, training and support costs. Some manufacturers may sacrifice some Intel advertising or marketing support, because Intel isnt going to help promote AMD-based laptops.
These extra costs will have to be covered by a lower volume of sales of Ryzen Mobile laptops, which is why its hard to compete with Wintel.
Under those circumstances, would you really want to slash the price of Ryzen laptops? Margins on Windows laptops are thin as it is. If I was getting Ryzen chips a bit cheaper than Intel chips, Id price the laptops the same and hope that  after swallowing the extra costs  I came out with a slightly better profit margin.
For the next year or so, I expect Ryzen PCs to be sold on their extra cores and threads, better graphics, and other features that appeal to a geekier audience. Gamers are already the primary market. From there, AMD can expand into the wider market for people will pay for extra performance, such as video editors, programmers and graphics artists. Theres no rush for people like us.
Have you got a question? Email it to Ask.Jack@theguardian.com
