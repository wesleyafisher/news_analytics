__HTTP_STATUS_CODE
200
__TITLE

Asp  or ash? Climate historians link Cleopatra's demise to volcanic eruption

__AUTHORS

Hannah Devlin Science correspondent
__TIMESTAMP

Tuesday 17 October 2017 13.21EDT

__ARTICLE
The fall of Cleopatras Egypt to Augustus, the first Roman emperor, is usually told as a melodramatic power struggle between elites on the world stage.
Cleopatra famously forged a doomed political alliance with the Roman general Mark Antony, who was also her lover. But when their combined forces were defeated at the battle of Actium, the pair killed themselves and Egypt became a province of the newly formed Roman empire.
However, a new analysis suggests the seeds of Cleopatras defeat may have been sown a decade earlier by environmental forces beyond her control. It links a massive volcanic eruption  which probably happened somewhere in the Tropics, although the team is not sure  with severe disruption to the seasonal flooding of the Nile, and devastating consequences for Egyptian agriculture.
The study, based on evidence from ice-core records of eruption dates, the Islamic Nilometer (an ancient history of Nile water levels) and Ancient Egyptian documentation of social unrest, suggests that a giant volcanic eruption in 44BC may have suppressed rainfall, leading to famines, plague and social unrest. Ultimately, the authors argue, this may have weakened Cleopatras hold on power a decade before her defeat in 30BC, changing the course of world history.
The team found a strong correlation between more recent volcanic eruptions and severe dips in Nile flooding
Francis Ludlow, a climate historian at Trinity College Dublin, and co-author of the study, said: Weve shown evidence that the failure of these floodwaters are connected to things like revolt and sales of land, and these are triggering social stresses.
Previously historians have focused on the downward spiral of the 300-year Ptolemaic dynasty, of which Cleopatra was the final ruler, driven by infighting, decadence and incest, with siblings routinely married for political reasons.
They are portrayed as these horrible, drunken, womanising despots, literally drunken idiots who cant run the country, said Joe Manning, a historian at Yale University and also a co-author. The Romans took a really grim view of these guys. Probably unfairly.
We have a more complex story, Manning added. Were saying that the environment and Nile behaviour was important for understanding the economy.
Egyptian agriculture was critically dependent on the annual flood of the Nile, due the almost total absence of rainfall inland. If the flood doesnt rise high enough, you just dont grow anything, said Ludlow. It can be catastrophic.
Ptolemaic rulers developed extensive grain stores to buffer against annual variation in flooding, but extreme water shortage remained a vulnerability.
The new paper links shows that the biggest volcanic eruption in 2,500 years, marked by a spike in the sulphate content in ice-core records, occurred somewhere in the world in 44BC. Separately, the team found a strong correlation between more recent volcanic eruptions and severe dips in Nile flooding, as shown in data from the Islamic Nilometer, the longest-known annually recorded hydrological record, which started in 622 AD.
Giant eruptions inject vast quantities of sulphur dioxide into the stratosphere, which form aerosols that block sunlight. This reduces the amount of water evaporating from oceans and lowers rainfall.
The team also found references to famines, revolts and the desertion of land in numerous papyrus records, linked to other eruptions seen in ice-core data in 209BC and 238BC. And Cleopatras doctor wrote a treatise on the plague following the 44BC eruption, which could have been triggered by mass migration to cities during a famine.
The paper comes as a growing number of academics are turning to climate records, genetic and disease data to reinterpret some of the most significant events in history  although the conclusions are not universally accepted.
Theres a distrust among historians of attributing big historical events to an environmental influence, said Ludlow. People dont like to feel that whats happening in society is beyond their control. They have preferred to explain history through what the great men of history were doing.
Were not saying throw out the history books  were just saying heres a new angle, he added.
The findings are published in the journal Nature Communications.
