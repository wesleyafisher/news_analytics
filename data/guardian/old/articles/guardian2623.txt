__HTTP_STATUS_CODE
200
__TITLE

Food brands 'cheat' eastern European shoppers with inferior products

__AUTHORS

Daniel Boffey in Brussels
__TIMESTAMP

Friday 15 September 2017 12.00EDT

__ARTICLE
Multinational food and drink companies have cheated and misled shoppers in eastern Europe for years by selling them inferior versions of well-known brands, according to the European commissions most senior official responsible for justice and consumers.
In an outspoken attack on the corporate giants she claims have been flagrantly breaking the law, the Czech commissioner, Vra Jourov, told the Guardian: We say for the first time clearly: this is unfair commercial practice. In many cases, yes, I am convinced [the law has been broken] because there is manifest cheating.
From fruit drinks and fish fingers to detergents and luncheon meats, the eastern versions of brands sold across Europe have repeatedly been found to be inferior in quality to those sold in the west, Jourov said, even when they are wrapped in exactly the same branding.
She promised to force multinational companies to stop what she regards as an attempt to mislead consumers in eastern Europe, and said they had been making excuses for too long.
Coca-Cola, whose drink in Slovenian stores was found by researchers there to contain more sugar and more fructo-glucose syrup than that sold in Austria, responded by saying it adapted its recipe to local tastes. Spar, whose own-brand strawberry yoghurt in Slovenia was found to have 40% less strawberry than the Austrian version, said it was merely producing what Slovenians wanted.
Other well-known brands including Lidl, Pepsi and Birds Eye have also been highlighted in recent studies. The companies insisted that where differences had been found in similarly branded products, they had been responding to local demand rather than deliberately misleading consumers.
For the moment, Jourov has promised that the European commision will not name and shame the companies involved  but she said she was prepared to do so if they did not change their behaviour. I will not hesitate to name the brands  and even to encourage people not to buy them. I am quite brave on this.
Jourovs comments follow a deep analysis of data collected from across the EU, and an examination of the parameters of the current body of consumer law. In his state of the union speech on Wednesday, the European commission president, Jean-Claude Juncker, signalled that Brussels was now prepared to act  by legally and financially empowering national consumer bodies to investigate and prosecute those who continue to break the law.
I will not hesitate to name the brands  and even encourage people not to buy them. I am quite brave on this
A final interpretation of the relevant EU legislation is to be provided to the member states on 28 September. But Jourov said she was convinced the law as it stood had been broken, and no further legislation was required.
We have seen the growing dissatisfaction of people who feel the need to buy things abroad in order to have fish fingers that will contain fish meat, or orange juice that will contain oranges  The frustration is growing and we should do something against it.
Until now, Jourov said, the food industry had undermined the case against it by questioning the validity of member states complaints, and making light of the problem because of the lack of direct health implications. For a long time the issue was ridiculed [as unimportant]  but this is about the equal treatment of consumers, the commissioner said.
I have spoken to representatives of traders and producers and they keep producing the same argument: they are adapting the product for the national taste, and they are producing products for local markets using local products. Or for detergents, they say they are adapting the product for the water because there is hard and soft water.
A single testing method is now being established for national consumer bodies, Jourov said, to ensure the industry can no longer avoid accountability. Whenever there are tests made, [the companies] question whether they are relevant, whether they use the right methodology, whether they are comparable. We want to make sure that once we have the results, they cannot be ridiculed.
She added that while the industry had taken a positive step by establishing a code of conduct in response to the commissions concerns, this did not go far enough.
Why do we need a code of conduct to obey the law? she said. I have made it quite clear there are several ways to solve this problem. My ideal solution is to increase the quality of the food. The second best is to rename the brands [in the east] so that people are not misled  but thats not my preferred option. I dont like to have these differences.
The issue of so-called dual food has been pushed up the agenda of the commission thanks to sustained lobbying by eastern states including Bulgaria, Slovenia, the Czech Republic and Hungary, which have all recently published comparative studies. A further study is expected to be published by Croatia on 27 September.
In an interview with the Guardian, Slovenias prime minister, Miro Cerar, said he too believed the multinationals had been involved in misleading practices. I believe sometimes you can clearly see the reasons for such unacceptable practices is simply to gain more profit, he said. This is what the companies usually try to do. But anything essential for quality of life must be brought under control.
A spokesman for Coca-Cola said: We occasionally slightly adapt our beverages to meet local consumer tastes and preferences, to source local ingredients or to follow local regulation. Our commitment to serving beverages that are high quality, affordable and taste great is unwavering in more than 200 countries we serve around the world, including Slovenia.
A PepsiCo spokesman said: PepsiCo is dedicated to producing high-quality, great-tasting products in every market we operate.
A spokesman for Spar said: Our policy is to fulfil consumer wishes, so each Spar country has its own Spar products; the recipes are developed in the country.
A spokesman for Lidl said: Together with our suppliers, we set strict quality specifications for all of our internationally sold own-brand products. These specifications are the same across all markets that the product is sold in.
A spokesman for Birds Eye said it adapted its products to local tastes.
Florence Ranson, a spokeswoman for FoodDrink Europe, the representative group for the industry, said: I would like to stress that we do take the accusations of alleged dual quality very seriously. Consumers are core to our business and equally important wherever they are.
We are active proponents of the single market and the free circulation of goods in particular. Within this frame, it is normal practice that manufacturers source ingredients locally and adapt to local tastes. It must also be stressed that whatever the recipe, our food always meets European standards and remains the safest in the world. The companies currently in the spotlight have rigorous quality management systems in place to ensure consistent quality across their brands all over the world.
