__HTTP_STATUS_CODE
200
__TITLE

A referee's-eye view of Real Madrid, baseball cat and slo-mo overtaking

__AUTHORS

Guardian sport
__TIMESTAMP

Thursday 17 August 2017 05.00EDT

__ARTICLE
1) It might look like one of those adverts where a member of the public is chucked into a professional game for some reason, but the results of mounting a camera on the refs head for the game between MLS All-Stars and Real Madrid are all kinds of excellent. Warning though: not for the easily sea sick. Its David Elleray vs Tony Adams for the 21st century. 
2) Its NFL pre-season training, where the LA Rams and San Diego Chargers have been practising against each other and, well, it all kicked off.
3) Looks like rugby is finally learning something from football. France winger Shannon Izar displays an exceptional first touch before picking the ball up and scoring a brilliant try against Australia in the Womens World Cup. 
4) Catch tha...OH BLOODY HELL, HE DID! Glorious work by Yorkshires Jack Leaning - and against Lancashire too. Cincinnati Reds baseball man Billy Hamilton took a pretty solid one, too. 
5) Speaking of baseball, all human and feline life is here: a cat runs onto the field during the St Louis Cardinals vs the Kansas City Royals, and on the next pitch after the squirming, bitey moggy is removed from the field Yadi Molina hammers a grand slam.
\_()_/ pic.twitter.com/7RRrf6AIOe
6) Taking one for the team in an individual sport is a rare old thing, but Rod Pampling did just that at the USPGA Championships. Pampling was at +12 and obviously going to miss the cut, and with his group facing a race against the clock to finish their final hole before the bad light klaxon went off, he belted his tee shot at speed so that everyone could get their round in. That was the best thing I did all day, he said afterwards.
7) Slo-mo overtaking is surprisingly hypnotic. 
1) Wasim Akram was good, wasnt he? Get a load of his hat-tricks, not least the one which was all bowled.
2) Got 94 minutes to spare? Why not enjoy this trick shot competition featuring Terry Griffiths and John Virgo, presented by Jeremy Beadle, naturally enough.
3) If Conor McGregor really was the big man, maybe he could have a go at medieval knight fighting. He might change his mind after this brutal knockout, mind. 
4) Sport can be confusing. But so confusing that a basketball team goes the wrong way and shoots into the wrong basket? The looks on the faces of the coaches, wildly shooing them the right way, is something to behold.
5) When you have to evade a wall, the rain and umbrellas to catch the ball, but you still do.
Spotters badges: denothemeno, LeeWall, BlackCaesar and TheCedarRoom
