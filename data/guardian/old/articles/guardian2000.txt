__HTTP_STATUS_CODE
200
__TITLE

Jordan Spieths steady start at Tour Championship in pursuit of $10m bonus

__AUTHORS

Ewan Murray at East Lake
__TIMESTAMP

Thursday 21 September 2017 19.06EDT

__ARTICLE
Jordan Spieth can afford to ignore the multitude of arithmetic when calculating who will be the FedEx Cup winner. The Texans equation is simple; win the Tour Championship on Sunday and he will also leave Atlanta with the $10m (7.35m) bonus pot for the second time in his career. It is about as straightforward as this complex play-off series ever gets.
Spieth made a solid start with this dual target in mind. His opening round of 67, three under par, was produced against the backdrop of the Open champion not looking particularly at ease with any aspect of his game. This was classic Spieth; he can score well in all scenarios. He is three behind the 18-hole lead.
Spieths playing partner, and the recent US PGA Championship winner, Justin Thomas signed for the same score as his childhood friend, while Dustin Johnson, the world No1, shot a 68 as did Justin Rose. Jason Day dropped a stroke at the 18th en route to a 69.
The pacesetter was Kyle Stanley, who scored 64. For the 29-year-old to claim the FedEx Cup, he need maintain such form and hope Spieth has problems. Stanleys closest challengers for now are Brooks Koepka, Paul Casey, Webb Simpson and Daniel Berger at four under.
Sergio Garca, the Masters champion, slipped to a 73 that proved two strokes better than Hideki Matsuyama. The Japanese does not yet look to have recovered from falling short when Thomas won at Quail Hollow.
In this, the week when the PGA Tour flexes its financial muscle more than all others, Rory McIlroy has stated his belief that a takeover of the European Tour is inevitable. McIlroy, who failed to qualify for the Tour Championship, believes a single global tour is more viable than the current split between continents.
The PGA Tour is already expanding, going to Asia and other places, he told the No Laying Up podcast. The World Tour, its going to happen one day and it has to happen. To have all these tours competing against each other, and having to change dates, its counter-productive. Everyone has to come together and say this is what we need to do.
The easy thing would be for the PGA Tour to go out and buy the European Tour and take it from there. They could say you still run the European events and well have, say, 12 big events a year outside the majors, a bit like they do in tennis.
I just dont see any other way. I know discussions have taken place, so maybe one day. Im a player, and I will play anywhere.
Meanwhile, the worst kept secret in the game is out, with the confirmation that Catriona Matthew will captain Europes Solheim Cup team in 2019. The 48-year-old is to spearhead the bid to reclaim the cup in her native Scotland, with Gleneagles the venue for the next staging of the biennial event.
It is a dream come true to be the captain at home in Scotland, she said. From the extremely high standard of play to the enormous crowds, huge infrastructure and fantastic media coverage, the event just keeps getting bigger and better. Scotland will provide the perfect stage.
