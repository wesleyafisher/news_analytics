__HTTP_STATUS_CODE
200
__TITLE

Dynasty review  fracking hell! It's back  and even more berserk than before

__AUTHORS

Lucy Mangan
__TIMESTAMP

Wednesday 11 October 2017 09.31EDT

__ARTICLE
The original premiere of Dynasty aired in 1981. It lasted three hours and was called Oil. The decision not to add an exclamation mark after that was the first and last moment of restraint the most 80s of all 80s soaps ever showed. By the end of the three hours, badger-headed patriarch and oil(!) tycoon Blake Carrington had married his former secretary Krystle, beaten a rival oil-rigger, tried to marry off his Headstrong Daughter Fallon (to give her full title) to a cousin to bring about a merger with rival ColbyCo, and everyone had hit their hysterical marks without disturbing a hair on their uber-coiffed heads. Then just 12 episodes later, Joan Collins arrived.
The bar for this reboot, then, is magnificently high. Can the team in charge  The OC and Gossip Girl creators Josh Schwartz and Stephanie Savage developed the project, while the creators of ur-Dynasty itself Richard and Esther Shapiro are attached as producers  deliver? Lets redo our lipgloss, clip on our diamond earrings and find out! 
We open with Fallon expositing like a good un. As with Fallon the First, she is the overlooked, underestimated daughter of Blake Carrington, the owner of oil company Carrington Atlantic. Power, she says, is literally our business. The ratio of ex cathedra pronouncements like this to actual dialogue accords splendidly with 80s tradition. Trust doesnt come easy to Carringtons! and Change the game  or dont play at all! are soon standing proud, helping to bear the weight of accumulating madnesses.
Onward!
Stephen the Second is still gay, but now out. He still has a social conscience and recently organised a protest that cost his papa a $600m deal to frack the whole of Georgia, or something. There are a lot of fracking machinations in this opening episode. I cant pretend to have followed everything. Blake and Stephen are on no-speaks, anyway. 
Fallon is banging the chauffeur, BTW. This is canon, though because it is also 36 years (36 years on, dear God! Is time a flat circle or what?) since she first did, there is now cunnilingus too, because Feminism. 
But something must be afoot because Blake recalls both his children to the marble mansion. They walk in on him having sex with one of his twentysomething female employees on his study desk. This turns out to be Cristal Flores, a reimagining of Linda Evans Krystle that breaks with tradition by making her Hispanic, giving her name a reasonable number of vowels and minor motility to her face and hair. She is going to be their new mom. Thats whats afoot. She also has a dark past, known unto butler Anders  played by Alan Dale with a haunted look that I dont think is a character choice  and a brother who turns out to be Stephens new boyfriend. 
Stephen is amused by this idiosyncratic introduction to Mommy Youngest but Fallon is furious. When made furiouser still by daddy passing a business tip she gave him to Stephen to close the deal, she heads to tech mogul Jeff Colby (now African American and Blakes sworn enemy because of software shenanigans rather than oil-based rivalry) and forms her own company to carry out the frackinations Blake had planned for himself. 
Oh, and Blake has Cristals former lover Michael, husband of nutty Claudia, killed. She turns up at the Carrington wedding  to whose petal-strewn absurdity I cannot possibly do justice with only the paltry resource of the written word  and accuses him of murder. 
Credits, though Im surprised they have the energy, roll.
Its fun, theres no question. But though it tries, occasionally painfully hard (the moment Fallon bites off the head of the wedding cake topper in front of Cristal is risible) it doesnt have the camp wit or excess of the original. And of course, times have changed. There is a (near) joke in it at one point about the 99% and the 1%, and  from Jeff Colbys sister  about how white Fallons world is, in an attempt to acknowledge the fact that we are breathing rarefied air here, but it doesnt really work. We have seen too much of what goes on behind the curtain since original Dynasty was able to sweep us up and away. In the Trumpocene era, what was once an hour of escapism about madly rich and simply mad rich people now has the air of sanitised truth. Fracking hell.
