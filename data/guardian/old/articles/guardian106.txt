__HTTP_STATUS_CODE
200
__TITLE

Haute Hawkins: how Stranger Things won fashion

__AUTHORS

Lauren Cochrane
__TIMESTAMP

Monday 16 October 2017 10.34EDT

__ARTICLE
With the second series of Stranger Things starting later this month on Netflix, households everywhere are preparing to settle down to the eighties-set supernatural thriller. The fashionable will join them: brands are embracing Stranger Things as cult TV.
From the 20 October, Topshop and Topman will collaborate with Netflix to produce a range of clothing themed around the show  sort of merchandise for the series. In-jokes are rife. One T-shirt features the Upside Down  the alternate dimension featured in the show  while a backpack has patches for Hawkins Middle School AV Club, attended by the characters in the show.
There will be Stranger Things takeover in the brands stores in London, Birmingham, Liverpool and Manchester in the run-up to Halloween. This includes immersive  slightly terrifying  sets. In the Oxford Circus store, there will be the Hawkins lab, the wooded den created by the main character Will and, of course, the Upside Down. An area will be set up to watch episodes the night before its debuted on Netflix. Were always thinking about how we can create an experience, says Sheena Sauvaire, director of marketing and communications at Topshop. Stranger Things has cultural relevance and its very much a part of the zeitgeist.
High fashion is on board too. 13-year-old Millie Bobby Brown, who plays Eleven in the show, starred in the advertising campaign for Calvin Klein this year and sat front row at the brands show in September. Nicolas Ghesquiere, the creative director of Louis Vuitton, is also watching the series. For the catwalk show earlier this month, T-shirts for Stranger Things were included amongst the four-figure luxury outfits. 
The first series was a hit for Netflix. According to Variety, viewing figures averaged 14.07m adults aged 18-49 in the first 35 days after it was released. While it was hugely popular, the series retained a crucial cool factor. This is perhaps down to the eighties setting, the movie buff references and the character of Barb. While her demise was relatively swift, she lived on in fashion-friendly memes thanks to her oversized glasses and cardigans, both items key to Guccis cult geek chic aesthetic. Barb is of course included in the Topshop range. She features on a T-shirt, with the slogan never forget.
Alice Casely-Hayford, the fashion director of Refinery29, believes this trend is about our current obsession with merch. 2015 and 2016 were about [Kanye Wests] Life of Pablo, Justin Bieber. This is another way to show youre part of something. Casely-Hayford says that pop culture-centric consumers are disenchanted with the band T-shirt now it has become a fashion item  with the TV T-shirt taking its place. Stranger Things is far more accessible [than an obscure band], but its genuine, she says. Sauvaire considers that this new alliance between fashion and TV may yet become closer as Netflix and Amazon dominate the cultural conversation. Pop culture is rooted in our brand so thats why this felt right, she says. Fashion has now moved beyond its more immediate creative disciplines.
