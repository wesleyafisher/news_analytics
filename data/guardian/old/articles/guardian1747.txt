__HTTP_STATUS_CODE
200
__TITLE

Anti-smog bikes: could pedal power clean China's polluted air?

__AUTHORS

Anna Hart
__TIMESTAMP

Friday 19 May 2017 05.37EDT

__ARTICLE
Dutch designer Daan Roosegaarde has announced the next phase of his Smog Free Project: a bike that sucks in polluted air and releases purified air in a cloud around the cyclist. 
According to Roosegaarde, whose design firm Studio Roosegaarde has offices in both Rotterdam and Beijing, the idea for his Smog Free Project came just over three years ago, as he gazed out of his Beijing apartments window. On a Saturday, the city skyline is visible; on weekdays, its shrouded in smog.
Roosegaarde first proposed a smog vacuum cleaner to remove pollutants from the skies in 2013. This evolved into an air-purifying tower which, following a successful Kickstarter campaign, has just been unveiled in a public park in Tianjin. The Smog Free Tower uses positive ionisation to remove particulate matter from the surrounding air, expelling purified air through vents in the side. The same technology is now being applied  in theory, anyway  to bicycles. 
Bikes have always been a symbol of energy-friendly and congestion-reducing living, but this bike serves a double function by cleaning the air as you cycle, says Roosegaarde. For me, design has never been about creating yet another chair or another table. We should use creativity to improve the way we live.
Beijing and Tianjin are just two of 300 cities in China that badly failed air-quality tests in 2015, according to Greenpeace. More than 1.6 million people die every year in China from breathing toxic air, and in December a much-publicised airpocalypse hit 460 million people, who were exposed to smog levels six times higher than the WHOs daily guidelines. Flights were grounded and schools closed, as 24 cities across China were put on red alert for extreme smog.
Healthier cities and urban innovation will always be connected with large-scale governmental investment, says Roosegaarde. But Im too impatient to wait for change to trickle down. The government does top-down, and designers, universities and NGOs do bottom-up, and hopefully we meet in the middle.
Roosegaarde hopes that smog-free bikes will be adopted by Chinas increasingly popular bike-sharing programmes  there are 29 in total across the country  such as Mobike, Bluegogo and Ofo. 
Of course one tower and a few bikes wont solve Chinas air pollution problem, but smog-free bikes are an exciting idea that will hopefully activate communities towards creating greener cities, says Roosegaarde. Our aspirations are being redefined; in the future, the biggest luxury wont be a Louis Vuitton bag. It will be clean air.
Follow Guardian Cities on Twitter and Facebook to join the discussion, and explore our archive here
