__HTTP_STATUS_CODE
200
__TITLE

Lloyds shareholders' court case over HBOS takeover set to begin

__AUTHORS

Jill Treanor
__TIMESTAMP

Sunday 15 October 2017 07.19EDT

__ARTICLE
A 600m case is due to begin in the high court this week which is expected to lead to five former directors of Lloyds Banking Group being asked to explain the circumstances that led to the rescue of HBOS during the height of the financial crisis.
The banks former chair Sir Victor Blank and former chief executive Eric Daniels are among those named in the case brought by Lloyds shareholders who argue they would not have voted through the takeover of HBOS if they had been given the true picture of its financial health.
The case names Blank, Daniels, the former finance director Tim Tookey, former head of retail Helen Weir and former head of wholesale banking Truett Tate as defendants, as well as the bank itself. Lloyds, which is also representing its former directors, is contesting the action by around 5,700 private investors and 300 corporate entities.
Lloyds announced its takeover of HBOS in September 2008, just days after the collapse of Lehman Brothers had unleashed fear in the financial markets. The prime minister at the time, Gordon Brown, is said to have brokered the deal which required the government to override competition law on public interest grounds to ensure the stability of the UK financial system.
The taxpayer took a 43% stake in the enlarged Lloyds during the crisis, but that shareholding has now been sold off.
The Lloyds shareholder action group is arguing that information about lifelines provided to HBOS from the Bank of England and Federal Reserve was not disclosed at the time they were asked to vote on the transaction.
It was a deal so bad that directors should never have recommended it, said a spokesperson for the action group. In putting it to the vote, shareholders didnt have the right information. If theyd been given the right information they wouldnt have voted for it on those terms.
A spokesperson for Lloyds said: The groups position remains that we do not believe there to be any merit to these claims and we will robustly contest this legal action.
The case  which is due to begin on Wednesday  is also taking place at a time when Daniels, who left Lloyds in 2011, is suing the bank for hundreds of thousands of pounds in disputed bonuses in a separate legal case. He is reported to be taking action to claim performance bonuses that were not paid to him despite hitting targets. 
Lloyds said: As this matter is a live legal issue it would be inappropriate to comment. 
Since leaving Lloyds, Daniels has taken roles at the peer-to-peer lender Funding Circle, the Mayfair-based investment bank StormHarbour and the private equity firm CVC Capital Partners. Weir is Marks & Spencers finance director and Tookey is in the same role at Old Mutual Wealth.
The lawyers for the Lloyds shareholders are Harcus Sinclair and the funding is provided by Therium Capital Management, a specialist company which backs litigation . The case is starting a fortnight later than expected to allow a new judge, Justice Alastair Norris, to take over from Justice Christopher Nugee, who has a broken leg.
All five former directors named as defendants are expected to give evidence over the case which is expected to run into next year. It comes after a settlement in June between shareholders in Royal Bank of Scotland and the Edinburgh-based bank over allegations they were misled about the health of the bank during its 12bn cash call in April 2008. 
Had the RBS case not been settled, it would have forced its former chief executive Fred Goodwin to defend his actions during the 2008 banking crisis in the high court.
