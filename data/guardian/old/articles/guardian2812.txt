__HTTP_STATUS_CODE
200
__TITLE

Rochdale charitys demolition plans spark social cleansing claims

__AUTHORS

Josh Halliday North of England correspondent
__TIMESTAMP

Sunday 15 October 2017 18.30EDT

__ARTICLE
 
They have been a landmark on Greater Manchesters skyline for half a century. But four of Rochdales Seven Sisters tower blocks are facing demolition, prompting opposition from hundreds of residents and accusations from the councils housing boss of social cleansing.
Rochdale Boroughwide Housing (RBH), a charitable trust that runs all former council stock, said it planned to tear down four of the social housing blocks to make way for new townhouses and regenerate the town centre.
The high-rises border an estate that was until recently classed as Englands most deprived neighbourhood, and identified by the then prime minister, David Cameron, early last year as typical of the so-called sink estates that should be bulldozed in a blitz on poverty.
The plan is to build 120 townhouses in place of the four demolished tower blocks and give the three others a multimillion-pound makeover, in what RBH has called a housing regeneration in Rochdale town centre.
But the proposals have been opposed by hundreds of residents and described as crazy by Neil Emmott, Rochdale borough councils cabinet member for housing, who said it smacks of social cleansing and risked worsening Greater Manchesters homelessness problem.
Emmott said he had been given a presentation by RBH on its proposals and that it focused on how many people are on benefits and how many people are poor round that area. Frankly, I was appalled, he said. It really sickened me to see them coming out with this sort of stuff.
Were all in favour of regeneration but regeneration should be for everybody, regardless of income, regardless of background. Were not going to have proper regeneration simply by sweeping people on lower incomes under the carpet.
One in 10 working-age adults in the blocks and its surrounding streets are unemployed, three times more than Rochdales average and four times the national average. 
Emmott said he was concerned that RBHs proposals would lead to a net loss of more than 400 homes  it plans to build 120 townhouses in place of the 528 flats that would be lost  and would add more people to the waiting list for homes in the borough. Every resident of the 761 flats, thought to be more than 1,000 people, would have to move out under the proposals, with only those in the three renovated blocks able to return.
RBH said there was absolutely no truth in Emmotts social cleansing claim and that all current residents would be able to stay in the town centre. It said its proposals could result in a net increase of 500 homes in the area but did not explain the figure. A spokesman said it would cost at least 10m each to renovate the blocks, compared with 1.9m each to demolish.
RBH said: As our homes in College Bank and Lower Falinge have lower demand than some of our other neighbourhoods, currently College Bank and Lower Falinge cost more in maintenance than we collect in rent. By providing a better quality and mix of homes, we can deliver a more sustainable neighbourhood that meets the needs of tenants now and in the future.
Audrey Middlehurst, 82, a retired teacher who has lived in one of the blocks for 29 years, said the blocks elderly residents would be absolutely devastated to have to move. Its just so worrying for them  and theres no reason why they should move out, she said. Can you just imagine having an 80-year-old having the upset of all this upheaval? They feel really devastated.
Robin Parker and Ann Jones, the former mayor and mayoress of Rochdale, who live in the blocks, said their lives had been put on hold until they knew whether they were being evicted. It distracts you from other things. Its with you every minute of your waking day. Its a burden on you, said Parker, 73.
Jones, 62, said the area had been damaged by the bad press it has received but that she was proud to live in the Seven Sisters blocks, which are officially known as College Bank. The term sink estate does so much damage to areas, she said. Just that one phrase, use that and the area is then tarnished, the people living there are then tarnished.
Far from being haunted by the Grenfell Tower fire in west London, which triggered a national debate on social housing blocks, Parker said he felt safe in the Seven Sisters. They were built to be totally self-contained so, if a fire happens in one of these, there is no issues at all of it spreading to others, he said. Theres no cladding or anything, so theres no risk whatsoever of anything like Grenfell happening here.
Saiqa Naz, an NHS mental health worker and Rochdale resident who has offered support to Seven Sisters tenants, said the uncertainty was taking a toll on some of the blocks elderly residents. Ive spoken to people in their 80s, crying, not eating or sleeping, she said. Theres the hidden cost of regeneration, which is mental health, physical health, breaking up a community and moving them away from their support networks.
