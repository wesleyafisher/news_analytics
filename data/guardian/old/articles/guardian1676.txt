__HTTP_STATUS_CODE
200
__TITLE

UK Treasury rejects OECD's call for second Brexit referendum

__AUTHORS

Phillip Inman
__TIMESTAMP

Tuesday 17 October 2017 12.51EDT

__ARTICLE
The Treasury has flatly rejected calls for a second EU referendum after the wests leading economic thinktank, the Organisation for Economic Cooperation and Development, said reversing the decision to leave would significantly benefit the economy.
We are leaving the EU and there will not be a second referendum, the Treasury said in a terse statement that reflected the governments unhappiness with the OECDs intervention.
The Paris-based group, which has 35 of the worlds richest countries as members, said it would revise the gloomy forecasts it made in its annual health check were Britain to stay in the EU.
In case Brexit gets reversed by political decision (change of majority, new referendum, etc), the positive impact on growth would be significant, the report said.
No 10 joined the chancellor, Philip Hammond, in rejecting the OECDs analysis, though a spokeswoman for the prime minister would not be drawn on whether the statement was irresponsible or on whether the UKs 10m-a-year membership contribution to the thinktank was well spent.
The OECD are a respected international body but what we should bear in mind is that its based on a no-deal situation, which is not what we are looking for. We are confident we are going to strike a good deal, she said.
Leave campaigners were more critical of the OECD, which receives most of its funds from other EU member states. 
The Change Britain chair and former Labour MP Gisela Stuart said: It is laughable that the EU-funded OECD, at a time that is the most helpful possible for Brussels, has the gall to intervene in our negotiations and call for Brexit to be reversed. 
These EU elites refuse to accept that 17.4 million people voted to take back control, and meant it. The British public didnt believe the OECDs scaremongering before, and nor should they start now.
Angel Gurra, the OECDs secretary general, insisted that the thinktank respected the decision of the referendum and sources at the organisation sought to play down the second referendum call, saying it was merely sketching out alternative scenarios.
However, the OECD said Britain must secure the closest possible economic relationship with the EU after Brexit to prevent the economy suffering a long-term decline.
Gurra said Brexit would be as harmful as the second world war blitz and the British would need to act on the propaganda maxim to keep calm and carry on.
The deputy leader of the Liberal Democrats said it was clear from the OECD report that a second vote was needed to prevent the harm caused by Brexit.
Jo Swinson said: Brexit has already caused the UK to slip from top to bottom of the international growth league for major economies. 
This will only get worse if the government succeeds in dragging us out of the single market and customs union, or we end up crashing out of Europe without a deal.
The thinktank, which has predicted the UKs growth rate will fall to 1% next year, said a disorderly exit from the EU single market and customs union in 2019 would hurt trading relationships and reduce long-term growth.
Entering the debate over Brexit at a crucial stage in negotiations, the OECD added that steep falls in the UKs productivity performance relative to other major economies, allied with the failure of its export industries to grab a slice of expanding world trade, have left the country in a weak position to operate outside the EU.
Britain wants to discuss its future trading relationship with the EU because44% of UK exports go to, and 53% of imports come from, the EU 27 countries. Post-Brexit conditions of trade could, therefore, have a major effect on Britains economy.
TheWorld Bank estimates UK trade with the EU in goods and services could fall by 50% and 62% respectively if no trade deal is agreed after Brexit, against 12% and 16% if the UK stays in the single market through a Norway-style agreement.
Clean Brexit campaigners say the shortfall can be offset through more trade with non-EU countries, but those who argue the UK must retain close links with the single market doubt this, certainly any time soon. Both groups want certainty.
However, theEU27s negotiating guidelines for the two-year Brexit talkssay discussion of the framework of a future relationship can only take place in phase two of the talks, once sufficient progress has been made on the separation phase and particularly the UKs exit bill.
The warning follows a week of shuttle diplomacy between London and Brussels. The UK government says it has gained a commitment from EU leaders to speed up talks, although there has been no progress in crucial areas, including the divorce bill.
Officials at the OECD have adopted one of the gloomiest outlooks for the British economy with an assumption that a trade deal with the EU would take four years to negotiate after Brexit, leading to further uncertainty and lower growth.
 
To offset some of the damage, the OECD urged Hammond to spend spare funds on identifying ways to improve productivity, which measures the output per hour of an individual worker, by enhancing the skills of low-income workers.
On the possibility that the UK might change course altogether, it said: In case Brexit gets reversed by political decision (change of majority, new referendum, etc), the positive impact on growth would be significant.
The report also called on the chancellor to revive his plans to raise funds through an increase income tax on the self-employed, and end the triple lock on state pension rises, arguing that the state pension should rise in line with average earnings.
Both proposals were immediately slapped down by Hammond, who said there was no intention to revisit the self-employment pension increases, which would continue to be in line with the highest of inflation, earnings or 2.5%.
But the OECD reserved a warning for the Bank of England, which it said must guard against raising interest rates during a period of low growth, declining rates of productivity and while the economy remained vulnerable to Brexit. 
It said Threadneedle Street should look through the current spike in inflation and maintain a loose monetary policy.
The Treasury said: Increasing productivity is a key priority for this government, so that we can build on our record employment levels and improve peoples quality of life. 
Today, the OECD has recognised the importance of our 23bn national productivity investment fund, which will improve our countrys infrastructure, increase research and development and build more houses. 
In addition, our reforms to technical education and our ambitious industrial strategy will also help to deliver an economy that works for everyone.
