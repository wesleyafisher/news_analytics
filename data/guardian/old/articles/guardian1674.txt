__HTTP_STATUS_CODE
200
__TITLE

Brexit: cabinet split as Amber Rudd calls no-deal unthinkable

__AUTHORS

Rowena Mason, and 
Jennifer Rankin in Luxembourg
__TIMESTAMP

Wednesday 18 October 2017 02.31EDT

__ARTICLE
A cabinet split has emerged over whether the UK could walk away from the EU without any Brexit deal, as Amber Rudd said it was unthinkable but David Davis insisted it must remain an option.
Rudd, the home secretary, appeared to undermine the governments position that no deal is better than a bad deal on Tuesday as she dismissed the idea of not getting an agreement that at the very least covered security.
It is unthinkable there would be no deal. It is so much in their interest as well as ours  We will make sure there is something between them and us to maintain our security, Rudd told parliaments home affairs committee.
An hour earlier, Davis had told the House of Commons that it was necessary to keep the option of no deal open. He said the UK was straining every sinew to get a deal with the EU, but it was necessary to prepare for all outcomes.
The maintenance of the option of no deal is both for negotiating reasons and sensible security. Any government doing its job properly will do that, the Brexit secretary said.
Rudd later insisted she was not contradicting the government position on walking away with no deal, highlighting Theresa Mays Florence speech that said the UKs proposal on security cooperation was unconditional. 
There are no threats either way on security, Rudd said, denying that there were different messages coming from individual members of the cabinet.
However, the divisions between cabinet members were further highlighted when Liam Fox, the trade secretary, took a very different tone from both Rudd and Davis in an interview with the BBC, saying there was no need to fear no deal.
Leaving without a deal will not be the armageddon that some people project and leaving with a deal will give us a slightly better growth rate, he said. We need to concentrate on the realities, get rid of the hyperbole and focus on the fact that if we can get a good agreement with the EU both Britain and the EU will be better off for it.
It comes after hardline Conservative Brexit supporters have accused Philip Hammond, the chancellor, of failing to put enough effort into getting ready for the possibility of no deal.
May has promised Eurosceptic MPs that everything necessary will be done to prepare for leaving the EU without an agreement, while insisting that the governments preferred plan is to achieve a deal.
In the Commons, Davis reassured MPs that a deal was likely to be done despite the stalemate in talks, but also accused the EU of trying to drag out the negotiations to try to get more money out of the UK.
The Brexit secretary said the UK was ready to start discussions on the future trading relationship, as a deal needed to be struck before exit day in March 2019. 
They are using time pressure to get more money out of us. Bluntly, that is whats going on. Its obvious to anybody, he told MPs.
We must be able to talk about the future. We all must recognise that we are reaching the limits of what we can achieve without consideration of the future relationship. 
At the European summit later this week, I hope the leaders of the 27 will recognise the progress made and provide Michel Barnier [the EU chief negotiator] with a mandate to build on the momentum and spirit of cooperation we now have. Doing so will allow us to best achieve our joint objectives and move towards a deal.
Davis suggested he was confident of some progress on the issue soon, saying: Lets just see what the European council comes up with on Friday, shall we?
The UK is lobbying hard for EU leaders to widen the scope of talks when they meet for a summit in Brussels on Friday in order to achieve a deal within the time limit. Discussions are currently limited to EU citizens, Northern Ireland and the financial settlement.
The issue of money appears to be causing the deadlock between the UK and EU, which led the prime minister to make a last-minute trip to Brussels on Monday for dinner with Jean-Claude Juncker, the president of the European commission. 
By the time of the summit May will also have spoken individually to the leaders of France, Ireland, Spain and Italy in an attempt to get them on board for moving on to trade talks.
However, Barnier suggested the talks remained at an impasse as he arrived in Luxembourg on Tuesday for a Brexit meeting with 27 EU ministers.
Asked whether Monday nights Brussels dinner had changed anything, he said: I said three weeks ago I am ready to accelerate the rhythm, but to accelerate it takes two. It was a good working dinner and what we told Prime Minister May is just that is very important to maintain this constructive dynamic in the coming two months.
Barnier rejected accusations that Europe was holding back progress, pointing out that the EU had had to wait for May to trigger article 50 and for the result of a general election. The EU is not holding anything or anybody back. We are ready and willing to even speed up the negotiation.
A senior EU official said Mays Florence speech, which contained a promise of a 20bn (18bn) transition payments, had helped to unlock progress. Before the speech, EU capitals were more and more were afraid we were heading to a no-deal scenario, but after the speech the reading is we are negotiating in good faith and that a deal is still in reach.
Keir Starmer, the shadow Brexit secretary, said in the Commons that Davis was failing to acknowledge the seriousness of the situation we find ourselves in and called on both sides to show more flexibility to break the deadlock.
Every passing week without progress on transitional arrangements make things worse, not better, he said.
Starmer accused the government of sounding too enthusiastic about failing to strike an agreement with the EU, saying: Only fantasists and fanatics talk up no deal.
Davis responded by saying Starmer had no strategy of his own, and he denied accusations that he was talking up the prospect of not achieving a deal. But he also said it was scaremongering for the Resolution Foundation thinktank to say no deal would lead to a sharp increase in prices for food and other goods.
Conservative MPs are deeply split over whether the UK should be contemplating a no-deal outcome, with some of the most strident Brexit supporters arguing that the UK should simply walk away without wasting time on talks.
James Duddridge, a former minister and Brexit supporter, said no deal was his preferred way to achieve a fast, clean and boring Brexit and the sooner we get on with it the better.
On the other side of the argument, Nicky Morgan, a former Conservative education secretary, urged Davis to beware the siren voices calling on him to walk away from Brexit talks. 
