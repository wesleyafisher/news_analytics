__HTTP_STATUS_CODE
200
__TITLE

Asda cuts nearly 300 jobs at its head office as part of cost-saving effort

__AUTHORS
Press Association
__TIMESTAMP

Thursday 7 September 2017 01.39EDT

__ARTICLE
Asda has axed nearly 300 jobs at its head office as part of a cost-cutting drive at the supermarket.
The grocer, which is owned by Walmart, informed 288 affected staff about the cull on Wednesday afternoon, instructing all of those workers to leave their posts immediately. 
A further 800 staff have had the scope of their job descriptions changed as part of the shake-up.
The job cuts account for more than one-tenth of Asdas 2,500 head office roles, and it is understood that the majority of staff being let go had been working at its office in Leeds while the rest were based at Asdas office in Leicester.
A spokesperson for the supermarket said: At Asda we value each and every one of our colleagues. The changes are in response to the ever-changing sector in which were working and the need to adapt to create an agile business which is fit for the future.
Asda is in the midst of a turnaround plan under the new chief executive, Sean Clarke, who is attempting to arrest falling sales as the supermarket scraps it out with rivals in a price war that has eroded profits.
Reports surfaced last month that thousands of Asda workers across 18 underperforming stores were facing redundancy or changes to their working hours. Staffing arrangements in a further 59 stores are also being looked at as Clarke brings in a series of changes.
In August, there were signs that his strategy was beginning to bear fruit, with Asda posting its first quarterly sales growth in three years. The supermarket reported a 1.8% rise in like-for-like sales in the second quarter, bringing an end to 11 consecutive quarters of deterioration.
Figures were boosted by a combination of price cuts and rising inflation, and came a year after Asda reported its worst quarterly performance on record when sales tumbled by 7.5%.
Clarke, who took up the reins last summer after being parachuted in to replace the previous boss, Andy Clarke, has slashed the prices of everyday items as part of attempts to woo back shoppers.
