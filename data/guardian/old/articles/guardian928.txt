__HTTP_STATUS_CODE
200
__TITLE

Dearborn, Michigan: a city divided by religion, race and class

__AUTHORS

__TIMESTAMP

Friday 4 August 2017 08.01EDT

__ARTICLE
In the past 12 months the city of Dearborn, Michigan, has been thrown into conflict: conflict powered by fear, ideology and identity politics  and, at its heart, what it means to be an American.
Dearborn, home to the largest mosque in north America, is a place of apparent contradictions. It is simultaneously a sleepy, affluent suburb and yet also the subject of rumours about Isis terror cells and sharia law.
This film takes us into the lives of five very different citizens who are caught in the crossfire, from Muslims to Christians, citizen militias to university graduates. For all, their American identity is paramount. As they grapple with questions of religion, race and class, do these separate communities have more in common than they realise?
Dearborn, Michigan is commissioned by the Guardian in collaboration with The Filmmaker Fund.
Katharine Round is a filmmaker and artist with more than 19 years experience in creative documentary for broadcast and cinema. Her production company is Disobedient Films and her work spans broad themes of economics, society and science, often told through the prism of psychology and character-driven narratives. In 2016, she directed the critically acclaimed The Divide, a feature-length film on the psychological impact of income inequality, inspired by the book The Spirit Level, described as fierce and unsettling by Peter Bradshaw. Katharine is also an accomplished producer, devising new models of finance, and is the co-founder of documentary filmmakers organisation Doc Heads.
Ben Steele is a director and cinematographer, creating visually stunning stories that leap off the screen and stick in the mind. His most recent film, Love and Hate Crime, premiered at Sheffield Doc/Fest in June 2017. In this documentary, a young man in Mississippi is sentenced to life for the savage murder of the woman he loved, after which his story slowly unravels as his own past is unmasked. His other films include Hunted, exploring the rising tide of anti-gay violence in Russia from the perspective of violent vigilante gangs, and the BAFTA-nominated Orphans of Ebola.
This documentary is accompanied by a new article on the Muslim Americans leading the push to stand up and be leaders in politics
The Guardian wrote immediately after the US election about the concerns of the local Muslim community in Dearborn and has tracked the mixed economic fortunes of neighbouring city Detroit over recent years, as well as the rise of another local city with a majority Muslim city council, Hamtramck. Nationally, weve explored the worrying reality for Muslim Americans in Trumps America. The seeds of this situation can be seen in interviews that took place with eight Muslim Americans only a year earlier, and this piece from 2015 on whether a Muslim woman could be a President.
We want to hear from you - What does it mean to be American? Share your story.
Unexpectedly tucked away in suburban London, a very special cricket match takes place every week. The group who play here are young refugees and asylum seekers. Coming from war-torn countries and separated from their families, most have been traumatised. The Refugee Cricket Projects weekly cricketing sessions are not just a chance to play and connect with friends, but also an opportunity to access advice and support as they navigate the complex asylum system and other challenges in their new home. Against a backdrop of preparation for immigration tribunals, some surprising fixtures and the amazing rise of the Afghanistan National Team, this is the story of a cricket club like no other. Released August 25th
An Inconvenient Sequel - Friday 11th August, 7:15pm, venues across the country including Showroom Cinema, Sheffield
A decade on from Al Gores groundbreaking documentary An Inconvenient Truth comes this much needed follow-up, addressing what progress has been made in the years since the original release, and what exactly is happening to our planet now.
Taking a fearlessly confrontational stance against Trumps climate-change-denying governance, Truth to Power is both a hopeful exercise detailing a decade of achievements and changed attitudes, and a fiery call to arms on what will happen if we dont continue to change and improve our stance on climate change.
Including satellite Q&A with Al Gore
Watch the trailer here
If you like what we do with documentaries at the Guardian, then please consider becoming a supporter. Our ability to investigate and expose stories such as these is made possible by our supporters. Join today to support our journalism.
