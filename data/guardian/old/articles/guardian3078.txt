__HTTP_STATUS_CODE
200
__TITLE

In Montana it feels like theres lots of sky stacked on top of us, layer upon layer

__AUTHORS
Interview by 
Jane Dunford
__TIMESTAMP

Friday 2 June 2017 07.30EDT

__ARTICLE
Im on a quest, with my twin brother, to make five films in five parts of Montana. The latest, Walking Out, is number three. Montana is our canvas for stories about the American west and mans connection with nature. Its a very cinematic place and has become our muse. The films based on a short story set in Montana  something I read when I was about 14, the age of the main character. Its about an estranged father and son surviving in the wild.
I grew up on a ranch of sorts in the west of Montana. It was surrounded by mountains and streams, about a mile from the Blackfoot river, where A River Runs Through It was filmed. It was a community of ranchers and loggers, but our parents were more hippie bohemians. We spent a lot of time in nature, goofing around in the woods  there were always chores though, mending fences, chopping wood. There was no TV reception, but wed go to the arthouse cinema in the nearest college town  its what made me want to make films.
This is called Big Sky Country and its one of those things that you cant really explain, but when people show up they get it immediately. It feels like theres a lot of sky stacked up on top of us, layer upon layer of it.
Montanas a very big state with a very small population. You have a lot of elbow room and feel youre a small part of a vast wilderness. Its ruggedly beautiful but can be daunting. People who choose to live here embrace the wildness and harshness: theyre stoic and self-sufficient but they like to hang out  theyll drive 100 miles for a beer.
Montana appeals to writers, the vastness is provocative and inspiring: Jim Harrison, Tom McGuane, Norman Maclean, and native American writer James Welsh (whose Winter in the Blood was adapted into our second film) to name a few.
Every season has its magnificence but Id suggest coming in summer. You have these really long days and amazing starry nights. Just hit the road and see where it takes you. Ive been to most of the national parks in the US, but there is something special about Glacier national park. Even in the summer, when its well visited, you can easily get off the beaten track and feel like you have it to yourself.
Id recommend people get out on the rivers. Try for Smith river  you have to put in for the annual permit lottery as numbers are restricted. Its a four-night, five-day river float along 60 miles of river and you can raft, canoe or kayak and camp on the banks at night. Its so peaceful. There are also several companies that run trips if you dont want to go alone or dont get a permit.
There are seven first nations reserves in Montana and even more tribes. Powwow season (spring and summer) is a beautiful, powerful experience. Tribes gather for drumming and dance competitions, parades and rodeos. You can dip into the real history of Americas first nations. Blackfeet reservation has a rodeo and Powwow happening simultaneously in the main town of Browning. So you can have the Powwow and Indian cowboy experience at the same time.
Butte, in western Montana, was once the richest copper mine in the world. It was a real boom town at the turn of the 20th century: that disappeared and now theres a burgeoning art community. Its historically fascinating, but also a friendly place. There are beautiful art deco buildings  and now lots of artists have moved in because its cheap.
The Hi-Line, the northernmost tier of Montana, is another must-see. Its beauty is more spartan. The Amtrak runs across it, which is a great way to see Big Sky Country. I always try to stay at Grand Union Hotel in Fort Benton, overlooking the Missouri river (made famous by the Lewis and Clark expedition in the early 19th century). Its also a great place to dine.
Montanas better known for its bars than its food scene. There are plenty of honky-tonks and highway taverns, such as the Murray Hotel in Livingston. People filming in Paradise valley, south of town, often visit: the director Sam Peckinpah shot up a suite there back in the 70s. The area around it is great for bar-hopping. Livingston is quite a small town  it would be bigger but the wind drives people away! Another one not to be missed is the Sip n Dip in Great Falls. It has been done up in Tiki, tropical style, with windows onto the swimming pool. So you sit at the bar watching people swim, and they have mermaids performing at the weekend  all quite surreal when its winter and -8C outside.
 Walking Out will be screened as part of Sundance Festival: London on 3 and 4 June at Picture House Central
