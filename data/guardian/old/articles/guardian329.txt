__HTTP_STATUS_CODE
200
__TITLE

Double honours for Daniel Taylor as the Guardian wins four SJA awards

__AUTHORS

Guardian sport
__TIMESTAMP

Monday 27 February 2017 17.31EST

__ARTICLE
Daniel Taylor picked up two major prizes at Monday nights prestigious Sports Journalists Association awards for his reporting of the football abuse scandal, being named football journalist of the year and winning scoop of the year.
Taylors original Guardian story, published in November, led to the reporting of hundreds of historical allegations of sexual abuse. Last month detectives were examining possible attacks on 526 people, with investigations by 20 police forces who had identified 184 potential suspects and 248 affected clubs at all levels of the game.
Judges said he wrote the football story of the year, one which made headlines across all media. They added it was a huge story with vast and far-reaching consequences well beyond the sport itself and paid tribute to his superb, sensitive, powerful journalism.
In a very competitive category, the winners insight and revelations about the sex scandal that has besmirched football was explosive and challenging, the judges added.
Andy Woodward, the former Crewe player whose decision to waive his anonymity and speak with Taylor prompted a flood of other victims to come forward, was also present at the awards and addressed the audience.
Without all of you, this would never have happened. Its mind blowing. Im going to try and continue to safeguard children moving forward, Woodward said. 
This is important in sport. Not just football, but all sports. I cant thank all of you journalists enough from the bottom of my heart.
The judging panel added that the football abuse scandal was a difficult story, brilliantly and sensitively told. The facts of the story were horrifying and shocking but the winners style and sensitive approach managed to make these facts ever-so-slightly easier to read.
There were also awards for Sean Ingle, named specialist correspondent of the year for his athletics reporting, including the Russian doping scandal, David Rudishas 800m Olympic gold and his column on an ultra-marathon runner whose cheating was exposed by sponsors.
Judges described Ingles work as personable, with an almost anecdotal tone, which makes for easy, informative reading. Describing Ingle as a modern story-teller, the panel also commended his style, authority, investigative zeal and compelling grasp of narrative structure.
The Guardians sport website was named website of the year for the third time in succession. Judges said it was the destination of choice for serious sports fans. Live coverage is invariably elegant, witty and most importantly quick. The site looks handsome, is easily navigable and boasts some of the finest sports journalism around.
The Guardian and Observer were also highly commended in six categories: cricket writers Vic Marks and Rob Smyth, columnist Marina Hyde, Television Sport for our Hillsborough documentary, Owen Gibson for sports news reporter, and multimedia package for our coverage of Rio 2016.
Ladbrokes Football Journalist: Daniel Taylor
Specialist Correspondent: Sean Ingle
Sports Scoop: Daniel Taylor  Andy Woodward interview and sexual abuse coverage
Sports Website: www.theguardian.com/sport
Highly commended: Vic Marks, Rob Smyth, Marina Hyde, Owen Gibson, Television Sport (Hillsborough documentary), Rio 2016 multimedia package.
