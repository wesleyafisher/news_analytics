__HTTP_STATUS_CODE
200
__TITLE

Raqqa recaptured from Islamic State by US-backed forces

__AUTHORS

Saeed Kamali Dehghan
__TIMESTAMP

Tuesday 17 October 2017 12.44EDT

__ARTICLE
The Syrian city of Raqqa, once de facto capital of the Islamic States self-declared caliphate, has fallen to US-backed forces after a gruelling four-month battle.
The recapture of Raqqa after three years of Isis rule is a symbolic loss to the terrorist group, which is under intense pressure in Syria and neighbouring Iraq, forced into a strip of the Euphrates valley and surrounding desert between the two countries.
The Kurdish-Arab Syrian Democratic Forces (SDF) said on Tuesday that they had captured the citys stadium and the nearby hospital, which were the last holdouts of the terrorist group in the city. But they fell short of issuing an official statement announcing the liberation of Raqqa. 
Military operations have ended in Raqqa, but sweeping operations are continuing to destroy sleeper cells if they exist and to cleanse the city from mines, Talal Selo, a SDF spokesman, said in a statement. The situation is under control in Raqqa and soon we will announce the liberation of the city.
Selo said the remaining Isis fighters who had not accepted the safe passage deal negotiated by the local council had either surrendered or been killed. The terrorists have been eliminated completely from Raqqa, he added. Obviously we are doing sweeping operations and there may be a terrorist here or there who will be destroyed, but currently the terrorist presence in Raqqa has been eliminated.
A Pentagon spokesman said the SDF had seized the last Isis strongholds, but warned that the advancing militiamen could still encounter pockets of resistance from about 100 fighters who remained in the city. 
News agencies reported jubilant militiamen raising yellow flags in the citys al-Naim square  which under Isis rule was known as Hell Roundabout because of its use as a setting for public executions. 
The UK-based Syrian Observatory for Human Rights also said in a statement: With [SDF] control over the local municipal stadium, the city of Raqqa is now completely outside the control of the Islamic State and its completely empty of Isis members. Subsequently, in the entire Raqqa province the presence of Islamic State has ended.
 Islamic State first captured Raqqa in early 2014 before spreading to a number of other major cities in Syria and Iraq, including Mosul. It is believed to have used the city as a command centre for operations in the Middle East and well as in the west. A number of westerners imprisoned by Isis were also held there before being killed.
Lina Khatib, head of the Middle East and north Africa programme at Chatham House, said the liberation of Raqqa was an emblematic loss for Isis because it put an end to its claim of ruling a physical Islamic state, but she warned that it was still not clear who would control the city now. 
If the SDF ends up governing Raqqa, there are concerns about ethnic tensions between the local residents of Raqqa  who are mostly Arab  and the Kurdish-majority SDF, she told the Guardian. The SDF has also in the near past handed over areas it had taken over from Isis to the Syrian army. Repeating this scenario in Raqqa would plant the seed for a new wave of clashes.
Umm Abdullah, a 44-year-old Raqqa native, was forced to leave the city after its capture by Isis and fled into exile in Kobani, 70 miles away. The AFP news agency quoted her as saying: I cant describe my happiness. When my sister told me it had been freed, she started to cry, and then I started to cry. Thank God. Thank God.
After three years of rule under Isis and a series of airstrikes and ground fighting, the city is a panorama of ruined buildings and rubble. 
The SDF began its assault on Raqqa in June but a few hundred jihadists remained in the city despite a deal that allowed many to leave on Sunday. 
On Tuesday, Isis suffered another serious setback in the province of Deir ez-Zor, where its jihadists are still in control of at least five neighbourhoods. There, Russian and Iran-backed government forces were reported to have captured large swaths of territory, stretching between Deir ez-Zor and Mayadeen.
