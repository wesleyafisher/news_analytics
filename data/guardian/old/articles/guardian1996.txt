__HTTP_STATUS_CODE
200
__TITLE

Kristi McCluer's best photograph: playing golf while America burns

__AUTHORS
Interview by 
Tim Jonze
__TIMESTAMP

Wednesday 27 September 2017 11.37EDT

__ARTICLE
I noticed the Eagle Creek fire when I was 14,000 feet in the air above Oregon in early September. I was preparing for my third skydive of the day and, through the plane door, I spotted a vertical plume of smoke that hadnt been there on my previous jump. A lot of fires pop up at this time of year, so it wasnt surprising  but I didnt realise how big it would become or that it was Eagle Creek, which is one of my favourite trails to hike.
A couple of days later, after the fire had spread, I went to take photos. I ended up being redirected and found myself in the parking lot of a golf course that I didnt know existed. I completely stumbled on this scene. 
There was nobody on the green when I started shooting. I just wanted to capture the fire, which was both beautiful to look at and horrifying to think about. When the golfers turned up, they were in the way of my picture! But I realised it was a cool shot with them in it. It was their last hole of the day and, to me, the scene sums up how life goes on regardless. They might look as if theyre oblivious to this huge thing going on behind them but the picture is just 1/800th of a second in time. They were very concerned and talking about how terrible it was. 
This image was a lucky shot  no pun intended; Im not a professional photographer. You can tell from the picture that I dont entirely know what Im doing  its a little grainy, theres a bunch of ash in the air. I was trying to change my camera settings to get the best picture but the light was changing constantly. The fire would flare up, then get covered by smoke. But its the image, not the quality of the print, that caught peoples attention.
I understand why the picture is seen as a metaphor for climate change. But according to reports, the fire was caused by a 15-year-old who tried to throw a firework into Punch Bowl falls. He missed and started a load of havoc.
People say it was dangerous to be out there, but thats not true. The fire was more than a mile away across the river, and although embers can cross water, a fire needs fuel to burn. The golf course was well watered with no dry trees. You couldnt even feel the heat from where we were.
I was not prepared for the reaction to the photo. A friend posted it to Reddit  I didnt even know what Reddit was. Then one day I turned on the TV, and there was my picture on the morning news show. I was like: How did they get that on there? 
I heard that someone called David Simon had tweeted the photo. [He posted it on Twitter with the caption: In the pantheon of visual metaphors for America today, this is the money shot.] I thought OK, well, whatever. Then someone said: Is that the David Simon who created The Wire? I was like, oh my God, thats my favourite TV show. The reaction has been overwhelming.
Some people think the picture is faked and I can see why. Its a crazy, bizarre scene  who golfs next to a fire? Also, I dont have a great lens on my camera  you can see a line at the point where the darkness of the golfers in black meets the lightness of the green grass, which makes it look altered. But thats just an effect called chromatic aberration, and if I had a better camera it wouldnt be there. Maybe after this I will buy a camera with a better lens.
The fire spread across almost 50,000 acres, and its still smouldering today despite four days of rain. It will be a long time before anyone can get back on the trails: several big trees are still to fall and there will be landslides during the rainy season. But the Pacific Crest Trail Association does an amazing job of fixing things.
I normally take pictures of birds, waterfalls and flowers, which I put on calendars for friends and family. I dont play golf, but my parents and several friends do, so this picture will probably end up on their calendars next year.
Born: Spokane, Washington, 1966.
Training: No training  Im an amateur photographer.
Influences: Nature and wildlife inspire most of my shots. I see beautiful pictures on social media, and think, how can I learn to take a picture like that?
High point: This picture.
Low point: Losing an SD card full of pictures I hadnt backed up.
Tip: Always keep a camera with you  you never know what you might find. 
