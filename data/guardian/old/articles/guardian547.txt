__HTTP_STATUS_CODE
200
__TITLE

Readers recommend playlist: songs featuring echoes

__AUTHORS

Chaz Cozens
__TIMESTAMP

Thursday 12 October 2017 07.00EDT

__ARTICLE
Here is this weeks playlist  songs picked by a reader from hundreds of suggestions on last weeks callout. Thanks for taking part. Read more about how our weekly series works at the end of the piece.
The response to last weeks callout was more muted than usual and seemed to draw lots of suggestions from the same nominators, creating a bit of an echo below the line, if you will. That said, you still managed to nominate nearly 300 songs, comprising more than 20 hours of music. No wonder I dont seem to have had time to do much else but listen (with one notable exception, which I will get to later).
I will start the A-list with two songs that reference the myth of Echo and Narcissus, from which we get the word echo. First, Cocteau Twins Mud and Dark; second, Daniel Land & the Modern Painters Echo & Narcissus.
Next, we get to tracks that make use of echo and reverb-like effects in the music. As contributor Pussel put it: Cant have this topic without dub. So, well have Gregory Isaacs Public Eyes, which makes plentiful use of such things, as does Neko Cases Hold On, Hold On, albeit in a different genre. There is also the fabulous use of echo in the introduction to Madnesss One Step Beyond. Wrapping up this section is the incredible use of echo effects by Brian May on Queens Brighton Rock.
The next sub-division is songs that made it on to the A-list because they include the word echo in the lyrics. Harry Nilsson can only hear the echoes of my mind even though Everybodys Talkin. David Coverdale, meanwhile, hears the songs that I have sung echo in the distance on Deep Purples Soldier of Fortune. Three versions of this track were nominated; I went for the original, since it features Coverdales soulful vocals and Ritchie Blackmores atmospheric guitar playing. Last here are Simon and Garfunkels words  Like silent raindrops fell and echoed in the wells of silence  in the wonderful The Sound of Silence.
Now we get to songs with echo in the title. First up is a Scottish folk song, Echo Mocks the Corncrake, by Songs of Separation. The title refers to the way in which the corncrake cry is reflected back from the stone ends of crofts and how the poor bird tries to have a conversation with itself. Then we get to a couple of pieces of whimsy: the Ink Spots We Three (My Echo, My Shadow and Me) is a melancholy meditation on being single, while the Bonzo Dog Doo Dah Band tell us of Little Sir Echo. The title of Rushs song, Test for Echo, refers to a standard location sound test, in this case in the context of increasing media involvement in criminal proceedings.
Several people nominated the next song as a must for this week. I couldnt agree more, so here is Echo by the late Tom Petty, with the Heartbreakers.
Finally, we come to the pachyderm in the pantry. My final choice will be controversial, since its zedded. However, I found time to do one thing other than listen to your recommendations this week. That thing was to visit the superb Their Mortal Remains exhibition at the V&A. When told this song was zedded, one nominator replied:
But zedded! 
Sure, but fuck that! It's the best and most appropriate song for this topic.
 I completely agree. So, featuring the topic in the title, the lyrics and the music, here, with 23 minutes of prog brilliance, we end on Pink Floyds Echoes.
The new theme will be announced at 8pm (BST) on Thursday 12 October. You have until 11pm on Monday 16 October to submit nominations.
Here is a reminder of some of the guidelines for readers recommend:
