__HTTP_STATUS_CODE
200
__TITLE

Carbs, fags and wine: Roisin Conatys GameFace, a tragicomic portrait of a life unravelled

__AUTHORS

Fiona Sturges
__TIMESTAMP

Saturday 14 October 2017 05.42EDT

__ARTICLE
If you throw yourself off this building its going to be really bad  for me, groans Marcella in the darkly comic GameFace (19 October, 9.30pm, E4). She only popped up to the office roof for a ciggie and now shes stuck trying to talk down a stranger whos intent on ending it all.
To be fair, life hasnt been kind to Marcella either. She is, as she explains to Suicidal Woman, an actor who hasnt had a part in four years and who, in between temping jobs, is reduced to playing fairy princesses at childrens parties on Saturday mornings with a banging hangover. Furthermore, Marcella is 28K in debt and still mourning the ex who dumped her after 12 years and promptly married a woman hed known for six days. And Ive got fat hands, she adds, triumphantly.
Christ, it sounds like you should jump, mate, says Suicidal Woman, not unreasonably.
Set in London, GameFace is the brainchild of writer and actor Roisin Conaty, last seen propping up Greg Davies in Man Down. With her tragicomic portrait of a woman whose life has unravelled, there are echoes of Phoebe Waller-Bridges brilliant Fleabag here, although GameFace is far from an imitation, not least because the pilot was screened two years before Fleabag aired.
Still, there are similarities, particularly in Conatys protagonist who, rather than being winsome and lovable, is gratifyingly messed up. Marcella is in her 30s, which, in sitcom-world, usually signals a husband, a brace of kids and a neutral, age-appropriate wardrobe that will see her all the way to retirement. Not here. Not only is Marcella single, up for a night out and partial to a push-up bra, she is unreliable, eats crap food and looks as if she could do with a good bath. You can practically smell the booze on her as she is ejected from yet another kids party and totters home in a giant princess dress, teeth stained with red wine, her hair caked in something unmentionable.
Whether drunk-texting her ex, getting lost while camping in a wood or trying to hide the full extent of her hopelessness from her life coach, Graham (the sessions were a birthday present from her mother), life conspires to deliver a fresh kick in the teeth every day.
How do you deal with stress? asks Graham. Carbs, fags, wine, Marcella shrugs.
Where Fleabag centres on a woman undone by self-loathing, underneath Marcellas Barbie-meets-Bet-Lynch exterior is a woman fitfully trying to get it together. GameFaces parade of brilliantly awkward set-pieces is underpinned by Marcellas essential optimism that things will get better. Its the kind of big-hearted comedy that somehow manages to make depression and catastrophic loneliness funny.
Self-sabotage is a running theme as Marcella routinely forgets the driving lessons that will, she hopes, give her new independence. Elsewhere, she does battle with her conspiracy-theorist neighbour, listens patiently to the brocialist prattling of her flatmates boyfriend and dutifully turns up to auditions and gives them her all, even if it means rubbing onion in her eyes to make herself cry. GameFace isnt just about one womans nightmare. Its an affectionate paean to all the Marcellas out there gamely staggering through life, screwing up and getting screwed. Seriously, have a drink, babes. You look like you need it.
