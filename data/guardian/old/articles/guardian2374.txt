__HTTP_STATUS_CODE
200
__TITLE

Kepos Street Kitchen's zucchini, sujuk and labneh omelette recipe

__AUTHORS
Michael Rantissi & Kristy Frawley
__TIMESTAMP

Saturday 30 September 2017 17.24EDT

__ARTICLE
Michael Rantissi and Kristy Frawley are the couple behind Sydneys popular Kepos Street Kitchen and Kepos & Co restaurants. Their first cookbook Falafel for Breakfast, focused on the restaurants signature dishes. Their latest book, Hummus & Co, is full of the Middle Eastern recipes they make at home to share with their friends and family.
serves 4
This dish, which sits somewhere between an omelette and a frittata, is so easy to make and is excellent served for brunch or even a Sunday night dinner. I dont think we celebrate the versatility of eggs enough  they are quick to prepare, nutritious and so much more than a breakfast food. 
The main ingredient in this dish is zucchini  and it may seem that there is a lot of it, but it helps to give the omelette its light, fluffy texture. The sujuk sausage adds a salty, spicy kick. 
800g zucchini, about 4 in total, coarsely grated1 tbsp salt100ml olive oil200g sujuk sausage, diced4 eggs2 tbsp labneh, plus 4 tbsp extra to serve3 tbsp chopped flat-leaf (Italian) parsley2 large handfuls mixed herbs, such as chives, flat-leaf parsley, mint and coriander, leaves picked70g pine nuts, toastedJuice of  lemon
Put the grated zucchini in a bowl and sprinkle with the salt. Set aside until the zucchini releases its juices, about 15 minutes. Drain the zucchini, return to the bowl and set aside.
Preheat the oven to 180C.
Heat 2 tablespoons of the olive oil in a 24cm (9 inch) ovenproof frying pan over mediumhigh heat. Fry the sujuk until crisp, then transfer to a plate and set aside. Leave the excess oil in the pan to cook the omelette.
Add the eggs, labneh, chopped parsley and half the crispy sujuk to the zucchini. Season with freshly ground black pepper and whisk to combine. 
Reheat the frying pan over medium heat. Pour in the egg mixture and cook for 23 minutes, then transfer the pan to the oven and cook for 10 minutes, or until the omelette is done to your liking. 
While the omelette is cooking, put the remaining crispy sujuk, mixed herbs, pine nuts, lemon juice and remaining olive oil in a bowl and toss to combine.
Flip the cooked omelette on to a plate and serve topped with dollops  of labneh and the sujuk and herb mixture.
