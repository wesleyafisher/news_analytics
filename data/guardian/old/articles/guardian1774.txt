__HTTP_STATUS_CODE
200
__TITLE

'Why would we invest in a disabled person?': fighting bigotry in Mozambique

__AUTHORS

Lucy Lamble
__TIMESTAMP

Tuesday 10 October 2017 05.00EDT

__ARTICLE
At 21, Mateus Mbazo from Sofala province in Mozambique faced a stark choice: starve or steal. Recently orphaned, with disabilities on his right side affecting his arm and leg, Mbazo had to feed himself and his two younger brothers. He had missed out on schooling, and getting a job to earn even a meagre sum was a difficult task so a life as a petty thief seemed the only option. 
At his church that he heard about an initiative that would change his life: a programme offering training for people with disabilities. 
Now 23, his biggest concern is not how to provide for his family, but defending the plentiful harvest in his market garden business from local goats, who love to munch on his crops. On his carefully tended plot, lent by the landowner, onions, tomatoes, cabbages, sprouts and lettuce are flourishing.
Mbazos success is unusual in Mozambique, where people with disabilities are four times more likely to be out of work than their contemporaries. In some places, stigma, such as fear of contagion, persists. This reflects the broader picture within the developing world, where its estimated that between 80-90% of disabled people are unemployed  by comparison in the UK that figure is 52%. 
Mbazos training came from international organisation Light for the World together with social enterprise Young Africa. They are working to give people with disabilities the skills to make a living and in a programme in Sofala, have taught 160 young people  alongside more than 13,500 able-bodied students  in subjects from tailoring and cooking to welding and electrical engineering.
Young Africas director, Aksana Varela, and her team are proud of their efforts: the centre has been fitted out with ramps, lecturers have been trained in sign language and assistive computer software installed in the library.
 Priority is given to the most disadvantaged.
Chef Joana Nhantote, 27, knows how difficult it can be to persuade employers to look beyond a disability. She lost her hearing at 13.
It was hard to [find] a job  not because of the work itself, but because of the discrimination for me being deaf, she signs.
Now in a permanent job, she says: It is a bit different from cooking at home. I didnt know the ingredients, the type of seasoning and how you use it, but now since I finished the course, I can distinguish one from another. Im a better cook. I love working here.
Restaurant owner Dauva Barrientos admits that initially she found it hard to train Nhantote. But then I stopped and thought to myself  I need to understand how she does things and how she can best understand me. Whatever I do in the kitchen or here at the restaurant she understands me immediately. I do things and I show her, and then she sees whatever I am doing and she does it exactly how I did it. Shes very smart.
Its not just finding a job that is a struggle. Families cant always invest as much in a child who is unlikely to be a future provider. At least half of the worlds 65 million school-age children with disabilities are not in primary or lower secondary school. 
Professor Tom Shakespeare, chair of Light for the World, says many families dont seek out what little support is there. Often people just dont bother, he says. They think, Why would we invest in a disabled child?
We reach out to the disabled child, support them, make sure that mum or dad sends them to school in the first place. Without education, disabled people are really sunk.
For 11-year-old Marta, who has a malformed lower leg, physiotherapy and a set of crutches have meant she can go to school. Ramps have been installed at her primary school in Buzi, a small coastal town where her father is a fisherman, and teachers are ready to support her. 
Her mother, Isabel Jos, says that since Martas support worker spent time in the community talking about the issues facing children with disabilities, people have become far kinder towards Marta. Some of them, they used to laugh at her, looking bad at my child. Whenever that happens I protect my child with all my strength. I explain to them that she didnt choose to be disabled and that it could happen to any of us, she says.
The dream that I have for my child is that she focus on her studies, and I expect my child to be a director one day, an engineer, informed with a position in society.
If Marta leaves school with just basic numeracy and literacy, she will already be ahead of many of her peers, chipping away at the inequality of opportunity.


 The take-up of this new approach  known as community-based rehabilitation  in Mozambique owes much to Sofalas director of gender, child and social action, Jos Diquissone Tole, himself blind. I benefited from an inclusive education, he says. It was on my mind that disabled people should be included in their community and work side by side, not in special places.
Everyone understands that this is a good way to include people in society but we need to build a strong network involving other sectors like education and health and we need to look more at the decision-making level. 
Shakespeare says governments can see the value. This is a hard-to-reach population that are sometimes written off as hopeless but here we have CBR joining the dots, joining the people to existing services and enabling them to be economically productive. Its what everyone wants.
Market gardener Mateus Mbazo is in no doubt of the power of the initiative. My dream is to develop my skills and become a famous farmer, he says. 
