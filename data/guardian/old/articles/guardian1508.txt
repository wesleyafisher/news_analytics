__HTTP_STATUS_CODE
200
__TITLE

'This is a really big deal': Canada natural gas emissions far worse than feared

__AUTHORS

Ashifa Kassam in Toronto
__TIMESTAMP

Tuesday 17 October 2017 17.41EDT

__ARTICLE
 Albertas oil and gas industry  Canadas largest producer of fossil fuel resources  could be emitting 25 to 50% more methane than previously believed, new research has suggested. 
The pioneering peer reviewed study, published in Environmental Science & Technology on Tuesday, used airplane surveys to measure methane emissions from oil and gas infrastructure in two regions in Alberta. The results were then compared with industry-reported emissions and estimates of unreported sources of the powerful greenhouse gas, which warm the planet more than 20 times as much as similar volumes of carbon dioxide.
Our first reaction was Oh my goodness, this is a really big deal, said Matthew Johnson, a professor at Carleton University in Ottawa and one of the studys authors. If we thought it was bad, its worse.
Carried out last autumn, the survey measured the airborne emissions of thousands of oil and gas wells in the regions. Researchers also tracked the amount of ethane to ensure that methane emissions from cattle would not end up in their results. 
In one region dominated by heavy oil wells, researchers found that the type of heavy oil recovery used released 3.6 times more methane than previously believed. The technique is used in several other sites across the province, suggesting emissions from these areas are also underestimated. 
In the second region, home to a mix of gas and light oil wells, researchers found results that were roughly equal to those reported by industry and unreported sources. However, they found that only 6% of methane emissions in this region were from industry-reported sources, with the remaining emissions, known as fugitive emissions, from unreported sources such as unintentional equipment leaks.
The finding could have major implications as Alberta and Ottawa strive to reduce methane emissions by 45% from 2012 levels by 2025, said Johnson. It shows how much isnt captured in current reporting requirements, and therein is a challenge and an opportunity all wrapped in one. 
The study then sought to conservatively extrapolate the findings, correcting only for sites that are home to heavy oil. What they found was in Alberta  home to 68% of Canadas natural gas production, 47% of its light crude oil production as well as 80% of all crude oil and equivalents  total emissions were likely 25 to 50% higher than previous government estimates. The findings excluded mined oil sands, which are believed to be responsible for about 11% of methane emissions.
Canadian advocacy group Environmental Defence described the findings as alarming. The methane gas currently being wasted would supply almost all the natural gas needs of Alberta, and is worth $530m per year, Dale Marshall of the organisation said in a statement. This represents an economic cost for governments in the form of lost royalties and taxes, and for industry in terms of revenue.
Marshall pointed to the readily available solutions for controlling leaks and intentional releases of methane gas, portraying them as some of lowest cost strategies available to reduce carbon emissions. 
Researchers said they have already begun presenting their findings to various levels of government, depicting it as a chance for industry and regulators to more effectively tackle emissions of methane  a gas far more potent than CO2 but which persists for less time in the atmosphere. 
When you take methane emissions and convert them to CO2 emissions so you can compare to cars, for Alberta, the total methane were talking about on a 100-year scale is 8 to 9.7 million vehicles. If we do it on a 20-year timescale, were talking maybe 28 to 33 million vehicles, said Johnson. This is a real opportunity.
