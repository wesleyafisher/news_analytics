__HTTP_STATUS_CODE
200
__TITLE

'We're walking down a dark path': Biden hammers Trump in scathing speech

__AUTHORS

David Smith in Washington
__TIMESTAMP

Thursday 5 October 2017 20.51EDT

__ARTICLE
Joe Biden, the former US vice-president, has taken off the gloves with a scathing denunciation of Donald Trump and the existential threat he poses to the postwar international order.
Passionate and pugnacious, his voice sometimes erupting in anger, Biden warned on Thursday that the US was heading down a very dark path and urged Washingtons foreign policy establishment to take a stand.
It is rare for ex-presidents or vice-presidents to criticise their successors actions. So far Barack Obama has carefully picked his targets, issuing a few pointed remarks or written statements. But Biden, 74, speaking to an invited audience at a thinktank, decided to let rip.
I really feel incredibly strongly that the women and men sitting before me, who have been the intellectual backbone of the foreign policy establishment in this country for decades, have to start to speak out, he said. President Obama and I have been very quiet and respectful, giving the administration time, but some of these roots are being sunk too deeply. I believe its time to challenge some of the dangerous assumptions that are attempting to replace that liberal world order. 
Biden, who spent four decades serving in high public office, was receiving the Zbigniew Brzezinski annual prize at the Center for Strategic and International Studies thinktank. The auditorium was packed, with many people standing, and hushed as he offered a damning verdict on the Trump presidency.
Acknowledging that many Americans feel left behind by globalisation, he said: The appeal to populism and nationalism is a siren song, a way for charlatans to aggrandise their power, raise themselves up, break down those mechanisms that were designed, whether in our constitution or internationally, to limit the abuse of power, and destabilise the world.
Its not alarmist. Were walking down a very dark path that isolates the United States on the world stage and, as a consequence, endangers  not strengthens  endangers American interests and the American people.
In a dig at Trumps past career as a New York property developer, Biden said: Rather than building a shared narrative of freedom and democracy that inspires nations to unite in common goals, this administration casts global affairs in a dog-eat-dog competition, like its a competition: who gets that plot to build the new high-rise building.
Biden, tipped as a possible Democratic candidate for president in 2020, showed his willingness to hammer the president hard. Among the many problems plaguing this administrations foreign policy: ideological incoherence, inconsistent and confusing messaging, erratic decision-making, unwillingness or inability to solve problems caused by understaffing. Whens the last time, in the state department, you can stand on the seventh floor and yell and hear an echo?
There was some laughter from the audience but Biden pointed and chided: No, you think Im kidding. Its irresponsible. Its this brand of zero-sum thinking that I find the most disturbing and dangerous.
Both Democratic and Republican administrations in the past had worked to uphold the liberal international order, he continued. The presidents vision, in contrast, is grounded in a disturbing admiration for autocrats and an apparent faith that the world will mistake bluster and bullying for strength.
He derided Trumps recent appearance at the UN, where the president emphasized national sovereignty and self-interest. To stand in the well of the general assembly, and wave the flag of narrow nationalism, while warning of a future vulnerable to decay, dominion and defeat marks a dangerous revision of political small-mindedness that led the world to consume itself in two world wars in the last century, and it abandons Americas hard-won position as the indispensable nation, as a leader that inspires more than fear.
The former vice-president criticised Trump for pulling out of the Trans-Pacific Partnership and threatening to scrap the Iran nuclear deal, which Biden said would isolate the US rather than Iran: Next time, were not going to have the world on our side. Such a move would have a knock-on effect on negotiations with North Korea, he contended, where Trumps aggressive rhetoric has already caused untold damage.
Trading insults. Deploying taunting nicknames. Promising to totally destroy a country of 25 million people. Such erratic action only worsens the crisis and rejects the possibility of diplomacy.
Biden, who received a standing ovation and cheers, warned of the threat posed by Russia and accused President Vladimir Putin of seeking to destabilise the west so he could target Ukraine and Georgia. So far President Trump has been unwilling to call out Putin for Russias meddling, even in our own democratic process, or criticise his action. Think of that. Think of the signal it sends around the world, for Gods sake.
Biden also recalled a meeting with Putin, who was showing off his magnificent office. I held my arms up, I said: Its amazing what capitalism will do  I said, Mr President, Im looking in your eyes and you have no soul. And he looked back at me and he said: We understand one another.
