# Import the packages installed from pip
import requests
from bs4 import BeautifulSoup

# Instantiate conditional variables
urlCount = 0
count = 0
titleExists = True
authorExists = True
timestampExists = True
articleExists = True
# Get the URL from each line in the file
with open('master-urls2-final.txt', 'r') as masterList:
    f = open('guardian.csv', 'w')
    f.write('year,month,day,section,headline,story\n')
    for url in masterList:
        urlCount += 1
        # Strip the newline character off the url or it will 404
        stripNewLine = url.rstrip('\n')
        # Get the HTML page from the URL
        page = requests.get(stripNewLine)
        # Use BeautifulSoup to get the page content in an object
        soup = BeautifulSoup(page.content, 'html.parser')
        if (page.status_code == 200):
            # Get title
            title = soup.find('h1', class_='content__headline')
            if title is not None:
                f_title = title.getText().encode('ascii', 'ignore')
                titleExists = True
            else:
                titleExists = False

            # Get timestamp
            time = soup.find('time', class_='content__dateline-wpd js-wpd')
            if time is not None:
                try:
                    time = time['datetime']
                    f_year = time[:10].split('-', 2)[0]
                    f_month = time[:10].split('-', 2)[1]
                    f_day = time[:10].split('-', 2)[2]
                    timestampExists = True
                except(AttributeError, KeyError):
                    timestampExists = False
            else:
                timestampExists = False
            # Get section
            f_section = url[28:].split('/', 1)[0]

            # Get article
            tempArticle = soup.find('div', class_='content__article-body from-content-api js-article__body')
            if tempArticle is not None:
                articleText = tempArticle.find_all('p')
                if articleText is not None:
                    f_article = ''
                    for p in articleText:
                        if p is not None:
                            f_article += p.getText().encode('ascii', 'ignore')
                            articleExists = True
                        else:
                            articleExists = False
                else:
                    articleExists = False
            else:
                articleExists = False

            # Write the file
            if (titleExists and authorExists and timestampExists and articleExists):
                count += 1
                # Get rid of commas
                f_year = f_year.replace(',', '')
                f_month = f_month.replace(',', '')
                f_day = f_day.replace(',', '')
                f_section = f_section.replace(',', '')
                f_title = f_title.replace(',', '')
                f_article = f_article.replace(',', '')
                f_year = f_year.replace('\n', '')
                f_month = f_month.replace('\n', '')
                f_day = f_day.replace('\n', '')
                f_section = f_section.replace('\n', '')
                f_title = f_title.replace('\n', '')
                f_article = f_article.replace('\n', '')
                # year, month, day, section, title, article
                f.write(f_year)
                f.write(',')
                f.write(f_month)
                f.write(',')
                f.write(f_day)
                f.write(',')
                f.write(f_section)
                f.write(',')
                f.write(f_title)
                f.write(',')
                f.write(f_article)
                f.write('\n')
            # Clear the strings
            f_year = ''
            f_month = ''
            f_day = ''
            f_section = ''
            f_title = ''
            f_article = ''
            print 'Processing URL ' + str(urlCount)
    f.close()
