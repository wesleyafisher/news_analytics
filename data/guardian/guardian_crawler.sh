#!/bin/bash

httrack -p0 -r3 www.theguardian.com/us-news
cp hts-cache/new.txt urls2/us-news_urls.txt
httrack -p0 -r3 www.theguardian.com/us-news/us-politics
cp hts-cache/new.txt urls2/us-politics_urls.txt
httrack -p0 -r3 www.theguardian.com/world
cp hts-cache/new.txt urls2/world_urls.txt
httrack -p0 -r3 www.theguardian.com/us/commentisfree
cp hts-cache/new.txt urls2/opinion_urls.txt
httrack -p0 -r3 www.theguardian.com/us/technology
cp hts-cache/new.txt urls2/technology_urls.txt
httrack -p0 -r3 www.theguardian.com/us/culture
cp hts-cache/new.txt urls2/arts_urls.txt
httrack -p0 -r3 www.theguardian.com/us/lifeandstyle
cp hts-cache/new.txt urls2/lifestyle_urls.txt
httrack -p0 -r3 www.theguardian.com/us/business
cp hts-cache/new.txt urls2/business_urls.txt
httrack -p0 -r3 www.theguardian.com/us/travel
cp hts-cache/new.txt urls2/travel_urls.txt
httrack -p0 -r3 www.theguardian.com/us/environment
cp hts-cache/new.txt urls2/environment_urls.txt
