#!/bin/bash

httrack -p0 -r3 www.greenvilleonline.com/news
cp hts-cache/new.txt urls/news_urls.txt
httrack -p0 -r3 www.greenvilleonline.com/sports
cp hts-cache/new.txt urls/sports_urls.txt
httrack -p0 -r3 www.greenvilleonline.com/downtown
cp hts-cache/new.txt urls/downtown_urls.txt
httrack -p0 -r3 www.greenvilleonline.com/entertainment
cp hts-cache/new.txt urls/entertainment_urls.txt
httrack -p0 -r3 www.greenvilleonline.com/business
cp hts-cache/new.txt urls/business_urls.txt
httrack -p0 -r3 www.greenvilleonline.com/life
cp hts-cache/new.txt urls/life_urls.txt
httrack -p0 -r3 www.greenvilleonline.com/opinion
cp hts-cache/new.txt urls/opinion_urls.txt
