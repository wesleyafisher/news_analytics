#!/bin/bash

httrack -p0 -r3 www.thetigernews.com/news
cp hts-cache/new.txt urls/news_urls.txt
httrack -p0 -r3 www.thetigernews.com/outlook
cp hts-cache/new.txt urls/outlook_urls.txt
httrack -p0 -r3 www.thetigernews.com/timeout
cp hts-cache/new.txt urls/timeout_urls.txt
httrack -p0 -r3 www.thetigernews.com/letters_to_editor
cp hts-cache/new.txt urls/letters_to_editor_urls.txt
httrack -p0 -r3 www.thetigernews.com/sports
cp hts-cache/new.txt urls/sports_urls.txt
