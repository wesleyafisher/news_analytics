# Keven Fuentes
# Word Cloud for Fox News, NY Times, and Tiger New

# Clear workspace
rm(list = ls())

# Include library
library(RTextTools)
library(readr)
library(tm)
library(SnowballC)
library(wordcloud)

#Remove common and general terms shared by two sides and not topics in the news coverage
commonwords <- c("think","news","told","list",
                  "get","said","will","new","people","one","like",
                 "also","just","say","much","first","among","may","two",
                 "well","far","now","mrs","called","without","day","long","yet",
                 "still","need","time","made","another","use","next","might",
                 "part","though","already","whether","even","including","way",
                 "going","back","according","take","dont","sen","last","asked",
                 "reading","saying","make","rep","contributed","work","says",
                 "minutes","earlier","week","call","percent","group","reported",
                 "terms","event","know","run","state","states","can","many","year",
                 "since","city","little","north","times","put","used","days",
                 "recent","must","want","three","clear","month","help","come",
                 "tuesday","found","every","groups","decision","several",
                 "took","thats","added","given","big","likely","wednesday",
                 "better","good","often","whose","end","less","administration","around",
                 "see","months","never","least","issue","important","nearly",
                 "become","nearly","pay","ago","october","zone","announced",
                 "monday","thursday","data","continue","making","post",
                  "former","nov","campaign","november","full","brietbarts","house",
                 "thats","dont","follow","com","comments","page","reflects","count","dont",
                 "government","senate","white","didnt","years","chief","congress","donald",
                 "hes","things","county","information","system","point","leaders","office",
                 "federal","doesnt","world","meeting","deal","presidents","policy",
                 "republicans","democrats","republican","democrat","read","bill","article"
                 ,"united","comment","democratic","report","country","bill","wrote","number"
                 ,"small","america","republicans","plan","view","past","posts","committee",
                 "national","visit","site","party","law","effort","home","left","place","lot"
                 ,"order","really","americans","american","court","statement","media","political"
                  ,"officials","political","theres","set","real","great","thats","dont","public",
                 "local","away","four","cant","came","friday","weeks","top","hes","thats",
                 "list0","lists","listconnect","listnew","ntttt","ntt","listwhats")

# Initialize Fox data
foxnews <- read.csv(file.path(getwd(), "fox.csv"),na.strings=c("","NA"), stringsAsFactors = FALSE)
foxnews <-foxnews[foxnews$section=='politics',]

# Create Corpus for Fox 
foxCorpus <- Corpus(VectorSource(foxnews$story))
foxCorpus <- tm_map(foxCorpus, content_transformer(tolower))
foxCorpus <- tm_map(foxCorpus, removeWords, c("faq","said","material","rights","listnew",
                                              "may","news","network","reserved","fox","published",
                                              "broadcast", "rewritten","redistributed","llc","2017",
                                              "ntt","ntttt","listwhats","hes","listfaq","ntt ","oct",
                                              "meeting","breitbart","span","facebook","copyright"))
foxCorpus <- tm_map(foxCorpus, removePunctuation)
foxCorpus <- tm_map(foxCorpus, removeWords, commonwords)
foxCorpus <- tm_map(foxCorpus, removeWords, stopwords('english'))

#Create red word cloud for Fox
wordcloud(foxCorpus, max.words = 100 , random.order = FALSE,colors = "red")


# Initialize NY Times data
nytimes <- read.csv(file.path(getwd(), "NewYorkTimesPolitics.csv"),na.strings=c("","NA"),header = FALSE,  stringsAsFactors = FALSE)
nytimes$V1 <- NULL
names(nytimes)[1] <- "V1"
names(nytimes)[2] <- "V2"
names(nytimes)[3] <- "V3"
names(nytimes)[4] <- "V4"
names(nytimes)[5] <- "V5"
names(nytimes)[6] <- "V6"

# Create Corpus for NY Times
nytimesCorpus <- Corpus(VectorSource(nytimes$V6))
nytimesCorpus <- tm_map(nytimesCorpus, content_transformer(tolower))
nytimesCorpus <- tm_map(nytimesCorpus, removePunctuation)
nytimesCorpus <- tm_map(nytimesCorpus, removeWords, stopwords('english'))
nytimesCorpus <- tm_map(nytimesCorpus, removeWords, commonwords)

#Create blue word cloud for NY Times
wordcloud(nytimesCorpus, max.words = 100, random.order = FALSE,colors = "blue")

# Initialize Tiger News data
tigernews <- read.csv(file.path(getwd(), "tiger.csv"),na.strings=c("","NA"),header = FALSE,  stringsAsFactors = FALSE,quote = "")
tigernews <-tigernews[tigernews$V4=='outlook'|tigernews$V4=='letters_to_editor',]

# Create Corpus for Tiger News
tigernewsCorpus <- Corpus(VectorSource(tigernews$V6))
tigernewsCorpus <- tm_map(tigernewsCorpus, content_transformer(tolower))
tigernewsCorpus <- tm_map(tigernewsCorpus, removePunctuation)
tigernewsCorpus <- tm_map(tigernewsCorpus, removeWords, stopwords('english'))
tigernewsCorpus <- tm_map(tigernewsCorpus, removeWords, commonwords)

#Create Clemson orange word cloud for Tiger News
orange <- rgb(red = 234,green = 106,blue=32,alpha=255,names = NULL,maxColorValue = 255)
wordcloud(tigernewsCorpus, max.words = 100, random.order = FALSE,colors = orange)






